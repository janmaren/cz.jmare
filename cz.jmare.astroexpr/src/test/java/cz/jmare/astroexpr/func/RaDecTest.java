package cz.jmare.astroexpr.func;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import cz.jmare.astroexpr.type.RaDecHolder;
import cz.jmare.math.geometry.entity.RaDec;
import cz.jmare.mexpr.ExpressionParser;

public class RaDecTest {
    @Test
    public void raDecDistTest() {
        Map<Object, Object> variableSuppliers = new HashMap<>();
        variableSuppliers.put("c1", new RaDecHolder("+83:50:43.5 -5:24:57.8"));
        variableSuppliers.put("c2", new RaDecHolder("+83:50:43.5 -5:20:57.8"));
        ExpressionParser expressionParser = new ExpressionParser("distance(c1, c2)", variableSuppliers);
        double dist1 = (double) expressionParser.evaluate();
        assertEquals(4 / 60.0, dist1, 0.001);

        variableSuppliers = new HashMap<>();
        variableSuppliers.put("c1", new RaDecHolder("+83:50:43.5 0:0:0"));
        variableSuppliers.put("c2", new RaDecHolder("+83:50:39.5 0:0:0"));
        expressionParser = new ExpressionParser("distance(c1, c2)", variableSuppliers);
        double dist2 = (double) expressionParser.evaluate();
        assertEquals(4 / 3600.0, dist2, 0.001);


        variableSuppliers = new HashMap<>();
        variableSuppliers.put("c1", new RaDecHolder("0:0:0 0:0:0"));
        variableSuppliers.put("c2", new Supplier<RaDec>() {
            @Override
            public RaDec get() {
                return new RaDec("0:4:0 0:4:0");
            }
        });

        expressionParser = new ExpressionParser("distance(c1, c2)", Map.of("c1", "RaDec('0:0:0 0:0:0')", "c2", "RaDec('0:4:0 0:4:0')"));
        double dist3 = (double) expressionParser.evaluate();
        assertEquals(0.0942, dist3, 0.001);
    }

    @Test
    public void raDecStrTest() {
        ExpressionParser expressionParser = new ExpressionParser("toNiceString(radec)", Map.of("radec", "RaDec('+99:41:54.75 -10:25:30.11')"));
        System.out.println(expressionParser.evaluate());
    }
}
