package cz.jmare.astroexpr.func;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Map;

import org.junit.jupiter.api.Test;

import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.parse.UnknownFunctionException;
import cz.jmare.mexpr.util.classutil.SuppliersRegistrar;

public class InitMexprTest {
    @Test
    public void basicTest() {
        SuppliersRegistrar.setUsePredefinedFunctions(false);
        assertThrows(UnknownFunctionException.class, ()-> {
            new ExpressionParser("sin(x)");
        });

        SuppliersRegistrar.setUsePredefinedFunctions(true);
        ExpressionParser expressionParser = new ExpressionParser("sin(pi)");
        double evaluateDouble = expressionParser.evaluateDouble();
        assertEquals(0, evaluateDouble, 0.0000000001);
    }

    @Test
    public void passVarTest() {
        ExpressionParser expressionParser = new ExpressionParser("sin(x)", Map.of("x", "pi + 0"));
        double evaluateDouble = expressionParser.evaluateDouble();
        assertEquals(0, evaluateDouble, 0.0000000001);
    }

    @Test
    public void passRecursVarTest() {
        ExpressionParser expressionParser = new ExpressionParser("sin(x)", Map.of("x", "pi + y", "y", "0"));
        double evaluateDouble = expressionParser.evaluateDouble();
        assertEquals(0, evaluateDouble, 0.0000000001);
    }
}
