package cz.jmare.astroexpr.type;

import java.util.function.Consumer;
import java.util.function.Supplier;

import cz.jmare.math.geometry.entity.RaDec;

public class RaDecHolder implements Supplier<RaDec>, Consumer<RaDec> {
    private RaDec value;

    public RaDecHolder(RaDec value) {
        this.value = value;
    }

    public RaDecHolder(String value) {
        this.value = new RaDec(value);
    }

    public RaDecHolder() {
    }

    @Override
    public RaDec get() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public void accept(RaDec value) {
        this.value = value;
    }
}
