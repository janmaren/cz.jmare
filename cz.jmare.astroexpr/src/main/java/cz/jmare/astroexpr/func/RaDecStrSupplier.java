package cz.jmare.astroexpr.func;

import java.util.function.Supplier;

import cz.jmare.math.geometry.entity.RaDec;
import cz.jmare.mexpr.arit.FunctionalSupplier;

@FunctionalSupplier(name = "RaDec", description = "It accepts a string with RA (degrees) and DEC (degrees) and returns instace of RaDec which contains RA and DEC. Such instance is cached and input string can't be changed.")
public class RaDecStrSupplier implements Supplier<RaDec> {

    private Supplier<String> raDecSupplier;

    private RaDec raDec = null;

    public RaDecStrSupplier(Supplier<String> raDecSupplier) {
        this.raDecSupplier = raDecSupplier;
    }

    public RaDec get() {
        if (raDec != null) {
            return raDec;
        }
        return raDec = new RaDec(raDecSupplier.get());
    }
}
