package cz.jmare.astroexpr.func;

import java.util.function.Supplier;

import cz.jmare.math.geometry.entity.RaDec;
import cz.jmare.mexpr.arit.FunctionalSupplier;

@FunctionalSupplier(name = "RaDec", description = "Returns instace of coordinates RaDec which contains RA and DEC")
public class RaDecSupplier implements Supplier<RaDec> {

    private Supplier<Double> raSupplier;
    private Supplier<Double> decSupplier;

    public RaDecSupplier(Supplier<Double> raSupplier, Supplier<Double> decSupplier) {
        this.raSupplier = raSupplier;
        this.decSupplier = decSupplier;
    }

    public RaDec get() {
        return new RaDec(raSupplier.get(), decSupplier.get());
    }
}
