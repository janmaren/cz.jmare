package cz.jmare.astroexpr.func;

import java.util.function.Supplier;

import cz.jmare.math.geometry.entity.RaDec;
import cz.jmare.mexpr.arit.FunctionalSupplier;

@FunctionalSupplier(name = "distance", description = "Returns angle distance")
public class DistanceRaDec implements Supplier<Double> {

    private Supplier<RaDec> polarDegSup1;
    private Supplier<RaDec> polarDegSup2;

    public DistanceRaDec(Supplier<RaDec> polarDegSup1, Supplier<RaDec> polarDegSup2) {
        this.polarDegSup1 = polarDegSup1;
        this.polarDegSup2 = polarDegSup2;
    }

    public Double get() {
        RaDec raDec1 = polarDegSup1.get();
        RaDec raDec2 = polarDegSup2.get();

        double sinDelta = Math.sin((raDec2.dec - raDec1.dec) / 2);
        double sinAlpha = Math.sin((raDec2.ra - raDec1.ra) / 2);

        return 2 * Math.asin(Math.sqrt(sinDelta * sinDelta + Math.cos(raDec1.dec) * Math.cos(raDec2.dec) * sinAlpha * sinAlpha));
    }
}
