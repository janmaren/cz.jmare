package cz.jmare.astroexpr.func;

import java.util.function.Supplier;

import cz.jmare.data.util.GonUtil;
import cz.jmare.math.geometry.entity.RaDec;
import cz.jmare.mexpr.arit.FunctionalSupplier;

@FunctionalSupplier(name = "toNiceString", description = "Returns string representing coordinates RA, DEC")
public class ToNiceStringRaDec implements Supplier<String> {
    private Supplier<RaDec> raDecSupplier;

    public ToNiceStringRaDec(Supplier<RaDec> raDecSupplier) {
        this.raDecSupplier = raDecSupplier;
    }

    public String get() {
        RaDec raDec = raDecSupplier.get();
        return GonUtil.formatDegrees(raDec.ra) + ", " + GonUtil.formatDegrees(raDec.dec, true);
    }
}
