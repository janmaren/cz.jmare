package cz.jmare.pureraster;

import org.eclipse.swt.events.MouseEvent;

public interface RasterCursorMoveListener {
    void changedTo(PositionData positionData, MouseEvent mouseEvent);
}
