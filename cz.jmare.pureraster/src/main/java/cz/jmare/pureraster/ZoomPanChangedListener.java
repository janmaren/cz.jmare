package cz.jmare.pureraster;

public interface ZoomPanChangedListener {
    void onZoomPanChanged(ZoomPan zoomPan);
}
