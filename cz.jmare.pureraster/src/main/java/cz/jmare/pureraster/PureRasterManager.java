package cz.jmare.pureraster;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class PureRasterManager extends Composite {
    private double[] zooms = {0.0625, 0.09375, 0.125, 0.1875, 0.25, 0.375, 0.5, 0.75, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32};
    private Canvas canvas;
    private Image image;

    /**
     * X offset where to start to draw the image
     */
    private int panX;

    /**
     * Y offset where to start to draw the image
     */
    private int panY;

    /**
     * Zoom level. For zoom 2 the 2 pixels will be used to draw one picture pixel
     */
    private double zoom = 1;

    /**
     * Last canvas X position
     */
    protected int screenX;

    /**
     * Last canvas Y position
     */
    protected int screenY;

    protected volatile Image dragImage;
    protected boolean dragButtonDown;
    protected boolean dragging;
    /**
     * Canvas X position where started to drag
     */
    protected int startDragX = 0;
    /**
     * Canvas Y position where started to drag
     */
    protected int startDragY = 0;

    private Cursor movingCursor;

    private Cursor standardCursor;
    
    private Cursor currentCursor;
    
    private RasterCursorMoveListener rasterCursorMoveListener;
    
    private DrawObjectsListener drawObjectsListener;

    private ZoomPanChangedListener zoomPanChangedListener;

    private boolean zoomPanOccured;

    /**
     * Position to which to go after we know the new dimensions of canvas
     */
    private PositionData scheduleCenterPosition;

    private Image imageBuffer;

    private RasterToolListener rasterToolListener;

    private PureKeyPressedListener pureKeyPressedListener;

    /**
     * 1 usually left, 2 middle, 3 usually right
     */
    private int dragButton = 1;

    private int dragStateMask = 0;

    public PureRasterManager(Composite parent, int style) {
        super(parent, style);
        movingCursor = new Cursor(parent.getDisplay(), SWT.CURSOR_SIZEALL);
        standardCursor = new Cursor(parent.getDisplay(), SWT.CURSOR_ARROW);

        setLayout(new FillLayout());
        canvas = new Canvas(this, SWT.NO_BACKGROUND);
        canvas.addPaintListener(e -> paintCanvas(e));
        canvas.addKeyListener(KeyListener.keyPressedAdapter(e -> {
            if (pureKeyPressedListener != null) {
                pureKeyPressedListener.onRasterKeyPressed(e.keyCode, e.stateMask);
            }
        }));
        canvas.addMouseListener(new MouseListener() {
            @Override
            public void mouseUp(MouseEvent mouseEvent) {
                canvas.setCursor(standardCursor);
                currentCursor = standardCursor;
                if (mouseEvent.button == dragButton) {
                    dragButtonDown = false;
                }
                if (dragging && mouseEvent.button == dragButton) {
                    dragImage = null;
                    int deltaX = mouseEvent.x - startDragX;
                    int deltaY = mouseEvent.y - startDragY;
                    panX += deltaX;
                    panY += deltaY;
                    dragging = false;
                    zoomPanOccured = true;
                    refresh();
                } else {
                    if (image != null && rasterToolListener != null) {
                        if (rasterToolListener.mouseClickEvent(getPositionData(), mouseEvent.button,
                                mouseEvent.stateMask, true)) {
                            canvas.redraw();
                        }
                    }
                }
            }

            @Override
            public void mouseDown(MouseEvent mouseEvent) {
                canvas.setFocus();
                if (image != null && rasterToolListener != null) {
                    if (rasterToolListener.mouseClickEvent(getPositionData(), mouseEvent.button,
                            mouseEvent.stateMask, false)) {
                        canvas.redraw();
                    }
                }
                if (mouseEvent.button == dragButton && (mouseEvent.stateMask & dragStateMask) == dragStateMask) {
                    startDragX = mouseEvent.x;
                    startDragY = mouseEvent.y;
                    dragButtonDown = true;
                    
                    final Display display = Display.getDefault();
                    
                    GC gc = new GC(canvas);
                    dragImage = new Image(display, canvas.getSize().x, canvas.getSize().y);
                    gc.copyArea(dragImage, 0, 0);
                    gc.dispose();
                }
            }

            @Override
            public void mouseDoubleClick(MouseEvent mouseEvent) {
            }
        });
        
        canvas.addMouseMoveListener(mouseEvent -> {
            screenX = mouseEvent.x;
            screenY = mouseEvent.y;

            if (rasterCursorMoveListener != null) {
                PositionData positionData = getPositionData();
                if (image == null || positionData.imageX < 0 || positionData.imageY < 0 || positionData.imageX >= image.getImageData().width || positionData.imageY >= image.getImageData().height) {
                    positionData.imageX = null;
                    positionData.imageY = null;
                }
                rasterCursorMoveListener.changedTo(positionData, mouseEvent);
            }

            if (dragButtonDown) {
                dragging = true;
            }

            if (dragging) {
//                Image preparedImage = new Image(Display.getDefault(), canvas.getSize().x, canvas.getSize().y);
//
//                GC gcToPrepared = new GC(preparedImage);
//                gcToPrepared.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
//                Rectangle rect = canvas.getClientArea();
//                gcToPrepared.fillRectangle(rect.x, rect.y, rect.width, rect.height);
//                gcToPrepared.drawImage(dragImage, mouseEvent.x - startDragX, mouseEvent.y - startDragY);
//                gcToPrepared.dispose();
//
//                GC gc = new GC(canvas);
//                gc.drawImage(preparedImage, 0, 0);
//                gc.dispose();

                if (currentCursor != movingCursor) {
                    canvas.setCursor(movingCursor);
                    currentCursor = movingCursor;
                }
                refresh();
                return;
            }

            if (rasterToolListener != null && !dragging) {
                canvas.redraw();
            }
        });
        
        canvas.addMouseWheelListener(event -> {
            int wheelCount = event.count;
            Rectangle clientArea = canvas.getClientArea();
            double centerX = toImageX(clientArea.width / 2);
            double centerY = toImageY(clientArea.height / 2);

            if (wheelCount > 0) {
                zoomIn();
            }

            if (wheelCount < 0) {
                zoomOut();
            }

            centerToPositionInternal(centerX, centerY);

            if (rasterCursorMoveListener != null) {
                PositionData positionData = getPositionData();
                if (image == null || positionData.imageX < 0 || positionData.imageY < 0 || positionData.imageX >= image.getImageData().width || positionData.imageY >= image.getImageData().height) {
                    positionData.imageX = null;
                    positionData.imageY = null;
                }
                rasterCursorMoveListener.changedTo(positionData, event);
            }

            refresh();
        });

        addListener(SWT.Resize, e -> scheduleCenter());
    }

    protected void paintCanvas(PaintEvent e) {
        if (scheduleCenterPosition != null) {
            try {
                centerToPositionInternal(scheduleCenterPosition.imageX, scheduleCenterPosition.imageY);
            } finally {
                scheduleCenterPosition = null;
            }
        }

        int posX = panX;
        int posY = panY;
        if (dragging) {
            posX += screenX - startDragX;
            posY += screenY - startDragY;
        }

        if (imageBuffer != null) {
            ImageData imageData = imageBuffer.getImageData();
            if (imageData.width != canvas.getSize().x || imageData.height != canvas.getSize().y) {
                imageBuffer = null;
            }
        }
        if (imageBuffer == null) {
            imageBuffer = new Image(canvas.getDisplay(), canvas.getSize().x, canvas.getSize().y);

            GC gc = new GC(imageBuffer);

            drawBackground(gc, e.width, e.height);
            if (image == null) {
                gc.dispose();
                e.gc.drawImage(imageBuffer, 0, 0);
                return;
            }


            gc.setInterpolation(SWT.NONE);
            Transform transform = new Transform(getDisplay());
            transform.translate(posX, posY);
            transform.scale((float) zoom, (float) zoom);
            gc.setTransform(transform);
            gc.drawImage(image, 0, 0);
            gc.setTransform(null);
            transform.dispose();

            drawImageBorder(gc);

            if (drawObjectsListener != null) {
                drawObjectsListener.drawObjects(gc, getDrawScreenInfo());
            }

            gc.dispose();
        }

        if (zoomPanOccured && zoomPanChangedListener != null) {
            zoomPanChangedListener.onZoomPanChanged(getZoomPan());
            zoomPanOccured = false;
        }

        e.gc.drawImage(imageBuffer, 0, 0);

        if (rasterToolListener != null) {
            rasterToolListener.drawEvent(e.gc, getDrawScreenInfo(), getPositionData());
        }
    }

    public ZoomPan getZoomPan() {
        Rectangle clientArea = canvas.getClientArea();
        return new ZoomPan(zoom, new ImageCoordinate(toImageX(clientArea.width / 2),
                toImageY(clientArea.height / 2)));
    }

    private void drawImageBorder(GC gc) {
        DrawScreenInfo drawScreenInfo = getDrawScreenInfo();
        int screenX1 = drawScreenInfo.toScreenX(0);
        int screenY1 = drawScreenInfo.toScreenY(0);
        int screenX2 = drawScreenInfo.toScreenX(image.getImageData().width);
        int screenY2 = drawScreenInfo.toScreenY(image.getImageData().height);
        Color origForeground = gc.getForeground();
        gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
        gc.drawRectangle(screenX1 - 1, screenY1 - 1, screenX2 - screenX1 + 1, screenY2 - screenY1 + 1);
        gc.setForeground(origForeground);
    }

    private int findNearestZoomIndex() {
        int minIndex = -1;
        double minDiff = Double.MAX_VALUE;
        for (int i = 0; i < zooms.length; i++) {
            double diff = Math.abs(zooms[i] - zoom);
            if (diff < minDiff) {
                minIndex = i;
                minDiff = diff;
            }
        }
        return minIndex;
    }

    private boolean zoomIn() {
        if (image != null) {
            int index = findNearestZoomIndex();
            if (index >= zooms.length - 1) {
                return false;
            }
            zoom = zooms[index + 1];
            zoomPanOccured = true;
        }
        return true;
    }
    
    private boolean zoomOut() {
        if (image != null) {
            int index = findNearestZoomIndex();
            if (index == 0) {
                return false;
            }
            zoom = zooms[index - 1];
            zoomPanOccured = true;
        }
        return true;
    }
    
    private void zoomToFit() {
        if (image != null) {
            ImageData imageData = image.getImageData();
            zoom = zooms[0];
            while (true) {
                if (!zoomIn()) {
                    return;
                }
                if (zoom * imageData.width > canvas.getSize().x || zoom * imageData.height > canvas.getSize().y) {
                    zoomOut();
                    return;
                }
            }
        }
    }

    public void fit() {
        zoomToFit();
        center();
    }

    public static double log(double value, double base) {
        return Math.log(value) / Math.log(base);
    }
    
    private void drawBackground(GC gc, int screenWidth, int screenHeight) {
        gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
        gc.fillRectangle(new Rectangle(0, 0, screenWidth, screenHeight));
    }

    public void setImageAndCenterFitRefresh(Image image) {
        setImage(image);
        zoomToFit();
        center();
        canvas.redraw();
    }
    
    /**
     * Setting image (like new layer) but not setting pan and zoom
     * @param image swt image to set
     */
    public void setImageAndRefresh(Image image) {
        setImage(image);
        canvas.redraw();
    }

    public void setImage(Image image) {
        this.imageBuffer = null;
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    @Override
    public void dispose() {
        movingCursor.dispose();
        standardCursor.dispose();
        super.dispose();
    }
    
    private PositionData getPositionData() {
        PositionData positionData = new PositionData();
        if (image != null) {
            positionData.imageX = toImageX(screenX);
            positionData.imageY = toImageY(screenY);
            positionData.imageWidth = image.getImageData().width;
            positionData.imageHeight = image.getImageData().height;
        }
        positionData.zoom = zoom;
        return positionData;
    }
    
    private DrawScreenInfo getDrawScreenInfo() {
        int posX = panX;
        int posY = panY;
        if (dragging) {
            posX += (screenX - startDragX);
            posY += (screenY - startDragY);
        }
        DrawScreenInfo drawScreenInfo = new DrawScreenInfo(zoom, posX, posY, canvas.getBounds().width, canvas.getBounds().height);
        return drawScreenInfo;
    }
    
    public void setRasterCursorMoveListener(RasterCursorMoveListener rasterCursorMoveListener) {
        this.rasterCursorMoveListener = rasterCursorMoveListener;
    }

    public void setDrawObjectsListener(DrawObjectsListener drawObjectsListener) {
        this.drawObjectsListener = drawObjectsListener;
    }
    
    public void refresh() {
        imageBuffer = null;
        canvas.redraw();
    }
    
    public double toImageX(int screenX) {
        int posX = panX;
        if (dragging) {
            posX += (screenX - startDragX);
        }
        double imageX = screenX / zoom - posX / zoom;
        return imageX;
    }
    
    public double toImageY(int screenY) {
        int posY = panY;
        if (dragging) {
            posY += (screenY - startDragY);
        }
        double imageY = screenY / zoom - posY / zoom;
        return imageY;
    }
    
    private void centerToPositionInternal(double imageX, double imageY) {
        int cx = (int) (imageX * zoom);
        int cy = (int) (imageY * zoom);
        Rectangle clientArea = canvas.getClientArea();
        panX = (clientArea.width / 2) - cx;
        panY = (clientArea.height / 2) - cy;
    }

    public void centerToPosition(double imageX, double imageY) {
        centerToPositionInternal(imageX, imageY);
        zoomPanOccured = true;
    }

    /**
     * Image center to the midle of the screen
     */
    public void center() {
        if (image != null) {
            centerToPositionInternal(image.getImageData().width / 2.0, image.getImageData().height / 2.0);
        }
    }
    
    public void scheduleCenter() {
        scheduleCenterPosition = getPositionData();
        Rectangle clientArea = canvas.getClientArea();
        scheduleCenterPosition.imageX = toImageX((clientArea.width / 2));
        scheduleCenterPosition.imageY = toImageY((clientArea.height / 2));
    }

    public Rectangle getCanvasDim() {
        return new Rectangle(0, 0, canvas.getBounds().width, canvas.getBounds().height);
    }

    public double getZoom() {
        return zoom;
    }

    /**
     * Set cursor. Using middle of image as location where the cursor points
     * @param imageData image with cursor. It should have set tthe transaparentPixel otherwise the black color is set as transparent
     */
    public void setCursor(ImageData imageData) {
        if (imageData.transparentPixel == -1) {
            imageData.transparentPixel = 0;
        }
        Cursor crossCursor = new Cursor(getShell().getDisplay(), imageData, imageData.width / 2, imageData.height / 2);
        canvas.setCursor(crossCursor);
    }

    public void setZoomPanChangedListener(ZoomPanChangedListener zoomPanChangedListener) {
        this.zoomPanChangedListener = zoomPanChangedListener;
    }

    public void setZoomPanAndRefresh(ZoomPan zoomPan) {
        this.zoom = zoomPan.getZoom();
        centerToPositionInternal(zoomPan.getPan().x, zoomPan.getPan().y);
        refresh();
    }

    public void setPanAndRefresh(ImageCoordinate pan) {
        centerToPositionInternal(pan.x, pan.y);
        refresh();
    }

    public void setZooms(double[] zooms) {
        this.zooms = zooms;
    }

    public RGB getColor(ImageCoordinate imageCoordinate) {
        if (image == null) {
            return null;
        }
        ImageData imageData = image.getImageData();
        int pixel = imageData.getPixel((int) imageCoordinate.x, (int) imageCoordinate.y);
        return imageData.palette.getRGB(pixel);
    }

    public void setRasterToolListener(RasterToolListener rasterToolListener) {
        this.rasterToolListener = rasterToolListener;
    }

    public void setDragButton(int dragButton) {
        this.dragButton = dragButton;
    }

    public void setDragStateMask(int dragStateMask) {
        this.dragStateMask = dragStateMask;
    }

    public void setPureKeyPressedListener(PureKeyPressedListener pureKeyPressedListener) {
        this.pureKeyPressedListener = pureKeyPressedListener;
    }
}
