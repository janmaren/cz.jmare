package cz.jmare.pureraster;

public interface PureKeyPressedListener {
    void onRasterKeyPressed(int keyCode, int stateMask);
}
