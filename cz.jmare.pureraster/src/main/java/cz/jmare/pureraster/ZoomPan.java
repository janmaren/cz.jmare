package cz.jmare.pureraster;

public class ZoomPan {
    private double zoom;

    private ImageCoordinate pan;

    public ZoomPan(double zoom, ImageCoordinate pan) {
        this.zoom = zoom;
        this.pan = pan;
    }

    public double getZoom() {
        return zoom;
    }

    public ImageCoordinate getPan() {
        return pan;
    }

    @Override
    public String toString() {
        return "ZoomPan{" +
                "zoom=" + zoom +
                ", pan=" + pan +
                '}';
    }
}
