package cz.jmare.pureraster;

import org.eclipse.swt.graphics.GC;

public interface RasterToolListener {
    /**
     * Mouse up or down event. Note: with mouse up method needn't be called when dragging occured.
     * @param button 1 for the first button (usually 'left'), 2 for the second button (usually 'middle'), 3 for the third button (usually 'right')
     * @param up button when true then up position occured, otherwise down
     * @return true when need quick redraw, false when point not used or forced full redraw by itself
     */
    boolean mouseClickEvent(PositionData positionData, int button, int stateMask, boolean up);

    void drawEvent(GC gc, DrawScreenInfo drawScreenInfo, PositionData positionData);
}
