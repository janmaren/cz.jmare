package cz.jmare.pureraster;

import org.eclipse.swt.graphics.GC;

public interface DrawObjectsListener {
    void drawObjects(GC gc, DrawScreenInfo drawScreenInfo);
}
