package cz.jmare.pureraster;

public class DrawScreenInfo {
    private double zoom;
    private int panX;
    private int panY;
    public int width;
    public int height;
    public DrawScreenInfo(double zoom, int panX, int panY, int width, int height) {
        super();
        this.zoom = zoom;
        this.panX = panX;
        this.panY = panY;
        this.width = width;
        this.height = height;
    }

    public int toScreenX(double imageX) {
        return (int) (imageX * zoom + panX);
    }
    
    public int toScreenY(double imageY) {
        return (int) (imageY * zoom + panY);
    }
    
    public int distRounded(double value) {
        return (int) Math.round(value * zoom);
    }

    public double getZoom() {
        return zoom;
    }
}
