package cz.jmare.pureraster;

public class PositionData {
    public Double imageX;
    
    public Double imageY;
    
    public double zoom;
    
    public Integer imageWidth;
    
    public Integer imageHeight;

    @Override
    public String toString() {
        return "PositionData{" +
                "imageX=" + imageX +
                ", imageY=" + imageY +
                ", zoom=" + zoom +
                '}';
    }
}
