package cz.jmare.pureraster;

import org.eclipse.swt.graphics.GC;

public interface CursorDrawListener {
    void draw(GC gc, DrawScreenInfo drawScreenInfo, double x, double y);
}
