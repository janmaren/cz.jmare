package cz.jmare.xml;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// Example:
//    Document document = XmlModelUtil.getDocument(filename);
//    JobSchedulingData jobSchedulingData = XmlModelUtil.getOne(document, "job-scheduling-data", new Function<Element, JobSchedulingData>(){
//      @Override
//      public JobSchedulingData apply(Element t) {
//          JobSchedulingData jobSchedulingData = new JobSchedulingData();
//          jobSchedulingData.setScheduleList(XmlModelUtil.getList(t, "schedule", new SchedulerMapper()));
//          return jobSchedulingData;
//      }});
//
// with XPath:
//   XPathExpression expr = xpath.compile("/job-scheduling-data/schedule");
//   NodeList nodeList = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
//   Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
//   Job job = XmlModelUtil.getOne(node, "job", new Function<Element, Job>()...
public class XmlModelUtil {
    /**
     * Parse to DOM
     * @param filename
     * @return
     */
    public static Document getDocument(String filename) {
        FileInputStream is = null;
        try {
            DocumentBuilder b = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            is = new FileInputStream(filename);
            return b.parse(is);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // no handling
                }
            }
        }
    }

    /**
     * Return list of model objects
     * @param parentElement
     * @param childElementName
     * @param mapper
     * @return
     */
    public static <T> List<T> getList(Node parentElement, String childElementName, Function<Element, T> mapper) {
        ArrayList<T> resultList = new ArrayList<T>();
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element childElement = (Element) node;
                if (childElementName.equals(childElement.getNodeName())) {
                    T populatedObject = mapper.apply(childElement);
                    resultList.add(populatedObject);
                }
            }
        }

        return resultList;
    }

    /**
     * Return just one object of given name or null when not found
     * @param parentElement
     * @param childElementName
     * @param mapper
     * @return
     */
    public static <T> T getOne(Node parentElement, String childElementName, Function<Element, T> mapper) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element childElement = (Element) node;
                if (childElementName.equals(childElement.getNodeName())) {
                    return mapper.apply(childElement);
                }
            }
        }

        return null;
    }

    /**
     * Return just one object of given name or null when not found
     * @param parentElement
     * @param childElementName
     * @param mapper
     * @throws RuntimeException when multiple such elements exist
     * @return
     */
    public static <T> T getOneStrict(Node parentElement, String childElementName, Function<Element, T> mapper) {
        T populatedObject = null;
        boolean found = false;
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element childElement = (Element) node;
                if (childElementName.equals(childElement.getNodeName())) {
                    if (found) {
                        throw new RuntimeException("Expected one but there is multiple occurence of " + childElementName + " under " + parentElement);
                    } else {
                        populatedObject = mapper.apply(childElement);
                        found = true;
                    }
                }
            }
        }

        return populatedObject;
    }

    public static <T> T getValue(Node parentElement, String childElementName, Function<String, T> retype) {
        String value = getValue(parentElement, childElementName);
        if (value == null) {
            return null;
        }
        return retype.apply(value);
    }

    public static String getValue(Node parentElement, String childElementName) {
        return getOne(parentElement, childElementName, new Function<Element, String>(){
            @Override
            public String apply(Element t) {
                return t.getTextContent();
            }});

    }

    public static <T> T getAttribute(Element parentElement, String childElementName, Function<String, T> mapper) {
        String attribute = getAttribute(parentElement, childElementName);
        if (attribute == null) {
            return null;
        }
        T populatedObject = mapper.apply(attribute);
        return populatedObject;
    }

    public static String getAttribute(Element parentElement, String childElementName) {
        return parentElement.getAttribute(childElementName);
    }

    public static final class StringToInteger implements Function<String, Integer> {
        @Override
        public Integer apply(String t) {
            return Integer.valueOf(t);
        }
    }

//    public static final class StringToDate implements Function<String, Date> {
//        @Override
//        public Date apply(String t) {
//            return DatatypeConverter.parseDate(t).getTime();
//        }
//    }
//
//    public static final class StringToDateTime implements Function<String, Date> {
//        @Override
//        public Date apply(String t) {
//            return DatatypeConverter.parseDateTime(t).getTime();
//        }
//    }
}
