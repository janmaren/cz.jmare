package cz.jmare.xml;

import java.util.List;

import cz.jmare.xml.XmlOpeningTagParser.XmlOpeningTagResult;

public class XmlOpeningTagGenerator {
    private int indent;

    private int indentAttr;

    private String openingTagPart;

    private String lineSeparator;

    private boolean firstOfMultipleOnNewLine;

    public XmlOpeningTagGenerator(String openingTagPart, int indent, int indentAttr, String lineSeparator) {
        super();
        this.openingTagPart = openingTagPart;
        this.indent = indent;
        this.indentAttr = indentAttr;
        this.lineSeparator = lineSeparator;
    };

    public String getOpeningTag() {
        XmlOpeningTagParser xmlOpeningTagParser = new XmlOpeningTagParser(openingTagPart);
        XmlOpeningTagResult xmlOpeningTagResult = xmlOpeningTagParser.getXmlOpeningTagResult();
        if (xmlOpeningTagResult == null) {
            return openingTagPart;
        }
        StringBuilder sb = new StringBuilder(xmlOpeningTagResult.getTagName());
        List<String> attributes = xmlOpeningTagResult.getAttributes();
        if (attributes.size() == 1) {
            sb.append(" ").append(attributes.get(0));
        } else if (attributes.size() > 1) {
            int attrIndent = 0;
            if (indentAttr >= 0) {
                attrIndent = indent + indentAttr;
            } else {
                attrIndent = indent + xmlOpeningTagResult.getTagName().length() + 1;
            }
            String spacesIndent = getSpacesIndent(attrIndent);
            for (int i = 0; i < attributes.size(); i++) {
                String attribute = attributes.get(i);
                if (!firstOfMultipleOnNewLine && i == 0) {
                    sb.append(" ");
                } else {
                    sb.append(lineSeparator);
                    sb.append(spacesIndent);
                }
                sb.append(attribute);
            }
        }

        if (xmlOpeningTagResult.isSelfclosing()) {
            sb.append("/");
        }
        sb.append(">");
        return sb.toString();
    }

    private static final String getSpacesIndent(int size) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public void setFirstOfMultipleOnNewLine(boolean firstOfMultipleOnNewLine) {
        this.firstOfMultipleOnNewLine = firstOfMultipleOnNewLine;
    }

    public static void main(String[] args) {
        XmlOpeningTagGenerator xmlOpeningTagParser = new XmlOpeningTagGenerator("<a8c:aaa\r\nb=\n\"c\"  x=\"nazdarek\"    >", 10, -1, "\n");
        String xmlOpeningTagResult = xmlOpeningTagParser.getOpeningTag();
        System.out.println("          " + xmlOpeningTagResult);
    }
}
