package cz.jmare.xselector;

import java.util.List;

import org.w3c.dom.Element;

public interface ElementVector<R> {
    List<R> apply(Element element, ElementContext contextMap);
}
