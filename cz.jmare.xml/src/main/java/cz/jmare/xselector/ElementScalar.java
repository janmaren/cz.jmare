package cz.jmare.xselector;

import org.w3c.dom.Element;

public interface ElementScalar<R> {
    R apply(Element element, ElementContext contextMap);
}
