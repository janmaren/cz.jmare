package cz.jmare.xselector;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class XmlLoaderUtil {
    public static Document getDocumentByInputStream(InputStream inputStream) {
        try {
            DocumentBuilder b = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return b.parse(inputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                // no handling
            }
        }
    }

    public static Document getDocumentByFilename(String filename) {
        FileInputStream inputStream;
        try {
            inputStream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to open " + filename, e);
        }
        return getDocumentByInputStream(inputStream);
    }

    public static Document getDocumentByString(String str) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(str));

            Document doc = db.parse(is);
            return doc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
