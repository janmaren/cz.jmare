package cz.jmare.xselector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XSelector {
    private static final String DEFAULT_NAMESPACE_PREFIX = "xmlns";

    private static Map<String, String> getNamespaces(Element element) {
        Map<String, String> attrMap = new HashMap<>();
        NamedNodeMap attributes = element.getAttributes();
        int length = attributes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = attributes.item(i);
            if (item instanceof Attr) {
                Attr attr = (Attr) item;
                String name = attr.getName();
                if (name.startsWith(DEFAULT_NAMESPACE_PREFIX)) {
                    if (name.equals(DEFAULT_NAMESPACE_PREFIX)) {
                        attrMap.put("", attr.getValue());
                    } else {
                        if (name.charAt(DEFAULT_NAMESPACE_PREFIX.length()) == ':') {
                            String substring = name.substring(DEFAULT_NAMESPACE_PREFIX.length() + 1);
                            attrMap.put(substring, attr.getValue());
                        }
                    }
                }
            }
        }
        return attrMap;
    }

    private static boolean matches(Element element, String reqNamespace, String reqName, ElementContext contextMap) {
        String name = element.getNodeName();
        int indexColon = name.indexOf(":");
        String prefix = null;
        if (indexColon != -1) {
            prefix = name.substring(0, indexColon);
            name = name.substring(indexColon + 1);
        }
        if ("".equals(reqNamespace)) {
            if (prefix == null) {
                String emptyNamespaceValue = contextMap.getNamespace("");
                return name.equals(reqName) && "".equals(emptyNamespaceValue == null ? "" : emptyNamespaceValue);
            } else {
                return false;
            }
        } else {
            if ("*".equals(reqNamespace)) {
                return name.equals(reqName);
            }
            if (prefix == null) {
                String emptyNamespaceValue = contextMap.getNamespace("");
                return name.equals(reqName) && reqNamespace.equals(emptyNamespaceValue == null ? "" : emptyNamespaceValue);
            } else {
                return name.equals(reqName) && reqNamespace.equals(contextMap.getNamespace(prefix));
            }
        }
    }

    private static <T> void traverseAll(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementBiconsumer elementBiconsumer) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    elementBiconsumer.accept(element, subContextMap);
                }
            }
        }
    }

    public static <T> T firstMatchHasOneNode(Node parentElement, String reqNamespace, String reqName, final ElementContext contextMap,
            final ElementScalar<T> mapper) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    T populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject != null) {
                        return populatedObject;
                    }
                }
            }
        }

        return null;
    }

    public static <T> List<T> firstMatchHasManyNodes(Node parentElement, String reqNamespace, String reqName, final ElementContext contextMap,
            final ElementVector<T> mapper) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    List<T> populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject.size() > 0) {
                        return populatedObject;
                    }
                }
            }
        }

        return new ArrayList<T>();
    }

    public static <T> List<T> firstMatchHasOneNodeAsList(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementScalar<T> mapper) {
        ArrayList<T> arrayList = new ArrayList<T>();
        T first = firstMatchHasOneNode(parentElement, reqNamespace, reqName, contextMap, mapper);
        if (first != null) {
            arrayList.add(first);
        }
        return arrayList;
    }

    public static <T> T firstMatchHasOneNodeSkipRoot(Document document, String reqNamespace, String reqName, ElementScalar<T> mapper) {
        Node firstChild = document.getFirstChild();
        if (firstChild == null || !(firstChild instanceof Element)) {
            return null;
        }
        Element element = (Element) firstChild;
        ElementContext elementContext = new ElementContext(getNamespaces(element));

        return firstMatchHasOneNode(element, reqNamespace, reqName, elementContext, mapper);
    }

    public static <T> List<T> firstMatchHasManyNodesSkipRoot(Document document, String reqNamespace, String reqName, ElementVector<T> mapper) {
        Node firstChild = document.getFirstChild();
        if (firstChild == null || !(firstChild instanceof Element)) {
            return null;
        }
        Element element = (Element) firstChild;
        ElementContext elementContext = new ElementContext(getNamespaces(element));

        return firstMatchHasManyNodes(element, reqNamespace, reqName, elementContext, mapper);
    }

    public static <T> List<T> eachMatchHasOneNode(Node parentElement, String reqNamespace, String reqName, final ElementContext contextMap,
            final ElementScalar<T> mapper) {
        final ArrayList<T> resultList = new ArrayList<T>();
        traverseAll(parentElement, reqNamespace, reqName, contextMap, new ElementBiconsumer() {
            @Override
            public void accept(Element element, ElementContext subContextMap) {
                T populatedObject = mapper.apply(element, subContextMap);
                resultList.add(populatedObject);
            }
        });

        return resultList;
    }

    public static <T> List<T> eachMatchHasManyNodes(Node parentElement, String reqNamespace, String reqName, final ElementContext contextMap,
            final ElementVector<T> mapper) {
        final ArrayList<T> resultList = new ArrayList<T>();
        traverseAll(parentElement, reqNamespace, reqName, contextMap, new ElementBiconsumer() {
            @Override
            public void accept(Element element, ElementContext subContextMap) {
                List<T> populatedObject = mapper.apply(element, subContextMap);
                resultList.addAll(populatedObject);
            }
        });
        return resultList;
    }

    public static <T> T firstDeepMatchHasOneNode(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementScalar<T> mapper) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    T populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject != null) {
                        return populatedObject;
                    }
                }
                T firstDeep = firstDeepMatchHasOneNode(element, reqNamespace, reqName, subContextMap, mapper);
                if (firstDeep != null) {
                    return firstDeep;
                }
            }
        }

        return null;
    }

    public static <T> List<T> firstDeepMatchHasManyNodes(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementVector<T> mapper) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    List<T> populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject.size() > 0) {
                        return populatedObject;
                    }
                }
                List<T> subnodes = firstDeepMatchHasManyNodes(element, reqNamespace, reqName, subContextMap, mapper);
                if (subnodes.size() > 0) {
                    return subnodes;
                }
            }
        }

        return new ArrayList<T>();
    }

    public static <T> List<T> firstDeepMatchHasManyNodes(Document document, String reqNamespace, String reqName, ElementVector<T> mapper) {
        return firstDeepMatchHasManyNodes(document, reqNamespace, reqName, new ElementContext(), mapper);
    }

    public static <T> T firstDeepMatchHasOneNode(Document document, String reqNamespace, String reqName, ElementScalar<T> mapper) {
        return firstDeepMatchHasOneNode(document, reqNamespace, reqName, new ElementContext(), mapper);
    }

    public static <T> List<T> eachDeepMatchHasOneNode(Document parentElement, String reqNamespace, String reqName, ElementScalar<T> mapper) {
        return eachDeepMatchHasOneNode(parentElement, reqNamespace, reqName, new ElementContext(), mapper);
    }

    public static <T> List<T> eachDeepMatchHasManyNodes(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementVector<T> mapper) {
        ArrayList<T> resultList = new ArrayList<T>();
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    List<T> populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject != null) {
                        resultList.addAll(populatedObject);
                    }
                }
                List<T> allDeep = eachDeepMatchHasManyNodes(element, reqNamespace, reqName, subContextMap, mapper);
                resultList.addAll(allDeep);
            }
        }

        return resultList;
    }

    public static <T> List<T> eachDeepMatchHasOneNode(Node parentElement, String reqNamespace, String reqName, ElementContext contextMap,
            ElementScalar<T> mapper) {
        ArrayList<T> resultList = new ArrayList<T>();
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Map<String, String> namespaces = getNamespaces(element);
                ElementContext subContextMap = new ElementContext(contextMap);
                subContextMap.putAllNamespaces(namespaces);

                if (matches(element, reqNamespace, reqName, subContextMap)) {
                    T populatedObject = mapper.apply(element, subContextMap);
                    if (populatedObject != null) {
                        resultList.add(populatedObject);
                    }
                }
                List<T> allDeep = eachDeepMatchHasOneNode(element, reqNamespace, reqName, subContextMap, mapper);
                resultList.addAll(allDeep);
            }
        }

        return resultList;
    }

    public static <T> List<T> eachDeepMatchHasManyNodes(Document document, String reqNamespace, String reqName, ElementVector<T> mapper) {
        return eachDeepMatchHasManyNodes(document, reqNamespace, reqName, new ElementContext(), mapper);
    }

    public static <T> List<T> eachMatchHasManyNodesSkipRoot(Document document, String reqNamespace, String reqName, ElementVector<T> mapper) {
        Node firstChild = document.getFirstChild();
        if (firstChild == null || !(firstChild instanceof Element)) {
            return null;
        }
        Element element = (Element) firstChild;
        ElementContext elementContext = new ElementContext(getNamespaces(element));

        return eachMatchHasManyNodes(element, reqNamespace, reqName, elementContext, mapper);
    }

    public static <T> List<T> eachMatchHasManyNodes(Document document, String reqNamespace, String reqName, ElementVector<T> mapper) {
        return eachMatchHasManyNodes(document, reqNamespace, reqName, new ElementContext(), mapper);
    }
}
