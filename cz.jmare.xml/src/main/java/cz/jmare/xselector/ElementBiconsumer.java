package cz.jmare.xselector;

import org.w3c.dom.Element;

public interface ElementBiconsumer {
    void accept(Element element, ElementContext contextMap);
}
