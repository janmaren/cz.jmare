package cz.jmare.xselector;

import java.util.HashMap;
import java.util.Map;

public class ElementContext {
    private Map<String, String> namespacesMap = new HashMap<>();

    public ElementContext() {
    }

    public ElementContext(ElementContext elementContext) {
        namespacesMap.putAll(elementContext.namespacesMap);
    }

    public ElementContext(Map<String, String> namespacesMap) {
        this.namespacesMap.putAll(namespacesMap);
    }

    public void putAllNamespaces(Map<String, String> namespacesMap) {
        this.namespacesMap.putAll(namespacesMap);
    }

    public String getNamespace(String namespacePrefix) {
        return namespacesMap.get(namespacePrefix);
    }
}
