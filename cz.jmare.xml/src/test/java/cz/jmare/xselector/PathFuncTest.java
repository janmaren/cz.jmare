package cz.jmare.xselector;

import static cz.jmare.xselector.XSelector.eachDeepMatchHasOneNode;
import static cz.jmare.xselector.XSelector.eachMatchHasManyNodesSkipRoot;
import static cz.jmare.xselector.XSelector.eachMatchHasOneNode;
import static cz.jmare.xselector.XSelector.firstMatchHasOneNodeAsList;
import static cz.jmare.xselector.XSelector.firstMatchHasManyNodesSkipRoot;
import static cz.jmare.xselector.XSelector.firstDeepMatchHasOneNode;
import static cz.jmare.xselector.XSelector.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PathFuncTest {

    @Test
    public void testFindSinglePath() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/addresses.xml"));

        List<String> all = eachMatchHasManyNodesSkipRoot(document, "*", "author", new ElementVector<String>() {
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                // element = author
                return eachMatchHasOneNode(element, "*", "address", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }
        });

//        List<String> allElements = eachMatchHasManyNodesSkipRoot(document, "*", "author", (e, c) -> {
//            return eachMatchHasOneNode(e, "*", "address", c, (e1, c1) -> {return e1.getTextContent();});
//        });

        Assert.assertEquals("[praha, brno, ostrava, plzen]", all.toString());
    }

    @Test
    public void testAllDeep() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/addresses.xml"));

        List<String> allSkipRootDeep = eachDeepMatchHasOneNode(document, "*", "address", new ElementScalar<String>(){
            @Override
            public String apply(Element parentElement, ElementContext contextMap) {
                return parentElement.getTextContent();
            }});

        Assert.assertEquals("[praha, brno, ostrava, plzen]", allSkipRootDeep.toString());
    }

    @Test
    public void testGetAllGetFirstNamespaceMatch() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books-namespaces.xml"));
        List<String> all = eachMatchHasManyNodesSkipRoot(document, "*", "book", new ElementVector<String>() {
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                // element = book
                return firstMatchHasOneNodeAsList(element, "http://www.amazon.com/HarryPotter", "author", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }
        });

        Assert.assertEquals("[Some details about the author]", all.toString());
    }

    @Test
    public void testGetAllGetFirstNamespaceInheritMatch() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books-namespaces.xml"));
        List<String> all = eachMatchHasManyNodesSkipRoot(document, "http://www.amazon.com", "book", new ElementVector<String>() {
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                // element = book
                return firstMatchHasOneNodeAsList(element, "http://www.amazon.com", "author", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }
        });

        Assert.assertEquals("[Arkadij with brather autors of Stalker, adam nathans title]", all.toString());
    }

    @Test
    public void testGetAllGetFirstNamespaceNotMatchNamespace() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books-namespaces.xml"));
        List<String> all = eachMatchHasManyNodesSkipRoot(document, "*", "book", new ElementVector<String>() {
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                // element = book
                return firstMatchHasOneNodeAsList(element, "", "author", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }
        });

        Assert.assertEquals("[]", all.toString());
    }

    @Test
    public void testFirstDeepWithNamespaces() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books-namespaces.xml"));
        String firstDeepSkipRoot = firstDeepMatchHasOneNode(document, "http://www.amazon.com/HarryPotter", "author", new ElementScalar<String>(){

            @Override
            public String apply(Element element, ElementContext contextMap) {
                return element.getTextContent();
            }});

        Assert.assertEquals("Some details about the author", firstDeepSkipRoot);
    }

    @Test
    public void testAllDeepWithNamespaces() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books-namespaces.xml"));
        List<String> allSkipRootDeep = eachDeepMatchHasOneNode(document, "*", "author", new ElementScalar<String>(){
            @Override
            public String apply(Element parentElement, ElementContext contextMap) {
                return parentElement.getTextContent();
            }});

        Assert.assertEquals("[Arkadij with brather autors of Stalker, Boris with brather autors of Stalker, Some details about the author, adam nathans title]", allSkipRootDeep.toString());
    }

    @Test
    public void testGetAllGetFirstNamespaceNotMatch() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books.xml"));
        List<String> all = eachMatchHasManyNodesSkipRoot(document, "*", "book", new ElementVector<String>() {
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                // element = book
                return firstMatchHasOneNodeAsList(element, "http://www.amazon.com/HarryPotter", "author", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }
        });

        Assert.assertEquals("[]", all.toString());
    }

    @Test
    public void testFirstDeep() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books.xml"));
        String firstDeepSkipRoot = firstDeepMatchHasOneNode(document, "http://www.amazon.com/HarryPotter", "author", new ElementScalar<String>(){

            @Override
            public String apply(Element element, ElementContext contextMap) {
                return element.getTextContent();
            }});

        Assert.assertEquals(null, firstDeepSkipRoot);
    }

    @Test
    public void testAllDeepBooks() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books.xml"));
        List<String> allSkipRootDeep = eachDeepMatchHasOneNode(document, "*", "author", new ElementScalar<String>(){
            @Override
            public String apply(Element parentElement, ElementContext contextMap) {
                return parentElement.getTextContent();
            }});

        Assert.assertEquals("[Arkadij with brather autors of Stalker, Boris with brather autors of Stalker, Some details about the author, adam nathans title]", allSkipRootDeep.toString());
    }

    @Test
    public void testGetFirstGetAll() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books.xml"));
        List<String> firstSkipRoot = firstMatchHasManyNodesSkipRoot(document, "*", "book", new ElementVector<String>(){
            @Override
            public List<String> apply(Element element, ElementContext contextMap) {
                return eachMatchHasOneNode(element, "", "author", contextMap,
                        new ElementScalar<String>() {
                            @Override
                            public String apply(Element element, ElementContext arg1) {
                             // element = author
                                return element.getTextContent();
                            }
                        });
            }});

        Assert.assertEquals("[Arkadij with brather autors of Stalker, Boris with brather autors of Stalker]", firstSkipRoot.toString());
    }

    @Test
    public void testDeepAllAuthorsOfFirstBook() {
        Document document = XmlLoaderUtil.getDocumentByInputStream(PathFuncTest.class.getResourceAsStream("/xml/books.xml"));
        List<String> allSkipRootDeep = firstDeepMatchHasManyNodes(document, "", "book", new ElementVector<String>(){
            @Override
            public List<String> apply(Element parentElement, ElementContext contextMap) {
                return eachMatchHasOneNode(parentElement, "", "author", contextMap, new ElementScalar<String>() {
                    @Override
                    public String apply(Element element, ElementContext contextMap) {
                        return element.getTextContent();
                    }
                });
            }});

        Assert.assertEquals("[Arkadij with brather autors of Stalker, Boris with brather autors of Stalker]", allSkipRootDeep.toString());
    }
}
