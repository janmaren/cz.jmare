package cz.jmare.math.geo;

import java.util.Objects;

/**
 * Geographic coordinates where longitude is in range -180 (west) to +180 (east) degrees and latitude in range +90 (north pole) to -90 (south pole)
 */
public class GeoLoc {
    
    /**
     * latitude in range +90 (north pole) to -90 (south pole)
     */
    public double latitude;
    
    /**
     * longitude in range -180 (west) to +180 (east)
     */
    public double longitude;

    public GeoLoc(double latitude, double longitude) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "geoLoc(" + latitude + ", " + longitude + ")";
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GeoLoc other = (GeoLoc) obj;
        return Double.doubleToLongBits(latitude) == Double.doubleToLongBits(other.latitude)
                && Double.doubleToLongBits(longitude) == Double.doubleToLongBits(other.longitude);
    }
}
