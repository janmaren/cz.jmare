package cz.jmare.math.geometry.entity;

public class Xyz {
    public double x;

    public double y;
    
    public double z;

    public Xyz(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "xyz(" + x + ", " + y + ", " + z + ")";
    }
}
