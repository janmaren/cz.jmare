package cz.jmare.math.geometry.entity;

import java.util.ArrayList;
import java.util.List;

public class TwoPoints {
    public double x1;

    public double y1;

    public double x2;

    public double y2;

    public TwoPoints() {

    }

    public TwoPoints(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public TwoPoints(Point point1, Point point2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    public TwoPoints(double x1, double y1, Point point2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    public TwoPoints(Point point1, double x2, double y2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public String toString() {
        return "TwoPoints [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + "]";
    }

    public Point getPoint1() {
        return new Point(x1, y1);
    }

    public Point getPoint2() {
        return new Point(x2, y2);
    }

    public static List<TwoPoints> buildTwoPointsList(List<Point> points1, List<Point> points2) {
        if (points1.size() != points2.size()) {
            throw new IllegalArgumentException("Lists haven't the same sizes");
        }
        List<TwoPoints> result = new ArrayList<TwoPoints>();
        for (int i = 0; i < points1.size(); i++) {
            Point point1 = points1.get(i);
            Point point2 = points2.get(i);
            result.add(new TwoPoints(point1, point2));
        }

        return result;
    }

    public static List<Point> extractPoints1(List<TwoPoints> twoPoints) {
        List<Point> result = new ArrayList<Point>();
        for (TwoPoints twoPoint : twoPoints) {
            result.add(twoPoint.getPoint1());
        }
        return result;
    }

    public static List<Point> extractPoints2(List<TwoPoints> twoPoints) {
        List<Point> result = new ArrayList<Point>();
        for (TwoPoints twoPoint : twoPoints) {
            result.add(twoPoint.getPoint2());
        }
        return result;
    }
}
