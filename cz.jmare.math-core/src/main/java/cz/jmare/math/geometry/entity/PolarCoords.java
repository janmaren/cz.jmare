package cz.jmare.math.geometry.entity;

public class PolarCoords {
    public double andleDeg;

    public double dist;

    public PolarCoords(double andleDeg, double dist) {
        super();
        this.andleDeg = andleDeg;
        this.dist = dist;
    }

    @Override
    public String toString() {
        return "PolarCoords [andleDeg=" + andleDeg + ", dist=" + dist + "]";
    }

    @Override
    public PolarCoords clone() {
        PolarCoords polarCoords = new PolarCoords(andleDeg, dist);
        return polarCoords;
    }
}
