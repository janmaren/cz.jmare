package cz.jmare.math.geometry.entity;

public class StraightFloatLine {
    public float a;

    public float b;

    public float c;

    public StraightFloatLine(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public StraightFloatLine(float x1, float y1, float x2, float y2) {
        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    public StraightFloatLine(float k, float q) {
        this(1, 1 * k + q, 2, 2 * k + q);
    }

    public StraightFloatLine(LineSegmentFloat lineSegment) {
        this(lineSegment.x1, lineSegment.y1, lineSegment.x2, lineSegment.y2);
    }

    @Override
    public String toString() {
        return "StraightFloatLine [a=" + a + ", beta=" + b + ", c=" + c + "]";
    }

    public static StraightFloatLine createXParallel(float y) {
        return new StraightFloatLine(0, 1, -y);
    }

    public static StraightFloatLine createYParallel(float x) {
        return new StraightFloatLine(1, 0, -x);
    }

    public static StraightFloatLine createXAxe() {
        return createXParallel(0);
    }

    public static StraightFloatLine createYAxe() {
        return createYParallel(0);
    }

    public float yOnLine(float x) {
        return -(a * x + c) / b;
    }

    public float xOnLine(float y) {
        return -(b * y + c) / a;
    }
}
