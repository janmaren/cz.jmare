package cz.jmare.math.geometry.entity;

import java.util.ArrayList;
import java.util.List;

public class Point {
    public double x;

    public double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point [x=" + x + ", y=" + y + "]";
    }

    public static List<Point> ofList(double... xy) {
        if (xy.length % 2 != 0) {
            throw new IllegalArgumentException("Number of x-y arguments must be even");
        }
        ArrayList<Point> points = new ArrayList<Point>();
        for (int i = 0; i < xy.length; i += 2) {
            Point point = new Point(xy[i], xy[i + 1]);
            points.add(point);
        }
        return points;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point other = (Point) obj;
        if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
            return false;
        if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
            return false;
        return true;
    }
}
