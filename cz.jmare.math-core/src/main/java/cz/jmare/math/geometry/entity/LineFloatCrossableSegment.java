package cz.jmare.math.geometry.entity;

/**
 * Line segment with coordinates and precalculated coefs a, beta, c
 * Finding a crosspoint provided here
 */
public class LineFloatCrossableSegment {
    public float x1;

    public float y1;

    public float x2;

    public float y2;

    public float a;

    public float b;

    public float c;

    public LineFloatCrossableSegment(float x1, float y1, float x2, float y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    public LineFloatCrossableSegment(LineSegmentFloat segmentFloat) {
        this(segmentFloat.x1, segmentFloat.y1, segmentFloat.x2, segmentFloat.y2);
    }

    /**
     * Return crossection point or null when segments don't have conjunctive point
     * @param other
     * @return
     */
    public PointFloat crossection(LineFloatCrossableSegment other) {
        float j = other.a;
        float k = other.b;
        float l = other.c;

        float denominator = b * j - a * k;
        if (denominator == 0.0f) {
            return null;
        }
        float y = (float) ((a * l - c * j) / denominator);
        float x = (float) ((c * k - b * l) / denominator);

        if (!pointInSegment(this, x, y)) {
            return null;
        }
        if (!pointInSegment(other, x, y)) {
            return null;
        }

        return new PointFloat(x, y);
    }

    public PointFloat crossection(StraightFloatLine other) {
        double j = other.a;
        double k = other.b;
        double l = other.c;

        double denominator = b * j - a * k;
        if (denominator == 0.0f) {
            return null;
        }
        float y = (float) ((a * l - c * j) / denominator);
        float x = (float) ((c * k - b * l) / denominator);

        if (!pointInSegment(this, x, y)) {
            return null;
        }

        return new PointFloat(x, y);
    }

    private boolean pointInSegment(LineFloatCrossableSegment segment, float poX, float poY) {
//        if (Math.abs(segment.x1 - segment.x2) > Math.abs(segment.y1 - segment.y2)) {
//            return segment.x1 <= poX && poX <= segment.x2 || segment.x1 >= poX && poX >= segment.x2;
//        } else {
//            return segment.y1 <= poY && poY <= segment.y2 || segment.y1 >= poY && poY >= segment.y2;
//        }
        if (Math.abs(segment.x1 - segment.x2) > 0) {
            if (segment.x1 <= poX && poX <= segment.x2 || segment.x1 >= poX && poX >= segment.x2) {
                return true;
            }
        }
        // we try both variants because of rounding error
        if (Math.abs(segment.y1 - segment.y2) > 0) {
            if (segment.y1 <= poY && poY <= segment.y2 || segment.y1 >= poY && poY >= segment.y2) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "LineFloatCrossableSegment [" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + "]";
    }
}
