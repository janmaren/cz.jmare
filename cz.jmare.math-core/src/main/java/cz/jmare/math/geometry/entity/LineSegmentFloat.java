package cz.jmare.math.geometry.entity;

/**
 * Line segment with coordinates
 */
public class LineSegmentFloat {
    public float x1;

    public float y1;

    public float x2;

    public float y2;

    public LineSegmentFloat(float x1, float y1, float x2, float y2) {
        super();
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public String toString() {
        return "LineSegmentFloat [" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + "]";
    }
}
