package cz.jmare.math.geometry.entity;

/**
 * Line segment with coordinates and precalculated coefs a, beta, c
 * Finding a crosspoint provided here
 */
public class LineDoubleCrossableSegment {
    public double x1;

    public double y1;

    public double x2;

    public double y2;

    public double a;

    public double b;

    public double c;

    public LineDoubleCrossableSegment(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    public LineDoubleCrossableSegment(LineSegmentDouble segmentdouble) {
        this(segmentdouble.x1, segmentdouble.y1, segmentdouble.x2, segmentdouble.y2);
    }

    /**
     * Return crossection point or null when segments don't have conjunctive point
     * @param other
     * @return
     */
    public Point crossection(LineDoubleCrossableSegment other) {
        double j = other.a;
        double k = other.b;
        double l = other.c;

        double denominator = b * j - a * k;
        if (denominator == 0.0f) {
            return null;
        }
        double y = (double) ((a * l - c * j) / denominator);
        double x = (double) ((c * k - b * l) / denominator);

        if (!pointInSegment(this, x, y)) {
            return null;
        }
        if (!pointInSegment(other, x, y)) {
            return null;
        }

        return new Point(x, y);
    }

    public Point crossection(StraightLine other) {
        double j = other.a;
        double k = other.b;
        double l = other.c;

        double denominator = b * j - a * k;
        if (denominator == 0.0f) {
            return null;
        }
        double y = (double) ((a * l - c * j) / denominator);
        double x = (double) ((c * k - b * l) / denominator);

        if (!pointInSegment(this, x, y)) {
            return null;
        }

        return new Point(x, y);
    }

    private boolean pointInSegment(LineDoubleCrossableSegment segment, double poX, double poY) {
//        if (Math.abs(segment.x1 - segment.x2) > Math.abs(segment.y1 - segment.y2)) {
//            return segment.x1 <= poX && poX <= segment.x2 || segment.x1 >= poX && poX >= segment.x2;
//        } else {
//            return segment.y1 <= poY && poY <= segment.y2 || segment.y1 >= poY && poY >= segment.y2;
//        }
        if (Math.abs(segment.x1 - segment.x2) > 0) {
            if (segment.x1 <= poX && poX <= segment.x2 || segment.x1 >= poX && poX >= segment.x2) {
                return true;
            }
        }
        // we try both variants because of rounding error
        if (Math.abs(segment.y1 - segment.y2) > 0) {
            if (segment.y1 <= poY && poY <= segment.y2 || segment.y1 >= poY && poY >= segment.y2) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "LineDoubleCrossableSegment [" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + "]";
    }
}
