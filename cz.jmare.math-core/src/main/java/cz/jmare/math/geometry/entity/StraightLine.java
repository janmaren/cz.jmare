package cz.jmare.math.geometry.entity;

public class StraightLine {
    public double a;

    public double b;

    public double c;

    public StraightLine(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public StraightLine(double x1, double y1, double x2, double y2) {
        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    public StraightLine(TwoPoints twoPoints) {
        this(twoPoints.x1, twoPoints.y1, twoPoints.x2, twoPoints.y2);
    }

    public StraightLine(double k, double q) {
        this(1, 1 * k + q, 2, 2 * k + q);
    }

    public StraightLine(LineSegmentFloat lineSegment) {
        this(lineSegment.x1, lineSegment.y1, lineSegment.x2, lineSegment.y2);
    }

    public StraightLine(LineSegmentDouble lineSegment) {
        this(lineSegment.x1, lineSegment.y1, lineSegment.x2, lineSegment.y2);
    }

    @Override
    public String toString() {
        return "StraightLine [a=" + a + ", beta=" + b + ", c=" + c + "]";
    }

    public static StraightLine createXParallel(double y) {
        return new StraightLine(0, 1, -y);
    }

    public static StraightLine createYParallel(double x) {
        return new StraightLine(1, 0, -x);
    }

    public static StraightLine createXAxe() {
        return createXParallel(0);
    }

    public static StraightLine createYAxe() {
        return createYParallel(0);
    }

    public double yOnLine(double x) {
        return -(a * x + c) / b;
    }

    public double xOnLine(double y) {
        return -(b * y + c) / a;
    }
}
