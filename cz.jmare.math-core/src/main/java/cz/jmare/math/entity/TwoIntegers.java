package cz.jmare.math.entity;

public class TwoIntegers {
    public int x1;

    public int x2;

    public TwoIntegers() {
    }

    public TwoIntegers(int x1, int x2) {
        super();
        this.x1 = x1;
        this.x2 = x2;
    }

    @Override
    public String toString() {
        return "TwoIntegers [x1=" + x1 + ", x2=" + x2 + "]";
    }
}
