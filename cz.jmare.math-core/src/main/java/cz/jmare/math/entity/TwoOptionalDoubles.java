package cz.jmare.math.entity;

public class TwoOptionalDoubles {
    public Double x1;

    public Double x2;

    public TwoOptionalDoubles() {
    }

    public TwoOptionalDoubles(Double x1, Double x2) {
        super();
        this.x1 = x1;
        this.x2 = x2;
    }

    @Override
    public String toString() {
        return "TwoOptionalDoubles [x1=" + x1 + ", x2=" + x2 + "]";
    }
}
