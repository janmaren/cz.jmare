package cz.jmare.math.entity;

public class TwoFloats {
    public float x1;

    public float x2;

    public TwoFloats() {
    }

    public TwoFloats(float x1, float x2) {
        super();
        this.x1 = x1;
        this.x2 = x2;
    }

    @Override
    public String toString() {
        return "[x1=" + x1 + ", x2=" + x2 + "]";
    }
}
