package cz.jmare.math.entity;

import cz.jmare.math.geometry.entity.Point;

/**
 * Coeffiecients for conformal tranformation for: x' = a * x - b * y + c; y' = a * y + b * x + d
 */
@Deprecated
public class ConformalElements {
    public double a, b, c, d;

    public ConformalElements(double a, double b, double c, double d) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public ConformalElements(double[] coefs) {
        super();
        this.a = coefs[0];
        this.b = coefs[1];
        this.c = coefs[2];
        this.d = coefs[3];
    }
    
    public ConformalElements() {
    }

    public Point transform(Point point) {
        return new Point(point.x * a - point.y * b + c, point.y * a + point.x * b + d);
    }

    @Override
    public String toString() {
        return "Elements [a=" + a + ", beta=" + b + ", c=" + c + ", d=" + d + "]";
    }
}
