package cz.jmare.math.entity;

public class AFrames {
    private float sampleRate;

    private float[][] values;

    public AFrames(float[][] values, float sampleRate) {
        if (values.length == 0) {
            throw new IllegalArgumentException("There must be at least one data row");
        }
        this.sampleRate = sampleRate;
        this.values = values;
    }

    public AFrames(float[] values, float sampleRate) {
        this.sampleRate = sampleRate;
        this.values = new float[1][];
        this.values[0] = values;
    }

    @Override
    public String toString() {
        return "AFrame [sampleRate=" + sampleRate + ", number of rows= " + values.length + ", number of values=" + values[0].length + " which is " + getDurationSecs()  +  "s]";
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public float[][] getValues() {
        return values;
    }

    public void setValues(float[][] values) {
        this.values = values;
    }

    public float getDurationSecs() {
        return values[0].length / sampleRate;
    }
}
