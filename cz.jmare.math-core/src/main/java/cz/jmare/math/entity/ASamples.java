package cz.jmare.math.entity;

public class ASamples {
    private float sampleRate;

    private float[] values;

    public ASamples(float[] values, float sampleRate) {
        super();
        this.sampleRate = sampleRate;
        this.values = values;
    }

    @Override
    public String toString() {
        return "ASample [sampleRate=" + sampleRate + ", number of values=" + values.length + " which is " + getDurationSecs() +  "s]";
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public float[] getValues() {
        return values;
    }

    public void setValues(float[] values) {
        this.values = values;
    }

    public AFrames toFrames() {
        return new AFrames(values, sampleRate);
    }

    public float getDurationSecs() {
        return values.length / sampleRate;
    }
}
