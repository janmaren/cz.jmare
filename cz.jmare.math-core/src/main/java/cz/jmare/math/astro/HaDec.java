package cz.jmare.math.astro;

public class HaDec {
    /**
     * Hour angle in degrees
     */
    public double ha;

    /**
     * Declination (or latitude) with meanful values -90 to 90 degrees
     */
    public double dec;

    public HaDec(double haDegrees, double dec) {
        super();
        this.ha = haDegrees;
        this.dec = dec;
    }

    @Override
    public String toString() {
        return "haDec(" + ha + ", " + dec + ")";
    }
}
