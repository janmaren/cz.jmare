package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.math.geometry.entity.Point;

/**
 * Polar coordinates in degrees where ra represents a right ascension (or longitude) and dec is declination (or latitude)
 */
public class RaDec {
    /**
     * Right ascension (or longitude) with meanful values 0 to 360 degrees unless a next round
     */
    public double ra;

    /**
     * Declination (or latitude) with meanful values -90 to 90 degrees
     */
    public double dec;

    public RaDec(double raDegrees, double dec) {
        super();
        this.ra = raDegrees;
        this.dec = dec;
    }

    public RaDec(String str) {
        super();
        RaDec parseCoords = ACooUtil.parseCoordsNormalized(str, true);
        this.ra = parseCoords.ra;
        this.dec = parseCoords.dec;
    }

    public RahDec toRahDec() {
        return new RahDec(ra / 15.0, dec);
    }

    public Point toPoint() {
        return new Point(ra, dec);
    }

    public Galactic toGalacticJ2000() {
        return AstroConversionUtil.raDecToGalacticJ2000(ra, dec);
    }

    @Override
    public String toString() {
        return "raDec(" + ra + ", " + dec + ")";
    }

    public static RaDec ofHoursDegs(double raHours, double decDegs) {
        return new RaDec(raHours * 15.0, decDegs);
    }

    public RahDec toNormalizedRahDec() {
        double raNormalized = GonUtil.normalizeDegZero360(ra);
        double decNormalized = GonUtil.normalizeDegMinus180180(dec);
        return new RahDec(raNormalized / 15.0, decNormalized);
    }
    
    public RaDecR toRaDecR(double r) {
    	return new RaDecR(ra, dec, r);
    }
    
    public static void main(String[] args) {
        RaDec raDec = ACooUtil.parseCoords("18 53 35.0967659112 +33 01 44.883287544");
        Galactic galactic = raDec.toGalacticJ2000();
        System.out.println(galactic.l + " " + galactic.b);
        RaDec raDecJ2000 = galactic.toRaDecJ2000();
        System.out.println(ACooUtil.formatCoordsPublic(raDecJ2000));
    }
}
