package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.data.util.HourUtil;

public class EclipticR {
	/**
	 * Lambda in degrees
	 */
	public double lambda;
	
	/**
	 * Beta in degrees
	 */
	public double beta;
	
	/**
	 * Distance in AU
	 */
	public double r;

	public EclipticR(double l, double b, double r) {
		super();
		this.lambda = l;
		this.beta = b;
		this.r = r;
	}

	@Override
	public String toString() {
		return "eclipr(" + lambda + ", " + beta + ", " + r + ")";
	}
	
	public Ecliptic toEcliptic() {
		return new Ecliptic(lambda, beta);
	}
	
	/**
	 * Convert to RaDec using J2000 obliquity
	 * @return
	 */
	public RaDecR toRaDecR() {
		RaDec raDec = AstroConversionUtil.eclipToRaDecJ2000(lambda, beta);
		return new RaDecR(raDec.ra, raDec.dec, r);
	}
	
    /**
     * Convert to RaDec
     * @return
     */
    public RaDecR toRaDecR(double obliq) {
        RaDec raDec = AstroConversionUtil.eclipToRaDec(lambda, beta, obliq);
        return new RaDecR(raDec.ra, raDec.dec, r);
    }
    
	public static void main(String[] args) {
		Ecliptic eclip = new Ecliptic(168.737, 1.208);
		RaDec raDec = eclip.toRaDec();
		System.out.println(raDec);
		System.out.println(HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra)));
	}
}
