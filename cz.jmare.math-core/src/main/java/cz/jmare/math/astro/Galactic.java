package cz.jmare.math.astro;
import static java.lang.Math.toRadians;

import cz.jmare.data.util.AstroConversionUtil;
public class Galactic {
    public static double poleRa2000 = toRadians(192.85950833333334); // 12h 51m 26.282s
    public static double poleDec2000 = toRadians(27.12833611111111); // 27° 07′ 42.01"
    public static double posAngle2000 = toRadians(122.932 - 90.0); // 122.932° - 90°

    /**
     * galactic longitude with meanful values 0 to 360 degrees unless a next round
     */
    public double l;
    
    /**
     * galactic latitude with meanful values -90 to 90 degrees
     */
    public double b;

    public Galactic(double l, double b) {
        super();
        this.l = l;
        this.b = b;
    }
    
    public RaDec toRaDecJ2000() {
        return AstroConversionUtil.galacticToRaDecJ2000(l, b);
//        double lRad = toRadians(l);
//        double bRad = toRadians(b);
//
//        double ra = atan2((cos(bRad) * cos(lRad - posAngle2000)),
//                (sin(bRad) * cos(poleDec2000) - cos(bRad) * sin(poleDec2000) * sin(lRad - posAngle2000))) + poleRa2000;
//        double dec = asin(cos(bRad) * cos(poleDec2000) * sin(lRad - posAngle2000) + sin(bRad) * sin(poleDec2000));
//
//        return new RaDec(toDegrees(ra), toDegrees(dec));
    }

    @Override
    public String toString() {
        return "galactic(" + l + ", " + b + ")";
    }
}
