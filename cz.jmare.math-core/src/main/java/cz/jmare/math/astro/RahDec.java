package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.GonUtil;
import cz.jmare.data.util.HourUtil;

/**
 * Celestial coordinates using RA[hours] and DEC[degrees]
 */
public class RahDec {
    /**
     * Right ascension with meanful values 0 to 24 (excl.) hours
     */
    public double rah;

    /**
     * Declination (or latitude) with meanful values -90 to 90 degrees
     */
    public double dec;

    public RahDec(double raHours, double dec) {
        super();
        this.rah = raHours;
        this.dec = dec;
    }

    public RahDec(String str) {
        super();
        RaDec parseCoords = ACooUtil.parseCoords(str, false);
        RahDec normalizedPolarRaDec = parseCoords.toNormalizedRahDec();
        this.rah = normalizedPolarRaDec.rah;
        this.dec = normalizedPolarRaDec.dec;
    }

    @Override
    public String toString() {
        return "RahDec [ra=" + HourUtil.formatHours(rah) + ", dec=" + GonUtil.formatDegrees(dec) + "]";
    }

    public RaDec toRaDec() {
        return new RaDec(rah * 15.0, dec);
    }
}
