package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;

/**
 * Polar coordinates in degrees where ra represents a right ascension (or longitude) and dec is declination (or latitude)
 */
public class RaDecR {
    /**
     * Right ascension (or longitude) with meanful values 0 to 360 degrees unless a next round
     */
    public double ra;

    /**
     * Declination (or latitude) with meanful values -90 to 90 degrees
     */
    public double dec;
    
    /**
     * Distance in AU
     */
    public double r;
    
    public RaDecR(double raDegrees, double dec, double r) {
        super();
        this.ra = raDegrees;
        this.dec = dec;
        this.r = r;
    }

    public RaDecR(String str, double r) {
        super();
        RaDec parseCoords = ACooUtil.parseCoords(str, true);
        this.ra = parseCoords.ra;
        this.dec = parseCoords.dec;
        this.r = r;
    }

    public RaDec toRaDec() {
        return new RaDec(this.ra, this.dec);
    }

    @Override
    public String toString() {
        return "raDec(" + ra + ", " + dec + "," + r + ")";
    }
}
