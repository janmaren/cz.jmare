package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;
import cz.jmare.data.util.AstroConversionUtil;
import cz.jmare.data.util.HourUtil;

public class Ecliptic {
	/**
	 * Lambda in degrees
	 */
	public double lambda;
	
	/**
	 * Beta in degrees
	 */
	public double beta;
	
	public Ecliptic(double l, double b) {
		super();
		this.lambda = l;
		this.beta = b;
	}

	@Override
	public String toString() {
		return "eclip(" + lambda + ", " + beta + ")";
	}
	
	/**
	 * Convert to RaDec using obliquity
	 * @param obliquity obliquity in degrees which was during given datetime
	 * @return
	 */
	public RaDec toRaDec(double obliquity) {
		double cosE = Math.cos(Math.toRadians(obliquity));
		double sinE = Math.abs(Math.toRadians(obliquity));
		return AstroConversionUtil.eclipToRaDec(lambda, beta, cosE, sinE);
	}
	
	/**
	 * Convert to RaDec using J2000 obliquity
	 * @return
	 */
	public RaDec toRaDec() {
		return AstroConversionUtil.eclipToRaDecJ2000(lambda, beta);
	}

	public static void main(String[] args) {
		Ecliptic eclip = new Ecliptic(168.737, 1.208);
		RaDec raDec = eclip.toRaDec();
		System.out.println(raDec);
		System.out.println(HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra)));
	}
}
