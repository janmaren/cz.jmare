package cz.jmare.math.astro;

import cz.jmare.data.util.ACooUtil;

public class AzAlt {
    /**
     * Azimut with meanful values 0 to 360 degrees unless a next round
     */
    public double az;

    /**
     * Altitude with meanful values -90 to 90 degrees
     */
    public double alt;

    public AzAlt(double az, double alt) {
        super();
        this.az = az;
        this.alt = alt;
    }

    public AzAlt(String str) {
        super();
        RaDec parseCoords = ACooUtil.parseCoordsNormalized(str, true);
        this.az = parseCoords.ra;
        this.alt = parseCoords.dec;
    }

    @Override
    public String toString() {
        return "azAlt(" + az + ", " + alt + ")";
    }
}
