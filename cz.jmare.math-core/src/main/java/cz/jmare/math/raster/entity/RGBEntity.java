package cz.jmare.math.raster.entity;

public class RGBEntity {
    public int red;

    public int green;

    public int blue;

    public RGBEntity(int red, int green, int blue) {
        super();
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public RGBEntity(String str) {
        if (str.startsWith("#")) {
            if (str.length() != 7) {
                throw new IllegalArgumentException("Color definition must have 7 characters, as #RRGGBB");
            }
            red = hex2decimal(str.substring(1, 3));
            green = hex2decimal(str.substring(3, 5));
            blue = hex2decimal(str.substring(5, 7));
        } else {
            String[] split = str.trim().split(",");
            if (split.length != 3) {
                throw new IllegalArgumentException("String " + str + " hasn't 3 numbers");
            }
            try {
                red = Integer.parseInt(split[0].trim());
                green = Integer.parseInt(split[1].trim());
                blue = Integer.parseInt(split[2].trim());
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid number, must be #RRGGBB or RRR,GGG,BBB");
            }
            if (red < 0 || red > 255) {
                throw new IllegalArgumentException("Red not in range 0-255");
            }
            if (green < 0 || green > 255) {
                throw new IllegalArgumentException("Green not in range 0-255");
            }
            if (blue < 0 || blue > 255) {
                throw new IllegalArgumentException("Blue not in range 0-255");
            }
        }
    }

    @Override
    public String toString() {
        return "[r:" + red + ", g:" + green + ", beta:" + blue + "]";
    };

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            if (d == -1) {
                throw new IllegalArgumentException("Only hex characters 0-9 and A-F allowed");
            }
            val = 16*val + d;
        }
        return val;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + blue;
        result = prime * result + green;
        result = prime * result + red;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RGBEntity other = (RGBEntity) obj;
        if (blue != other.blue)
            return false;
        if (green != other.green)
            return false;
        if (red != other.red)
            return false;
        return true;
    }
}
