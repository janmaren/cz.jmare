package cz.jmare.math.raster.entity;

import java.awt.image.BufferedImage;

public class RGBEntUtil {

    public static RGBEntity rHighestToRGB(int rgb) {
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xFF;
        return new RGBEntity(red, green, blue);
    }

    public static int rgbToRHighest(RGBEntity rgb) {
        return rgb.red << 16 | rgb.green << 8 | rgb.blue;
    }

    public static int rgbToRHighest(RGBEntity rgb, int alpha) {
        return (alpha << 24) | rgb.red << 16 | rgb.green << 8 | rgb.blue;
    }

    public static String rHighestToString(int rgb) {
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xFF;
        return red + ", " + green + ", " + blue;
    }

    public static String toRGBNumbersString(RGBEntity rgb) {
        return rgb.red + ", " + rgb.green + ", " + rgb.blue;
    }

    public static String toRGBHexString(RGBEntity rgb) {
        return "#" + numberToHex(rgb.red) + numberToHex(rgb.green) + numberToHex(rgb.blue);
    }

    private static String numberToHex(int number) {
        String str = number < 16 ? "0" : "";
        return str + Integer.toHexString(number);
    }

    public static int extractAlpha(int argb) {
        return (argb >> 24) & 0xff;
    }

    /**
     * To int with red highest weight and alpha byte.
     * Corresponds to {@link BufferedImage#TYPE_INT_ARGB}
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static int rHighestAlphaToRGBInt(int red, int green, int blue, int alpha) {
        return (alpha << 24) | red << 16 | green << 8 | blue;
    }

    /**
     * To int with red highest weight and opaque to alpha byte.
     * Corresponds to {@link BufferedImage#TYPE_INT_RGB}
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static int rHighestOpaqToRGBInt(int red, int green, int blue) {
        return (0xFF << 24) | red << 16 | green << 8 | blue;
    }

    public static int rLowestAlphaToRGBInt(int red, int green, int blue, int alpha) {
        return red | green << 8 | blue << 16 | alpha << 24;
    }

    public static String rLowestRGBToString(int rgb) {
        int red = rgb & 0xFF;
        int green = (rgb >> 8) & 0xff;
        int blue = (rgb >> 16) & 0xff;
        return red + ", " + green + ", " + blue;
    }

    public static int rLowestToRGBInt(int red, int green, int blue) {
        return red | green << 8 | blue << 16;
    }

    public static int setAlpha(int rgb, int alpha) {
        return (alpha << 24) | rgb;
    }
}
