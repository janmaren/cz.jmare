package cz.jmare.data.util;

import cz.jmare.math.astro.*;
import cz.jmare.math.geo.GeoLoc;
import cz.jmare.math.geometry.entity.Xyz;

import static java.lang.Math.*;

public class AstroConversionUtil {
	/**
	 * Obliquity for J2000
	 */
	private static final double J2000_OBLIQUITY = 23.439291111;
	static double cosE = cos(toRadians(J2000_OBLIQUITY));
	static double sinE = sin(toRadians(J2000_OBLIQUITY));

    public static double nPoleRa2000 = toRadians(192.85950833333334); // 12h 51m 26.282s
    public static double nPoleDec2000 = toRadians(27.12833611111111); // 27° 07′ 42.01"
    public static double posAngle2000 = toRadians(122.932); // 122.932°
    public static double sinNPoleDec2000 = sin(nPoleDec2000);
    public static double cosNPoleDec2000 = cos(nPoleDec2000);

	public static RaDec eclipToRaDecJ2000(double lambda, double beta) {
		return eclipToRaDec(lambda, beta, cosE, sinE);
	}

	/**
	 * Convert ecliptic coordinates to equitorial
	 * @param lambda lambda in degrees
	 * @param beta beta in degrees
	 * @param obliq obliquity for given date
	 * @return
	 */
    public static RaDec eclipToRaDec(double lambda, double beta, double obliq) {
        double cosE = cos(toRadians(obliq));
        double sinE = sin(toRadians(obliq));
        return eclipToRaDec(lambda, beta, cosE, sinE);
    }

	public static RaDec eclipToRaDec(double lambda, double beta, double cosE, double sinE) {
		double radL = toRadians(lambda);
		double sinL = sin(radL);
		double cosL = cos(radL);
		double radB = toRadians(beta);
		double tanB = Math.tan(radB);
		double cosB = cos(radB);
		double sinB = sin(radB);

		double alpha = Math.atan2(sinL * cosE - tanB * sinE, cosL);
		double delta = asin(sinB * cosE + cosB * sinE * sinL);

		return new RaDec(toDegrees(alpha), toDegrees(delta));
	}

	public static Ecliptic raDecToEclipJ2000(double ra, double dec) {
		return raDecToEclip(ra, dec, cosE, sinE);
	}

	public static Ecliptic raDecToEclip(double ra, double dec, double obliq) {
		double cosE = cos(toRadians(obliq));
		double sinE = sin(toRadians(obliq));
		return raDecToEclip(ra, dec, cosE, sinE);
	}

	public static Ecliptic raDecToEclip(double ra, double dec, double cosE, double sinE) {
		double radRa = toRadians(ra);
		double sinRa = sin(radRa);
		double cosRa = cos(radRa);
		double radDec = toRadians(dec);
		double tanDec = Math.tan(radDec);
		double cosDec = cos(radDec);
		double sinDec = sin(radDec);

		double lambda = Math.atan2(sinRa * cosE + tanDec * sinE, cosRa);
		double beta = asin(sinDec * cosE - cosDec * sinE * sinRa);

		return new Ecliptic(toDegrees(lambda), toDegrees(beta));
	}

	public static Xyz eclipticToXyz(EclipticR eclipticR) {
		return eclipticToXyz(eclipticR.lambda, eclipticR.beta, eclipticR.r);
	}

	public static Xyz eclipticToXyz(double lambda, double beta, double r) {
		double betaRad = toRadians(beta);
		double lamRad = toRadians(lambda);
		double x = r * cos(betaRad) * cos(lamRad);
		double y = r * cos(betaRad) * sin(lamRad);
		double z = r * sin(betaRad);
		return new Xyz(x, y, z);
	}

	public static EclipticR xyzToEclipr(double x, double y, double z) {
		double r = sqrt(x * x + y * y + z * z);
		double lambda = Math.atan2(y, x);
		double beta = asin(z / r);
		return new EclipticR(toDegrees(lambda), toDegrees(beta), r);
	}

    public static Galactic raDecToGalacticJ2000(double raDeg, double decDeg) {
        double decRad = toRadians(decDeg);
        double raRad = toRadians(raDeg);
        double raRadMinusNPole = raRad - nPoleRa2000;
        double l = posAngle2000 - atan2(cos(decRad) * sin(raRadMinusNPole), sin(decRad) * cosNPoleDec2000 - cos(decRad) * sinNPoleDec2000 * cos(raRadMinusNPole));
        double b = asin(sin(decRad) * sinNPoleDec2000 + cos(decRad) * cosNPoleDec2000 * cos(raRadMinusNPole));
        return new Galactic(toDegrees(l), toDegrees(b));
    }
    
    public static RaDec galacticToRaDecJ2000(double lDeg, double bDeg) {
        double lRad = toRadians(lDeg) - (posAngle2000 - Math.PI / 2.0);
        double bRad = toRadians(bDeg);
        double ra = atan2((cos(bRad) * cos(lRad)),
                (sin(bRad) * cos(nPoleDec2000) - cos(bRad) * sin(nPoleDec2000) * sin(lRad))) + nPoleRa2000;
        double dec = asin(cos(bRad) * cos(nPoleDec2000) * sin(lRad) + sin(bRad) * sin(nPoleDec2000));
        return new RaDec(toDegrees(ra), toDegrees(dec));
    }

	/**
	 * Convert Az, Alt to hour angle in degrees and declination in degrees
	 * @param azAlt coordinates of object
	 * @param latDeg geographic latitude, positive on north
	 * @return HaDec, where HA in degrees and DEC in degrees
	 */
	private static HaDec toHaDec(AzAlt azAlt, double latDeg) {
		double alt = toRadians(azAlt.alt); // altitude
		double lat = toRadians(latDeg); // geographic latitude
		double az = toRadians(azAlt.az); // azimute

		double sinA = sin(alt);
		double sinL = sin(lat);
		double cosL = cos(lat);

		double sinD = sinA * sinL + cos(alt) * cosL * cos(az);
		double dec = asin(sinD);

		double cosH= (sinA - sinL * sinD) / (cosL * cos(dec));
		if (cosH < -1) {
			cosH = -1;
		} else if (cosH > 1) {
			cosH = 1;
		}
		double h = acos(cosH);

		if (sin(az) > 0) {
			h = 2 * PI - h;
		}

		return new HaDec(toDegrees(h), toDegrees(dec));
	}

	/**
	 * Convert declination, hour anngle and geographic latitude to horizontal coordinates
	 * @param decDeg declination in degrees
	 * @param haDeg hour angle in degrees
	 * @param latDeg geographic latitude
	 * @return horizontal coordinates
	 */
	public static AzAlt toAzAlt(double decDeg, double haDeg, double latDeg) {
		double d = toRadians(decDeg);
		double h = toRadians(haDeg);
		double l = toRadians(latDeg);

		double sinD = sin(d);
		double sinL = sin(l);
		double cosL = cos(l);

		double alt = asin(sinL * sinD + (cosL * cos(h) * cos(d)));

		double cosA = (sinD - sinL * sin(alt)) / (cosL * cos(alt));
		if (cosA < -1) {
			cosA = -1;
		} else if (cosA > 1) {
			cosA = 1;
		}
		double az = acos(cosA);

		if (sin(h) > 0) {
			az = 2 * PI - az;
		}

		return new AzAlt(toDegrees(az), toDegrees(alt));
	}

	/**
	 * Convert equitorial coordinates to horizontal
	 * @param rahDec equitorial coordinates
	 * @param geoLoc geo coordinates of observer
	 * @param jd julian date
	 * @return horizontal coordinates
	 */
	public static AzAlt toAzAlt(RahDec rahDec, GeoLoc geoLoc, double jd) {
		double lmstHours = GMSTUtil.toLmstHours(GMSTUtil.calcGmstHoursSimple(jd), geoLoc.longitude);
		double ha = lmstHours - rahDec.rah;
		ha = ha % 24.0;
		if (ha < 0) {
			ha += 24.0;
		}
		return toAzAlt(rahDec.dec, ha * 15.0, geoLoc.latitude);
	}

	/**
	 * Convert horizontal coordinates to equatorial.
	 * @param azAlt horizontal coordinates
	 * @param geoLoc observer geo coordinates
	 * @param jd julian date
	 * @return equatorial coordinates
	 */
	public static RahDec toRahDec(AzAlt azAlt, GeoLoc geoLoc, double jd) {
		HaDec haDec = toHaDec(azAlt, geoLoc.latitude);
		double lmstHours = GMSTUtil.toLmstHours(GMSTUtil.calcGmstHoursSimple(jd), geoLoc.longitude);
		double raHours = lmstHours - haDec.ha / 15.0;
		raHours = raHours % 24;
		if (raHours < 0) {
			raHours += 24.0;
		}
		return new RahDec(raHours, haDec.dec);
	}

	/**
	 * https://www.cloudynights.com/topic/711324-the-j2000-epoch-and-ra/
	 */
	public static RaDec convertEquinoxSimply(RaDec sourceRaDec, double sourceEquinox, double destinationEquinox) {
		double dt = destinationEquinox - sourceEquinox;
		double t = (sourceEquinox + destinationEquinox) / 2.0;
		double T = (t - 1900.0) / 100.0;

		double e0 = 23.452294 - 0.0130125 * T;
		double psi = (50.3708 + 0.0050 * T) / 3600.0;
		double l = (0.1247 - 0.0188 * T) / 3600.0;

		double m = psi * cos(toRadians(e0)) - l;
		double n = psi * sin(toRadians(e0));
		double mRad = toRadians(m);
		double nRad = toRadians(n);

		double ra0 = sourceRaDec.ra;
		double dec0 = sourceRaDec.dec;
		double ra2 = toRadians(ra0) + (mRad + nRad * sin(toRadians(ra0)) * tan(toRadians(dec0))) * dt;
		double dec2 = toRadians(dec0) + (nRad * cos(toRadians(ra0))) * dt;

		return new RaDec(toDegrees(ra2), toDegrees(dec2));
	}

	/**
	 * https://sceweb.uhcl.edu/helm/WEB-Positional%20Astronomy/Tutorial/Precession/Precession.html
	 */
	public static RaDec convertEquinoxByEdu(RaDec sourceRaDec, double sourceEquinox, double destinationEquinox) {
		double dt = destinationEquinox - sourceEquinox;

		double m = 3.074;
		double n = 1.337;

		double ra0 = sourceRaDec.ra;
		double dec0 = sourceRaDec.dec;

		double delRa = m + n * sin(toRadians(ra0)) * tan(toRadians(dec0));
		double delDec = 15 * n * cos(toRadians(ra0));

		double rah = ra0 / 15 + delRa / 3600 * dt;
		double dec = dec0 + delDec / 3600 * dt;

		return new RaDec(rah * 15, dec);
	}

	/**
	 * https://www.cloudynights.com/topic/561254-ra-dec-epoch-conversion/
	 */
	public static RaDec convertEquinoxExactly(RaDec sourceRaDec, double JD0, double JD) {
		double ra0 = sourceRaDec.ra;
		double dec0 = sourceRaDec.dec;

		// Find the transformation matrix
		double T0 = (JD0 - 2433282.423) / 36524.219878;
		double T = (JD - JD0) / 36524.219878;
		double Xx = 1 - ((29696 + 26 * T0) * T * T - 13 * T * T * T) * 0.00000001;
		double Xy = ((2234941 + 1355 * T0) * T - 676 * T * T + 221 * T * T * T) * 0.00000001;
		double Xz = ((971690 - 414 * T0) * T + 207 * T * T + 96 * T * T * T) * 0.00000001;
		double Yx = -Xy;
		double Yy = 1 - ((24975 + 30 * T0) * T * T - 15 * T * T * T) * 0.00000001;
		double Yz = -((10858 + 2 * T0) * T * T) * 0.00000001;
		double Zx = -Xz;
		double Zy = Yz;
		double Zz = 1 - ((4721 - 4 * T0) * T * T) * 0.00000001;

		// Transform coordinates
		double rar0 = toRadians(ra0);
		double decr0 = toRadians(dec0);
		double x0 = cos(decr0) * cos(rar0);
		double y0 = cos(decr0) * sin(rar0);
		double z0 = sin(decr0);
		double x = Xx * x0 + Yx * y0 + Zx * z0;
		double y = Xy * x0 + Yy * y0 + Zy * z0;
		double z = Xz * x0 + Yz * y0 + Zz * z0;
		double rarad = atan(y / x);
		if (x < 0) rarad = rarad + PI;
		if ((y < 0) && (x > 0)) rarad = rarad + 2 * PI;
		double decrad = atan(z / sqrt(1 - z * z));

		return new RaDec(toDegrees(rarad), toDegrees(decrad));
	}

	/**
	 * https://www.cloudynights.com/topic/561254-ra-dec-epoch-conversion/
	 * PREC2
	 */
	public static RaDec convertEquinoxExactlyPrec2(RaDec sourceRaDec, double JD0, double JD) {
		double ra0 = sourceRaDec.ra;
		double dec0 = sourceRaDec.dec;

		// Find the precession angles
		double TT = (JD0 - 2451545.0) / 36525.0;
		double t = (JD - JD0) / 36525.0;
		double zt = (2306.2181 + 1.39656*TT - 0.000139*TT*TT)*t;
		double z = zt;
		zt = zt + (0.30188 - 0.000344*TT)*t*t + 0.017998*t*t*t;
		zt = zt/3600*PI/180;
		z = z + (1.09468 + 0.000066*TT)*t*t + 0.018203*t*t*t;
		z = z/3600*PI/180;
		double th = (2004.3109 - 0.85330*TT - 0.000217*TT*TT)*t;
		th = th - (0.42665 + 0.000217*TT)*t*t - 0.041833*t*t*t;
		th = th/3600*PI/180;

		// Find the transformation matrix
		double Xx = cos(z)*cos(th)*cos(zt)-sin(z)*sin(zt);
		double Yx = -cos(z)*cos(th)*sin(zt)-sin(z)*cos(zt);
		double Zx = -cos(z)*sin(th);
		double Xy = sin(z)*cos(th)*cos(zt)+cos(z)*sin(zt);
		double Yy = -sin(z)*cos(th)*sin(zt)+cos(z)*cos(zt);
		double Zy = -sin(z)*sin(th);
		double Xz = sin(th)*cos(zt);
		double Yz = -sin(th)*sin(zt);
		double Zz = cos(th);

		// Transform coordinates
		double rar0 = toRadians(ra0);
		double decr0 = toRadians(dec0);
		double x0 = cos(decr0)*cos(rar0);
		double y0 = cos(decr0)*sin(rar0);
		double z0 = sin(decr0);
		double x = Xx*x0 + Yx*y0 + Zx*z0;
		double y = Xy*x0 + Yy*y0 + Zy*z0;
		double z_ = Xz*x0 + Yz*y0 + Zz*z0;
		double rarad = atan(y / x);
		if (x < 0) rarad = rarad + PI;
		if ((y < 0) && (x > 0)) rarad = rarad + 2*PI;
		double decrad = atan(z_/sqrt(1 - z_*z_));

		return new RaDec(toDegrees(rarad), toDegrees(decrad));
	}

	public static RaDec properMotion(RaDec sourceRaDec, double sourceEquinox, double destinationEquinox, double masyrRa, double masyrDe) {
		double dt = destinationEquinox - sourceEquinox;

		double delRa = masyrRa / 3.6E6 * dt; // deg
		double delDec = masyrDe / 3.6E6 * dt; // deg

		return new RaDec(sourceRaDec.ra + delRa, sourceRaDec.dec + delDec);
	}

	public static void main(String[] args) {
		RaDec raDecArcturus = convertEquinoxExactly(ACooUtil.parseCoords("14 13 22.74367 +19 26 30.9786"), JulianUtil.julianDateB1950(), JulianUtil.julianDateJ2000());
		RaDec raDecArcturus2 = convertEquinoxExactlyPrec2(ACooUtil.parseCoords("14 13 22.74367 +19 26 30.9786"), JulianUtil.julianDateB1950(), JulianUtil.julianDateJ2000());
		raDecArcturus = properMotion(raDecArcturus, JulianUtil.toEpochYear(JulianUtil.julianDateB1950()), JulianUtil.toEpochYear(JulianUtil.julianDateJ2000()), -1093.39, -2000.06);
		System.out.println(ACooUtil.formatRaDec(raDecArcturus));
		raDecArcturus2 = properMotion(raDecArcturus2, JulianUtil.toEpochYear(JulianUtil.julianDateB1950()), JulianUtil.toEpochYear(JulianUtil.julianDateJ2000()), -1093.39, -2000.06);
		System.out.println(ACooUtil.formatRaDec(raDecArcturus2));

		RaDec raDecApollo = convertEquinoxExactly(ACooUtil.parseCoords("02 31 49.09456 +89 15 50.7923"), 2451545.0, JulianUtil.julianDateNow());
		RaDec raDecApollo2 = convertEquinoxExactlyPrec2(ACooUtil.parseCoords("02 31 49.09456 +89 15 50.7923"), 2451545.0, JulianUtil.julianDateNow());
		raDecApollo = properMotion(raDecApollo, JulianUtil.toEpochYear(JulianUtil.julianDateJ2000()), JulianUtil.toEpochYear(JulianUtil.julianDateNow()), 44.48, -11.85);
		System.out.println(ACooUtil.formatRaDec(raDecApollo));
		raDecApollo2 = properMotion(raDecApollo2, JulianUtil.toEpochYear(JulianUtil.julianDateJ2000()), JulianUtil.toEpochYear(JulianUtil.julianDateNow()), 44.48, -11.85);
		System.out.println(ACooUtil.formatRaDec(raDecApollo2));

		RaDec raDecA = convertEquinoxByEdu(ACooUtil.parseCoords("14 15 39.67207 +19 10 56.6730"), 1950, 2000);
		raDecA = properMotion(raDecA, 2000, 2024.3, -1093.39, -2000.06);
		String sA = ACooUtil.formatRaDec(raDecA);
		System.out.println(sA);

		RaDec raDecX = convertEquinoxByEdu(ACooUtil.parseCoords("01 48 47.87574 +89 01 43.6298"), 1950, 2000);
		raDecX = properMotion(raDecX, 1950, 2000, 44.48, -11.85);
		String sX = ACooUtil.formatRaDec(raDecX);
		System.out.println(sX);

		RaDec raDec = convertEquinoxByEdu(ACooUtil.parseCoords("14 14 44.9 +19 17 09"), 1980, 2000);
		String s1 = ACooUtil.formatRaDec(raDec);
		System.out.println(s1);

		double epochYearJ2000 = JulianUtil.toEpochYear(JulianUtil.julianDateJ2000());
		double epochYearNow = JulianUtil.toEpochYear(JulianUtil.julianDateNow());
		RaDec raDec2 = convertEquinoxByEdu(ACooUtil.parseCoords("02 31 47 +89 15 51"), epochYearJ2000, epochYearNow);
		System.out.println(ACooUtil.formatRaDec(raDec2));
		raDec2 = properMotion(raDec2, 2000, epochYearNow, 44.48, -11.85);
		System.out.println(ACooUtil.formatRaDec(raDec2));

//		GeoLoc geoLoc = new GeoLoc(50 + 4 / 60.0 + 52 / 3600.0, 14 + 26 / 60.0 + 52 / 3600.0);
//		double jd = JulianUtil.toJulianExactly(2022, 11, 25, 20, 0, 0);
//		RahDec rahDec = toRahDec(
//				new AzAlt(263 + 12 / 60.0 + 30 / 3600.0, 26 + 55 / 60.0 + 11 / 3600.0),
//				geoLoc,
//				jd);
//		String s = ACooUtil.formatCoordsPublic(rahDec.toRaDec());
//		System.out.println(s);
//
//		AzAlt azAlt = toAzAlt(rahDec, geoLoc, jd);
//		String strAzAlt = ACooUtil.formatAzAlt(azAlt);
//		System.out.println(strAzAlt);
//
//		AzAlt azAlt1 = ACooUtil.parseAzAltCoords(strAzAlt);
//		System.out.println(azAlt1);
	}
}
