package cz.jmare.data.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class JulianUtil {
    private final static List<String> MONTH_NAMES = List.of("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
    
    
    /**
     * Returns the Julian day number that begins at noon of this day, Positive year
     * signifies A.D., negative year B.C. Remember that the year after 1 B.C. was 1
     * A.D.
     *
     * ref : Numerical Recipes in C, 2nd ed., Cambridge University Press 1992
     */
    // Gregorian Calendar adopted Oct. 15, 1582 (2299161)
    public static int JGREG = 15 + 31 * (10 + 12 * 1582);
    public static double HALFSECOND = 0.5;

    private static double fracDayFromNoon(int hours, int minutes, double seconds) {
        return ((hours - 12) + minutes / 60.0 + seconds / 3600.0) / 24.0;
    }

    public static double toJulianExactly(int year, int month, int day, int hours, int minutes, double seconds) {
        return toJulian(year, month, day) + fracDayFromNoon(hours, minutes, seconds);
    }

    /**
     * Calculate juliand date without decimal part
     * @param year
     * @param month jan=1, feb=2,...
     * @param day
     * @return
     */
    public static double toJulian(int year, int month, int day) {
        int julianYear = year;
        if (year < 0)
            julianYear++;
        int julianMonth = month;
        if (month > 2) {
            julianMonth++;
        } else {
            julianYear--;
            julianMonth += 13;
        }

        double julian = (java.lang.Math.floor(365.25 * julianYear) + java.lang.Math.floor(30.6001 * julianMonth) + day
                + 1720995.0);
        if (day + 31 * (month + 12 * year) >= JGREG) {
            // change over to Gregorian calendar
            int ja = (int) (0.01 * julianYear);
            julian += 2 - ja + (0.25 * ja);
        }
        return java.lang.Math.floor(julian);
    }
    
    /**
     * Split julian day to gregorian elements year, month, day, hour, minute, second. Seconds are rounded by number of decimals
     * @param injulian julian day
     * @param decimals rounding to number of decimals. Zero means rounding to whole seconds
     * @return array with year, month, day, hour, minute, second (rounded)
     */
    public static double[] fromJulianRoundSeconds(double injulian, int decimals) {
        double[] dt = fromJulian(injulian);
        BigDecimal bd = new BigDecimal(dt[5]);
        bd = bd.setScale(decimals, RoundingMode.HALF_UP);
        double seconds = bd.doubleValue();
        if (seconds >= 60.0) {
            dt[5] = 0.0;
            dt[4]++;
            if (dt[4] >= 60.0) {
                dt[4] -= 60.0;
                dt[3]++;
                if (dt[3] >= 24.0) {
                    dt[3] -= 24.0;
                    LocalDate localDate = LocalDate.of((int) dt[0], (int) dt[1], (int) dt[2]);
                    localDate = localDate.plusDays(1);
                    dt[0] = localDate.getYear();
                    dt[1] = localDate.getMonthValue();
                    dt[2] = localDate.getDayOfMonth();
                }
            }
        } else {
            dt[5] = seconds;
        }
        return dt;
    }
    
    /**
     * Split julian day to gregorian elements year, month, day, hour, minute, second. Seconds are rounded to whole seconds
     * @param injulian julian day
     * @return array with year, month, day, hour, minute, second (rounded)
     */
    public static double[] fromJulianRoundSeconds(double injulian) {
        return fromJulianRoundSeconds(injulian, 0);
    }
    
    /**
     * Split julian day to gregorian elements year, month, day, hour, minute, second. Seconds may contain decimals
     * @param injulian julian day
     * @return array with year, month, day, hour, minute, second (with decimals)
     */
    public static double[] fromJulian(double injulian) {
        int jalpha, ja, jb, jc, jd, je, year, month, day;
        double julian = injulian + HALFSECOND / 86400.0;
        ja = (int) julian;
        if (ja >= JGREG) {
            jalpha = (int) (((ja - 1867216) - 0.25) / 36524.25);
            ja = ja + 1 + jalpha - jalpha / 4;
        }

        jb = ja + 1524;
        jc = (int) (6680.0 + ((jb - 2439870) - 122.1) / 365.25);
        jd = 365 * jc + jc / 4;
        je = (int) ((jb - jd) / 30.6001);
        day = jb - jd - (int) (30.6001 * je);
        month = je - 1;
        if (month > 12)
            month = month - 12;
        year = jc - 4715;
        if (month > 2)
            year--;
        if (year <= 0)
            year--;
        
        double julianInt = toJulian(year, month, day);
        double julianFrac = injulian - julianInt;
        double hourss = 12 + julianFrac * 24.0;
        if (hourss >= 24.0) {
            hourss -= 24.0;
            LocalDate date = LocalDate.of(year, month, day);
            date = date.plusDays(1);
            year = date.getYear();
            month = date.getMonthValue();
            day = date.getDayOfMonth();
        }
        int hours = (int) hourss;
        int minutes = (int) ((hourss - hours) * 60.0);
        double seconds = (hourss - hours - minutes / 60.0) * 3600.0;
        
        return new double[] { year, month, day, hours, minutes, seconds };
    }
    
    public static String formatJulian(double injulian) {
        double[] res = fromJulian(injulian);
        return (int) res[0] + "-" + (res[1] < 10 ? "0" : "") + (int) res[1] + "-" + (res[2] < 10 ? "0" : "") + (int) res[2] + " " 
                + (res[3] < 10 ? "0" : "") + (int) res[3] + ":" + (res[4] < 10 ? "0" : "") + (int) res[4] + ":" + (res[5] < 10 ? "0" : "") + res[5]; 
    }
    
    public static String formatJulianRoundSeconds(double injulian, int decimals) {
        double[] res = fromJulianRoundSeconds(injulian, decimals);
        String seconds = decimals <= 0 ? String.format("%02.0f", res[5]) : String.format("%0"+(decimals + 3) + "." + decimals+"f", res[5]);
        return (int) res[0] + "-" + (res[1] < 10 ? "0" : "") + (int) res[1] + "-" + (res[2] < 10 ? "0" : "") + (int) res[2] + " " 
                + (res[3] < 10 ? "0" : "") + (int) res[3] + ":" + (res[4] < 10 ? "0" : "") + (int) res[4] + ":" + seconds; 
    }

    public static String formatJulianRoundSeconds(double injulian, int decimals, double timeOffsetHours) {
        String dateTime = formatJulianRoundSeconds(injulian + timeOffsetHours / 24.0, decimals);
        ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds((int) (timeOffsetHours * 3600.0));
        return dateTime + " " + zoneOffset.toString();
    }

    /**
     * Format and when seconds miss the fraction part then return without it as "03". When frac. part present return as "03.234" 
     * @param injulian
     * @param decimals
     * @return
     */
    public static String formatJulianRoundSecondsSkipFrac(double injulian, int decimals) {
        double[] res = fromJulianRoundSeconds(injulian, decimals);
        double ro = Math.floor(res[5]);
        String seconds = decimals <= 0 || ro == res[5] ? String.format("%02.0f", res[5]) : String.format("%0"+(decimals + 3) + "." + decimals+"f", res[5]);
        return (int) res[0] + "-" + (res[1] < 10 ? "0" : "") + (int) res[1] + "-" + (res[2] < 10 ? "0" : "") + (int) res[2] + " " 
                + (res[3] < 10 ? "0" : "") + (int) res[3] + ":" + (res[4] < 10 ? "0" : "") + (int) res[4] + ":" + seconds; 
    }
    
    /**
     * Format using only time and when seconds miss the fraction part then return without it as "03". When frac. part present return as "03.234" 
     * @param injulian
     * @param decimals
     * @return
     */
    public static String formatJulianTimeRoundSecondsSkipFrac(double injulian, int decimals) {
        double[] res = fromJulianRoundSeconds(injulian, decimals);
        double ro = Math.floor(res[5]);
        String seconds = decimals <= 0 || ro == res[5] ? String.format("%02.0f", res[5]) : String.format("%0"+(decimals + 3) + "." + decimals+"f", res[5]);
        return (res[3] < 10 ? "0" : "") + (int) res[3] + ":" + (res[4] < 10 ? "0" : "") + (int) res[4] + ":" + seconds; 
    }

    public static String formatJulianTimeRoundSecondsSkipFrac(double injulian, int decimals, double timeOffsetHours) {
        String time = formatJulianTimeRoundSecondsSkipFrac(injulian + timeOffsetHours / 24.0, decimals);
        ZoneOffset zoneOffset = ZoneOffset.ofTotalSeconds((int) (timeOffsetHours * 3600.0));
        return time + " " + zoneOffset.toString();
    }
    
    public static String formatJulianRoundSeconds(double injulian) {
        return formatJulianRoundSeconds(injulian, 0); 
    }
    
    /**
     * Parse Gregorian date to julian  
     * @param str Gregorian date using format "yyyy-MM-dd[ HH:mm[:ss[.SSS]]]", like 2010-10-30 14:33:12.123 or 2010-Oct-30, etc...
     * @return
     */
    public static double parseToJulian(String str) {
        str = str.trim();
        if (str.equals("")) {
            throw new IllegalStateException("Date mustn't be empty");
        }
        String[] split = str.split("\\s");
        String date = split[0];
        String[] dateParts = date.split("-");
        if (dateParts.length != 3) {
            throw new IllegalStateException("Date " + date + " is invalid");
        }
        if (dateParts[0].length() != 4) {
            throw new IllegalStateException("Wrong year " + dateParts[0] + " in date " + date);
        }
        
        int year;
        try {
            year = Integer.parseInt(dateParts[0]);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Wrong year " + dateParts[0] + " in date " + date + " - nondigit character present");
        }
        
        if (dateParts[1].length() > 3 || dateParts[1].length() < 1) {
            throw new IllegalStateException("Wrong month " + dateParts[1] + " in date " + date);
        }
        
        int month;
        
        int indexOf = MONTH_NAMES.indexOf(dateParts[1].toLowerCase());
        if (indexOf != -1) {
            month = indexOf + 1;
        } else {
            try {
                 month = Integer.parseInt(dateParts[1]);
            } catch (NumberFormatException e) {
                throw new IllegalStateException("Wrong month " + dateParts[1] + " in date " + date + " - nondigit character present or not a name Jan-Dec");
            }
        }
        if (month > 12 || month < 1) {
            throw new IllegalStateException("Wrong month number " + month);
        }
        
        if (dateParts[2].length() > 2 || dateParts[2].length() < 1) {
            throw new IllegalStateException("Wrong day " + dateParts[2] + " in date " + date);
        }
        
        int day;
        try {
            day = Integer.parseInt(dateParts[2]);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Wrong day " + dateParts[2] + " in date " + date + " - nondigit character present");
        }
        if (day > 31 || day < 1) {
            throw new IllegalStateException("Wrong day number " + day);
        }
        
        if (split.length == 1) {
            return toJulian(year, month, day);
        }
        
        int hour = 0;
        int minute = 0;
        double second = 0;
        String timeStr = split[1].trim();
        String[] tms = timeStr.split(":");
        try {
            hour = Integer.parseInt(tms[0]);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Wrong hour " + tms[0] + " in time " + timeStr);
        }
        if (tms.length > 1) {
            try {
                minute = Integer.parseInt(tms[1]);
            } catch (NumberFormatException e) {
                throw new IllegalStateException("Wrong minute " + tms[1] + " in time " + timeStr);
            }
            if (tms.length > 2) {
                try {
                    second = Double.parseDouble(tms[2]);
                } catch (NumberFormatException e) {
                    throw new IllegalStateException("Wrong seconds " + tms[2] + " in time " + timeStr);
                }   
            }
        }
        
        return toJulianExactly(year, month, day, hour, minute, second);
    }

    /**
     * Convert to UT date time
     * @param jd
     * @return
     */
    public static LocalDateTime toUTCDateTime(double jd) {
        double[] doubles = JulianUtil.fromJulian(jd);
        int seconds = (int) doubles[5];
        int nanoSeconds = (int) ((doubles[5] - seconds) * 1E9);
        return LocalDateTime.of((int) doubles[0], (int) doubles[1], (int) doubles[2], (int) doubles[3], (int) doubles[4], seconds, nanoSeconds);
    }

    public static double toJulianDate(LocalDateTime utc) {
        return JulianUtil.toJulianExactly(utc.getYear(), utc.getMonthValue(), utc.getDayOfMonth(), utc.getHour(), utc.getMinute(), utc.getSecond() + utc.getNano() / 1e9);
    }

    /**
     * Convert to date time with given offset
     * @param jd
     * @param requiredOffset
     * @return
     */
    public static OffsetDateTime toOffsetDateTime(double jd, double requiredOffset) {
        OffsetDateTime utc = OffsetDateTime.of(toUTCDateTime(jd), ZoneOffset.UTC);
        return utc.withOffsetSameInstant(ZoneOffset.ofTotalSeconds((int) (requiredOffset * 3600)));
    }

    public static double toJulianDate(OffsetDateTime offsetDateTime) {
        OffsetDateTime utc = offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC);
        return JulianUtil.toJulianExactly(utc.getYear(), utc.getMonthValue(), utc.getDayOfMonth(), utc.getHour(), utc.getMinute(), utc.getSecond() + utc.getNano() / 1e9);
    }

    public static double julianDateNow() {
        return toJulianDate(OffsetDateTime.now());
    }

    public static double julianDateJ2000() {
        return toJulianExactly(2000, 1, 1, 12, 0, 0);
    }

    public static double julianDateB1950() {
        return 2433282.4235;
    }

    /**
     * https://en.wikipedia.org/wiki/Epoch_(astronomy)
     */
    public static double toEpochYear(double jd) {
        return 2000 + (jd - 2451545.0) / 365.25;
    }

    public static void main(String args[]) {
        double julian = toJulianExactly(2021, 11, 17, 3, 59, 4.5);
        String formatJulian = formatJulianRoundSeconds(julian);
        System.out.println(formatJulian);
        
        // THIRD TEST
        double date1 = toJulian(2005, 1, 1);
        double date2 = toJulian(2005, 1, 31);
        System.out.println("Between 2005-01-01 and 2005-01-31 : " + (date2 - date1) + " days");

        /*
         * expected output : Julian date for May 23, 1968 : 2440000.0 ... back to
         * calendar 1968 5 23 Julian date for today : 2453487.0 ... back to calendar
         * 2005 4 26 Between 2005-01-01 and 2005-01-31 : 30.0 days
         */

        System.out.println("Julian: " + toJulianExactly(2021, 1, 19, 11, 0, 0.0));
    }
}
