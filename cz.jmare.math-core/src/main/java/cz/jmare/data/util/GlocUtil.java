package cz.jmare.data.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import cz.jmare.math.geo.GeoLoc;

/**
 * Geo location
 */
public class GlocUtil {
    public static final List<String> CHARS = List.of("E", "W", "N", "S");
    
    public static final List<String> CHANGE_SIGN_CHARS = List.of("W", "S");
    
    public static final List<String> LON_CHARS = List.of("W", "E");
    
    public static final double EARTH_R = 6378000; // m

    public static GeoLoc parseCoordinatesNormalized(String str) {
        GeoLoc geoLoc = parseCoordinates(str);
        geoLoc.latitude = GonUtil.normalizeDegMinus180180(geoLoc.latitude);
        geoLoc.latitude = GonUtil.roundDegrees(geoLoc.latitude);
        geoLoc.longitude = GonUtil.roundDegrees(geoLoc.longitude);
        if (geoLoc.latitude > 90 || geoLoc.latitude < -90) {
            throw new IllegalArgumentException("Latitude not in range -90 to +90 degrees");
        }
        return geoLoc;
    }
    
    public static GeoLoc parseCoordinates(String str) {
        str = str.trim();
        String[] split = str.split("[;,]");
        if (split.length > 2) {
            throw new IllegalArgumentException("Coordinates have too much separators");
        }
        Double longitude = null, latitude = null;
        if (split.length == 2) {
            String t1 = split[0].trim();
            String t2 = split[1].trim();
            if (t1.length() == 0 || t2.length() == 0) {
                throw new IllegalArgumentException("Separator doesn't separate coordinates");
            }
            
            int pos1 = charPos(t1);
            Double angle = parseAngle(t1, pos1);
            if (pos1 >= 0) {
                String ch = String.valueOf(t1.charAt(pos1)).toUpperCase();
                if (LON_CHARS.contains(ch)) {
                    longitude = angle;
                } else {
                    latitude = angle;
                }
            } else {
                latitude = angle;
            }
            

            int pos2 = charPos(t2);
            angle = parseAngle(t2, pos2);
            if (pos2 >= 0) {
                String ch = String.valueOf(t2.charAt(pos2)).toUpperCase();
                if (LON_CHARS.contains(ch)) {
                    if (longitude != null) {
                        throw new IllegalArgumentException("Both coordinates are longitudes");
                    }
                    longitude = angle;
                } else {
                    if (latitude != null) {
                        throw new IllegalArgumentException("Both coordinates are latitudes");
                    }
                    latitude = angle;
                }
            } else {
                longitude = angle;
            }
        } else {
            int pos1 = charPos(str);
            if (pos1 == -1) {
                split = str.split("\\s+");
                
                String t1 = split[0].trim();
                latitude = parseAngle(t1, -1);
                
                String t2 = split[1].trim();
                longitude = parseAngle(t2, -1);
            } else {
                String t1 = str.substring(0, pos1 + 1);
                String t2Sig = str.substring(pos1 + 1);
                int pos2 = charPos(t2Sig);
                if (pos2 == -1) {
                    throw new IllegalArgumentException("Wrong second coordinate");
                }
                Double angle = parseAngle(t1, pos1);

                String ch = String.valueOf(t1.charAt(pos1)).toUpperCase();
                if (LON_CHARS.contains(ch)) {
                    longitude = angle;
                } else {
                    latitude = angle;
                }
                
                String t2 = t2Sig.substring(0, pos2 + 1);
                angle = parseAngle(t2, pos2);
                ch = String.valueOf(t2.charAt(pos2)).toUpperCase();
                if (LON_CHARS.contains(ch)) {
                    if (longitude != null) {
                        throw new IllegalArgumentException("Both coordinates are longitudes");
                    }
                    longitude = angle;
                } else {
                    if (latitude != null) {
                        throw new IllegalArgumentException("Both coordinates are latitudes");
                    }
                    latitude = angle;
                }
            }
        }
        
        return new GeoLoc(latitude, longitude);
    }

    private static Double parseAngle(String str, int charPos) {
        Double angle;
        if (charPos != -1) {
            String str1 = str.substring(0, charPos);
            try {
                angle = GonUtil.parseDegrees(str1);
            } catch (Exception e) {
                angle = GonUtil.parseDegreesDegSymbol(str1);
            }
            if (CHANGE_SIGN_CHARS.contains(String.valueOf(str.charAt(charPos)).toUpperCase())) {
                angle = -angle;
            }
        } else {
            try {
                angle = GonUtil.parseDegrees(str);
            } catch (Exception e) {
                angle = GonUtil.parseDegreesDegSymbol(str);
            }
        }
        return angle;
    }
    
    public static boolean isGeoCoordinate(String str) {
        try {
            int pos1 = charPos(str);
            Double angle = parseAngle(str, pos1);
            return angle >= -180 && angle <= 180;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static int charPos(String str) {
        for (int i = 0; i < str.length(); i++) {
        char charAt = str.charAt(i);
        if (Character.isAlphabetic(charAt) && CHARS.contains(String.valueOf(charAt).toUpperCase())) {
            return i;
        }
        }
        return -1;
    }

    public static String formatCoordsPublic(GeoLoc geoLoc) {
        return formatCoordinates(geoLoc, ", ");
    }
    
    public static String formatCoordsProfi(GeoLoc geoLoc) {
        return formatCoordinates(geoLoc, "  ");
    }
    
    public static String formatCoordinates(GeoLoc geoLoc, String separator) {
        return formatCoordinates(geoLoc, separator, 6);
    }
    
    public static String formatCoordinates(GeoLoc geoLoc, String separator, int decimals) {
        double latitude = geoLoc.latitude;
        String chLat = "N";
        if (latitude < 0) {
            latitude = -latitude;
            chLat = "S";
        }
        double longitude = geoLoc.longitude;
        String chLon = "E";
        if (longitude < 0) {
            longitude = -longitude;
            chLon = "W";
        }
        
        return toDecimalsString(latitude, decimals) + chLat + separator + toDecimalsString(longitude, decimals) + chLon;
    }
    
    
    public static String toDecimalsString(double value, int significantDigits) {
        if (significantDigits < 0) throw new IllegalArgumentException();
        significantDigits += value < 1 ? 0 : Math.ceil(Math.log10(value));

        BigDecimal bd = new BigDecimal(value, MathContext.DECIMAL64);
        bd = bd.round(new MathContext(significantDigits, RoundingMode.HALF_UP));
        final int precision = bd.precision();
        if (precision < significantDigits) {
            bd = bd.setScale(bd.scale() + (significantDigits - precision));
        }
        return bd.toPlainString();
    }
    
    public static double angularDistanceDeg(GeoLoc geo1, GeoLoc geo2) {
        double beta1Rad = Math.toRadians(geo1.latitude);
        double beta2Rad = Math.toRadians(geo2.latitude);
        double lambda1Rad = Math.toRadians(geo1.longitude);
        double lambda2Rad = Math.toRadians(geo2.longitude);
        
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        double distance = Math.acos(sum);
        
        return Math.toDegrees(distance);
    }
    
    public static double angularDistanceRad(double lambda1Rad, double beta1Rad, double lambda2Rad, double beta2Rad) {
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        return Math.acos(sum);
    }
    
    public static double surfaceDistanceMeters(GeoLoc geo1, GeoLoc geo2) {
        double angularDistanceDeg = angularDistanceDeg(geo1, geo2);
        double angularDistanceRad = Math.toRadians(angularDistanceDeg);
        return angularDistanceRad * EARTH_R;
    }
    
    public static double surfaceDistanceMeters(double angularDistanceDeg) {
        double angularDistanceRad = Math.toRadians(angularDistanceDeg);
        return angularDistanceRad * EARTH_R;
    }
    
    public static double angularDistanceDeg(double surfaceDistanceMeters) {
        double angularDistanceRad = surfaceDistanceMeters / EARTH_R;
        return Math.toDegrees(angularDistanceRad);
    }
    
    public static void main(String[] args) {
        System.out.println(toDecimalsString(0.00001, 3));
        GeoLoc coordinates = parseCoordinates("49\u00B048' 57\"S 12\u00B007'08\"W");
        System.out.println(formatCoordsPublic(coordinates));
    }
}
