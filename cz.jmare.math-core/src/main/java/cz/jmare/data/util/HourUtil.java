package cz.jmare.data.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Utility for hours
 */
public class HourUtil {
    /**
     * Return decimal hours
     * @param hoursStr
     * @return
     */
    public static double parseHours(String hoursStr) {
        hoursStr = hoursStr.trim();
        String[] split = hoursStr.split("[:\\s]");
        if (split.length > 3) {
            throw new IllegalArgumentException("Wrong or not supported format of " + hoursStr);
        }
        double hours = split.length > 1 ? Integer.parseInt(split[0].trim()) : Double.parseDouble(split[0].trim());
        if (hours >= 24.0) {
            throw new IllegalArgumentException("Max value for hours is 24 excluding (23 including)");
        }
        double minutes = 0;
        if (split.length > 1) {
            minutes = split.length > 2 ? Integer.parseInt(split[1].trim()) : Double.parseDouble(split[1].trim());
        }
        double seconds = 0;
        if (split.length > 2) {
            seconds = Double.parseDouble(split[2].trim());
        }
        return hours + minutes / 60.0 + seconds / 3600.0;
    }
    
    public static double parseHoursHms(String hoursStr) {
        hoursStr = hoursStr.trim();
        String[] split = hoursStr.split("h");
        if (split.length > 3) {
            throw new IllegalArgumentException("Wrong or not supported format of " + hoursStr);
        }
        double hours = split.length > 1 ? Integer.parseInt(split[0].trim()) : Double.parseDouble(split[0].trim());
        if (hours >= 24.0) {
            throw new IllegalArgumentException("Max value for hours is 24 excluding (23 including)");
        }
        double minutes = 0;
        double seconds = 0;
        if (split.length > 1) {
            String[] split2 = split[1].trim().split("m");
            if (split2.length != 2 && split2.length != 1) {
                throw new IllegalArgumentException("Wrong or not supported format of " + hoursStr);
            }
            minutes = split2.length == 2 ? Integer.parseInt(split2[0].trim()) : Double.parseDouble(split2[0].trim());
            if (split2.length == 2) {
                split2[1] = split2[1].trim();
                if (split2[1].endsWith("s")) {
                    split2[1] = split2[1].substring(0, split2[1].length() - 1);
                }
                seconds = Double.parseDouble(split2[1]);
            }
        }
        return hours + minutes / 60.0 + seconds / 3600.0;
    }
    
    public static String formatHours(double hourss) {
        return formatHours(hourss, ":", 2);
    }
    
    // TODO: implement 10 decimals
    public static String formatHoursProfi(double hourss) {
        return formatHours(hourss, ":", 10);
    }
    
    // probably bug when seconds >= 60 (not adding day)
    public static String formatHours(double hourss, String separator, int decimalPlaces) {
        if (Double.isNaN(hourss)) {
            return "";
        }
        int hours = (int) hourss;
        int minutes = (int) ((hourss - hours) * 60.0);
        double seconds = (hourss - hours - minutes / 60.0) * 3600.0;
        seconds = round(seconds, decimalPlaces);
//        int temp = (int)(seconds * 100.0);
//        double secondsCut = ((double) temp) / 100.0;
        if (seconds >= 60) {
            seconds -= 60;
            minutes++;
        }
        
        if (minutes >= 60) {
            minutes -= 60;
            hours++;
        }
        
        if (hours >= 24) {
            hours -= 24;
        }
        
        String secondsCut = String.valueOf(seconds);
        int pointIndex = secondsCut.indexOf(".");
        if (pointIndex == -1) {
            secondsCut += ".00";
        } else {
            if (secondsCut.length() - pointIndex <= 2) {
                secondsCut += "0";
            }
        }
        return (hours < 10 ? "0" : "") + hours + separator + (minutes < 10 ? "0" : "") + minutes + separator + (seconds < 10 ? "0" : "") + secondsCut;
    }

    public static String formatHoursMinutes(double hourss, String separator) {
        if (Double.isNaN(hourss)) {
            return "";
        }
        int hours = (int) hourss;
        double minutes = ((hourss - hours) * 60.0);

        BigDecimal bd = new BigDecimal(minutes);
        bd = bd.setScale((int) (double) 1, RoundingMode.HALF_UP);
        minutes = bd.doubleValue();

        if (minutes >= 60) {
            minutes -= 60;
            hours++;
        }

        if (hours >= 24) {
            hours -= 24;
        }

        return (hours < 10 ? "0" : "") + hours + separator + (minutes < 10 ? "0" : "") + minutes;
    }
    
    private static double round(double number, int scale) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(scale, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void main(String[] args) {
        String s = formatHoursMinutes(12.444, ":");
        System.out.println(s);
    }
}
