package cz.jmare.data.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.math.astro.AzAlt;
import cz.jmare.math.astro.RaDec;
import cz.jmare.math.astro.RahDec;

/**
 * Coordinates util for astronomy
 */
public class ACooUtil {
    private final static Pattern SPACES_SEP = Pattern.compile("\\s+");
    
    private final static char[] MINUS_CHARS = {'\u2212', '\uFE63', '\u2013'};

    /**
     * Parses coordinates RA and DEC
     * @param str support all variants like Simbad, see http://simbad.harvard.edu/simbad/sim-fcoo
     *        note a comma or space may be present between RA and DEC. RA supposed to be in hours
     *        unless a degrees symbol present
     * @return RaDec instance which contains RA in degrees and DEC in degrees
     */
    public static RaDec parseCoords(String str) {
        return parseCoords(str, false);
    }
    
    public static RahDec parseHCoords(String str) {
        RaDec raDec = parseCoords(str, false);
        return raDec.toRahDec();
    }
    
    public static RaDec parseCoordsNormalized(String str, Boolean raInDegrees) {
        RaDec raDec = parseCoords(str, raInDegrees);
        raDec.ra = GonUtil.normalizeDegZero360(raDec.ra);
//        raDec.ra = GonUtil.roundDegrees(raDec.ra);
//        raDec.dec = GonUtil.roundDegrees(raDec.dec);
        if (raDec.dec > 90 || raDec.dec < -90) {
            throw new IllegalArgumentException("Declination not in range -90 to +90 degrees");
        }
        return raDec;
    }
    
    /**
     * Parses coordinates RA and DEC
     * @param str support all variants like Simbad, see http://simbad.harvard.edu/simbad/sim-fcoo
     *        note a comma or space may be present between RA and DEC
     * @param raInDegrees when true - RA coordinates are in degrees, otherwise by default in hours
     *        It can be ignored when coordinates for RA contains units like 'd'
     * @return RaDec instance which contains RA in degrees and DEC in degrees
     */
    public static RaDec parseCoords(String str, Boolean raInDegrees) {
        double[] coords = parseCoordinates(str, raInDegrees);
        return new RaDec(coords[0], coords[1]);
    }

    /**
     * Parses 2 coordinates which can be separated by comma or space. Returning both coordinates in degrees
     * @param str support all variants like Simbad, see http://simbad.harvard.edu/simbad/sim-fcoo
     *        note a comma or space may be present between RA and DEC
     * @param firstCooInDegrees when true - first coordinate in degrees, otherwise by default in hours
     *        It can be ignored when coordinates for first coordinate contains units like 'd'
     * @return double[] with 2 values in degrees
     */
    private static double[] parseCoordinates(String str, Boolean firstCooInDegrees) {
        str = normalizeMinuses(str);
        int index = findDecBeginIndex(str);
        if (index == -1) {
            index = str.indexOf(",");
        }
        if (index == -1) {
            throw new IllegalArgumentException("Unable to distinguish RA and DEC value in " + str);
        }
        String raString = str.substring(0, index).trim();
        if (raString.endsWith(",")) {
            raString = raString.substring(0, raString.length() - 1);
        }
        String decString = str.substring(index).trim();
        if (decString.startsWith(",")) {
            decString = decString.substring(1);
        }
        if (raString.equals("")) {
            throw new IllegalArgumentException("Unable to recognize RA in" + str);
        }
        if (decString.equals("")) {
            throw new IllegalArgumentException("Unable to recognize DEC in" + str);
        }
        double ra = 0;
        double dec = 0;

        // -- RA --
        if (raString.contains("h") || raString.contains("H")) {
            ra = ACooUtil.hoursToDegrees(HourUtil.parseHoursHms(raString));
        } else {
            if (firstCooInDegrees == null && (raString.contains("D") || raString.contains("d") || raString.contains("\u00B0"))) {
                firstCooInDegrees = true;
            }
            if (firstCooInDegrees == null) {
                throw new IllegalArgumentException("Unable to recognize units H/DEG for RA" + ra);
            }
            if (firstCooInDegrees) {
                try {
                    ra = GonUtil.parseDegrees(raString);
                } catch (Exception e) {
                    ra = GonUtil.parseDegreesDegSymbol(raString);
                }
            } else {
                double hours;
                try {
                    hours = HourUtil.parseHours(raString);
                } catch (Exception e) {
                    hours = HourUtil.parseHoursHms(raString);
                }
                ra = ACooUtil.hoursToDegrees(hours);
            }
        }

        // -- DEC --
        try {
            dec = GonUtil.parseDegrees(decString);
        } catch (Exception e) {
            dec = GonUtil.parseDegreesDegSymbol(decString);
        }

        return new double[] {ra, dec};
    }

    public static String formatCoordsPublic(RaDec raDec) {
    	return formatCoords(raDec, ":", ", ");
    }

    /**
     * Produces simbad, Nasa Horizons format...
     * @param raDec
     * @return
     */
    public static String formatCoordsProfi(RaDec raDec) {
        return HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra), " ", 10) + "  " + GonUtil.formatDegrees(raDec.dec, true, " ", 2, 10);
    }

    /**
     * Format RA, DEC coordinates. RA as hours. 2 places for seconds. Custom separator between RA and DEC. Custom separator for h,m,s or deg,m,s , e.g. <pre>05:16:41.36, +45:59:52.77</pre> 
     * @param raDec number to format
     * @param separator separator between hours, minutes and seconds and between degrees, minutes and seconds
     * @param raDecSeparator separator text between RA and DEC parts
     * @return formated string
     */
    public static String formatCoords(RaDec raDec, String separator, String raDecSeparator) {
    	return HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra), separator, 2) + raDecSeparator + GonUtil.formatDegrees(raDec.dec, true, separator, 2, 2);
    }
    
    private static int findDecBeginIndex(String str) {
        int index = findLastSignIndex(str);
        if (index == -1) {
            index = findSpaceBetween(str);
        }
        return index;
    }

    private static int findLastSignIndex(String str) {
        int indexOfSig = str.lastIndexOf("+");
        if (indexOfSig == 0 || indexOfSig == -1) {
            indexOfSig = str.lastIndexOf("-");
            if (indexOfSig == 0 || indexOfSig == -1) {
                return -1;
            }
        }
        return indexOfSig;
    }

    private static int findSpaceBetween(String str) {
        int numberOfWhitespaces = numberOfWhitespaces(str);
        if (numberOfWhitespaces % 2 == 0) {
            return -1;
        }
        return findPositionOfXSpace(str, numberOfWhitespaces / 2 + 1);
    }

    /**
     *
     * @param str
     * @param spaceIndex starting from 1
     * @return
     */
    private static int findPositionOfXSpace(String str, int spaceIndex) {
        int pos = 0;
        Matcher matcher = SPACES_SEP.matcher(str);
        while (matcher.find()) {
            pos++;
            if (pos == spaceIndex) {
                return matcher.start();
            }
        }
        throw new IllegalStateException("Expected to find " + spaceIndex + "x space but not found");
    }

    private static int numberOfWhitespaces(String str) {
        String[] split = str.split("\\s+");
        return split.length - 1;
    }
    
    static String normalizeMinuses(String str) {
        for (char c : MINUS_CHARS) {
            str = str.replace(c, '-');
        }
        return str;
    }
    
    public static String formatRaDec(RaDec raDec) {
        return HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra)) + "  " + GonUtil.formatDegrees(raDec.dec, true);
    }
    
    public static String formatRaDecProfi(RaDec raDec) {
        return HourUtil.formatHoursProfi(ACooUtil.degreesToHours(raDec.ra)) + "  " + GonUtil.formatDegrees(raDec.dec, true, ":", 2, 10);
    }

    public static String formatRaDecProfi(RaDec raDec, boolean usingRaInDegrees) {
        return (usingRaInDegrees ? GonUtil.formatDegrees(raDec.ra) : HourUtil.formatHoursProfi(ACooUtil.degreesToHours(raDec.ra))) + "  " + GonUtil.formatDegrees(raDec.dec, true, ":", 2, 10);
    }

    public static String formatRaDec(RaDec raDec, boolean usingRaInDegrees, boolean usingComma) {
        return (usingRaInDegrees ? GonUtil.formatDegrees(raDec.ra) : HourUtil.formatHours(ACooUtil.degreesToHours(raDec.ra))) + (usingComma ? ", " : " ") + GonUtil.formatDegrees(raDec.dec, true);
    }

    public static String formatAzAlt(AzAlt azAlt) {
        return formatAzAlt(azAlt, true);
    }

    public static String formatAzAlt(AzAlt azAlt, boolean usingComma) {
        return (GonUtil.formatDegrees(azAlt.az, false) + (usingComma ? ", " : " ") + GonUtil.formatDegrees(azAlt.alt, true));
    }

    public static AzAlt parseAzAltCoords(String str) {
        double[] doubles = parseCoordinates(str, true);
        return new AzAlt(doubles[0], doubles[1]);
    }

    public static double hoursToDegrees(double hours) {
        return hours * 15.0;
    }

    public static double degreesToHours(double degrees) {
        return degrees / 15.0;
    }

    public static double radToHours(double angleRad) {
        double hoursDecimal = 12 * angleRad / Math.PI;
        if (hoursDecimal >= 24.0) {
            hoursDecimal = hoursDecimal % 24;
        } else if (hoursDecimal < 0) {
            hoursDecimal = hoursDecimal % 24 + 24.0;
        }
        return hoursDecimal;
    }
    
    public static void main(String[] args) {
        RaDec parseCoords = parseCoords("    05 16 41.35871 +45 59 52.7693", false);
        System.out.println(formatCoords(parseCoords, ":", ", "));
    }

}
