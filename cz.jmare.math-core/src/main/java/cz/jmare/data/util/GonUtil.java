package cz.jmare.data.util;

import static java.lang.Math.PI;

import java.math.BigDecimal;
import java.math.RoundingMode;

import cz.jmare.math.astro.RaDec;

/**
 * Utility for coordinates with degrees
 */
public class GonUtil {
    public static final double TWO_PI = 2 * PI;

    /**
     * Normalize to interval 0 to 2 * PI
     * @param angleRad
     * @return
     */
    public static double normalizeRadZeroTwoPi(double angleRad) {
        angleRad %= TWO_PI;
        angleRad = (angleRad + TWO_PI) % TWO_PI;
        return angleRad;
    }

    /**
     * Normalize to interval -PI to PI
     * @param angleRad
     * @return
     */
    public static double normalizeRadMinusPiPi(double angleRad) {
        angleRad %= TWO_PI;
        angleRad = (angleRad + TWO_PI) % TWO_PI;
        if (angleRad > PI) {
            angleRad -= TWO_PI;
        }
        return angleRad;
    }

    /**
     * Normalize to interval 0 to 360 (excl.)
     * @param angleDeg
     * @return
     */
    public static double normalizeDegZero360(double angleDeg) {
        angleDeg %= 360.0;
        angleDeg = (angleDeg + 360.0) % 360.0;
        return angleDeg;
    }

    /**
     * Normalize to interval -180 to 180
     * @param angleDeg
     * @return
     */
    public static double normalizeDegMinus180180(double angleDeg) {
        angleDeg %= 360.0;
        angleDeg = (angleDeg + 360.0) % 360.0;
        if (angleDeg > 180.0) {
            angleDeg -= 360.0;
        }
        return angleDeg;
    }

    public static double angularDistanceDeg(RaDec raDec1, RaDec raDec2) {
        double beta1Rad = Math.toRadians(raDec1.dec);
        double beta2Rad = Math.toRadians(raDec2.dec);
        double lambda1Rad = Math.toRadians(raDec1.ra);
        double lambda2Rad = Math.toRadians(raDec2.ra);
        
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        double distance = Math.acos(sum);
        
        return Math.toDegrees(distance);
    }
    
    public static double angularDistanceRad(double lambda1Rad, double beta1Rad, double lambda2Rad, double beta2Rad) {
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        return Math.acos(sum);
    }
    
    /**
     * Format degrees with opening sign, ':' as separator, 2 decimal places, e.g. <pre>+8:07:24.60</pre> 
     * @param degrees number to format
     * @return formated string
     */
    public static String formatDegrees(double degrees) {
        return formatDegrees(degrees, true);
    }

    /**
     * Format degrees with ':' as separator, seconds with 2 decimal places, e.g. <pre>+8:07:24.60</pre> 
     * @param degrees number to format
     * @param withPlus when true then also opening '+' sign will be present
     * @return formated string
     */
    public static String formatDegrees(double degrees, boolean withPlus) {
    	return formatDegrees(degrees, withPlus, ":", 0, 2);
    }
    
    /**
     * Format degrees depending on arguments. Seconds round to 2 decimal places<br>
     * This is the most common formating method
     * @param degrees degrees to format
     * @param withPlus when true also + character will be prepended
     * @param separator character between degrees minutes and seconds
     * @param fillToDegreesPlaces fill degrees to number of places e.g. 2 useful for declination which has range +90 to -90, 3 for range 0 to 360 or -180 to 180
     * @param decimalPlaces number of places for seconds. When greater than 0 then rounding be used
     * @return
     */
    public static String formatDegrees(double degrees, boolean withPlus, String separator, int fillToDegreesPlaces, int decimalPlaces) {
        if (Double.isNaN(degrees)) {
            return "";
        }
        String sign = "";
        if (degrees < 0) {
            sign = "-";
            degrees = -degrees;
        } else {
            if (withPlus) {
                sign = "+";
            }
        }
        double totalSeconds = degrees * 3600;
        int degs = (int) degrees;
        totalSeconds -= degs * 3600;
        int minutes = (int) (totalSeconds / 60.0);
        totalSeconds -= minutes * 60;
        double seconds = totalSeconds;
        
        seconds = round(seconds, decimalPlaces);
        if (seconds >= 60) {
            seconds -= 60;
            minutes++;
        }

        if (minutes >= 60) {
            minutes -= 60;
            degs++;
        }

        String secondsCut = String.valueOf(seconds);
        int pointIndex = secondsCut.indexOf(".");
        if (pointIndex != -1) {
            int places = secondsCut.length() - pointIndex - 1;
            if (places < decimalPlaces) {
//                int number = decimalPlaces - places;
//                for (int i = 0; i < number; i++) {
//                    secondsCut += "0";
//                }
            } else if (places > decimalPlaces) {
                secondsCut = secondsCut.substring(0, secondsCut.length() - (places - decimalPlaces));
                if (secondsCut.endsWith(".")) {
                    secondsCut = secondsCut.substring(0, secondsCut.length() - 1);
                }
            }
        }

        String degsFormated = (fillToDegreesPlaces > 1 && degs < 10 ? "0" : "") + degs;
        if (fillToDegreesPlaces > 2 && degs < 100) {
            degsFormated = "0" + degsFormated;
        }
        return sign + degsFormated + separator + (minutes < 10 ? "0" : "") + minutes + separator + (seconds < 10 ? "0" : "") + secondsCut;
    }

    public static String formatDegreesMinutes(double degrees, boolean withPlus, String separator) {
        if (Double.isNaN(degrees)) {
            return "";
        }
        String sign = "";
        if (degrees < 0) {
            sign = "-";
            degrees = -degrees;
        } else {
            if (withPlus) {
                sign = "+";
            }
        }

        int degree = (int) degrees;
        double minutes = ((degrees - degree) * 60.0);

        BigDecimal bd = new BigDecimal(minutes);
        bd = bd.setScale((int) (double) 0, RoundingMode.HALF_UP);
        minutes = bd.doubleValue();

        if (minutes >= 60) {
            minutes -= 60;
            degree++;
        }

        if (degree >= 360) {
            degree -= 360;
        }


        return sign + (degree < 10 ? "0" : "") + degree + separator + (minutes < 10 ? "0" : "") + ((int) minutes);
    }

    /**
     * Useful to format angular distance
     * @param degrees
     * @return
     */
    public static String formatDegreesForLength(double degrees) {
        if (Double.isNaN(degrees)) {
            return "";
        }
        String sign = "";
        if (degrees < 0) {
            sign = "-";
            degrees = -degrees;
        }
        double totalSeconds = degrees * 3600;
        int degs = (int) degrees;
        totalSeconds -= degs * 3600;
        int minutes = (int) (totalSeconds / 60.0);
        totalSeconds -= minutes * 60;
        double seconds = totalSeconds;
        
        seconds = round(seconds, 2);
        if (seconds >= 60) {
            seconds -= 60;
            minutes++;
        }

        if (minutes >= 60) {
            minutes -= 60;
            degs++;
        }

        String secondsCut = String.valueOf(seconds);
        int pointIndex = secondsCut.indexOf(".");
        if (pointIndex == -1) {
            secondsCut += ".00";
        } else {
            if (secondsCut.length() - pointIndex <= 2) {
                secondsCut += "0";
            }
        }
        
        String str = secondsCut + "\"";
        if (minutes > 0) {
        	str = minutes + "'" + str;
        }
        if (degs > 0) {
        	str = degs + "\u00B0" + str;
        }
        str = sign + str;
        return str;
    }

    /**
     * Format radians with opening plus, ':' as separator, seconds with 2 decimal places, e.g. <pre>+8:07:24.60</pre> 
     * @param angleRad number to format
     * @return formated string
     */
    public static String formatRadAsDegrees(double angleRad) {
        return formatRadAsDegrees(angleRad, true);
    }

    /**
     * Format radians with ':' as separator, seconds with 2 decimal places, e.g. <pre>+8:07:24.60</pre> 
     * @param angleRad number to format
     * @param withPlus when true then also opening '+' sign will be present
     * @return formated string
     */
    public static String formatRadAsDegrees(double angleRad, boolean withPlus) {
        double degrees = Math.toDegrees(angleRad);
        return formatDegrees(degrees, withPlus);
    }

    /**
     * Convert string to decimal degrees. 
     * String must contain separator space or colon, it musn't be a degree character (unless after degrees real number, without minutes and seconds)
     *  or ' or " otherwise an exception throwed
     * @param degreesStr
     * @return
     */
    public static double parseDegrees(String degreesStr) {
        degreesStr = degreesStr.trim().replace('\u2212', '-').replace('\uFE63', '-');
        String[] split = degreesStr.split("[:\\s]");
        if (split.length > 3) {
            throw new IllegalArgumentException("Wrong or not supported format of " + degreesStr);
        }
        boolean minus = false;
        String degsString = split[0].trim();
        if (split.length == 1) {
            degsString = rtrimUntilNumber(degsString);
        }
        double degrees = split.length > 1 ? Integer.parseInt(degsString) : Double.parseDouble(degsString);
        if (degrees < 0) {
            minus = true;
            degrees = -degrees;
        }
        if (degreesStr.startsWith("-")) {
        	minus = true;
        }
        double minutes = 0;
        if (split.length > 1) {
            minutes = split.length > 2 ? Integer.parseInt(split[1].trim()) : Double.parseDouble(split[1].trim());
        }
        double seconds = 0;
        if (split.length == 3) {
            seconds = Double.parseDouble(split[2].trim());
        }
        double res = degrees + minutes / 60.0 + seconds / 3600.0;
        if (minus) {
            res = -res;
        }
        return res;
    }
    
    private static String rtrimUntilNumber(String str) {
        if (str == null) {
            return null;
        }
        int pos = str.length();
        while (--pos >= 0) {
            if (!Character.isDigit(str.charAt(pos)) && str.charAt(pos) != '.') {
                return str.substring(0, pos);
            }
        }
        return str;
    }
    
    /**
     * Parse a degrees string which contains a degree chracter and ' and "
     * @param degreesStr
     * @return
     */
    public static double parseDegreesDegSymbol(String degreesStr) {
        degreesStr = degreesStr.trim().replace('\u2212', '-').replace('\uFE63', '-');
        String[] split = degreesStr.split("[\u00B0d\u00BA]");
        if (split.length != 2 && split.length != 1) {
            throw new IllegalArgumentException("Wrong or not supported format of " + degreesStr);
        }
        double degrees = split.length > 1 ? Integer.parseInt(split[0].trim()) : Double.parseDouble(split[0].trim());
        boolean minus = false;
        if (degrees < 0) {
            minus = true;
            degrees = -degrees;
        }
        if (degreesStr.startsWith("-")) {
        	minus = true;
        }
        double minutes = 0;
        double seconds = 0;
        if (split.length > 1) {
            String[] split2 = split[1].trim().split("[\u2018\u2019'm\u2032\u201B\u00b4]");
            if (split2.length != 2 && split2.length != 1) {
                throw new IllegalArgumentException("Wrong or not supported format of " + degreesStr);
            }
            minutes = split2.length == 2 ? Integer.parseInt(split2[0].trim()) : Double.parseDouble(split2[0].trim());
            if (split2.length == 2) {
                String lastPart = split2[1].trim();
                if (lastPart.equals("")) {
                    throw new IllegalArgumentException("Wrong or not supported format of " + degreesStr);
                }
                while (!Character.isDigit(lastPart.charAt(lastPart.length() - 1))) {
                    lastPart = lastPart.substring(0, lastPart.length() - 1);
                }
                seconds = Double.parseDouble(lastPart);
            }
        }

        double res = degrees + minutes / 60.0 + seconds / 3600.0;
        if (minus) {
            res = -res;
        }
        return res;
    }

    public static double roundDegrees(double degrees) {
        BigDecimal bd = new BigDecimal(degrees);
        bd = bd.setScale(7, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    private static double round(double number, int scale) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(scale, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public static void main(String[] args) {
        String s = formatDegreesMinutes(88.3333, true, ":");
        System.out.println(s);
        double parseDegrees = parseDegrees("01:02:03.0");
        System.out.println(formatDegrees(parseDegrees, true, ":", 3, 6));
    }

    public static double parseAngleCoord(String angleCoordStr, Boolean inDegrees) {
    	angleCoordStr = ACooUtil.normalizeMinuses(angleCoordStr);
        double angle = 0;
        
        if (inDegrees == null && angleCoordStr.contains("h") || angleCoordStr.contains("H")) {
            angle = ACooUtil.hoursToDegrees(HourUtil.parseHoursHms(angleCoordStr));
        } else {
            if (inDegrees == null && (angleCoordStr.contains("D") || angleCoordStr.contains("d") || angleCoordStr.contains("\u00B0"))) {
                inDegrees = true;
            }
            if (inDegrees == null) {
                throw new IllegalArgumentException("Unable to recognize units H/DEG " + angleCoordStr);
            }
            if (inDegrees) {
                try {
                    angle = parseDegrees(angleCoordStr);
                } catch (Exception e) {
                    angle = parseDegreesDegSymbol(angleCoordStr);
                }
            } else {
                double hours;
                try {
                    hours = HourUtil.parseHours(angleCoordStr);
                } catch (Exception e) {
                    hours = HourUtil.parseHoursHms(angleCoordStr);
                }
                angle = ACooUtil.hoursToDegrees(hours);
            }
        }
    
        return angle;
    }
}
