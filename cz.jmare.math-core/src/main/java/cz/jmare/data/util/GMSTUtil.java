package cz.jmare.data.util;

public class GMSTUtil {
    public static double calcGmstHoursSimple(double jd) {
        double gmst = 18.697374558 + 24.06570982441908 * (jd - 2451545);
        return gmst % 24;
    }

    public static double toLmstHours(double gmst, double longitude) {
        return gmst + longitude / 15.0;
    }

    public static void main(String[] args) {
        double jd = JulianUtil.julianDateNow();
        double h = calcGmstHoursSimple(jd);
        String hStr = HourUtil.formatHours(h);
        System.out.println(hStr);

        double lmst = toLmstHours(h, (14 + 26 / 60.0));
        System.out.println(HourUtil.formatHours(lmst));

        double polaris = lmst - HourUtil.parseHours("03:03:16.79");
        System.out.println(polaris);

        System.out.println(HourUtil.formatHours(polaris)); // polar clock 6:32
    }
}
