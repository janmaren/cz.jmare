package cz.jmare.upath;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jcifs.CIFSContext;
import jcifs.Configuration;
import jcifs.config.PropertyConfiguration;
import jcifs.config.Reconfigure;
import jcifs.context.SingletonContext;
import jcifs.smb.NtlmPasswordAuthenticator;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

public class SmbUPath implements UPath {
    private static final int DEFAULT_BUFFER_SIZE = 8192;

    private SmbFile smbFile;
    private String uri;

    private static Pattern CREDS_PATTERN = Pattern.compile("^smb://([^:]+:[^@]+)@");

    public SmbUPath(String uri) throws InvalidPathException {
        this.uri = uri;
        if (uri.startsWith("smb:/") && !uri.startsWith("smb://")) {
            uri = uri.substring(0, 5) + "/" + uri.substring(5);
        }
        try {
            smbFile = new SmbFile(uri, getCIFSContext(uri));
        } catch (MalformedURLException e) {
            new InvalidPathException(uri, "Not valid samba URI");
        }
    }

    public static CIFSContext getCIFSContext(String uri) {
        Matcher matcher = CREDS_PATTERN.matcher(uri);
        if (matcher.find()) {
            String[] split = matcher.group(1).split(":");
            CIFSContext context = SingletonContext.getInstance();
            context = context.withCredentials(new NtlmPasswordAuthenticator(split[0], split[1]));
            return context;
        } else {
            CIFSContext context = SingletonContext.getInstance();
            Configuration config = context.getConfig();
            Reconfigure.reconfigure((PropertyConfiguration) config);
            context = context.withAnonymousCredentials();
            return context;
        }
    }

    public SmbUPath(Path path) {
        this(path.toString());
    }

    @Override
    public boolean isDirectory() {
        try {
            return smbFile.isDirectory();
        } catch (SmbException e) {
            return false;
        }
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        try (OutputStream os = smbFile.getOutputStream();
                InputStream is = new ByteArrayInputStream(bytes);) {
            copyLarge(is, os);
        }
    }

    public static long copyLarge(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    @Override
    public void delete() throws IOException {
        smbFile.delete();
    }

    @Override
    public InputStream createInputStream() throws IOException {
        return smbFile.getInputStream();
    }

    @Override
    public OutputStream createOutputStream() throws IOException {
        return smbFile.getOutputStream();
    }

    @Override
    public void renameTo(String uri) throws IOException {
        CIFSContext context = SingletonContext.getInstance();
        context = context.withAnonymousCredentials();
        SmbFile dest = new SmbFile(uri, context);
        smbFile.renameTo(dest);
    }

    @Override
    public String toString() {
        return uri.toString();
    }

    @Override
    public boolean isFile() {
        try {
            return smbFile.isFile();
        } catch (SmbException e) {
            return false;
        }
    }

    @Override
    public void createDirectories() throws IOException {
        try {
            smbFile.mkdirs();
        } catch (SmbException e) {
            if (e.getMessage() != null && e.getMessage().contains("already exist")) {
                return;
            }
            throw e;
        }
    }

    @Override
    public long size() throws IOException {
        long contentLength = smbFile.getContentLengthLong();
        return contentLength;
    }

    @Override
    public void setCreationTime(long millis) throws IOException {
        smbFile.setCreateTime(millis);
    }

    @Override
    public void setModificationTime(long millis) throws IOException {
        smbFile.setLastModified(millis);
    }

    @Override
    public long getCreationTime() throws IOException {
        return smbFile.createTime();
    }

    @Override
    public long getModificationTime() throws IOException {
        return smbFile.getLastModified();
    }

    public static void main(String[] args) {
        //createCreds("smb://jmeno:heslo@host/neco");
    }
}
