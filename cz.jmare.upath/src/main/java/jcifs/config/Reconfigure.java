package jcifs.config;

import jcifs.DialectVersion;

public class Reconfigure {
    public static void reconfigure(PropertyConfiguration config) {
        config.maxVersion = DialectVersion.SMB1;
    }
}
