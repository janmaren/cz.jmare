package cz.jmare.file;

import java.io.File;
import java.util.function.Supplier;

import cz.jmare.args.Coalesce;
import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;

public class FindAppUtil {
    final static String[] WINDOWS_SUFFIXES = {".exe", ".cmd"};
    final static String[] WINDOWS_SUFFIXES_SEC = {".bat", ".com"};

    public static String locateAppPath(String appName) {
        String path = System.getenv("PATH");
        if (path == null) {
            return appName;
        }
        if (new File(appName).isAbsolute()) {
            return appName;
        }
        if (AppHelper.getPlatform() == PlatformType.WINDOWS) {
            return Coalesce.coalesce(findWinApp(appName, path, WINDOWS_SUFFIXES), findWinApp(appName, path, WINDOWS_SUFFIXES_SEC));
        } else if (AppHelper.getPlatform() == PlatformType.LINUX) {
            String[] paths = path.split(":");
            for (String dir : paths) {
                File file = new File(dir, appName);
                if (file.isFile() && file.canExecute()) {
                    return file.toString();
                }
            }
        }
        return null;
    }

    private static Supplier<String> findWinApp(String appName, String path, String[] suffixes) {
        String[] paths = path.split(";");
        for (String dir : paths) {
            dir = dir.trim();
            for (String suff : suffixes) {
                File file = new File(dir, appName + suff);
                if (file.isFile()) {
                    return () -> file.toString();
                }
            }

            File file = new File(dir, appName);
            if (file.isFile()) {
                return () -> file.toString();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        String locateAppPath = locateAppPath("tailflog.sh");
        System.out.println(locateAppPath);
    }
}
