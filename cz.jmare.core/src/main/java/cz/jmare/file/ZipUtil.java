package cz.jmare.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipUtil {
    /**
     * Return list of files which match given criteria or null when no such directory exist
     * @param zipFilename
     * @param directory directory in zip or "" for root directory
     * @param suffix
     * @return note: for directory "" it never returns null because root directory always exists
     * @throws IOException when zip file doesn't exist
     */
    public static List<String> listZip(String zipFilename, String directory, String suffix) throws IOException {
        if (directory.startsWith("/")) {
            directory = directory.substring(1);
        }
        if (!suffix.startsWith(".")) {
            suffix = "." + suffix;
        }

        if ("".equals(directory)) {
            final String filterSuffix = suffix;
            Predicate<ZipEntry> predicate = new Predicate<ZipEntry>() {
                @Override
                public boolean test(ZipEntry zipEntry) {
                    if (zipEntry.isDirectory()) {
                        return false;
                    }
                    String name = zipEntry.getName();
                    if (name.contains("/")) {
                        return false;
                    }
                    if (!endsWithIgnoreCase(name, filterSuffix)) {
                        return false;
                    }
                    return true;
                }
            };
            return listZip(zipFilename, predicate);
        } else {
            if (!directory.endsWith("/")) {
                directory += "/";
            }
            DetectDirExistsPredicate detectDirExistsPredicate = new DetectDirExistsPredicate(directory, suffix);
            List<String> listZip = listZip(zipFilename, detectDirExistsPredicate);
            if (listZip.isEmpty() && !detectDirExistsPredicate.dirExists) {
                return null;
            }
            return listZip;
        }
    }

    /**
     * Return matching file names of zip
     * @param zipFilename
     * @param predicate
     * @return
     * @throws IOException
     */
    public static List<String> listZip(String zipFilename, Predicate<ZipEntry> predicate) throws IOException {
        List<String> listing = new ArrayList<String>();

        try (ZipFile zipFile = new ZipFile(zipFilename)) {
            Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
            while (zipEntries.hasMoreElements()) {
                ZipEntry zipEntry = zipEntries.nextElement();
                if (predicate.test(zipEntry)) {
                    listing.add(zipEntry.getName());
                }
            }
        }

        return listing;
    }

    public static void doInZip(String zipFilename, final List<String> filesInZip, BiFunction<ZipFile, ZipEntry, Boolean> consumer) throws IOException {
        Predicate<ZipEntry> predicate = new Predicate<ZipEntry>(){
            @Override
            public boolean test(ZipEntry zipEntry) {
                return filesInZip.contains(zipEntry.getName());
            }};
        doInZip(zipFilename, predicate, consumer);
    }

    /**
     * Consume zipFile and zipEntries which are valid for predicate. When consumer returns false then do not continue processing next entry
     * @param zipFilename
     * @param predicate
     * @param consumer
     * @throws IOException
     */
    public static void doInZip(String zipFilename, Predicate<ZipEntry> predicate, BiFunction<ZipFile, ZipEntry, Boolean> consumer) throws IOException {
        try (ZipFile zipFile = new ZipFile(zipFilename)) {
            Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
            while (zipEntries.hasMoreElements()) {
                ZipEntry zipEntry = zipEntries.nextElement();
                if (predicate.test(zipEntry)) {
                    if (!consumer.apply(zipFile, zipEntry)) {
                        break;
                    }
                }
            }
        }
    }

    public static class DetectDirExistsPredicate implements Predicate<ZipEntry> {
        private boolean dirExists;

        private String dir;

        private String suffix;

        public DetectDirExistsPredicate(String dir, String suffix) {
            this.dir = dir;
            this.suffix = suffix;
        }

        @Override
        public boolean test(ZipEntry zipEntry) {
            if (zipEntry.isDirectory()) {
                if (zipEntry.getName().startsWith(dir)) {
                    dirExists = true;
                }
                return false;
            }
            String name = zipEntry.getName();
            if (!name.startsWith(dir)) {
                return false;
            }
            if (name.indexOf("/", dir.length()) != -1) {
                return false; // there is a subdir
            }
            if (!endsWithIgnoreCase(name, suffix)) {
                return false;
            }
            return true;
        }

        public boolean dirExists() {
            return dirExists;
        }
    }

    public static boolean endsWithIgnoreCase(String str, String startsWith) {
        if (str.length() < startsWith.length()) {
            return false;
        }
        String strLowerCase = str.toLowerCase();
        String startsWithLowerCase = startsWith.toLowerCase();
        return strLowerCase.endsWith(startsWithLowerCase);
    }

    public static void dirToZip(Path toZipDirPath, Path pathname) throws IOException {
        dirToZip(toZipDirPath, false, pathname);
    }

    public static void dirToZip(Path toZipDirPath, boolean onlyContent, Path pathname) throws IOException {
        Path relateTo = toZipDirPath;
        if (!onlyContent) {
            relateTo = relateTo.getParent();
        }
        List<Path> pathsToZip = DirUtil.listEntities(toZipDirPath, 100, true);

        try (FileOutputStream fos = new FileOutputStream(pathname.toFile()); ZipOutputStream zos = new ZipOutputStream(fos)) {
            byte[] buffer = new byte[8192];
            int len;

            for (Path pathToZip : pathsToZip) {
                String zippath = relateTo.relativize(pathToZip).toString();
                ZipEntry zen = new ZipEntry(zippath);
                zos.putNextEntry(zen);
                try (FileInputStream fis = new FileInputStream(pathToZip.toFile())) {
                    while ((len = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    zos.closeEntry();
                }
            }
        }
    }

    public static void fileToZip(Path toZipFilePath, Path pathname) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(pathname.toFile()); ZipOutputStream zos = new ZipOutputStream(fos)) {
            byte[] buffer = new byte[8192];
            int len;

            ZipEntry zen = new ZipEntry(toZipFilePath.getFileName().toString());
            zos.putNextEntry(zen);
            try (FileInputStream fis = new FileInputStream(toZipFilePath.toFile())) {
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
            }
        }
    }
}
