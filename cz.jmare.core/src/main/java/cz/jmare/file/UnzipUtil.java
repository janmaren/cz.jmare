package cz.jmare.file;

import static java.nio.file.Files.copy;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.createDirectory;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.notExists;
import static java.nio.file.Files.walkFileTree;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

public class UnzipUtil {
    public static void unzipStreamAndClose(final InputStream input, final Path destDirectory) throws IOException {
        Path zipPath = null;
        try {
            zipPath = createTempFile("unzipStream", ".zip");
            copy(input, zipPath, StandardCopyOption.REPLACE_EXISTING);
            unzipInternal(zipPath, destDirectory);
        } finally {
            input.close();
            if (zipPath != null) {
                Files.deleteIfExists(zipPath);
            }
        }
    }

    /**
     * Safe variant working also with Samba source file
     * @param zipFile
     * @param destDirectory
     * @throws IOException
     */
    public static void unzip(final Path zipFile, final Path destDirectory) throws IOException {
        Path zipPath = null;
        try {
            zipPath = createTempFile("unzipFile", ".zip");
            copy(zipFile, zipPath, StandardCopyOption.REPLACE_EXISTING);
            unzipInternal(zipPath, destDirectory);
        } finally {
            if (zipPath != null) {
                Files.deleteIfExists(zipPath);
            }
        }
    }

    /**
     * Unzip (but not works with Samba source file)
     * @param zipFile
     * @param destDir
     * @throws IOException
     */
    private static void unzipInternal(final Path zipFile, final Path destDir) throws IOException {
        if (notExists(destDir)) {
            createDirectories(destDir);
        }

        try (FileSystem zipFileSystem = FileSystems.newFileSystem(zipFile, (ClassLoader) null)) {
            final Path root = zipFileSystem.getRootDirectories().iterator().next();

            walkFileTree(root, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file,
                                                 BasicFileAttributes attrs) throws IOException {
                    final Path destFile = Paths.get(destDir.toString(), file.toString());
                    try {
                        copy(file, destFile, StandardCopyOption.REPLACE_EXISTING);
                    } catch (DirectoryNotEmptyException ignore) {
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir,
                                                         BasicFileAttributes attrs) throws IOException {
                    final Path dirToCreate = Paths.get(destDir.toString(), dir.toString());
                    if (notExists(dirToCreate)) {
                        createDirectory(dirToCreate);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }
}
