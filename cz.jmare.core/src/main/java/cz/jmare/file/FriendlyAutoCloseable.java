package cz.jmare.file;

public interface FriendlyAutoCloseable extends AutoCloseable {
    @Override
    public void close();
}
