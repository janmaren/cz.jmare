package cz.jmare.file;

import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;

public class PathUtil {

    /**
     * @param path absolute(also starting with file:) or relative path, example is src/main/resources/moeba/mapping-bs*.xml
     * @return example mapping-bs*.xml (no slash)
     */
    public static String getLastPartFromPath(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return path;
        }
        String fileNameWithWildCard = path.substring(slashIndex + 1);
        return fileNameWithWildCard;
    }

    /**
     * Get last part without trailing slashes
     * @param path
     * @return
     */
    public static String getLastPartFromPathNice(String path) {
        if (endsWithSep(path)) {
            path = path.substring(0, path.length() - 1);
        }
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return path;
        }
        String fileNameWithWildCard = path.substring(slashIndex + 1);
        return fileNameWithWildCard;
    }

    /**
     * @param path absolute(also starting with file:) or relative path, example is src/main/resources/moeba/mapping-bs*.xml
     * @return example src/main/resources/moeba (no slash at the end)
     */
    public static String getPathWithoutFilename(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return ".";
        }
        String fileNameWithWildCard = path.substring(0, slashIndex);
        return fileNameWithWildCard;
    }

    public static int lastSepIndex(String path) {
        int lastIndexSlash = path.lastIndexOf("/");
        int lastIndexBackSlash = path.lastIndexOf("\\");
        int slashIndex = lastIndexSlash > lastIndexBackSlash ? lastIndexSlash: lastIndexBackSlash;
        return slashIndex;
    }

    public static boolean endsWithSep(String str) {
        char lastChar = str.charAt(str.length() - 1);
        return (lastChar == '/' || lastChar == '\\');
    }

    /**
     * Return true when suffix is a suffix of string. Note: string '.txt' is not a suffix of file name '.txt' but rather a hidden file.
     * @param fullPathOrName
     * @param suffix
     * @param caseSensitive
     * @return
     */
    public static boolean hasSuffix(String fullPathOrName, String suffix, boolean caseSensitive) {
        if (endsWithSep(fullPathOrName)) {
            return false;
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        if (!suffix.startsWith(".")) {
            suffix = "." + suffix;
        }
        if (fullPathOrName.length() <= suffix.length()) {
            return false;
        }
        if (caseSensitive) {
            return fullPathOrName.endsWith(suffix);
        }
        return fullPathOrName.substring(fullPathOrName.length() - suffix.length()).equalsIgnoreCase(suffix);
    }

    public static boolean hasSuffix(String fullPathOrName, String suffix) {
        return hasSuffix(fullPathOrName, suffix, AppHelper.getPlatform() != PlatformType.WINDOWS);
    }

    public static boolean hasSuffix(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            return false;
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        return fullPathOrName.indexOf(".") > 0;
    }

    /**
     * Return suffix like '.txt' or IllegalArgumentException when no exists
     * @param fullPathOrName
     * @return
     * @throws IllegalArgumentException
     */
    public static String getSuffix(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            throw new IllegalArgumentException("String " + fullPathOrName + " is a directory");
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        int lastIndexOf = fullPathOrName.lastIndexOf(".");
        if (lastIndexOf <= 0) {
            throw new IllegalArgumentException("No suffix exists for " + fullPathOrName);
        }
        return fullPathOrName.substring(lastIndexOf);
    }

    /**
     * Return suffix like '.txt' or empty string ""
     * @param fullPathOrName
     * @return
     */
    public static String getSuffixOrEmpty(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            return "";
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        int lastIndexOf = fullPathOrName.lastIndexOf(".");
        if (lastIndexOf <= 0) {
            return "";
        }
        return fullPathOrName.substring(lastIndexOf);
    }

    @Deprecated
    public static String[] getNameAndSuffix(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            throw new IllegalArgumentException("String " + fullPathOrName + " is a directory");
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        int lastIndexOf = fullPathOrName.lastIndexOf(".");
        if (lastIndexOf <= 0) {
            return new String[] {fullPathOrName};
        }
        return new String[] {fullPathOrName.substring(0, lastIndexOf), fullPathOrName.substring(lastIndexOf)};
    }

    /**
     * Get name and suffix of full path. Leading directory ommited
     * @param fullPathOrName
     * @return
     */
    public static String[] getNameAndSuffixOrEmpty(String fullPathOrName) {
        if (endsWithSep(fullPathOrName)) {
            throw new IllegalArgumentException("String " + fullPathOrName + " is a directory");
        }
        int lastSepIndex = lastSepIndex(fullPathOrName);
        if (lastSepIndex != -1) {
            fullPathOrName = fullPathOrName.substring(lastSepIndex + 1);
        }
        int lastIndexOf = fullPathOrName.lastIndexOf(".");
        if (lastIndexOf <= 0) {
            return new String[] {fullPathOrName, ""};
        }
        return new String[] {fullPathOrName.substring(0, lastIndexOf), fullPathOrName.substring(lastIndexOf)};
    }

    /**
     * Get dir(0) and file name(1) or empty strings
     * @param path
     * @return parsed path with indexes [0] and [1] where [0]+[1] makes full path
     */
    public static String[] parseFullPath(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return new String[] {"", path};
        }
        return new String[] {path.substring(0, slashIndex + 1), path.substring(slashIndex + 1)};
    }

    /**
     * Get path until beginning of suffix(0) and suffix (1) or empty strings
     * @param path
     * @return parsed path with indexes [0] and [1] where [0]+[1] makes full path
     */
    public static String[] parseFullPathSuffix(String path) {
        int lastIndexOf = path.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return new String[] {path, ""};
        }
        return new String[] {path.substring(0, lastIndexOf), path.substring(lastIndexOf)};
    }
    
    /**
     * Get path with replaced name but keeped suffix
     * @param origPath
     * @param newNameWithoutSuffix
     * @return
     */
    public static String replaceName(String origPath, String newNameWithoutSuffix) {
    	String[] fullPath = parseFullPath(origPath);
    	String name = fullPath[1];
    	String[] nameAndSuffixOrEmpty = getNameAndSuffixOrEmpty(name);
        return fullPath[0] + newNameWithoutSuffix + nameAndSuffixOrEmpty[1];
    }
    
    /**
     * Get path with different suffix
     * @param origPath
     * @param newSuffix new suffix
     * @return
     */
    public static String replaceSuffix(String origPath, String newSuffix) {
        String[] nameAndSuffix = parseFullPathSuffix(origPath);
        return nameAndSuffix[0] + newSuffix;
    }
}
