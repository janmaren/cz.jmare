package cz.jmare.file;

import java.io.File;
import java.nio.file.WatchEvent.Kind;

public interface FileChangedListener {
    void changed(File file, Kind<?> kind);
}
