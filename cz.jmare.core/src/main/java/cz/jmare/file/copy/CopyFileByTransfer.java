package cz.jmare.file.copy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Callable;

import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class CopyFileByTransfer implements Callable<Boolean> {
    private Path srcFile;

    private Path dstFile;

    private BasicFileAttributes srcAttrs;

    private ProgressStatus<Long> progressStatus;

    public CopyFileByTransfer(Path srcFile, Path dstFile) throws IOException {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        srcAttrs = Files.readAttributes(srcFile, BasicFileAttributes.class);
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByTransfer(Path srcFile, Path dstFile, BasicFileAttributes srcAttrs) {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByTransfer(Path srcFile, Path dstFile, BasicFileAttributes srcAttrs, ProgressStatus<Long> progressStatus) {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        this.progressStatus = progressStatus;
    }

    @Override
    public Boolean call() throws Exception {
        try (FileInputStream input = new FileInputStream(srcFile.toString());
                FileOutputStream output = new FileOutputStream(dstFile.toString());
                FileChannel in = input.getChannel();
                FileChannel out = output.getChannel()) {
            long length = in.size();
            progressStatus.setMaxValue(length);
            progressStatus.setProgressValue(0L);

            in.transferTo(0, length, out);

            progressStatus.setProgressValue(length);
        }

        Files.setAttribute(dstFile, "creationTime", srcAttrs.creationTime());
        Files.setAttribute(dstFile, "lastModifiedTime", srcAttrs.lastModifiedTime());

        return true;
    }
}
