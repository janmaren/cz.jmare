package cz.jmare.file.copy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Callable;

import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class CopyFileByStream implements Callable<Boolean> {
    private Path srcFile;

    private Path dstFile;

    private BasicFileAttributes srcAttrs;

    private ProgressStatus<Long> progressStatus;

    private static final int BUFFER_SIZE = 8192 * 4;

    public CopyFileByStream(Path fromFile, Path dstFile) throws IOException {
        super();
        this.srcFile = fromFile;
        this.dstFile = dstFile;
        srcAttrs = Files.readAttributes(fromFile, BasicFileAttributes.class);
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByStream(Path fromFile, Path dstFile, BasicFileAttributes srcAttrs) {
        super();
        this.srcFile = fromFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByStream(Path fromFile, Path dstFile, BasicFileAttributes srcAttrs, ProgressStatus<Long> progressStatus) {
        super();
        this.srcFile = fromFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        this.progressStatus = progressStatus;
    }

    @Override
    public Boolean call() throws Exception {
        try (InputStream input = new FileInputStream(srcFile.toString()); OutputStream output = new FileOutputStream(dstFile.toString())) {
            progressStatus.setMaxValue(srcAttrs.size());
            progressStatus.setProgressValue(0L);
            byte[] buf = new byte[BUFFER_SIZE];
            int bytesRead;
            long count = 0;
            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
                count += bytesRead;
                progressStatus.setProgressValue(count);
                if (Thread.interrupted()) {
                    throw new InterruptedException("Interrupted");
                }
            }
        }

        Files.setAttribute(dstFile, "creationTime", srcAttrs.creationTime());
        Files.setAttribute(dstFile, "lastModifiedTime", srcAttrs.lastModifiedTime());

        return true;
    }
}
