package cz.jmare.file.copy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Callable;

import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class CopyFileByChannel implements Callable<Boolean> {
    private Path srcFile;

    private Path dstFile;

    private BasicFileAttributes srcAttrs;

    private ProgressStatus<Long> progressStatus;

    public CopyFileByChannel(Path srcFile, Path dstFile) throws IOException {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        srcAttrs = Files.readAttributes(srcFile, BasicFileAttributes.class);
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByChannel(Path srcFile, Path dstFile, BasicFileAttributes srcAttrs) {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        progressStatus = new NullLongProgressStatus();
    }

    public CopyFileByChannel(Path srcFile, Path dstFile, BasicFileAttributes srcAttrs, ProgressStatus<Long> progressStatus) {
        super();
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        this.srcAttrs = srcAttrs;
        this.progressStatus = progressStatus;
    }

    @Override
    public Boolean call() throws Exception {
        try (InputStream input = new FileInputStream(srcFile.toString());
                OutputStream output = new FileOutputStream(dstFile.toString());
                ReadableByteChannel inputChannel = Channels.newChannel(input);
                WritableByteChannel outputChannel = Channels.newChannel(output)) {
            final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
            long length = srcAttrs.size();
            progressStatus.setMaxValue(length);
            progressStatus.setProgressValue(0L);
            int bytesRead;
            long count = 0;

            while ((bytesRead = inputChannel.read(buffer)) != -1) {
                buffer.flip();
                outputChannel.write(buffer);
                buffer.compact();
                count += bytesRead;
                progressStatus.setProgressValue(count);
                if (Thread.interrupted()) {
                    throw new InterruptedException("Interrupted");
                }
            }
            buffer.flip();
            // make sure the buffer is fully drained.
            while (buffer.hasRemaining()) {
                outputChannel.write(buffer);
            }

            progressStatus.setProgressValue(length);
        }

        Files.setAttribute(dstFile, "creationTime", srcAttrs.creationTime());
        Files.setAttribute(dstFile, "lastModifiedTime", srcAttrs.lastModifiedTime());

        return true;
    }
}
