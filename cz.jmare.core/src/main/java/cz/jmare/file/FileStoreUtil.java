package cz.jmare.file;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileStoreUtil {

    public static String getVolumeLabel(Path dir) {
        FileStore fileStore;
        try {
            fileStore = Files.getFileStore(dir);
            String name = fileStore.name();
            if (name == null) {
                return null;
            }
            if ("".equals(name.trim())) {
                return null;
            }
            return name;
        } catch (IOException e) {
            return null;
        }
    }

}
