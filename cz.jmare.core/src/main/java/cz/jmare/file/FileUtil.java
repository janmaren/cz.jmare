package cz.jmare.file;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * alternative for commons-io
 */
public class FileUtil {
    private static final int DEFAULT_BUFFER_SIZE = 8192 * 2;

    public static String readFileText(Reader input) throws IOException {
        StringWriter output = new StringWriter();
        char[] buffer = new char[DEFAULT_BUFFER_SIZE];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toString();
    }

    public static String getUtf8FileTextSafe(String filename) throws IOException {
        return getUtf8FileTextSafe(new FileInputStream(filename));
    }

    public static String getUtf8FileTextSafe(InputStream is) throws IOException {
        return getFileTextSafe(is, "UTF-8");
    }

    public static String getFileTextSafe(String filename, String charsetName) throws IOException {
        return getFileTextSafe(new FileInputStream(filename), charsetName);
    }

    public static String getFileTextSafe(String filename, String charsetName, int maxBytes) throws IOException {
        return getFileTextSafe(new FileInputStream(filename), charsetName, maxBytes);
    }

    private static String getFileTextSafe(InputStream is, String charsetName, int maxBytes) throws UnsupportedEncodingException, IOException {
        StringWriter output = new StringWriter();
        int writed = 0;
        try (InputStreamReader input = new InputStreamReader(is, charsetName)) {
            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                if (writed + n <= maxBytes) {
                    output.write(buffer, 0, n);
                    writed += n;
                } else {
                    int len = maxBytes - writed;
                    output.write(buffer, 0, len);
                    writed += len;
                    break;
                }
            }
        }
        return output.toString();
    }

    public static String getFileTextSafe(InputStream is, String charsetName) throws IOException {
        StringWriter output = new StringWriter();
        try (InputStreamReader input = new InputStreamReader(is, charsetName)) {
            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n);
            }
        }
        return output.toString();
    }

    public static void saveFileText(String filename, String str, String charsetName) throws IOException {
        OutputStream out = new FileOutputStream(filename);
        try (OutputStreamWriter writer = new OutputStreamWriter(out, charsetName)) {
            writer.write(str);
        }
    }

    public static void saveUtf8FileText(String filename, String str) throws IOException {
        saveFileText(filename, str, "UTF-8");
    }

    public static String getFileText(String filename, String charsetName) throws IOException {
        return getFileText(new FileInputStream(filename), charsetName);
    }

    public static String getFileText(String filename, String charsetName, int maxBytes) throws IOException {
        return getFileText(new FileInputStream(filename), charsetName, maxBytes);
    }

    public static String getFileText(InputStream inputStream, String charsetName) throws IOException {
        StringBuilder builder = new StringBuilder();
        try (InputStreamReader reader = new InputStreamReader(inputStream, charsetName)) {
            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = reader.read(buffer)) != -1) {
                builder.append(buffer, 0, length);
            }
        }
        return builder.toString();
    }

    public static String getFileText(InputStream inputStream, String charsetName, int maxBytes) throws IOException {
        StringBuilder builder = new StringBuilder();
        try (InputStreamReader reader = new InputStreamReader(inputStream, charsetName)) {
            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int length;
            int writed = 0;
            while ((length = reader.read(buffer)) != -1) {
                if (writed + length <= maxBytes) {
                    builder.append(buffer, 0, length);
                    writed += length;
                } else {
                    int len = maxBytes - writed;
                    builder.append(buffer, 0, len);
                    writed += len;
                    break;
                }
                builder.append(buffer, 0, length);
            }
        }
        return builder.toString();
    }

    public static byte[] getFile(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        copyLarge(inputStream, os, buffer);
        os.close();
        return os.toByteArray();
    }

    public static long copyLarge(final InputStream input, final OutputStream output)
            throws IOException {
        byte[] buffer = new byte[8192];
        return copyLarge(input, output, buffer);
    }

    public static long copyLarge(final InputStream input, final OutputStream output, final byte[] buffer)
            throws IOException {
        long count = 0;
        int n;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    public static String[] splitToLines(String string) {
        return string.split("\\r?\\n");
    }

    public static String[] splitToLinesQuick(String string) {
        List<String> res = new ArrayList<>();
        int index = -1;
        int lastPos = 0;
        while ((index = string.indexOf("\n", index + 1)) != -1) {
            int to = index;
            if (index > 0 && string.charAt(index - 1) == '\r') {
                to--;
            }
            String str = string.substring(lastPos, to);
            res.add(str);
            lastPos = index + 1;
        }
        String lastStr = string.substring(lastPos);
        if (!"".equals(lastStr)) {
            res.add(lastStr);
        }
        return res.toArray(new String[res.size()]);
    }

    public static String[] loadLines(String filename) throws IOException {
        return loadLines(filename, StandardCharsets.ISO_8859_1.toString());
    }

    public static String[] loadLines(String filename, String charset) throws IOException {
        try (FileInputStream is = new FileInputStream(filename)) {
            String fileText = FileUtil.getFileText(is, charset);
            String[] splitToLines = splitToLinesQuick(fileText);
            return splitToLines;
        }
    }

    public static String[] loadLinesQuick(String filename) throws IOException {
        return loadLinesQuick(filename, StandardCharsets.ISO_8859_1.toString());
    }

    public static String[] loadLinesQuick(String filename, String charset) throws IOException {
        byte[] readAllBytes = Files.readAllBytes(Paths.get(filename));
        String fileText = new String(readAllBytes, charset);
        return splitToLinesQuick(fileText);
    }

    public static String[] loadLinesQuick(InputStream is) throws IOException {
        return loadLinesQuick(is, StandardCharsets.ISO_8859_1.toString());
    }

    public static String[] loadLinesQuick(InputStream is, String charset) throws IOException {
        byte[] readAllBytes = readInputStreamBytes(is);
        String fileText = new String(readAllBytes, charset);
        return splitToLinesQuick(fileText);
    }

    private static byte[] readInputStreamBytes(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[DEFAULT_BUFFER_SIZE];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
          buffer.write(data, 0, nRead);
        }

        return buffer.toByteArray();
    }

}
