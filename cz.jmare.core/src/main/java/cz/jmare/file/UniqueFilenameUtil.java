package cz.jmare.file;

import java.io.File;

public class UniqueFilenameUtil {
    private static final int MAX_TRY_ATTEMPTS = 1000000;


    /**
     * Parses name and returns part before suffix as [0] and suffix including prepending dot as [1]<br/>
     * [0] + [1] must give original argument value<br/>
     * Example: c:\temp\file1.txt =>    [0] - "c:\temp\file1",      [1] - ".txt"<br/>
     * Example: c:\temp\file2 =>        [0] - "c:\temp\file2",      [1] - ""<br/>
     * Example: c:\temp\.dir\file3 =>   [0] - "c:\temp\.dir\file3", [1] - ""<br/>
     * Example: c:\temp\.file4 =>       [0] - "c:\temp\file4",      [1] - ""<br/>
     * @param path path ending by file name
     * @return
     */
    public static String[] parseName(String path) {
        int i = path.lastIndexOf('.');
        int lastSlash = PathUtil.lastSepIndex(path);
        String[] res = new String[2];
        if (lastSlash > i) {
            res[0] = path;
            res[1] = "";
            return res;
        }
        if (i > 0) {
            if (lastSlash + 1 == i) {
                res[0] = path;
                res[1] = "";
            } else {
                res[0] = path.substring(0, i);
                res[1] = path.substring(i);
            }
        } else {
            res[0] = path;
            res[1] = "";
        }
        return res;
    }

    /**
     * Find file which doesn't exist on file system
     * @param inputPath planned nonexisting file
     * @return nonexisting file
     */
    public static File nonexistingFile(String inputPath) {
        String[] parseName = parseName(inputPath);
        File outputFile = new File(parseName[0] + "(" + System.currentTimeMillis() + ")" + parseName[1]); // falback
        String discr = "";
        for (int i = 0; i <= MAX_TRY_ATTEMPTS; i++) {
            if (i > 0) {
                discr = "(" + i + ")";
            }
            outputFile = new File(parseName[0] + discr + parseName[1]);
            if (!outputFile.exists()) {
                break;
            }
        }

        return outputFile;
    }

    /**
     * Find file which doesn't exist on file system. File is composed by inputPath and discriminator<br/>
     * Example: inputPath="c:\temp\file1.txt", discriminator="-output" => c:\temp\file1-output.txt or c:\temp\file1-output(1).txt or ...
     * @param inputPath path on which the result file is based. It can be for example source file path
     * @param discriminator string added before suffix or at the end of file name
     * @return
     */
    public static File nonexistingFileAddSuffix(String inputPath, String discriminator) {
        String[] parseName = parseName(inputPath);
        File outputFile = new File(parseName[0] + discriminator + "(" + System.currentTimeMillis() + ")" + parseName[1]); // falback
        String discr = "";
        for (int i = 0; i <= MAX_TRY_ATTEMPTS; i++) {
            if (i > 0) {
                discr = "(" + i + ")";
            }
            outputFile = new File(parseName[0] + discriminator + discr + parseName[1]);
            if (!outputFile.exists()) {
                break;
            }
        }

        return outputFile;
    }
}
