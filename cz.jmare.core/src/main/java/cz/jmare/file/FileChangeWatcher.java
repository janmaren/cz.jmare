package cz.jmare.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class FileChangeWatcher implements Runnable {
    private File file;
    private FileChangedListener fileChangedListener;
    private WatchService watchService;
    private long lastTimestamp = -1;

    public FileChangeWatcher(File file, FileChangedListener fileChangedListener) {
        this(file, fileChangedListener, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
    }

    public FileChangeWatcher(File file, FileChangedListener fileChangedListener, WatchEvent.Kind<?>... events) {
        super();
        this.fileChangedListener = fileChangedListener;
        try {
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            throw new RuntimeException("Unable to get watch service", e);
        }
        this.file = file;
        if (this.file.isFile()) {
            lastTimestamp = this.file.lastModified();
        }
        Path path = file.toPath();
        Path dirPath = path.getParent();
        if (dirPath == null) {
            // probably absolute path
            dirPath = Paths.get(""); // current directory
        }
        try {
            dirPath.register(watchService, events);
        } catch (IOException e) {
            throw new RuntimeException("Unable to register watch service", e);
        }
    }

    @Override
    public void run() {
        while (true) {
            WatchKey key;
            try {
                key = watchService.take();
            } catch (InterruptedException e) {
                try {
                    watchService.close();
                } catch (IOException e1) {
                }
                return;
            }
            for (WatchEvent<?> watchEvent : key.pollEvents()) {
                WatchEvent.Kind<?> kind = watchEvent.kind();
                Path eventFile = (Path) watchEvent.context();
                if (eventFile.getFileName().toString().equals(file.getName())) {
                    long lastModified = kind == StandardWatchEventKinds.ENTRY_DELETE ? -1 : file.lastModified();
                    if (lastModified != lastTimestamp) {
                        fileChangedListener.changed(this.file, kind);
                        lastTimestamp = lastModified;
                    }
                }
            }
            key.reset();
        }
    }

    public static void main(String[] args) {
        FileChangeWatcher fileChangeWatcher = new FileChangeWatcher(new File("c:\\temp\\file1.txt"), new FileChangedListener() {
            @Override
            public void changed(File file, Kind<?> kind) {
                System.out.println("Zmenen soubor " + file + " na " + kind);
            }
        });
        Thread thread = new Thread(fileChangeWatcher);
        thread.start();
        try {
            Thread.sleep(1000 * 60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.interrupt();

        while(true);
    }
}
