package cz.jmare.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.BiFunction;

import cz.jmare.file.DirUtil.Entity.Type;

public class DirUtil {
    public static class Entity {
        public Entity(Path path, Type type) {
            this.path = path.normalize();
            this.type = type;
        }

        public Path path;

        public Type type;

        public String getName() {
            return path.getFileName().toString();
        }

        public boolean isDirectory() {
            return type == Type.DIRECTORY;
        }

        @Override
        public String toString() {
            return (type == Type.DIRECTORY ? "D:" : "F:") + getName();
        }

        public enum Type {
            FILE, DIRECTORY
        }
    }

    public static List<Entity> listEntities(String path) {
        return listEntities(Paths.get(path));
    }

    public static List<Entity> listEntities(File path) {
        return listEntities(path.toPath());
    }

    public static List<Entity> listEntities(Path path) {
        final List<Entity> entities = new ArrayList<DirUtil.Entity>();
        EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        try {
            Files.walkFileTree(path, opts, 1, new SimpleFileVisitor<Path>(){
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    entities.add(new Entity(file, attrs.isDirectory() ? Type.DIRECTORY : Type.FILE));
                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e)
                    throws IOException {
                    return FileVisitResult.SKIP_SUBTREE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Unable to list " + getAbsolutePath(path), e);
        }
        return entities;
    }

    public static List<Path> listEntities(Path path, int maxDepth, BiFunction<Path, BasicFileAttributes, Boolean> filter, boolean followLinks) {
        final List<Path> paths = new ArrayList<Path>();
        EnumSet<FileVisitOption> opts = followLinks ? EnumSet.of(FileVisitOption.FOLLOW_LINKS) : EnumSet.noneOf(FileVisitOption.class);
        try {
            Files.walkFileTree(path, opts, maxDepth, new SimpleFileVisitor<Path>(){
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (filter.apply(file, attrs)) {
                        paths.add(file);
                    }
                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e)
                    throws IOException {
                    return FileVisitResult.SKIP_SUBTREE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Unable to list " + getAbsolutePath(path), e);
        }
        return paths;
    }

    public static List<Path> listEntities(Path path, int maxDepth, boolean followLinks) {
        return listEntities(path, maxDepth, new AcceptAllBiFunction(), followLinks);
    }

    public static class AcceptAllBiFunction implements BiFunction<Path, BasicFileAttributes, Boolean> {
        @Override
        public Boolean apply(Path t, BasicFileAttributes u) {
            return true;
        }
    }

    private static String getAbsolutePath(Path path) {
        try {
            if (path.isAbsolute()) {
                return path.normalize().toString();
            }
            Path absolutePath = path.toAbsolutePath().normalize();
            return absolutePath.toString();
        } catch (Exception e) {
            return path.toString();
        }
    }
}
