package cz.jmare.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

public class CopyDir extends SimpleFileVisitor<Path> {
    private Path sourceDir;
    private Path targetDir;
    private CopyOption[] options = {};

    public CopyDir(Path sourceDir, Path targetDir) {
        this.sourceDir = sourceDir;
        this.targetDir = targetDir;
    }

    public CopyDir(Path sourceDir, Path targetDir, CopyOption[] options) {
        this.sourceDir = sourceDir;
        this.targetDir = targetDir;
        this.options = options;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {
        Path targetFile = null;
        try {
            targetFile = targetDir.resolve(sourceDir.relativize(file));
            Files.copy(file, targetFile, options);
        } catch (IOException ex) {
            throw new IllegalStateException("Unable to copy " + file + " to " + targetFile, ex);
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attributes) {
        Path newDir = null;
        try {
            newDir = targetDir.resolve(sourceDir.relativize(dir));
            if (!Files.isDirectory(newDir)) {
                Files.createDirectory(newDir);
            }
        } catch (IOException ex) {
            throw new IllegalStateException("Unable to create directory " + newDir, ex);
        }

        return FileVisitResult.CONTINUE;
    }

    /**
     * Copy directory to other directory
     * @param from
     * @param to
     * @throws IOException
     */
    public static void copy(String from, String to) throws IOException {
        Path sourceDir = Paths.get(from);
        Path targetDir = Paths.get(to);
        copy(sourceDir, targetDir, new CopyOption[0]);
    }

    public static void copy(String from, String to, CopyOption[] options) throws IOException {
        Path sourceDir = Paths.get(from);
        Path targetDir = Paths.get(to);
        copy(sourceDir, targetDir, options);
    }

    public static void copyWithReplace(String from, String to) throws IOException {
        Path sourceDir = Paths.get(from);
        Path targetDir = Paths.get(to);
        CopyOption[] options = {StandardCopyOption.REPLACE_EXISTING};
        copy(sourceDir, targetDir, options);
    }

    public static void copy(Path sourceDir, Path targetDir, CopyOption[] options) throws IOException {
        Files.walkFileTree(sourceDir, new CopyDir(sourceDir, targetDir, options));
    }

    /**
     * Copy only content of source directory which will became content of destination directory
     * @param from
     * @param to
     * @throws IOException
     */
    public static void copyContent(String from, String to) throws IOException {
        Path sourceDir = Paths.get(from);
        Path targetDir = Paths.get(to);
        copyContent(sourceDir, targetDir, new CopyOption[0]);
    }

    public static void copyContentWithReplace(String from, String to) throws IOException {
        Path sourceDir = Paths.get(from);
        Path targetDir = Paths.get(to);
        CopyOption[] options = {StandardCopyOption.REPLACE_EXISTING};
        copyContent(sourceDir, targetDir, options);
    }

    public static void copyContent(Path sourceDir, Path targetDir, CopyOption[] options) throws IOException {
        if (!Files.isDirectory(targetDir)) {
            Files.createDirectory(targetDir);
        }
        File[] srcFiles = sourceDir.toFile().listFiles();
        for (File srcFile : srcFiles) {
            Path srcPath = srcFile.toPath();
            Path destPath = targetDir.resolve(sourceDir.relativize(srcPath));
            Files.walkFileTree(srcPath, new CopyDir(srcPath, destPath, options));
        }
    }

    public static void main(String[] args) throws IOException {
        copyContent(args[0], args[1]);
    }
}
