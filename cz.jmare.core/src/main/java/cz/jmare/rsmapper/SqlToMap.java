package cz.jmare.rsmapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import cz.jmare.db.NamedStatementPopulator;

public class SqlToMap {
    /**
     * Utility to launch sql select and return resultset converted to list of maps
     * @param dataSource
     * @param sql
     * @return
     */
    public static List<Map<String, Object>> query(DataSource dataSource, String sql) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            return ResultSetConvertor.convertAll(rs);
        } catch (SQLException se) {
            throw new RuntimeException("Problems during run " + sql, se);
        }
    }

    public static List<Map<String, Object>> query(DataSource dataSource, String sql, Map<String, Object> parametersMap) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(conn, sql, parametersMap);
            ResultSet rs = prepareStatement.executeQuery();
            return ResultSetConvertor.convertAll(rs);
        } catch (SQLException se) {
            throw new RuntimeException("Problems during run " + sql, se);
        }
    }

    public static List<Map<String, Object>> query(Connection conn, String sql) {
        try {
            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            ResultSet rs = prepareStatement.executeQuery();
            return ResultSetConvertor.convertAll(rs);
        } catch (SQLException se) {
            throw new RuntimeException("Problems during run " + sql, se);
        }
    }

    public static List<Map<String, Object>> query(Connection conn, String sql, Map<String, Object> parametersMap) {
        try {
            PreparedStatement prepareStatement = NamedStatementPopulator.prepareStatement(conn, sql, parametersMap);
            ResultSet rs = prepareStatement.executeQuery();
            return ResultSetConvertor.convertAll(rs);
        } catch (SQLException se) {
            throw new RuntimeException("Problems during run " + sql, se);
        }
    }

}
