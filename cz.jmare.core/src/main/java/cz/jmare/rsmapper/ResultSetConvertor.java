package cz.jmare.rsmapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility to convert lines of resultset into List of Maps. Key of map is column name fetched from metadata.<br>
 * Note: the s$ special marker can be used. When resultset contains column name in format s$&lt;_suffix&gt;, all next columns will have
 * the suffix &lt;_suffix&gt; unless other suffix used. E.g. select id, '1' $s_gr, id, name... will make a map with keys id, id_gr and name_gr
 */
public class ResultSetConvertor {
    private static final String SEPARATOR = "s$";

    private static final String SEPARATOR_U = "S$";

    public static List<Map<String, Object>> convertAll(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        while (rs.next()) {
            HashMap<String, Object> row = convertOne(rs, md, null);
            list.add(row);
        }

        return list;
    }

    public static List<Map<String, Object>> convertAll(ResultSet rs, RemapConsumer remapConsumer) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        while (rs.next()) {
            HashMap<String, Object> row = convertOne(rs, md, remapConsumer);
            list.add(row);
        }

        return list;
    }

    public static HashMap<String, Object> convertOne(ResultSet rs) throws SQLException {
        return convertOne(rs, rs.getMetaData(), null);
    }

    public static HashMap<String, Object> convertOne(ResultSet rs, RemapConsumer remapConsumer) throws SQLException {
        return convertOne(rs, rs.getMetaData(), remapConsumer);
    }

    public static HashMap<String, Object> convertOne(ResultSet rs, ResultSetMetaData md, RemapConsumer remapConsumer) throws SQLException {
        int columns = md.getColumnCount();
        HashMap<String, Object> row = new HashMap<String, Object>(columns);
        String columnNameSuffix = "";
        for (int i = 1; i <= columns; ++i) {
            String columnName = md.getColumnName(i).toLowerCase();
            if (columnName.startsWith(SEPARATOR) || columnName.startsWith(SEPARATOR_U)) {
                if (columnName.equals(SEPARATOR) || columnName.equals(SEPARATOR_U)) {
                    columnNameSuffix = "";
                } else {
                    columnNameSuffix = columnName.substring(SEPARATOR.length());
                }
                continue;
            }
            if (remapConsumer != null && remapConsumer.remap(md, rs, i, row)) {
                continue;
            }
            Object value = getObject(rs, i, md);

            String columnNameWithSuffix = columnName + columnNameSuffix;
            if (row.containsKey(columnNameWithSuffix)) {
                throw new IllegalStateException("Resultset contains duplicit column '" + columnNameWithSuffix + "'");
            }
            row.put(columnNameWithSuffix, value);
        }
        return row;
    }

    private static Object getObject(ResultSet rs, int index, ResultSetMetaData md) throws SQLException {
        Object value = rs.getObject(index);
        if (value == null) {
            return null;
        }
        String className = value.getClass().getName();
        if (className.startsWith("oracle.")) {
            if (className.startsWith("oracle.sql.TIMESTAMP")) { // oracle.sql.TIMESTAMP, oracle.sql.TIMESTAMPTZ
                value = rs.getTimestamp(index);
            } else if ("oracle.sql.DATE".equals(className)) {
                String metaDataClassName = md.getColumnClassName(index);
                if ("java.sql.Timestamp".equals(metaDataClassName) || "oracle.sql.TIMESTAMP".equals(metaDataClassName)) {
                    value = rs.getTimestamp(index);
                } else {
                    value = rs.getDate(index);
                }
            } else if ("oracle.sql.BLOB".equals(className)) {
                value = rs.getBytes(index);
            }  else if ("oracle.sql.CLOB".equals(className)) {
                value = rs.getString(index);
            } else if (value instanceof java.sql.Date) {
                if ("java.sql.Timestamp".equals(md.getColumnClassName(index))) {
                    value = rs.getTimestamp(index);
                }
            }
        }
        return value;
    }

    /**
     * Vraci true pokud byl sloupec preveden a pridan do mapy
     */
    public static interface RemapConsumer {
        public boolean remap(ResultSetMetaData md, ResultSet rs, int index, Map<String, Object> row);
    }
}

