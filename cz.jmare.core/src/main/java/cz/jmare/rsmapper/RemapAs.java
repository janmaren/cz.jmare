package cz.jmare.rsmapper;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Used for resultset mapping when defaults should be overriden
 */
@Target({FIELD})
@Retention(RUNTIME)
public @interface RemapAs {
    /**
     * (Optional) The name of the column in lowercase. Defaults to decameled field name.
     */
    String name() default "";

    /**
     * (Optional) Whether column may be missing in resultset
     */
    boolean nonstrictRead() default false;
}
