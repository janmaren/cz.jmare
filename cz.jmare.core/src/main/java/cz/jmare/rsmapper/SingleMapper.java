package cz.jmare.rsmapper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Remap to result (root) object. The result object must be just one. If resultset is empty the {@ling RecordNotFoundException} will be throwed.
 * When multiple object would be returned the {@link NotScalarException} will be throwed.
 */
public class SingleMapper {
    /**
     * Shodne zaznamy, ktere vrati tato funkce maji patrit do stejne skupiny. Funkce nesmi vratit null
     */
    private Function<Map<String, Object>, Object> keyFunction;
    /**
     * Funkce, ktera na zaklade mapy reprezentujici zaznam vrati instanci objektu
     */
    private Function<Map<String, Object>, ?> remapObjectFunction;
    /**
     * Mapovani asociaci
     */
    private FieldMapper[] fieldMappers;

    public SingleMapper(String columnName, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.keyFunction = new Function<Map<String, Object>, Object>(){
            @Override
            public Object apply(Map<String, Object> t) {
                Object object = t.get(columnName);
                if (object == null) {
                    if (!t.containsKey(columnName)) {
                        throw new IllegalStateException("Column " + columnName + " not exists in resultset");
                    }
                }
                return object;
            }};
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    public SingleMapper(Function<Map<String, Object>, Object> keyFunction, Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.keyFunction = keyFunction;
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    public SingleMapper(Function<Map<String, Object>, ?> remapObjectFunction, FieldMapper... fieldMappers) {
        this.keyFunction = new AllMatchFunction(); // remapovavat se bude mapa, ktera jiz obsahuje radky jednoho objektu, proto se berou vsechny jako shodne a vznikne z nich jedna skupina
        this.remapObjectFunction = remapObjectFunction;
        this.fieldMappers = fieldMappers;
    }

    @SuppressWarnings("unchecked")
    public <T> T remap(List<Map<String, Object>> objectRow, Class<T> clazz) {
        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            throw new IllegalStateException("There is column '" + keyFunction + "' with null. " + objectRow);
        }

        if (objectRow.size() == 0) {
            throw new RecordNotFoundException("Unable to return instance of " + clazz.getName() + " because resultset is empty");
        }

        Map<Object, List<Map<String, Object>>> groupedRecords = objectRow.stream().collect(Collectors.groupingBy(keyFunction));
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();

        if (groupedRecordsWithoutKey.size() > 1) {
            throw new NotScalarException("Expected scalar but result data contain vector of '" + clazz.getName() + "'. Multiple records: " + SingleFieldMapper.createIdsListString(groupedRecords));
        }
        if (groupedRecordsWithoutKey.size() == 0) {
            throw new IllegalArgumentException("Unable to return instance of " + clazz.getName() + " because no data passed");
        }
        List<Map<String, Object>> records = groupedRecordsWithoutKey.iterator().next();

        T resultObject = null;
        Map<String, Object> record = records.iterator().next();
        resultObject = (T) remapObjectFunction.apply(record);
        for (FieldMapper fieldMapper : fieldMappers) {
            fieldMapper.remapField(records, resultObject);
        }

        return resultObject;
    }

    /**
     * Remap but don't return an object. Please, use {@link #remap(List, Class)} to return a result object.<br>
     * Note: it should be used together with e.g. {@link cz.nn.common.persistence.rsmapper.SimpleRemapper#withObjectProvider(Function)} - the existing
     * object will be used and there is no need to return it because reference to it must exist on other place.
     * @param objectRow
     */
    public void remap(List<Map<String, Object>> objectRow) {
        if (keyFunction.apply(objectRow.iterator().next()) == null) {
            throw new IllegalStateException("There is column '" + keyFunction + "' with null. " + objectRow);
        }

        Map<Object, List<Map<String, Object>>> groupedRecords = objectRow.stream().collect(Collectors.groupingBy(keyFunction));
        Collection<List<Map<String, Object>>> groupedRecordsWithoutKey = (Collection<List<Map<String, Object>>>) groupedRecords.values();

        if (groupedRecordsWithoutKey.size() > 1) {
            throw new RuntimeException("Expected scalar but result data contain vector'. Multiple records: " + SingleFieldMapper.createIdsListString(groupedRecords));
        }
        if (groupedRecordsWithoutKey.size() == 0) {
            throw new IllegalArgumentException("Unable to return remap because no data in resultset");
        }
        List<Map<String, Object>> records = groupedRecordsWithoutKey.iterator().next();

        Map<String, Object> record = records.iterator().next();
        Object resultObject = remapObjectFunction.apply(record);
        for (FieldMapper fieldMapper : fieldMappers) {
            fieldMapper.remapField(records, resultObject);
        }
    }
}
