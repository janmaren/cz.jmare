package cz.jmare.rsmapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * It remaps just one row of resultset to result object (without associations).<br>
 * By default it expects the column name same as field but field name is camel case and column name contains underscores.
 * E.g. field lastEmployeeName corresponds to column last_employee_name.
 * For different mapping names the annotation {@link RemapAs#name()} must be used. For column name suffixes present in resultset it's possible
 * to use {@link #withColumnNameSuffix(String)}, in this case the right column in resultset is used and suffix is truncated to determine the right field name.<br>
 * <br>
 * By default it tries to populate all basic fields but when not found corresponding column in resultset the exception is thrown unless the annotation
 * {@link RemapAs#nonstrictRead()} is true. Basic fields are:<br>
 * Integer, Short, Long, Double, Float, Boolean, Byte, Character, String, java.util.Date, Calendar, java.sql.Date, Time, Timestamp, BigInteger, BigDecimal, LocalDate, LocalDateTime and all primitive types
 * unless the transient modifier used.
 * <br>
 * When needed to do conversion into other than basic type use the {@link #withCustomMapper(BiConsumer)}. E.g. varchar column uuid may be converted by line
 * <pre>((SomeClass) obj).setUuid(UUID.fromString(map.get("uuid")))</pre>Be careful because in case of custom mapping the column suffix must be used
 * regardless the {@link #withColumnNameSuffix(String)}
 * <br>
 * Associations (both single and collections) are not remapped here but rather later by result classes like {@link CollectionFieldMapper} or {@link SingleFieldMapper} or by a custom functional class.
 * <br>
 */
public class SimpleRemapper implements Function<Map<String, Object>, Object> {
    private Class<?> clazz;

    private List<FieldColumn> fieldColumns;

    private boolean nonstrictRead;

    private BiConsumer<Map<String, Object>, Object> customMapper;

    /**
     * Suffix pridany ke vsem nazvum sloupcu
     */
    private String columnNameSuffix = "";

    private Function<Map<String, Object>, Object> objectProvider;

    public SimpleRemapper(Class<?> clazz) {
        super();
        this.clazz = clazz;
        init();
    }

    public SimpleRemapper withNonstrictRead() {
        this.nonstrictRead = true;
        return this;
    }

    public SimpleRemapper withColumnNameSuffix(String columnNameSuffix) {
        this.columnNameSuffix = columnNameSuffix;
        return this;
    }

    public SimpleRemapper withCustomMapper(BiConsumer<Map<String, Object>, Object> customMapper) {
        this.customMapper = customMapper;
        return this;
    }

    public SimpleRemapper withObjectProvider(Function<Map<String, Object>, Object> objectProvider) {
        this.objectProvider = objectProvider;
        return this;
    }

    public static boolean isBasicType(Class<?> clazz) {
        if (Collection.class.isAssignableFrom(clazz)) {
            return false;
        }

        if (clazz.isArray() && clazz.getComponentType().isPrimitive()) {
            return true;
        }

        if (clazz.isPrimitive()) {
            return true;
        }
        if (clazz == Integer.class) return true;
        if (clazz == Short.class) return true;
        if (clazz == Long.class) return true;
        if (clazz == Double.class) return true;
        if (clazz == Float.class) return true;
        if (clazz == Boolean.class) return true;
        if (clazz == Byte.class) return true;
        if (clazz == Character.class) return true;

        if (clazz == String.class) return true;

        if (clazz == Date.class) return true;
        if (clazz == Calendar.class) return true;
        if (clazz == java.sql.Date.class) return true;
        if (clazz == Time.class) return true;
        if (clazz == Timestamp.class) return true;

        if (clazz == BigInteger.class) return true;
        if (clazz == BigDecimal.class) return true;

        if (clazz == LocalDate.class) return true;
        if (clazz == LocalDateTime.class) return true;

        return false;
    }

    protected void init() {
        Field[] allFields = ClassUtil.getAllFieldsSupportInheritance(clazz);
        List<Field> fieldsList = Arrays.asList(allFields);
        List<Field> fields = fieldsList.stream().filter(f -> {
            int modifiers = f.getModifiers();
            return !Modifier.isStatic(modifiers) && isBasicType(f.getType()) && !Modifier.isTransient(modifiers);
        }).collect(Collectors.toList());
        fieldColumns = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            fieldColumns.add(new FieldColumn(field, decamelString(field.getName()), findAnnotation(field)));
        }
    }

    public static String decamelString(String str) {
        StringBuilder sb = new StringBuilder();
        for (String token : str.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            if (sb.length() > 0) {
                sb.append("_");
            }
            sb.append(token.toLowerCase());
        }
        return sb.toString();
    }

    public static String toCamel(String str) {
        String[] split = str.split("_");
        StringBuilder sb = new StringBuilder(split[0]);
        for (int i = 1; i < split.length; i++) {
            String str2 = split[i];
            str2 = str2.substring(0, 1).toUpperCase() + str2.substring(1);
            sb.append(str2);
        }
        return sb.toString();
    }

    @Override
    public Object apply(Map<String, Object> map) {
        Object object;

        if (objectProvider != null) {
            object = objectProvider.apply(map);
            if (object == null) {
                throw new RuntimeException("Not found object of type " + clazz.getCanonicalName() + " which corresponds to row " + map);
            }
        } else {
            try {
                Constructor<?> cons = clazz.getConstructor();
                object = cons.newInstance();
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException("Unable to create instance of " + clazz, e);
            }
        }

        for (FieldColumn fieldColumn : fieldColumns) {
            Field field = fieldColumn.field;

            String columnName = fieldColumn.getName() + columnNameSuffix;
            Object value = map.get(columnName);
            if (value == null) {
                if (!map.containsKey(columnName)) {
                    if (!nonstrictRead && !(fieldColumn.remapAnnotation != null && fieldColumn.remapAnnotation.nonstrictRead())) {
                        throw new RuntimeException(
                                "Unable to remap to '" + clazz.getName() + "." + field.getName() + "' because resultset doesn't contain column '" + columnName + "'");
                    }
                    continue;
                }
            }
            Class<?> fieldType = field.getType();
            try {
                if (value != null) {
                    if (value.getClass() == BigDecimal.class) {
                        if (fieldType != BigDecimal.class) {
                            BigDecimal bigDecimalValue = (BigDecimal) value;
                            if (fieldType == Double.class) {
                                value = bigDecimalValue.doubleValue();
                            } else if (fieldType == Float.class) {
                                value = bigDecimalValue.floatValue();
                            } else {
                                if (fieldType == Long.class || fieldType == Long.TYPE) {
                                    value = bigDecimalValue.longValueExact();
                                } else if (fieldType == Integer.class || fieldType == Integer.TYPE) {
                                    value = bigDecimalValue.intValueExact();
                                } else if (fieldType == Short.class || fieldType == Short.TYPE) {
                                    value = bigDecimalValue.intValueExact();
                                } else if (fieldType == String.class) {
                                    value = String.valueOf(bigDecimalValue.doubleValue());
                                }
                            }
                        }
                    } else if (value.getClass() == Date.class) {
                        Date dateValue = (Date) value;
                        if (fieldType != Date.class) {
                            if (fieldType == LocalDate.class) {
                                value = dateValue.toLocalDate();
                            } else if (fieldType == LocalDateTime.class) {
                                Instant instant = dateValue.toInstant();
                                value = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
                            } else if (fieldType == java.util.Date.class) {
                                value = new java.util.Date(dateValue.getTime());
                            }
                        }
                    } else if (value.getClass() == Timestamp.class) {
                        Timestamp timestamp = (Timestamp) value;
                        if (fieldType != Timestamp.class) {
                            if (fieldType == LocalDate.class) {
                                value = timestamp.toLocalDateTime().toLocalDate();
                            } else if (fieldType == LocalDateTime.class) {
                                value = timestamp.toLocalDateTime();
                            } else if (fieldType == java.util.Date.class) {
                                value = new java.util.Date(timestamp.getTime());
                            }
                        }
                    } else {
                        if (fieldType == Boolean.class || fieldType == Boolean.TYPE) {
                            if (value.getClass() == String.class) {
                                if ("Y".equals(value) || "y".equals(value) || "1".equals(value) || "yes".equalsIgnoreCase((String) value)) {
                                    value = true;
                                } else if ("N".equals(value) || "n".equals(value) || "0".equals(value) || "no".equalsIgnoreCase((String) value)) {
                                    value = false;
                                }
                            } else if (value.getClass() == Integer.class || value.getClass() == Integer.TYPE) {
                                int intValue = (int) value;
                                if (intValue == 1) {
                                    value = true;
                                } else if (intValue == 0) {
                                    value = false;
                                }
                            }
                        }
                    }
                }

                field.set(object, value);
            } catch (ArithmeticException | IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException("Unable to set value '" + (value != null ? value : null) + "'" +  (value != null ? ":" + value.getClass().getName() : "")  + " to " + clazz.getName() + "." + field.getName() + ":" + fieldType.getName(), e);
            }
        }

        if (customMapper != null) {
            customMapper.accept(map, object);
        }

        return object;
    }

    private static RemapAs findAnnotation(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof RemapAs) {
                return (RemapAs) annotation;
            }
        }
        return null;
    }

    class FieldColumn {
        Field field;
        String columnName;
        RemapAs remapAnnotation;
        public FieldColumn(Field field, String columnName, RemapAs remapAnnotation) {
            super();
            this.field = field;
            this.columnName = columnName;
            this.remapAnnotation = remapAnnotation;
        }

        public String getName() {
            if (remapAnnotation != null && !"".equals(remapAnnotation.name())) {
                return remapAnnotation.name();
            }
            return columnName;
        }
    }
}
