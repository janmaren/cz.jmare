package cz.jmare.rsmapper;

/**
 * Used when just one record expected to be made but resultset doesn't contain any data
 */
public class RecordNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 4091223691696677553L;

    public RecordNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecordNotFoundException(String message) {
        super(message);
    }
}
