package cz.jmare.rsmapper;

/**
 * Used when expected one result object but a vector would be made
 */
public class NotScalarException extends RuntimeException {
    private static final long serialVersionUID = 4091223691696677553L;

    public NotScalarException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotScalarException(String message) {
        super(message);
    }
}
