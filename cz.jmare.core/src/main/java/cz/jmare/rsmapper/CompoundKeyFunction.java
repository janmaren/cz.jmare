package cz.jmare.rsmapper;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * It creates a {@link CompoundKey}. Column names in constructor should make an unique identification of object.<br>
 */
public class CompoundKeyFunction implements Function<Map<String, Object>, Object> {
    private String[] columns;

    public CompoundKeyFunction(String... columns) {
        super();
        this.columns = columns;
    }

    @Override
    public Object apply(Map<String, Object> map) {
        Map<String, Object> keyValueMap = new HashMap<String, Object>();
        boolean nonnullExists = false;
        for (String column : columns) {
            Object value = map.get(column);
            if (value == null) {
                if (!map.containsKey(column)) {
                    throw new IllegalStateException("There is no column '" + column + "' in result set.");
                }
                return null;
            } else {
                nonnullExists = true;
            }
            keyValueMap.put(column, value);
        }
        if (!nonnullExists) {
            return null;
        }
        return new CompoundKey(keyValueMap);
    }
}
