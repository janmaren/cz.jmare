package cz.jmare.exec;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import cz.jmare.os.AppHelper;
import cz.jmare.os.PlatformType;

public class ConsoleExec {
    private final static String[] WINDOWS_SUFFIXES = {".cmd", ".com", ".exe", ".bat"};

    public static List<String> execute(Charset charset, boolean stderrInStdout, List<String> args) throws IOException {
        String[] array = args.toArray(new String[args.size()]);
        return execute(charset, stderrInStdout, array);
    }

//    public static List<String> execute(Charset charset, boolean stderrInStdout, String... args) throws IOException {
//        ArrayList<String> lines = new ArrayList<String>();
//        Consumer<String> outputConsumer = new Consumer<String>() {
//            @Override
//            public void accept(String t) {
//                lines.add(t);
//            }
//        };
//        execute(charset, stderrInStdout, outputConsumer, args);
//        return lines;
//    }

    public static void execute(Charset charset, boolean stderrInStdout, Consumer<String> outputConsumer, String... args) throws IOException {
        //String collect = Arrays.asList(args).stream().collect(Collectors.joining(" "));
        ProcessBuilder builder = new ProcessBuilder(args);
        builder.redirectErrorStream(stderrInStdout);
        Process p = builder.start();
        InputStreamReader in = charset != null ? new InputStreamReader(p.getInputStream(), charset) : new InputStreamReader(p.getInputStream());
        try (BufferedReader r = new BufferedReader(in)) {
            while (true) {
                String line = r.readLine();
                if (line == null) {
                    break;
                }
                outputConsumer.accept(line);
            }
        }
        while (p.isAlive()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            }
        }
        if (p.exitValue() != 0) {
            throw new IOException("Command exited with error");
        }
    }

    public static List<String> execute(Charset charset, boolean stderrInStdout, String... args) throws IOException {
        List<String> result = new ArrayList<>();
        ProcessBuilder builder = new ProcessBuilder(args);
        builder.redirectErrorStream(stderrInStdout);
        Process p = builder.start();
        InputStreamReader in = charset != null ? new InputStreamReader(p.getInputStream(), charset) : new InputStreamReader(p.getInputStream());
        try (BufferedReader r = new BufferedReader(in)) {
            while (true) {
                String line = r.readLine();
                if (line == null) {
                    break;
                }
                result.add(line);
            }
        }
        while (p.isAlive()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new IOException("Interrupted", e);
            }
        }
        if (p.exitValue() != 0) {
            throw new IOException(result.stream().collect(Collectors.joining(System.getProperty("line.separator"))));
        }
        return result;
    }

    public static String detectFullPath(String appName) {
        if (AppHelper.getPlatform() == PlatformType.WINDOWS) {
            if (new File(appName).isAbsolute()) {
                return appName;
            }
            String path = System.getenv("PATH");
            if (path == null) {
                return appName;
            }
            String[] paths = path.split(";");
            for (String dir : paths) {
                File file;
                for (String suff : WINDOWS_SUFFIXES) {
                    file = new File(dir, appName + suff);
                    if (file.isFile()) {
                        return file.toString();
                    }
                }

                file = new File(dir, appName);
                if (file.isFile()) {
                    return file.toString();
                }
            }
        }
        return appName;
    }

    public static List<String> executeDosCmd(Charset charset, boolean stderrInStdout, String... args) throws IOException {
        String[] newArgs = new String[args.length + 2];
        newArgs[0] = "cmd.exe";
        newArgs[1] = "/c";
        System.arraycopy(args, 0, newArgs, 2, args.length);
        return execute(charset, stderrInStdout, newArgs);
    }

    public static List<String> executeDosCmd(boolean stderrInStdout, String... args) throws IOException {
        String[] newArgs = new String[args.length + 2];
        newArgs[0] = "cmd.exe";
        newArgs[1] = "/c";
        System.arraycopy(args, 0, newArgs, 2, args.length);
        return execute(null, stderrInStdout, newArgs);
    }

    public static List<String> executeDosCmd(String... args) throws IOException {
        String[] newArgs = new String[args.length + 2];
        newArgs[0] = "cmd.exe";
        newArgs[1] = "/c";
        System.arraycopy(args, 0, newArgs, 2, args.length);
        return execute(null, false, newArgs);
    }

    public static Charset getCharset(String str) {
        try {
            return Charset.forName(str);
        } catch (Exception e) {
            return null;
        }
    }
}
