package cz.jmare.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import cz.jmare.progress.ReadableProgressStatus;

public class InputStreamReadUtil {
    public static int readBytesWithWholeTimeout(InputStream is, byte[] b, int timeoutMillis) throws IOException {
        int bufferOffset = 0;
        long maxTimeMillis = System.currentTimeMillis() + timeoutMillis;
        while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
            int readLength = java.lang.Math.min(is.available(), b.length - bufferOffset);
            if (readLength > 0) {
                // can alternatively use bufferedReader, guarded by isReady():
                int readResult = is.read(b, bufferOffset, readLength);
                if (readResult == -1)
                    break;
                bufferOffset += readResult;
            }
        }
        return bufferOffset;
    }

    public static int readBytesDetectComming(InputStream is, byte[] b, int timeoutMillisBetweenReads) throws IOException {
        int bufferOffset = 0;
        long maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
        while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
            int readLength = java.lang.Math.min(is.available(), b.length - bufferOffset);
            if (readLength > 0) {
                // can alternatively use bufferedReader, guarded by isReady():
                int readResult = is.read(b, bufferOffset, readLength);
                if (readResult == -1) {
                    break;
                }
                if (readResult > 0) {
                    maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
                }
                bufferOffset += readResult;
            }
        }
        return bufferOffset;
    }

    public static int readBytesDetectComming(InputStream is, byte[] b, int timeoutMillisBetweenReads, ReadableProgressStatus<Long> progressStatus) throws IOException {
        Long originProgress = progressStatus.getProgressValue();
        int bufferOffset = 0;
        long maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
        while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
            int readLength = java.lang.Math.min(is.available(), b.length - bufferOffset);
            if (readLength > 0) {
                // can alternatively use bufferedReader, guarded by isReady():
                int readResult = is.read(b, bufferOffset, readLength);
                if (readResult == -1) {
                    break;
                }
                if (readResult > 0) {
                    maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
                }
                bufferOffset += readResult;
                progressStatus.setProgressValue(originProgress + bufferOffset);
            }
        }
        return bufferOffset;
    }

    public static int readBytesDetectCommingNoEx(InputStream is, byte[] b, int timeoutMillisBetweenReads) {
        int bufferOffset = 0;
        try {
            long maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
            while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
                int readLength = java.lang.Math.min(is.available(), b.length - bufferOffset);
                // can alternatively use bufferedReader, guarded by isReady():
                if (readLength > 0) {
                    int readResult = is.read(b, bufferOffset, readLength);
                    if (readResult == -1) {
                        break;
                    }
                    if (readResult > 0) {
                        maxTimeMillis = System.currentTimeMillis() + timeoutMillisBetweenReads;
                    }
                    bufferOffset += readResult;
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Expected to read " + b.length + " bytes but only " + bufferOffset + " bytes came");
        }
        return bufferOffset;
    }

    public static String readTextWithTimeout(InputStream inputStream, int activityTimeout, String charset) throws IOException {
        byte[] bytes = new byte[8192];

        StringBuilder sb = new StringBuilder();
        int count = 0;
        while ((count = InputStreamReadUtil.readBytesDetectComming(inputStream, bytes, activityTimeout)) == bytes.length) {
            sb.append(new String(bytes, 0, count, charset));
        }
        sb.append(new String(bytes, 0, count, charset));
        String content = sb.toString();

        return content;
    }

    public static byte[] readBytesWithTimeout(InputStream inputStream, int activityTimeout) throws IOException {
        byte[] bytes = new byte[8192];

        int count;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while ((count = InputStreamReadUtil.readBytesDetectComming(inputStream, bytes, activityTimeout)) == bytes.length) {
            byteArrayOutputStream.write(bytes, 0, count);
        }
        byteArrayOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }
}
