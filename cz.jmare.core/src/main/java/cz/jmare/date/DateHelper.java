package cz.jmare.date;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


public class DateHelper {
    public static LocalDate getLocalDateDetectNearestYear(int month, int day, LocalDate againstDate) {
        int year = againstDate.getYear();
        int previousYear = year - 1;
        int nextYear = year + 1;

        LocalDate thisYearLocalDate = null;
        long thisBetween = Long.MAX_VALUE;
        try {
            thisYearLocalDate = LocalDate.of(year, month, day);
            thisBetween = Math.abs(ChronoUnit.DAYS.between(againstDate, thisYearLocalDate));
        } catch (Exception e) {
        }

        LocalDate previousYearLocalDate = null;
        long previousBetween = Long.MAX_VALUE;
        try {
            previousYearLocalDate = LocalDate.of(previousYear, month, day);
            previousBetween = Math.abs(ChronoUnit.DAYS.between(againstDate, previousYearLocalDate));
        } catch (Exception e) {
        }

        LocalDate nextYearLocalDate = null;
        long nextBetween = Long.MAX_VALUE;
        try {
            nextYearLocalDate = LocalDate.of(nextYear , month, day);
            nextBetween = Math.abs(ChronoUnit.DAYS.between(againstDate, nextYearLocalDate));
        } catch (Exception e) {
        }

        LocalDate resultLocalDate = previousYearLocalDate;
        if (thisBetween < previousBetween) {
            resultLocalDate = thisYearLocalDate;
        }
        if (nextBetween < thisBetween) {
            resultLocalDate = nextYearLocalDate;
        }

        if (resultLocalDate == null) {
            throw new IllegalArgumentException("Unable to detect LocalDate for month " + month + " and day " + day);
        }

        return resultLocalDate;
    }

    public static LocalDate getLocalDateDetectNearestYear(int month, int day) {
        LocalDate now = LocalDate.now();
        return getLocalDateDetectNearestYear(month, day, now);
    }

    public static void main(String[] args) {
        System.out.println(getLocalDateDetectNearestYear(2, 29, LocalDate.of(2017, 2, 1))); // 2016-02-29
        System.out.println(getLocalDateDetectNearestYear(1, 11, LocalDate.of(2018, 4, 1))); // 2017-11-01
    }
}
