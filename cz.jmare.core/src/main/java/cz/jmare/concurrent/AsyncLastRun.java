package cz.jmare.concurrent;

import java.io.IOException;

/**
 * Start runnable in thread but when later runnable should run the current one is interrupted.
 * Only last provided Runnable should run
 * Note: Runnable should finish as soon as possible when isInterrupt()==true otherwise the next processRunnable will wait
 */
public class AsyncLastRun {
    private volatile Thread runnableThread;
    private volatile Runnable processRunnable;
    private Thread checkingThread;
    private long checkIntervalMillis = 30;
    private long timestamp = System.currentTimeMillis();
    private String name = null;

    public AsyncLastRun() {
        this(null);
    }

    public AsyncLastRun(String name) {
        super();
        this.checkingThread = new Thread(new CheckDoneRunnable(), (name != null ? name + "-" : "") + "CheckDoneRunnable-" + timestamp);
        this.checkingThread.start();
    }

    public AsyncLastRun(String name, long checkIntervalMillis) {
        this(name);
        this.checkIntervalMillis = checkIntervalMillis;
    }

    public synchronized void run(Runnable runnable) {
        if (this.processRunnable != null && runnableThread != null) {
            runnableThread.interrupt();
        }
        this.processRunnable = runnable;
    }

    public void destroy() {
        this.checkingThread.interrupt();
        if (runnableThread != null) {
            runnableThread.interrupt();
        }
    }

    private synchronized void submit() {
        if (processRunnable != null) {
            try {
                runnableThread = new Thread(processRunnable, (name != null ? name + "-" : "") + "AsyncLastRun-" + timestamp);
                runnableThread.start();
                this.processRunnable = null;
            } catch (Exception e) {
            }
        }
    }

    private class CheckDoneRunnable implements Runnable {
        @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()) {
                if (processRunnable != null) {
                    if (runnableThread == null || !runnableThread.isAlive()) {
                        submit();
                    } else {
                        runnableThread.interrupt();
                    }
                }
                try {
                    Thread.sleep(checkIntervalMillis);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        AsyncLastRun asyncLastRun = new AsyncLastRun();

        asyncLastRun.run(new TestRunnableImplementation("a"));
        Thread.sleep(100);
        asyncLastRun.run(new TestRunnableImplementation("b"));
        Thread.sleep(300);
        asyncLastRun.run(new TestRunnableImplementation("c"));
        Thread.sleep(10);
        System.out.println("closing");
        asyncLastRun.destroy();
        System.out.println("closed");
    }
}
