package cz.jmare.concurrent;

final class TestRunnableImplementation implements Runnable {
    String name;

    public TestRunnableImplementation(String name) {
        super();
        this.name = name;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis() + 1000;
        long lastTime = 0;
        while (System.currentTimeMillis() < end) {
            if (lastTime != System.currentTimeMillis()) {
                System.out.println(name + ":" + (System.currentTimeMillis() - start));
                lastTime = System.currentTimeMillis();
            }
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("Interrupt " + name);
                return;
            }
        }
        System.out.println("Regullary finished " + name);
    }

    @Override
    public String toString() {
        return "RunnableImplementation [name=" + name + "]";
    }
}
