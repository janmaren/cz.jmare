package cz.jmare.upath;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UPathUtil {
    private final static Pattern WINDOWS_DRIVE_SOMEWHERE = Pattern.compile("[a-zA-Z]*:");

    private final static Pattern FILE_HOST_PATH = Pattern.compile("^(file://[a-zA-Z0-9]+/)");

    private final static Pattern PROTOCOL_PATH = Pattern.compile("^([a-zA-Z][a-zA-Z]+)://");

    /**
     * Concat base path and relative extension
     * @param baseUpath
     * @param extension
     * @return
     */
    public static UPath resolve(UPath baseUpath, String extension) {
        if (isAbsolute(extension)) {
            throw new IllegalArgumentException("argument musn't be absolute as " + extension);
        }
        extension = extension.replace("\\", "/");
        String basepathString = baseUpath.toString();
        if (basepathString.isEmpty()) {
            throw new RuntimeException("Invalid empty UPath URI");
        }
        if (basepathString.endsWith("/")) {
            return UPathFactory.getUPath(basepathString + extension);
        }
        return UPathFactory.getUPath(basepathString + "/" + extension);
    }

    /**
     * Concat base path and relative extension
     * @param baseUpath
     * @param extension
     * @return
     */
    public static String resolve(String baseUpath, String extension) {
        extension = extension.replace("\\", "/");
        if (isAbsolute(extension)) {
            throw new IllegalArgumentException("argument musn't be absolute as " + extension);
        }
        String basepath = baseUpath.toString();
        if (basepath.isEmpty()) {
            return extension;
        }
        if (basepath.endsWith("/")) {
            return basepath + extension;
        }
        return basepath + "/" + extension;
    }

    /**
     * True when absolute address passed
     * @param address
     * @return
     */
    public static boolean isAbsolute(String address) {
        return (address.startsWith("/") || address.startsWith("\\") || WINDOWS_DRIVE_SOMEWHERE.matcher(address).find());
    }

    /**
     * Return parent or null when it hasn't parent
     * @param upath
     * @return
     */
    public static UPath getParent(UPath upath) {
        String path = upath.toString();
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        int lastIndexOf = path.lastIndexOf("/");
        if (lastIndexOf == -1) {
            return null;
        }
        return UPathFactory.getUPath(path.substring(0, lastIndexOf));
    }

    /**
     * Return parent or null when it hasn't parent
     * @param upath
     * @return
     */
    public static String getParent(String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        int lastIndexOf = path.lastIndexOf("/");
        if (lastIndexOf == -1) {
            return null;
        }
        return path.substring(0, lastIndexOf);
    }

    /**
     * Replace .. by directory name where possible. Convert host to lower case.
     * @param path
     * @return
     */
    public static String normalize(String path) {
        UrlParts urlParts = getUrlParts(path);

        String[] split = urlParts.relativeRemainder.split("/");
        ArrayList<String> parts = new ArrayList<String>(Arrays.asList(split));

        for (int i = 0; i < parts.size(); i++) {
            String part = parts.get(i);
            if (part.equals("..")) {
                if (i > 0) {
                    parts.remove(i - 1);
                    parts.remove(i - 1);
                    i -= 2;
                }
            } else if (part.equals(".")) {
                parts.remove(i - 1);
                i--;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.size(); i++) {
            if (i > 0) {
                sb.append("/");
            }
            sb.append(parts.get(i));
        }

        if (urlParts.absolutePrefix.equals("file://localhost/")) {
            urlParts.absolutePrefix = "file:///";
        }
        urlParts.absolutePrefix = urlParts.absolutePrefix.toLowerCase();

        return urlParts.absolutePrefix + sb.toString();
    }

    /**
     * for file://localhost/c:/t1/t2/b3/b4 returns file://localhost/c:/ and t1/t2/b3/b4<br/>
     * for file:///c:/t1/t2/b3/b4 returns file:///c:/ and t1/t2/b3/b4<br/>
     * for c:/t1/t2/b3/b4 returns c:/ and  t1/t2/b3/b4
     * for file://host/c:/etc/fstab returns file://host/c:/ and etc/fstab
     * for file://host/etc/fstab returns file://host/ and etc/fstab
     * @param path
     * @return
     */
    public static UrlParts getUrlParts(String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        path = path.replace('\\', '/');
        String prefix = "";
        String relativePath = "";
        if (path.startsWith("file://localhost/")) {
            prefix = "file:///";
            relativePath = path.substring("file://localhost/".length());
        } else if (path.startsWith("file:///")) {
            prefix = "file:///";
            relativePath = path.substring("file:///".length());
        } else if (FILE_HOST_PATH.matcher(path).find()) {
            Matcher matcher = FILE_HOST_PATH.matcher(path);
            matcher.find();
            prefix = path.substring(0, matcher.group(1).length());
            relativePath = path.substring(matcher.group(1).length());
        } else if (isAbsolute(path)) {
            int indexOf = path.indexOf(':');
            if (indexOf >= 0) {
                relativePath = path;
                // najdeme prvni nelomitko
                for (int i = indexOf + 1; i < path.length(); i++) {
                    if (path.charAt(i) != '/') {
                        prefix = path.substring(0, i);
                        relativePath = path.substring(i);
                        if (!isAbsolute(relativePath)) {
                            // not case file:///c:/foo
                            break;
                        } else {
                            // case file:///c:/foo - need to get behind c:/ - continue
                            i = path.indexOf("/", i);
                            if (i < 0) {
                                throw new RuntimeException("Detected absolute path but no '/' found");
                            }
                        }
                    }
                }
            } else {
                int i = indexOf + 1;
                for (; i < path.length(); i++) {
                    if (path.charAt(i) != '/') {
                        prefix = path.substring(0, i);
                        relativePath = path.substring(i);
                        break;
                    }
                }
                if (i == path.length()) {
                    throw new RuntimeException("Not found slash in absolute path");
                }
            }
        } else {
            relativePath = path;
        }

        UrlParts urlParts = new UrlParts();
        urlParts.absolutePrefix = prefix;
        urlParts.relativeRemainder = relativePath;

        return urlParts;
    }

    public static class UrlParts {
        /**
         * In case of absoulute path:<br/>
         * file://<br/>
         * file:///<br/>
         * file:///c:/<br/>
         * c:/<br/>
         * smb://<br/>
         * or empty string for relative path
         */
        public String absolutePrefix;

        /**
         * Relative path or empty string when no relative path exists
         */
        public String relativeRemainder;

        @Override
        public String toString() {
            return absolutePrefix + relativeRemainder;
        }
    }

    /**
     * Return relative address how to get from uriFrom to uriTo
     * @param uriFrom
     * @param uriTo
     * @return
     */
    public static String relativize(String uriFrom, String uriTo) {
        UrlParts urlBasicParts = getUrlParts(uriFrom);
        UrlParts urlSimilarParts = getUrlParts(uriTo);
        if (!urlBasicParts.absolutePrefix.toLowerCase().equals(urlSimilarParts.absolutePrefix.toLowerCase())) {
            throw new IllegalArgumentException("Different filesystems passed");
        }
        String basicNormalized = normalize(urlBasicParts.relativeRemainder);
        String similarNormalized = normalize(urlSimilarParts.relativeRemainder);

        String[] fromSplit = basicNormalized.split("/");
        String[] toSplit = similarNormalized.split("/");

        ArrayList<String> absPath = new ArrayList<String>();

        int diffIndex = 0;
        for (; diffIndex < toSplit.length && diffIndex < fromSplit.length; diffIndex++) {
            if (!fromSplit[diffIndex].equals(toSplit[diffIndex])) {
                break;
            }
        }
        for (int i = fromSplit.length - 1; i >= diffIndex; i--) {
            absPath.add("..");
        }
        for (int i = diffIndex; i < toSplit.length; i++) {
            absPath.add(toSplit[i]);
        }

        String absPathStr = absPath.stream().collect(Collectors.joining("/"));

        return absPathStr;
    }

    /**
     * Return path including protocol prefix. The file:// protocol is the default
     * @param path
     * @return
     */
    public static String urize(String path) {
        if (path.startsWith("file://")) {
            return path;
        }
        path = normalize(path);
        if (PROTOCOL_PATH.matcher(path).find()) {
            return path;
        }
        return "file:///" + path;
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        UrlParts urlParts = getUrlParts("file://host/etc/fstab");
        System.out.println("Prefix: " + urlParts.absolutePrefix);
        System.out.println("Suffix: " + urlParts.relativeRemainder);
    }
}
