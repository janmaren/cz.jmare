package cz.jmare.upath;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface UPath {
    boolean isDirectory();

    boolean isFile();

    void write(byte[] bytes) throws IOException;

    void delete() throws IOException;

    InputStream createInputStream() throws IOException;

    OutputStream createOutputStream() throws IOException;

    void renameTo(String uri) throws IOException;

    void createDirectories() throws IOException;

    long size() throws IOException;

    void setCreationTime(long millis) throws IOException;

    void setModificationTime(long millis) throws IOException;

    long getCreationTime() throws IOException;

    long getModificationTime() throws IOException;
}
