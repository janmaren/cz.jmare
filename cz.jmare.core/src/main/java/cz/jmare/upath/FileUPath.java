package cz.jmare.upath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;

public class FileUPath implements UPath {

    private Path path;

    private String uri;

    public FileUPath(String uri) {
        uri = uri.replace('\\', '/');
        this.path = Paths.get(uri);
        this.uri = uri;
    }

    public FileUPath(Path path) {
        this.path = path;
        this.uri = path.toString();
    }

    @Override
    public boolean isDirectory() {
        return Files.isDirectory(path);
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        Files.write(path, bytes);
    }

    @Override
    public void delete() throws IOException {
        Files.delete(path);
    }

    @Override
    public InputStream createInputStream() throws IOException {
        return new FileInputStream(path.toFile());
    }

    @Override
    public OutputStream createOutputStream() throws IOException {
        return new FileOutputStream(path.toFile());
    }

    @Override
    public void renameTo(String uri) throws IOException {
        boolean renameTo = path.toFile().renameTo(new File(uri));
        if (!renameTo) {
            throw new IOException();
        }
    }


    @Override
    public String toString() {
        return uri;
    }

    @Override
    public boolean isFile() {
        return Files.isRegularFile(path);
    }

    @Override
    public void createDirectories() throws IOException {
        Files.createDirectories(path);
    }

    @Override
    public long size() throws IOException {
        return Files.size(path);
    }

    @Override
    public void setCreationTime(long millis) throws IOException {
        Files.setAttribute(path, "creationTime", FileTime.fromMillis(millis));
    }

    @Override
    public void setModificationTime(long millis) throws IOException {
        Files.setAttribute(path, "lastModifiedTime", FileTime.fromMillis(millis));
    }

    @Override
    public long getCreationTime() throws IOException {
        long millis = ((FileTime) Files.getAttribute(path, "creationTime")).toMillis();
        return millis;
    }

    @Override
    public long getModificationTime() throws IOException {
        long millis = ((FileTime) Files.getAttribute(path, "lastModifiedTime")).toMillis();
        return millis;
    }
}
