package cz.jmare.upath;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.InvalidPathException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UPathFactory {
    private final static Pattern PROTOCOL_URL = Pattern.compile("^([a-zA-Z0-9][a-zA-Z0-9]+)://");

    public static UPath getUPath(String uri) throws InvalidPathException {
        if (uri.isEmpty()) {
            throw new IllegalArgumentException("Invalid empty URI");
        }
        if (!UPathUtil.isAbsolute(uri)) {
            throw new IllegalArgumentException("Relative address " + uri + " passed");
        }
        Matcher matcher = PROTOCOL_URL.matcher(uri);
        if (matcher.find()) {
            String protocol = matcher.group(1);
            String classPrefix = protocol.substring(0, 1).toUpperCase() + protocol.substring(1);
            String className = "cz.jmare.upath." + classPrefix + "UPath";
            try {
                Class<?> c = Class.forName(className);
                Constructor<?> ctor = c.getConstructor(String.class);
                UPath upath = (UPath) ctor.newInstance(new Object[] { uri });
                return upath;
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalArgumentException("Not found class to handle URI " + uri, e);
            }
        }

        return new FileUPath(uri);
    }
}
