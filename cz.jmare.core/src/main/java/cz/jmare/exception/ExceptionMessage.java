package cz.jmare.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ExceptionMessage {
    public static String getCombinedMessage(Exception e) {
        String exceptionName = e.getClass().getSimpleName();
        List<String> tokens = decamelString(exceptionName);

        StringJoiner stringJoiner = new StringJoiner(" ");
        for (int i = 0; i < tokens.size(); i++) {
            String token = tokens.get(i);
            if (token.equalsIgnoreCase("exception")) {
                token = "error";
            }
            if (i == 0) {
                token = token.substring(0, 1).toUpperCase() + token.substring(1).toLowerCase();
            } else {
                token = token.toLowerCase();
            }
            stringJoiner.add(token);
        }
        StringBuilder sb = new StringBuilder(stringJoiner.toString());
        if (e.getLocalizedMessage() != null) {
            sb.append(" - ").append(e.getLocalizedMessage());
        } else if (e.getMessage() != null) {
            sb.append(" - ").append(e.getMessage());
        }
        return sb.toString();
    }

    public static List<String> decamelString(String str) {
        ArrayList<String> tokens = new ArrayList<String>();
        for (String token : str.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            tokens.add(token);
        }
        return tokens;
    }

    public static String getCombinedMessage(String mainMessage, Exception e) {
        if (e.getMessage() != null) {
            return mainMessage + ": " + e.getMessage();
        }
        return mainMessage;
    }

    public static void main(String[] args) {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Neni to legalni");
        String combinedMessage = getCombinedMessage(illegalArgumentException);
        System.out.println(combinedMessage);
    }
}
