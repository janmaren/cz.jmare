package cz.jmare.http;

import java.io.IOException;
import java.io.OutputStream;

public class BinaryPopulateBodyConsumer implements PopulateBodyConsumer {
    private byte[] body;

    public BinaryPopulateBodyConsumer(byte[] body) {
        super();
        this.body = body;
    }

    @Override
    public void accept(OutputStream outputStream, String charset) throws IOException {
        outputStream.write(body);
        outputStream.close();
    }
}
