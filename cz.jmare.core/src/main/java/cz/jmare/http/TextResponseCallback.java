package cz.jmare.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import cz.jmare.file.FileUtil;

public class TextResponseCallback implements ResponseCallback {
    private String response;

    @Override
    public void processResponse(RequestData requestData,  Map<String, String> headerItems, InputStream inputStream) {
        String responseCharset = headerItems.get("content-encoding");
        String responseContentType = headerItems.get("content-type");

        String charset = null;
        if (responseCharset != null) {
            charset = responseCharset;
        } else {
            if (responseContentType != null) {
                charset = extractCharsetFromContentType(responseContentType, charset);
            }
            if (charset == null) {
                charset = detectDefautCharset(responseContentType);
            }
        }

        if (charset != null) {
            try {
                response = FileUtil.getFileTextSafe(inputStream, charset);
            } catch (IOException e) {
                // Probably no response data exists
            }
        } else {
            try {
                response = FileUtil.getFileTextSafe(inputStream, "US-ASCII");
                //LoggerSlf4jOptional.logWarn("Unable to determine charset for response - used UTF-8");
            } catch (IOException e) {
                // Probably no response data exists
            }
        }
    }

    private String detectDefautCharset(String responseContentType) {
        if ("application/json".equals(responseContentType)) {
            return "UTF-8";
        }
        return null;
    }

    private String extractCharsetFromContentType(String responseContentType, String charset) {
        int indexOf = responseContentType.indexOf(";");
        if (indexOf != -1 && responseContentType.length() > indexOf + 1) {
            String charsetAssigment = responseContentType.substring(indexOf + 1).trim();
            if (charsetAssigment.length() > 0) {
                int indexOf2 = charsetAssigment.indexOf("=");
                if (indexOf2 != -1) {
                    charset = charsetAssigment.substring(indexOf2 + 1);
                }
            }
        }
        return charset;
    }

    public String getResponse() {
        return response;
    }
}
