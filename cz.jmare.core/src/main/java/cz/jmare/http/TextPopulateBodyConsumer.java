package cz.jmare.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class TextPopulateBodyConsumer implements PopulateBodyConsumer {
    private String text;

    public TextPopulateBodyConsumer(String text) {
        super();
        this.text = text;
    }

    @Override
    public void accept(OutputStream outputStream, String charset) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset);
        outputStreamWriter.write(text);
        outputStreamWriter.close();
    }
}
