package cz.jmare.http;

import java.io.InputStream;
import java.util.Map;

public interface ResponseCallback {
    void processResponse(RequestData requestData,  Map<String, String> headerItems, InputStream inputStream);
}
