package cz.jmare.http;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class KeystoreUtil {
    public static KeyStore getJKSKeyStore(String url, String password) {
        KeyStore keyStore;
        try {
            keyStore = KeyStore.getInstance("JKS");
            try (final InputStream is = new FileInputStream(url)) {
                try {
                    keyStore.load(is, password.toCharArray());
                } catch (NoSuchAlgorithmException | CertificateException e) {
                    throw new RuntimeException("Error during load", e);
                }
            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException("File " + url + " not accessible", e);
            } catch (IOException e) {
                throw new RuntimeException("An IO exception during load of " + url, e);
            }
            return keyStore;
        } catch (KeyStoreException e) {
            throw new RuntimeException("Java doesn't know JKS instance...");
        }
    }
}
