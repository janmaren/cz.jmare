package cz.jmare.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

import cz.jmare.collection.CaseInsensitiveMap;
import cz.jmare.logging.LoggerSlf4jOptional;

public class HttpClient {
    /**
     * Suitable for GET and POST
     * @param requestUrl
     * @param method
     * @param requestData
     * @return
     * @throws HttpIOException
     * @throws ErrorStatusException
     */
    public static String sendGetString(String requestUrl, String method, RequestData requestData) throws HttpIOException, ErrorStatusException {
        if (requestData == null) {
            requestData = new RequestData();
        }
        TextResponseCallback textResponseCallback = new TextResponseCallback();
        int resultCode = sendSupportRedirect(requestUrl, method, requestData, textResponseCallback);
        if (resultCode == 200 || resultCode == 201 || resultCode == 204 /* return null */) {
            if (resultCode == 204) {
                LoggerSlf4jOptional.logWarn("Client expected to get some data but response returned status code 204");
            }
            return textResponseCallback.getResponse();
        }
        throw new ErrorStatusException(resultCode, requestUrl);
    }

    /**
     * Suitable for PUT and DELETE
     * @param requestUrl
     * @param method
     * @param requestData
     * @throws HttpIOException
     * @throws ErrorStatusException
     */
    public static void send(String requestUrl, String method, RequestData requestData) throws HttpIOException, ErrorStatusException {
        if (requestData == null) {
            requestData = new RequestData();
        }
        int resultCode = sendSupportRedirect(requestUrl, method, requestData, null);
        if (resultCode != 200 && resultCode != 201 && resultCode != 202 && resultCode != 204) {
            throw new ErrorStatusException(resultCode, requestUrl);
        }
        if (resultCode == 200) {
            LoggerSlf4jOptional.logWarn("Client doesn't expected any response data but response returned status code 200");
        }
    }

    /**
     * Suitable for GET and POST. It returns data in binary format.
     * @param requestUrl
     * @param method
     * @param requestData
     * @return
     * @throws HttpIOException
     * @throws ErrorStatusException
     */
    public static byte[] sendGetBytes(String requestUrl, String method, RequestData requestData) throws HttpIOException, ErrorStatusException {
        return sendGetBytes(requestUrl, method, requestData, null);
    }

    public static byte[] sendGetBytes(String requestUrl, String method, RequestData requestData, Consumer<Map<String, String>> responseHeaderConsumer) throws HttpIOException, ErrorStatusException {
        if (requestData == null) {
            requestData = new RequestData();
        }
        BytesResponseCallback textResponseCallback = new BytesResponseCallback(responseHeaderConsumer);
        int resultCode = sendSupportRedirect(requestUrl, method, requestData, textResponseCallback);
        if (resultCode == 200 || resultCode == 201) {
            return textResponseCallback.getResponse();
        }
        throw new ErrorStatusException(resultCode, requestUrl);
    }

    public static int sendSupportRedirect(String requestUrl, String method, RequestData requestData, ResponseCallback responseCallback) throws ErrorStatusException, HttpIOException {
        try {
            return send(requestUrl, method, requestData, responseCallback);
        } catch (RedirectErrorStatusException e) {
            return send(e.getLocation(), method, requestData, responseCallback);
        }
    }

    /**
         * Process request, send data and process incoming data
         * @param requestUrl the url
         * @param method GET, POST, DELETE, ...
         * @param requestData headers
         * @param responseCallback result data are passed to appropriate implementation
         * @return http result code
         * @throws ErrorStatusException exception for http result codes different than 200
         * @throws HttpIOException when other error during communication occures. It's also returned when proxy is required but it's not passed
         */
    public static int send(String requestUrl, String method, RequestData requestData, ResponseCallback responseCallback) throws ErrorStatusException, HttpIOException {
        if (!requestUrl.startsWith("http://") && !requestUrl.startsWith("https://")) {
            throw new IllegalArgumentException("Not a http or https protocol");
        }

        URL url;
        try {
            url = new URI(requestUrl).toURL();
        } catch (MalformedURLException | URISyntaxException e1) {
            throw new IllegalArgumentException("requestUrl " + requestUrl + " has bad format", e1);
        }

        Proxy proxy = requestData.getProxy();
        SSLSocketFactory sslSf = requestData.getSslSocketFactory();
        Map<String, String> headerItems = requestData.getHeaderItems();

        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) (proxy != null ? url.openConnection(proxy) : url.openConnection());
            connection.setRequestMethod(method);

            if (connection instanceof HttpsURLConnection && sslSf != null) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) connection;
                httpsURLConnection.setSSLSocketFactory(sslSf);
            }

            for (Map.Entry<String, String> entry : headerItems.entrySet()) {
                connection.setRequestProperty(entry.getKey(), entry.getValue());
            }

            connection.setUseCaches(requestData.isUseCaches());

            if (requestData.getFollowRedirects() != null) {
                connection.setInstanceFollowRedirects(requestData.getFollowRedirects());
            }

            if (requestData.getConnectTimeout() != null) {
                connection.setConnectTimeout(requestData.getConnectTimeout());
            }

            if (requestData.getReadTimeout() != null) {
                connection.setReadTimeout(requestData.getReadTimeout());
            }

            if (requestData.getPopulateBodyConsumer() != null) {
                connection.setDoOutput(true);
                if (requestData.getPopulateBodyConsumer() instanceof FormUrlEncodedPopulateBodyConsumer) {
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                } else
                if (requestData.getCharset() != null && requestData.getHeaderItems().get("Content-Type") != null) {
                    connection.setRequestProperty("Content-Type", "text/plain; charset=" + requestData.getCharset());
                }
                requestData.getPopulateBodyConsumer().accept(connection.getOutputStream(), requestData.getCharset());
            } else
            if (requestData.getPopulateMultipartBodyConsumer() != null) {
                String boundary = "------------------------" + System.nanoTime();
                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                connection.setDoOutput(true);
                requestData.getPopulateMultipartBodyConsumer().accept(connection.getOutputStream(), boundary, requestData.getCharset());
            }

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                if (responseCallback != null) {
                    Map<String, String> responseHeaderItems = grabHeaderItems(connection);
                    responseCallback.processResponse(requestData, responseHeaderItems, connection.getInputStream());
                }
                return connection.getResponseCode();
            } else if (responseCode == HttpURLConnection.HTTP_NO_CONTENT || responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                return responseCode;
            } else {
                String errorStr = null;
                InputStream es = null;
                try {
                    es = connection.getErrorStream();
                    if (es != null) {
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        copyLarge(es, output);
                        if (connection.getContentEncoding() != null) {
                            errorStr = output.toString(connection.getContentEncoding());
                        } else {
                            errorStr = output.toString();
                        }
                    }
                } finally {
                    if (es != null) {
                        es.close();
                    }
                }
                if (responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                    throw new RedirectErrorStatusException(responseCode, requestUrl, connection.getResponseMessage(), errorStr, connection.getHeaderField("Location"));
                } else {
                    throw new ErrorStatusException(responseCode, requestUrl, connection.getResponseMessage(), errorStr);
                }
            }
        } catch(IOException e) {
            throw new HttpIOException(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private static Map<String, String> grabHeaderItems(HttpURLConnection connection) {
        Map<String, String> responseHeaderItems = new CaseInsensitiveMap<String>();
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        for (Entry<String, List<java.lang.String>> entry : headerFields.entrySet()) {
            String key = entry.getKey();
            List<java.lang.String> value = entry.getValue();
            if (value.size() == 1) {
                responseHeaderItems.put(key, value.get(0));
            }
        }
        return responseHeaderItems;
    }

    private static final long copyLarge(final InputStream input, final OutputStream output)
            throws IOException {
        byte[] buffer = new byte[8192];
        long count = 0;
        int n;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
}
