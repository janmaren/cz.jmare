package cz.jmare.http;

public class ErrorStatusException extends RuntimeException {
    private static final long serialVersionUID = 1184318782540905760L;

    private int status;

    private String otherMessage;

    private String url;

    public ErrorStatusException(int status, String url) {
        this.url = url;
        this.status = status;
    }

    public ErrorStatusException(int status, String url, String message, String responseMessage) {
        this.url = url;
        this.status = status;
        this.otherMessage = message != null ? (responseMessage != null ? message + ":" + responseMessage : message) : responseMessage;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "ErrorStatusException [status=" + status + ", responseMessage=" + otherMessage + ", getMessage()=" + getMessage() + "]";
    }

    public String getOtherMessage() {
        return otherMessage;
    }

    @Override
    public String getMessage() {
        return "Returned status code " + status + " after call " + url + (otherMessage != null ? " - " + otherMessage : "");
    }

	public String getUrl() {
		return url;
	}
}
