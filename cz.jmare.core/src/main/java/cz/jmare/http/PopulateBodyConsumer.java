package cz.jmare.http;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface to send simple single body
 */
public interface PopulateBodyConsumer {
    void accept(OutputStream outputStream, String charset) throws IOException;
}
