package cz.jmare.http;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Base64;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import cz.jmare.collection.CaseInsensitiveMap;

public class RequestData {
    private Map<String, String> headerItems = new CaseInsensitiveMap<String>();
    /**
     * proxy or null when no proxy should be used
     */
    private Proxy proxy;
    private SSLSocketFactory sslSocketFactory;
    private Boolean followRedirects;
    private boolean useCaches;
    private Integer connectTimeout;
    private Integer readTimeout;
    private PopulateMultipartBodyConsumer populateMultipartBodyConsumer;
    private PopulateBodyConsumer populateBodyConsumer;
    /**
     * Charset for text content. It may be null when binary file is sent.
     */
    private String charset;

    public RequestData() {
    }

    public RequestData(PopulateMultipartBodyConsumer populateBodyOutputStreamConsumer) {
        this.populateMultipartBodyConsumer = populateBodyOutputStreamConsumer;
    }

    public RequestData(PopulateBodyConsumer populateBodyConsumer) {
        this.populateBodyConsumer = populateBodyConsumer;
    }

    public Map<String, String> getHeaderItems() {
        return headerItems;
    }

    public Proxy getProxy() {
        return proxy;
    }

    public void setProxy(String hostname, int port) {
        proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
    }

    public void addHeaderItem(String key, String value) {
        headerItems.put(key, value);
    }

    public void addBasicAuthorizationHeaderItem(String username, String password) {
        headerItems.put("Authorization", "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes()));
    }

    public SSLSocketFactory getSslSocketFactory() {
        return sslSocketFactory;
    }

    public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {
        this.sslSocketFactory = sslSocketFactory;
    }

    public Boolean getFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(Boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public boolean isUseCaches() {
        return useCaches;
    }

    public void setUseCaches(boolean useCaches) {
        this.useCaches = useCaches;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public PopulateMultipartBodyConsumer getPopulateMultipartBodyConsumer() {
        return populateMultipartBodyConsumer;
    }

    public void setPopulateMultipartBodyConsumer(PopulateMultipartBodyConsumer populateMultipartBodyConsumer) {
        this.populateMultipartBodyConsumer = populateMultipartBodyConsumer;
    }

    public PopulateBodyConsumer getPopulateBodyConsumer() {
        return populateBodyConsumer;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public void setPopulateBodyConsumer(PopulateBodyConsumer populateBodyConsumer) {
        this.populateBodyConsumer = populateBodyConsumer;
    }
}
