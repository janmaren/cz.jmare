package cz.jmare.http;

public class HttpIOException extends RuntimeException {

    private static final long serialVersionUID = 310679793499671516L;

    public HttpIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpIOException(Throwable cause) {
        super(cause);
    }

}
