package cz.jmare.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.function.Consumer;

import cz.jmare.file.FileUtil;

public class BytesResponseCallback implements ResponseCallback {
    byte[] response;
    private Map<String, String> headerItems;
    private RequestData requestData;
    private Consumer<Map<String, String>> responseHeaderConsumer;

    public BytesResponseCallback() {
        super();
    }

    public BytesResponseCallback(Consumer<Map<String, String>> responseHeaderConsumer) {
        super();
        this.responseHeaderConsumer = responseHeaderConsumer;
    }

    @Override
    public void processResponse(RequestData requestData,  Map<String, String> headerItems, InputStream inputStream) {
        this.requestData = requestData;
        this.headerItems = headerItems;
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            FileUtil.copyLarge(inputStream, output);
            response = output.toByteArray();
            if (responseHeaderConsumer != null) {
                responseHeaderConsumer.accept(headerItems);
            }
        } catch (IOException e) {
            // probably used http method which doesn't return any data
        }
    }

    public byte[] getResponse() {
        return response;
    }

    public Map<String, String> getHeaderItems() {
        return headerItems;
    }

    public RequestData getRequestData() {
        return requestData;
    }
}
