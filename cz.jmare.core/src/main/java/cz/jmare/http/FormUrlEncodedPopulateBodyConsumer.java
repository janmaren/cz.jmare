package cz.jmare.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class FormUrlEncodedPopulateBodyConsumer implements PopulateBodyConsumer {
    private Map<String, String> params;

    public FormUrlEncodedPopulateBodyConsumer(Map<String, String> params) {
        super();
        this.params = params;
    }

    @Override
    public void accept(OutputStream outputStream, String charset) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset);
        outputStreamWriter.write(urlEncodeParams(params));
        outputStreamWriter.close();
    }

    private String urlEncodeParams(Map<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for(Map.Entry<String, String> entry : params.entrySet()){
                if (first) {
                    first = false;
                } else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }

        return result.toString();
    }
}
