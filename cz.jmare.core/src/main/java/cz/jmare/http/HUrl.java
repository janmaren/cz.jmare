package cz.jmare.http;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class HUrl {
    public static String getString(String url) {
        return HttpClient.sendGetString(url, "GET", new RequestData());
    }

    public static byte[] getByteArray(String url, Consumer<Map<String, String>> responseHeaderConsumer) {
        return HttpClient.sendGetBytes(url, "GET", new RequestData(), responseHeaderConsumer);
    }

    public static byte[] getByteArray(String url, RequestData requestData, Consumer<Map<String, String>> responseHeaderConsumer) {
        return HttpClient.sendGetBytes(url, "GET", requestData, responseHeaderConsumer);
    }

    public static byte[] getByteArray(String url) {
        return HttpClient.sendGetBytes(url, "GET", new RequestData());
    }

    public static String getString(String url, RequestData requestData) {
        return HttpClient.sendGetString(url, "GET", requestData);
    }

    public static byte[] getByteArray(String url, RequestData requestData) {
        return HttpClient.sendGetBytes(url, "GET", requestData);
    }

    public static String postTextReturnString(String url, String body, String charset, RequestData requestData) {
        requestData.setPopulateBodyConsumer(new TextPopulateBodyConsumer(body));
        requestData.setCharset(charset);
        String string = HttpClient.sendGetString(url, "POST", requestData);
        return string;
    }

    public static String postTextReturnString(String url, String body, String charset) {
        return postTextReturnString(url, body, charset, new RequestData());
    }

    public static String postTextReturnString(String url, String body) {
        return postTextReturnString(url, body, "UTF-8");
    }

    public static String postBinaryReturnString(String url, byte[] bytes) {
        RequestData requestData = new RequestData(new BinaryPopulateBodyConsumer(bytes));
        String string = HttpClient.sendGetString(url, "POST", requestData);
        return string;
    }

    public static String postFileMultipartReturnString(String url, String fieldName, File file) {
        RequestData requestData = new RequestData();
        return postFileMultipartReturnString(url, fieldName, file, requestData);
    }

    public static String postFileMultipartReturnString(String url, String fieldName, File file, RequestData requestData) {
        Map<String, File> map = new HashMap<String, File>();
        map.put(fieldName, file);
        requestData.setPopulateMultipartBodyConsumer(new SimplePopulateMultipartBodyConsumer(map));
        String string = HttpClient.sendGetString(url, "POST", requestData);
        return string;
    }

    public static String postFieldsFilesMultipartReturnString(String url, Map<String, String> fields, Map<String, File> files, RequestData requestData) {
        requestData.setPopulateMultipartBodyConsumer(new SimplePopulateMultipartBodyConsumer(fields, files));
        String string = HttpClient.sendGetString(url, "POST", requestData);
        return string;
    }

    public static void putText(String url, String body, String charset) {
        RequestData requestData = new RequestData(new TextPopulateBodyConsumer(body));
        requestData.setCharset(charset);
        HttpClient.send(url, "PUT", requestData);
    }

    public static void putText(String url, String body) {
        putText(url, body, "UTF-8");
    }

    public static void putBinary(String url, byte[] bytes) {
        RequestData requestData = new RequestData(new BinaryPopulateBodyConsumer(bytes));
        HttpClient.send(url, "PUT", requestData);
    }

    public static void putFileMultipart(String url, String fieldName, File file, RequestData requestData) {
        Map<String, File> map = new HashMap<String, File>();
        map.put(fieldName, file);
        requestData.setPopulateMultipartBodyConsumer(new SimplePopulateMultipartBodyConsumer(map));
        HttpClient.send(url, "PUT", requestData);
    }

    public static void putFileMultipart(String url, String fieldName, File file) {
        RequestData requestData = new RequestData();
        putFileMultipart(url, fieldName, file, requestData);
    }
}
