package cz.jmare.http;

public class RedirectErrorStatusException extends ErrorStatusException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4848544372501954992L;
	private final String location;

    public RedirectErrorStatusException(int status, String url, String message, String responseMessage, String location) {
        super(status, url, message, responseMessage);
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
