package cz.jmare.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SimplePopulateMultipartBodyConsumer implements PopulateMultipartBodyConsumer {
    private static final byte[] LINE_FEED = {13, 10};

    private Map<String, String> fields = new HashMap<>();

    private Map<String, File> files = new HashMap<>();


    public SimplePopulateMultipartBodyConsumer() {
        super();
    }

    public SimplePopulateMultipartBodyConsumer(Map<String, String> fields, Map<String, File> files) {
        super();
        this.fields = fields;
        this.files = files;
    }

    public SimplePopulateMultipartBodyConsumer(Map<String, File> files) {
        super();
        this.files = files;
    }

    protected final static void write(OutputStream outputStream, String str) throws IOException {
        outputStream.write(str.getBytes(StandardCharsets.ISO_8859_1));
    }

    protected final static void writeln(OutputStream outputStream, String str) throws IOException {
        outputStream.write(str.getBytes(StandardCharsets.ISO_8859_1));
        outputStream.write(LINE_FEED);
    }

    protected final static void writeln(OutputStream outputStream) throws IOException {
        outputStream.write(LINE_FEED);
    }

    @Override
    public void accept(OutputStream outputStream, String boundary, String charset) throws IOException {
        for (Entry<String, String> entry : fields.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            addFormField(outputStream, key, value, boundary, charset);
        }

        for (Entry<String, File> entry : files.entrySet()) {
            String key = entry.getKey();
            File value = entry.getValue();
            addFilePart(outputStream, key, value, boundary, charset);
        }

        writeln(outputStream, "--" + boundary + "--");

        outputStream.close();
    }

    public static void addFormField(OutputStream os, String name, String value, String boundary, String charset) throws IOException {
        writeln(os, "--" + boundary);
        writeln(os, "Content-Disposition: form-data; name=\"" + name + "\"");
        // ??? use when needed: writeln(os, "Content-Type: text/plain; charset= " + charset);
        writeln(os);
        writeln(os, value);
    }

    private void addFilePart(OutputStream os, String fieldName, File uploadFile, String boundary, String charset) throws IOException {
        String fileName = uploadFile.getName();
        writeln(os, "--" + boundary);
        writeln(os, "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"");
        String guessContentTypeFromName = URLConnection.guessContentTypeFromName(fileName);
        if (guessContentTypeFromName != null) {
            writeln(os, "Content-Type: " + guessContentTypeFromName);
        }
        writeln(os);

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            os.write(buffer, 0, bytesRead);
        }
        os.flush();
        inputStream.close();
        writeln(os);
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }
}
