package cz.jmare.http;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface to send multipart body
 * Note the boundary is pregenerated during header generation phase and it's passed here to be used
 */
public interface PopulateMultipartBodyConsumer {
    void accept(OutputStream outputStream, String boundary, String charset) throws IOException;
}
