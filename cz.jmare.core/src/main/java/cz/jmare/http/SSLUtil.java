package cz.jmare.http;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

public class SSLUtil {

    public static SSLSocketFactory createSimpleSSLSocketFactory(String keystorePath, String truststorePath, String keystorePassword, String truststorePassword)
            throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        KeyStore clientKeyStore = KeystoreUtil.getJKSKeyStore(keystorePath, keystorePassword);

        KeyManager[] km = null;
        if (keystorePath != null) {
            KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyFactory.init(clientKeyStore, keystorePassword.toCharArray());
            km = keyFactory.getKeyManagers();
        }

        TrustManager[] tm = null;
        if (truststorePath != null) {
            KeyStore clientTrustStore = KeystoreUtil.getJKSKeyStore(truststorePath, truststorePassword);
            TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustFactory.init(clientTrustStore);
            tm = trustFactory.getTrustManagers();
        }

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(km, tm, null);
        SSLSocketFactory sslSf = sslContext.getSocketFactory();
        return sslSf;
    }

}
