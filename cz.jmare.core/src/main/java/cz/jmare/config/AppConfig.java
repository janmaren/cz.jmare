package cz.jmare.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class AppConfig {
    private static final String DEFAULT_CONFIG_FILE_NAME = "config";

    private static AppConfig appConfig;

    private String appName;

    private Map<String, Object> configStores = new HashMap<String, Object>();

    private AppConfig() {
    }

    public AppConfig init(String appName) {
        if (this.appName != null) {
            throw new RuntimeException("Not allowed to change application");
        }
        this.appName = appName;
        return this;
    }

    public static AppConfig getInstance() {
        if (appConfig == null) {
            appConfig = new AppConfig();
        }
        return appConfig;
    }

    public ConfigStore getConfigStore() {
        return getConfigStore(DEFAULT_CONFIG_FILE_NAME);
    }

    public ConfigStore getConfigStore(String name) {
        ConfigStore configStore = (ConfigStore) configStores.get(name);
        if (configStore == null) {
            if (appName == null) {
                throw new RuntimeException("no application name passed by init method");
            }
            File appDirFile = getExistingDataDir();
            File file = new File(appDirFile, name);
            configStore = new ConfigStore(file);
            configStores.put(name, configStore);
        }
        return configStore;
    }

    public ListStore getListStore(String name) {
        ListStore listStore = (ListStore) configStores.get(name);
        if (listStore == null) {
            if (appName == null) {
                throw new RuntimeException("no application name passed by init method");
            }
            File appDirFile = getExistingDataDir();
            File file = new File(appDirFile, name);
            listStore = new ListStore(file);
            configStores.put(name, listStore);
        }
        return listStore;
    }

    public File getExistingDataDir() {
        String homeDir = System.getProperty("user.home");
        File homeDirFile = new File(homeDir);
        File appDirFile = new File(homeDirFile, appName);
        appDirFile.mkdirs();
        return appDirFile;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new RuntimeException("Clone not allowed");
    }
}
