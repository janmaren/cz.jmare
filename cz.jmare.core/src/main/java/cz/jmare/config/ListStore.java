package cz.jmare.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import cz.jmare.util.FileLinesUtil;

public class ListStore {
    private File textFile;

    List<String> list;

    public ListStore(File propertiesFile) {
        this.textFile = propertiesFile;
        load();
    }

    private void load() {
        try (InputStream is = new FileInputStream(textFile)) {
            list = FileLinesUtil.readLines(is, "UTF-8");
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
    }

    public void save() {
        try (OutputStream os = new FileOutputStream(textFile)) {
            FileLinesUtil.writeLines(os, list, "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setList(List<String> list) {
        this.list = list;
        save();
    }

    public List<String> getList() {
        return list;
    }
}
