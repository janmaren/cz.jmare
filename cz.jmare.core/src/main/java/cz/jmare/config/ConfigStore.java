package cz.jmare.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;


public class ConfigStore {
    private Properties props;

    private File propertiesFile;

    private boolean reloadBeforePut = true;

    public ConfigStore(File propertiesFile) {
        this.propertiesFile = propertiesFile;
        load();
    }

    public ConfigStore(File propertiesFile, boolean reloadBeforePut) {
        this.propertiesFile = propertiesFile;
        this.reloadBeforePut = reloadBeforePut;
        load();
    }

    private void load() {
        props = new Properties();
        if (propertiesFile.isFile()) {
            try {
                props.load(new FileInputStream(propertiesFile));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void save() {
        try (FileOutputStream os = new FileOutputStream(propertiesFile)) {
            props.store(os, "Configuration");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getValue(String key, String defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return val.trim();
    }

    public Integer getIntValue(String key, Integer defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return Integer.valueOf(val.trim());
    }

    public Enum<?> getEnumValue(String key, Enum<?> defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        Class<?> class1 = defaultVal.getClass();
        Enum<?> instEnum = instEnum(class1, val);
        if (instEnum == null) {
            return null;
        }
        return instEnum;
    }

    public Enum<?> instEnum(Class<?> clazz, String name) {
        try {
            Method valueOf = clazz.getMethod("valueOf", String.class);
            Object value = valueOf.invoke(null, name);
            return (Enum<?>) value;
        } catch ( ReflectiveOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean getBoolValue(String key, boolean defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return Boolean.valueOf(val.trim());
    }

    public void putBoolValue(String key, Boolean value) {
        putValue(key, value == null ? null : String.valueOf(value));
    }

    public void putIntValue(String key, Integer value) {
        putValue(key, value == null ? null : String.valueOf(value));
    }

    public void putEnumValue(String key, Enum<?> value) {
        putValue(key, value == null ? null : value.name());
    }

    public void remove(String key) {
        if (reloadBeforePut) {
            load();
        }
        props.remove(key);
        save();
    }

    public void putValue(String key, String value) {
        if (reloadBeforePut) {
            load();
        }
        if (value == null) {
            props.remove(key);
        } else {
            props.put(key, value);
        }
        save();
    }

    public ArrayList<String> getList(String key) {
        String stringList = getValue(key, "");
        if ("".equals(stringList)) {
            return new ArrayList<>();
        }
        String[] split = stringList.split("\t");
        ArrayList<String> result = new ArrayList<String>();
        for (String string : split) {
            string = string.replace("\0x00", "\t");
            result.add(string);
        }
        return result;
    }

    public void setList(String key, List<String> list) {
        StringJoiner stringJoiner = new StringJoiner("\t");
        for (String string : list) {
            string = string.replace("\t", "\0x00");
            stringJoiner.add(string);
        }
        putValue(key, stringJoiner.toString());
    }

    public void addAtBeginningOfListAndLimit(String key, String value, int limit) {
        ArrayList<String> list = getList(key);
        if (list.contains(value)) {
            list.remove(value);
        }
        list.add(0, value);
        while (list.size() > limit) {
            list.remove(list.size() - 1);
        }
        setList(key, list);
    }

    public String getPropertiesFile() {
        return propertiesFile.toString();
    }

    public double getDoubleValue(String key, double defaultVal) {
        String val = (String) props.get(key);
        if (val == null) {
            return defaultVal;
        }
        return Double.valueOf(val.trim());
    }

    public void putDoubleValue(String key, Double value) {
        putValue(key, value == null ? null : String.valueOf(value));
    }

    public String getFromBeginningOfList(String key, String defaultValue) {
        ArrayList<String> list = getList(key);
        if (list.size() > 0) {
            return list.get(0);
        }
        return defaultValue;
    }
}
