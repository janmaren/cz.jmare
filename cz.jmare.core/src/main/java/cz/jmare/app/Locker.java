package cz.jmare.app;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class Locker {
    private File lockFile;
    private FileChannel lockChannel;
    private FileLock lock;

    public Locker(File f) {
        this.lockFile = f;
    }

    /**
     * Returns true when lock was created and false when lock already exists
     * It also automatically unlocks when application finishes
     * @return
     */
    @SuppressWarnings("resource")
    public boolean tryLock() {
        try {
            // Check if the lock exist
            if (lockFile.exists()) {
                // if exist try to delete it
                lockFile.delete();
            }
            // Try to get the lock
            lockChannel = new RandomAccessFile(lockFile, "rw").getChannel();

            lock = lockChannel.tryLock();
            if (lock == null) {
                // File is lock by other application
                lockChannel.close();
                return false;
            }
            // Add shutdown hook to release lock when application shutdown
            ShutdownHook shutdownHook = new ShutdownHook();
            Runtime.getRuntime().addShutdownHook(shutdownHook);

            return true;
        } catch (IOException e) {
            throw new RuntimeException("Could not start process.", e);
        }
    }

    public void unlockFile() {
        // release and delete file lock
        try {
            if (lock != null)
                lock.release();
            lockChannel.close();
            lockFile.delete();
        } catch (Exception e) {
            // maybe already unlocked
        }
    }

    class ShutdownHook extends Thread {
        @Override
        public void run() {
            unlockFile();
        }
    }
}
