package cz.jmare.util;

public class ByteCodeDebugUtil {
    public static String toCppArrayStr(byte[] byteCode) {
        StringBuilder sb = new StringBuilder("uint8_t byteCode[] = {");
        sb.append("        ");
        for (int i = 0; i < byteCode.length; i++) {
            if (i % 16 == 0) {
                sb.append("\n");
                sb.append("        ");
            }
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("0x");
            sb.append(HexUtil.asHex(byteCode[i]));
        }
        sb.append("};");

        return sb.toString();
    }
}
