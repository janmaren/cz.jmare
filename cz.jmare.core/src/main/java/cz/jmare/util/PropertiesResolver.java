package cz.jmare.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.InvalidPropertiesFormatException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesResolver {
    private final static Pattern PLACEHOLDER_PATTERN = Pattern.compile("\\$\\{([\\w\\.]+)\\}");

    private File propertyFile;

    private Properties properties;

    public PropertiesResolver(File propertyFile) {
        super();
        this.propertyFile = propertyFile;
        properties = new Properties();
    }

    public Properties getProperties() throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
        properties.load(new FileInputStream(propertyFile));
        resolvePlaceholders();
        return properties;
    }

    private void resolvePlaceholders() throws InvalidPropertiesFormatException {
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        for (Entry<Object, Object> entry : entrySet) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            Matcher matcher = PLACEHOLDER_PATTERN.matcher(value);
            while (matcher.find()) {
                String innerKey = matcher.group(1);
                HashSet<String> alreadySolvedKeys = new HashSet<String>();
                alreadySolvedKeys.add(key);
                String innerValue = findRecursiveValue(innerKey, alreadySolvedKeys);
                value = value.replace(matcher.group(), innerValue);

                matcher = PLACEHOLDER_PATTERN.matcher(value);
            }
            properties.put(key, value.trim());
        }
    }

    private String findRecursiveValue(String key, Set<String> alreadySolvedKeys) throws InvalidPropertiesFormatException {
        if (alreadySolvedKeys.contains(key)) {
            throw new InvalidPropertiesFormatException("Found cyclic properties, problematic key is: " + key);
        }
        String value = properties.getProperty(key);
        if (value == null) {
            throw new InvalidPropertiesFormatException("Used placeholder ${" + key + "} but not found a value for such key");
        }
        Matcher matcher = PLACEHOLDER_PATTERN.matcher(value);
        StringBuilder result = new StringBuilder();
        int idxOld = 0;
        int startIdx = 0;
        alreadySolvedKeys.add(key);
        while (matcher.find()) {
            String innerKey = matcher.group(1);
            String findValue = findRecursiveValue(innerKey, alreadySolvedKeys);

            idxOld = matcher.start();
            result.append(value.substring(startIdx, idxOld));
            result.append(findValue);
            startIdx = idxOld + matcher.group(0).length();
        }
        alreadySolvedKeys.remove(key);
        if (startIdx == 0) {
            return value; //nothing found
        }
        result.append(value.substring(startIdx));
        return result.toString();
    }
}
