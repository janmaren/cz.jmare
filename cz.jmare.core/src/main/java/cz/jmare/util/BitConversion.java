package cz.jmare.util;

public class BitConversion {
    public static void main(String[] args) {
        byte b = (byte) 126;
        System.out.println(HexUtil.asBinaryString(b));
        b = (byte) 127;
        System.out.println(HexUtil.asBinaryString(b));
        b = (byte) 128;
        System.out.println(HexUtil.asBinaryString(b));
        b = (byte) 129;
        System.out.println(HexUtil.asBinaryString(b));
        b = (byte) 255;
        System.out.println(HexUtil.asBinaryString(b));

        short i = convertToShortUnsigned(b);
        System.out.println(HexUtil.asBinaryString(i));

        short sh = 1025;
        System.out.println(HexUtil.asBinaryString(sh));
        byte[] shortToLittleEndian = shortToLittleEndian(sh);
        short sh2 = littleEndianToShort(shortToLittleEndian);
        System.out.println(HexUtil.asBinaryString(sh2));
    }



    public static int convertToIntUnsigned(byte b) {
        return Byte.toUnsignedInt(b);
    }

    public static short convertToShortUnsigned(byte x) {
        return (short) (((short) x) & 0xff);
    }

    public static byte[] shortToLittleEndian(short x) {
        byte[] ret = new byte[2];
        ret[0] = (byte) x;
        ret[1] = (byte) (x >> 8);
        return ret;
    }

    public static void shortToLittleEndian(short x, byte[] bytes, int offset) {
        bytes[offset] = (byte) x;
        bytes[offset + 1] = (byte) (x >> 8);
    }

    public static byte[] shortToBigEndian(short x) {
        byte[] ret = new byte[2];
        ret[1] = (byte) x;
        ret[0] = (byte) (x >> 8);
        return ret;
    }

    public static void shortToBigEndian(short x, byte[] bytes, int offset) {
        bytes[offset + 1] = (byte) x;
        bytes[offset] = (byte) (x >> 8);
    }

    public static short littleEndianToShort(byte[] bytes) {
        short s = (short) (bytes[1] << 8 | bytes[0] & 0xff);
        return s;
    }

    public static short littleEndianToShort(byte[] bytes, int offset) {
        short s = (short) (bytes[offset + 1] << 8 | bytes[offset] & 0xff);
        return s;
    }

    public static short bigEndianToShort(byte[] bytes) {
        short s = (short) (bytes[0] << 8 | bytes[1] & 0xff);
        return s;
    }

    public static short bigEndianToShort(byte[] bytes, int offset) {
        short s = (short) (bytes[offset] << 8 | bytes[offset + 1] & 0xff);
        return s;
    }

    public static int littleEndianToInt2Bytes(byte[] bytes) {
        int number = 0;
        number |= bytes[1] & 0xFF;
        number <<= 8;
        number |= bytes[0] & 0xFF;
        return number;
    }

    public static int littleEndianToIntBytes(byte[] bytes) {
        int number = 0;
        number |= bytes[3] & 0xFF;
        number <<= 8;
        number |= bytes[2] & 0xFF;
        number <<= 8;
        number |= bytes[1] & 0xFF;
        number <<= 8;
        number |= bytes[0] & 0xFF;
        return number;
    }

    public static int littleEndianToIntBytes(byte[] bytes, int offset) {
        int number = 0;
        number |= bytes[3 + offset] & 0xFF;
        number <<= 8;
        number |= bytes[2 + offset] & 0xFF;
        number <<= 8;
        number |= bytes[1 + offset] & 0xFF;
        number <<= 8;
        number |= bytes[0 + offset] & 0xFF;
        return number;
    }
}
