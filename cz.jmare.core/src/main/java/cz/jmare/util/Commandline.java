package cz.jmare.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class Commandline {
    public static final Pattern CONTAINS_WHITESPACE = Pattern.compile("\\s");

    /**
     * Crack a command line.
     * @param toProcess the command line to process.
     * @return the command line broken into strings.
     * An empty or null toProcess parameter results in a zero sized array list.
     */
    public static List<String> translateCommandline(String toProcess) {
        final ArrayList<String> result = new ArrayList<String>();
        if (toProcess == null || toProcess.length() == 0) {
            //no command? no string
            return result;
        }
        // parse with a simple finite state machine

        final int normal = 0;
        final int inQuote = 1;
        final int inDoubleQuote = 2;
        int state = normal;
        final StringTokenizer tok = new StringTokenizer(toProcess, "\"\' ", true);
        final StringBuilder current = new StringBuilder();
        boolean lastTokenHasBeenQuoted = false;

        while (tok.hasMoreTokens()) {
            String nextTok = tok.nextToken();
            switch (state) {
            case inQuote:
                if ("\'".equals(nextTok)) {
                    lastTokenHasBeenQuoted = true;
                    state = normal;
                } else {
                    current.append(nextTok);
                }
                break;
            case inDoubleQuote:
                if ("\"".equals(nextTok)) {
                    lastTokenHasBeenQuoted = true;
                    state = normal;
                } else {
                    current.append(nextTok);
                }
                break;
            default:
                if ("\'".equals(nextTok)) {
                    state = inQuote;
                } else if ("\"".equals(nextTok)) {
                    state = inDoubleQuote;
                } else if (" ".equals(nextTok)) {
                    if (lastTokenHasBeenQuoted || current.length() != 0) {
                        result.add(current.toString());
                        current.setLength(0);
                    }
                } else {
                    current.append(nextTok);
                }
                lastTokenHasBeenQuoted = false;
                break;
            }
        }
        if (lastTokenHasBeenQuoted || current.length() != 0) {
            result.add(current.toString());
        }
        if (state == inQuote || state == inDoubleQuote) {
            throw new RuntimeException("unbalanced quotes in " + toProcess);
        }
        return result;
    }

    public static List<String> tokenizeCommandLine(String toProcess) {
        List<String> translateCommandline = translateCommandline(toProcess);
        List<String> tokenizedList = new ArrayList<>();
        for (String string : translateCommandline) {
            if (string.startsWith("-") && string.length() > 2) {
                if (string.charAt(1) == '-') {
                    tokenizedList.add(string);
                } else {
                    tokenizedList.add(string.substring(0, 2));
                    tokenizedList.add(string.substring(2));
                }
            } else {
                tokenizedList.add(string);
            }
        }

        return tokenizedList;
    }

    public static String joinTokens(List<String> tokens) {
        StringJoiner joiner = new StringJoiner(" ");
        for (String str : tokens) {
            if (CONTAINS_WHITESPACE.matcher(str).find()) {
                str = "\"" + str + "\"";
            }
            joiner.add(str);
        }
        String joined = joiner.toString();
        return joined;
    }

    public static String normalizeCommandLine(String str) {
        return joinTokens(Commandline.tokenizeCommandLine(str));
    }

    public static void main(String[] args) {
        String str = "-p /path -d \"here's  my description\" --verbose other args -dneco=\"tu a tu\"";

        List<String> tokens = Commandline.tokenizeCommandLine(str);

        for (String string : tokens) {
            System.out.println(string);
        }

        System.out.println(Commandline.joinTokens(tokens));
    }
}
