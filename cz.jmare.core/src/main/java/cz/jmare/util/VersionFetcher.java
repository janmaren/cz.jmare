package cz.jmare.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class VersionFetcher {
    private static String version;

    public static String getVersion() {
        if (version != null) {
            return version;
        }
        populateByManifest();
        return version;
    }

    public static String getShortVersion() {
        String version2 = getVersion();
        int indexOf = version2.indexOf(" ");
        if (indexOf == -1) {
            return version2;
        }
        return version2.substring(0, indexOf);
    }

    public static void populateByManifest() {
        InputStream manifestStream = null;
        try {
            Class<?> clazz = VersionFetcher.class;
            String className = clazz.getSimpleName() + ".class";
            String classPath = clazz.getResource(className).toString();
            if (classPath.startsWith("jar")) {
                String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF";
                manifestStream = new URI(manifestPath).toURL().openStream();
            } else {
                manifestStream = clazz.getResourceAsStream("/META-INF/MANIFEST.MF");
            }
            Manifest manifest = new Manifest(manifestStream);
            Attributes attr = manifest.getMainAttributes();
            String impVersion = attr.getValue("Implementation-Version");
            version = impVersion;
            manifestStream.close();
        } catch (Exception e1) {
            return;
        } finally {
            if (manifestStream != null) {
                try {
                    manifestStream.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
