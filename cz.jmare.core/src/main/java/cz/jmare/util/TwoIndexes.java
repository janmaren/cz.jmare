package cz.jmare.util;

public class TwoIndexes {
    public int startIndexIncluding;

    public int endIndexExcluding;

    public TwoIndexes(int startIndexIncluding, int endIndexExcluding) {
        super();
        this.startIndexIncluding = startIndexIncluding;
        this.endIndexExcluding = endIndexExcluding;
    }

    @Override
    public String toString() {
        return "[" + startIndexIncluding + ", " + endIndexExcluding + "]";
    }
}
