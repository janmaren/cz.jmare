package cz.jmare.util;

import java.io.UnsupportedEncodingException;

public class HexUtil {
    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    public static String hexadecimal(String input, String charsetName) throws UnsupportedEncodingException {
        if (input == null)
            throw new NullPointerException();
        return asHex(input.getBytes(charsetName));
    }

    public static String hexadecimalWithSpaces(String input, String charsetName) throws UnsupportedEncodingException {
        if (input == null)
            throw new NullPointerException();
        return asHexWithSpaces(input.getBytes(charsetName), 16);
    }

    public static String asHex(byte[] buf) {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    public static String asHexRange(byte[] buf, int offset, int size) {
        char[] chars = new char[2 * buf.length];
        for (int i = offset; i < offset + size; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    public static String asHex(byte buf) {
        char[] chars = new char[2];
        chars[0] = HEX_CHARS[(buf & 0xF0) >>> 4];
        chars[1] = HEX_CHARS[buf & 0x0F];
        return new String(chars);
    }

    public static String asHexWithSpaces(byte[] buf, int rowLength) {
        if (buf.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        int pos = 0;
        while (pos < buf.length) {
            sb.append(String.format("%4s: ", pos));

            char high = HEX_CHARS[(buf[pos] & 0xF0) >>> 4];
            char low = HEX_CHARS[buf[pos] & 0x0F];
            sb.append(high);
            sb.append(low);

            int until = pos + rowLength;
            if (until > buf.length) {
                until = buf.length;
            }

            pos++;
            for (;pos < until; ++pos) {
                high = HEX_CHARS[(buf[pos] & 0xF0) >>> 4];
                low = HEX_CHARS[buf[pos] & 0x0F];
                sb.append(" ");
                sb.append(high);
                sb.append(low);
            }
            if (pos < buf.length) {
                sb.append("\n");
            }
        }


        return sb.toString();
    }

    public static String asHexWithSpacesMixed(byte[] buf, int rowLength) {
        if (buf.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        int pos = 0;
        while (pos < buf.length) {
            sb.append(String.format("%4s: ", pos));
            if (buf[pos] >= 65 && buf[pos] < 127) {
                sb.append(" ");
                sb.append((char) buf[pos]);
            } else {
                char high = HEX_CHARS[(buf[pos] & 0xF0) >>> 4];
                char low = HEX_CHARS[buf[pos] & 0x0F];
                sb.append(high);
                sb.append(low);
            }

            int until = pos + rowLength;
            if (until > buf.length) {
                until = buf.length;
            }

            pos++;
            for (;pos < until; ++pos) {
                if (buf[pos] >= 65 && buf[pos] < 127) {
                    sb.append("  ");
                    sb.append((char) buf[pos]);
                } else {
                    char high = HEX_CHARS[(buf[pos] & 0xF0) >>> 4];
                    char low = HEX_CHARS[buf[pos] & 0x0F];
                    sb.append(" ");
                    sb.append(high);
                    sb.append(low);
                }
            }
            if (pos < buf.length) {
                sb.append("\n");
            }
        }


        return sb.toString();
    }

    public static String asBinaryString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(asBinaryString(bytes[i]));
            if (i < bytes.length - 1) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public static String asBinaryString(byte b) {
        return String.format("%8s", Integer.toBinaryString((b + 256) % 256)).replace(' ', '0');
    }

    public static String asBinaryString(int i) {
        return Integer.toBinaryString(i);
    }

    public static String asBinaryString(short i) {
        return asBinaryString((int) i);
    }

    public static String asBinaryString(char ch) {
        return asBinaryString((int) ch);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /**
     * Case insensitive conversion number in hex representation to int
     * @param s
     * @return
     */
    public static int hexToInt(String s) {
        return hexToInt(s, 0);
    }

    /**
     * Case insensitive conversion number in hex representation to int
     * @param s
     * @return
     */
    public static int hexToInt(String s, int offset) {
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 97) {
                c -= 32;
            }
            int d = Character.digit(c, 16);
            val = (val << 4) + d;
        }
        return val;
    }
}
