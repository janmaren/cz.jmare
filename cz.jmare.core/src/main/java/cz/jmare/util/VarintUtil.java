package cz.jmare.util;

import java.util.concurrent.atomic.AtomicInteger;

public class VarintUtil {
    /**
     * Encode positive int value to bytes. Result can have from 1 byte (value <= 127) to 5 bytes (e.g. for Integer.MAX_VALUE)
     * @param value positive value or zero
     * @return byte array representing int value. The occupied size can be obtained by bytes.length
     */
    public static byte[] encodeVarint(int value) {
        byte[] output = new byte[5];
        int outputSize = 0;
        //While more than 7 bits of data are left, occupy the last output byte
        // and set the next byte flag
        while (value > 127) {
            //|128: Set the next byte flag
            output[outputSize] = (byte) (((byte)(value & 127)) | 128);
            //Remove the seven bits we just wrote
            value >>= 7;
            outputSize++;
        }
        output[outputSize++] = (byte) (((byte)value) & 127);

        byte[] outputRightLenght = new byte[outputSize];
        System.arraycopy(output, 0, outputRightLenght, 0, outputSize);

        return outputRightLenght;
    }

    /**
     * Decode input bytes to build positive integer number.
     * @param input
     * @param consumedBytesLength number of bytes from input to build number. It will be from 1 to 5 bytes.
     * @return
     */
    public static int decodeVarint(byte[] input, AtomicInteger consumedBytesLength) {
        int ret = 0;
        for (int i = 0; i < input.length; i++) {
            ret |= (input[i] & 127) << (7 * i);
            //If the next-byte flag is set
            if((input[i] & 128) == 0) {
                consumedBytesLength.set(i + 1);
                break;
            }
        }
        return ret;
    }

    public static int decodeVarint(byte[] input, AtomicInteger consumedBytesLength, int offset) {
        int ret = 0;
        for (int i = 0; i + offset < input.length; i++) {
            ret |= (input[i + offset] & 127) << (7 * i);
            //If the next-byte flag is set
            if((input[i + offset] & 128) == 0) {
                consumedBytesLength.set(i + 1);
                break;
            }
        }
        return ret;
    }
}
