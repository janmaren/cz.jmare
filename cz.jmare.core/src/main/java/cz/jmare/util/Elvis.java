package cz.jmare.util;

import java.util.function.Function;

/**
 * Example:
 * <pre>
 * private String getStreetName(Order order) {
 *   return elvis(order,
 *           Order::getCustomer, Customer::getAddress, Address::getStreetName);
 * }
 * </pre>
 *
 * Taken from <a href="http://blog.codefx.org/java/roll-your-own-pirate-elvis-operator/">this article</a>
 */
public class Elvis {

    public static <T1> T1 elvis(T1 target) {
        return target == null ? null : target;
    }

    public static <T1, T2> T2 elvis(T1 target, Function<T1, T2> f) {
        return target == null ? null : f.apply(target);
    }

    public static <T1, T2, T3> T3 elvis(T1 target, Function<T1, T2> f1, Function<T2, T3> f2) {
        return elvis(elvis(target, f1), f2);
    }

    public static <T1, T2, T3, T4> T4 elvis(T1 target, Function<T1, T2> f1, Function<T2, T3> f2, Function<T3, T4> f3) {
        return elvis(elvis(target, f1, f2), f3);
    }

    public static <T1, T2, T3, T4, T5> T5 elvis(T1 target, Function<T1, T2> f1, Function<T2, T3> f2, Function<T3, T4> f3, Function<T4, T5> f4) {
        return elvis(elvis(target, f1, f2, f3), f4);
    }

}
