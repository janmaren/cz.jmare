package cz.jmare.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BinFileUtil {
    private static final int DEFAULT_BUFFER_SIZE = 8192 * 4;

    public static byte[] getByteArray(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        copyStream(inputStream, os, buffer);
        os.close();
        return os.toByteArray();
    }

    public static long copyStream(final InputStream input, final OutputStream output, final byte[] buffer)
            throws IOException {
        long count = 0;
        int n;
        while ((n = input.read(buffer)) != -1) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
}
