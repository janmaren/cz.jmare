package cz.jmare.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class FileLinesUtil {
    public static ArrayList<String> readLines(InputStream is, String charset) throws IOException {
        ArrayList<String> result = new ArrayList<String>();

        BufferedReader br = new BufferedReader(new InputStreamReader(is, charset));
        String line;
        while ((line = br.readLine()) != null) {
            result.add(line);
        }

        return result;
    }

    public static List<String> readNLines(String filepath, String charset, int firstNLines) {
        List<String> result = new ArrayList<String>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), charset))) {
            String line;
            int readLines = 0;
            while ((line = br.readLine()) != null) {
                result.add(line);
                if (++readLines >= firstNLines) {
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read " + filepath, e);
        }

        return result;
    }

    public static void skipNLines(BufferedReader br, int skipNLines) {
        try {
            @SuppressWarnings("unused")
            String line;
            int readLines = 0;
            while ((line = br.readLine()) != null) {
                if (++readLines >= skipNLines) {
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read inputstream", e);
        }
    }

    public static void writeLines(OutputStream os, List<String> lines, String charset) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, charset));
        for(int i = 0; i < lines.size(); i++) {
            bw.write(lines.get(i));
            bw.newLine();
        }
        bw.flush();
    }

    public static void writeLines(OutputStream os, List<String> lines, String charset, String newLine) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, charset));
        for(int i = 0; i < lines.size(); i++) {
            bw.write(lines.get(i));
            bw.write(newLine);
        }
        bw.flush();
    }
}
