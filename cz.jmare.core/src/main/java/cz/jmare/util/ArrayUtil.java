package cz.jmare.util;

import java.util.Arrays;

public class ArrayUtil {
    public static byte[] concat(byte[] first, byte[] second) {
        byte[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static byte[] concat(byte[] first, byte second) {
        byte[] result = Arrays.copyOf(first, first.length + 1);
        result[first.length] = second;
        return result;
    }

    public static byte[] concat(byte[] first, char second) {
        byte[] result = Arrays.copyOf(first, first.length + 1);
        result[first.length] = (byte) second;
        return result;
    }

    public static byte[] concat(byte first, byte[] second) {
        byte[] result = new byte[second.length + 1];
        result[0] = first;
        System.arraycopy(second, 0, result, 1, second.length);
        return result;
    }

    public static byte[] concat(int first, byte[] second) {
        if (first > Byte.MAX_VALUE || first < Byte.MIN_VALUE) {
            throw new IllegalArgumentException("Too complicated - length of statement " + first);
        }
        return concat((byte)first, second);
    }

    public static byte[] concat(byte[] first, int second) {
        if (second > Byte.MAX_VALUE || second < Byte.MIN_VALUE) {
            throw new IllegalArgumentException("Too complicated - length of statement " + second);
        }
        return concat(first, (byte) second);
    }

    public static byte[] cut(byte[] bytes, int length) {
        byte[] result = new byte[length];
        System.arraycopy(bytes, 0, result, 0, length);
        return result;
    }

    public static byte[] removeBegin(byte[] bytes, int length) {
        byte[] result = new byte[bytes.length - length];
        System.arraycopy(bytes, length, result, 0, bytes.length - length);
        return result;
    }

    public static float[] concat(float[] first, float[] second) {
        float[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
    
    public static String[] concat(String[] first, String second) {
        String[] result = Arrays.copyOf(first, first.length + 1);
        result[first.length] = second;
        return result;
    }
}
