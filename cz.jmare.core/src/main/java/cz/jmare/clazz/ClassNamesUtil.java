package cz.jmare.clazz;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class ClassNamesUtil {
    public static Set<String> getClasses(File[] files, String packageName, boolean alsoSubpackages) {
        return getClasses(Arrays.asList(files), packageName, alsoSubpackages);
    }

    public static Set<String> getClasses(Collection<File> files, String packageName, boolean alsoSubpackages) {
        Set<String> classes = new HashSet<String>();
        for (File filePath : files) {
            Set<String> classesFromFile = getClasses(filePath, packageName, alsoSubpackages);
            classes.addAll(classesFromFile);
        }

        return classes;
    }

    public static Set<String> getClasses(File filePath, String packageName, boolean alsoSubpackages) {
        Set<String> classes = new HashSet<String>();
        if (filePath.toString().endsWith(".jar")) {
            Set<String> fromJARFile = getFromJARFile(filePath, packageName, alsoSubpackages);
            classes.addAll(fromJARFile);
        } else {
            Set<String> fromDirectory = getFromDirectory(filePath, packageName, alsoSubpackages);
            classes.addAll(fromDirectory);
        }
        return classes;
    }

    /**
     * Get all classes classloaded by classloader from given package and given
     * directory on filesystem No classes of parent included (no JRE)
     *
     * @param directory
     *            directory where are required classes or subpackages. It must fit
     *            to packageName parameter
     * @param packageName
     *            package in normal format (dot as delimiter), never null (empty
     *            string allowed)
     * @param alsoSubpackages
     * @param also
     *            classes of subpackages to be returned
     * @return
     */
    private static Set<String> getFromDirectory(File directory, String packageName, boolean alsoSubpackages) {
        Set<String> classes = new HashSet<String>();
        if (directory.exists()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    if (!alsoSubpackages) {
                        continue;
                    }
                    Set<String> fromDirectory = getFromDirectory(file,
                            (packageName.length() > 0 ? packageName + "." : packageName) + file.getName(), alsoSubpackages);
                    classes.addAll(fromDirectory);
                } else if (file.isFile()) {
                    if (file.getName().endsWith(".class")) {
                        String name = (packageName.length() > 0 ? packageName + "." : packageName) + stripExtension(file.getName());
                        classes.add(name);
                    }
                }
            }
        }
        return classes;
    }

    /**
     * Get all classes classloaded by classloader from given package and given
     * directory on filesystem No classes of parent included (no JRE)
     *
     * @param packageName
     *            package in normal format (dot as delimiter), never null (empty
     *            string allowed)
     * @param alsoSubpackages
     * @param also
     *            classes of subpackages to be returned
     * @return
     */
    private static Set<String> getFromJARFile(File jar, String packageName, boolean alsoSubpackages) {
        String packageNamePlusDot = "".equals(packageName) ? packageName : packageName + ".";
        Set<String> classes = new HashSet<String>();
        try (JarInputStream jarFile = new JarInputStream(new FileInputStream(jar))) {
            JarEntry jarEntry;
            do {
                jarEntry = jarFile.getNextJarEntry();
                if (jarEntry != null && !jarEntry.isDirectory()) {
                    String className = jarEntry.getName();
                    if (className.endsWith(".class")) {
                        className = stripExtension(className);
                        String name = className.replace('/', '.');
                        if (name.startsWith(packageNamePlusDot)) {
                            if (!alsoSubpackages) {
                                String simpleClassName = getExtension(name);
                                String comparableCanonClassName = packageName.length() > 0 ? packageName + "." + simpleClassName
                                        : simpleClassName;
                                if (!comparableCanonClassName.equals(name)) {
                                    continue;
                                }
                            }
                            classes.add(name);
                        }
                    }
                }
            } while (jarEntry != null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return classes;
    }

    private static String stripExtension(String dotedName) {
        int lastIndexOf = dotedName.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return dotedName;
        }

        return dotedName.substring(0, lastIndexOf);
    }

    private static String getExtension(String dotedName) {
        int lastIndexOf = dotedName.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return dotedName;
        }

        return dotedName.substring(lastIndexOf + 1);
    }
}
