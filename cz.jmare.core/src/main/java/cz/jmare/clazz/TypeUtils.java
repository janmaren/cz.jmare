package cz.jmare.clazz;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

/**
 * Utility to work with Java 5 generic type parameters. Mainly for internal use
 * within the framework.
 *
 * @author Ramnivas Laddad
 * @author Juergen Hoeller
 * @since 2.0.7
 */
public abstract class TypeUtils {

    /**
     * Check if the right-hand side type may be assigned to the left-hand side
     * type following the Java generics rules.
     *
     * @param lhsType the target type
     * @param rhsType the value type that should be assigned to the target type
     * @return true if rhs is assignable to lhs
     */
    public static boolean isAssignable(Type lhsType, Type rhsType) {

        if (lhsType.equals(rhsType)) {
            return true;
        }
        if (lhsType instanceof Class<?> && rhsType instanceof Class<?>) {
            return ClassUtils.isAssignable((Class<?>) lhsType, (Class<?>) rhsType);
        }
        if (lhsType instanceof ParameterizedType && rhsType instanceof ParameterizedType) {
            return isAssignable((ParameterizedType) lhsType, (ParameterizedType) rhsType);
        }
        if (lhsType instanceof WildcardType) {
            return isAssignable((WildcardType) lhsType, rhsType);
        }
        return false;
    }

    private static boolean isAssignable(ParameterizedType lhsType, ParameterizedType rhsType) {
        if (lhsType.equals(rhsType)) {
            return true;
        }
        Type[] lhsTypeArguments = lhsType.getActualTypeArguments();
        Type[] rhsTypeArguments = rhsType.getActualTypeArguments();
        if (lhsTypeArguments.length != rhsTypeArguments.length) {
            return false;
        }
        for (int size = lhsTypeArguments.length, i = 0; i < size; ++i) {
            Type lhsArg = lhsTypeArguments[i];
            Type rhsArg = rhsTypeArguments[i];
            if (!lhsArg.equals(rhsArg) && !(lhsArg instanceof WildcardType && isAssignable((WildcardType) lhsArg, rhsArg))) {
                return false;
            }
        }
        return true;
    }

    private static boolean isAssignable(WildcardType lhsType, Type rhsType) {
        Type[] upperBounds = lhsType.getUpperBounds();
        Type[] lowerBounds = lhsType.getLowerBounds();
        for (int size = upperBounds.length, i = 0; i < size; ++i) {
            if (!isAssignable(upperBounds[i], rhsType)) {
                return false;
            }
        }
        for (int size = lowerBounds.length, i = 0; i < size; ++i) {
            if (!isAssignable(rhsType, lowerBounds[i])) {
                return false;
            }
        }
        return true;
    }


    /**
     * Primitvni trida dle nazvu nebo null, pokud neni primitivni
     * @param canonClassName
     * @return
     */
    public static Class<?> getPrimitiveClassWhenIs(String canonClassName) {
        if (canonClassName.equals(Integer.TYPE.toString()))
            return Integer.TYPE;
        if (canonClassName.equals(Long.TYPE.toString()))
            return Long.TYPE;
        if (canonClassName.equals(Double.TYPE.toString()))
            return Double.TYPE;
        if (canonClassName.equals(Float.TYPE.toString()))
            return Float.TYPE;
        if (canonClassName.equals(Boolean.TYPE.toString()))
            return Boolean.TYPE;
        if (canonClassName.equals(Character.TYPE.toString()))
            return Character.TYPE;
        if (canonClassName.equals(Byte.TYPE.toString()))
            return Byte.TYPE;
        if (canonClassName.equals(Short.TYPE.toString()))
            return Short.TYPE;
        return null;
    }

    /**
     * Vezme prvni genericky argument a vratiho, pokud se jedna o Class. Pokud se jedna o jakykoliv jiny typ
     * nez Class, napr. vnoreny ParameterizedType, pak se vraci null
     * @param type
     * @return
     */
    public static Class<?> getFirstClassFromGeneric(ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        Type type0 = actualTypeArguments[0];
        if (type0 instanceof Class<?>) {
            return (Class<?>) type0;
        }
        return null;
    }

    /**
     * Pokud se jedna o interface, tak vrati default implementaci,
     * jinak vrati tutez tridu
     * @param interfaceOrClassCanonName
     * @return
     */
    public static String getDefaultImplementation(String interfaceOrClassCanonName) {
        if (interfaceOrClassCanonName.equals("java.util.List")) {
            return "java.util.ArrayList";
        }
        return interfaceOrClassCanonName;
    }

    /**
     * Ziska class z classloaderu na zaklade kanonickeho nazvu
     * Kanonicky nazev muze byt: java.lang.String, java.lang.String[], byte[], java.lang.Long[], java.lang.Long, package.Employee ...
     * @param classLoader
     * @param canonClassName
     * @return
     * @throws ClassNotFoundException
     */
    public static Class<?> loadClass(ClassLoader classLoader, String canonClassName) throws ClassNotFoundException {
        Class<?> clazz = null;
        if (canonClassName.endsWith("[]")) {
            String componentCanonClassName = canonClassName.substring(0, canonClassName.length() - 2);
            Class<?> clazzComp = getPrimitiveClassWhenIs(componentCanonClassName);
            if (clazzComp == null) {
                clazzComp = classLoader.loadClass(componentCanonClassName);
            }
            Object newInstance = Array.newInstance(clazzComp, 0);
            clazz = newInstance.getClass();
        } else {
            clazz = getPrimitiveClassWhenIs(canonClassName);
            if (clazz == null) {
                clazz = classLoader.loadClass(canonClassName);
            }
        }
        return clazz;
    }
}
