package cz.jmare.clazz;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.file.FileUtil;

public class AppVersion {

    private static String version;

    private static final Pattern PATTERN_VERSION = Pattern.compile("Implementation-Version: (.*)");

    private static Function<String, String> IMP_VER_FUNC = new Function<String, String>() {
        @Override
        public String apply(String text) {
            Matcher matcher = PATTERN_VERSION.matcher(text);
            if (matcher.find()) {
                return matcher.group(1);
            }
            return null;
        }
    };

    public static String getVersionFromManifest(Class<?> clazz) {
        if (version == null) {
            String className = clazz.getSimpleName() + ".class";
            String classPath = clazz.getResource(className).toString();
            if (!classPath.startsWith("jar")) {
                // Class not from JAR
                String relativePath = clazz.getName().replace('.', File.separatorChar) + ".class";
                String classFolder = classPath.substring(0, classPath.length() - relativePath.length() - 1);
                String manifestPath = classFolder + "/META-INF/MANIFEST.MF";
                version = getVersion(manifestPath, IMP_VER_FUNC);
            } else {
                String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF";
                version = getVersion(manifestPath, IMP_VER_FUNC);
            }
        }
        return version;
    }

    private static String getVersion(String manifestPath, Function<String, String> function) {
        try (InputStream openStream = new URI(manifestPath).toURL().openStream()) {
            String fileText = FileUtil.getFileText(openStream, StandardCharsets.US_ASCII.name());
            return function.apply(fileText);
        } catch (Exception e) {
        }
        return null;
    }
}
