package cz.jmare.collection;

import java.util.HashMap;
import java.util.Map;

public class CaseInsensitiveMap<T> extends HashMap<String, T> {
    private static final long serialVersionUID = 2214905090044565940L;

    public CaseInsensitiveMap() {
        super();
    }

    public CaseInsensitiveMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public CaseInsensitiveMap(int initialCapacity) {
        super(initialCapacity);
    }

    public CaseInsensitiveMap(Map<? extends String, ? extends T> m) {
        m.forEach((String key, T value) -> {
            super.put(key == null ? null : key.toLowerCase(), value);
        });
    }

    @Override
    public T put(String key, T value) {
       return super.put(key == null ? null : key.toLowerCase(), value);
    }

    public T get(String key) {
       return super.get(key.toLowerCase());
    }
}
