package cz.jmare.collection;

import java.util.HashMap;
import java.util.Map;

public class Maputil {
    @SuppressWarnings("unchecked")
    public static <T, U> Map<T, U> of(T key1, U value1, Object... keyValue) {
        if (keyValue.length % 2 != 0) {
            throw new IllegalArgumentException("Number of key-value arguments must be even");
        }
        HashMap<T, U> map = new HashMap<T, U>();
        map.put(key1, value1);
        for (int i = 0; i < keyValue.length; i += 2) {
            map.put((T) keyValue[i], (U) keyValue[i + 1]);
        }

        return map;
    }

    public static Map<Object, Object> of(Object... keyValue) {
        if (keyValue.length % 2 != 0) {
            throw new IllegalArgumentException("Number of key-value arguments must be even");
        }
        HashMap<Object, Object> map = new HashMap<Object, Object>();
        for (int i = 0; i < keyValue.length; i += 2) {
            map.put(keyValue[i], keyValue[i + 1]);
        }

        return map;
    }

    public static Map<String, Object> of(String strKey1, Object value1, Object... keyValue) {
        if (keyValue.length % 2 != 0) {
            throw new IllegalArgumentException("Number of key-value arguments must be even");
        }
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(strKey1, value1);
        for (int i = 0; i < keyValue.length; i += 2) {
            map.put((String) keyValue[i], keyValue[i + 1]);
        }

        return map;
    }

    public static Map<String, String> ofString(String... keyValue) {
        if (keyValue.length % 2 != 0) {
            throw new IllegalArgumentException("Number of key-value arguments must be even");
        }
        HashMap<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < keyValue.length; i += 2) {
            map.put(keyValue[i], keyValue[i + 1]);
        }

        return map;
    }
}
