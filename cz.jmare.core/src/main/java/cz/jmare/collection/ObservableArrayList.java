package cz.jmare.collection;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ObservableArrayList<E> extends ArrayList<E> {
    private static final long serialVersionUID = -8969062219330882582L;

    public interface ListObserver<E> {
        void onElementAdded(E element);
        void onElementRemoved(E element);
    }

    public ObservableArrayList(int capacity) {
        super(capacity);
        ensureObserver();
    }

    public ObservableArrayList() {
        ensureObserver();
    }

    public ObservableArrayList(Collection<? extends E> collection) {
        super(collection);
        ensureObserver();
    }

    private List<WeakReference<ListObserver<E>>> listObserverWeakRefList;

    public void addObserver(ListObserver<E> observer) {
        listObserverWeakRefList.add(new WeakReference<ListObserver<E>>(observer));
    }

    private void ensureObserver() {
        if (listObserverWeakRefList == null) {
            listObserverWeakRefList = new ArrayList<>();
        }
    }

    @Override
    public boolean add(E object) {
        boolean add = super.add(object);
        callAdd(object);
        return add;
    }

    @Override
    public boolean remove(Object object) {
        boolean removed = super.remove(object);
        if (removed) {
            callRemove(object);
        }
        return removed;
    }

    @Override
    public E remove(int index) {
        E removed = super.remove(index);
        callRemove(removed);
        return removed;
    }

    @SuppressWarnings("unchecked")
    private void callAdd(Object element) {
        for (WeakReference<ListObserver<E>> observerRef : listObserverWeakRefList) {
            ListObserver<E> observer = observerRef.get();
            observer.onElementAdded((E) element);
        }
    }

    @SuppressWarnings("unchecked")
    private void callRemove(Object element) {
        for (WeakReference<ListObserver<E>> observerRef : listObserverWeakRefList) {
            ListObserver<E> observer = observerRef.get();
            observer.onElementRemoved((E) element);
        }
    }

    public static void main(String[] args) {
        ObservableArrayList<String> observableArrayList = new ObservableArrayList<String>();
        observableArrayList.addObserver(new ListObserver<String>() {
            @Override
            public void onElementAdded(String element) {
                System.out.println("added" + element);
            }

            @Override
            public void onElementRemoved(String element) {
                System.out.println("removed" + element);
            }
        });
        observableArrayList.add("str1");
        observableArrayList.add("str2");
        observableArrayList.remove("str2");
    }
}
