package cz.jmare.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Contains unique set of objects but keeps order in which data were added.
 * Contains also {@link #get(Object)} method to get effectively existing instance in set.
 */
public class KeepOrderSet<T> implements Set<T> {
    private LinkedHashMap<T, T> map = new LinkedHashMap<T, T>();

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return map.get(o) != null;
    }

    @Override
    public Iterator<T> iterator() {
        return map.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return map.values().toArray();
    }

    @Override
    public <X> X[] toArray(X[] a) {
        return map.values().toArray(a);
    }

    @Override
    public boolean add(T e) {
        T previous = map.put(e, e);
        return previous == null;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) != null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object object : c) {
            if (!map.containsKey(object)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        for (T object : c) {
            if (add(object)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object object : c) {
            if (map.remove(object) != null) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Collection<T> values = map.values();
        for (Iterator<T> iterator = values.iterator(); iterator.hasNext();) {
            T next = iterator.next();
            if (!c.contains(next)) {
                iterator.remove();
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        map.clear();
    }

    /**
     * Get item equals to argument
     *
     * <p>Note: it can be older instance of object with different fields which are not present in equals method
     * This is useful to avoid iterations over collection
     * @param object to find
     * @return object which is equal to given argument
     */
    public T get(T t) {
        return map.get(t);
    }

    @Override
    public String toString() {
        Iterator<T> i = iterator();
        if (!i.hasNext())
            return "[]";

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (;;) {
            T e = i.next();
            sb.append(e == this ? "(this Collection)" : e);
            if (!i.hasNext())
                return sb.append(']').toString();
            sb.append(", ");
        }
    }
}
