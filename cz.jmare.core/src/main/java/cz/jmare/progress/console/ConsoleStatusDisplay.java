package cz.jmare.progress.console;

import cz.jmare.progress.ReadableProgressStatus;


public class ConsoleStatusDisplay implements Runnable {
    private static int LINE_LENGTH = 60;

    private ReadableProgressStatus<Long> readableProgressStatus;

    public ConsoleStatusDisplay(ReadableProgressStatus<Long> simpleProgressStatus) {
        super();
        this.readableProgressStatus = simpleProgressStatus;
    }

    @Override
    public void run() {
        long maxValue;
        while (true) {
            maxValue = readableProgressStatus.getMaxValue();
            if (maxValue != -1) {
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
        }
        System.out.print("[");
        for (int i = 0; i < LINE_LENGTH; i++) {
            System.out.print(" ");
        }
        System.out.println("]");
        while (true) {
            long progressValue = readableProgressStatus.getProgressValue();
            if (progressValue > 0) {
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
        }
        System.out.print("[");
        int written = 0;
        if (maxValue > 0) {
            while (true) {
                long progressValue = readableProgressStatus.getProgressValue();
                int relatio = (int) (progressValue * LINE_LENGTH / maxValue);
                while (written < relatio) {
                    System.out.print("=");
                    written ++;
                }
                if (relatio >= LINE_LENGTH) {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    progressValue = readableProgressStatus.getProgressValue();
                    if (progressValue == maxValue) {
                        while (written < LINE_LENGTH) {
                            System.out.print("=");
                            written ++;
                        }
                    }
                    break;
                }
            }
        }
        System.out.println("]");
    }

}
