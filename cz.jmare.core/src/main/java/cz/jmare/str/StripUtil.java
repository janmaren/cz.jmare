package cz.jmare.str;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

/**
 * Stripping tool to remove diacritics and return rather a base letters contained in a string.
 * It can convert all diacritics characters from <a href="https://unicode-table.com/en/">this</a> table
 */
public class StripUtil {
    /**
     * Manual mapping for very special characters which can't be converted automatically
     */
    private final static char[][] specialDiaToNodiaArray = {
            {'B', '\u0181', '\u0182', '\u0183'},
            {'b', '\u0184', '\u0180', '\u0185'},
            {'C', '\u0186', '\u0187'},
            {'c', '\u0188'},
            {'D', '\u0189', '\u018a', '\u0110', '\u00d0'},
            {'d', '\u018b', '\u018c', '\u018d', '\u0111'},
            {'E', '\u018e', '\u018f', '\u0190'},
            {'e', '\u01dd'},
            {'F', '\u0191'},
            {'f', '\u0192'},
            {'G', '\u0193', '\u01e4'},
            {'g', '\u01e5'},
            {'i', '\u0196', '\u0197'},
            {'K', '\u0198', '\u0138'},
            {'k', '\u0199'},
            {'l', '\u019a', '\u0140', '\u0142'},
            {'L', '\u019b', '\u013f', '\u0141'},
            {'M', '\u019c'},
            {'N', '\u019d', '\u014a'},
            {'n', '\u019e', '\u0149', '\u014b'},
            {'O', '\u019f'},
            {'P', '\u01a4'},
            {'p', '\u01a5'},
            {'R', '\u01a6'},
            {'S', '\u01a7', '\u01a9'},
            {'s', '\u01a8', '\u017f', '\u00df'},
            {'t', '\u01ab', '\u01ad'},
            {'T', '\u01ac', '\u01ae'},
            {'U', '\u01b1', '\u01b2'},
            {'w', '\u01bf'},
            {'x', '\u00d7'},
            {'Y', '\u01b3'},
            {'y', '\u01b4'},
            {'Z', '\u01b5', '\u01b7', '\u01b8', '\u01ee'},
            {'z', '\u01b6', '\u01b9', '\u01ba', '\u01bb', '\u01bc', '\u01bd', '\u01be', '\u01ef'},
        };

    /**
     * Prepared mapping for very special characters
     */
    private final static Map<Character, Character> specialDiaToNodiaMap = new HashMap<Character, Character>();

    static {
        StringBuilder sb = new StringBuilder();
        for (char[] nodia : specialDiaToNodiaArray) {
            for (int i = 1; i < nodia.length; i++) {
                specialDiaToNodiaMap.put(nodia[i], nodia[0]);
                sb.append(nodia[i]);
            }
        }
    }

    /**
     * Return string without diacritics when possible. When not possible return origin letter instead
     * @param str
     * @return
     */
    public static String stripString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            Character stripedDia = normalizeChar(charAt);
            if (stripedDia == null) {
                stripedDia = specialDiaToNodiaMap.get(charAt);
            }
            sb.append(stripedDia == null ? charAt: stripedDia);
        }
        return sb.toString();
    }

    /**
     * Return string without diacritics
     * @param str
     * @return
     * @throws IllegalArgumentException when not convertable character present in argument - it may be bug because method can convert almost all
     */
    public static String stripStringStrict(String str) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            Character stripedDia = normalizeChar(charAt);
            if (stripedDia == null) {
                stripedDia = specialDiaToNodiaMap.get(charAt);
            }
            if (stripedDia == null) {
                throw new IllegalArgumentException("Character " + charAt + " is not strippable");
            }
            sb.append(stripedDia);
        }
        return sb.toString();
    }

    /**
     * Return true when argument contains a letter with diacritics<br/>
     * Note, diacritics letter is somehow different than its' base type.
     * @param str
     * @return
     */
    public static boolean isDiacriticsLetter(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            Character stripedDia = normalizeChar(charAt);
            if (stripedDia == null || charAt != stripedDia) {
                return true;
            }
        }
        return false;
    }

    final static String normalizeString(String str) {
        String subjectString = Normalizer.normalize(str, Normalizer.Form.NFD);
        String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "");
        return resultString;
    }

    final static Character normalizeChar(char ch) {
        String subjectString = Normalizer.normalize(String.valueOf(ch), Normalizer.Form.NFD);
        String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "");
        if ("".equals(resultString)) {
            return null;
        }
        return resultString.charAt(0);
    }
}
