package cz.jmare.logging;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigureLogger {

    private static final int DEFAULT_MAX_COUNT = 5;
    private static final int DEFAULT_MAX_FILE_SIZE = 10000000;

    public static class StdoutConsoleHandler extends ConsoleHandler {
        @Override
        protected void setOutputStream(OutputStream out) throws SecurityException {
            super.setOutputStream(System.out);
        }
    }

    public static void setupLogger(String name) throws IOException {
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        for (Handler handler : handlers) {
            rootLogger.removeHandler(handler);
        }
        Formatter oneRowFormatter = new OneLineFormatter();
        Handler fileHandler = new FileHandler("%t/" + name + ".log", DEFAULT_MAX_FILE_SIZE, DEFAULT_MAX_COUNT, true);
        fileHandler.setFormatter(oneRowFormatter);
        rootLogger.addHandler(fileHandler);
        ConfigureLogger.StdoutConsoleHandler handler = new ConfigureLogger.StdoutConsoleHandler();
        handler.setFormatter(oneRowFormatter);
        handler.setLevel(Level.FINEST);
        rootLogger.addHandler(handler);
     }

}
