package cz.jmare.logging;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LoggerSlf4jOptional {

    public static void log(String levelMethodName, String message) {
        String className = Thread.currentThread().getStackTrace()[2].getClassName();
        try {
            Class.forName("org.slf4j.event.LoggingEvent");
            Class<?> loggerFactoryClass = Class.forName("org.slf4j.LoggerFactory");
            Method method = loggerFactoryClass.getMethod("getLogger", String.class);
            Object loggerObject = method.invoke(null, className);

            Class<?> loggerClass = loggerObject.getClass();
            Method levelMethod = loggerClass.getDeclaredMethod(levelMethodName, String.class);
            levelMethod.invoke(loggerObject, message);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | NoClassDefFoundError | InvocationTargetException e) {
            if ("error".equals(levelMethodName)) {
                System.err.println(levelMethodName + ":" + message);
            } else {
                System.out.println(levelMethodName + ":" + message);
            }
        }
    }

    public static void log(String levelMethodName, String message, Exception exception) {
        String className = Thread.currentThread().getStackTrace()[2].getClassName();
        try {
            Class.forName("org.slf4j.event.LoggingEvent");
            Class<?> loggerFactoryClass = Class.forName("org.slf4j.LoggerFactory");
            Method method = loggerFactoryClass.getMethod("getLogger", String.class);
            Object loggerObject = method.invoke(null, className);

            Class<?> loggerClass = loggerObject.getClass();
            Method levelMethod = loggerClass.getDeclaredMethod(levelMethodName, String.class, Throwable.class);
            levelMethod.invoke(loggerObject, message, exception);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | NoClassDefFoundError | InvocationTargetException e) {
            if ("error".equals(levelMethodName)) {
                System.err.println(levelMethodName + ":" + message);
            } else {
                System.out.println(levelMethodName + ":" + message);
            }
            exception.printStackTrace();
        }
    }

    public static void logDebug(String message) {
        log("debug", message);
    }

    public static void logTrace(String message) {
        log("trace", message);
    }

    public static void logWarn(String message) {
        log("warn", message);
    }

    public static void logInfo(String message) {
        log("info", message);
    }

    public static void logError(String message) {
        log("error", message);
    }

    public static void logError(String message, Exception exception) {
        log("error", message, exception);
    }
}
