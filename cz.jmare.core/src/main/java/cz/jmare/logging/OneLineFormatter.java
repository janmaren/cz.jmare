package cz.jmare.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class OneLineFormatter extends Formatter {
    private static final String LINE_SEP = System.getProperty("line.separator");
    private static final String ST_SEP = LINE_SEP + " ";
    private static final String defaultTimeFormat = "dd-MM-yyyy HH:mm:ss.SSS";
    private final DateFormat formatter;


    public OneLineFormatter() {
        formatter = new SimpleDateFormat(defaultTimeFormat);
    }

    public OneLineFormatter(String format) {
        formatter = new SimpleDateFormat(format);
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();

        addTimestamp(sb, record.getMillis());

        sb.append(' ');
        sb.append(record.getLevel());

        sb.append(' ');
        sb.append('[');
        sb.append(Thread.currentThread().getName());
        sb.append(']');

        sb.append(' ');
        sb.append(record.getSourceClassName());
        sb.append('.');
        sb.append(record.getSourceMethodName());

        sb.append(' ');
        sb.append(formatMessage(record));

        if (record.getThrown() != null) {
            sb.append(ST_SEP);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            record.getThrown().printStackTrace(pw);
            pw.close();
            sb.append(sw.getBuffer());
        }

        sb.append(LINE_SEP);

        return sb.toString();
    }

    protected synchronized void addTimestamp(StringBuilder buf, long timestamp) {
        Date date = new Date(timestamp);
        String dateFormatted = formatter.format(date);
        buf.append(dateFormatted);
    }
}
