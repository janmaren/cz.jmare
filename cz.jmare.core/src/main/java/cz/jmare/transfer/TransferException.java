package cz.jmare.transfer;

public class TransferException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 4453545958925119933L;

    public TransferException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
