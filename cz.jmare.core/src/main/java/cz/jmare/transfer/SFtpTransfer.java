package cz.jmare.transfer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import cz.jmare.exec.ConsoleExec;

@SuppressWarnings("unused")
public class SFtpTransfer implements Transfer {
	private String hostShare;

    private String username;

    private String password;

    private Process process;

    private InputStreamReader reader;

    private OutputStreamWriter writer;


    public SFtpTransfer(String hostShare) throws IOException {
        this.hostShare = hostShare;
        init();
    }

    public SFtpTransfer(String hostShare, String username, String password) throws IOException {
        this.hostShare = hostShare;
        this.username = username;
        this.password = password;
        init();
    }

    protected void init() throws IOException {
        List<String> execute = ConsoleExec.execute(StandardCharsets.ISO_8859_1, true, "sftp", "superadmin@10.0.25.126");
        for (String string : execute) {
            System.out.println(string);
        }
//        ProcessBuilder builder = new ProcessBuilder(connectCommand());
//        builder.redirectErrorStream(true);
//        process = builder.start();
//        reader = new InputStreamReader(process.getInputStream());
//        writer = new OutputStreamWriter(process.getOutputStream());
//        try (BufferedReader r = new BufferedReader(reader)) {
//            while (true) {
//                String line = r.readLine();
//                System.out.println(line);
//                if (line == null) {
//                    break;
//                }
//
//            }
//        }
    }

    private String launch(String command) throws IOException {
        InputStream inputStream = process.getInputStream();
        while (inputStream.available() > 0) {
            inputStream.read();
        }
        writer.write(command + System.getProperty("line.separator"));
        StringBuilder sb = new StringBuilder();
        while (true) {
            int read = reader.read();
            if (read == '\r') {
                continue;
            }
            if (read == '\n') {
                return sb.toString();
            }
            sb.append((char) read);
        }
    }

    @Override
    public void close() {
        process.destroy();
    }

    @Override
    public void put(String localPath, String remotePath) {
        try {
            launch("put " + localPath);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to launch put " + localPath);
        }
    }

    @Override
    public boolean remoteFileExists(String remotePath) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean remoteDirExists(String remotePath) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void mkdirs(String remotePath) {
        // TODO Auto-generated method stub

    }

    @Override
    public void get(String localPath, String remotePath) {
        // TODO Auto-generated method stub

    }
}
