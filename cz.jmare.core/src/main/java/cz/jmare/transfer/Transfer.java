package cz.jmare.transfer;

import java.io.Closeable;

public interface Transfer extends Closeable {
    void put(String localPath, String remotePath);
    boolean remoteFileExists(String remotePath);
    boolean remoteDirExists(String remotePath);
    void mkdirs(String remotePath);
    void get(String localPath, String remotePath);
}
