package cz.jmare.transfer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import cz.jmare.exec.ConsoleExec;
import cz.jmare.file.PathUtil;

public class SmbClientTransfer implements Transfer {
    private String username;

    private String password;

    private String hostShare;

    public SmbClientTransfer(String hostShare) {
        super();
        this.hostShare = hostShare;
    }

    @Override
    public void put(String srcPath, String remotePath) {
        String dstDir = PathUtil.getPathWithoutFilename(remotePath);
        String dstFileName = PathUtil.getLastPartFromPath(remotePath);
        try {
            List<String> command = connectCommand();
            command.add("-c");
            command.add("cd " + dstDir + "; put " + srcPath + " " + dstFileName);
            ConsoleExec.execute(StandardCharsets.ISO_8859_1, true, command);
        } catch (IOException e) {
            throw new TransferException("Unable to put " + srcPath + " to " + dstDir + "/" + dstFileName, e);
        }
    }

    private List<String> connectCommand() {
        List<String> list = new ArrayList<>();
        list.add("smbclient");
        list.add("-U");
        if (username != null) {
            list.add(username + "%" + password != null ? password : "");
        } else {
            list.add("%");
        }
        list.add(hostShare);
        return list;
    }

    @Override
    public void get(String srcPath, String dstPath) {
        throw new RuntimeException("Not implemented get");
    }

    @Override
    public boolean remoteFileExists(String remotePath) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean remoteDirExists(String remotePath) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void mkdirs(String remotePath) {
    }

    @Override
    public void close() {
    }

}
