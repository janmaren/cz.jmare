package cz.jmare.args;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;

import cz.jmare.regexp.PatternTraverser;
import cz.jmare.regexp.ReplaceCallback;
import cz.jmare.regexp.ReplaceCallback.Callback;

public class InputArgsUtil {
    public static Map<String, String> argsToMap(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        for (String arg : args) {
            int indexOf = arg.indexOf("=");
            if (indexOf == -1) {
                throw new IllegalArgumentException("Illegal parameter " + arg + ". Allowed format is 'key=value' (= must be present).");
            }
            String value = arg.substring(indexOf + 1);
            map.put(arg.substring(0, indexOf), value);
        }
        return map;
    }

    public static Map<String, String> listToMap(List<String> args)  {
        Map<String, String> map = new HashMap<String, String>();
        for (String arg : args) {
            int indexOf = arg.indexOf("=");
            if (indexOf == -1) {
                throw new IllegalArgumentException("Illegal parameter " + arg + ". Allowed format is 'key=value' (= must be present).");
            }
            String value = arg.substring(indexOf + 1);
            map.put(arg.substring(0, indexOf), value);
        }
        return map;
    }

    /**
     * Populate by values passed as system properties or enviroment variables
     * @param map
     */
    public static void populateVariables(Map<String, String> map) {
        Set<String> keySet = map.keySet();
        for (String key : keySet) {
            String value = map.get(key);
            try {
                value = populateVariables(value);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Unable to process parameter '" + key + "=" + value + "': " + e.getMessage());
            }
            map.put(key, value);
        }
    }

    private static String populateVariables(String value) {
        try {
            return ReplaceCallback.replace("\\$\\{([a-zA-Z\\.-_0-9]+)\\}", value, new Callback() {
                @Override
                public String matchFound(Matcher match) {
                    String varName = match.group(1);
                    String property = System.getProperty(varName);
                    if (property != null) {
                        return property;
                    }
                    String getenv = System.getenv(varName);
                    if (getenv != null) {
                        return getenv;
                    }
                    throw new IllegalStateException("Unable to find value for variable '" + varName + "'");
                }});
        } catch (IllegalStateException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static Map<String, String> paramsStringToMap(String string) {
        Map<String, String> map = new HashMap<String, String>();
        if (string == null) {
            return map;
        }
        List<String> pars = new ArrayList<String>();
        PatternTraverser patternTraverser = new PatternTraverser("(?<space>\\s+)|(?<quote>\")|(?<rest>[^\\s\"]+)", string, new Consumer<Matcher>() {
            boolean inQuote;
            String currentString = "";

            @Override
            public void accept(Matcher m) {
                if (m.group("space") != null) {
                    if (inQuote) {
                        currentString += m.group();
                    } else {
                        pars.add(currentString);
                        currentString = "";
                    }
                } else if (m.group("quote") != null) {
                    inQuote = !inQuote;
                }
            }
        });
        patternTraverser.traverse();
        return map;
    }

    public static Map<String, String> parseStringToArgsMap(String parametersString) {
        ArgumentsParser argumentsParser = new ArgumentsParser(parametersString);
        List<String> list = argumentsParser.getList();
        return listToMap(list);
    }

    public static void main(String[] args) {
        String string = "a=val1 \"b=cc\\\" val2\" c=val3";
        System.out.println(string);
        ArgumentsParser argumentsParser = new ArgumentsParser(string);
        List<String> list = argumentsParser.getList();
        Map<String, String> argsToMap = argsToMap(list.toArray(new String[list.size()]));
        System.out.println(argsToMap);
    }
}
