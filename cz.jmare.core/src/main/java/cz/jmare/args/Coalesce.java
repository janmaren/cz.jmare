package cz.jmare.args;

import java.util.function.Supplier;

public class Coalesce {
    @SafeVarargs
    public static <T> T coalesce(Supplier<T>... items) {
        for (Supplier<T> item : items) {
            if (item == null) {
                continue;
            }
            T t = item.get();
            if (t != null) {
                return t;
            }
        }
        return null;
    }
}
