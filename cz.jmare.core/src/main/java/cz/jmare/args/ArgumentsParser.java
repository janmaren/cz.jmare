package cz.jmare.args;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;

import cz.jmare.regexp.PatternTraverser;

public class ArgumentsParser {
    private String string;

    boolean inQuote;
    int pos = -1;
    String currentString = "";

    public ArgumentsParser(String string) {
        super();
        this.string = string;
    }

    public List<String> getList() {
        List<String> pars = new ArrayList<String>();

        if (string == null) {
            return pars;
        }
        String regex = "(?<space>\\s+)|(?<quote>\\\\?\")|(?<rest>[^\\s\"\\\\]+)";

        PatternTraverser patternTraverser = new PatternTraverser(regex, string, new Consumer<Matcher>() {
            @Override
            public void accept(Matcher m) {
                if (m.group("space") != null) {
                    if (inQuote) {
                        currentString += m.group();
                    } else {
                        pars.add(currentString);
                        currentString = "";
                    }
                } else if (m.group("quote") != null) {
                    String group = m.group("quote");
                    if (!group.startsWith("\\")) {
                        inQuote = !inQuote;
                    } else {
                        currentString += "\"";
                    }
                } else if (m.group("rest") != null) {
                    currentString += m.group();
                }
            }
        });
        patternTraverser.traverse();
        if (!"".equals(currentString)) {
            if (inQuote) {
                throw new IllegalArgumentException("Missing closing double quote");
            }
            pars.add(currentString);
        }

        return pars;
    }
}
