package cz.jmare.os;

/**
 *
 * Class containing help methods on application level.
 * @author Vity
 */
public final class AppHelper {

    private static PlatformType activePlatformType = null;

    private AppHelper() {
    }


    /*
    * Defines the default value for the platform resource,
    * either "osx" or "default".
    */
    public static PlatformType getPlatform() {
        if (activePlatformType != null)
            return activePlatformType;
        activePlatformType = PlatformType.DEFAULT;

		String osName = System.getProperty("os.name");
        if (osName != null) {
            osName = osName.toLowerCase();
            for (PlatformType platformType : PlatformType.values()) {
                for (String pattern : platformType.getPatterns()) {
                    if (osName.startsWith(pattern)) {
                        return activePlatformType = platformType;
                    }
                }
            }
        }
        return activePlatformType = PlatformType.DEFAULT;
    }
}
