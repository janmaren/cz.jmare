package cz.jmare.comparing;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Create comparator by reflection<br>
 * Order property can be a chain of properties separated by '.', E.g. 'group.groupName', where root class could be an Employee having getter getGroup() and group has getter getGroupName()<br>
 * Terminal property must return a type which is comparable.<br>
 * Supported also 'is' getter for Boolean values but note the 'is' getter must be in the end of chain. Also note the primitive value isn't comparable, the 'is' hasn't sense unless Boolean used<br>
 * The order may be ascending or descending<br>
 * Null value may be in the middle of chain (evaluating whole expression as null) unless prohibitNullValueInChain is true<br>
 * Null values are comparable and it is possible to define whether null represents biggest or lowest value
 */
public class UniversalReflectiveComparator<T> implements Comparator<T> {
    private static final java.lang.Object[] NO_ARGS = null;

    /**
     * 1 = ascending, -1 = descending
     */
    private final int order;

    /**
     * 1 = null is lowest value, -1 = null is biggest value
     */
    private int nullAsBiggest = 1;

    private Method[] methodsChain;

    private boolean prohibitNullValueInChain;

    public UniversalReflectiveComparator(Class<?> classToCompare, String propertiesChain) {
        this(classToCompare, propertiesChain, false);
    }

    public UniversalReflectiveComparator(Class<?> classToCompare, String propertiesChain, boolean descending) {
        initMethodsChain(classToCompare, propertiesChain);
        this.order = descending ? -1 : 1;
    }

    public UniversalReflectiveComparator(Class<?> classToCompare, String propertiesChain, boolean descending, boolean prohibitNullValueInChain) {
        initMethodsChain(classToCompare, propertiesChain);
        this.order = descending ? -1 : 1;
        this.prohibitNullValueInChain = prohibitNullValueInChain;
    }

    public UniversalReflectiveComparator(Class<?> classToCompare, String propertiesChain, boolean descending, boolean prohibitNullValueInChain, boolean nullAsBiggest) {
        initMethodsChain(classToCompare, propertiesChain);
        this.order = descending ? -1 : 1;
        this.prohibitNullValueInChain = prohibitNullValueInChain;
        this.nullAsBiggest = nullAsBiggest ? -1 : 1;
    }

    private void initMethodsChain(Class<?> rootClass, String methodNames)  {
        Class<?> clazz = rootClass;
        ArrayList<Method> methodsChainList = new ArrayList<>();
        String[] split = methodNames.split("\\.");
        String methodName;
        String methodIsName;
        for (int i = 0; i < split.length; i++) {
            String name = split[i];
            methodName = buildGetGetter(name);
            try {
                Method getterMethod = clazz.getMethod(methodName, (java.lang.Class<?>[]) null);
                clazz = getterMethod.getReturnType();
                if (i == split.length - 1) {
                    checkComparable(clazz);
                }
                methodsChainList.add(getterMethod);
            } catch (NoSuchMethodException e) {
                Method getterMethod;
                methodIsName = buildIsGetter(name);
                try {
                    getterMethod = clazz.getMethod(methodName, (java.lang.Class<?>[]) null);
                    clazz = getterMethod.getReturnType();
                } catch (NoSuchMethodException e1) {
                    throw new IllegalArgumentException("Class " + clazz + " has not method " + methodName + " or " + methodIsName + ". Property on position [" + i + "] (zero based) in " + methodNames + " can't ba evaluated");
                }

                if (i == split.length - 1) {
                    checkComparable(clazz);
                    methodsChainList.add(getterMethod);
                } else {
                    throw new IllegalArgumentException("Class " + clazz + " has method " + methodIsName + " but not as last property in chain (boolean can't return another object by a getter)");
                }
            }
        }
        methodsChain = methodsChainList.toArray(new Method[methodsChainList.size()]);
    }

    private String buildIsGetter(String name) {
        return "is" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private String buildGetGetter(String name) {
        return "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private void checkComparable(Class<?> clazz) {
        boolean assignableFrom = Comparable.class.isAssignableFrom(clazz);
        if (!assignableFrom) {
            throw new IllegalArgumentException("Last property must implement comparable but it doesn't");
        }
    }

    private Comparable<?> getValue(T o) {
        Object obj = o;
        for (int i = 0; i < methodsChain.length; i++) {
            Method method = methodsChain[i];
            try {
                obj = method.invoke(obj, NO_ARGS);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalArgumentException("Problems calling " + obj.getClass() + "." + method);
            }
            if (obj == null) {
                if (i < methodsChain.length - 1) {
                    if (prohibitNullValueInChain) {
                        throw new NullPointerException("Method " + method + " returned null which is not allowed");
                    }
                }
                return null;
            }
        }
        return (Comparable<?>) obj;
    }

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public int compare(T o1, T o2) {
        try {
            Comparable o1attribute = getValue(o1);
            Comparable o2attribute = getValue(o2);
            if (o1attribute == null) {
                if (o2attribute == null) {
                    return 0;
                }
                return -nullAsBiggest * order;
            }
            if (o2attribute == null) {
                return nullAsBiggest * order;
            }
            return o1attribute.compareTo(o2attribute) * order;
        } catch (Exception e) {
            throw new IllegalStateException("Failed to compare " + clazz(o1) + " with " + clazz(o2), e);
        }
    }

    /**
     * Returns the type and string value of the given object. eg: java.lang.String
     * 'Hello World!'
     */
    private static String clazz(Object object) {
        return object.getClass().getCanonicalName() + " '" + String.valueOf(object) + "'";
    }
}
