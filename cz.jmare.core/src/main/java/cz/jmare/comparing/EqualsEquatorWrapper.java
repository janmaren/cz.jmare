package cz.jmare.comparing;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EqualsEquatorWrapper<T> {
    private T object;

    private SameTypeEquator<T> equatorObject;

    public EqualsEquatorWrapper(T object, SameTypeEquator<T> equatorObject) {
        super();
        this.object = object;
        this.equatorObject = equatorObject;
    }

    public T getObject() {
        return object;
    }


    public static <T> Set<EqualsEquatorWrapper<T>> wrapToSet(Collection<T> origCollection, SameTypeEquator<T> equatorObject) {
        return origCollection.stream().map(g -> new EqualsEquatorWrapper<T>(g, equatorObject)).collect(Collectors.toSet());
    }

    public static <T> List<T> unwrapToList(Set<EqualsEquatorWrapper<T>> wrappedSet) {
        return wrappedSet.stream().map(g -> g.getObject()).collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((equatorObject == null) ? 0 : equatorObject.hashCode());
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EqualsEquatorWrapper<?> other = (EqualsEquatorWrapper<?>) obj;
        if (equatorObject == null) {
            if (other.equatorObject != null)
                return false;
        } else if (!equatorObject.isEqual(this.getObject(), (T) other.getObject()))
            return false;
        return true;
    }
}
