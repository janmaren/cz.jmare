package cz.jmare.comparing;

public interface SameTypeEquator<T> {
    boolean isEqual(T obj1, T obj2);
}
