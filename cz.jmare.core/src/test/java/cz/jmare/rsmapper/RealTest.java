package cz.jmare.rsmapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

public class RealTest {
    @Test
    @Ignore
    public void realTest() throws ClassNotFoundException {
        Class.forName("oracle.jdbc.OracleDriver");
        String url = "jdbc:oracle:thin:@tordb04.vs.csin.cz:1521/CPTFAT";
        String username = "cpt";
        String password = "CPTFATpassword_2018";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
//            PreparedStatement prepareStatement = connection.prepareStatement(
//                    "select cp.*, pav.* from cpt.client_product cp left join cpt.PRODUCT_ATTRIBUTE_VALUE pav on cp.prod_inst_id=pav.prod_inst_id where cp.prod_inst_id in (1231519) order by cp.prod_inst_id");
            PreparedStatement prepareStatement = connection.prepareStatement("select cp.*, '1' as s$_1, pav.* from cpt.client_product cp left join cpt.PRODUCT_ATTRIBUTE_VALUE pav on cp.prod_inst_id=pav.prod_inst_id where cp.prod_inst_id in (48268) order by cp.prod_inst_id");
            ResultSet rs = prepareStatement.executeQuery();
            List<Map<String, Object>> list = ResultSetConvertor.convertAll(rs);
            for (Map<String, Object> map : list) {
                System.out.println(map);
            }
            CollectionMapper collectionMapper = new CollectionMapper("prod_inst_id", new SimpleRemapper(ClientProduct.class),
                    new CollectionFieldMapper("productAttributeValues", new SimpleRemapper(ProductAttributeValue.class).withColumnNameSuffix("_1"))
                            );
            List<ClientProduct> remapToList = collectionMapper.remapToList(list, ClientProduct.class);
            System.out.println(remapToList);
        } catch (SQLException e) {
            throw new RuntimeException("An sql exception occured", e);
        }
    }
}
