package cz.jmare.rsmapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cz.jmare.rsmapper.vo.Employee;
import cz.jmare.rsmapper.vo.EmployeeGroup;
import cz.jmare.rsmapper.vo.Group;

public class MapperTest2 {
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    @Before
    public void prepare() {
        HashMap<String, Object> row = null;

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("eg_id", 1);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 1);
        row.put("g_id", 1);
        row.put("g_name", "gr 1");
        list.add(row);

        row = new HashMap<>();
        row.put("e_id", 1);
        row.put("e_firstname", "john");
        row.put("eg_id", 2);
        row.put("eg_employee_id", 1);
        row.put("eg_group_id", 2);
        row.put("g_id", 2);
        row.put("g_name", "gr 2");
        list.add(row);

        // left join employe-group, kde neexistuje zaznam
        row = new HashMap<>();
        row.put("e_id", 2);
        row.put("e_firstname", "jack");
        row.put("eg_id", null);
        row.put("eg_employee_id", null);
        row.put("eg_group_id", null);
        row.put("g_id", null);
        row.put("g_name", null);
        list.add(row);
    }

    @Test
    public void testRemapAuto() {
        CollectionMapper collectionMapper = new CollectionMapper("e_id", new SimpleRemapper(Employee.class), new CollectionFieldMapper("employeeGroupList",
                "eg_id", new SimpleRemapper(EmployeeGroup.class), new SingleFieldMapper("group", "g_id", new SimpleRemapper(Group.class))));

        List<Employee> employees = collectionMapper.remapToList(list, Employee.class);
        Assert.assertEquals(2, employees.size());
        Assert.assertEquals(2, employees.get(0).getEmployeeGroupList().size());
        Assert.assertEquals(null, employees.get(1).getEmployeeGroupList());
    }
}
