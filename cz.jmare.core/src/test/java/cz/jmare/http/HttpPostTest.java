package cz.jmare.http;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.junit.Ignore;
import org.junit.Test;

public class HttpPostTest {
    @Test
    @Ignore
    public void testPostMultipart() throws ErrorStatusException, IOException {
        HashMap<String, File> map = new HashMap<String, File>();
        map.put("files", new File("pom.xml"));
        RequestData requestData = new RequestData(new SimplePopulateMultipartBodyConsumer(map));
        requestData.addBasicAuthorizationHeaderItem("stmare", "xxxxxxxxxxxxxx");
        String sendGetString = HttpClient.sendGetString("https://api.bitbucket.org/2.0/repositories/stmare/moval/downloads", "POST", requestData);
        System.out.println(sendGetString);
    }

    @Test
    @Ignore
    public void testHurlPost() {
        RequestData requestData = new RequestData();
        requestData.addBasicAuthorizationHeaderItem("stmare", "xxxxxxxxxxxxxx");
        String str = HUrl.postFileMultipartReturnString("https://api.bitbucket.org/2.0/repositories/stmare/moval/downloads", "files", new File("pom.xml"), requestData);
        System.out.println(str);
    }
}
