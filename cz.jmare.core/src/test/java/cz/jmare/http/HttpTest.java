package cz.jmare.http;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.SSLSocketFactory;

import org.junit.Ignore;
import org.junit.Test;

public class HttpTest {
    @Test
    @Ignore
    public void withSSLFactoryTest() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ErrorStatusException, IOException {
        RequestData requestProperties = new RequestData();
        requestProperties.setProxy("proxy.csin.cz", 8080);

        String keystorePath = "c:/temp/KeyStore.jks";
        String truststorePath = "c:/temp/TrustStore";
        String keystorePassword = "5943413037";
        String truststorePassword = "changeit";
        SSLSocketFactory sslSf = SSLUtil.createSimpleSSLSocketFactory(keystorePath, truststorePath, keystorePassword, truststorePassword);

        requestProperties.setSslSocketFactory(sslSf);
        String result = HttpClient.sendGetString("https://knz-ws-test.koop.cz/knz-ws/KnzCarService?wsdl", "GET", requestProperties);
        System.out.println(result);
    }

    @Test
    @Ignore
    public void csasTest() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ErrorStatusException, IOException {
        RequestData requestProperties = new RequestData();
        requestProperties.setProxy("proxy.csin.cz", 8080);
        String result = HttpClient.sendGetString("https://www.seznam.cz", "GET", requestProperties);
        System.out.println(result);
    }

    @Test
    @Ignore
    public void simpleTest() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ErrorStatusException, IOException {
        String result = HttpClient.sendGetString("https://www.seznam.cz", "GET", new RequestData());
        System.out.println(result);
    }

    @Test
    @Ignore
    public void basicWithHeaderTest() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ErrorStatusException, IOException {
        RequestData requestProperties = new RequestData();
        requestProperties.addBasicAuthorizationHeaderItem("guest", "guest");
        String result = HttpClient.sendGetString("https://jigsaw.w3.org/HTTP/Basic/", "GET", requestProperties);
        System.out.println(result);
    }

    @Test
    @Ignore
    public void simpleRedirectTest() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ErrorStatusException, IOException {
        RequestData requestProperties = new RequestData();
        requestProperties.setProxy("proxy.csin.cz", 8080);
        String result = HttpClient.sendGetString("https://jigsaw.w3.org/HTTP/300/301.html", "GET", requestProperties);
        System.out.println(result);
    }
}
