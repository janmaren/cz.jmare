package cz.jmare.regexp;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import org.junit.Assert;
import org.junit.Test;

public class ReplaceCallbackTest {
    @Test
    public void testReplace() {
        final Map<String,String> props = new HashMap<String,String>();
        props.put("MY_NAME", "Kip");
        props.put("DEPT", "R&D");
        props.put("BOSS", "Dave");

        String subjectString = "begining <a href=\"expr\">expr</a>expr, other text";
        String sRegex = "<(\\S+?)(.*?)>(.*?)</\\1>?|(expr)";

        String replacement = ReplaceCallback.replace(sRegex, subjectString, new ReplaceCallback.Callback() {
            @Override
            @SuppressWarnings("unused")
            public String matchFound(Matcher match) {
                String possibleTagName = match.group(1);
                String possibleHref = match.group(2);
                String possibleTagValue = match.group(3);
                String possibleExpr = match.group(4);
                if (possibleExpr != null) {
                    return "<b>expr</b>";
                } else
                if (possibleHref != null) {
                    return match.group(0).replace("\"expr", "\"expr?id=1");
                }
                return match.group(0);
            }
        });

        Assert.assertEquals("begining <a href=\"expr?id=1\">expr</a><b>expr</b>, other text", replacement);
    }
}
