package cz.jmare.collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class KeepOrderSetTest {
    @Test
    public void setTest() {
        KeepOrderSet<Employee> keepOrderSet = new KeepOrderSet<Employee>();
        boolean add1 = keepOrderSet.add(createEmployee(100L, "John", "Novak"));
        Assert.assertEquals(true, add1);

        boolean add2 = keepOrderSet.add(createEmployee(100L, "John", "Nowotny"));
        Assert.assertEquals(false, add2);

        boolean add3 = keepOrderSet.add(createEmployee(99L, "John", "Nowotny"));
        Assert.assertEquals(true, add3);

        Employee createEmployee1 = createEmployee(98L, "John", "Nowotny");
        Employee createEmployee2 = createEmployee(97L, "John", "Nowotny");
        ArrayList<Employee> arrayList = new ArrayList<Employee>();
        arrayList.add(createEmployee1);
        arrayList.add(createEmployee2);
        boolean addAll = keepOrderSet.addAll(arrayList);
        Assert.assertEquals(true, addAll);

        Employee createEmployee3 = createEmployee(101L, "John", "Nowotny");
        arrayList.add(createEmployee3);
        addAll = keepOrderSet.addAll(arrayList);
        Assert.assertEquals(true, addAll);

        addAll = keepOrderSet.addAll(arrayList);
        Assert.assertEquals(false, addAll);

        Employee employee = keepOrderSet.get(createEmployee(100L, null, null));
        assertEquals("Nowotny", employee.getSurname());

        boolean contains = keepOrderSet.contains(createEmployee(1000L, null, null));
        assertEquals(false, contains);

        boolean contains2 = keepOrderSet.contains(createEmployee(100L, null, null));
        assertEquals(true, contains2);

        boolean retainAll = keepOrderSet.retainAll(arrayList);
        assertEquals(true, retainAll);
        assertTrue(keepOrderSet.toString().contains("id=98,"));
        assertTrue(keepOrderSet.toString().contains("id=97,"));
        assertTrue(keepOrderSet.toString().contains("id=101,"));
        assertTrue(!keepOrderSet.toString().contains("id=100,"));

        boolean containsAll = keepOrderSet.containsAll(arrayList);
        assertEquals(true, containsAll);

        Object[] array = keepOrderSet.toArray();
        assertEquals(3, array.length);

        Employee[] array2 = keepOrderSet.toArray(new Employee[0]);
        assertEquals(3, array2.length);

        boolean remove = keepOrderSet.remove(createEmployee2);
        assertEquals(true, remove);
        assertEquals(2, keepOrderSet.size());

        remove = keepOrderSet.remove(createEmployee(1001L, null, null));
        assertEquals(false, remove);

        boolean add = keepOrderSet.add(createEmployee2);
        assertEquals(true, add);

        ArrayList<Employee> arrayList2 = new ArrayList<Employee>();
        arrayList2.add(createEmployee(101L, "Evzen", "Dvorzak"));
        arrayList2.add(createEmployee(110L, "Eugen", "Horsky"));
        arrayList2.add(createEmployee(111L, "Peter", "Ludwik"));
        boolean addAll2 = keepOrderSet.addAll(arrayList2);
        assertEquals(true, addAll2);

        boolean removeAll = keepOrderSet.removeAll(arrayList);
        assertEquals(true, removeAll);
        assertEquals(2, keepOrderSet.size());

        keepOrderSet.clear();
        boolean empty = keepOrderSet.isEmpty();
        assertEquals(true, empty);
        assertEquals(0, keepOrderSet.size());

        // System.out.println(keepOrderSet);
    }

    public static Employee createEmployee(Long id, String firstName, String surname) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setFirstName(firstName);
        employee.setSurname(surname);
        return employee;
    }

    public static class Employee {
        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        private Long id;

        private String surname;

        private String firstName;

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Employee other = (Employee) obj;
            if (id == null) {
                if (other.id != null)
                    return false;
            } else if (!id.equals(other.id))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "Employee [id=" + id + ", firstName=" + firstName + ", surname=" + surname + "]";
        }
    }
}
