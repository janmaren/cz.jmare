package cz.jmare.collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.function.Predicate;

import org.junit.Test;

import cz.jmare.collection.KeepOrderSetTest.Employee;


public class ColutilTest {
    @Test
    public void colutilTest() {
        KeepOrderSet<Employee> keepOrderSet = new KeepOrderSet<Employee>();
        keepOrderSet.add(KeepOrderSetTest.createEmployee(101L, "John", "Novak"));
        keepOrderSet.add(KeepOrderSetTest.createEmployee(100L, "John", "Nowotny"));
        keepOrderSet.add(KeepOrderSetTest.createEmployee(99L, "John", "Nowotny"));

        Collection<Employee> nows = Colutil.filter(keepOrderSet, new Predicate<Employee>(){
            @Override
            public boolean test(Employee t) {
                return t != null && t.getSurname() != null && t.getSurname().startsWith("Nowo");
            }});
        assertEquals(2, nows.size());
        Employee[] array = nows.toArray(new Employee[0]);
        assertEquals((Long)100L, array[0].getId());
        assertEquals((Long)99L, array[1].getId());

        Employee findLast = Colutil.findLast(keepOrderSet, new Predicate<Employee>() {
            @Override
            public boolean test(Employee t) {
                return t != null && t.getSurname() != null && t.getSurname().startsWith("Nowo");
            }
        });
        assertNotNull(findLast);
        assertEquals((Long) 99L, findLast.getId());

        findLast = Colutil.findLast(keepOrderSet, new Predicate<Employee>() {
            @Override
            public boolean test(Employee t) {
                return t != null && t.getSurname() != null && t.getSurname().startsWith("xxx");
            }
        }, KeepOrderSetTest.createEmployee(120L, "Alena", "Jirsakova"));
        assertNotNull(findLast);
        assertEquals((Long) 120L, findLast.getId());

        Employee findFirst = Colutil.findFirst(keepOrderSet, new Predicate<Employee>() {
            @Override
            public boolean test(Employee t) {
                return t != null && t.getSurname() != null && t.getSurname().startsWith("Nowo");
            }
        });
        assertNotNull(findFirst);
        assertEquals((Long) 100L, findFirst.getId());

        Collection<Employee> customCol = Colutil.filter(keepOrderSet, new Predicate<Employee>(){
            @Override
            public boolean test(Employee t) {
                return t != null && t.getSurname() != null && t.getSurname().startsWith("Nowo");
            }}, new KeepOrderSet<Employee>());
        assertEquals(2, customCol.size());
        Employee[] array2 = customCol.toArray(new Employee[0]);
        assertEquals((Long) 100L, array2[0].getId());
        assertEquals((Long) 99L, array2[1].getId());
    }
}
