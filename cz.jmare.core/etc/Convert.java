package cz.jmare.str;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.util.FileLinesUtil;

public class Convert {
    private final static Pattern PAT = Pattern.compile("<li class=\"symb\" data-code=\"([^\"]+)\" title=\"([^\"]+)\">([^\"]+)</li>");
    private final static Pattern UCODE = Pattern.compile("[^ ]+");

    public static void doConvert() throws FileNotFoundException, IOException {
        File file = new File("c:/temp/chars.txt");
        ArrayList<String> readLinesAndClose = FileLinesUtil.readLinesAndClose(new FileInputStream(file), "UTF-8");
        for (String string : readLinesAndClose) {
            Matcher matcher = PAT.matcher(string);
            if (matcher.find()) {
                String potChar = matcher.group(3);
                if (!potChar.startsWith("&") && potChar.length() == 1) {
                    Character normalizeChar = CzechConvUtil.normalizeChar(potChar.charAt(0));
                    if (normalizeChar == null) {
                        String codeStr = matcher.group(2);
                        Matcher matcher2 = UCODE.matcher(codeStr);
                        if (matcher2.find()) {
                            String ucode = matcher2.group();
                            ucode = "\\u" + ucode.substring(2).toLowerCase();
                            System.out.println("{'" + potChar + "', '" + ucode + "'},");
                        } else {
                            throw new RuntimeException("not found " + codeStr);
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        doConvert();

    }

}
