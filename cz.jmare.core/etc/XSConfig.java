package cz.jmare.comp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XSConfig {
    private static XSConfig xsConfig;
    private String appName;
    private boolean recoverable;

    private XSConfig() {
    }

    public static synchronized XSConfig getInstance() {
        if (xsConfig == null) {
            xsConfig = new XSConfig();
        }
        return xsConfig;
    }

    public XSConfig init(String appName) {
        if (this.appName != null) {
            throw new RuntimeException("init called second time");
        }
        this.appName = appName;
        return this;
    }

    public XSConfig recoverable() {
        recoverable = true;
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> getList(Class<T> clazz) throws IOException {
        String name = getListFileName(clazz);
        XStream xStream = new XStream(new DomDriver());
        File file = new File(getExistingDataDir(), name);
        if (!file.isFile()) {
            return new ArrayList<T>();
        }
        try (Reader reader = new InputStreamReader(new FileInputStream(file))) {
            try {
                return (List<T>) xStream.fromXML(reader);
            } catch (XStreamException e) {
                if (recoverable) {
                    Files.move(file.toPath(), new File(file.getParent(), file.getName() + "." + System.currentTimeMillis() + ".bak").toPath());
                    return new ArrayList<T>();
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> clazz) throws IOException {
        String name = getFileName(clazz);
        XStream xStream = new XStream(new DomDriver());
        File file = new File(getExistingDataDir(), name);
        if (!file.isFile()) {
            try {
                return clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        try (Reader reader = new InputStreamReader(new FileInputStream(file))) {
            try {
                return (T) xStream.fromXML(reader);
            } catch (XStreamException e) {
                if (recoverable) {
                    Files.move(file.toPath(), new File(file.getParent(), file.getName() + "." + System.currentTimeMillis() + ".bak").toPath());
                    try {
                        return clazz.newInstance();
                    } catch (InstantiationException | IllegalAccessException e1) {
                        throw new RuntimeException(e);
                    }
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public <T> void addToList(T t) throws IOException {
        @SuppressWarnings("unchecked")
        Class<T> class1 = (Class<T>) t.getClass();
        addToList(t, class1);
    }

    public <T extends U, U> void addToList(T t, Class<U> clazz) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        List<U> tList = getList(clazz);
        tList.add(t);
        String name = getListFileName(clazz);
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(new File(getExistingDataDir(), name)))) {
            xStream.toXML(tList, out);
        }
    }

    public <T> void removeFromList(T t) throws IOException {
        @SuppressWarnings("unchecked")
        Class<T> class1 = (Class<T>) t.getClass();
        removeFromList(t, class1);
    }

    public <T extends U, U> void removeFromList(T t, Class<U> clazz) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        List<U> tList = getList(clazz);
        tList.remove(t);
        String name = getListFileName(clazz);
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(new File(getExistingDataDir(), name)))) {
            xStream.toXML(tList, out);
        }
    }

    public <T> void setList(List<T> list, Class<T> clazz) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        String name = getListFileName(clazz);
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(new File(getExistingDataDir(), name)))) {
            xStream.toXML(list, out);
        }
    }

    public <T> void set(T t) throws IOException {
        set(t, t.getClass());
    }

    public <T> void set(T t, Class<?> clazz) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        String name = getFileName(clazz);
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(new File(getExistingDataDir(), name)))) {
            xStream.toXML(t, out);
        }
    }

    private static String getListFileName(Class<?> clazz) {
        String name = clazz.getSimpleName();
        name = name.substring(0, 1).toLowerCase() + name.substring(1) + "List.xml";
        return name;
    }

    private static String getFileName(Class<?> clazz) {
        String name = clazz.getSimpleName();
        name = name.substring(0, 1).toLowerCase() + name.substring(1) + ".xml";
        return name;
    }

    public File getExistingDataDir() {
        if (appName == null) {
            throw new RuntimeException("init now called - unknown appName");
        }
        String homeDir = System.getProperty("user.home");
        File homeDirFile = new File(homeDir);
        File appDirFile = new File(homeDirFile, appName);
        appDirFile.mkdirs();
        return appDirFile;
    }
}
