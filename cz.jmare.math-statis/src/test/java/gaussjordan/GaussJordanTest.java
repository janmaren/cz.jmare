package gaussjordan;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.junit.jupiter.api.Test;
import org.maren.statis.gaussjordan.GaussJordanElimination;

public class GaussJordanTest {
    @Test
    public void test1() {
        double[][] A = {
                { 0, 1,  1 },
                { 2, 4, -2 },
                { 0, 3, 15 }
            };
            double[] b = { 4, 2, 36 };

        long start = System.nanoTime();
        double[] solve = GaussJordanElimination.solve(A, b);
        long end = System.nanoTime();
        System.out.println("dobaa " + (end - start));

        assertNotNull(solve);
        assertEquals(-1.0, solve[0], 0.01);
        assertEquals(2.0, solve[1], 0.01);
        assertEquals(2.0, solve[2], 0.01);
    }

    @Test
    public void test1A() {
        double[][] A = {
                { 0, 1,  1 },
                { 2, 4, -2 },
                { 0, 3, 15 }
        };
        double[] b = { 4, 2, 36 };



        RealMatrix coefficients =
                new Array2DRowRealMatrix(A,
                        false);
        long start = System.nanoTime();
        DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();


        RealVector constants = new ArrayRealVector(b, false);
        RealVector solution = solver.solve(constants);
        long end = System.nanoTime();
        System.out.println("doba " + (end - start));

        assertNotNull(solution);
        assertEquals(-1.0, solution.getEntry(0), 0.01);
        assertEquals(2.0, solution.getEntry(1), 0.01);
        assertEquals(2.0, solution.getEntry(2), 0.01);
    }

    @Test
    public void test2() {
        double[][] A = {
                {  1, -3,   1 },
                {  2, -8,   8 },
                { -6,  3, -15 }
            };
        double[] b = { 4, -2, 9 };
        double[] solve = GaussJordanElimination.solve(A, b);
        assertNotNull(solve);
        assertEquals(3.00000, solve[0], 0.000001);
        assertEquals(-1.0000, solve[1], 0.000001);
        assertEquals(-2.0000, solve[2], 0.000001);
    }

    @Test
    public void test3SingularNoSolutions() {
        double[][] A = {
                {  2, -3, -1,  2,  3 },
                {  4, -4, -1,  4, 11 },
                {  2, -5, -2,  2, -1 },
                {  0,  2,  1,  0,  4 },
                { -4,  6,  0,  0,  7 },
            };
        double[] b = { 4, 4, 9, -6, 5 };
        double[] solve = GaussJordanElimination.solve(A, b);
        assertNull(solve);
    }

    @Test
    public void test4InfinitelyManySolutions() {
        double[][] A = {
                {  2, -3, -1,  2,  3 },
                {  4, -4, -1,  4, 11 },
                {  2, -5, -2,  2, -1 },
                {  0,  2,  1,  0,  4 },
                { -4,  6,  0,  0,  7 },
            };
        double[] b = { 4, 4, 9, -5, 5 };
        double[] solve = GaussJordanElimination.solve(A, b);
        assertNull(solve);
    }

    @Test
    public void test5NoSolutions() {
        double[][] A = {
                {  2, -1,  1 },
                {  3,  2, -4 },
                { -6,  3, -3 },
            };
        double[] b = { 1, 4, 2 };
        double[] solve = GaussJordanElimination.solve(A, b);
        assertNull(solve);
    }

    @Test
    public void test6InfinitelyManySolutions() {
        double[][] A = {
                {  1, -1,  2 },
                {  4,  4, -2 },
                { -2,  2, -4 },
            };
        double[] b = { -3, 1, 6 };
        double[] solve = GaussJordanElimination.solve(A, b);
        assertNull(solve);
    }
}
