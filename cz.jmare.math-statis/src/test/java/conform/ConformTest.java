package conform;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.maren.statis.conformal.ConformalElements;
import org.maren.statis.conformal.ConformalTransCoefsCalculator;
import org.maren.statis.conformal.ConformalTwoPointsCalculator;
import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;

public class ConformTest {
    @Test
    public void testConform() {
        List<ControlPoint> list = createPoints();
        Coordinate co = new Coordinate(121.48, 22.78);
        
        ConformalElements el = ConformalTwoPointsCalculator.calcCoefs(list);
        Coordinate t1 = el.transform(co);
        assertEquals(305.4694594594594, t1.x, 0.0001);
        assertEquals(378.73324324324324, t1.y, 0.0001);
        
        ConformalElements el2 = ConformalTransCoefsCalculator.calcCoefs(list);
        Coordinate t2 = el2.transform(co);
        assertEquals(305.4694594594594, t2.x, 0.0001);
        assertEquals(378.73324324324324, t2.y, 0.0001);
    }

    private List<ControlPoint> createPoints() {
        ControlPoint c1 = new ControlPoint(10, 10, 350, 190);
        ControlPoint c2 = new ControlPoint(80, 60, 250, 300);
        List<ControlPoint> list = List.of(c1, c2);
        return list;
    }
}
