package affine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.maren.statis.affine.AffTrans;
import org.maren.statis.affine.AffineElements;
import org.maren.statis.entity.Coordinate;

public class AffineTest {
    @Test
    public void testBasicAffine() {
        List<Coordinate> originCoordinates = List.of(
                new Coordinate(1713.0, 1509.0), new Coordinate(1909.0, 521.0), new Coordinate(2729.0, 1389.0)
                );
        List<Coordinate> targetCoordinates = List.of(
                new Coordinate(57.29058333333333, 24.05341388888889), new Coordinate(57.027249999999995, 24.98841388888889), new Coordinate(56.218875, 24.11333888888889)
                );
        double[] coefs = AffTrans.calcAffine(originCoordinates, targetCoordinates);
        Coordinate p1 = AffTrans.transformAffine(originCoordinates.get(0), coefs);
        Coordinate p2 = AffTrans.transformAffine(originCoordinates.get(1), coefs);
        Coordinate p3 = AffTrans.transformAffine(originCoordinates.get(2), coefs);
        assertEquals(targetCoordinates.get(0).x, p1.x, 0.0000000001);
        assertEquals(targetCoordinates.get(0).y, p1.y, 0.0000000001);
        assertEquals(targetCoordinates.get(1).x, p2.x, 0.0000000001);
        assertEquals(targetCoordinates.get(1).y, p2.y, 0.0000000001);
        assertEquals(targetCoordinates.get(2).x, p3.x, 0.0000000001);
        assertEquals(targetCoordinates.get(2).y, p3.y, 0.0000000001);
    }

    @Test
    public void testSwtAffine() {
        List<Coordinate> originCoordinates = List.of(
                new Coordinate(1713.0, 1509.0), new Coordinate(1909.0, 521.0), new Coordinate(2729.0, 1389.0)
                );
        List<Coordinate> targetCoordinates = List.of(
                new Coordinate(57.29058333333333, 24.05341388888889), new Coordinate(57.027249999999995, 24.98841388888889), new Coordinate(56.218875, 24.11333888888889)
                );
        float[] coefs = AffTrans.calcAffineMatrixFloats(originCoordinates, targetCoordinates);
        Coordinate p1 = AffTrans.transformByMatrixFloats(originCoordinates.get(0), coefs);
        Coordinate p2 = AffTrans.transformByMatrixFloats(originCoordinates.get(1), coefs);
        Coordinate p3 = AffTrans.transformByMatrixFloats(originCoordinates.get(2), coefs);
        assertEquals(targetCoordinates.get(0).x, p1.x, 0.00001);
        assertEquals(targetCoordinates.get(0).y, p1.y, 0.00001);
        assertEquals(targetCoordinates.get(1).x, p2.x, 0.00001);
        assertEquals(targetCoordinates.get(1).y, p2.y, 0.00001);
        assertEquals(targetCoordinates.get(2).x, p3.x, 0.00001);
        assertEquals(targetCoordinates.get(2).y, p3.y, 0.00001);
    }
    
    @Test
    @Disabled
    public void testBasicAffineXXX() {
        List<Coordinate> originCoordinates = List.of(
                new Coordinate(1, 1), new Coordinate(4, 1), new Coordinate(4, 4)
                );
        List<Coordinate> targetCoordinates = List.of(
                new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(1, 1)
                );
        double[] coefs = AffTrans.calcAffine(originCoordinates, targetCoordinates);
        AffineElements affineElements = new AffineElements(coefs);
        for (int i = 0; i <=6; i++) {
            for (int j = 0; j <= 6; j++) {
                System.out.println(i + ", " + j + ": " + affineElements.transform(new Coordinate(i, j)));
            }
        }
    }
    
    @Test
    @Disabled
    public void testBasicAffine05() {
        List<Coordinate> originCoordinates = List.of(
                new Coordinate(1.5, 1.5), new Coordinate(4.5, 1.5), new Coordinate(4.5, 4.5)
                );
        List<Coordinate> targetCoordinates = List.of(
                new Coordinate(0.5, 0.5), new Coordinate(1.5, 0.5), new Coordinate(1.5, 1.5)
                );
        double[] coefs = AffTrans.calcAffine(originCoordinates, targetCoordinates);
        AffineElements affineElements = new AffineElements(coefs);
        for (int i = 0; i <=6; i++) {
            for (int j = 0; j <= 6; j++) {
                System.out.println(i + ", " + j + ": " + affineElements.transform(new Coordinate(i, j)));
            }
        }
    }
}
