package org.maren.statis.affine;

import java.util.List;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;
import org.maren.statis.gaussjordan.GaussJordanElimination;

/**
 * Calculations of affine elements with exactly 3 control points (no least square algorithm)
 */
public class AffTrans {
    /**
     * Transform point by coefs which have order:
     * [0] m11 the first element of the first row of the matrix
     * [1] m12 the second element of the first row of the matrix
     * [2] dx the third element of the first row of the matrix
     * [3] m21 the first element of the second row of the matrix
     * [4] m22 the second element of the second row of the matrix
     * [5] dy the third element of the second row of the matrix
     * @param point point to transform
     * @param coefs coefs with given order
     * @return
     */
    public static Coordinate transformAffine(Coordinate point, double[] coefs) {
        return new Coordinate(point.x * coefs[0] + point.y * coefs[1] + coefs[2], point.x * coefs[3] + point.y * coefs[4] + coefs[5]);
    }

    /**
     * Calculate coefficients for affine transformation
     * [0] m11 the first element of the first row of the matrix
     * [1] m12 the second element of the first row of the matrix
     * [2] dx the third element of the first row of the matrix
     * [3] m21 the first element of the second row of the matrix
     * [4] m22 the second element of the second row of the matrix
     * [5] dy the third element of the second row of the matrix
     * @param originCoordinates
     * @param targetCoordinates
     * @return affine elements or null when unable to find
     */
    public static double[] calcAffine(List<Coordinate> originCoordinates, List<Coordinate> targetCoordinates) {
        if (originCoordinates.size() != 3) {
            throw new IllegalArgumentException("Just 3 points must be passed");
        }
        if (originCoordinates.size() != targetCoordinates.size()) {
            throw new IllegalArgumentException("Origin and targetpoints have different sizes");
        }
        double x1 = originCoordinates.get(0).x;
        double y1 = originCoordinates.get(0).y;
        double x2 = originCoordinates.get(1).x;
        double y2 = originCoordinates.get(1).y;
        double x3 = originCoordinates.get(2).x;
        double y3 = originCoordinates.get(2).y;
        double[][] A = { { x1, y1, 1, 0, 0, 0 }, { 0, 0, 0, x1, y1, 1 }, { x2, y2, 1, 0, 0, 0 }, { 0, 0, 0, x2, y2, 1 },
                { x3, y3, 1, 0, 0, 0 }, { 0, 0, 0, x3, y3, 1 } };

        double[] b = { targetCoordinates.get(0).x, targetCoordinates.get(0).y, targetCoordinates.get(1).x, targetCoordinates.get(1).y,
                targetCoordinates.get(2).x, targetCoordinates.get(2).y };
        double[] coefs = GaussJordanElimination.solve(A, b);
        return coefs;
    }

    /**
     * Same order as org.eclipse.swt.graphics.Transform.setElements(float, float, float, float, float, float)
     * or java.awt.geom.AffineTransform
     * @param originCoordinates
     * @param targetCoordinates
     * @return elements in order m[line][column]: m00, m10, m01, m11, m02, m12
     * @throws IllegalArgumentException when unable to find solution
     */
    public static float[] calcAffineMatrixFloats(List<Coordinate> originCoordinates, List<Coordinate> targetCoordinates) {
        double[] doubles = AffTrans.calcAffine(originCoordinates, targetCoordinates);
        if (doubles == null) {
            throw new IllegalArgumentException("Unable to find solution for affine transformation");
        }
        float[] floats = new float[6];
        floats[0] = (float) doubles[0];
        floats[1] = (float) doubles[3];
        floats[2] = (float) doubles[1];
        floats[3] = (float) doubles[4];
        floats[4] = (float) doubles[2];
        floats[5] = (float) doubles[5];
        return floats;
    }

    /**
     * Calc affine elements
     * @param originCoordinates
     * @param targetCoordinates
     * @return
     * @throws IllegalArgumentException when unable to find solution
     */
    public static AffineElements calcAffineElements(List<Coordinate> originCoordinates, List<Coordinate> targetCoordinates) {
        double[] doubles = AffTrans.calcAffine(originCoordinates, targetCoordinates);
        if (doubles == null) {
            throw new IllegalArgumentException("Unable to find solution for affine transformation");
        }
        AffineElements elements = new AffineElements(doubles[0], doubles[2], doubles[4], doubles[1], doubles[3], doubles[5]);
        return elements;
    }

    public static AffineElements calcAffineElements(List<ControlPoint> refCoordinates) {
        if (refCoordinates.size() != 3) {
            throw new IllegalArgumentException("Just 3 points must be passed");
        }
        double[] doubles = AffTrans.calcAffine(
                List.of(refCoordinates.get(0).getPoint1(), refCoordinates.get(1).getPoint1(), refCoordinates.get(2).getPoint1()),
                List.of(refCoordinates.get(0).getPoint2(), refCoordinates.get(1).getPoint2(), refCoordinates.get(2).getPoint2()));
        AffineElements elements = new AffineElements(doubles[0], doubles[1], doubles[2], doubles[3], doubles[4], doubles[5]);
        return elements;
    }

    /**
     * Transform coordinates by given elements<br>
     * Same order as org.eclipse.swt.graphics.Transform.setElements(float, float, float, float, float, float)
     * or java.awt.geom.AffineTransform
     * @param point point to transform
     * @param elements elements in order m[line][column]: m00, m10, m01, m11, m02, m12
     * @return
     */
    public static Coordinate transformByMatrixFloats(Coordinate point, float[] elements) {
        return new Coordinate(point.x * elements[0] + point.y * elements[2] + elements[4], point.x * elements[1] + point.y * elements[3] + elements[5]);
    }

    public static Coordinate transform(Coordinate point, AffineElements elements) {
        return new Coordinate(point.x * elements.a + point.y * elements.b + elements.c, point.x * elements.d + point.y * elements.e + elements.f);
    }
}
