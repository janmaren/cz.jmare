package org.maren.statis.affine;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;

public class AffineTransCalcUtil {
    public static AffineElements calcAffineElements(List<ControlPoint> controlPoints) {
        return AffineTransCoefsCalculator.calcCoefs(controlPoints);
    }
    
    public static AffineElements calcAffineElements(List<ControlPoint> controlPoints, 
            Coordinate centerImgPixel) {
        
        List<ControlPoint> refPoints = controlPoints.stream()
                .map(cp -> new ControlPoint(subtractPoint(cp.getPoint1(), centerImgPixel), 
                        cp.getPoint2()))
                .collect(Collectors.toList());

        return AffineTransCoefsCalculator.calcCoefs(refPoints);
    }
    
    /**
     * Calc standard deviation for affineElements and original control points
     * @param affineElements
     * @param controlPoints
     * @param diffCalc calculate distance between expected and transformed point
     * @return standard deviation
     */
    public static double calcStandardDeviation(AffineElements affineElements, 
            List<ControlPoint> controlPoints, BiFunction<Coordinate, Coordinate, Double> diffCalc) {
        double sumPowerErrors = 0;
        int count = 0;
        
        for (ControlPoint controlPoint : controlPoints) {
            Coordinate transformed = affineElements.transform(controlPoint.getPoint1());
            double error = diffCalc.apply(controlPoint.getPoint2(), transformed);
            sumPowerErrors += error * error;
            count++;
        }

        return Math.sqrt(sumPowerErrors / count);
    }
    
    private static Coordinate subtractPoint(Coordinate imgPoint, Coordinate centerImgPixel) {
        return new Coordinate(imgPoint.x - centerImgPixel.x, imgPoint.y - centerImgPixel.y);
    }
}
