package org.maren.statis.affine;

import static org.maren.statis.leastsquare.LeastSquaresCalc.solve;

import java.util.List;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Matrix;

/**
 * Calculate affine coefficients using 3 or more control points using least square algorithm 
 */
public class AffineTransCoefsCalculator {
    /**
     * Calculate coefficients a, b, c, d, e, f for affine transformation or null when unpossible to do it
     */
    public static AffineElements calcCoefs(List<ControlPoint> refPoints) {
        if (refPoints.size() < 3) {
            throw new IllegalStateException("Not at least 3 ref. points passed");
        }
        double[][] rows = new double[refPoints.size() * 2][];
        double[] outputs = new double[refPoints.size() * 2];
        for (int i = 0; i < refPoints.size(); i++) {
            ControlPoint twoPoints = refPoints.get(i);
            rows[i * 2] = new double[6];
            rows[i * 2 + 1] = new double[6];
            rows[i * 2][0] = twoPoints.x1;
            rows[i * 2][1] = twoPoints.y1;
            rows[i * 2][2] = 1;
            rows[i * 2][3] = 0;
            rows[i * 2][4] = 0;
            rows[i * 2][5] = 0;
            
            rows[i * 2 + 1][0] = 0;
            rows[i * 2 + 1][1] = 0;
            rows[i * 2 + 1][2] = 0;
            rows[i * 2 + 1][3] = twoPoints.x1;
            rows[i * 2 + 1][4] = twoPoints.y1;
            rows[i * 2 + 1][5] = 1;
            
            outputs[i * 2] = twoPoints.x2;
            outputs[i * 2 + 1] = twoPoints.y2;
        }
        
        Matrix A = new Matrix(rows);
        Matrix b = Matrix.ofRowsOneD(outputs);
        Matrix abcdef = solve(A, b);
        
        if (abcdef == null) {
            return null;
        }
        
        return new AffineElements(abcdef.toArrayOneD());
    }
}
