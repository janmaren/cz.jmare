package org.maren.statis.affine;

import org.maren.statis.entity.Coordinate;

/**
 * Coeffiecients for affine tranformation for: x' = a * x + b * y + c; y' = d * x + e * y + f
 */
public class AffineElements {
    public double a, b, c, d, e, f;

    public AffineElements(double a, double b, double c, double d, double e, double f) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    public AffineElements(double[] coefs) {
        super();
        this.a = coefs[0];
        this.b = coefs[1];
        this.c = coefs[2];
        this.d = coefs[3];
        this.e = coefs[4];
        this.f = coefs[5];
    }

    public Coordinate transform(Coordinate point) {
        return new Coordinate(point.x * a + point.y * b + c, point.x * d + point.y * e + f);
    }
    
    public Coordinate transform(double x, double y) {
        return new Coordinate(x * a + y * b + c, x * d + y * e + f);
    }
    
    @Override
    public String toString() {
        return "Elements [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + ", e=" + e + ", f=" + f + "]";
    }
}
