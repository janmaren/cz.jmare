package org.maren.statis.entity;

import java.util.ArrayList;
import java.util.List;

public class ControlPoint {
    public double x1;

    public double y1;

    public double x2;

    public double y2;

    public ControlPoint() {

    }

    public ControlPoint(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public ControlPoint(Coordinate point1, Coordinate point2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    public ControlPoint(double x1, double y1, Coordinate point2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    public ControlPoint(Coordinate point1, double x2, double y2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public String toString() {
        return "TwoPoints [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + "]";
    }

    public Coordinate getPoint1() {
        return new Coordinate(x1, y1);
    }

    public Coordinate getPoint2() {
        return new Coordinate(x2, y2);
    }

    public static List<ControlPoint> buildTwoPointsList(List<Coordinate> points1, List<Coordinate> points2) {
        if (points1.size() != points2.size()) {
            throw new IllegalArgumentException("Lists haven't the same sizes");
        }
        List<ControlPoint> result = new ArrayList<ControlPoint>();
        for (int i = 0; i < points1.size(); i++) {
            Coordinate point1 = points1.get(i);
            Coordinate point2 = points2.get(i);
            result.add(new ControlPoint(point1, point2));
        }

        return result;
    }

    public static List<Coordinate> extractPoints1(List<ControlPoint> twoPoints) {
        List<Coordinate> result = new ArrayList<Coordinate>();
        for (ControlPoint twoPoint : twoPoints) {
            result.add(twoPoint.getPoint1());
        }
        return result;
    }

    public static List<Coordinate> extractPoints2(List<ControlPoint> twoPoints) {
        List<Coordinate> result = new ArrayList<Coordinate>();
        for (ControlPoint twoPoint : twoPoints) {
            result.add(twoPoint.getPoint2());
        }
        return result;
    }
}
