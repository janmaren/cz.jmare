package org.maren.statis.conformal;

import java.util.List;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;
import org.maren.statis.gaussjordan.GaussJordanElimination;

public class ConformalTwoPointsCalculator {
    /**
     * Calculate coefficients a, b, c, d for conformal transformation using exactly 2 points
     */
    public static ConformalElements calcCoefs(List<ControlPoint> refPoints) {
        if (refPoints.size() != 2) {
            throw new IllegalArgumentException("Just 2 points must be passed");
        }
        double x1 = refPoints.get(0).x1;
        double y1 = refPoints.get(0).y1;
        double x2 = refPoints.get(1).x1;
        double y2 = refPoints.get(1).y1;

        double[][] A = { {x1, y1, 1, 0}, {y1, -x1, 0, 1}, {x2, y2, 1, 0}, {y2, -x2, 0, 1} };

        double[] b = { refPoints.get(0).x2, refPoints.get(0).y2, refPoints.get(1).x2, refPoints.get(1).y2};
        double[] coefs = GaussJordanElimination.solve(A, b);
        
        return new ConformalElements(coefs);
    }
    
    public static void main(String[] args) {
        ControlPoint c1 = new ControlPoint(10, 10, 350, 190);
        ControlPoint c2 = new ControlPoint(80, 60, 250, 300);
        List<ControlPoint> list = List.of(c1, c2);
        ConformalElements el = calcCoefs(list);
        System.out.println(el);
        
        Coordinate co = new Coordinate(121.48, 22.78);
        System.out.println(el.a * co.x + el.b * co.y + el.c);
        System.out.println(-el.b * co.x + el.a * co.y + el.d);
        
        Coordinate transform = el.transform(co);
        System.out.println(transform);
        
        
        ConformalElements calcCoefs = ConformalTransCoefsCalculator.calcCoefs(list);
        Coordinate transform2 = calcCoefs.transform(co);
        System.out.println(transform2);
        
    }
}
