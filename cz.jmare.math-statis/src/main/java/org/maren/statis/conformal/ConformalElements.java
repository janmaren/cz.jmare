package org.maren.statis.conformal;

import org.maren.statis.entity.Coordinate;

/**
 * Coeffiecients for conformal tranformation for: x' = a * x - b * y + c; y' = a * y + b * x + d
 */
public class ConformalElements {
    public double a, b, c, d;

    public ConformalElements(double a, double b, double c, double d) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public ConformalElements(double[] coefs) {
        super();
        this.a = coefs[0];
        this.b = coefs[1];
        this.c = coefs[2];
        this.d = coefs[3];
    }
    
    public Coordinate transform(Coordinate point) {
        return new Coordinate(a * point.x + b * point.y + c, -b * point.x + a * point.y + d);
    }

    @Override
    public String toString() {
        return "Elements [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + "]";
    }
}
