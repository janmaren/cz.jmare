package org.maren.statis.conformal;

import static org.maren.statis.leastsquare.LeastSquaresCalc.solve;

import java.util.List;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;
import org.maren.statis.entity.Matrix;

public class ConformalTransCoefsCalculator {
    /**
     * Calculate coefficients a, b, c, d for conformal transformation
     */
    public static ConformalElements calcCoefs(List<ControlPoint> refPoints) {
        double[][] rows = new double[refPoints.size() * 2][];
        double[] outputs = new double[refPoints.size() * 2];
        for (int i = 0; i < refPoints.size(); i++) {
            ControlPoint ControlPoint = refPoints.get(i);
            rows[i * 2] = new double[4];
            rows[i * 2 + 1] = new double[4];
            rows[i * 2][0] = ControlPoint.x1;
            rows[i * 2][1] = -ControlPoint.y1;
            rows[i * 2][2] = 1;
            rows[i * 2][3] = 0;
            
            rows[i * 2 + 1][0] = ControlPoint.y1;
            rows[i * 2 + 1][1] = ControlPoint.x1;
            rows[i * 2 + 1][2] = 0;
            rows[i * 2 + 1][3] = 1;
            
            outputs[i * 2] = ControlPoint.x2;
            outputs[i * 2 + 1] = ControlPoint.y2;
        }
        
        Matrix A = new Matrix(rows);
        Matrix b = Matrix.ofRowsOneD(outputs);
        Matrix abcdef = solve(A, b);
        
        double[] arrayOneD = abcdef.toArrayOneD();
        arrayOneD[1] = -arrayOneD[1];
        return new ConformalElements(arrayOneD);
    }
    
    public static void main(String[] args) {
        ControlPoint ControlPoint1 = new ControlPoint(121.622, -128.066, 1049422.40, 51089.20);
        ControlPoint ControlPoint2 = new ControlPoint(141.228, 187.718, 1049413.95, 49659.30);
        ControlPoint ControlPoint3 = new ControlPoint(175.802, 135.728, 1049244.95, 49884.95);
        List<ControlPoint> list = List.of(ControlPoint1, ControlPoint2, ControlPoint3);
        ConformalElements conformalElements = calcCoefs(list);
        System.out.println(conformalElements);
        
        Coordinate transform = conformalElements.transform(new Coordinate(174.148, -120.262));
        System.out.println(transform);
    }
}
