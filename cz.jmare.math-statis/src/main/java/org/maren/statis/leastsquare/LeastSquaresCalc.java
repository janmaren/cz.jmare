package org.maren.statis.leastsquare;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.maren.statis.entity.Matrix;

public class LeastSquaresCalc {

    public static Matrix solve(Matrix A, Matrix b) {
        Matrix squareSymmetric = A.transpose().multiply(A);
        Matrix AtransposeB = A.transpose().multiply(b);
        
        LinearSolver solver = new LinearSolver(squareSymmetric, AtransposeB);
        Matrix solution = solver.getSolution();
        
        return solution;
    }
    
    public static Matrix solve(String file) {
        try {
            Matrix[] matrixes = parseTwoMatrixes(file);
            return solve(matrixes[0], matrixes[1]);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read file " + file, e);
        }
    }
    
    public static Matrix[] parseTwoMatrixes(String file) throws IOException {
        String str = Files.readString(Paths.get(file));
        List<List<Double>> m1 = new ArrayList<List<Double>>();
        List<List<Double>> m2 = null;
        try (Scanner scanner = new Scanner(str)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.isBlank()) {
                    if (m2 != null) {
                        break;
                    } else {
                        m2 = new ArrayList<List<Double>>();
                        continue;
                    }
                }
                String[] row = line.trim().split("\\s+");
                List<Double> rowDoubles = new ArrayList<Double>();
                for (String string : row) {
                    rowDoubles.add(Double.valueOf(string));
                }
                if (m2 == null) {
                    if (m1.size() > 0) {
                        if (m1.get(0).size() != rowDoubles.size()) {
                            throw new IllegalStateException("Row " + line + " doesn't contain the same number of elements like previous (" + m1.get(0).size() + ")");
                        }
                    }
                    m1.add(rowDoubles);
                } else {
                    if (m2.size() > 0) {
                        if (m2.get(0).size() != rowDoubles.size()) {
                            throw new IllegalStateException("Row " + line + " doesn't contain the same number of elements like previous (" + m2.get(0).size() + ")");
                        }
                    }
                    m2.add(rowDoubles);
                }
            }
        }
        
        Matrix matrix1 = new Matrix(m1.size(), m1.get(0).size());
        Matrix matrix2 = new Matrix(m2.size(), m2.get(0).size());
        
        for (int i = 0; i < m1.size(); i++) {
            List<Double> list = m1.get(i);
            for (int j = 0; j < list.size(); j++) {
                matrix1.set(list.get(j), i, j);
            }
        }
        
        for (int i = 0; i < m2.size(); i++) {
            List<Double> list = m2.get(i);
            for (int j = 0; j < list.size(); j++) {
                matrix2.set(list.get(j), i, j);
            }
        }
        
        return new Matrix[] {matrix1, matrix2};
    }
    
    public static void main(String[] args) {
        Matrix matr = solve("C:\\Users\\E_JMAREN\\Documents\\matrixes.txt");
        System.out.println(matr);
    }
}
