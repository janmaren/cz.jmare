package org.maren.statis.gaussjordan;

import java.util.Arrays;

/**
 * https://www.sanfoundry.com/java-program-gaussian-elimination-algorithm/
 */
public class GaussJordanElimination {
    private static final double EPSILON = 1e-8;

    /**
     * Solve Gauss Jordan
     * @param A
     * @param B
     * @return elements or null when unable to solve
     */
    public static double[] solve(double[][] A, double[] B)    {
        int N = B.length;
        for (int k = 0; k < N; k++) {
            /** find pivot row **/
            int max = k;
            for (int i = k + 1; i < N; i++) {
                if (Math.abs(A[i][k]) > Math.abs(A[max][k])) {
                    max = i;
                }
            }

            /** swap row in A matrix **/
            double[] temp = A[k];
            A[k] = A[max];
            A[max] = temp;

            /** swap corresponding values in constants matrix **/
            double t = B[k];
            B[k] = B[max];
            B[max] = t;

            /** pivot within A and B **/
            for (int i = k + 1; i < N; i++) {
                if (Math.abs(A[k][k]) < EPSILON) {
                    return null;
                }
                double factor = A[i][k] / A[k][k];
                B[i] -= factor * B[k];
                for (int j = k; j < N; j++) {
                    A[i][j] -= factor * A[k][j];
                }
            }
        }

        double[] solution = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < N; j++) {
                sum += A[i][j] * solution[j];
            }
            if (Math.abs(A[i][i]) < EPSILON) {
                return null;
            }
            solution[i] = (B[i] - sum) / A[i][i];
        }
        return solution;
    }

    public static void main(String[] args) {
        double[][] A = {
                {  1, -3,   1 },
                {  2, -8,   8 },
                { -6,  3, -15 }
            };
            double[] b = { 4, -2, 9 };
        for (int i = 0; i < 5; i++) {
            try {
                long start1 = System.nanoTime();
                double[] solve = solve(A, b);
                long end1 = System.nanoTime();
                System.out.println(Arrays.toString(solve));
                System.out.println("Nanos solve:" + (end1 - start1));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            long start2 = System.nanoTime();
//            GaussJordanElimination gaussian = new GaussJordanElimination(A, beta);
//            double[] primal = gaussian.primal();
//            long end2 = System.nanoTime();
//            System.out.println(Arrays.toString(primal));
//            System.out.println("Nanos gauss:" + (end2 - start2));
        }
    }
}
