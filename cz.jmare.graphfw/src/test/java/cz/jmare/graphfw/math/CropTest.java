package cz.jmare.graphfw.math;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.LineSegmentFloat;
import cz.jmare.math.geometry.entity.StraightLine;

public class CropTest {
    @Test
    public void topBottomCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(3, -1, 3, 7));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(3, 0, 3, 7), crop));
    }

    @Test
    public void leftTopCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(-1, 4, 2, 7));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(0, 5, 1, 6), crop));
    }

    @Test
    public void rightTopCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(4, 7, 7, 4));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(5, 6, 6, 5), crop));
    }

    @Test
    public void rightBottomCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(4, -1, 7, 2));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(5, 0, 6, 1), crop));
    }

    @Test
    public void leftBottomCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(-1, 2, 2, -1));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(0, 1, 2, -1), crop));
    }

    @Test
    public void leftBottomPointCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(-1, 1, 1, -1));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(0, 0, 0, 0), crop));
    }

    @Test
    public void rightTopPointCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(0, 0, 6, 6);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(5, 7, 7, 5));
        assertNotNull(crop);
        assertTrue(equals(new LineSegmentFloat(6, 6, 6, 6), crop));
    }

    @Test
    public void bugCross() {
        CropWindowFloat cropWindow = new CropWindowFloat(-0.04665592f, -0.03309779f, 0.04665592f, 0.03323071f);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(0.036024366f, -0.03309843f, 0.036024764f, -0.033321965f));
        assertNull(crop);
    }

    @Test
    public void straightLineCross() {
        StraightLine straightLine = new StraightLine(1.0f, 8.0f, 2.2f, 9f);
        LineSegmentDouble crop = CropWindowDoubleFunctional.crop(straightLine, 2.0, 3.0, 7.0, 9.0);
        assertTrue(equals(new LineSegmentDouble(2.200000047683716, 9.0, 2.0, 8.833333300219643), crop));
    }

    @Test
    public void bug2Cross() {
        CropWindowFloat cropWindow = new CropWindowFloat(6.0f, 2.75f, 21.46875f, 15.0f);
        LineSegmentFloat crop = cropWindow.crop(new LineSegmentFloat(0.0f, 15.0f, 20.0f, 15.0f));
        assertNotNull(crop);
    }

    private final static boolean equals(LineSegmentFloat s1, LineSegmentFloat s2) {
        double pythag = Math.sqrt((s1.x1 - s2.x1) * (s1.x1 - s2.x1) + (s1.y1 - s2.y1) * (s1.y1 - s2.y1));
        if (pythag < 0.001) {
            return true;
        }
        pythag = Math.sqrt((s1.x1 - s2.x2) * (s1.x1 - s2.x2) + (s1.y1 - s2.y2) * (s1.y1 - s2.y2));
        if (pythag < 0.001) {
            return true;
        }
        return false;
    }
    private final static boolean equals(LineSegmentDouble s1, LineSegmentDouble s2) {
        double pythag = Math.sqrt((s1.x1 - s2.x1) * (s1.x1 - s2.x1) + (s1.y1 - s2.y1) * (s1.y1 - s2.y1));
        if (pythag < 0.001) {
            return true;
        }
        pythag = Math.sqrt((s1.x1 - s2.x2) * (s1.x1 - s2.x2) + (s1.y1 - s2.y2) * (s1.y1 - s2.y2));
        if (pythag < 0.001) {
            return true;
        }
        return false;
    }

    @Test
    @Ignore(value = "fix as soon as possible")
    public void bugExpectCrossTest() {
        // java.lang.IllegalStateException: Expected one crossection for LineSegmentDouble [-0.6438356164383562, -0.6438356164383562, -0.6404109589041096, -0.6404109589041096] in window -0.9897260273972602, -0.6404109589041096 0.9931506849315068, 0.6404109589041096 but no found
        LineSegmentDouble straightLine = new LineSegmentDouble(-0.6438356164383562, -0.6438356164383562, -0.6404109589041096, -0.6404109589041096);
        LineSegmentDouble crop = CropWindowDoubleFunctional.crop(straightLine, -0.9897260273972602, -0.6404109589041096, 0.9931506849315068, 0.6404109589041096);
        assertTrue(equals(new LineSegmentDouble(2.200000047683716, 9.0, 2.0, 8.833333300219643), crop));
    }
}
