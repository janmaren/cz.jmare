package cz.jmare.graphfw.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import cz.jmare.math.geometry.entity.LineFloatCrossableSegment;
import cz.jmare.math.geometry.entity.PointFloat;

public class LineIntCrossableSegmentTest {
    @Test
    public void noCross() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(2, 2, 4, 4);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(5, 3, 6, 2);
        assertNull(seg1.crossection(seg2));

        LineFloatCrossableSegment seg3 = new LineFloatCrossableSegment(6, 2, 5, 3);
        assertNull(seg1.crossection(seg3));
    }

    @Test
    public void crossInEdge() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(2, 2, 4, 4);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(4, 4, 6, 2);
        assertEquals(new PointFloat(4, 4), seg1.crossection(seg2));
        assertEquals(new PointFloat(4, 4), seg2.crossection(seg1));
    }

    @Test
    public void crossInEdge2() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(2, 2, 4, 4);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(3, 5, 6, 2);
        assertEquals(new PointFloat(4, 4), seg1.crossection(seg2));
        assertEquals(new PointFloat(4, 4), seg2.crossection(seg1));
    }

    @Test
    public void crossVert() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(2, 2, 2, 4);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(1, 3, 3, 3);
        assertEquals(new PointFloat(2, 3), seg1.crossection(seg2));
        assertEquals(new PointFloat(2, 3), seg2.crossection(seg1));
    }

    @Test
    public void noCrossVert() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(2, 2, 2, 4);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(1, 5, 3, 5);
        assertNull(seg1.crossection(seg2));
        assertNull(seg2.crossection(seg1));
    }

    @Test
    public void bugCrossVert() {
        LineFloatCrossableSegment seg1 = new LineFloatCrossableSegment(-0.09112486f
                ,0.09112486f
                ,-0.064644136f
                ,-0.064644136f);
        LineFloatCrossableSegment seg2 = new LineFloatCrossableSegment(0.05294f
                ,  0.05295f
                ,  -0.068169855f
                ,  -0.064643584f);
        assertEquals(new PointFloat(-0.06514374f, -0.061705325f), seg1.crossection(seg2));
    }
}
