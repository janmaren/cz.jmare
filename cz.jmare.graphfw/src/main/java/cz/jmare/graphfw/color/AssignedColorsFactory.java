package cz.jmare.graphfw.color;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Color;
import cz.jmare.swt.color.ColorUtil;

public class AssignedColorsFactory implements ColorProviderFactory<String> {
    private String initialColor;

    private List<String> colors;

    @Override
    public ColorProvider<String> getColorProvider() {
        ArrayList<Color> resultColors = new ArrayList<>();
        for (String color : colors) {
            resultColors.add(ColorUtil.getColor(color));
        }
        if (initialColor != null) {
            return new AssignedColorProvider(ColorUtil.getColor(initialColor), resultColors);
        }
        return new AssignedColorProvider(resultColors);
    }

    public void setInitialColor(String initialColor) {
        this.initialColor = initialColor;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }
}
