package cz.jmare.graphfw.color;

import org.eclipse.swt.graphics.Color;

public class ColorRange implements Comparable<ColorRange> {
    public Double fromValue;

    public Color color;

    public ColorRange(Double fromValue, Color color) {
        super();
        this.fromValue = fromValue;
        this.color = color;
    }

    @Override
    public int compareTo(ColorRange range) {
        if (fromValue < range.fromValue) {
            return -1;
        }
        if (fromValue > range.fromValue) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Range [fromValue=" + fromValue + ", color=" + color + "]";
    }
}
