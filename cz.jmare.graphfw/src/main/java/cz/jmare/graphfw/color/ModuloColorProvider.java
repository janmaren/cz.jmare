package cz.jmare.graphfw.color;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import cz.jmare.swt.color.ColorUtil;

public class ModuloColorProvider implements ColorProvider<Integer> {
    private Color initialColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

    public ModuloColorProvider() {
    }

    @Override
    public Color getColor(Integer categ) {
        if (categ == null) {
            return initialColor;
        }
        int category = categ + 1;
        int mod = category % 3;
        category *= 90001;
        int red = category & 0xFF;
        int green = (category & 0xFF00) >> 8;
        int blue = (category & 0xFF0000) >> 16;
        if (mod == 0) {
            return ColorUtil.getColorByRGB(red, blue, green);
        }
        if (mod == 1) {
            return ColorUtil.getColorByRGB(blue, green, red);
        }
        return ColorUtil.getColorByRGB(red, blue, green);
    }
}
