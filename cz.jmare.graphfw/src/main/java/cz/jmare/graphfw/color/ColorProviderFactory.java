package cz.jmare.graphfw.color;

public interface ColorProviderFactory<T> {
    ColorProvider<T> getColorProvider();
}
