package cz.jmare.graphfw.color;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.eclipse.swt.graphics.Color;

import cz.jmare.graphfw.file.ColorFileUtil;
import cz.jmare.swt.util.RGBSwtUtil;
import cz.jmare.swt.color.ColorUtil;

public class ColorMapUtil {

    @SuppressWarnings("unchecked")
    public static ColorProvider<String> getDefaultAssignedColorProvider() {
        try {
            return ColorFileUtil.loadColorProviderFromProperties(ColorMapUtil.class.getResourceAsStream("/default-assigned-colors.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load /default-assigned-colors.properties");
        }
    }

    @SuppressWarnings("unchecked")
    public static ColorProvider<Double> getDefaultRangeColorProvider() {
        try {
//            return ColorFileUtil.loadColorProvider(ColorUtil.class.getResourceAsStream("/default-range-colors.xml"), Double.class);
            return ColorFileUtil.loadColorProviderFromProperties(ColorMapUtil.class.getResourceAsStream("/default-range-colors.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load /default-range-colors.properties");
        }
    }

    @SuppressWarnings("unchecked")
    public static ColorProvider<Double> getDefaultAreaRangeColorProvider() {
        try {
            return ColorFileUtil.loadColorProviderFromProperties(ColorMapUtil.class.getResourceAsStream("/default-area-range-0-1-colors.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load /default-area-range-colors.properties");
        }
    }

    @SuppressWarnings("unchecked")
    public static ColorProvider<String> getDefaultAreaAssignedColorProvider() {
        try {
            return (ColorProvider<String>) ColorFileUtil.loadColorProviderFromProperties(ColorMapUtil.class.getResourceAsStream("/default-area-assigned-colors.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load /default-area-assigned-colors.properties");
        }
    }

    /**
     * Get category-value pairs. Ordered by category
     * @param properties
     * @return
     */
    public static LinkedHashMap<Integer, Color> loadCategoryProperties(Properties properties) {
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        Map<Integer, Color> mapUnsorted = new HashMap<Integer, Color>();
        for (Entry<Object, Object> entry : entrySet) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.startsWith("category.")) {
                String catStr = key.substring("category.".length());
                if (catStr.contains(".")) {
                    continue;
                }
                int category = Integer.parseInt(catStr);
                Color rgb = ColorUtil.getColor(RGBSwtUtil.createRGB(value));
                mapUnsorted.put(category, rgb);
            }
        }
        List<Integer> keys = new ArrayList<>(mapUnsorted.keySet());
        Collections.sort(keys);
        LinkedHashMap<Integer, Color> mapSorted = new LinkedHashMap<Integer, Color>();
        for (Integer key : keys) {
            mapSorted.put(key, mapUnsorted.get(key));
        }
        return mapSorted;
    }

    public static LinkedHashMap<Integer, PropColorItem> loadCategoryRangeProperties(Properties properties) {
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        Map<Integer, PropColorItem> mapUnsorted = new HashMap<Integer, PropColorItem>();
        for (Entry<Object, Object> entry : entrySet) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.startsWith("category.")) {
                String catStr = key.substring("category.".length());
                String oper = null;
                if (catStr.contains(".")) {
                    int indexOf = catStr.indexOf(".");
                    oper = catStr.substring(indexOf + 1);
                    catStr = catStr.substring(0, indexOf);
                }
                int category = Integer.parseInt(catStr);

                PropColorItem range = mapUnsorted.get(category);
                if (range == null) {
                    range = new PropColorItem();
                    mapUnsorted.put(category, range);
                }

                if ("fromValue".equals(oper)) {
                    range.fromValue = Double.parseDouble(value);
                } else if (oper == null) {
                    range.color = ColorUtil.getColor(RGBSwtUtil.createRGB(value));
                } else {
                    throw new IllegalStateException("Not supported property type " + oper + " for " + key + "=" + value);
                }

                mapUnsorted.put(category, range);
            }
        }
        List<Integer> keys = new ArrayList<>(mapUnsorted.keySet());
        Collections.sort(keys);
        LinkedHashMap<Integer, PropColorItem> mapSorted = new LinkedHashMap<Integer, PropColorItem>();
        for (Integer key : keys) {
            mapSorted.put(key, mapUnsorted.get(key));
        }
        return mapSorted;
    }

    public static class PropColorItem {
        public Color color;
        public double fromValue;
    }
}
