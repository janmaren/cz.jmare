package cz.jmare.graphfw.color;

public class Range {
    public Double fromValue;
    public String color;
    public Range(Double value, String color) {
        super();
        this.fromValue = value;
        this.color = color;
    }
}
