package cz.jmare.graphfw.color;

import org.eclipse.swt.graphics.Color;

public interface ColorProvider<T> {
    Color getColor(T category);
}
