package cz.jmare.graphfw.color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.jmare.swt.color.ColorUtil;

public class RangeColorsFactory implements ColorProviderFactory<Double> {
    private String initialColor;

    private List<Range> ranges;

    @Override
    public ColorProvider<Double> getColorProvider() {
        ArrayList<ColorRange> colorRanges = new ArrayList<>();
        for (Range range : ranges) {
            colorRanges.add(new ColorRange(range.fromValue, ColorUtil.getColor(range.color)));
        }

        Collections.sort(colorRanges);

        if (initialColor != null) {
            return new RangeColorProvider(ColorUtil.getColor(initialColor), colorRanges);
        }

        return new RangeColorProvider(colorRanges);
    }

    public void setInitialColor(String initialColor) {
        this.initialColor = initialColor;
    }

    public void setRanges(List<Range> ranges) {
        this.ranges = ranges;
    }
}
