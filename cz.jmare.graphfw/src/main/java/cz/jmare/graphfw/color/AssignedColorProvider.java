package cz.jmare.graphfw.color;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

public class AssignedColorProvider implements ColorProvider<String> {
    private Color initialColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

    private List<Color> colors;

    public AssignedColorProvider() {
        super();
    }

    public AssignedColorProvider(ArrayList<Color> colors) {
        this.colors = colors;
    }

    public AssignedColorProvider(Color initialColor, List<Color> colors) {
        this.initialColor = initialColor;
        this.colors = colors;
    }

    @Override
    public Color getColor(String category) {
        if (category == null) {
            return initialColor;
        }
        int hashCode = category != null ? category.hashCode() : 0;
        if (hashCode < 0) {
            hashCode = -hashCode;
        }
        int colorIndex = hashCode % colors.size();
        return colors.get(colorIndex);
    }

    public Color getColor(double value) {
        int hashCode = ((Double) value).hashCode();
        if (hashCode < 0) {
            hashCode = -hashCode;
        }
        int colorIndex = hashCode % colors.size();
        return colors.get(colorIndex);
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }
}
