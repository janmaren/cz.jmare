package cz.jmare.graphfw.color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

public class RangeColorProvider implements ColorProvider<Double> {
    private Color initialColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

    private List<ColorRange> ranges = new ArrayList<>();

    public RangeColorProvider(Color initialColor, List<ColorRange> ranges) {
        this.setInitialColor(initialColor);
        this.setRanges(ranges);
    }

    public RangeColorProvider(List<ColorRange> ranges) {
        this.setRanges(ranges);
    }

    public RangeColorProvider(Color initialColor) {
        this.setInitialColor(initialColor);
    }

    public RangeColorProvider() {
    }

    public Color getColor(String category) {
        if (category == null) {
            return initialColor;
        }
        return getColor(Double.valueOf(category));
    }

    @Override
    public Color getColor(Double value) {
        Color color = initialColor;
        for (ColorRange colorRange : ranges) {
            if (colorRange.fromValue <= value) {
                color = colorRange.color;
            }
        }
        return color;
    }

    public static void main(String[] args) {
        Color color1 = new Color(Display.getCurrent(), 10, 100, 200, 100);
        Color color2 = new Color(Display.getCurrent(), 10, 200, 200, 100);
        List<ColorRange> asList = Arrays.asList(new ColorRange(5d, color1), new ColorRange(3d, color2));
        Collections.sort(asList);
        System.out.println(asList);

    }

    public void setRanges(List<ColorRange> ranges) {
        this.ranges = ranges;
    }

    public void setInitialColor(Color initialColor) {
        this.initialColor = initialColor;
    }
}
