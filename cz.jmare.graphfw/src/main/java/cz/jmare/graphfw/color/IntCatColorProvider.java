package cz.jmare.graphfw.color;

import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.swt.color.ColorUtil;

public class IntCatColorProvider implements ColorProvider<Integer> {
    private Color initialColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

    private Map<Integer, RGBEntity> catToRGB;

    public IntCatColorProvider(Map<Integer, RGBEntity> catToRGB) {
        this.catToRGB = catToRGB;
    }

    @Override
    public Color getColor(Integer categ) {
        if (categ == null) {
            return initialColor;
        }
        RGBEntity rgb = catToRGB.get(categ);
        if (rgb == null) {
            return initialColor;
        }
        return ColorUtil.getColorByRGB(rgb.red, rgb.blue, rgb.green);
    }
}
