package cz.jmare.graphfw.color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

public class DefinedAssignedColorProvider implements ColorProvider<String> {
    protected Map<String, Integer> categoryToIndexMap = new HashMap<>();
    protected ArrayList<Color> colors;
    protected Color initialColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);

    public DefinedAssignedColorProvider(ArrayList<Color> colors, Map<String, Integer> categoryToIndexMap) {
        this.colors = colors;
        this.categoryToIndexMap = categoryToIndexMap;
    }

    public DefinedAssignedColorProvider(Color initialColor, List<Color> colors, Map<String, Integer> categoryToIndexMap) {
        this.initialColor = initialColor;
        this.categoryToIndexMap = categoryToIndexMap;
    }

    protected Integer getColorIndex(String category) {
        return categoryToIndexMap.get(category);
    }

    @Override
    public Color getColor(String category) {
        Integer index = categoryToIndexMap.get(category);
        if (index == null || index >= colors.size()) {
            return resolveNotmappedCategory(category);
        }
        return colors.get(index);
    }

    protected Color resolveNotmappedCategory(String category) {
        return initialColor;
    }

    public Color getColor(double value) {
        return getColor(String.valueOf((int) value));
    }
}
