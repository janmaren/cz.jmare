package cz.jmare.graphfw;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import cz.jmare.graphfw.graphcomposite.CanvasManagerListener;
import cz.jmare.graphfw.graphcomposite.DefaultPosFrom;
import cz.jmare.graphfw.rastercollector.RasterAreaPainter;
import cz.jmare.graphfw.tool.PaintTool;
import cz.jmare.math.geometry.entity.TwoPoints;

public class CanvasManager implements CoordState {
    private static final String DOUBLE_BUFFER_IMAGE = "double-buffer-image";

    private static final int BY_WHICH_PIXELS_AXIS_NUMBER = 50;

    private static final int SHIFT_TEXT_BY_PIXELS = 3;

    private final static DecimalFormat FORMAT_DEC_3 = new DecimalFormat("0.000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    protected Canvas canvas;

    protected RasterAreaPainter rasterAreaPainter;

    protected double zoom = 1.0;

    protected double gridStepX = 10.0f;
    protected double gridStepY = 10.0f;

    /**
     * Real position of middle x on screen (pan)
     */
    protected double posX = 0;

    /**
     * Real position of middle y on screen (pan)
     */
    protected double posY = 0;

    /**
     * Last x position
     */
    protected int screenX;

    /**
     * Last y position
     */
    protected int screenY;

    protected int screenHeight = -1;

    protected int screenWidth = -1;

    protected int startDragX = 0;
    protected int startDragy = 0;
    protected boolean dragging;

    /**
     * PaintTool instance or null when no one is selected
     */
    protected PaintTool paintTool;

    protected Color colorGrey;

    protected Shell shell;

    protected boolean holdingShift;

    protected boolean holdingCtrl;

    private boolean showGrid = true;

    private String unitsCoordY;

    private String unitsCoordX;

    private double xyScale = 1;

    private CoordsChangedListener coordsChangedListener;

    private ZoomChangedListener zoomChangedListener;

    private CanvasManagerListener canvasManagerListener;

    private boolean mainAxisGray;

    protected final static int GRID_WIDTH = 1;

    protected final static int GRID_THICK_WIDTH = 2;

    protected final static int GRID_MIN_GRID_LENGTH = 6;

    protected final static int GRID_MIN_THICK_LENGTH = 100;

    private Zooming zooming = new SimpleZooming();

    private boolean snapToGrid;

    public CanvasManager(Shell shell, Canvas canvas, CanvasManagerListener canvasManagerListener) {
        super();
        this.shell = shell;
        this.canvas = canvas;
        this.canvasManagerListener = canvasManagerListener;

        doInit();

        Display current = Display.getCurrent();
        current.addFilter(SWT.KeyDown, new Listener() {
            @Override
            public void handleEvent(Event e) {
                if (e.keyCode == 262144) {
                    holdingCtrl = true;
                } else if (e.keyCode == 131072) {
                    holdingShift = true;
                }
            }
        });
        current.addFilter(SWT.KeyUp, new Listener() {
            @Override
            public void handleEvent(Event e) {
                if (e.keyCode == 262144) {
                    holdingCtrl = false;
                } else if (e.keyCode == 131072) {
                    holdingShift = false;
                }
            }
        });
    }

    private void doInit() {
           canvas.addPaintListener(new PaintListener() {
                @Override
                public void paintControl(PaintEvent e) {
                    initScreenDimensions();
                    Image image = (Image) canvas.getData(DOUBLE_BUFFER_IMAGE);
                    if (image == null || image.getBounds().width != canvas.getSize().x
                            || image.getBounds().height != canvas.getSize().y) {
                        image = new Image(canvas.getDisplay(), canvas.getSize().x, canvas.getSize().y);

                        GC gc = new GC(image);
                        drawBackground(gc);
                        drawAreas(gc);
                        if (showGrid) {
                            drawGrid(gc);
                        }
                        drawAxis(gc);
                        drawFigures(gc);
                        drawCoordsUnits(gc);
                        gc.dispose();

                        canvas.setData(DOUBLE_BUFFER_IMAGE, image);
                    }

                    if (dragging) {
                        int deltaX = screenX - startDragX;
                        int deltaY = screenY - startDragy;
                        e.gc.drawImage(image, deltaX, deltaY);
                    } else {
                        e.gc.drawImage(image, 0, 0);
                        if (paintTool != null) {
                            paintTool.paintActive(e.gc);
                        }
                    }

                    if (zoomChangedListener != null) {
                        zoomChangedListener.zoomChanged(zoom);
                    }
                }
            });

            canvas.addMouseListener(new MouseListener() {
                @Override
                public void mouseUp(MouseEvent mouseEvent) {
                    shell.setCursor(new Cursor(canvas.getDisplay(), SWT.CURSOR_ARROW));
                    if (dragging) {
                        dragging = false;
                        canvas.setData(DOUBLE_BUFFER_IMAGE, null);
                        int deltaX = mouseEvent.x - startDragX;
                        int deltaY = startDragy - mouseEvent.y;
                        panEvent(deltaX, deltaY);
                        canvas.redraw();
                    }
                }

                @Override
                public void mouseDown(MouseEvent mouseEvent) {
                    canvas.setFocus();
                    if (mouseEvent.button == 1) {
                        int x = mouseEvent.x;
                        int y = mouseEvent.y;
                        double coordX = getCoordX(x);
                        double coordY = getCoordY(y);
                        if (rasterAreaPainter != null) {
                            rasterAreaPainter.showClick(coordX, coordY, mouseEvent.stateMask);
                        }
                        if (paintTool != null) {
                            if (snapToGrid && holdingShift) {
                                coordX = snapGridX(coordX);
                                coordY = snapGridY(coordY);
                            }
                            if (paintTool.click(coordX, coordY, mouseEvent.stateMask)) {
                                canvas.setData(DOUBLE_BUFFER_IMAGE, null);
                            }
                            canvas.redraw();
                        } else {
                            canvasManagerListener.onCanvasMouseDown(mouseEvent, CanvasManager.this, getCoordX(mouseEvent.x), getCoordY(mouseEvent.y));
                        }
                    } else if (mouseEvent.button == 3) {
                        startDragX = mouseEvent.x;
                        startDragy = mouseEvent.y;
                        dragging = true;
                        shell.setCursor(new Cursor(canvas.getDisplay(), SWT.CURSOR_SIZEALL));
                    }
                }

                @Override
                public void mouseDoubleClick(MouseEvent mouseEvent) {
                }
            });

            canvas.addKeyListener(new KeyListener() {

                @Override
                public void keyReleased(KeyEvent arg0) {
                }

                @Override
                public void keyPressed(KeyEvent event) {
                    if (paintTool != null && event.keyCode == SWT.ESC) {
                        paintTool.cancel();
                        refreshRedraw();
                    } else {
                        canvasManagerListener.onCanvasKeyPressed(event, CanvasManager.this);
                    }
                }
            });

            canvas.addMouseMoveListener(new MouseMoveListener() {
                @Override
                public void mouseMove(MouseEvent mouseEvent) {
                    screenX = mouseEvent.x;
                    screenY = mouseEvent.y;

                    double coordX = getCoordX(screenX);
                    double coordY = getCoordY(screenY);
                    if (coordsChangedListener != null) {
                        coordsChangedListener.coordsChanged(coordX, coordY, mouseEvent.stateMask);
                    }
                    if (rasterAreaPainter != null) {
                        rasterAreaPainter.coordsChanged(coordX, coordY, mouseEvent.stateMask);
                    }
                    if (dragging) {
                        canvas.redraw();
                    }
                    if (paintTool != null) {
                        paintTool.coordsChanged(coordX, coordY, mouseEvent.stateMask);
                        canvas.redraw();
                    }
                }
            });

            canvas.addMouseWheelListener(new MouseWheelListener() {
                @Override
                public void mouseScrolled(MouseEvent event) {
                    int wheelCount = event.count;
                    if (wheelCount > 0) {
                        setZoom(zooming.zoomIn(zoom));
                        canvas.setData(DOUBLE_BUFFER_IMAGE, null);
                        canvas.redraw();
                    }

                    if (wheelCount < 0) {
                        setZoom(zooming.zoomOut(zoom));
                        canvas.setData(DOUBLE_BUFFER_IMAGE, null);
                        canvas.redraw();
                    }
                }
            });

            colorGrey = new Color(canvas.getDisplay(), 160, 160, 160);
    }

    @Override
    public int getScreenX(double x) {
        return (int) (((x - posX) * zoom) + (screenWidth / 2));
    }

    @Override
    public int getScreenY(double y) {
        return (int) (screenHeight - (((y - posY) / (xyScale / zoom)) + (screenHeight / 2)));
    }

    @Override
    public double getCoordX(int screenX) {
        return posX + (screenX - (screenWidth / 2)) / zoom;
    }

    @Override
    public double getCoordY(int screenY) {
        return posY + (screenHeight - screenY - (screenHeight / 2)) * (xyScale / zoom);
    }

    @Override
    public int toPixelsX(double length) {
        return (int) (length * zoom);
    }

    @Override
    public int toPixelsY(double length) {
        return (int) (length / (xyScale / zoom));
    }

    public void panEvent(int deltaX, int deltaY) {
        posX = posX - (deltaX / zoom);
        posY = posY - (deltaY * xyScale / zoom);
    }

    public void zoomin(int x, int y) {
        posX = getCoordX(x);
        posY = getCoordY(y);
        zoom *= 2;
    }

    public void center(int x, int y) {
        posX = getCoordX(x);
        posY = getCoordY(y);
    }

    public void positionOnScreen(double x, double y, DefaultPosFrom defaultPosFrom, int onScreenX, int onScreenY) {
        if (defaultPosFrom == DefaultPosFrom.LEFT_BOTTOM) {
            posX = x;
            posY = y;
            panEvent(onScreenX - screenWidth / 2, onScreenY - screenHeight / 2);
            refreshRedraw();
        } else if (defaultPosFrom == DefaultPosFrom.CENTER) {
            posX = x;
            posY = y;
            panEvent(onScreenX, onScreenY);
            refreshRedraw();
        } else if (defaultPosFrom == DefaultPosFrom.LEFT_TOP) {
            posX = x;
            posY = y;
            panEvent(onScreenX - screenWidth / 2, onScreenY - screenHeight / 2 + screenHeight);
            refreshRedraw();
        } else if (defaultPosFrom == DefaultPosFrom.LEFT_MIDDLE) {
            posX = x;
            posY = y;
            panEvent(onScreenX - screenWidth / 2, onScreenY);
            refreshRedraw();
        } else {
            throw new RuntimeException("Not implemented " + defaultPosFrom);
        }
    }

    public void changeZoom(double zoomMultiply) {
        double zoom = this.zoom * zoomMultiply;
        if (zoom > 1E-13) {
            this.zoom = zoom;
        }
        if (zoomChangedListener != null) {
            zoomChangedListener.zoomChanged(zoom);
        }
        refreshRedraw();
    }

    public void setZoom(double zoom) {
        this.zoom = zoom != 0 ? zoom : 1;
    }

    public void changeXYScale(double xyScale) {
        this.xyScale = xyScale != 0 ? xyScale : 1;
        refreshRedraw();
    }

    public void setDisplayParameters(double posX, double posY, double zoom) {
        this.posX = posX;
        this.posY = posY;
        this.zoom = zoom;
    }

    public void zoomExtent(TwoPoints extent) {
        if (extent == null) {
            return;
        }
        if (!Double.isFinite(extent.x1) || !Double.isFinite(extent.x2) || !Double.isFinite(extent.y1) || !Double.isFinite(extent.y2)) {
            return;
        }
        // cut extent of too long coordinate
        double diffX = Math.abs(extent.x1 - extent.x2);
        if (extent.x1 == -Double.MAX_VALUE && extent.x2 == Double.MAX_VALUE || extent.x1 == Double.MAX_VALUE && extent.x2 == -Double.MAX_VALUE) {
            diffX = 0;
        }
        double diffY = Math.abs(extent.y1 - extent.y2) / xyScale;
        if (extent.y1 == -Double.MAX_VALUE && extent.y2 == Double.MAX_VALUE || extent.y1 == Double.MAX_VALUE && extent.y2 == -Double.MAX_VALUE) {
            diffY = 0;
        }
        if (diffY != 0 && diffX / diffY > 4) {
            extent.x2 = extent.x1 + diffY * 4;
        } else if (diffX != 0 && diffY / diffX > 4) {
            extent.y2 = extent.y1 + diffX * 4;
        }

        double locPosX = (extent.x1 + extent.x2) / 2f;
        double locPosY = (extent.y1 + extent.y2) / 2f;

        if (diffX == 0 || diffY == 0 || Double.isInfinite(locPosX) || Double.isInfinite(locPosY) || Double.isNaN(locPosX) || Double.isNaN(locPosY) || locPosX > 10e13 || locPosY > 10e13 || locPosX < 10e-13 || locPosY < 10e-13) {
            refreshRedraw();
            return;
        }
        posX = locPosX;
        posY = locPosY;

        double coordX0 = getCoordX(0);
        double coordY0 = getCoordY(screenHeight);
        double coordX1 = getCoordX(screenWidth);
        double coordY1 = getCoordY(0);
        if ((coordX0 > extent.x1 || coordY0 > extent.y1 ||
                coordX1 < extent.x2 || coordY1 < extent.y2)) {
            while (coordX0 > extent.x1 || coordY0 > extent.y1 ||
                    coordX1 < extent.x2 || coordY1 < extent.y2) {
                zoom /= 1.25d;
                coordX0 = getCoordX(0);
                coordY0 = getCoordY(screenHeight);
                coordX1 = getCoordX(screenWidth);
                coordY1 = getCoordY(0);
            }
            zoom /= 1.25f;
        } else {
            if (extent.x1 != extent.x2 || extent.y1 != extent.y2) {
                double origZoom = zoom;
                int maxIterations = 1000;
                while (--maxIterations > 0 && !(coordX0 > extent.x1 || coordY0 > extent.y1 ||
                        coordX1 < extent.x2 || coordY1 < extent.y2)) {
                    origZoom = zoom;
                    zoom *= 1.25f;
                    coordX0 = getCoordX(0);
                    coordY0 = getCoordY(screenHeight);
                    coordX1 = getCoordX(screenWidth);
                    coordY1 = getCoordY(0);
                }
                zoom = origZoom / 1.25f;
            }
        }
        refreshRedraw();
    }

    public double snapGridX(double click) {
        double floor = Math.floor(click / gridStepX) * gridStepX;
        if (click - floor < (floor + gridStepX - click)) {
            return (double) floor;
        } else {
            return (double) (floor + gridStepX);
        }
    }

    public double snapGridY(double click) {
        double floor = Math.floor(click / gridStepY) * gridStepY;
        if (click - floor < (floor + gridStepY - click)) {
            return (double) floor;
        } else {
            return (double) (floor + gridStepY);
        }
    }

    @Override
    public int getScreenHeight() {
        return screenHeight;
    }

    @Override
    public int getScreenWidth() {
        return screenWidth;
    }

    public void initScreenDimensions() {
        Rectangle clientArea = canvas.getClientArea();
        screenHeight = clientArea.height;
        screenWidth = clientArea.width;
    }

    private void drawBackground(GC gc) {
        gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
        gc.fillRectangle(new Rectangle(0, 0, screenWidth, screenHeight));
    }


    public void drawAreas(GC gc) {
        if (rasterAreaPainter != null) {
            rasterAreaPainter.paint(gc, this);
        }
        if (paintTool != null) {
            paintTool.paintAll(gc);
        }
    }

    public void drawAxis(GC gc) {
        gc.setLineWidth(3);
        gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_GRAY));

        int osaX = getScreenX(0);
        int osaY = getScreenY(0);
        if (osaX >= 0 && osaX < screenWidth) {
            gc.drawLine(osaX, 0, osaX, screenHeight);
        }
        if (osaY >= 0 && osaY < screenHeight) {
            gc.drawLine(0, osaY, screenWidth, osaY);
        }
        gc.setLineWidth(1);
        gc.setForeground(gc.getDevice().getSystemColor(mainAxisGray ? SWT.COLOR_DARK_GRAY : SWT.COLOR_BLACK));
        if (osaX >= 0 && osaX < screenWidth) {
            gc.drawLine(osaX, 0, osaX, screenHeight);
        }
        if (osaY >= 0 && osaY < screenHeight) {
            gc.drawLine(0, osaY, screenWidth, osaY);
        }

        drawAxisNumbers(gc);
    }

    public void drawAxisNumbers(GC gc) {
        gc.setLineWidth(1);
        gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_BLACK));

        double x0 = getCoordX(0);
        double xRange = getCoordX(BY_WHICH_PIXELS_AXIS_NUMBER);
        double delta = xRange - x0;
        double power = Math.floor(Math.log10(delta));
        double base = (double) Math.pow(10, power);
        double step = base * 2.5f;

        if (step < delta) {
            step = base * 5.0f;
        }
        if (step < delta) {
            step = base * 10.0f;
        }
        if (step < delta) {
            step = base * 20.0f;
        }

        int osaX = getScreenX(0);
        int osaY = getScreenY(0);
        double coordY0 = getCoordY(screenHeight);
        double coordY1 = getCoordY(0);

        double coordX0 = getCoordX(0);
        double coordX1 = getCoordX(screenWidth);
        int ax = (int) Math.floor(coordX0 / step) + 1;
        int i = 0;
        double gx = ((ax + i) * step);
        while (gx < coordX1) {
            int screenX = getScreenX(gx);

            if (gx != 0) {
                gc.drawLine(screenX, osaY - 5, screenX, osaY + 5);
                String numberString = roundAxisNumber(gx);
                int textWidth = gc.stringExtent(numberString).x;
                gc.drawText(numberString, screenX - (textWidth / 2), osaY + SHIFT_TEXT_BY_PIXELS, SWT.DRAW_TRANSPARENT);
            }

            gx = ((ax + (++i)) * step);
        }

        int ay = (int) Math.floor(coordY0 / (step * xyScale)) + 1;
        i = 0;
        double gy = ((ay + i) * step * xyScale);
        while (gy < coordY1) {
            int screenY = getScreenY(gy);

            if (gy != 0) {
                gc.drawLine(osaX - 5, screenY, osaX + 5, screenY);
                String numberString = roundAxisYNumber(gy);
                int textWidth = gc.stringExtent(numberString).x;
                int textHeight = gc.stringExtent(numberString).y;
                gc.drawText(numberString, osaX - textWidth - 3 - SHIFT_TEXT_BY_PIXELS, screenY - (textHeight / 2), SWT.DRAW_TRANSPARENT);
            }

            gy = ((ay + (++i)) * step * xyScale);
        }

        gc.drawOval(osaX - 3, osaY - 3, 6, 6);
        gc.drawText("0", osaX + 3, osaY + SHIFT_TEXT_BY_PIXELS, SWT.DRAW_TRANSPARENT);
    }

    public void drawCoordsUnits(GC gc) {
        gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_BLACK));
        int alpha = gc.getAlpha();
        gc.setAlpha(150);
        if (unitsCoordY != null) {
            gc.drawText(unitsCoordY, 2, 0, true);
            Point textExtentY = gc.textExtent(unitsCoordY);
            gc.drawText("\u2191", (int) (textExtentY.x / 2.0) - 3, textExtentY.y, true);
        }
        if (unitsCoordX != null) {
            Point textExtentX = gc.textExtent("\u2191 " + unitsCoordX);
            gc.drawText("\u2192 " + unitsCoordX, screenWidth - textExtentX.x - 2, screenHeight - textExtentX.y - 1, true);
        }
        gc.setAlpha(alpha);
    }

    public String roundAxisNumber(double f) {
        if (zoom < 1E-4 || zoom > 1100000) {
            return "";
        }

        String format = FORMAT_DEC_3.format(f);
        if (format.endsWith(".000")) {
            format = format.substring(0, format.length() - 4);
        }
        return format;
    }

    public String roundAxisYNumber(double f) {
        if (xyScale / zoom < 1E-4 || xyScale / zoom > 1100000) {
            return "";
        }

        String format = FORMAT_DEC_3.format(f);
        if (format.endsWith(".000")) {
            format = format.substring(0, format.length() - 4);
        }
        return format;
    }


    public void drawGrid(GC gc) {
        int origAlpha = gc.getAlpha();
        gc.setAlpha(150);
        gc.setForeground(colorGrey);

        double coordX0 = getCoordX(0);
        double coordX1 = getCoordX(screenWidth);
        double coordY0 = getCoordY(screenHeight);
        double coordY1 = getCoordY(0);

        // ==== Vertical lines ====
        double ax = (double) Math.floor(coordX0 / gridStepX) + 1;
        double gx = ax * gridStepX;
        int pixelsX = toPixelsX(gridStepX);
        if (pixelsX > GRID_MIN_GRID_LENGTH) {
            while (gx < coordX1) {
                int screenX = getScreenX(gx);
                gc.drawLine(screenX, 0, screenX, screenHeight);
                ax += 1;
                gx = ax * gridStepX;
            }
        }

        // ---- thick
        int width = 2;//pixelsX > GRID_MIN_GRID_LENGTH && thickGrid > gridStep ? GRID_THICK_WIDTH : GRID_WIDTH;
        ax = (double) Math.floor(coordX0 / (gridStepX * 10)) + 1;
        gx = ax * gridStepX * 10;
        int thickPixelsX = toPixelsX(gridStepX * 10);
        if (thickPixelsX > GRID_MIN_THICK_LENGTH && gridStepX * 10 != gridStepX) {
            gc.setLineWidth(width);
            while (gx < coordX1) {
                int screenX = getScreenX(gx);
                gc.drawLine(screenX, 0, screenX, screenHeight);
                ax += 1;
                gx = ax * gridStepX * 10;
            }
            gc.setLineWidth(GRID_WIDTH);
        }

        // ==== Horizontal lines ====
        double ay = (double) Math.floor(coordY0 / gridStepY) + 1;
        double gy = ay * gridStepY;
        int pixelsY = toPixelsY(gridStepY);
        if (pixelsY > GRID_MIN_GRID_LENGTH) {
            while (gy < coordY1) {
                int screenY = getScreenY(gy);
                gc.drawLine(0, screenY, screenWidth, screenY);
                ay += 1;
                gy = ay * gridStepY;
            }
        }

        // ---- thick
        ay = (double) Math.floor(coordY0 / (gridStepY * 10)) + 1;
        gy = ay * gridStepY * 10;
        int thickPixelsY = toPixelsY(gridStepY * 10);
        if (thickPixelsY > GRID_MIN_THICK_LENGTH) {
            gc.setLineWidth(width);
            while (gy < coordY1 && gridStepY * 10 != gridStepY) {
                int screenY = getScreenY(gy);
                gc.drawLine(0, screenY, screenWidth, screenY);
                ay += 1;
                gy = ay * gridStepY * 10;
            }
            gc.setLineWidth(GRID_WIDTH);
        }
        gc.setAlpha(origAlpha);
    }

    public void drawFigures(GC gc) {
        canvasManagerListener.onCanvasPaint(gc, this);
    }

    public void refreshRedraw() {
        canvas.setData(DOUBLE_BUFFER_IMAGE, null);
        canvas.redraw();
    }

    public void dispose() {
        colorGrey.dispose();
    }

    public void setPaintTool(PaintTool paintTool) {
        if (this.paintTool != null) {
            this.paintTool.cancel();
        }
        this.paintTool = paintTool;
        if (this.paintTool != null) {
            this.paintTool.setCoordState(this);
        }
        refreshRedraw();
    }

    public void setGridStep(double gridStep) {
        this.gridStepX = gridStep != 0 ? gridStep : 1;
        this.gridStepY = gridStep != 0 ? gridStep : 1;
        refreshRedraw();
    }

    public void setGridStepXY(double gridStepX, double gridStepY) {
        this.gridStepX = gridStepX != 0 ? gridStepX : 1;
        this.gridStepY = gridStepY != 0 ? gridStepY : 1;
        refreshRedraw();
    }

    public void setGridStepX(double gridStep) {
        this.gridStepX = gridStep != 0 ? gridStep : 1;
        refreshRedraw();
    }

    public void setGridStepY(double gridStep) {
        this.gridStepY = gridStep != 0 ? gridStep : 1;
        refreshRedraw();
    }

    public void zoomIn() {
        setZoom(zooming.zoomIn(zoom));
    }

    public void zoomOut() {
        setZoom(zooming.zoomOut(zoom));
    }

    public void setRasterAreaPainter(RasterAreaPainter rasterAreaPainter) {
        this.rasterAreaPainter = rasterAreaPainter;
    }

    public void setShowGrid(boolean showGrid) {
        this.showGrid  = showGrid;
    }

    public RasterAreaPainter getRasterAreaPainter() {
        return this.rasterAreaPainter;
    }

    public double getGridStepX() {
        return gridStepX;
    }

    public double getGridStepY() {
        return gridStepX;
    }

    public void setCoordsChangedListener(CoordsChangedListener coordsChangedListener) {
        this.coordsChangedListener = coordsChangedListener;
    }

    public void setZoomChangedListener(ZoomChangedListener zoomChangedListener) {
        this.zoomChangedListener = zoomChangedListener;
    }

    public void setUnitsCoordY(String textCoordY) {
        this.unitsCoordY = textCoordY;
    }

    public void setUnitsCoordX(String textCoordX) {
        this.unitsCoordX = textCoordX;
    }

    public void setMainAxisGray(boolean mainAxisGray) {
        this.mainAxisGray = mainAxisGray;
        refreshRedraw();
    }

    public void setZooming(Zooming zooming) {
        this.zooming = zooming;
    }

    public void setSnapToGrid(boolean snapToGrid) {
        this.snapToGrid = snapToGrid;
    }
}
