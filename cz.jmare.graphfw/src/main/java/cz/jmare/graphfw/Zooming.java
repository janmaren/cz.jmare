package cz.jmare.graphfw;

public interface Zooming {
    double zoomIn(double origZoom);
    double zoomOut(double origZoom);
}
