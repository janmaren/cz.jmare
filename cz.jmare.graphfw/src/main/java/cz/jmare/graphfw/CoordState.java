package cz.jmare.graphfw;

public interface CoordState {
    double getCoordX(int screenX);

    double getCoordY(int screenY);

    int getScreenX(double x);

    int getScreenY(double y);

    public int getScreenHeight();

    public int getScreenWidth();

    /**
     * Return length in pixels on screen. Can be used for X axe. It musn't be used for Y axe unless xyScale=1
     * @param length
     * @return
     */
    int toPixelsX(double length);

    /**
     * Return length in pixels on screen. Can be used for Y axe when xyScale<>1
     * @param length
     * @return
     */
    int toPixelsY(double length);
}
