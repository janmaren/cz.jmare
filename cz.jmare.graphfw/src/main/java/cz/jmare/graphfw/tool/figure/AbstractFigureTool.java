package cz.jmare.graphfw.tool.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.CoordsChangedListener;
import cz.jmare.graphfw.tool.PaintTool;

public abstract class AbstractFigureTool<E> implements PaintTool {
    protected CoordState coordState;

    protected double lastCursorX;

    protected double lastCursorY;

    protected E collector;

    protected CoordsChangedListener coordsChangedListener;

    public AbstractFigureTool(E figureCollector) {
        super();
        this.collector = figureCollector;
    }

    public AbstractFigureTool() {
        super();
    }

    @Override
    public void coordsChanged(double x, double y, int stateMask) {
        lastCursorX = x;
        lastCursorY = y;
        if (coordsChangedListener != null) {
            coordsChangedListener.coordsChanged(x, y, stateMask);
        }
    }

    @Override
    public void setCoordState(CoordState coordState) {
        this.coordState = coordState;
    }

    public void setCoordsChangedListener(CoordsChangedListener coordsChangedListener) {
        this.coordsChangedListener = coordsChangedListener;
    }

    /**
     * This is mainly for active parts of curve and {@link #paintAll(GC)} needn't be used
     * so let's implement it empty
     */
    @Override
    public void paintAll(GC gc) {
    }
}
