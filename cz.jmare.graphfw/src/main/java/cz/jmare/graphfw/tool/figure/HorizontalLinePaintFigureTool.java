package cz.jmare.graphfw.tool.figure;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import cz.jmare.graphfw.figure.FirstClickFigure;
import cz.jmare.graphfw.figure.HorizontalLineFigure;
import cz.jmare.graphfw.figurecollector.AddCollector;

public class HorizontalLinePaintFigureTool extends AbstractFigureTool<AddCollector> {
    protected double startCoordX;

    protected double startCoordY;

    private boolean firstPointAdded;

    private Color lineColor = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);

    private String label;

    private int labelPosition;

    private int lineWidth = 1;

    private int alpha = 255;

    public HorizontalLinePaintFigureTool(AddCollector addCollector) {
        super(addCollector);
    }

    protected void addFigure(double endCoordX, double endCoordY) {
        HorizontalLineFigure figure = new HorizontalLineFigure(endCoordY);
        figure.setLabel(label);
        figure.setLabelPosition(labelPosition);
        figure.setAlpha(alpha);
        figure.setCategory(label);
        figure.setLineWidth(lineWidth);
        collector.addFigure(figure);
    }

    @Override
    public boolean click(double x, double y, int stateMask) {
        if (firstPointAdded) {
            addFigure(x, y);
            firstPointAdded = false;
        } else {
            startCoordX = x;
            startCoordY = y;
            firstPointAdded = true;
            collector.addFigure(new FirstClickFigure());
        }
        return !firstPointAdded;
    }

    @Override
    public void paintActive(GC gc) {
        if (!firstPointAdded) {
            return;
        }
        gc.setAlpha(alpha);
        gc.setLineWidth(lineWidth);
        int osaY = coordState.getScreenY(lastCursorY);
        gc.setForeground(lineColor);
        gc.drawLine(0, osaY, coordState.getScreenWidth(), osaY);
        gc.setAlpha(255);
        if (label != null) {
            if (labelPosition == SWT.TOP) {
                int textHeight = gc.stringExtent(label).y;
                gc.drawText(label, 0, osaY - textHeight, true);
            } else {
                gc.drawText(label, 0, osaY + 2, true);
            }
        }
    }

    @Override
    public void cancel() {
        firstPointAdded = false;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * {@link SWT#TOP} or {@link SWT#BOTTOM}
     * @param labelPosition
     */
    public void setLabelPosition(int labelPosition) {
        this.labelPosition = labelPosition;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }
}
