package cz.jmare.graphfw.tool.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.figure.PointFigure;
import cz.jmare.graphfw.figurecollector.AddCollector;

public class PointPaintFigureTool extends AbstractFigureTool<AddCollector> {

    double x;

    double y;

    public PointPaintFigureTool(AddCollector addCollector) {
        super(addCollector);
    }

    @Override
    public boolean click(double x, double y, int stateMask) {
        collector.addFigure(new PointFigure(x, y));
        return true;
    }

    @Override
    public void paintActive(GC gc) {
        int lastCursorScreenX = coordState.getScreenX(lastCursorX);
        if (lastCursorScreenX >= 0) {
            int lastCursorScreenY = coordState.getScreenY(lastCursorY);
            gc.setLineWidth(1);
            gc.drawOval(lastCursorScreenX - 3, lastCursorScreenY - 3, 6, 6);
        }
    }

    @Override
    public void cancel() {
    }

}
