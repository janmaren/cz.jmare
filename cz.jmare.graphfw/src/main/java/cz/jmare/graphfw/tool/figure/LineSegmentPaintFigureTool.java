package cz.jmare.graphfw.tool.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.figure.LineSegmentFigure;
import cz.jmare.graphfw.figurecollector.AddCollector;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;

public class LineSegmentPaintFigureTool extends AbstractFigureTool<AddCollector> {
    protected double startCoordX;

    protected double startCoordY;

    private boolean firstPointAdded;

    public LineSegmentPaintFigureTool(AddCollector addCollector) {
        super(addCollector);
    }

    protected void addFigure(double endCoordX, double endCoordY) {
        collector.addFigure(new LineSegmentFigure(startCoordX, startCoordY, endCoordX, endCoordY));
    }

    @Override
    public boolean click(double x, double y, int stateMask) {
        if (firstPointAdded) {
            addFigure(x, y);
            firstPointAdded = false;
        } else {
            startCoordX = x;
            startCoordY = y;
            firstPointAdded = true;
        }
        return !firstPointAdded;
    }

    @Override
    public void paintActive(GC gc) {
        if (!firstPointAdded) {
            int lastCursorScreenX = coordState.getScreenX(lastCursorX);
            if (lastCursorScreenX >= 0) {
                int lastCursorScreenY = coordState.getScreenY(lastCursorY);
                gc.drawRectangle(lastCursorScreenX - 2, lastCursorScreenY - 2, 4, 4);
            }
            return;
        }
        int screenX1 = coordState.getScreenX(startCoordX);
        int screenY1 = coordState.getScreenY(startCoordY);
        int screenX2 = coordState.getScreenX(lastCursorX);
        int screenY2 = coordState.getScreenY(lastCursorY);

        double coordCurrentX = coordState.getCoordX(screenX2);
        double coordCurrentY = coordState.getCoordY(screenY2);

        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);

        LineSegmentDouble lineSegmentCrop = CropWindowDoubleFunctional.crop(new LineSegmentDouble(startCoordX, startCoordY, coordCurrentX, coordCurrentY), canvasX0, canvasY0, canvasX1, canvasY1);
        if (lineSegmentCrop != null) {
            screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
            screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
            screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
            screenY2 = coordState.getScreenY(lineSegmentCrop.y2);
        }
        gc.drawRectangle(screenX1 - 2, screenY1 - 2, 4, 4);
        gc.drawRectangle(screenX2 - 2, screenY2 - 2, 4, 4);
        gc.drawLine(screenX1, screenY1, screenX2, screenY2);
    }

    @Override
    public void cancel() {
        firstPointAdded = false;
    }
}
