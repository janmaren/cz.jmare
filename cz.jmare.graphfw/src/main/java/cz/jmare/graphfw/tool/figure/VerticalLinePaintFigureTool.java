package cz.jmare.graphfw.tool.figure;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;

import cz.jmare.graphfw.figure.FirstClickFigure;
import cz.jmare.graphfw.figure.VerticalLineFigure;
import cz.jmare.graphfw.figurecollector.AddCollector;

public class VerticalLinePaintFigureTool extends AbstractFigureTool<AddCollector> {
    protected double startCoordX;

    protected double startCoordY;

    private boolean firstPointAdded;

    private Color lineColor = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);

    private String label;

    private int labelPosition;

    private int lineWidth = 1;

    private int alpha = 128;

    public VerticalLinePaintFigureTool(AddCollector addCollector) {
        super(addCollector);
    }

    protected void addFigure(double endCoordX, double endCoordY) {
        VerticalLineFigure figure = new VerticalLineFigure(endCoordX);
        figure.setLabel(label);
        figure.setLabelPosition(labelPosition);
        figure.setCategory(label);
        figure.setLineWidth(lineWidth);
        figure.setAlpha(alpha);
        figure.setColor(lineColor);
        collector.addFigure(figure);
    }

    @Override
    public boolean click(double x, double y, int stateMask) {
        if (firstPointAdded) {
            addFigure(x, y);
            firstPointAdded = false;
        } else {
            startCoordX = x;
            startCoordY = y;
            firstPointAdded = true;
            collector.addFigure(new FirstClickFigure());
        }
        return !firstPointAdded;
    }

    @Override
    public void paintActive(GC gc) {
        if (!firstPointAdded) {
            return;
        }
        gc.setAlpha(alpha);
        gc.setLineWidth(lineWidth);
        int osaX = coordState.getScreenX(lastCursorX);
        gc.setForeground(lineColor);
        gc.drawLine(osaX, 0, osaX, coordState.getScreenHeight());
        gc.setAlpha(255);
        if (label != null) {
            Point stringExtent = gc.stringExtent(label);
            if (labelPosition == SWT.LEFT) {
                gc.drawText(label, osaX - stringExtent.x, coordState.getScreenHeight() - stringExtent.y, true);
            } else {
                gc.drawText(label, osaX + 3, coordState.getScreenHeight() - stringExtent.y, true);
            }
        }
    }

    @Override
    public void cancel() {
        firstPointAdded = false;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * {@link SWT#LEFT} or {@link SWT#RIGHT}
     * @param labelPosition
     */
    public void setLabelPosition(int labelPosition) {
        this.labelPosition = labelPosition;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }
}
