package cz.jmare.graphfw.tool.figure;

import cz.jmare.graphfw.figure.StraightLineFigure;
import cz.jmare.graphfw.figurecollector.AddCollector;

public class StraightLinePaintFigureTool extends LineSegmentPaintFigureTool {

    public StraightLinePaintFigureTool(AddCollector addCollector) {
        super(addCollector);
    }

    @Override
    protected void addFigure(double endCoordX, double endCoordY) {
        collector.addFigure(new StraightLineFigure(startCoordX, startCoordY, endCoordX, endCoordY));
    }
}
