package cz.jmare.graphfw.tool;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;

public interface PaintTool {
    /**
     * Return true when figure is complete
     * @param x
     * @param y
     * @param stateMask use SWT constants like {@link SWT#SHIFT}, {@link SWT#CONTROL}, {@link SWT#ALT} or {@link SWT#ALT_GR}
     * to detect other keys use during clicking
     * @return
     */
    boolean click(double x, double y, int stateMask);

    /**
     * Current position after cursor has moved
     * @param x
     * @param y
     * @param stateMask
     */
    void coordsChanged(double x, double y, int stateMask);

    /**
     * Paint curve or active part of curve to be positioned. It shouldn't draw prepending part of curve drawn by {@link #paintAll(GC)}
     * @param gc
     */
    void paintActive(GC gc);

    /**
     * Paint all defined points but it needn't contain part to be drawn by {@link #paintActive(GC)}
     * @param gc
     */
    void paintAll(GC gc);

    /**
     * Cancel or stop or finish edit
     */
    void cancel();

    /**
     * CanvasManager uses this metod to pass {@link CoordState} which contains data like pan and zoom
     * needed to paint curves correctly positioned
     * @param coordState
     */
    void setCoordState(CoordState coordState);
}
