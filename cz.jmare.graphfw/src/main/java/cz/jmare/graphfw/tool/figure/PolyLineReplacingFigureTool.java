package cz.jmare.graphfw.tool.figure;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.figure.CurveFigure;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.ReplaceCollector;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;

public class PolyLineReplacingFigureTool extends AbstractFigureTool<ReplaceCollector> {
    private double firstX, firstY;
    private boolean moveHorizontalMode;
    private boolean moveVerticalMode;
    private boolean changeAmplitudeMode;
    private CurveFigure originFigure;
    private boolean equiDistant;

    public PolyLineReplacingFigureTool(ReplaceCollector replaceCollector) {
        super(replaceCollector);
    }

    @Override
    public boolean click(double x, double y, int stateMask) {
        ReplaceCollector replaceCollector = collector;
        if (originFigure == null) {
            if (replaceCollector.select(coordState, x, y, false)) {
                List<Figure> figures = new ArrayList<>(replaceCollector.getSelectedFigures());
                if (figures.size() == 1 && figures.get(0) instanceof CurveFigure) {
                    firstX = x;
                    firstY = y;
                    originFigure = (CurveFigure) figures.get(0);
                    replaceCollector.beginEditFigure(originFigure);
                    // replaceCollector.unselectAll();
                    return true;
                }
                // replaceCollector.unselectAll();
            }
            return false;
        } else {
            onChange(originFigure.getCategory(), moveHorizontalMode ? x - firstX : 0, moveVerticalMode ? y - firstY : 0, changeAmplitudeMode ? y / firstY : 1.0);
            originFigure = null;
//              SequentialCurve movePoints = ChangeCurveUtil.movePoints(originFigure.getPoints(), moveHorizontalMode ? x - firstX : 0, moveVerticalMode ? y - firstY : 0);
//              collector.commitEditFigure(originFigure.copy(movePoints));
//              originFigure = null;
            return true;
        }
    }

    /**
     * To override and accept new delta values to recalculate
     * @param id
     * @param deltaX
     * @param deltaY
     * @param deltaAmpY
     */
    public void onChange(String id, double deltaX, double deltaY, double deltaAmpY) {

    }

    private double deltaX() {
        if (moveHorizontalMode) {
            return lastCursorX - firstX;
        }
        return 0;
    }

    private double deltaY() {
        if (moveVerticalMode) {
            return lastCursorY - firstY;
        }
        return 0;
    }

    private double deltaAmpY() {
        if (changeAmplitudeMode) {
            return lastCursorY / firstY;
        }
        return 1;
    }

    @Override
    public void paintActive(GC gc) {
        if (originFigure != null) {

            double canvasX0 = coordState.getCoordX(0);
            double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
            double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
            double canvasY1 = coordState.getCoordY(0);

            RandomAccessCurve points = originFigure.getPoints();
            Point lastPoi = points.get(0);
            double deltaX = deltaX();
            double deltaY = deltaY();
            double deltaAmpY = deltaAmpY();
            Point lastPoint = new Point(lastPoi.x + deltaX, lastPoi.y * deltaAmpY + deltaY);

            int lastScreenX1 = -1;
            int lastScreenY1 = -1;
            int lastScreenX2 = -1;
            int lastScreenY2 = -1;

            int inc = 1;
            if (equiDistant) {
                double pixelDistance = coordState.toPixelsX((points.get(1).x - points.get(0).x) * 10000) / 10000.0;
                if (pixelDistance != 0) {
                    inc = (int) (1.0 / pixelDistance) / 2;
                    if (inc == 0) {
                        inc = 1;
                    }
                } else {
                    inc = 10000;
                }
            }

            for (int i = 1; i < points.size(); i += inc) {
                Point poi = points.get(i);
                Point point = new Point(poi.x + deltaX, poi.y * deltaAmpY + deltaY);
                LineSegmentDouble lineSegmentCrop = CropWindowDoubleFunctional.crop(new LineSegmentDouble(lastPoint.x, lastPoint.y, point.x, point.y), canvasX0, canvasY0, canvasX1, canvasY1);
                if (lineSegmentCrop != null) {

                    int screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
                    int screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
                    int screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
                    int screenY2 = coordState.getScreenY(lineSegmentCrop.y2);

                    if (!(lastScreenX1 == screenX1 && lastScreenX2 == screenX2 && lastScreenY1 == screenY1 && lastScreenY2 == screenY2)) {
                        gc.drawLine(screenX1, screenY1, screenX2, screenY2);
                        lastScreenX1 = screenX1;
                        lastScreenY1 = screenY1;
                        lastScreenX2 = screenX2;
                        lastScreenY2 = screenY2;
                    }
                }
                lastPoint = point;
            }
        }
    }

    @Override
    public void cancel() {
        collector.rollbackEditFigure();
        originFigure = null;
    }

    public void setMoveHorizontalMode(boolean moveHorizontalMode) {
        this.moveHorizontalMode = moveHorizontalMode;
    }

    public void setMoveVerticalMode(boolean moveVerticalMode) {
        this.moveVerticalMode = moveVerticalMode;
    }

    public void setEquiDistant(boolean equiDistant) {
        this.equiDistant = equiDistant;
    }

    public void setChangeAmplitudeMode(boolean changeAmplitudeMode) {
        this.changeAmplitudeMode = changeAmplitudeMode;
    }
}
