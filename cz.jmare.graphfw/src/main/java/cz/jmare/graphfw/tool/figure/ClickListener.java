package cz.jmare.graphfw.tool.figure;

public interface ClickListener {
    void onClick(double x, double y, int stateMask);
}
