package cz.jmare.graphfw.graphcomposite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import cz.jmare.graphfw.figurecollector.AddCollector;
import cz.jmare.graphfw.figurecollector.AddEventConsumer;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.FigureEventConsumer;
import cz.jmare.graphfw.figurecollector.SimpleFigureCollector;
import cz.jmare.graphfw.tool.figure.LineSegmentPaintFigureTool;
import cz.jmare.graphfw.tool.figure.PointPaintFigureTool;
import cz.jmare.graphfw.tool.figure.StraightLinePaintFigureTool;

public class GraphBasicToolsFiguresComposite extends GraphEditComposite {

    private Button buttonAddPoint;

    private Button buttonAddLineSegment;

    private Button buttonAddStraightLine;

    private Text categoryText;

    public GraphBasicToolsFiguresComposite(Composite c, int style, SimpleFigureCollector figureCollector, Integer... graphSettings) {
        super(c, style, figureCollector, graphSettings);
        init();
    }

    private void init() {
        if (settingList.contains(GraphConfigConstant.SHOW_CATEGORY_INPUT)) {
            if (figureCollector instanceof AddEventConsumer) {
                AddEventConsumer addEventConsumer = (AddEventConsumer) figureCollector;
                addEventConsumer.addFigureEventConsumer(new FigureEventConsumer() {
                    @Override
                    public void onUpdatedFigure(Figure originFigure, Figure figure) {
                    }

                    @Override
                    public void onDeletedFigure(Figure figure) {
                    }

                    @Override
                    public void onAddedFigure(Figure figure) {
                        if (figure instanceof Category) {
                            Category category = (Category) figure;
                            category.setCategory(getCategory());
                        }
                    }
                });
            }
        }
    }

    @Override
    public void toolButtonSelected(Button selectedButton) {
        super.toolButtonSelected(selectedButton);
        if (buttonAddPoint != null && selectedButton != buttonAddPoint) buttonAddPoint.setSelection(false);
        if (buttonAddLineSegment != null && selectedButton != buttonAddLineSegment) buttonAddLineSegment.setSelection(false);
        if (buttonAddStraightLine != null && selectedButton != buttonAddStraightLine) buttonAddStraightLine.setSelection(false);
    }

    @Override
    protected void createToolButtons(Composite toolbarComposite) {
        super.createToolButtons(toolbarComposite);
        if (settingList.contains(GraphConfigConstant.SHOW_ADD_POINT_BUTTON)) {
            buttonAddPoint = new Button(toolbarComposite, SWT.TOGGLE);
            buttonAddPoint.setToolTipText("Add Point");
            buttonAddPoint.setImage(getImageStore().registerImage("/images/point-" + getIconSize() + ".png"));
            buttonAddPoint.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.setPaintTool(new PointPaintFigureTool((AddCollector) figureCollector));
                    toolButtonSelected(buttonAddPoint);
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (settingList.contains(GraphConfigConstant.SHOW_ADD_LINESEGMENT_BUTTON)) {
            buttonAddLineSegment = new Button(toolbarComposite, SWT.TOGGLE);
            buttonAddLineSegment.setToolTipText("Add Line Segment");
            buttonAddLineSegment.setImage(getImageStore().registerImage("/images/linesegment-" + getIconSize() + ".png"));
            buttonAddLineSegment.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.setPaintTool(new LineSegmentPaintFigureTool((AddCollector) figureCollector));
                    toolButtonSelected(buttonAddLineSegment);
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (settingList.contains(GraphConfigConstant.SHOW_ADD_STRAIGHTLINE_BUTTON)) {
            buttonAddStraightLine = new Button(toolbarComposite, SWT.TOGGLE);
            buttonAddStraightLine.setToolTipText("Add Straight Line");
            buttonAddStraightLine.setImage(getImageStore().registerImage("/images/line-" + getIconSize() + ".png"));
            buttonAddStraightLine.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.setPaintTool(new StraightLinePaintFigureTool((AddCollector) figureCollector));
                    toolButtonSelected(buttonAddStraightLine);
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (settingList.contains(GraphConfigConstant.SHOW_CATEGORY_INPUT)) {
            createCategoryInput(toolbarComposite);
        }
    }

    protected void createCategoryInput(Composite toolbarComposite) {
        RowData rowData = new RowData();
        rowData.width = 25;
        new Label(toolbarComposite, SWT.NONE).setText("Category:");
        categoryText = new Text(toolbarComposite, SWT.BORDER);
        categoryText.setToolTipText("Category");
        categoryText.setLayoutData(rowData);
        RowData layoutData = new RowData();
        layoutData.width = 60;
        categoryText.setLayoutData(layoutData);
    }

    public String getCategory() {
        if (categoryText == null) {
            return null;
        }
        String text = categoryText.getText().trim();
        if (text.length() > 0) {
            return text;
        }
        return null;
    }
}
