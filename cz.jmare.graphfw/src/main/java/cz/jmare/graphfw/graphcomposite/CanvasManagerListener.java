package cz.jmare.graphfw.graphcomposite;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;

public interface CanvasManagerListener {
    void onCanvasPaint(GC gc, CoordState coordState);
    void onCanvasKeyPressed(KeyEvent keyEvent, CoordState coordState);
    void onCanvasMouseDown(MouseEvent mouseEvent, CoordState coordState, double x, double y);
}
