package cz.jmare.graphfw.graphcomposite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import cz.jmare.graphfw.CanvasManager;
import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.CoordsChangedListener;
import cz.jmare.graphfw.CoordsChangedListenerMulti;
import cz.jmare.graphfw.ToolChangedListener;
import cz.jmare.graphfw.ZoomChangedListener;
import cz.jmare.graphfw.ZoomChangedListenerMulti;
import cz.jmare.graphfw.Zooming;
import cz.jmare.graphfw.figurecollector.SimpleFigureCollector;
import cz.jmare.graphfw.rastercollector.RasterAreaPainter;
import cz.jmare.graphfw.tool.PaintTool;
import cz.jmare.swt.image.ImageStore;
import cz.jmare.swt.util.STextInput;

public class GraphReadComposite extends Composite implements CanvasManagerListener, Redrawable {
    private Text gridSizeText;

    private String iconSize = "22";

    protected CanvasManager canvasManager;

    private ImageStore imageStore = new ImageStore();

    protected Button buttonCursor;

    private ZoomChangedListenerMulti zoomChangedListenerMulti;

    private CoordsChangedListenerMulti coordsChangedListenerMulti;

    private Label axeX;

    private Label axeY;

    private Label zoomLabel;

    private Combo xyScaleCombo;

    protected SimpleFigureCollector figureCollector;

    protected List<Integer> settingList;

    protected Boolean multiSelection;

    protected List<ToolChangedListener> toolChangedListeners = new ArrayList<ToolChangedListener>();

    private Button buttonMainAxisGray;

    private DefaultPosFrom defaultPosFrom = DefaultPosFrom.CENTER;

    private double coordToDefaultX = 0;

    private double coordToDefaultY = 0;

    private int onScreenPosX = 0;

    private int onScreenPosY = 0;

    @SuppressWarnings("deprecation")
	public GraphReadComposite(Composite c, int style, SimpleFigureCollector figureCollector, Integer... graphSettings) {
        super(c, style);
        this.figureCollector = figureCollector;
        settingList = Arrays.asList(graphSettings);
        boolean smallIcons = settingList.contains(GraphConfigConstant.SHOW_SMALL_ICONS);
        if (smallIcons) {
            this.iconSize = "16";
        }

        GridLayout gl = new GridLayout();
        gl.numColumns = 1;
        this.setLayout(gl);

        createToolBar();

        createCanvas(this, figureCollector);
        canvasManager.setShowGrid(settingList.contains(GraphConfigConstant.SHOW_GRID));
        if (settingList.contains(GraphConfigConstant.SHOW_GRID_INPUT)) {
            double gridStepX = canvasManager.getGridStepX();
            double gridStepY = canvasManager.getGridStepY();
            String str = String.valueOf(gridStepX) + ", " + String.valueOf(gridStepY);
            if (gridStepX == gridStepY) {
                str = String.valueOf(gridStepX);
            }
            gridSizeText.setText(str);
        }

        createStatusBar();

        zoomChangedListenerMulti = new ZoomChangedListenerMulti(new ZoomChangedListener() {
            @Override
            public void zoomChanged(double zoom) {
                if (zoom < 1.0) {
                    zoomLabel.setText(String.format(Locale.US, "zoom: %.11f", zoom));
                } else {
                    zoomLabel.setText(String.format(Locale.US, "zoom: %.1f", zoom));
                }
            }});
        canvasManager.setZoomChangedListener(zoomChangedListenerMulti);

        coordsChangedListenerMulti = new CoordsChangedListenerMulti(new CoordsChangedListener() {
            @Override
            public void coordsChanged(double x, double y, int stateMask) {
                axeX.setText("x: " + String.format(Locale.US, "%.13f", x));
                axeY.setText("y: " + String.format(Locale.US, "%.13f", y));
            }
        });
        canvasManager.setCoordsChangedListener(coordsChangedListenerMulti);
    }

    protected void createCanvas(Composite parent, SimpleFigureCollector vectorProvider) {
        Canvas canvas = new Canvas(parent, SWT.NO_BACKGROUND | SWT.BORDER);

        canvasManager = new CanvasManager(getShell(), canvas, this);

        GridData data = new GridData ();
        data.horizontalAlignment = GridData.FILL;
        data.verticalAlignment = GridData.FILL;
        data.grabExcessHorizontalSpace = true;
        data.grabExcessVerticalSpace = true;
        canvas.setLayoutData(data);
    }

    protected void createStatusBar() {
        Composite statusBarComposite = new Composite(this, SWT.NONE);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 3;
        statusBarComposite.setLayout(gridLayout);
        GridData gridDataStatus = new GridData();
        gridDataStatus.grabExcessHorizontalSpace = true;
        gridDataStatus.horizontalAlignment = GridData.FILL;
        statusBarComposite.setLayoutData(gridDataStatus);

        axeX = new Label(statusBarComposite, SWT.NONE);
        axeX.setText("x:");
        GridData data = new GridData();
        data.widthHint = 200;
        data.horizontalAlignment = GridData.END;
        data.grabExcessHorizontalSpace = true;
        axeX.setLayoutData(data);

        axeY = new Label(statusBarComposite, SWT.NONE);
        axeY.setText("y:");
        data = new GridData();
        data.widthHint = 200;
        data.horizontalAlignment = GridData.END;
        axeY.setLayoutData (data);

        zoomLabel = new Label(statusBarComposite, SWT.NONE);
        zoomLabel.setText("zoom:");
        data = new GridData();
        data.widthHint = 200;
        data.horizontalAlignment = GridData.END;
        zoomLabel.setLayoutData(data);
    }


    protected void createToolBar() {
        boolean showGrid = settingList.contains(GraphConfigConstant.SHOW_GRID_INPUT);
        boolean showXYScale = settingList.contains(GraphConfigConstant.SHOW_XY_SCALE);
        boolean showExtent = settingList.contains(GraphConfigConstant.SHOW_EXTENT);
        boolean showZoom = settingList.contains(GraphConfigConstant.SHOW_ZOOM);
        boolean showSelection = settingList.contains(GraphConfigConstant.SHOW_SELECTION);
        boolean showMainAxisColor = settingList.contains(GraphConfigConstant.SHOW_MAIN_AXIS_COLOR);
        boolean defaultPos = settingList.contains(GraphConfigConstant.SHOW_DEFAULT_POS);

        Composite toolbarComposite = new Composite(this, SWT.NONE);
        RowLayout toolbarLayout = new RowLayout(SWT.HORIZONTAL);
        toolbarLayout.center = true;
        toolbarComposite.setLayout(toolbarLayout);

        if (showSelection) {
            buttonCursor = new Button(toolbarComposite, SWT.TOGGLE);
            buttonCursor.setToolTipText("Select");
            buttonCursor.setImage(getImageStore().registerImage("/images/edit-select-" + getIconSize() + ".png"));
            buttonCursor.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    toolButtonSelected(buttonCursor);
                    canvasManager.setPaintTool(null);
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (showZoom) {
            Button buttonZoomIn = new Button(toolbarComposite, SWT.NONE);
            buttonZoomIn.setToolTipText("Zoom in");
            buttonZoomIn.setImage(getImageStore().registerImage("/images/zoom-in-" + getIconSize() + ".png"));
            buttonZoomIn.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.zoomIn();
                    canvasManager.refreshRedraw();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });

            Button buttonZoomOut = new Button(toolbarComposite, SWT.NONE);
            buttonZoomOut.setToolTipText("Zoom out");
            buttonZoomOut.setImage(getImageStore().registerImage("/images/zoom-out-" + getIconSize() + ".png"));
            buttonZoomOut.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.zoomOut();
                    canvasManager.refreshRedraw();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (showExtent) {
            Button buttonZoomExtent = new Button(toolbarComposite, SWT.NONE);
            buttonZoomExtent.setToolTipText("Zoom extent");
            buttonZoomExtent.setImage(getImageStore().registerImage("/images/zoom-fit-best-" + getIconSize() + ".png"));
            buttonZoomExtent.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    onZoomExtent();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (defaultPos) {
            Button buttonZoomExtent = new Button(toolbarComposite, SWT.NONE);
            buttonZoomExtent.setToolTipText("Default Position");
            buttonZoomExtent.setImage(getImageStore().registerImage("/images/center-pos-" + getIconSize() + ".png"));
            buttonZoomExtent.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    onDefaultPos();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        if (showMainAxisColor) {
            buttonMainAxisGray = new Button(toolbarComposite, SWT.TOGGLE);
            buttonMainAxisGray.setToolTipText("Axis grey");
            buttonMainAxisGray.setImage(getImageStore().registerImage("/images/snap-orthogonal-" + getIconSize() + ".png"));
            buttonMainAxisGray.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent arg0) {
                    canvasManager.setMainAxisGray(buttonMainAxisGray.getSelection());
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0) {
                }
            });
        }

        createToolButtons(toolbarComposite);

        Composite textsGroup = new Composite(toolbarComposite, SWT.SHADOW_IN);
        RowLayout layout = new RowLayout(SWT.HORIZONTAL);
        layout.center = true;
        textsGroup.setLayout(layout);


        if (showGrid) {
            RowData rowData = new RowData();
            rowData.width = 55;
            new Label(textsGroup, SWT.NONE).setText("Grid:");
            gridSizeText = new Text(textsGroup, SWT.BORDER);
            gridSizeText.setLayoutData(rowData);
            gridSizeText.setToolTipText("Set grid for both axes as a number or both axes as comma separated numbers");
            gridSizeText.addKeyListener(new KeyListener() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.keyCode == 16777219 || e.keyCode == 16777220) {
                        return;
                    }
                    double[] doubles = STextInput.validateAndGetDouble(gridSizeText, Double.MAX_VALUE);
                    canvasManager.setGridStepXY(doubles[0], doubles[1]);
                }

                @Override
                public void keyPressed(KeyEvent e) {
                }
            });
        }

        if (showXYScale) {
            new Label(textsGroup, SWT.NONE).setText("XY Scale:");
            xyScaleCombo = new Combo(textsGroup, SWT.BORDER);
            RowData rowData2 = new RowData();
            rowData2.width = 90;
            xyScaleCombo.setLayoutData(rowData2);
            xyScaleCombo.addKeyListener(new KeyListener() {
                @Override
                public void keyReleased(KeyEvent e) {
                    String text = xyScaleCombo.getText();
                    try {
                        double xyScale = Double.parseDouble(text);
                        canvasManager.changeXYScale(xyScale);
                    } catch (NumberFormatException e1) {
                    }
                }

                @Override
                public void keyPressed(KeyEvent e) {
                }
            });
            xyScaleCombo.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    String text = xyScaleCombo.getText();
                    try {
                        double xyScale = Double.parseDouble(text);
                        canvasManager.changeXYScale(xyScale);
                    } catch (NumberFormatException e1) {
                    }
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
            xyScaleCombo.setItems(new String[] {"0.0001", "0.001", "0.01", "0.1", "0.5", "1", "2", "10", "100", "1000", "10000"});
            xyScaleCombo.select(5);
        }
    }

    public void toolButtonSelected(Button toolButtonSelected) {
        toolChangedListeners.stream().forEach(l -> l.toolChanged(toolButtonSelected));
        if (buttonCursor != null && toolButtonSelected != buttonCursor) {
            buttonCursor.setSelection(false);
        }
    }

    protected void createToolButtons(@SuppressWarnings("unused") Composite toolbarComposite) {

    }

    @Override
    public void dispose() {
        super.dispose();
        canvasManager.dispose();
        getImageStore().dispose();
    }

    public void apply() {
        canvasManager.refreshRedraw();
    }

    public void applyExtent() {
        onZoomExtent();
    }

    public void setZoomMultiply(double zoomMultiply) {
        canvasManager.changeZoom(zoomMultiply);
    }

    public void setRasterAreaPainter(RasterAreaPainter rasterAreaPainter) {
        canvasManager.setRasterAreaPainter(rasterAreaPainter);
    }

    protected ImageStore getImageStore() {
        return imageStore;
    }

    public double getGridStepX() {
        return canvasManager.getGridStepX();
    }

    public double getGridStepY() {
        return canvasManager.getGridStepY();
    }

    public void setGridStep(double gridStep) {
        canvasManager.setGridStep(gridStep);
        if (gridSizeText != null) {
            gridSizeText.setText(String.valueOf(gridStep));
        }
    }

    public String getIconSize() {
        return iconSize;
    }

    public void addCoordsChangedListener(CoordsChangedListener coordsChangedListener) {
        coordsChangedListenerMulti.addCoordsChangedListener(coordsChangedListener);
    }

    public void addZoomChangedListener(ZoomChangedListener zoomChangedListener) {
        zoomChangedListenerMulti.addZoomChangedListener(zoomChangedListener);
    }

    public void setUnitsCoordY(String textCoordY) {
        canvasManager.setUnitsCoordY(textCoordY);
    }

    public void setUnitsCoordX(String textCoordX) {
        canvasManager.setUnitsCoordX(textCoordX);
    }

    public void setPaintTool(PaintTool paintTool) {
        canvasManager.setPaintTool(paintTool);
    }

    public boolean isSelectTool() {
        return buttonCursor != null && buttonCursor.getSelection();
    }

    @Override
    public void onCanvasPaint(GC gc, CoordState coordState) {
        figureCollector.paint(gc, coordState);
    }

    @Override
    public void onCanvasKeyPressed(KeyEvent keyEvent, CoordState coordState) {
    }

    @Override
    public void onCanvasMouseDown(MouseEvent mouseEvent, CoordState coordState, double x, double y) {
    }

    public SimpleFigureCollector getFigureCollector() {
        return figureCollector;
    }

    public void setMultiSelection(Boolean multiSelection) {
        this.multiSelection = multiSelection;
    }

    @Override
    public void refreshRedraw() {
        apply();
    }

    public void setXyScale(double scale) {
        if (xyScaleCombo != null) {
            xyScaleCombo.setText(String.valueOf(scale));
            canvasManager.changeXYScale(scale);
        }
    }

    public void setZoom(double zoom) {
        canvasManager.setZoom(zoom);
    }

    protected void onZoomExtent() {
        canvasManager.zoomExtent(figureCollector.getExtent());
    }

    public void onDefaultPos() {
        canvasManager.positionOnScreen(coordToDefaultX, coordToDefaultY, defaultPosFrom, onScreenPosX, onScreenPosY);
    }

    public void setDisplayParameters(double posX, double posY, double zoom) {
        canvasManager.setDisplayParameters(posX, posY, zoom);
    }

    public void setDefaultPosFrom(DefaultPosFrom defaultPosFrom) {
        this.defaultPosFrom = defaultPosFrom;
    }

    public void setCoordToDefaultX(double coordToDefaultX) {
        this.coordToDefaultX = coordToDefaultX;
    }

    public void setOnScreenPosX(int onScreenPosX) {
        this.onScreenPosX = onScreenPosX;
    }

    public void setOnScreenPosY(int onScreenPosY) {
        this.onScreenPosY = onScreenPosY;
    }

    public void setCoordToDefaultY(double coordToDefaultY) {
        this.coordToDefaultY = coordToDefaultY;
    }

    public void addToolChangedListener(ToolChangedListener toolChangedListener) {
        this.toolChangedListeners.add(toolChangedListener);
    }


    public void pushButtonMainAxisGray() {
        buttonMainAxisGray.setSelection(true);
        canvasManager.setMainAxisGray(true);
    }

    public void setZooming(Zooming zooming) {
        canvasManager.setZooming(zooming);
    }

    public void setSnapToGrid(boolean snapToGrid) {
        canvasManager.setSnapToGrid(snapToGrid);
    }

    public void setShowGrid(boolean showGrid) {
        canvasManager.setShowGrid(showGrid);
    }
}
