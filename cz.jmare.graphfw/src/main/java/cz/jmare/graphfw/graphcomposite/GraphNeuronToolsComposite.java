package cz.jmare.graphfw.graphcomposite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import cz.jmare.graphfw.figurecollector.AddCollector;
import cz.jmare.graphfw.figurecollector.AddEventConsumer;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.FigureEventConsumer;
import cz.jmare.graphfw.figurecollector.SimpleFigureCollector;
import cz.jmare.graphfw.tool.figure.PointPaintFigureTool;

public class GraphNeuronToolsComposite extends GraphEditComposite {
    private static final String[] CATEGORIES_ITEMS = {"0", "1", "Solve"};

    private Button buttonAddPoint;

    private SimpleFigureCollector figureCollector;

    private Combo categoryCombo;

    public GraphNeuronToolsComposite(Composite c, int style, SimpleFigureCollector figureCollector, Integer... graphConfigs) {
        super(c, style, figureCollector, graphConfigs);
        this.figureCollector = figureCollector;
        init(graphConfigs);
    }

    private void init(Integer... graphViewSettings) {
        if (figureCollector instanceof AddEventConsumer) {
            AddEventConsumer addEventConsumer = (AddEventConsumer) figureCollector;
            addEventConsumer.addFigureEventConsumer(new FigureEventConsumer() {
                @Override
                public void onAddedFigure(Figure figure) {
                    if (figure instanceof Category) {
                        Category category = (Category) figure;
                        category.setCategory(getCategory());
                    }
                }

                @Override
                public void onUpdatedFigure(Figure originFigure, Figure figure) {
                }

                @Override
                public void onDeletedFigure(Figure figure) {
                }});
        }
        buttonAddPoint.setSelection(true);
        canvasManager.setPaintTool(new PointPaintFigureTool((AddCollector) figureCollector));
    }

    @Override
    public void toolButtonSelected(Button toolButtonSelected) {
        super.toolButtonSelected(toolButtonSelected);
        if (toolButtonSelected != buttonAddPoint) buttonAddPoint.setSelection(false);
    }

    @Override
    protected void createToolButtons(Composite toolbarComposite) {
        buttonAddPoint = new Button(toolbarComposite, SWT.TOGGLE);
        buttonAddPoint.setToolTipText("Add Point");
        buttonAddPoint.setImage(getImageStore().registerImage("/images/point-" + getIconSize() + ".png"));
        buttonAddPoint.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                canvasManager.setPaintTool(new PointPaintFigureTool((AddCollector) figureCollector));
                toolButtonSelected(buttonAddPoint);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });
        createCategoryInput(toolbarComposite);
    }

    protected void createCategoryInput(Composite toolbarComposite) {
        RowData rowData = new RowData();
        rowData.width = 25;
        new Label(toolbarComposite, SWT.NONE).setText("Category:");
        categoryCombo = new Combo(toolbarComposite, SWT.BORDER | SWT.READ_ONLY);
        categoryCombo.setItems(CATEGORIES_ITEMS);
        categoryCombo.setToolTipText("Category - 0 and 1 + category for points to solve");
        categoryCombo.setLayoutData(rowData);
        categoryCombo.select(0);
        RowData layoutData = new RowData();
        layoutData.width = 60;
        categoryCombo.setLayoutData(layoutData);
    }

    public String getCategory() {
        return categoryCombo.getText();
    }
}
