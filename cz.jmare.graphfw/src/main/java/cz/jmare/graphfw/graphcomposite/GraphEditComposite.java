package cz.jmare.graphfw.graphcomposite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.SimpleFigureCollector;
import cz.jmare.swt.key.KeyLogger;

public class GraphEditComposite extends GraphReadComposite implements CanvasManagerListener {
    public GraphEditComposite(Composite c, int style, SimpleFigureCollector figureCollector, Integer... graphSettings) {
        super(c, style, figureCollector, graphSettings);
    }

    @Override
    public void onCanvasKeyPressed(KeyEvent keyEvent, CoordState coordState) {
        if (keyEvent.keyCode == SWT.DEL && isSelectTool()) {
            figureCollector.removeSelected();
            canvasManager.refreshRedraw();
        } else {
            super.onCanvasKeyPressed(keyEvent, coordState);
        }
    }

    @Override
    public void onCanvasMouseDown(MouseEvent mouseEvent, CoordState coordState, double x, double y) {
        if (isSelectTool()) {
            boolean toRepaint = figureCollector.select(coordState, x, y, multiSelection != null ? multiSelection : KeyLogger.ctrlPressed);
            if (toRepaint) {
                canvasManager.refreshRedraw();
            }
        } else {
            super.onCanvasMouseDown(mouseEvent, coordState, x, y);
        }
    }

    @Override
    protected void createToolButtons(Composite toolbarComposite) {
        super.createToolButtons(toolbarComposite);
        boolean showDelete = settingList.contains(GraphConfigConstant.SHOW_DELETE);

        if (showDelete) {
            Button buttonDelete = new Button(toolbarComposite, SWT.NONE);
            buttonDelete.setToolTipText("Delete");
            buttonDelete.setImage(getImageStore().registerImage("/images/edit-delete-" + getIconSize() + ".png"));
            buttonDelete.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    figureCollector.removeSelected();
                    canvasManager.refreshRedraw();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                }
            });
        }
    }
}
