package cz.jmare.graphfw.graphcomposite;

public interface Redrawable {
    void refreshRedraw();
}
