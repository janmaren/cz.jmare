package cz.jmare.graphfw.graphcomposite;

import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_ADD_POINT_BUTTON;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_CATEGORY_INPUT;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_DELETE;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_GRID_INPUT;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_SELECTION;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_SMALL_ICONS;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_ZOOM;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import cz.jmare.graphfw.figurecollector.MixedFigureCollector;
import cz.jmare.graphfw.rastercollector.GraphRasterAreaPainter;
import cz.jmare.graphfw.template.graph.OutlineComposite;
import cz.jmare.graphfw.template.graph.TwoGraphsOnePanelTemplateFigureComposite;

public abstract class TwoNeuronGraphsOnePanelTemplateComposite extends TwoGraphsOnePanelTemplateFigureComposite {
    protected GraphRasterAreaPainter mainGraphRasterAreaPainter = new GraphRasterAreaPainter();

    public TwoNeuronGraphsOnePanelTemplateComposite(Composite parent) {
        super(parent, SWT.NONE);
        mainGraphEditComposite.setRasterAreaPainter(mainGraphRasterAreaPainter);
    }

    @Override
    public void createMainViewComposite(Composite parentComposite) {
        mainGraphEditComposite = new GraphNeuronToolsComposite(parentComposite, SWT.NONE, new MixedFigureCollector(), SHOW_GRID_INPUT, SHOW_ZOOM, SHOW_CATEGORY_INPUT, SHOW_ADD_POINT_BUTTON, SHOW_DELETE, SHOW_SELECTION);
        mainGraphEditComposite.setGridStep(1d);
        mainGraphEditComposite.setZoomMultiply(0.075d);
        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.grabExcessVerticalSpace = true;
        layoutData.verticalAlignment = SWT.FILL;
        layoutData.horizontalAlignment = SWT.FILL;
        mainGraphEditComposite.setLayoutData(layoutData);
    }

    @Override
    public void createSecondaryViewComposite(Composite parentComposite) {
        secondaryViewComposite = new GraphReadComposite(parentComposite, SWT.NONE, new MixedFigureCollector(), SHOW_SMALL_ICONS, SHOW_ZOOM);
    }

    @Override
    public abstract void createPanelContent(Composite panelComposite);

    @Override
    protected void createOutlineComposite(Composite parentComposite) {
        OutlineComposite outlineComposite = new OutlineComposite(parentComposite, SWT.NONE, (MixedFigureCollector) mainGraphEditComposite.getFigureCollector(), mainGraphRasterAreaPainter);
        outlineComposite.setGraphViewComposite(mainGraphEditComposite);
        GridData layoutData = new GridData();
        layoutData.minimumWidth = 100;
        layoutData.verticalAlignment = SWT.BEGINNING;
        outlineComposite.setLayoutData(layoutData);
    }

}
