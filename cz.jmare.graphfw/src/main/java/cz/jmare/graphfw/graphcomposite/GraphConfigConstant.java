package cz.jmare.graphfw.graphcomposite;

public interface GraphConfigConstant {
    int SHOW_SMALL_ICONS = 90;

    int SHOW_SELECTION = 101;
    @Deprecated // call GraphReadComposite.showGrid(true)
    int SHOW_GRID = 102;
    int SHOW_GRID_INPUT = 103;
    int SHOW_ZOOM = 104;
    int SHOW_EXTENT = 105;
    int SHOW_XY_SCALE = 106;
    int SHOW_MAIN_AXIS_COLOR = 107;
    int SHOW_DEFAULT_POS = 108;

    int SHOW_DELETE = 120;

    int SHOW_ADD_POINT_BUTTON = 201;
    int SHOW_ADD_LINESEGMENT_BUTTON = 202;
    int SHOW_ADD_STRAIGHTLINE_BUTTON = 203;
    int SHOW_CATEGORY_INPUT = 210;
}
