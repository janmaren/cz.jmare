package cz.jmare.graphfw.file;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import cz.jmare.graphfw.color.*;
import org.eclipse.swt.graphics.Color;

import cz.jmare.graphfw.color.ColorMapUtil.PropColorItem;
import cz.jmare.swt.util.RGBSwtUtil;
import cz.jmare.swt.color.ColorUtil;

public class ColorFileUtil {
    public static void saveRangeColorStream(String filename, Object colorObject) throws IOException {
        throw new RuntimeException("Not implemented"); // TODO: reimplement
    }

    public static <T> ColorProvider<T> loadColorProvider(InputStream is, Class<T> clazz) throws IOException {
        throw new RuntimeException("Not implemented");
    }

    @SuppressWarnings("rawtypes")
    public static ColorProvider loadColorProviderFromProperties(InputStream is) throws IOException {
        Properties properties = new Properties();
        properties.load(is);
        String colorProviderClass = properties.getProperty("colorProvider");
        try {
            Class<?> claz = Class.forName(colorProviderClass);
            Constructor<?> declaredConstructor = claz.getDeclaredConstructor();
            ColorProvider colorProvider = (ColorProvider) declaredConstructor.newInstance();
            if (colorProvider instanceof AssignedColorProvider) {
                AssignedColorProvider assignedColorProvider = (AssignedColorProvider) colorProvider;
                LinkedHashMap<Integer, Color> categoryProperties = ColorMapUtil.loadCategoryProperties(properties);
                List<Color> values = categoryProperties.entrySet().stream().map(e -> e.getValue())
                        .collect(Collectors.toList());
                assignedColorProvider.setColors(values);
                return assignedColorProvider;
            } else if (colorProvider instanceof RangeColorProvider) {
                RangeColorProvider rangeColorProvider = (RangeColorProvider) colorProvider;
                LinkedHashMap<Integer, PropColorItem> propItems = ColorMapUtil.loadCategoryRangeProperties(properties);

                List<ColorRange> ranges = propItems.entrySet().stream().map(e -> new ColorRange(e.getValue().fromValue, e.getValue().color)).collect(Collectors.toList());
                rangeColorProvider.setRanges(ranges);
                String initialColor = properties.getProperty("initialColor");
                if (initialColor != null) {
                    rangeColorProvider.setInitialColor(ColorUtil.getColor(RGBSwtUtil.createRGB(initialColor)));
                }
                return rangeColorProvider;
            } else {
                throw new RuntimeException("Not supported color provider " + claz);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Not found class " + colorProviderClass, e);
        }
    }
}
