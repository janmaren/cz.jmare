package cz.jmare.graphfw;

import java.util.ArrayList;
import java.util.List;

public class ZoomChangedListenerMulti implements ZoomChangedListener {
    private List<ZoomChangedListener> list = new ArrayList<>();

    public ZoomChangedListenerMulti() {
        super();
    }

    public ZoomChangedListenerMulti(ZoomChangedListener zoomChangedListener) {
        super();
        list.add(zoomChangedListener);
    }

    @Override
    public void zoomChanged(double zoom) {
        for (ZoomChangedListener zoomChangedListenerMulti : list) {
            zoomChangedListenerMulti.zoomChanged(zoom);
        }
    }

    public void addZoomChangedListener(ZoomChangedListener zoomChangedListenerMulti) {
        list.add(zoomChangedListenerMulti);
    }
}
