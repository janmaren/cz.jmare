package cz.jmare.graphfw;

public interface CoordsChangedListener {
    void coordsChanged(double x, double y, int stateMask);
}
