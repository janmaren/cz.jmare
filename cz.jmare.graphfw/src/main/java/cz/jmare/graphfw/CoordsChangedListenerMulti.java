package cz.jmare.graphfw;

import java.util.ArrayList;
import java.util.List;

public class CoordsChangedListenerMulti implements CoordsChangedListener {
    private List<CoordsChangedListener> list = new ArrayList<>();

    public CoordsChangedListenerMulti() {
        super();
    }

    public CoordsChangedListenerMulti(CoordsChangedListener coordsChangedListener) {
        super();
        list.add(coordsChangedListener);
    }


    public void addCoordsChangedListener(CoordsChangedListener coordsChangedListenerMulti) {
        list.add(coordsChangedListenerMulti);
    }

    @Override
    public void coordsChanged(double x, double y, int stateMask) {
        for (CoordsChangedListener coordsChangedListenerMulti : list) {
            coordsChangedListenerMulti.coordsChanged(x, y, stateMask);
        }
    }
}
