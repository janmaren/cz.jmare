package cz.jmare.graphfw;

public interface ZoomChangedListener {
    void zoomChanged(double zoom);
}
