package cz.jmare.graphfw.figurecollector;

import java.util.List;

import cz.jmare.math.geometry.entity.TwoPoints;

public class ExtentUtil {
    @SafeVarargs
    public static TwoPoints getExtent(List<Figure>... figuresSet) {
        TwoPoints coordsPair = new TwoPoints();
        boolean hasExtent = false;

        for (List<Figure> figures : figuresSet) {
            for (Figure figure : figures) {
                TwoPoints extent = figure.getExtent();
                if (extent == null) {
                    continue;
                }
                if (hasExtent) {
                    if (!Double.isNaN(extent.x1) && extent.x1 < coordsPair.x1) {
                        coordsPair.x1 = extent.x1;
                    }
                    if (!Double.isNaN(extent.y1) && extent.y1 < coordsPair.y1) {
                        coordsPair.y1 = extent.y1;
                    }
                    if (!Double.isNaN(extent.x2) && extent.x2 > coordsPair.x2) {
                        coordsPair.x2 = extent.x2;
                    }
                    if (!Double.isNaN(extent.y2) && extent.y2 > coordsPair.y2) {
                        coordsPair.y2 = extent.y2;
                    }
                } else {
                    coordsPair = extent;
                    hasExtent = true;
                }
            }
        }

        if (hasExtent) {
            return coordsPair;
        }

        return null;
    }

    @SafeVarargs
    public static TwoPoints getFiguresExtent(List<Figure>... figuresSet) {
        TwoPoints coordsPair = new TwoPoints();
        boolean hasExtent = false;

        for (List<Figure> figures : figuresSet) {
            for (Figure figure : figures) {
                TwoPoints extent = figure.getExtent();
                if (extent == null) {
                    continue;
                }
                if (hasExtent) {
                    if (extent.x1 < coordsPair.x1) {
                        coordsPair.x1 = extent.x1;
                    }
                    if (extent.y1 < coordsPair.y1) {
                        coordsPair.y1 = extent.y1;
                    }
                    if (extent.x2 > coordsPair.x2) {
                        coordsPair.x2 = extent.x2;
                    }
                    if (extent.y2 > coordsPair.y2) {
                        coordsPair.y2 = extent.y2;
                    }
                } else {
                    coordsPair = extent;
                    hasExtent = true;
                }
            }
        }

        if (hasExtent) {
            return coordsPair;
        }

        return null;
    }
}
