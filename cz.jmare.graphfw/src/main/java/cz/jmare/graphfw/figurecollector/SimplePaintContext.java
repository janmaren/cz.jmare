package cz.jmare.graphfw.figurecollector;

import org.eclipse.swt.graphics.Color;

public class SimplePaintContext implements PaintContext {

    private Color color;
    private double lineWidthFactor;

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public double getLineWidthFactor() {
        return lineWidthFactor;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setLineWidthFactor(double lineWidthFactor) {
        this.lineWidthFactor = lineWidthFactor;
    }

}
