package cz.jmare.graphfw.figurecollector;

public interface FigureEventConsumer {
    @Deprecated
    void onAddedFigure(Figure figure);
    @Deprecated
    void onUpdatedFigure(Figure originFigure, Figure figure);
    void onDeletedFigure(Figure figure /* change to String id */);
}
