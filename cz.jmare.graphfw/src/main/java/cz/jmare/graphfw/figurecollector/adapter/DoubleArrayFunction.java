package cz.jmare.graphfw.figurecollector.adapter;

public interface DoubleArrayFunction<R> {
    R apply(double[] values);
}
