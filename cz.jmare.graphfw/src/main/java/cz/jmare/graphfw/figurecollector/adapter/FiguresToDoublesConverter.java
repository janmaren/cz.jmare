package cz.jmare.graphfw.figurecollector.adapter;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.graphfw.figure.PointFigure;
import cz.jmare.graphfw.figurecollector.Figure;

public class FiguresToDoublesConverter {
    public static InputsResults figuresToDoubles(List<Figure> figures, StringDoubleArrayFunction stringDoubleArrayFunction) {
        InputsResults inputsResults = new InputsResults();
        ArrayList<double[]> inputsList = new ArrayList<double[]>();
        ArrayList<double[]> resultsList = new ArrayList<double[]>();
        for (Figure figure : figures) {
            if (figure instanceof PointFigure) {
                PointFigure pointFigure = (PointFigure) figure;
                if (pointFigure.category == null) {
                    continue;
                }
                double[] input = new double[2];
                input[0] = pointFigure.x;
                input[1] = pointFigure.y;
                inputsList.add(input);

                resultsList.add(stringDoubleArrayFunction.apply(pointFigure.category));
            }
        }

        if (inputsList.size() != resultsList.size()) {
            throw new IllegalStateException("inputsList.size() != resultsList.size()");
        }
        double[][] inputs = new double[inputsList.size()][];
        double[][] results = new double[inputsList.size()][];
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = inputsList.get(i);
            results[i] = resultsList.get(i);
        }

        inputsResults.inputs = inputs;
        inputsResults.results = results;

        return inputsResults;
    }

    public static InputsResults figuresToDoubles(List<Figure> figures) {
        StringDoubleArrayFunction stringDoubleArrayFunction = new StringDoubleArrayFunction() {
            @Override
            public double[] apply(String str) {
                try {
                    double parseDouble = Double.parseDouble(str);
                    return new double[] {parseDouble};
                } catch (NumberFormatException e) {
                    // not a double
                }
                double[] result = new double[str.length()];
                for (int i = 0; i < str.length(); i++) {
                    char charAt = str.charAt(i);
                    if (i == 0) {
                        if (charAt == '0') {
                            result[i] = 0d;
                        } else if (charAt == '1') {
                            result[i] = 1d;
                        } else {
                            throw new RuntimeException("Invalid figure category '" + str + "' found which doesn't start by 0 or 1");
                        }
                    } else {
                        if (charAt == 'A') {
                            result[i] = 0d;
                        } else if (charAt == 'B') {
                            result[i] = 1d;
                        } else {
                            throw new RuntimeException("Invalid figure category '" + str + "' found which doesn't contain A or B letters behind 0/1");
                        }
                    }
                }
                return result;
            }};
        return figuresToDoubles(figures, stringDoubleArrayFunction);
    }
}
