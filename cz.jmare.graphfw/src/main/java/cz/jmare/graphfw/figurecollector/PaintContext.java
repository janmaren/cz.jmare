package cz.jmare.graphfw.figurecollector;

import org.eclipse.swt.graphics.Color;

public interface PaintContext {
    Color getColor();
    double getLineWidthFactor();
}
