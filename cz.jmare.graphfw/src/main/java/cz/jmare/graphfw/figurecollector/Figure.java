package cz.jmare.graphfw.figurecollector;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.math.geometry.entity.TwoPoints;

public interface Figure {
    void paint(GC gc, CoordState coordState, PaintContext paintContext);

    TwoPoints getExtent();

    Double calculateDistance(CoordState coordState, double fromX, double fromY);
}
