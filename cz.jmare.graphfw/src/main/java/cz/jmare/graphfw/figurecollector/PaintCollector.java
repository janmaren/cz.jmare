package cz.jmare.graphfw.figurecollector;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.math.geometry.entity.TwoPoints;

public interface PaintCollector {
    void paint(GC gc, CoordState coordState);

    TwoPoints getExtent();
}
