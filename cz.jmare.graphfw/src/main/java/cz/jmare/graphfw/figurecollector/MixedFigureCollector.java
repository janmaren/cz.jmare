package cz.jmare.graphfw.figurecollector;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.color.ColorProvider;
import cz.jmare.graphfw.color.ColorMapUtil;
import cz.jmare.graphfw.legend.Legend;
import cz.jmare.math.geometry.entity.TwoPoints;

public class MixedFigureCollector implements SimpleFigureCollector, AddCollector, ReplaceCollector, AddEventConsumer {
    protected List<Figure> figures = new ArrayList<Figure>();
    private ColorProvider<String> colorProvider;
    private double lineWidth = 1;
    private List<FigureEventConsumer> figureEventConsumers = new ArrayList<>();
    private List<Figure> selectedFigures = new ArrayList<>();
    private Figure editingFigure;
    private Legend legend;

    public MixedFigureCollector(ColorProvider<String> colorProvider, Legend legend) {
        super();
        this.colorProvider = colorProvider;
        this.legend = legend;
        legend.setColorProvider(colorProvider);
    }

    public MixedFigureCollector(ColorProvider<String> colorProvider) {
        super();
        this.colorProvider = colorProvider;
    }

    public MixedFigureCollector() {
        super();
        colorProvider = ColorMapUtil.getDefaultAssignedColorProvider();
    }


    @Override
    @SuppressWarnings("deprecation")
	public synchronized void addFigure(Figure figure) {
        figures.add(figure);
        rebuildLegend();
        figureEventConsumers.stream().forEach(figureEventConsumer -> figureEventConsumer.onAddedFigure(figure));
    }

    public synchronized void removeFigure(Figure figure) {
        if (figures.remove(figure)) {
            rebuildLegend();
            figureEventConsumers.stream().forEach(figureEventConsumer -> figureEventConsumer.onDeletedFigure(figure));
        }
    }

    @Override
    public synchronized void paint(GC gc, CoordState coordState) {
        SimplePaintContext simplePaintContext = new SimplePaintContext();
        List<String> disabledCategories = new ArrayList<>();
        if (legend != null) {
            disabledCategories = legend.getDisabledCategories();
        }
        for (Figure figure : figures) {
            if (figure.equals(editingFigure)) {
                continue;
            }

            if (figure instanceof Category) {
                Category category = (Category) figure;
                if (disabledCategories.contains(category.getCategory())) {
                    if (selectedFigures.contains(figure)) {
                        selectedFigures.remove(figure);
                    }
                    continue;
                }
                Color color = colorProvider.getColor(category.getCategory());
                simplePaintContext.setColor(color);
            } else {
                simplePaintContext.setColor(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
            }

            if (selectedFigures.contains(figure)) {
                simplePaintContext.setLineWidthFactor(2 * lineWidth);
            } else {
                simplePaintContext.setLineWidthFactor(lineWidth);
            }

            figure.paint(gc, coordState, simplePaintContext);
        }
    }

    @Override
    public TwoPoints getExtent() {
        return ExtentUtil.getFiguresExtent(getVisibleFigures());
    }

    public List<Figure> getFigures() {
        return figures;
    }

    public List<Figure> getVisibleFigures() {
        if (legend != null) {
            List<String> categories = legend.getEnabledCategories();
            return figures.stream().filter(f -> (!(f instanceof Category)) || (f instanceof Category && categories.contains(((Category) f).getCategory()))).collect(Collectors.toList());
        } else {
            return figures;
        }
    }

    public void setFigures(List<Figure> figures) {
        this.figures = figures;
        selectedFigures = new ArrayList<>();
        rebuildLegend();
    }

    private void rebuildLegend() {
        if (legend == null) {
            return;
        }
        List<IdName> ids = new ArrayList<>();
        figures.forEach(f -> {
            if (f instanceof Category) {
                IdName idName = new IdName();
                idName.id = ((Category) f).getCategory();
                idName.name = ((Category) f).getName();
                if (!ids.contains(idName)) {
                    ids.add(idName);
                }
            }
        });
        legend.setCategories(ids);
    }

    @Override
    public boolean select(CoordState coordState, double coordX, double coordY, boolean multi) {
        double nearestFigureDistance = Double.MAX_VALUE;
        Figure selectedFigure = null;
        List<String> disabledCategories = new ArrayList<>();
        if (legend != null) {
            disabledCategories = legend.getDisabledCategories();
        }
        for (ListIterator<Figure> listIterator = figures.listIterator(); listIterator.hasNext();) {
            Figure figure = listIterator.next();

            if (figure instanceof Category) {
                Category category = (Category) figure;
                if (disabledCategories.contains(category.getCategory())) {
                    continue;
                }
            }

            Double figureDistance = figure.calculateDistance(coordState, coordX, coordY);
            if (figureDistance != null && figureDistance < nearestFigureDistance && coordState.toPixelsX(figureDistance) < 10) {
                nearestFigureDistance = figureDistance;
                selectedFigure = figure;
            }
        }

        if (selectedFigure != null) {
            if (selectedFigures.contains(selectedFigure)) {
                selectedFigures.remove(selectedFigure);
            } else {
                if (!multi) {
                    selectedFigures.clear();
                }
                selectedFigures.add(selectedFigure);
            }
        }

        return selectedFigure != null;
    }

    @Override
    public void removeSelected() {
        for (Figure figure : selectedFigures) {
            if (figures.remove(figure)) {
                figureEventConsumers.stream().forEach(figureEventConsumer -> figureEventConsumer.onDeletedFigure(figure));
            }
        }
        selectedFigures.clear();
        rebuildLegend();
    }

    public void setColorProvider(ColorProvider<String> colorProvider) {
        this.colorProvider = colorProvider;
        if (legend != null) {
            legend.setColorProvider(colorProvider);
        }
    }

    public int getFiguresCount() {
        return figures.size();
    }

    public void clear() {
        figures.clear();
    }

    @Override
    public void unselectAll() {
        selectedFigures.clear();
    }


    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    @Override
    public void addFigureEventConsumer(FigureEventConsumer figureEventConsumer) {
        this.figureEventConsumers.add(figureEventConsumer);
    }

    @Override
    public List<Figure> getSelectedFigures() {
        return selectedFigures;
    }

    @Override
    public void beginEditFigure(Figure origFigure) {
        this.editingFigure = origFigure;
    }

    @SuppressWarnings("deprecation")
	@Override
    public void commitEditFigure(Figure newFigure) {
        int indexOf = figures.indexOf(editingFigure);
        if (indexOf == -1) {
            throw new IllegalStateException("beginEditFigure not called previously");
        }
        figures.set(indexOf, newFigure);
        figureEventConsumers.stream().forEach(figureEventConsumer -> figureEventConsumer.onUpdatedFigure(editingFigure, newFigure));
        this.editingFigure = null;
    }

    @Override
    public void rollbackEditFigure() {
        this.editingFigure = null;
    }

    public void setLegend(Legend legend) {
        this.legend = legend;
        this.legend.setColorProvider(colorProvider);
    }

    @Override
    public boolean select(String id) {
        selectedFigures = new ArrayList<Figure>();
        if (id != null) {
            for (Figure figure : figures) {
                if (figure instanceof Category) {
                    Category category = (Category) figure;
                    if (category.getCategory().equals(id)) {
                        selectedFigures.add(figure);
                    }
                }
            }
        }
        return selectedFigures.size() > 0;
    }

    public int findIndexOfFigure(String id) {
        for (int i = 0; i < figures.size(); i++) {
            Figure figure = figures.get(i);
            if (figure instanceof Category) {
                Category category = (Category) figure;
                if (category.getCategory().equals(id)) {
                    return i;
                }
            }
        }
        return -1;
    }
}
