package cz.jmare.graphfw.figurecollector;

public interface Category {
    String getCategory();
    void setCategory(String category);

    String getName();
    void setName(String name);
}
