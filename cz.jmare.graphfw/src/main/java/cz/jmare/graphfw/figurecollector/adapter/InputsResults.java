package cz.jmare.graphfw.figurecollector.adapter;

import java.util.Arrays;

public class InputsResults {
    public double[][] inputs;
    public double[][] results;

    @Override
    public String toString() {
        return "InputsResults [inputs: " + Arrays.deepToString(inputs) + ", results:" + Arrays.deepToString(results) + "]";
    }

}
