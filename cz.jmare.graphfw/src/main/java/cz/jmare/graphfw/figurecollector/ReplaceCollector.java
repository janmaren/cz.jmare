package cz.jmare.graphfw.figurecollector;

import java.util.List;

import cz.jmare.graphfw.CoordState;

public interface ReplaceCollector {
    void beginEditFigure(Figure origFigure);
    void commitEditFigure(Figure newFigure);
    void rollbackEditFigure();
    boolean select(CoordState coordState, double coordX, double coordY, boolean multi);
    public List<Figure> getSelectedFigures();
}
