package cz.jmare.graphfw.figurecollector;

import cz.jmare.graphfw.CoordState;

public interface SimpleFigureCollector extends PaintCollector {
    boolean select(CoordState coordState, double coordX, double coordY, boolean multi);

    boolean select(String id);

    void unselectAll();

    void removeSelected();
}
