package cz.jmare.graphfw.figurecollector.adapter;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.graphfw.figure.PointFigure;
import cz.jmare.graphfw.figurecollector.Figure;

public class DoublesToFiguresConverter {
    public static List<Figure> doublesToFigures(double[][] inputs, double[][] results, int inputIndexX, int inputIndexY, DoubleArrayFunction<String> doubleArrayFunction) {
        ArrayList<Figure> figures = new ArrayList<Figure>();
        for (int i = 0; i < inputs.length; i++) {
            double[] input = inputs[i];
            double[] result = results[i];
            PointFigure pointFigure = new PointFigure(input[inputIndexX], input[inputIndexY]);
            pointFigure.setCategory(doubleArrayFunction.apply(result));
            figures.add(pointFigure);
        }
        return figures;
    }

    public static List<Figure> doublesToFigures(double[][] inputs, double[][] results, int inputIndexX, int inputIndexY) {
        DoubleArrayFunction<String> doubleArrayFunction = new DoubleArrayFunction<String>() {
            @Override
            public String apply(double[] values) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < values.length; i++) {
                    double val = values[i];
                    if (i == 0) {
                        if (val < 0.5) {
                            sb.append("0");
                        } else {
                            sb.append("1");
                        }
                    } else {
                        if (val < 0.5) {
                            sb.append("A");
                        } else {
                            sb.append("B");
                        }
                    }
                }
                return sb.toString();
            }};
        return doublesToFigures(inputs, results, inputIndexX, inputIndexY, doubleArrayFunction);
    }

    public static List<Figure> doublesToFigures(double[][] inputs, double[][] results) {
        return doublesToFigures(inputs, results, 0, 1);
    }
}
