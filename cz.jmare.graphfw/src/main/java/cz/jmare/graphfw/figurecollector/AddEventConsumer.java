package cz.jmare.graphfw.figurecollector;

public interface AddEventConsumer {
    void addFigureEventConsumer(FigureEventConsumer figureEventConsumer);
}
