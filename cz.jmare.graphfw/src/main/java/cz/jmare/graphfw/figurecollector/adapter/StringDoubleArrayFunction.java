package cz.jmare.graphfw.figurecollector.adapter;

public interface StringDoubleArrayFunction {
    double[] apply(String str);
}
