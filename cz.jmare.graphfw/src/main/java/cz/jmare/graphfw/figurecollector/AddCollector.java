package cz.jmare.graphfw.figurecollector;

public interface AddCollector {
    void addFigure(Figure figure);
}
