package cz.jmare.graphfw.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.math.geometry.entity.TwoPoints;

/**
 * Marker class (empty)
 */
public class FirstClickFigure implements Figure {
    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
    }

    @Override
    public TwoPoints getExtent() {
        return null;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return null;
    }
}
