package cz.jmare.graphfw.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.StraightLine;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.geometry.relation.DistanceUtil;

public class StraightLineFigure implements Figure, Category {
    public double a;

    public double b;

    public double c;

    public String category;

    private String name;

    public StraightLineFigure(double x1, double y1, double x2, double y2) {
        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    public StraightLineFigure(double a, double b, double c) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public String getCategory() {
        return category;
    }

    public StraightLine toStraightLine() {
        return new StraightLine(a, b, c);
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {

        double coordX0 = coordState.getCoordX(0);
        double coordX1 = coordState.getCoordX(coordState.getScreenWidth());
        double coordY0 = coordState.getCoordY(coordState.getScreenHeight());
        double coordY1 = coordState.getCoordY(0);

        LineSegmentDouble crop = CropWindowDoubleFunctional.crop(toStraightLine(), coordX0, coordY0, coordX1, coordY1);
        if (crop != null) {
            gc.setForeground(paintContext.getColor());
            gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));
            int screenX1 = coordState.getScreenX(crop.x1);
            int screenY1 = coordState.getScreenY(crop.y1);
            int screenX2 = coordState.getScreenX(crop.x2);
            int screenY2 = coordState.getScreenY(crop.y2);
            gc.drawLine(screenX1, screenY1, screenX2, screenY2);
            gc.setLineWidth(1);
        }
    }

    @Override
    public TwoPoints getExtent() {
        return null;
    }

    @Override
    public String toString() {
        return "StraightLineFigure [a=" + a + ", beta=" + b + ", c=" + c + ", category=" + category + "]";
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return DistanceUtil.pointToStraightLineDistance(fromX, fromY, toStraightLine());
    }

    public StraightLineFigure copy(double a, double b, double c) {
        StraightLineFigure straightLineFigure = new StraightLineFigure(a, b, c);
        straightLineFigure.setCategory(category);
        return straightLineFigure;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
