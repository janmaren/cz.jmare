package cz.jmare.graphfw.figure;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.graphfw.math.CropWindowFloat;
import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.raw.RawRandomAccessReader;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.LineSegmentFloat;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.geometry.relation.DistanceUtil;

public class CurveFigure implements Figure, Category {
    public RandomAccessCurve points;

    public String category;

    public boolean drawNodes;

    private String name;

    @Deprecated
    public CurveFigure(List<Point> points) {
        super();
        this.points = new RawRandomAccessReader(points);
    }

    public CurveFigure(RandomAccessCurve points) {
        super();
        this.points = points;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        if (points.size() < 2) {
            throw new IllegalStateException("There is curve with points size " + points.size());
        }
        gc.setForeground(paintContext.getColor());

        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);

        CropWindowFloat cropWindow = new CropWindowFloat((float) canvasX0, (float) canvasY0, (float) canvasX1, (float) canvasY1);

        gc.setForeground(paintContext.getColor());
        gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));


        int j = 0;
        while (j < points.size() && points.get(j).x < canvasX0) j++;
        Point lastPoint = points.get(j > 1 ? j - 1 : 0);
        if (!Double.isFinite(lastPoint.x) || !Double.isFinite(lastPoint.y)) {
            System.err.println("Found infinity on first point " + lastPoint);
            return;
        }
        int lastScreenX = Integer.MIN_VALUE;
        int minYinLastScreenX = Integer.MAX_VALUE;
        int maxYinLastScreenX = Integer.MIN_VALUE;
        int maxX = 0;
        int maxY = 0;
        int minX = coordState.getScreenWidth();
        int minY = coordState.getScreenHeight();

        for (int i = j; i < points.size(); i++) {
            Point point = points.get(i);
            if (!Double.isFinite(point.x) || !Double.isFinite(point.y)) {
                System.err.println("Found infinity on index " + i + " point " + lastPoint);
                return;
            }
            LineSegmentFloat segmentFloat = new LineSegmentFloat((float) lastPoint.x, (float) lastPoint.y, (float) point.x, (float) point.y);
            LineSegmentFloat lineSegmentCrop = cropWindow.crop(segmentFloat);

            if (lineSegmentCrop != null) {
                int screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
                int screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
                int screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
                int screenY2 = coordState.getScreenY(lineSegmentCrop.y2);

                if (lastScreenX == screenX1 && lastScreenX == screenX2) {
                    if (screenY2 < minYinLastScreenX || screenY2 > maxYinLastScreenX) {
                        gc.drawLine(screenX1, screenY1, screenX2, screenY2);

                        if (screenX2 > maxX) {
                            maxX = screenX2;
                        }
                        if (screenX2 < minX) {
                            minX = screenX2;
                        }
                        if (screenY2 > maxY) {
                            maxY = screenY2;
                        }
                        if (screenY2 < minY) {
                            minY = screenY2;
                        }

                        if (screenY2 < minYinLastScreenX) {
                            minYinLastScreenX = screenY2;
                        } else {
                            maxYinLastScreenX = screenY2;
                        }
                    }
                } else {
                    gc.drawLine(screenX1, screenY1, screenX2, screenY2);

                    if (screenX2 > maxX) {
                        maxX = screenX2;
                    }
                    if (screenX2 < minX) {
                        minX = screenX2;
                    }
                    if (screenY2 > maxY) {
                        maxY = screenY2;
                    }
                    if (screenY2 < minY) {
                        minY = screenY2;
                    }
                }
                if (lastScreenX != screenX2) {
                    lastScreenX = screenX2;
                    minYinLastScreenX = Integer.MAX_VALUE;
                    maxYinLastScreenX = Integer.MIN_VALUE;
                }
            } else {
                if (point.x > canvasX1) {
                    break;
                }
                lastScreenX = Integer.MIN_VALUE;
                minYinLastScreenX = Integer.MAX_VALUE;
                maxYinLastScreenX = Integer.MIN_VALUE;
            }
            lastPoint = point;
        }
        int maxXDist = maxX - minX;
        int maxYDist = maxY - minY;
        double pyth = Math.sqrt(maxXDist * maxXDist + maxYDist * maxYDist);
        if (pyth < 30) {
            Point centerPoint = points.get(points.size() / 2);
            if (centerPoint.x >= canvasX0 && centerPoint.x <= canvasX1 && centerPoint.y >= canvasY0 && centerPoint.y <= canvasY1) {
                int screenX = coordState.getScreenX(centerPoint.x);
                int screenY = coordState.getScreenY(centerPoint.y);
                int origLineStyle = gc.getLineStyle();
                gc.setLineStyle(SWT.LINE_DASH);
                gc.drawRectangle(screenX - 20, screenY - 20, 40, 40);
                gc.setLineStyle(origLineStyle);
            }
        }
        //System.out.println(pyth);
        if (drawNodes) {
            lastScreenX = Integer.MIN_VALUE;
            int lastScreenY = Integer.MIN_VALUE;
            for (int i = j; i < points.size(); i++) {
                Point point = points.get(i);
                if (point.x < canvasX0) {
                    continue;
                }
                if (point.x > canvasX1) {
                    break;
                }
                int screenX = coordState.getScreenX(point.x);
                int screenY = coordState.getScreenY(point.y);
                if (lastScreenX != screenX || lastScreenY != screenY) {
                    gc.drawRectangle(screenX - 2, screenY - 2, 4, 4);
                    lastScreenX = screenX;
                    lastScreenY = screenY;
                }
            }
        }
    }
    @Override
    public TwoPoints getExtent() {
        if (points.size() == 0) {
            return null;
        }
        TwoPoints coordsPair = new TwoPoints();
        Point point = points.get(0);
        coordsPair.x1 = point.x;
        coordsPair.y1 = point.y;
        coordsPair.x2 = point.x;
        coordsPair.y2 = point.y;

        for (int i = 1; i < points.size(); i++) {
            point = points.get(i);
            if (point.x < coordsPair.x1) {
                coordsPair.x1 = point.x;
            }
            if (point.y < coordsPair.y1) {
                coordsPair.y1 = point.y;
            }
            if (point.x > coordsPair.x2) {
                coordsPair.x2 = point.x;
            }
            if (point.y > coordsPair.y2) {
                coordsPair.y2 = point.y;
            }
        }

        return coordsPair;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        double min = Double.MAX_VALUE;
        Point lastPoint = points.get(0);
        for (int i = 1; i < points.size(); i++) {
            Point point = points.get(i);
            double distance = DistanceUtil.pointToLineSegmentDistance(fromX, fromY, new LineSegmentDouble(lastPoint.x, lastPoint.y, point.x, point.y));
            if (distance < min) {
                min = distance;
            }
            lastPoint = point;
        }

        for (int i = 0; i < points.size(); i++) {
            Point point = points.get(i);
            double distance = DistanceUtil.pointToPointDistance(fromX, fromY, point);
            if (distance < min) {
                min = distance;
            }
        }

        return min;
    }

    public void setDrawNodes(boolean drawNodes) {
        this.drawNodes = drawNodes;
    }

    public RandomAccessCurve getPoints() {
        return points;
    }

    public CurveFigure copy(RandomAccessCurve points) {
        CurveFigure polyLineFigure = new CurveFigure(points);
        polyLineFigure.setCategory(category);
        return polyLineFigure;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
