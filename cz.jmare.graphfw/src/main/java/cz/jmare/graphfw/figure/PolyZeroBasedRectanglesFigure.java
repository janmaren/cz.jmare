package cz.jmare.graphfw.figure;

import java.util.List;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;

public class PolyZeroBasedRectanglesFigure implements Figure, Category {
    public List<Point> points;

    public String category;

    protected double width = 0;

    protected int lineWidth = 1;

    protected boolean fullFilled;

    protected int alpha = 255;

    /**
     * By default the point is considered to be middle top but enabling this it'll be left top.
     * Has sense onlu when width>0
     */
    protected boolean pointIsLeftTop;

    private int lScreenX0, lScreenY, lScreenY2;

    private String name;

    int lastX = -1;
    double maxYForLastX = -1;

    public PolyZeroBasedRectanglesFigure(List<Point> points) {
        super();
        this.points = points;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        if (points.size() == 0) {
            return;
        }
        gc.setForeground(paintContext.getColor());
        if (lineWidth != 1) {
            gc.setLineWidth(lineWidth);
        }

        Integer origAlpha = null;
        if (alpha < 255) {
            origAlpha = gc.getAlpha();
            gc.setAlpha(alpha);
        }

        Color originBackground = null;
        if (fullFilled) {
            originBackground = gc.getBackground();
            gc.setBackground(paintContext.getColor());
        }

        lScreenX0 = Integer.MAX_VALUE;
        lScreenY = Integer.MAX_VALUE;
        lScreenY2 = Integer.MAX_VALUE;
        lastX = -1;
        maxYForLastX = -1;
        for (int i = 0; i < points.size(); i++) {
            Point point = points.get(i);
            boolean firstPoint = i == 0;
            paintPoint(gc, coordState, point, firstPoint);
        }

        if (originBackground != null) {
            gc.setBackground(originBackground);
        }
        if (origAlpha != null) {
            gc.setAlpha(origAlpha);
        }
        if (lineWidth != 1) {
            gc.setLineWidth(1);
        }
    }

    protected void paintPoint(GC gc, CoordState coordState, Point point, boolean firstPoint) {
        int screenY = coordState.getScreenY(0);
        double halfWidth = width / 2.0;
        double x0, x2;
        if (pointIsLeftTop) {
            x0 = point.x;
            x2 = point.x + width;
        } else {
            x0 = point.x - halfWidth;
            x2 = point.x + halfWidth;
        }
        int screenX0 = coordState.getScreenX(x0);
        int screenX2 = coordState.getScreenX(x2);
        int screenY2 = coordState.getScreenY(point.y);

        if (lastX == screenX0) {
            if (maxYForLastX >= point.y) {
                return;
            }
            maxYForLastX = point.y;
        } else {
           maxYForLastX = point.y;
           lastX = screenX0;
        }

        int topLineLength = screenX2 - screenX0;
        if (width == 0 || topLineLength == 0) {
            if (screenX0 != lScreenX0 || screenY != lScreenY || screenY2 != lScreenY2) {
                gc.drawLine(screenX0, screenY, screenX0, screenY2);
                lScreenX0 = screenX0;
                lScreenY = screenY;
                lScreenY2 = screenY2;
            }
        } else {
            if (fullFilled) {
                gc.fillRectangle(screenX0, screenY, topLineLength, screenY2 - screenY);
            } else {
                gc.drawRectangle(screenX0, screenY, topLineLength, screenY2 - screenY);
            }
        }
    }

    @Override
    public TwoPoints getExtent() {
        if (points.size() == 0) {
            return null;
        }
        TwoPoints coordsPair = new TwoPoints();
        Point point = points.get(0);
        coordsPair.x1 = point.x - width;
        coordsPair.y1 = point.y;
        coordsPair.x2 = point.x + width;
        coordsPair.y2 = point.y;

        for (int i = 1; i < points.size(); i++) {
            point = points.get(i);
            if (point.x - width < coordsPair.x1) {
                coordsPair.x1 = point.x - width;
            }
            if (point.y < coordsPair.y1) {
                coordsPair.y1 = point.y;
            }
            if (point.x + width> coordsPair.x2) {
                coordsPair.x2 = point.x + width;
            }
            if (point.y > coordsPair.y2) {
                coordsPair.y2 = point.y;
            }
        }

        return coordsPair;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setFullFilled(boolean fullFilled) {
        this.fullFilled = fullFilled;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setPointIsLeftTop(boolean pointIsLeftTop) {
        this.pointIsLeftTop = pointIsLeftTop;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return Double.MAX_VALUE;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
