package cz.jmare.graphfw.figure;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.geometry.relation.DistanceUtil;

public class PointFigure implements Figure, Category {
    public double x;

    public double y;

    public String category;

    protected int circleRadius = 3;

    protected int circleDiameter = circleRadius * 2;

    protected boolean fullFilled;

    private boolean showIndex;

    private Integer index;

    private String name;

    public PointFigure(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public PointFigure(Point point) {
        super();
        this.x = point.x;
        this.y = point.y;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));
        gc.setForeground(paintContext.getColor());

        int screenX = coordState.getScreenX(x);
        int screenY = coordState.getScreenY(y);
        if (circleRadius == 0) {
            gc.drawPoint(screenX, screenY);
        } else {
            if (fullFilled) {
                Color origBackgroung = gc.getBackground();
                gc.setBackground(gc.getForeground());
                gc.fillOval(screenX - circleRadius, screenY - circleRadius, circleDiameter, circleDiameter);
                gc.setBackground(origBackgroung);
            } else {
                gc.drawOval(screenX - circleRadius, screenY - circleRadius, circleDiameter, circleDiameter);
            }
        }
        if (showIndex && index != null) {
            gc.setLineWidth((int) (2 * paintContext.getLineWidthFactor()));
            gc.drawText(String.valueOf(index), screenX + 4, screenY, SWT.DRAW_TRANSPARENT);
        }
    }

    @Override
    public TwoPoints getExtent() {
        TwoPoints coordsPair = new TwoPoints();
        coordsPair.x1 = x;
        coordsPair.x2 = x;
        coordsPair.y1 = y;
        coordsPair.y2 = y;
        return coordsPair;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);
        if (x < canvasX0 || x >= canvasX1 || y < canvasY0 || y >= canvasY1) {
            return null;
        }
        return DistanceUtil.pointToPointDistance(fromX, fromY, toPoint());
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    public void setCircleRadius(int circleRadius) {
        this.circleRadius = circleRadius;
        this.circleDiameter = 2 * circleRadius;
    }

    @Override
    public String toString() {
        return "PointFigure [x=" + x + ", y=" + y + ", category=" + category + "]";
    }

    public void setFullFilled(boolean fullFilled) {
        this.fullFilled = fullFilled;
    }

    public void setShowIndex(boolean showIndex) {
        this.showIndex = showIndex;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Point toPoint() {
        return new Point(x, y);
    }

    public PointFigure copy(Point point) {
        PointFigure pointFigure = new PointFigure(point);
        pointFigure.setCategory(category);
        return pointFigure;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
