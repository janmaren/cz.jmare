package cz.jmare.graphfw.figure;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.geometry.relation.DistanceUtil;

public class LineSegmentFigure implements Figure, Category {
    public double x1;

    public double y1;

    public double x2;

    public double y2;

    public String category;

    private String name;

    public LineSegmentFigure(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public LineSegmentFigure(Point point1, Point point2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    @Override
    public String toString() {
        return "LineSegment [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + ", category=" + category + "]";
    }


    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {

        int screenX1; // = coordState.getScreenX(x1);
        int screenY1; // = coordState.getScreenY(y1);
        int screenX2; // = coordState.getScreenX(x2);
        int screenY2; // = coordState.getScreenY(y2);

        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);

        LineSegmentDouble lineSegmentCrop = CropWindowDoubleFunctional.crop(new LineSegmentDouble(x1, y1, x2, y2), canvasX0, canvasY0, canvasX1, canvasY1);
        if (lineSegmentCrop != null) {
            gc.setForeground(paintContext.getColor());
            gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));

            screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
            screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
            screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
            screenY2 = coordState.getScreenY(lineSegmentCrop.y2);

            if (x1 >= canvasX0 && x1 < canvasX1 && y1 >= canvasY0 && y1 < canvasY1) {
                gc.drawRectangle(screenX1 - 2, screenY1 - 2, 4, 4);
            }
            if (x2 >= canvasX0 && x2 < canvasX1 && y2 >= canvasY0 && y2 < canvasY1) {
                gc.drawRectangle(screenX2 - 2, screenY2 - 2, 4, 4);
            }
            gc.drawLine(screenX1, screenY1, screenX2, screenY2);
            gc.setLineWidth(1);
        }
    }

    @Override
    public TwoPoints getExtent() {
        TwoPoints coordsPair = new TwoPoints();
        if (x1 < x2) {
            coordsPair.x1 = x1;
            coordsPair.x2 = x2;
        } else {
            coordsPair.x1 = x2;
            coordsPair.x2 = x1;
        }
        if (y1 < y2) {
            coordsPair.y1 = y1;
            coordsPair.y2 = y2;
        } else {
            coordsPair.y1 = y2;
            coordsPair.y2 = y1;
        }
        return coordsPair;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return DistanceUtil.pointToLineSegmentDistance(fromX, fromY, toLineSegment());
    }

    public LineSegmentDouble toLineSegment() {
        return new LineSegmentDouble(x1, y1, x2, y2);
    }

    public LineSegmentFigure copy(Point point1, Point point2) {
        LineSegmentFigure lineSegmentFigure = new LineSegmentFigure(point1.x, point1.y, point2.x, point2.y);
        lineSegmentFigure.setCategory(category);
        return lineSegmentFigure;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
