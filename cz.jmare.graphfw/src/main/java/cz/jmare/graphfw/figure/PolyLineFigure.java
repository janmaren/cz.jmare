package cz.jmare.graphfw.figure;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.geometry.relation.DistanceUtil;

/**
 * Use {@link CurveFigure}
 */
@Deprecated
public class PolyLineFigure implements Figure, Category {
    public List<Point> points;

    public String category;

    public boolean drawNodes;

    public boolean equiDistant;

    private String name;

    public PolyLineFigure(List<Point> points) {
        super();
        this.points = points;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        if (points.size() < 2) {
            return;
        }
        gc.setForeground(paintContext.getColor());

        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);

        Point lastPoint = points.get(0);

        gc.setForeground(paintContext.getColor());
        gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));

        int inc = 1;
        if (equiDistant) {
            double pixelDistance = coordState.toPixelsX((points.get(1).x - points.get(0).x) * 10000) / 10000.0;
            if (pixelDistance != 0) {
                inc = (int) (1.0 / pixelDistance) / 2;
                if (inc == 0) {
                    inc = 1;
                }
            } else {
                inc = 10000;
            }
        }
        for (int i = 1; i < points.size(); i += inc) {
            Point point = points.get(i);
            LineSegmentDouble lineSegmentCrop = CropWindowDoubleFunctional.crop(new LineSegmentDouble(lastPoint.x, lastPoint.y, point.x, point.y), canvasX0, canvasY0, canvasX1, canvasY1);
            if (lineSegmentCrop != null) {

                int screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
                int screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
                int screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
                int screenY2 = coordState.getScreenY(lineSegmentCrop.y2);

                if (lastPoint.x >= canvasX0 && lastPoint.x < canvasX1 && lastPoint.y >= canvasY0 && lastPoint.y < canvasY1 && drawNodes) {
                    gc.drawRectangle(screenX1 - 2, screenY1 - 2, 4, 4);
                }
                if (point.x >= canvasX0 && point.x < canvasX1 && point.y >= canvasY0 && point.y < canvasY1 && drawNodes) {
                    gc.drawRectangle(screenX2 - 2, screenY2 - 2, 4, 4);
                }
                gc.drawLine(screenX1, screenY1, screenX2, screenY2);
            }
            lastPoint = point;
        }
    }

    @Override
    public TwoPoints getExtent() {
        if (points.size() == 0) {
            return null;
        }
        TwoPoints coordsPair = new TwoPoints();
        Point point = points.get(0);
        coordsPair.x1 = point.x;
        coordsPair.y1 = point.y;
        coordsPair.x2 = point.x;
        coordsPair.y2 = point.y;

        for (int i = 1; i < points.size(); i++) {
            point = points.get(i);
            if (point.x < coordsPair.x1) {
                coordsPair.x1 = point.x;
            }
            if (point.y < coordsPair.y1) {
                coordsPair.y1 = point.y;
            }
            if (point.x > coordsPair.x2) {
                coordsPair.x2 = point.x;
            }
            if (point.y > coordsPair.y2) {
                coordsPair.y2 = point.y;
            }
        }

        return coordsPair;
    }

    public void setDrawNodes(boolean drawNodes) {
        this.drawNodes = drawNodes;
    }

    public List<Point> getPoints() {
        return points;
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        double min = Double.MAX_VALUE;
        Point lastPoint = points.get(0);
        for (int i = 1; i < points.size(); i++) {
            Point point = points.get(i);
            double distance = DistanceUtil.pointToLineSegmentDistance(fromX, fromY, new LineSegmentDouble(lastPoint.x, lastPoint.y, point.x, point.y));
            if (distance < min) {
                min = distance;
            }
            lastPoint = point;
        }

        for (int i = 0; i < points.size(); i++) {
            Point point = points.get(i);
            double distance = DistanceUtil.pointToPointDistance(fromX, fromY, point);
            if (distance < min) {
                min = distance;
            }
        }

        return min;
    }

    public PolyLineFigure copy(List<Point> points) {
        PolyLineFigure polyLineFigure = new PolyLineFigure(points);
        polyLineFigure.setCategory(category);
        polyLineFigure.equiDistant = equiDistant;
        return polyLineFigure;
    }

    public List<Point> toPoints() {
        return new ArrayList<Point>(points);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
