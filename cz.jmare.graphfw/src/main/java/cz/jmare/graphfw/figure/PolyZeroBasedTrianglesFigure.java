package cz.jmare.graphfw.figure;

import java.util.List;

import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;

public class PolyZeroBasedTrianglesFigure extends PolyZeroBasedRectanglesFigure {

    public PolyZeroBasedTrianglesFigure(List<Point> points) {
        super(points);
    }

    @Override
    protected void paintPoint(GC gc, CoordState coordState, Point point, boolean firstPoint) {
        double halfWidth = width / 2.0;
        if (fullFilled) {
            if (firstPoint) {
                // TODO:
                LineSegmentDouble crop1 = crop(gc, coordState, point.x, 0, point.x, point.y);
                LineSegmentDouble crop2 = crop(gc, coordState, point.x + halfWidth, 0, point.x, point.y);
                if (crop1 != null && crop2 != null) {
                    int[] points = new int[] { coordState.getScreenX(crop1.x1), coordState.getScreenY(crop1.y1),
                            coordState.getScreenX(crop1.x2), coordState.getScreenY(crop1.y2),
                            coordState.getScreenX(crop2.x2), coordState.getScreenY(crop2.y2),
                            coordState.getScreenX(crop2.x1), coordState.getScreenY(crop2.y1),coordState.getScreenX(crop2.x1), coordState.getScreenY(0)
                            };
                    gc.fillPolygon(points);
                } else if (crop1 == null && crop2 != null) {
                    int[] points = new int[] { 0, coordState.getScreenY(crop2.y1),
                            0, coordState.getScreenY(crop2.y2),
                            coordState.getScreenX(crop2.x2), coordState.getScreenY(crop2.y2),
                            coordState.getScreenX(crop2.x1), coordState.getScreenY(crop2.y1),
                            };
                    gc.fillPolygon(points);
                }
            }
        } else {
            if (!firstPoint) {
                LineSegmentDouble crop = crop(gc, coordState, point.x - halfWidth, 0, point.x, point.y);
                if (crop != null) {
                    gc.drawLine(coordState.getScreenX(crop.x1), coordState.getScreenY(crop.y1), coordState.getScreenX(crop.x2), coordState.getScreenY(crop.y2));
                }
            }
            LineSegmentDouble crop = crop(gc, coordState, point.x + halfWidth, 0, point.x, point.y);
            if (crop != null) {
                gc.drawLine(coordState.getScreenX(crop.x1), coordState.getScreenY(crop.y1), coordState.getScreenX(crop.x2), coordState.getScreenY(crop.y2));
            }
        }
    }

    private LineSegmentDouble crop(GC gc, CoordState coordState, double x1, double y1, double x2, double y2) {
        double canvasX0 = coordState.getCoordX(0);
        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        double canvasY1 = coordState.getCoordY(0);
        return CropWindowDoubleFunctional.crop(new LineSegmentDouble(x1, y1, x2, y2), canvasX0, canvasY0, canvasX1, canvasY1);
    }

}
