package cz.jmare.graphfw.figure;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.math.geometry.entity.TwoPoints;

public class HorizontalLineFigure implements Figure, Category {
    public double y;

    public String label;

    private String category;

    private int labelPosition;

    private Color color;

    private int lineWidth = 1;

    private int alpha = 255;

    private String name;

    public HorizontalLineFigure(double y) {
        this.y = y;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        gc.setForeground(color != null ? color : paintContext.getColor());
        gc.setLineWidth((int) (lineWidth * paintContext.getLineWidthFactor()));
        gc.setAlpha(alpha);
        int osaY = coordState.getScreenY(y);
        gc.drawLine(0, osaY, coordState.getScreenWidth(), osaY);
        gc.setAlpha(255);
        if (label != null) {
            if (labelPosition == SWT.TOP) {
                int textHeight = gc.stringExtent(label).y;
                gc.drawText(label, 0, osaY - textHeight, true);
            } else {
                gc.drawText(label, 0, osaY + 2, true);
            }
        }
    }

    @Override
    public TwoPoints getExtent() {
        return new TwoPoints(-Double.MAX_VALUE, y, Double.MAX_VALUE, y);
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return Math.abs(fromY - y);
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setLabelPosition(int labelPosition) {
        this.labelPosition = labelPosition;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
