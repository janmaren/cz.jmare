package cz.jmare.graphfw.figure;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.graphfw.math.CropWindowDoubleFunctional;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.TwoPoints;

public class FunctionFigure implements Figure, Category {
    public String category;
    private Supplier<Double> function;
    private Consumer<Double> numberConsumer;
    private String name;
    private static final double EPSILON  = 1E-8;

    public FunctionFigure(Supplier<Double> function, Consumer<Double> numberConsumer) {
        super();
        this.function = function;
        this.numberConsumer = numberConsumer;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        gc.setLineWidth((int) (1 * paintContext.getLineWidthFactor()));
        gc.setForeground(paintContext.getColor());

        double canvasY0 = coordState.getCoordY(coordState.getScreenHeight());
        double canvasY1 = coordState.getCoordY(0);
        double canvasX0 = coordState.getCoordX(0);
        double canvasX1 = coordState.getCoordX(coordState.getScreenWidth());
        Double lastX = null, lastY = null;
        int lastSlope = 0;
        for (int screenX = 0; screenX < coordState.getScreenWidth(); screenX++) {
            double coordX = coordState.getCoordX(screenX);
            numberConsumer.accept(coordX);
            Double coordY = function.get();
            if (!Double.isFinite(coordY)) {
                lastX = null;
                lastY = null;
                continue;
            }
            int screenY = coordState.getScreenY(coordY);
            int slope = slope(coordX, coordY);
            if (lastX != null) {
                boolean changedType = false;
                if (lastSlope == -1 && lastY > coordY  && slope != 1 || lastSlope == 1 && lastY < coordY && slope != -1 || (slope == 0 && lastSlope == 0 && Math.abs(lastY - coordY) > EPSILON)) {
                    gc.setLineStyle(SWT.LINE_DOT);
                    gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
                    changedType = true;
                }
                LineSegmentDouble lineSegmentCrop = CropWindowDoubleFunctional.crop(new LineSegmentDouble(lastX, lastY, coordX, coordY), canvasX0,
                        canvasY0, canvasX1, canvasY1);
                if (lineSegmentCrop != null) {
                    int screenX1 = coordState.getScreenX(lineSegmentCrop.x1);
                    int screenY1 = coordState.getScreenY(lineSegmentCrop.y1);
                    int screenX2 = coordState.getScreenX(lineSegmentCrop.x2);
                    int screenY2 = coordState.getScreenY(lineSegmentCrop.y2);
                    gc.drawLine(screenX1, screenY1, screenX2, screenY2);
                }
                if (changedType) {
                    gc.setLineStyle(SWT.LINE_SOLID);
                    gc.setForeground(paintContext.getColor());
                }
            } else {
                if (coordY >= canvasY0 && coordY < canvasY1) {
                    gc.drawPoint(screenX, screenY);
                }
            }

            lastX = coordX;
            lastY = coordY;
            lastSlope = slope;
        }
    }

    /**
     * -1 is ascending left to right, +1 is descending left to right, 0 is parallel with x axe
     * @param x
     * @param y
     * @return
     */
    public int slope(double x, double y) {
        numberConsumer.accept(x - EPSILON);
        double epsValue = function.get();
        if (epsValue < y) {
            return -1;
        }
        if (epsValue > y) {
            return 1;
        }
        return 0;
    }

    @Override
    public TwoPoints getExtent() {
        numberConsumer.accept(0d);
        Double coordY = function.get();
        if (coordY == null || !Double.isFinite(coordY)) {
            return null;
        }
        return new TwoPoints(0d - 50, coordY - 50, 0d + 50, coordY + 50);
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        double distance = Double.MAX_VALUE;
        for (int screenX = 0; screenX < coordState.getScreenWidth(); screenX++) {
            double coordX = coordState.getCoordX(screenX);
            numberConsumer.accept(coordX);
            Double coordY = function.get();
            if (!Double.isFinite(coordY)) {
                continue;
            }
            double d = Math.sqrt((coordX - fromX) * (coordX - fromX) + (coordY - fromY) * (coordY - fromY));
            if (d < distance) {
                distance = d;
            }
        }
        return distance;
    }

    public FunctionFigure copy(Supplier<Double> function, Consumer<Double> numberConsumer) {
        FunctionFigure functionFigure = new FunctionFigure(function, numberConsumer);
        functionFigure.setCategory(category);
        return functionFigure;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
