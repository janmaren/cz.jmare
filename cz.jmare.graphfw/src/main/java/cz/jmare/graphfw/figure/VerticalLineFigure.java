package cz.jmare.graphfw.figure;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.figurecollector.Category;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.PaintContext;
import cz.jmare.math.geometry.entity.TwoPoints;

public class VerticalLineFigure implements Figure, Category {
    public double x;

    public String label;

    private String category;

    private int labelPosition;

    private Color color;

    private int lineWidth = 1;

    private int alpha = 150;

    private String name;

    public VerticalLineFigure(double x) {
        this.x = x;
    }

    @Override
    public void paint(GC gc, CoordState coordState, PaintContext paintContext) {
        gc.setForeground(getColor() != null ? getColor() : paintContext.getColor());
        gc.setLineWidth((int) (lineWidth * paintContext.getLineWidthFactor()));
        gc.setAlpha(alpha);
        int osaX = coordState.getScreenX(x);
        gc.drawLine(osaX, 0, osaX, coordState.getScreenHeight());
        gc.setAlpha(255);
        if (label != null) {
            Point stringExtent = gc.stringExtent(label);
            if (labelPosition == SWT.LEFT) {
                gc.drawText(label, osaX - stringExtent.x, coordState.getScreenHeight() - stringExtent.y, true);
            } else {
                gc.drawText(label, osaX + 3, coordState.getScreenHeight() - stringExtent.y, true);
            }
        }
    }

    @Override
    public TwoPoints getExtent() {
        return new TwoPoints(x, -Double.MAX_VALUE, x, Double.MAX_VALUE);
    }

    @Override
    public Double calculateDistance(CoordState coordState, double fromX, double fromY) {
        return Math.abs(fromX - x);
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setLabelPosition(int labelPosition) {
        this.labelPosition = labelPosition;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
