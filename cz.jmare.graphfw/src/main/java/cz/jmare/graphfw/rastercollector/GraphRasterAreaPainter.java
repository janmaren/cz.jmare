package cz.jmare.graphfw.rastercollector;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;
import cz.jmare.graphfw.color.ColorProvider;
import cz.jmare.graphfw.color.ColorMapUtil;

public class GraphRasterAreaPainter implements RasterAreaPainter {
    private List<DoubleBiFunction> doubleBiFunctions = new ArrayList<>();

    private ColorProvider<Double> colorProvider;

    private int areaPixelRadius = 1;

    private int alpha = 100;

    public GraphRasterAreaPainter() {
        colorProvider = ColorMapUtil.getDefaultAreaRangeColorProvider();
    }

    @Override
    public void paint(GC gc, CoordState coordState) {
        int origAlpha = gc.getAlpha();
        gc.setAlpha(alpha);
        int squareSize = 2 * areaPixelRadius + 1;
        long start = System.currentTimeMillis();
        for (DoubleBiFunction doubleBiFunction : doubleBiFunctions) {
            for (int i = areaPixelRadius; i < coordState.getScreenHeight() + areaPixelRadius; i += squareSize) {
                for (int j = areaPixelRadius; j < coordState.getScreenWidth() + areaPixelRadius; j += squareSize) {
                    double coordX = coordState.getCoordX(j);
                    double coordY = coordState.getCoordY(i);
                    double realValue = doubleBiFunction.apply(coordX, coordY);

                    Color color = colorProvider.getColor(realValue);
                    gc.setBackground(color);
                    gc.fillRectangle(j - areaPixelRadius, i - areaPixelRadius, squareSize, squareSize);
                }
            }
        }
        long end = System.currentTimeMillis();
        long duration = end - start;
        if (duration > 300) {
            areaPixelRadius ++;
        }
        if (duration < 50 && areaPixelRadius > 0) {
            areaPixelRadius--;
        }

        gc.setAlpha(origAlpha);
    }

    public void addDoubleBiFunction(DoubleBiFunction doubleBiFunction) {
        doubleBiFunctions.add(doubleBiFunction);
    }

    public void setDoubleBiFunction(DoubleBiFunction doubleBiFunction) {
        doubleBiFunctions.clear();
        if (doubleBiFunction != null) {
            doubleBiFunctions.add(doubleBiFunction);
        }
    }

    public void setAreaPixelRadius(int areaPixelRadius) {
        this.areaPixelRadius = areaPixelRadius;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setColorProvider(ColorProvider<Double> colorProvider) {
        this.colorProvider = colorProvider;
    }

    @Override
    public void showClick(double x, double y, int stateMask) {
    }

    @Override
    public void coordsChanged(double x, double y, int stateMask) {
    }
}
