package cz.jmare.graphfw.rastercollector;

public interface DoubleBiFunction {
    double apply(Double x, Double y);
}
