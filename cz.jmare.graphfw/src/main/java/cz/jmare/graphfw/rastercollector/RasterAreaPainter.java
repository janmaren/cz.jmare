package cz.jmare.graphfw.rastercollector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;

import cz.jmare.graphfw.CoordState;

public interface RasterAreaPainter {
    void paint(GC gc, CoordState coordState);

    /**
     * Return true when figure is complete
     * @param x
     * @param y
     * @param stateMask use SWT constants like {@link SWT#SHIFT}, {@link SWT#CONTROL}, {@link SWT#ALT} or {@link SWT#ALT_GR}
     * to detect other keys use during clicking
     */
    void showClick(double x, double y, int stateMask);

    /**
     * Current position after cursor has moved
     * @param x
     * @param y
     * @param stateMask
     */
    void coordsChanged(double x, double y, int stateMask);
}
