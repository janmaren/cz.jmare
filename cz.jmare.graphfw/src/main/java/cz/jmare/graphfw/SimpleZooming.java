package cz.jmare.graphfw;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SimpleZooming implements Zooming {
    private double step = 1.5;

    private double stepReverse = 1 / step;

    public SimpleZooming(double step) {
        super();
        this.step = step;
        this.stepReverse = 1 / this.step;
    }

    public SimpleZooming() {
        super();
    }

    @Override
    public double zoomIn(double origZoom) {
        if (origZoom >= 1.0) {
            int floor = (int) Math.round(log(origZoom, step));
            double result = Math.pow(step, floor + 1);
            int scale = result < 1 ? 2 : 1;

            BigDecimal bd = new BigDecimal(result).setScale(scale, RoundingMode.CEILING);
            double val2 = bd.doubleValue();
            return val2;
        } else {
            int floor = (int) Math.round(log(origZoom, stepReverse));
            if (floor == 0) {
                return 1.0;
            }
            double result = Math.pow(stepReverse, floor - 1);
            int orderOfMagnitude = getOrderOfMagnitude(result) * -1;
            BigDecimal bd = new BigDecimal(result).setScale(orderOfMagnitude + 1, RoundingMode.CEILING);
            double val2 = bd.doubleValue();
            return val2;
        }
    }

    @Override
    public double zoomOut(double origZoom) {
        if (origZoom >= 1.0) {
            int floor = (int) Math.round(log(origZoom, step));
            double result = Math.pow(step, floor - 1);
            int scale = result < 1 ? 2 : 1;
            BigDecimal bd = new BigDecimal(result).setScale(scale, RoundingMode.CEILING);
            double val2 = bd.doubleValue();
            return val2;
        } else {
            int floor = (int) Math.round(log(origZoom, stepReverse));
            if (floor == 0) {
                return 1.0;
            }
            double result = Math.pow(stepReverse, floor + 1);
            int orderOfMagnitude = getOrderOfMagnitude(result) * -1;
            BigDecimal bd = new BigDecimal(result).setScale(orderOfMagnitude + 1, RoundingMode.CEILING);
            double val2 = bd.doubleValue();
            return val2;
        }
    }

    public static double log(double x, double base) {
        return (Math.log(x) / Math.log(base));
    }

    public static int getOrderOfMagnitude(double number) {
        int order = (int) Math.floor(Math.log10(number < 0 ? -number : number));
        return order;
    }

    public static void main(String[] args) {
        SimpleZooming simpleZooming = new SimpleZooming();

        double zoom = 0.1;
        while (zoom < 20) {
            System.out.println(zoom);
            double origZoom = zoom;
            zoom = simpleZooming.zoomIn(zoom);
            if (origZoom == zoom) {
                throw new IllegalStateException("bad zoom in " + zoom);
            }
        }
        System.out.println("===========");
        zoom = 18;
        while (zoom >= 0.1) {
            System.out.println(zoom);
            double origZoom = zoom;
            zoom = simpleZooming.zoomOut(zoom);
            if (origZoom == zoom) {
                throw new IllegalStateException("bad zoom in " + zoom);
            }
        }
    }
}
