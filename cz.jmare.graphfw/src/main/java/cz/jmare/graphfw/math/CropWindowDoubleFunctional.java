package cz.jmare.graphfw.math;

import cz.jmare.math.geometry.entity.LineDoubleCrossableSegment;
import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;

public class CropWindowDoubleFunctional {
    /**
     * Return segment croped by window or null when segment doesn't go through window
     * @param segment
     * @return
     */
    public static LineSegmentDouble crop(LineSegmentDouble segment, double x0, double y0, double x1, double y1) {
        boolean pointIn1 = pointIn(segment.x1, segment.y1, x0, y0, x1, y1);
        boolean pointIn2 = pointIn(segment.x2, segment.y2, x0, y0, x1, y1);
        if (pointIn1 || pointIn2) {
            if (pointIn1 && pointIn2) {
                // no crosspoint, inside
                return segment;
            }
            // 1 crosspoint may exist
            LineDoubleCrossableSegment crossableSegment = new LineDoubleCrossableSegment(segment);
            LineDoubleCrossableSegment bottom = new LineDoubleCrossableSegment(x0, y0, x1, y0);
            Point crossection = bottom.crossection(crossableSegment);
            if (crossection == null) {
                LineDoubleCrossableSegment right = new LineDoubleCrossableSegment(x1, y0, x1, y1);
                crossection = right.crossection(crossableSegment);
                if (crossection == null) {
                    LineDoubleCrossableSegment top = new LineDoubleCrossableSegment(x0, y1, x1, y1);
                    crossection = top.crossection(crossableSegment);
                    if (crossection == null) {
                        LineDoubleCrossableSegment left = new LineDoubleCrossableSegment(x0, y0, x0, y1);
                        crossection = left.crossection(crossableSegment);
                        if (crossection == null) {
                            throw new IllegalStateException("Expected one crossection for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but no found");
                        }
                    }
                }
            }

            if (pointIn1) {
                return new LineSegmentDouble(segment.x1, segment.y1, crossection.x, crossection.y);
            } else {
                return new LineSegmentDouble(segment.x2, segment.y2, crossection.x, crossection.y);
            }
        } else {
            LineDoubleCrossableSegment crossableSegment = new LineDoubleCrossableSegment(segment);
            Point cross1 = null;
            LineDoubleCrossableSegment bottom = new LineDoubleCrossableSegment(x0, y0, x1, y0);
            cross1 = bottom.crossection(crossableSegment);
            LineDoubleCrossableSegment right = new LineDoubleCrossableSegment(x1, y0, x1, y1);
            Point crossection = right.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            LineDoubleCrossableSegment top = new LineDoubleCrossableSegment(x0, y1, x1, y1);
            crossection = top.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            LineDoubleCrossableSegment left = new LineDoubleCrossableSegment(x0, y0, x0, y1);
            crossection = left.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            if (cross1 != null) {
                throw new IllegalStateException("Expected 2 crossections or none for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but found 1: " + cross1);
            }

            // segment doesn't go through window
            return null;
        }
    }

    public static LineSegmentDouble crop(StraightLine line, double x0, double y0, double x1, double y1) {
        Point cross1 = null;
        LineDoubleCrossableSegment bottom = new LineDoubleCrossableSegment(x0, y0, x1, y0);
        cross1 = bottom.crossection(line);
        LineDoubleCrossableSegment right = new LineDoubleCrossableSegment(x1, y0, x1, y1);
        Point crossection = right.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
        LineDoubleCrossableSegment top = new LineDoubleCrossableSegment(x0, y1, x1, y1);
        crossection = top.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
        LineDoubleCrossableSegment left = new LineDoubleCrossableSegment(x0, y0, x0, y1);
        crossection = left.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentDouble(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
//        if (cross1 != null) {
//            throw new IllegalStateException("Expected 2 crossections or none for " + line + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but found 1: " + cross1);
//        }

        // segment doesn't go through window
        return null;
    }

    private static boolean pointIn(double x, double y, double x1, double y1, double x2, double y2) {
        return x >= x1 && x <= x2 && y >= y1 && y <= y2;
    }
}
