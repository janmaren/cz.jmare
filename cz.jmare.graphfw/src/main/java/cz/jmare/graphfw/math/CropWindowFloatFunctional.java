package cz.jmare.graphfw.math;

import cz.jmare.math.geometry.entity.LineFloatCrossableSegment;
import cz.jmare.math.geometry.entity.LineSegmentFloat;
import cz.jmare.math.geometry.entity.PointFloat;
import cz.jmare.math.geometry.entity.StraightFloatLine;

public class CropWindowFloatFunctional {
    /**
     * Return segment croped by window or null when segment doesn't go through window
     * @param segment
     * @return
     */
    public static LineSegmentFloat crop(LineSegmentFloat segment, float x0, float y0, float x1, float y1) {
        boolean pointIn1 = pointIn(segment.x1, segment.y1, x0, y0, x1, y1);
        boolean pointIn2 = pointIn(segment.x2, segment.y2, x0, y0, x1, y1);
        if (pointIn1 || pointIn2) {
            if (pointIn1 && pointIn2) {
                // no crosspoint, inside
                return segment;
            }
            // 1 crosspoint may exist
            LineFloatCrossableSegment crossableSegment = new LineFloatCrossableSegment(segment);
            LineFloatCrossableSegment bottom = new LineFloatCrossableSegment(x0, y0, x1, y0);
            PointFloat crossection = bottom.crossection(crossableSegment);
            if (crossection == null) {
                LineFloatCrossableSegment right = new LineFloatCrossableSegment(x1, y0, x1, y1);
                crossection = right.crossection(crossableSegment);
                if (crossection == null) {
                    LineFloatCrossableSegment top = new LineFloatCrossableSegment(x0, y1, x1, y1);
                    crossection = top.crossection(crossableSegment);
                    if (crossection == null) {
                        LineFloatCrossableSegment left = new LineFloatCrossableSegment(x0, y0, x0, y1);
                        crossection = left.crossection(crossableSegment);
                        if (crossection == null) {
                            throw new IllegalStateException("Expected one crossection for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but no found");
                        }
                    }
                }
            }

            if (pointIn1) {
                return new LineSegmentFloat(segment.x1, segment.y1, crossection.x, crossection.y);
            } else {
                return new LineSegmentFloat(segment.x2, segment.y2, crossection.x, crossection.y);
            }
        } else {
            LineFloatCrossableSegment crossableSegment = new LineFloatCrossableSegment(segment);
            PointFloat cross1 = null;
            LineFloatCrossableSegment bottom = new LineFloatCrossableSegment(x0, y0, x1, y0);
            cross1 = bottom.crossection(crossableSegment);
            LineFloatCrossableSegment right = new LineFloatCrossableSegment(x1, y0, x1, y1);
            PointFloat crossection = right.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            LineFloatCrossableSegment top = new LineFloatCrossableSegment(x0, y1, x1, y1);
            crossection = top.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            LineFloatCrossableSegment left = new LineFloatCrossableSegment(x0, y0, x0, y1);
            crossection = left.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            if (cross1 != null) {
                throw new IllegalStateException("Expected 2 crossections or none for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but found 1: " + cross1);
            }

            // segment doesn't go through window
            return null;
        }
    }

    public static LineSegmentFloat crop(StraightFloatLine line, float x0, float y0, float x1, float y1) {
        PointFloat cross1 = null;
        LineFloatCrossableSegment bottom = new LineFloatCrossableSegment(x0, y0, x1, y0);
        cross1 = bottom.crossection(line);
        LineFloatCrossableSegment right = new LineFloatCrossableSegment(x1, y0, x1, y1);
        PointFloat crossection = right.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
        LineFloatCrossableSegment top = new LineFloatCrossableSegment(x0, y1, x1, y1);
        crossection = top.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
        LineFloatCrossableSegment left = new LineFloatCrossableSegment(x0, y0, x0, y1);
        crossection = left.crossection(line);
        if (crossection != null) {
            if (cross1 == null) {
                cross1 = crossection;
            } else {
                return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
            }
        }
//        if (cross1 != null) {
//            throw new IllegalStateException("Expected 2 crossections or none for " + line + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but found 1: " + cross1);
//        }

        // segment doesn't go through window
        return null;
    }

    private static boolean pointIn(float x, float y, float x1, float y1, float x2, float y2) {
        return x >= x1 && x <= x2 && y >= y1 && y <= y2;
    }
}
