package cz.jmare.graphfw.math;

import cz.jmare.math.geometry.entity.LineFloatCrossableSegment;
import cz.jmare.math.geometry.entity.LineSegmentFloat;
import cz.jmare.math.geometry.entity.PointFloat;

public class CropWindowFloat {
    private LineFloatCrossableSegment bottom;

    private LineFloatCrossableSegment top;

    private LineFloatCrossableSegment left;

    private LineFloatCrossableSegment right;

    public CropWindowFloat(float x0, float y0, float x1, float y1) {
        bottom = new LineFloatCrossableSegment(x0, y0, x1, y0);
        top = new LineFloatCrossableSegment(x0, y1, x1, y1);
        left = new LineFloatCrossableSegment(x0, y0, x0, y1);
        right = new LineFloatCrossableSegment(x1, y0, x1, y1);
    }

    /**
     * Return segment croped by window or null when segment doesn't go through window
     * @param segment
     * @return
     */
    public LineSegmentFloat crop(LineSegmentFloat segment) {
        boolean pointIn1 = pointIn(segment.x1, segment.y1);
        boolean pointIn2 = pointIn(segment.x2, segment.y2);
        if (pointIn1 || pointIn2) {
            if (pointIn1 && pointIn2) {
                // no crosspoint, inside
                return segment;
            }
            // 1 crosspoint may exist
            LineFloatCrossableSegment crossableSegment = new LineFloatCrossableSegment(segment);
            PointFloat crossection = bottom.crossection(crossableSegment);
            if (crossection == null) {
                crossection = right.crossection(crossableSegment);
                if (crossection == null) {
                    crossection = top.crossection(crossableSegment);
                    if (crossection == null) {
                        crossection = left.crossection(crossableSegment);
                        if (crossection == null) {
                            throw new IllegalStateException("Expected one crossection for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but no found");
                        }
                    }
                }
            }

            if (pointIn1) {
                return new LineSegmentFloat(segment.x1, segment.y1, crossection.x, crossection.y);
            } else {
                return new LineSegmentFloat(segment.x2, segment.y2, crossection.x, crossection.y);
            }
        } else {
            LineFloatCrossableSegment crossableSegment = new LineFloatCrossableSegment(segment);
            PointFloat cross1 = null;
            cross1 = bottom.crossection(crossableSegment);
            PointFloat crossection = right.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            crossection = top.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
            crossection = left.crossection(crossableSegment);
            if (crossection != null) {
                if (cross1 == null) {
                    cross1 = crossection;
                } else {
                    return new LineSegmentFloat(cross1.x, cross1.y, crossection.x, crossection.y);
                }
            }
//            if (cross1 != null) {
//                throw new IllegalStateException("Expected 2 crossections or none for " + segment + " in window " + left.x1 + ", " + left.y1 + " " + right.x2 + ", " + right.y2 + " but found 1: " + cross1);
//            }

            // segment doesn't go through window
            return null;
        }
    }

    private boolean pointIn(float x, float y) {
        return x >= bottom.x1 && x <= bottom.x2 && y >= bottom.y1 && y <= top.y2;
    }
    
    public static void main(String[] args) {
        LineSegmentFloat lineSegmentFloat = new LineSegmentFloat(508.71042f, 457.43848f, 490.633f, 473.00006f);
        CropWindowFloat cropWindowFloat = new CropWindowFloat(0.0f, 0.0f, 633.0f, 473.0f);
        LineSegmentFloat crop = cropWindowFloat.crop(lineSegmentFloat);
        System.out.println(crop);
    }
}
