package cz.jmare.graphfw.math;

import java.util.List;

import cz.jmare.math.geometry.entity.LineDoubleCrossableSegment;
import cz.jmare.math.geometry.entity.Point;

public class PointIn {
    /**
     * Return true when point is in polygon
     * @param point
     * @param maxX max value in which polygon is. For example width of screen
     * @param polygon the polygon
     * @return
     */
    public static boolean isPointInPolygon(Point point, double maxX, List<Point> polygon) {
        LineDoubleCrossableSegment baseSegment = new LineDoubleCrossableSegment(point.x, point.y, maxX, point.y);
        Point lastPoint = polygon.get(polygon.size() - 1);
        int number = 0;
        for (Point point2 : polygon) {
            LineDoubleCrossableSegment testSegment = new LineDoubleCrossableSegment(lastPoint.x, lastPoint.y, point2.x, point2.y);
            if (baseSegment.crossection(testSegment) != null) {
                number++;
            }
            lastPoint = point2;
        }
        return number % 2 != 0;
    }

    /**
     * Return true when point is in polygon. The max value is also calculated (less efficient)
     * @param point
     * @param polygon the polygon
     * @return
     */
    public static boolean isPointInPolygon(Point point, List<Point> polygon) {
        double maxX = -Double.MAX_VALUE;
        for (Point point2 : polygon) {
            if (point2.x > maxX) {
                maxX = point2.x;
            }
        }
        return isPointInPolygon(point, maxX + 1, polygon);
    }

    public static void main(String[] args) {
        Point point = new Point(-1.9999999, 0);
        List<Point> poly = List.of(new Point(-1, 1), new Point(1, 1), new Point(1, -1), new Point(-1, -1));
        boolean pointInPolygon = isPointInPolygon(point, poly);
        System.out.println(pointInPolygon);
    }
}
