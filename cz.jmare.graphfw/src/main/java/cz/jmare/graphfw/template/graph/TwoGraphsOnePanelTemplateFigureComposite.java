package cz.jmare.graphfw.template.graph;

import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_EXTENT;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_GRID_INPUT;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_SMALL_ICONS;
import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_ZOOM;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import cz.jmare.graphfw.figurecollector.MixedFigureCollector;
import cz.jmare.graphfw.graphcomposite.GraphBasicToolsFiguresComposite;
import cz.jmare.graphfw.graphcomposite.GraphEditComposite;
import cz.jmare.graphfw.graphcomposite.GraphReadComposite;
import cz.jmare.graphfw.rastercollector.GraphRasterAreaPainter;
import cz.jmare.swt.composite.CompositeUtil;

public abstract class TwoGraphsOnePanelTemplateFigureComposite extends Composite {
    public static ColorRegistry colorReg = JFaceResources.getColorRegistry();

    protected GraphEditComposite mainGraphEditComposite;
    protected GraphReadComposite secondaryViewComposite;

    protected int[] getPanelWeights() {
        return new int[]{75, 25};
    }

    public TwoGraphsOnePanelTemplateFigureComposite(Composite parent, int style) {
        super(parent, style);
        GridLayout layout = new GridLayout();
        layout.marginTop = 0;
        layout.marginBottom = 0;
        setLayout(layout);
        createMenu();

        Composite topBottomParent = new Composite(this, SWT.NONE);
        topBottomParent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        topBottomParent.setLayout(new GridLayout());
        SashForm twoGraphsSash = new SashForm(topBottomParent, SWT.NONE);
        GridLayout gridLayout = new GridLayout(2, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        twoGraphsSash.setLayout(gridLayout);
        GridData sashLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
        twoGraphsSash.setLayoutData(sashLayoutData);

        Composite graphAndOutlineComposite = CompositeUtil.noMarginComposite(twoGraphsSash, 2);
        createMainViewComposite(graphAndOutlineComposite);
        GridData gridDataMain = new GridData(SWT.FILL, SWT.FILL, true, true);
        mainGraphEditComposite.setLayoutData(gridDataMain);

        createOutlineComposite(graphAndOutlineComposite);

        Composite noMarginComposite = CompositeUtil.noMarginComposite(twoGraphsSash, 2);
        twoGraphsSash.setWeights(getPanelWeights());
        Label separator2 = new Label(noMarginComposite, SWT.VERTICAL | SWT.SEPARATOR);
        separator2.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        createSecondaryViewComposite(noMarginComposite);

        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        secondaryViewComposite.setLayoutData(gridData);

        Composite panelComposite = new Composite(topBottomParent, SWT.NONE);
        panelComposite.setLayout(new GridLayout(1, false));

        Label separator = new Label(panelComposite, SWT.HORIZONTAL | SWT.SEPARATOR);
        separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        createPanelContent(panelComposite);
    }

    protected void createMenu() {
    }

    protected void createOutlineComposite(@SuppressWarnings("unused") Composite parentComposite) {
    }


    public void createMainViewComposite(Composite parentComposite) {
        MixedFigureCollector mixedFigureCollector = new MixedFigureCollector();
        mainGraphEditComposite = new GraphBasicToolsFiguresComposite(parentComposite, SWT.NONE, mixedFigureCollector, SHOW_GRID_INPUT, SHOW_EXTENT, SHOW_ZOOM);
        mainGraphEditComposite.setGridStep(1d);
        mainGraphEditComposite.setZoomMultiply(0.075d);
        mainGraphEditComposite.setShowGrid(true);
        GridData layoutData = new GridData();
        layoutData.grabExcessHorizontalSpace = true;
        layoutData.grabExcessVerticalSpace = true;
        layoutData.verticalAlignment = SWT.FILL;
        layoutData.horizontalAlignment = SWT.FILL;
        mainGraphEditComposite.setLayoutData(layoutData);
        GraphRasterAreaPainter graphRasterAreaPainter = new GraphRasterAreaPainter();
        mainGraphEditComposite.setRasterAreaPainter(graphRasterAreaPainter);
    }

    public void createSecondaryViewComposite(Composite parentComposite) {
        secondaryViewComposite = new GraphReadComposite(parentComposite, SWT.NONE, new MixedFigureCollector(), SHOW_SMALL_ICONS, SHOW_GRID_INPUT, SHOW_EXTENT, SHOW_ZOOM);
        secondaryViewComposite.setShowGrid(true);
    }

    public abstract void createPanelContent(Composite panelComposite);
}
