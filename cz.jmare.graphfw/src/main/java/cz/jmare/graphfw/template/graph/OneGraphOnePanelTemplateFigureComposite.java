package cz.jmare.graphfw.template.graph;

import static cz.jmare.graphfw.graphcomposite.GraphConfigConstant.SHOW_GRID_INPUT;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import cz.jmare.graphfw.figurecollector.MixedFigureCollector;
import cz.jmare.graphfw.graphcomposite.GraphEditComposite;
import cz.jmare.swt.composite.CompositeUtil;

public abstract class OneGraphOnePanelTemplateFigureComposite extends Composite {
    protected GraphEditComposite graphEditComposite;

    public OneGraphOnePanelTemplateFigureComposite(Composite parent, int style) {
        super(parent, style);
        setLayout(new GridLayout());

        Composite graphAndOutlineComposite = CompositeUtil.noMarginComposite(this, 2);
        createGraphEditComposite(graphAndOutlineComposite);
        GridData gridDataMain = new GridData(SWT.FILL, SWT.FILL, true, true);
        graphEditComposite.setLayoutData(gridDataMain);
        createOutlineComposite(graphAndOutlineComposite);

        GridData gridDataGraph = new GridData();
        gridDataGraph.heightHint = 5000;
        gridDataGraph.horizontalAlignment = GridData.FILL;
        gridDataGraph.verticalAlignment = GridData.FILL;
        gridDataGraph.grabExcessHorizontalSpace = true;
        gridDataGraph.grabExcessVerticalSpace = true;
        graphAndOutlineComposite.setLayoutData(gridDataGraph);

        Composite panelComposite = new Composite(this, SWT.NONE);
        panelComposite.setLayout(new GridLayout(1, false));

        GridData gridDataPanel = new GridData ();
        gridDataPanel.heightHint = 180;
        gridDataPanel.horizontalAlignment = GridData.FILL;
        gridDataPanel.verticalAlignment = GridData.FILL;
        gridDataPanel.grabExcessHorizontalSpace = true;
        gridDataPanel.grabExcessVerticalSpace = true;
        gridDataPanel.minimumHeight = 180;
        panelComposite.setLayoutData(gridDataPanel);

        createPanelContent(panelComposite);
    }

    public void createGraphEditComposite(Composite parentComposite) {
        graphEditComposite = new GraphEditComposite(parentComposite, SWT.NONE, new MixedFigureCollector(), SHOW_GRID_INPUT);
    }

    public abstract void createPanelContent(Composite panelComposite);


    protected void createOutlineComposite(Composite parentComposite) {
    }
}
