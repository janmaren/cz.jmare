package cz.jmare.graphfw.template.graph;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;

import cz.jmare.config.AppConfig;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.graphfw.color.ColorProvider;
import cz.jmare.graphfw.figurecollector.Figure;
import cz.jmare.graphfw.figurecollector.MixedFigureCollector;
import cz.jmare.graphfw.file.ColorFileUtil;
import cz.jmare.graphfw.file.FigureFileUtil;
import cz.jmare.graphfw.graphcomposite.GraphEditComposite;
import cz.jmare.graphfw.rastercollector.GraphRasterAreaPainter;

public class OutlineComposite extends Composite {

    private MixedFigureCollector mixedFigureCollector;
    private GraphEditComposite mainViewComposite;
    private GraphRasterAreaPainter graphRasterAreaPainter;

    public OutlineComposite(Composite parent, int style, MixedFigureCollector mixedFigureCollector, GraphRasterAreaPainter graphRasterAreaPainter) {
        super(parent, style);
        this.mixedFigureCollector = mixedFigureCollector;
        this.graphRasterAreaPainter = graphRasterAreaPainter;
        GridLayoutFactory.fillDefaults().applyTo(this);
        createPanelContent(this);
    }

    public void createPanelContent(Composite parent) {
        Button newButton = new Button(parent, SWT.NONE);
        newButton.setText("New");
        newButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                mixedFigureCollector.setFigures(new ArrayList<>());
                mainViewComposite.setRasterAreaPainter(null);
                mainViewComposite.apply();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        Button loadButton = new Button(parent, SWT.NONE);
        loadButton.setText("Load");
        loadButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
                fileDialog.setFilterExtensions(new String[] {"*.xml", "*.txt"});
                String filename = fileDialog.open();
                if (filename != null) {
                    if (filename.endsWith(".xml")) {
                        try {
                            List<Figure> figures = FigureFileUtil.loadFiguresByXStream(filename);
                            mixedFigureCollector.setFigures(figures);
                            mainViewComposite.applyExtent();
                        } catch (Exception e) {
                            MessageBox messageBox = new MessageBox(getShell(), SWT.CANCEL | SWT.ICON_ERROR);
                            messageBox.setMessage("File " + filename + " doesn't contain figures - invalid format.\n" + ExceptionMessage.getCombinedMessage(e));
                            messageBox.open();
                        }
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        Button saveButton = new Button(parent, SWT.NONE);
        saveButton.setText("Save");
        saveButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
                fileDialog.setFilterExtensions(new String[] {"*.xml", "*.txt"});
                String filename = fileDialog.open();
                if (filename != null) {
                    try {
                        FigureFileUtil.saveFiguresByXStream(filename, mixedFigureCollector.getFigures());
                    } catch (Exception e) {
                        MessageBox messageBox = new MessageBox(getShell(), SWT.CANCEL | SWT.ICON_ERROR);
                        messageBox.setMessage(ExceptionMessage.getCombinedMessage(e));
                        messageBox.open();
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        Button loadColorsButton = new Button(parent, SWT.NONE);
        loadColorsButton.setText("Load Colors");
        loadColorsButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
                fileDialog.setFilterExtensions(new String[] {"*.xml"});
                String filename = fileDialog.open();
                if (filename != null) {
                    try (InputStream is = new FileInputStream(filename)) {
                        ColorProvider<String> colorProvider = ColorFileUtil.loadColorProvider(is, String.class);
                        mixedFigureCollector.setColorProvider(colorProvider);
                        mainViewComposite.apply();
                    } catch (Exception e) {
                        MessageBox messageBox = new MessageBox(getShell(), SWT.CANCEL | SWT.ICON_ERROR);
                        messageBox.setMessage("File " + filename + " doesn't contain figures - invalid format.\n" + ExceptionMessage.getCombinedMessage(e));
                        messageBox.open();
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        Button loadAreaColorsButton = new Button(parent, SWT.NONE);
        loadAreaColorsButton.setText("Load Raster Colors");
        loadAreaColorsButton.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
                fileDialog.setFilterExtensions(new String[] {"*.xml"});
                fileDialog.setFileName(AppConfig.getInstance().getExistingDataDir().toString());
                String filename = fileDialog.open();
                if (filename != null) {
                    try (InputStream is = new FileInputStream(filename)) {
                        ColorProvider<Double> colorProvider = ColorFileUtil.loadColorProvider(is, Double.class);
                        if (graphRasterAreaPainter != null) {
                            graphRasterAreaPainter.setColorProvider(colorProvider);
                            mainViewComposite.apply();
                        }
                    } catch (Exception e) {
                        MessageBox messageBox = new MessageBox(getShell(), SWT.CANCEL | SWT.ICON_ERROR);
                        messageBox.setMessage("File " + filename + " doesn't contain figures - invalid format.\n" + ExceptionMessage.getCombinedMessage(e));
                        messageBox.open();
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

    }

    public void setGraphViewComposite(GraphEditComposite mainViewComposite) {
        this.mainViewComposite = mainViewComposite;
    }
}
