package cz.jmare.graphfw;

import org.eclipse.swt.widgets.Button;

public interface ToolChangedListener {
    void toolChanged(Button selectedButton);
}
