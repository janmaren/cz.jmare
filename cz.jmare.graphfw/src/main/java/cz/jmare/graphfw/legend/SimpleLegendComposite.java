package cz.jmare.graphfw.legend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import cz.jmare.graphfw.color.ColorProvider;
import cz.jmare.graphfw.figurecollector.IdName;
import cz.jmare.graphfw.graphcomposite.Redrawable;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;

public class SimpleLegendComposite extends Composite implements Legend {
    @SuppressWarnings("rawtypes")
	private ColorProvider colorProvider;

    private Table table;

    private Redrawable redrawable;

    private List<LegendChangedListener> listeners = new ArrayList<>();

    private SelectionChangedListener selectionChangedListener;

    public SimpleLegendComposite(Composite parent, int style, Redrawable redrawable, Integer... settings) {
        this(parent, style, null, redrawable, settings);
    }

    public SimpleLegendComposite(Composite parent, int style, String label, Redrawable redrawable, Integer... settings) {
        super(parent, style);
        this.redrawable = redrawable;
        ImageUtil.put("arrow-up", "/image/arrow-up.png");
        ImageUtil.put("arrow-down", "/image/arrow-down.png");

        setLayout(new GridLayout());

        Composite toolsComposite = createRowCompositeLineIntoGridComposite(this, gd -> {});
        List<Integer> settingsIntegers = Arrays.asList(settings);
        if (label != null) {
            new Label(toolsComposite, SWT.NONE).setText(label);
        }
        Create.imageButton(toolsComposite, ImageUtil.get("arrow-up"), "Move Up", () -> {
            doMoveUp();
        });
        Create.imageButton(toolsComposite, ImageUtil.get("arrow-down"), "Move Down", () -> {
            doMoveDown();
        });

        table = new Table(this, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.FULL_SELECTION);
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                if (event.detail == SWT.CHECK) {
                    redrawable.refreshRedraw();
                }
                notifyListeners();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        table.addListener(SWT.EraseItem, new Listener() {
            @Override
            public void handleEvent(Event event) {
                event.detail &= ~SWT.HOT;
                if ((event.detail & SWT.SELECTED) == 0) return; /// item not selected

                Table table =(Table)event.widget;
                //TableItem item =(TableItem)event.item;
                int clientWidth = table.getClientArea().width;

                GC gc = event.gc;
                //Color oldForeground = gc.getForeground();
                //Color oldBackground = gc.getBackground();

                //gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
                //gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION));
                int origAlpha = gc.getAlpha();
                gc.setAlpha(150);
                gc.fillRectangle(0, event.y, clientWidth, event.height);
                gc.setAlpha(origAlpha);

                //gc.setForeground(oldForeground);
                //gc.setBackground(oldBackground);
                event.detail &= ~SWT.SELECTED;
            }
        });

        TableColumn colorColumn = new TableColumn(table, SWT.CENTER);
        colorColumn.setText("");
        colorColumn.setWidth(50);
        colorColumn.setResizable(false);

        int widthItem = 10;
        if (settingsIntegers.contains(Legend.LEGEND_NAME_WIDTH_LONG)) {
            widthItem = 150;
        } else if (settingsIntegers.contains(Legend.LEGEND_NAME_WIDTH_MEDIUM)) {
            widthItem = 70;
        } else if (settingsIntegers.contains(Legend.LEGEND_NAME_WIDTH_SHORT)) {
            widthItem = 15;
        }
        TableColumn idColumn = new TableColumn(table, SWT.CENTER);
        idColumn.setText(settingsIntegers.contains(Legend.LEGEND_SHOW_NAME_LABEL) ? "Id" : "");
        if (widthItem != -1) {
            idColumn.setWidth(widthItem);
        }
        idColumn.setResizable(true);
        idColumn.setAlignment(SWT.LEFT);

        TableColumn nameColumn = new TableColumn(table, SWT.CENTER);
        nameColumn.setText(settingsIntegers.contains(Legend.LEGEND_SHOW_NAME_LABEL) ? "Name" : "");
        nameColumn.setWidth(250);
        nameColumn.setResizable(true);
        nameColumn.setAlignment(SWT.LEFT);

        table.setHeaderVisible(true);
        GridData gridDataTable = new GridData(SWT.FILL, SWT.FILL, true, true);
        int minHeight = -1;
        if (settingsIntegers.contains(Legend.LEGEND_MIN_HEIGHT_LONG)) {
            minHeight = 250;
        } else if (settingsIntegers.contains(Legend.LEGEND_MIN_HEIGHT_MEDIUM)) {
            minHeight = 125;
        } else if (settingsIntegers.contains(Legend.LEGEND_MIN_HEIGHT_SHORT)) {
            minHeight = 70;
        }
        if (minHeight != -1) {
            gridDataTable.minimumHeight = minHeight;
        }

        int minWidth = -1;
        if (settingsIntegers.contains(Legend.LEGEND_MIN_WIDTH_LONG)) {
            minWidth = 250;
        } else if (settingsIntegers.contains(Legend.LEGEND_MIN_WIDTH_MEDIUM)) {
            minWidth = 125;
        } else if (settingsIntegers.contains(Legend.LEGEND_MIN_WIDTH_SHORT)) {
            minWidth = 70;
        }
        if (minWidth != -1) {
            gridDataTable.minimumWidth = minWidth;
        }
        gridDataTable.grabExcessVerticalSpace = true;
        gridDataTable.verticalAlignment = SWT.FILL;
        table.setLayoutData(gridDataTable);


        table.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event event) {
                int selectionIndex = table.getSelectionIndex();
                if (selectionIndex >= 0) {
                    if ((event.stateMask & SWT.CTRL) != 0) {
                        if (table.isSelected(selectionIndex)) {
                            table.deselect(selectionIndex);
                        } else {
                            table.setSelection(selectionIndex);
                        }
                    } else {
                        table.setSelection(selectionIndex);
                    }
                    if (selectionChangedListener != null) {
                        selectionChangedListener.processSelectedId(getSelectedCategory());
                    }
                    notifyListeners();
                }
            }
        });

        Menu menuTable = new Menu(table);
        table.setMenu(menuTable);

        // Create menu item
        MenuItem miRename = new MenuItem(menuTable, SWT.NONE);
        miRename.setText("Rename");
        miRename.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                if (table.getSelectionCount() <= 0) {
                    event.doit = false;
                    return;
                }
                int index = table.getSelectionIndex();
                TableItem item = table.getItem(index);
                TextIdName idName = (TextIdName) item.getData();
                InputDialog inputDialog = new InputDialog(getShell(), "Rename", "Set new name for " + idName.id, idName.name, new IInputValidator() {
                    @Override
                    public String isValid(String newText) {
                        return null;
                    }
                });
                int open = inputDialog.open();
                if (open == Dialog.OK) {
                    selectionChangedListener.renameCurve(idName.id, inputDialog.getValue());
                    idName.tableItem.setText(2, inputDialog.getValue());
                    idName.name = inputDialog.getValue();
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent event) {
            }
        });
    }

    private void doMoveUp() {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex < 1) {
            return;
        }

        String prevId = null;
        if (selectionIndex >= 2) {
            TableItem prevItem = table.getItem(selectionIndex - 2);
            prevId = ((IdName) prevItem.getData()).id;
        }

        TableItem item = table.getItem(selectionIndex);
        String id = ((IdName) item.getData()).id;

        selectionChangedListener.changeObjectPosition(id, prevId);
    }

    private void doMoveDown() {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex >= table.getItemCount() - 1) {
            return;
        }

        TableItem prevItem = table.getItem(selectionIndex + 1);
        String prevId = ((IdName) prevItem.getData()).id;

        TableItem item = table.getItem(selectionIndex);
        String id = ((IdName) item.getData()).id;

        selectionChangedListener.changeObjectPosition(id, prevId);
    }

    public static Composite createRowCompositeLineIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    @SuppressWarnings("unchecked")
	@Override
    public void setCategories(List<IdName> categories) {
        int selectionIndex = table.getSelectionIndex();
        String selectedId = null;
        if (selectionIndex != -1) {
            TableItem item = table.getItem(selectionIndex);
            selectedId = ((IdName) item.getData()).id;
        }
        List<String> disabledCategories = getDisabledCategories();
        table.removeAll();
        int newSelectionIndex = -1;
        int pos = 0;
        for (IdName idName : categories) {
            TableItem tableItem = new TableItem(table, SWT.NONE);
            tableItem.setText(0, "   ");
            tableItem.setText(1, idName.id);
            tableItem.setData(new TextIdName(idName.id, idName.name, tableItem));
            tableItem.setText(2, idName.name != null ? idName.name : "");
            if (colorProvider != null) {
                tableItem.setBackground(0, colorProvider.getColor(idName.id));
            }
            if (!disabledCategories.contains(idName.id)) {
                tableItem.setChecked(true);
            }
            if (idName.id.equals(selectedId)) {
                newSelectionIndex = pos;
            }
            pos++;
        }
        if (newSelectionIndex != -1) {
            table.select(newSelectionIndex);
        }
        redrawable.refreshRedraw();
        notifyListeners();
    }

    @Override
    public List<String> getCategories() {
        TableItem[] items = table.getItems();
        List<String> categories = Arrays.asList(items).stream().map(i -> i.getText(1)).collect(Collectors.toList());
        return categories;
    }

    @Override
    public List<String> getEnabledCategories() {
        TableItem[] items = table.getItems();
        List<String> categories = Arrays.asList(items).stream().filter(i -> i.getChecked()).map(i -> i.getText(1)).collect(Collectors.toList());
        return categories;
    }

    @Override
    public List<String> getDisabledCategories() {
        TableItem[] items = table.getItems();
        List<String> categories = Arrays.asList(items).stream().filter(i -> !i.getChecked()).map(i -> i.getText(1)).collect(Collectors.toList());
        return categories;
    }

    @Override
    public void setColorProvider(@SuppressWarnings("rawtypes") ColorProvider colorProvider) {
        this.colorProvider = colorProvider;
    }

    public void addListener(LegendChangedListener legendChangedListener) {
        listeners.add(legendChangedListener);
    }

    private void notifyListeners() {
        for (LegendChangedListener legendChangedListener : listeners) {
            legendChangedListener.legendChanged();
        }
    }

    @Override
    public String getSelectedCategory() {
        int selectionIndex = table.getSelectionIndex();
        if (selectionIndex == -1) {
            return null;
        }
        TableItem[] items = table.getItems();
        return items[selectionIndex].getText(1);
    }

    public void setSelectionChangedListener(SelectionChangedListener selectionChangedListener) {
        this.selectionChangedListener = selectionChangedListener;
    }

    class TextIdName extends IdName {
        TableItem tableItem;

        public TextIdName(String id, String name, TableItem tableItem) {
            this.id = id;
            this.name = name;
            this.tableItem = tableItem;
        }
    }
}
