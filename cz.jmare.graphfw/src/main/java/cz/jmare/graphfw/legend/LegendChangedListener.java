package cz.jmare.graphfw.legend;

public interface LegendChangedListener {
    void legendChanged();
}
