package cz.jmare.graphfw.legend;

import java.util.List;

import cz.jmare.graphfw.color.ColorProvider;
import cz.jmare.graphfw.figurecollector.IdName;

public interface Legend {
    void setCategories(List<IdName> categories);

    void setColorProvider(@SuppressWarnings("rawtypes") ColorProvider colorProvider);

    List<String> getCategories();

    List<String> getEnabledCategories();

    List<String> getDisabledCategories();

    String getSelectedCategory();

    static final int LEGEND_SHOW_NAME_LABEL = 1;
    static final int LEGEND_NAME_WIDTH_SHORT = 2;
    static final int LEGEND_NAME_WIDTH_MEDIUM = 3;
    static final int LEGEND_NAME_WIDTH_LONG = 4;
    static final int LEGEND_MIN_HEIGHT_SHORT = 5;
    static final int LEGEND_MIN_HEIGHT_MEDIUM = 6;
    static final int LEGEND_MIN_HEIGHT_LONG = 7;
    static final int LEGEND_MIN_WIDTH_SHORT = 8;
    static final int LEGEND_MIN_WIDTH_MEDIUM = 9;
    static final int LEGEND_MIN_WIDTH_LONG = 10;
}
