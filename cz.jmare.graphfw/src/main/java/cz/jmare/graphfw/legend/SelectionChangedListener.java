package cz.jmare.graphfw.legend;

public interface SelectionChangedListener {
    void processSelectedId(String id);

    /**
     * Move object with given id behind other object
     * @param id
     * @param behindId it can be null to move to the beginning
     */
    void changeObjectPosition(String id, String behindId);

    void renameCurve(String id, String name);
}
