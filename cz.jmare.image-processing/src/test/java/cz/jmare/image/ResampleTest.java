package cz.jmare.image;

import java.io.File;
import java.io.IOException;

import cz.jmare.image.align.MultiAlignUniEngine;
import cz.jmare.image.resample.NearestNeighbourResampler;
import org.junit.jupiter.api.Test;
import org.maren.statis.affine.AffineElements;

import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class ResampleTest {
    @Test
    public void resamplingTest() throws UnableAlignException, IOException {
        CachedImage baseImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-base.png").getFile()));
        CachedImage aligningImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-rotated-crop.png").getFile()));

        MultiAligmentConfig config = new MultiAligmentConfig();
        config.pixelSelectionType(PixelSelectionType.MIN_BRIGHTNESS);
        config.minPixelBrightness(0.2f);

        MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseImage, aligningImage, config);
        AffineElements resutCoefs = multiAlignEngine.calcAffineElements();

        RasImageRead rasImageRead = baseImage.getRasImageRead();
        RasImageRead output = ResamplerEngine.transform(aligningImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), BilinearResampler::new, resutCoefs);
        ImFilesUtil.save8BitImage(new File("target/resampled.png"), output);
    }

    @Test
    public void resamplingNearesTest() throws UnableAlignException, IOException {
        CachedImage baseImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-base.png").getFile()));
        CachedImage aligningImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-rotated-crop.png").getFile()));

        MultiAligmentConfig config = new MultiAligmentConfig();
        config.pixelSelectionType(PixelSelectionType.MIN_BRIGHTNESS);
        config.minPixelBrightness(0.2f);
        MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseImage, aligningImage, config);
        AffineElements resutCoefs = multiAlignEngine.calcAffineElements();

        RasImageRead rasImageRead = baseImage.getRasImageRead();
        RasImageRead output = ResamplerEngine.transform(aligningImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), NearestNeighbourResampler::new, resutCoefs);
        ImFilesUtil.save8BitImage(new File("target/resampledNearest.png"), output);
    }
}
