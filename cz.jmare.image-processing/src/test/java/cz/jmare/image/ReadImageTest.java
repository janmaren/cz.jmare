package cz.jmare.image;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.centroid.CentroidsStats;
import cz.jmare.image.util.CachedImage;

public class ReadImageTest {
    @Test
    @Disabled
    public void readArgbTest() throws IOException {
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(new CachedImage(new File(ReadImageTest.class.getResource("/abgr4-test.png").getFile())), 20);
        centroidsAnalyzer.setMinClusterScore(100 / 255f);
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        assertEquals(3, centroids.size());
    }
    
    //@Test
    @Disabled
    public void compTest() throws IOException {
        CentroidsAnalyzer centroidsAnalyzer1 = new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m42\\DSC_0076-m42.NEF.jpg")), 200);
        List<Centroid> centroids1 = centroidsAnalyzer1.centroidsSortedScoreDesc();
        CentroidsAnalyzer centroidsAnalyzer2 = new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m42\\DSC_0066-m42.NEF.jpg")), 200);
        List<Centroid> centroids2 = centroidsAnalyzer2.centroidsSortedScoreDesc();
        for (int i = 0; i < Math.min(centroids1.size(),  centroids2.size()); i++) {
            System.out.println(centroids1.get(i));
            System.out.println(centroids2.get(i));
        }
        System.out.println(new CentroidsStats(centroidsAnalyzer1));
        System.out.println(new CentroidsStats(centroidsAnalyzer2));
    }
    
    @Test
    @Disabled
    public void readJpgTest() throws IOException {
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(new CachedImage(new File(ReadImageTest.class.getResource("/rgbtest.jpg").getFile())), 20);
        centroidsAnalyzer.setMinClusterScore(100 / 255f);
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        assertEquals(3, centroids.size());
    }
    
    @Test
    @Disabled
    public void readTifTest() throws IOException {
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(new CachedImage(new File(ReadImageTest.class.getResource("/rgbtest.tif").getFile())), 20);
        centroidsAnalyzer.setMinClusterScore(100 / 255f);
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        List<Centroid> sortedC = new ArrayList<>(centroids);
        Collections.sort(sortedC, (o1, o2) -> Integer.valueOf(o2.number).compareTo(Integer.valueOf(o1.number)));
        assertEquals(3, centroids.size());
        assertEquals(4, (int) sortedC.get(0).x);
        assertEquals(2, (int) sortedC.get(0).y);
    }
    
    //@Test
    public void real2Test() throws IOException {
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy-20220302\\DSC_0104.JPG")), 20);
        centroidsAnalyzer.setMinClusterScore(100 / 255f);
        long start = System.currentTimeMillis();
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        long end = System.currentTimeMillis();
        System.out.println("Doba: " + (end - start));
        List<Centroid> sortedC = new ArrayList<>(centroids);
        Collections.sort(sortedC, (o1, o2) -> Integer.valueOf(o2.number).compareTo(Integer.valueOf(o1.number)));
        System.out.println(sortedC.get(0));
        System.out.println(sortedC.size());
    }
}
