package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImagePack;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.ImConversionsUtil;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.SplineInterpolator;

public class HotpixelTest {
    public static RGBFloat interpolate(RasImageRead rasImageRead, int x, int y, boolean[][] hotpixels) {
        int neigh = 3;
        int from = x - neigh;
        int to = x + neigh + 1;

        List<Float> xValues = new ArrayList<>(to - from);
        List<Float> yRValues = new ArrayList<>(to - from);
        List<Float> yGValues = new ArrayList<>(to - from);
        List<Float> yBValues = new ArrayList<>(to - from);

        for (int i = from; i < to; i++) {
            if (i != x) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(i, y);
                xValues.add((float) i);
                yRValues.add(rgbFloat.red);
                yGValues.add(rgbFloat.green);
                yBValues.add(rgbFloat.blue);
            }
        }

        SplineInterpolator rInterp = SplineInterpolator.createMonotoneCubicSpline(xValues, yRValues);
        SplineInterpolator gInterp = SplineInterpolator.createMonotoneCubicSpline(xValues, yGValues);
        SplineInterpolator bInterp = SplineInterpolator.createMonotoneCubicSpline(xValues, yBValues);

        float r = rInterp.interpolate(x);
        float g = gInterp.interpolate(x);
        float b = bInterp.interpolate(x);

        return new RGBFloat(r, g, b);
    }

    public static boolean[][] create2dAray(int width, int height) {
        boolean[][] array = new boolean[height][];
        for (int i = 0; i < height; i++) {
            array[i] = new boolean[width];
        }
        return array;
    }

    @Test
    @Disabled
    void removeHotpixels() throws IOException {
        RasImagePack rasImagePack = RasImageFactory.get(new File("C:\\Users\\jmarencik\\Pictures\\testimage.tif"));

        RasImage rasImage = rasImagePack.getRasImage();

        RasBufImage outputImage = RasBufImage.createShortsInstance(rasImage.getWidth(), rasImage.getHeight());

        ImConversionsUtil.copy(rasImage, outputImage);


        boolean[][] hotpixels = create2dAray(rasImage.getWidth(), rasImage.getHeight());
        hotpixels[19][14] = true;
        hotpixels[19][15] = true;
        hotpixels[19][16] = true;
        hotpixels[18][15] = true;
        hotpixels[20][15] = true;

        hotpixels[21][16] = true;
        hotpixels[21][17] = true;
        hotpixels[21][18] = true;
        hotpixels[20][17] = true;
        hotpixels[22][17] = true;

        RGBFloat interpolated = interpolate(rasImage, 19, 14, hotpixels);
        outputImage.setRGBFloat(19, 14, interpolated);


        ImFilesUtil.save16BitImage(new File("C:\\Users\\jmarencik\\Pictures\\testimageout.tif"), outputImage);

    }
}
