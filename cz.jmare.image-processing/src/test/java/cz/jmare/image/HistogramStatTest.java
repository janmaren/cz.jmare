package cz.jmare.image;

import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.CachedImage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class HistogramStatTest {
    @Test
    public void centroidsTest() throws IOException, UnableAlignException {
        CachedImage baseImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-base.png").getFile()));
        float threshold0 = 0.4f;
        float percentil = HistogramStat.calcPercentilByThreshold(baseImage.getRasImageRead(), threshold0);
        float threshold = HistogramStat.calcThresholdByPercentil(baseImage.getRasImageRead(), percentil);
        Assertions.assertEquals(threshold0, threshold, 0.2);
    }

    @Test
    public void centroids3Test() throws IOException, UnableAlignException {
        int[] ints = new int[] {1, 3, 5, 7};
        int threshold0 = 3;
        float percentil = HistogramStat.calcPercentilByThreshold(ints, threshold0);
        int threshold = HistogramStat.calcThresholdByPercentil(ints, percentil);
        Assertions.assertEquals(threshold0, threshold);
    }
}