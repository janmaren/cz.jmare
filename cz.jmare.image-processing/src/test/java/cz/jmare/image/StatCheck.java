package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.CachedImage;

public class StatCheck {
    public void doFile(String file) throws IOException {
        CachedImage cachedImage = new CachedImage(new File(file));
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(cachedImage, 0.5f);
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        int sum = 0;
        for (Centroid centroid : centroids) {
            int number = centroid.number;
            sum += number;
        }
        int height = centroidsAnalyzer.getHeight();
        int width = centroidsAnalyzer.getWidth();
        int count = height * width;
        System.out.println(sum + ": " + ((float) sum / count) );
        
        int[] hist65535 = HistogramStat.hist65535(cachedImage.getRasImageRead());
        int cum = 0;
        for (int i = 65535; i >= 0; i--) {
            int value = hist65535[i];
            cum += value;
            System.out.println(i + ":" + value + "   " + cum);
            if (cum > sum) {
                break;
            }
        }
    }
    
    public void testFiles() throws IOException {
        //doFile("d:\\temp\\diffexpo]\\darktable_exported\\DSC_0081.tif");
        System.out.println("============");
        System.out.println(HistogramStat.invCdfValueFloat(new CachedImage(new File("C:\\temp\\m42zaloha\\DSC_0049-m42.NEF.jpg")).getRasImageRead(), 1f - 0.7e-4f));
        //doFile("d:\\temp\\diffexpo]\\darktable_exported\\DSC_0082.tif");
    }
}
