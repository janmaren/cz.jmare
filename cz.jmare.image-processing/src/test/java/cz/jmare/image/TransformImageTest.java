package cz.jmare.image;

import java.io.File;
import java.io.IOException;

import org.maren.statis.affine.AffineElements;

import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class TransformImageTest {
    //@Test
    public void readArgbTest() throws IOException {
        CachedImage bufferedImage = new CachedImage(new File(TransformImageTest.class.getResource("/abgr4-shifted.png").getFile()));
        AffineElements resutCoefs = new AffineElements(1, 1, 4, 1, 1, 0);
        RasImageRead transformed = ResamplerEngine.transform(bufferedImage.getRasImageRead(), 15, 10, BilinearResampler::new, resutCoefs);
        ImFilesUtil.save8BitImageWithAlpha(new File("c:\\temp\\out.png"), transformed);
    }
}
