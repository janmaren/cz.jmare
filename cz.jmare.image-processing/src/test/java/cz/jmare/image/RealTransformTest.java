package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.maren.statis.affine.AffineElements;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class RealTransformTest {
    //@Test
    public void transformTest() throws IOException {
        
        
        CachedImage baseImage = new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy-20220302\\stack\\sirius-iso100-062s-jpg\\DSC_0098.JPG"));
        CachedImage toTransImage = new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy-20220302\\stack\\sirius-iso100-062s-jpg\\DSC_0099.JPG"));
        
        CentroidsAnalyzer baseAnalyzer = new CentroidsAnalyzer(baseImage, 450);
        ArrayList<Centroid> baseList = new ArrayList<>(baseAnalyzer.centroids());
        Collections.sort(baseList, (o1, o2) -> Float.valueOf(o2.score).compareTo(o1.score));
        if (baseList.size() == 0) {
            throw new RuntimeException("No base object");
        }
        Centroid baseCentroid = baseList.get(0);
        
        CentroidsAnalyzer toTransAnalyzer = new CentroidsAnalyzer(toTransImage, 450);
        ArrayList<Centroid> toTransList = new ArrayList<>(toTransAnalyzer.centroids());
        Collections.sort(toTransList, (o1, o2) -> Float.valueOf(o2.score).compareTo(o1.score));
        if (toTransList.size() == 0) {
            throw new RuntimeException("No trans. object");
        }
        Centroid toTransCentroid = toTransList.get(0);

        double dx = toTransCentroid.x - baseCentroid.x;
        double dy = toTransCentroid.y - baseCentroid.y;
        int dintx = (int) Math.round(dx);
        int dinty = (int) Math.round(dy);
        System.out.println("dx: " + dintx + ", " + dinty);
        
        RasImageRead rasImageRead = baseImage.getRasImageRead();
        AffineElements resutCoefs = new AffineElements(1, 1, dintx, 1, 1, dinty);
        RasImageRead transformed = ResamplerEngine.transform(toTransImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), BilinearResampler::new, resutCoefs);
        ImFilesUtil.save8BitImageWithAlpha(new File("c:\\temp\\out.png"), transformed);
    }
}
