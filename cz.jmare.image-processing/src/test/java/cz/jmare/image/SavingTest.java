package cz.jmare.image;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.util.ImFilesUtil;

public class SavingTest {
    @Test
    public void saving16bitTest() throws IOException {
        RasBufImage rasFloats = RasBufImage.createShortsInstance(256, 256);
        
        for (int j = 0; j < 256; j ++) {
            for (int i = 0; i < 256; i++) {
                int val = j * 256 + i;
                float valFloat = val / 65535f;
                rasFloats.setRGBFloat(i, j, new RGBFloat(valFloat, 0f, 0f));
            }
        }
        
        ImFilesUtil.saveBufImage(new File("target/out16bit.png"), rasFloats.getBufferedImage());
        ImFilesUtil.saveBufImage(new File("target/out16bit.tiff"), rasFloats.getBufferedImage());
        
        RasImage rasImagePng = RasImageFactory.get(new File("target/out16bit.png")).getRasImage();
        RasImage rasImageTif = RasImageFactory.get(new File("target/out16bit.tiff")).getRasImage();
        
        for (int j = 0; j < 256; j ++) {
            for (int i = 0; i < 256; i++) {
                int val = j * 256 + i;
                float valFloat = val / 65535f;
                rasFloats.setRGBFloat(i, j, new RGBFloat(valFloat, 0f, 0f));
                RGBFloat rgbFloat = rasImagePng.getRGBFloat(i, j);
                if (valFloat != rgbFloat.red) {
                    System.out.println("Not same in png: " + i + ", " + j + ": expected " + valFloat + " but is " + rgbFloat.red + ", error: " + Math.abs(valFloat - rgbFloat.red));
                    assertEquals(valFloat, rgbFloat.red);
                }
                rgbFloat = rasImageTif.getRGBFloat(i, j);
                if (valFloat != rgbFloat.red) {
                    System.out.println("Not same in tiff: " + i + ", " + j + ": expected " + valFloat + " but is " + rgbFloat.red + ", error: " + Math.abs(valFloat - rgbFloat.red));
                    assertEquals(valFloat, rgbFloat.red);
                }
            }
        }
    }

    public static boolean same(float float1, float float2) {
        return float1 - 1e10 < float2 && float2 < float1 + 1e10;
    }
}
