package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import cz.jmare.image.align.MultiAlignUniEngine;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.maren.statis.affine.AffineElements;

import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class CentroidsTest {
    @Test
    @Disabled
    public void centroidsTest() throws IOException, UnableAlignException {
        CachedImage baseImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-base.png").getFile()));
        CentroidsAnalyzer centroidsAnalyzer = new CentroidsAnalyzer(baseImage, 0.1f);
        Set<Centroid> centroids = centroidsAnalyzer.centroids();
        for (Centroid centroid : centroids) {
            System.out.println(centroid);
        }
        System.out.println("---");
        CachedImage aligningImage = new CachedImage(new File(ReadImageTest.class.getResource("/fourstars-rotated-crop.png").getFile()));
        CentroidsAnalyzer centroidsAnalyzer2 = new CentroidsAnalyzer(aligningImage, 0.1f);
        Set<Centroid> centroids2 = centroidsAnalyzer2.centroids();
        for (Centroid centroid : centroids2) {
            System.out.println(centroid);
        }
        System.out.println("---");
        MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseImage, aligningImage);
        AffineElements resutCoefs = multiAlignEngine.calcAffineElements();
        System.out.println(resutCoefs);
        
        RasImageRead rasImageRead = baseImage.getRasImageRead();
        RasImageRead output = ResamplerEngine.transform(aligningImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), BilinearResampler::new, resutCoefs);
        ImFilesUtil.save8BitImage(new File("target/aligned.png"), output);
        
        CachedImage stackedImage = StackingTask.stack(List.of(baseImage, new CachedImage(new File("target/aligned.png"))), new File("target/stacked.tiff"), new StackingConfig());
        ImFilesUtil.save8BitImage(new File("target/stacked.png"), stackedImage.getRasImageRead());
    }
}
