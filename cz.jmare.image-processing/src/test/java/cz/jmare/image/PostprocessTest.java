package cz.jmare.image;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import cz.jmare.image.postprocess.AmplifyTrans;
import cz.jmare.image.ras.RGBFloatUtil;
import cz.jmare.image.util.CachedImage;

public class PostprocessTest {
    @Test
    @Disabled
    void amplify() throws IOException {
        CachedImage inputImage = new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\hvezdy\\hvezdy-20230920\\tulipan-all-tifs\\result\\xxx\\output_wb_01.tif"));
        float brightnessDark = RGBFloatUtil.getBrightness(inputImage.getRasImageRead().getRGBFloat(1375, 1032));
        float brightnessNebula = RGBFloatUtil.getBrightness(inputImage.getRasImageRead().getRGBFloat(1334, 996));
        PostprocessTask.transformPixels(inputImage,
                new File("C:\\Users\\jmarencik\\Pictures\\hvezdy\\hvezdy-20230920\\tulipan-all-tifs\\result\\xxx\\output_wb_01-out.tif"),
                new AmplifyTrans(brightnessDark, brightnessNebula, 1.3f, 3));
    }
}
