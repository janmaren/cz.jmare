package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cz.jmare.image.combine.GifCreationEngine;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class AnimCreationTask {
    public static CachedImage animate(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        return animate(filesListCached, outputFile, new AnimCreationConfig());
    }
    
    public static CachedImage animate(List<CachedImage> inputFiles, File outputFile, AnimCreationConfig animCreationConfig) throws IOException {
        outputFile.getParentFile().mkdirs();
        String format = ImFilesUtil.getFormat(outputFile).toLowerCase();
        if (!format.equals("gif")) {
            throw new IllegalArgumentException("Not a gif file the " + outputFile);
        }
        GifCreationEngine.createGif(inputFiles, outputFile, animCreationConfig.getDelay(), animCreationConfig.isLoop(), animCreationConfig.getLastFrameTimes(), animCreationConfig.getGraphConsumer());
        System.out.println("- Created " + outputFile + " -");
        return new CachedImage(outputFile);
    }
}
