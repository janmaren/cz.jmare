package cz.jmare.image.crop;

import java.util.function.Function;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class NullCropper implements Cropper {

    @Override
    public Function<RasImageRead, RasImageRead> crop(Centroid baseCentroid, CachedImage baseImage) {
        return new Function<RasImageRead, RasImageRead>() {
            @Override
            public RasImageRead apply(RasImageRead input) {
                return input;
            }
        };
    }

}
