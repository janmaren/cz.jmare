package cz.jmare.image.crop;

import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public interface Cropping {
    RasImageRead crop(CachedImage baseImage);
}
