package cz.jmare.image.crop;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class RadiusCropping implements Cropping {
    private Integer cropRadiusX;
    
    private Integer cropRadiusY;

    private int startXFin;

    private int startYFin;

    private int widthFin;

    private int heightFin;
    
    public RadiusCropping(int centerX, int centerY, int wholeWidth, int wholeHeight, Integer cropRadiusX, Integer cropRadiusY) {
        this.cropRadiusX = cropRadiusX;
        this.cropRadiusY = cropRadiusY;
        

        int width = 2 * cropRadiusX;
        int startX = (int) (centerX - cropRadiusX);
        if (startX < 0) {
            width += startX;
            startX = 0;
        }
        int height = 2 * cropRadiusY;
        int startY = (int) (centerY - cropRadiusY);
        if (startY < 0) {
            height += startY;
            startY = 0;
        }
        if (startX + width > wholeWidth) {
            width -= startX + width - wholeWidth;
        }
        if (startY + height > wholeHeight) {
            height -= startY + height - wholeHeight;
        }
        
        startXFin = startX;
        startYFin = startY;
        widthFin = width;
        heightFin = height;
    }

    public Integer getCropRadiusX() {
        return cropRadiusX;
    }

    public Integer getCropRadiusY() {
        return cropRadiusY;
    }
    @Override
    public RasImageRead crop(CachedImage cachedImage) {
        RasImageRead input = cachedImage.getRasImageRead();
        RasImageFloat output = new RasImageFloat(widthFin, heightFin);
        int endY = startYFin + heightFin;
        int endX = startXFin + widthFin;
        for (int j = startYFin; j < endY; j++) {
            for (int i = startXFin; i < endX; i++) {
                RGBFloat rgb = input.getRGBFloat(i, j);
                output.setRGBFloat(i - startXFin, j - startYFin, rgb);
            }
        }
        return output;
    }

}
