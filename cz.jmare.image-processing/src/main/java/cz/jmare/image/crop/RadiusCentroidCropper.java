package cz.jmare.image.crop;

import java.util.function.Function;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

/**
 * Calculate the right crop around a point
 */
public class RadiusCentroidCropper implements Cropper {
    private Integer cropRadiusX;
    
    private Integer cropRadiusY;
    
    public RadiusCentroidCropper(Integer cropRadiusX, Integer cropRadiusY) {
        super();
        this.cropRadiusX = cropRadiusX;
        this.cropRadiusY = cropRadiusY;
    }

    @Override
    public Function<RasImageRead, RasImageRead> crop(Centroid baseCentroid, CachedImage baseImage) {
        if (cropRadiusX == null) {
            return transImage -> transImage;
        }
        int width = 2 * cropRadiusX;
        int startX = (int) (baseCentroid.x - cropRadiusX);
        if (startX < 0) {
            width += startX;
            startX = 0;
        }
        int height = 2 * cropRadiusY;
        int startY = (int) (baseCentroid.y - cropRadiusY);
        if (startY < 0) {
            height += startY;
            startY = 0;
        }
        RasImageRead baseRasImageRead = baseImage.getRasImageRead();
        if (startX + width > baseRasImageRead.getWidth()) {
            width -= startX + width - baseRasImageRead.getWidth();
        }
        if (startY + height > baseRasImageRead.getHeight()) {
            height -= startY + height - baseRasImageRead.getHeight();
        }
        
        int startXFin = startX;
        int startYFin = startY;
        int widthFin = width;
        int heightFin = height;
        Function<RasImageRead, RasImageRead> outputCorrecter = new Function<RasImageRead, RasImageRead>() {
            @Override
            public RasImageRead apply(RasImageRead input) {
                RasImageFloat output = new RasImageFloat(widthFin, heightFin);
                int endY = startYFin + heightFin;
                int endX = startXFin + widthFin;
                for (int j = startYFin; j < endY; j++) {
                    for (int i = startXFin; i < endX; i++) {
                        RGBFloat rgb = input.getRGBFloat(i, j);
                        output.setRGBFloat(i - startXFin, j - startYFin, rgb);
                    }
                }
                return output;
            }
        };
                
        return outputCorrecter;
    }

    public Integer getCropRadiusX() {
        return cropRadiusX;
    }

    public Integer getCropRadiusY() {
        return cropRadiusY;
    }
}
