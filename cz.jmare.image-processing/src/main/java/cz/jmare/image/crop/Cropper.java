package cz.jmare.image.crop;

import java.util.function.Function;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

/**
 * Return function which will be used to crop an image. The baseCentroid and baseImage can be used for purposes when such cropping is based on the base image  
 */
public interface Cropper {
    Function<RasImageRead, RasImageRead> crop(Centroid baseCentroid, CachedImage baseImage);
}
