package cz.jmare.image.colorcorrect;

import java.io.File;
import java.io.IOException;

import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class ToOneColorApp {

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.out.println("Wrong number of arguments");
            System.out.println("Syntax: input_file output_file channel");
            System.exit(-1);
        }
        CachedImage cachedImage = new CachedImage(new File(args[0]));
        ValueToChannelColorCorrecter valueToChannelColorCorrecter = new ValueToChannelColorCorrecter(Integer.valueOf(args[2]));
        RasImageRead rasImageRead = cachedImage.getRasImageRead();
        valueToChannelColorCorrecter.init(rasImageRead);
        RasBufImage output = RasBufImage.createBytesInstance(rasImageRead.getWidth(), rasImageRead.getHeight(), false);
        for (int j = 0; j < rasImageRead.getHeight(); j++) {
            for (int i = 0; i < rasImageRead.getWidth(); i++) {
                output.setRGBFloat(i, j, valueToChannelColorCorrecter.transform(i, j));
            }
        }
        ImFilesUtil.save8BitImageWithAlpha(new File(args[1]), output);
    }

}
