package cz.jmare.image.colorcorrect;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class ValueToChannelColorCorrecter implements ColorCorrecter {

    private RasImageRead rasImage;

    private int channel;
    
    public ValueToChannelColorCorrecter(int channel) {
        super();
        this.channel = channel;
    }

    @Override
    public void init(RasImageRead rasImage) {
        this.rasImage = rasImage;
    }

    @Override
    public RGBFloat transform(int x, int y) {
        RGBFloat rgbFloat = rasImage.getRGBFloat(x, y);
        float value = (rgbFloat.red + rgbFloat.green + rgbFloat.blue);
        if (channel == 0) {
            return new RGBFloat(Math.min(1, value), 0, 0);
        }
        if (channel == 1) {
            return new RGBFloat(0, Math.min(1, value), 0);
        }
        if (channel == 2) {
            return new RGBFloat(0, 0, Math.min(1, value));
        }
        throw new IllegalStateException("Wrong channel");
    }

}
