package cz.jmare.image.colorcorrect;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class CopyChannelColorCorrecter implements ColorCorrecter {

    private RasImageRead rasImage;

    private int channel;

    public CopyChannelColorCorrecter(int channel) {
        super();
        this.channel = channel;
    }

    @Override
    public void init(RasImageRead rasImage) {
        this.rasImage = rasImage;
    }

    @Override
    public RGBFloat transform(int x, int y) {
        RGBFloat rgbFloat = rasImage.getRGBFloat(x, y);
        if (channel == 0) {
            return new RGBFloat(rgbFloat.red, 0, 0);
        }
        if (channel == 1) {
            return new RGBFloat(0, rgbFloat.green, 0);
        }
        if (channel == 2) {
            return new RGBFloat(0, 0, rgbFloat.blue);
        }
        throw new IllegalStateException("Wrong channel");
    }

}
