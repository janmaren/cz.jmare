package cz.jmare.image.colorcorrect;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public interface ColorCorrecter {
    final static RGBFloat TRANS_BLACK_RGB = new RGBFloat(0, 0, 0, 0);
    
    /**
     * Init data
     * @param rasImage image to correct
     */
    void init(RasImageRead rasImage);
    
    RGBFloat transform(int x, int y);
}
