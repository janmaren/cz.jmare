package cz.jmare.image;

public enum RotationCounterClockwise {
    ROT_90, ROT_180, ROT_270
}
