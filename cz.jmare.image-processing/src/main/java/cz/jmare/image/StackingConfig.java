package cz.jmare.image;

import cz.jmare.image.combine.AvgStacker;
import cz.jmare.image.combine.Stacker;

public class StackingConfig {
    private Stacker stacker = new AvgStacker();
    
    public StackingConfig stacker(Stacker stacker) {
        this.stacker = stacker;
        return this;
    }

    @Override
    public String toString() {
        return "stack method: " + stacker;
    }

    public Stacker getStacker() {
        return stacker;
    }
}
