package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class StackingTask {
    /**
     * Stack all files in in input dir and save to output file as 16bit float image (when format does not support then 8bit)<br>
     * Using default configuration (AVG type used)
     * @param inputDir
     * @param outputFile
     * @return
     * @throws IOException
     */
    public static CachedImage stack(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        return stack(filesListCached, outputFile, new StackingConfig());
    }
    
    /**
     * Stack aligned input files and save as float 16bit image (when format supports).<br>
     * Using algorithm by configuration
     * @param inputFiles aligned input (cached or not) files
     * @param outputFile 
     * @param stackingConfig
     * @throws IOException
     */
    public static CachedImage stack(List<CachedImage> inputFiles, File outputFile, StackingConfig stackingConfig) throws IOException {
        return stackingConfig.getStacker().stack(inputFiles, outputFile);
    }
    
    public static void main(String[] args) throws IOException {
        stack(new File("C:\\temp\\m42\\aligns\\"), new File("c:\\temp\\stack.png"));
    }
}
