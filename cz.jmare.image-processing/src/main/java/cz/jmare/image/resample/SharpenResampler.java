package cz.jmare.image.resample;

import org.maren.statis.affine.AffineElements;
import org.maren.statis.entity.Coordinate;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class SharpenResampler implements Resampler {

    private RasImageRead rasImage;
    private AffineElements affineElements;
    private int width;
    private int height;

    @Override
    public void init(RasImageRead rasImage, AffineElements affineElements) {
        this.rasImage = rasImage;
        this.affineElements = affineElements;
        width = rasImage.getWidth();
        height = rasImage.getHeight();
    }

    @Override
    public RGBFloat transform(int x, int y) {
        Coordinate p = affineElements.transform(new Coordinate(x, y));
        int tx = (int) Math.round(p.x);
        int ty = (int) Math.round(p.y);
        if (tx < 0 || tx >= width || ty < 0 || ty >= height) {
            return TRANS_BLACK_RGB;
        }
        RGBFloat rgb = rasImage.getRGBFloat(tx, ty);
        float sumR = rgb.red * 5;
        float sumG = rgb.green * 5;
        float sumB = rgb.blue * 5;
        if (tx > 0) {
            rgb = rasImage.getRGBFloat(tx - 1, ty);
            sumR += rgb.red * -1;
            sumG += rgb.green * -1;
            sumB += rgb.blue * -1;
        }
        if (tx < width - 1) {
            rgb = rasImage.getRGBFloat(tx + 1, ty);
            sumR += rgb.red * -1;
            sumG += rgb.green * -1;
            sumB += rgb.blue * -1;

        }
        if (ty > 0) {
            rgb = rasImage.getRGBFloat(tx, ty - 1);
            sumR += rgb.red * -1;
            sumG += rgb.green * -1;
            sumB += rgb.blue * -1;

        }
        if (ty < height - 1) {
            rgb = rasImage.getRGBFloat(tx, ty + 1);
            sumR += rgb.red * -1;
            sumG += rgb.green * -1;
            sumB += rgb.blue * -1;
        }

        sumR = Math.max(0, Math.min(1, sumR));
        sumG = Math.max(0, Math.min(1, sumG));
        sumB = Math.max(0, Math.min(1, sumB));

        RGBFloat rgbPix = new RGBFloat(sumR, sumG, sumB);
        return rgbPix;
    }
}
