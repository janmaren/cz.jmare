package cz.jmare.image.resample;

import java.util.function.Supplier;

import org.maren.statis.affine.AffineElements;

import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;

public class ResamplerEngine {
    public static RasImageRead transform(RasImageRead rasImageRead, int destWidth, int destHeight, Supplier<Resampler> sup, AffineElements affineCoefs) {
        Resampler resampler = sup.get();
        resampler.init(rasImageRead, affineCoefs);
        RasImageFloat rasImageFloat = new RasImageFloat(destWidth, destHeight);
        for (int j = 0; j < destHeight; j++) {
            for (int i = 0; i < destWidth; i++) {
                rasImageFloat.setRGBFloat(i, j, resampler.transform(i, j));
            }
        }
        return rasImageFloat;
    }
    
    public static RasImageRead convertToFloat(RasImageRead rasImageRead) {
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        RasImageFloat rasImageFloat = new RasImageFloat(width, height);
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                rasImageFloat.setRGBFloat(i, j, rasImageRead.getRGBFloat(i, j));
            }
        }
        return rasImageFloat;
    }
}
