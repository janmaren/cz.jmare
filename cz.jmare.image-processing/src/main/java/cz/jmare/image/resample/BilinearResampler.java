package cz.jmare.image.resample;

import org.maren.statis.affine.AffineElements;
import org.maren.statis.entity.Coordinate;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class BilinearResampler implements Resampler {

    private RasImageRead rasImage;
    private AffineElements affineElements;
    private int width;
    private int height;

    @Override
    public void init(RasImageRead rasImage, AffineElements affineElements) {
        this.rasImage = rasImage;
        this.affineElements = affineElements;
        width = rasImage.getWidth();
        height = rasImage.getHeight();
    }

    @Override
    public RGBFloat transform(int x, int y) {
        Coordinate p = affineElements.transform(new Coordinate(x, y));
        int fx = (int) p.x;
        int fy = (int) p.y;
        if (fx < 0 || fx >= width || fy < 0 || fy >= height) {
            return TRANS_BLACK_RGB;
        }
        int fx1 = fx + 1;
        int fy1 = fy + 1;
        
        double dimx1 = fx1 - p.x;
        double dimy1 = fy1 - p.y;
        double dimx2 = p.x - fx;
        double dimy2 = p.y - fy;

        double weight1 = dimx1 * dimy1;
        double weight2 = dimx2 * dimy1;
        double weight3 = dimx1 * dimy2;
        double weight4 = dimx2 * dimy2;
        
        RGBFloat rgbFloat1 = rasImage.getRGBFloat(fx, fy);
        RGBFloat rgbFloat2;
        if (fx1 < width) {
            rgbFloat2 = rasImage.getRGBFloat(fx1, fy);
        } else {
            rgbFloat2 = rgbFloat1;
        }
        RGBFloat rgbFloat3;
        if (fy1 < height) {
            rgbFloat3 = rasImage.getRGBFloat(fx, fy1);
        } else {
            rgbFloat3 = rgbFloat1;
        }
        RGBFloat rgbFloat4;
        if (fx1 < width && fy1 < height) {
            rgbFloat4 = rasImage.getRGBFloat(fx1, fy1);
        } else {
            rgbFloat4 = rgbFloat1;
        }
        
        return new RGBFloat(
                (float) (rgbFloat1.red * weight1 + rgbFloat2.red * weight2 + rgbFloat3.red * weight3 + rgbFloat4.red * weight4),
                (float) (rgbFloat1.green * weight1 + rgbFloat2.green * weight2 + rgbFloat3.green * weight3 + rgbFloat4.green * weight4),
                (float) (rgbFloat1.blue * weight1 + rgbFloat2.blue * weight2 + rgbFloat3.blue * weight3 + rgbFloat4.blue * weight4));
    }
}
