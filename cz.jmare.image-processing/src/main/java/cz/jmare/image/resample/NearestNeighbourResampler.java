package cz.jmare.image.resample;

import org.maren.statis.affine.AffineElements;
import org.maren.statis.entity.Coordinate;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class NearestNeighbourResampler implements Resampler {

    private RasImageRead rasImage;
    private AffineElements affineElements;
    private int width;
    private int height;

    @Override
    public void init(RasImageRead rasImage, AffineElements affineElements) {
        this.rasImage = rasImage;
        this.affineElements = affineElements;
        width = rasImage.getWidth();
        height = rasImage.getHeight();
    }

    @Override
    public RGBFloat transform(int x, int y) {
        Coordinate p = affineElements.transform(new Coordinate(x, y));
        int xx = (int) Math.round(p.x);
        int yy = (int) Math.round(p.y);
        if (xx < 0 || xx >= width || yy < 0 || yy >= height) {
            return TRANS_BLACK_RGB;
        }
        return rasImage.getRGBFloat(xx, yy);
    }
}
