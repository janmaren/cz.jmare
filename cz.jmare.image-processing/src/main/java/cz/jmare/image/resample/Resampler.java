package cz.jmare.image.resample;

import org.maren.statis.affine.AffineElements;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public interface Resampler {
    final static RGBFloat TRANS_BLACK_RGB = new RGBFloat(0, 0, 0, 0);
    
    /**
     * Init data
     * @param rasImage image to resample
     * @param affineElements affine elements - x and y are source coordinates for affineElements and the result must point to the right pixel in image
     */
    void init(RasImageRead rasImage, AffineElements affineElements);
    
    RGBFloat transform(int x, int y);
}
