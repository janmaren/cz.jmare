package cz.jmare.image;

import cz.jmare.image.exception.ProcessInterruptedException;
import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.exception.UnableAnimateException;
import cz.jmare.image.exception.UnableFlatException;
import cz.jmare.image.exception.UnablePostprocessException;
import cz.jmare.image.exception.UnableStackException;
import cz.jmare.image.exception.UnableSubtrackException;
import cz.jmare.image.filter.FilteringEngine;
import cz.jmare.image.hotpixel.HotpixelDetector;
import cz.jmare.image.hotpixel.InterByFilterEngine;
import cz.jmare.image.hotpixel.SimpleThresholdHotpixelDecider;
import cz.jmare.image.oper.FlatEngine;
import cz.jmare.image.oper.RotateEngine;
import cz.jmare.image.oper.SubtrackEngine;
import cz.jmare.image.postprocess.PixelTransformer;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.FileConstants;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.PathUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageProcess {
    public static boolean doImageProcess(ImageProcessConfig plan) {
        try {
            if (plan.getRootOutputDir() == null) {
                System.out.println("Unknown output directory");
                return false;
            }
            List<CachedImage> alignedImages = null;
            if (plan.isAligment()) {
                CachedImage stackedDarkImage = null;
                if (plan.getDarkFiles() != null && !plan.getDarkFiles().isEmpty()) {
                    try {
                        stackedDarkImage = doStackDarks(plan);
                        stackedDarkImage = doDarkRotation(stackedDarkImage, plan);
                    } catch (UnableStackException e) {
                        System.out.println("Unable to stack dark files" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                }

                // subtract master dark image
                List<CachedImage> inputFiles = plan.getLightFiles();
                CachedImage baseCachedImage = plan.getBaseFile();
                if (stackedDarkImage != null && (plan.isRemoveHotPixels() || plan.isSubtrackDarks())) {
                    try {
                        float[][] hotpixels = null;
                        if (plan.isRemoveHotPixels()) {
                            System.out.println("Hotpixel removing enabled - removing pixels from brightness " + (100 * plan.getMinHotPixelsBrightness()) + "%");
                            SimpleThresholdHotpixelDecider decider = new SimpleThresholdHotpixelDecider(plan.getMinHotPixelsBrightness());
                            hotpixels = HotpixelDetector.detectHotpixels(stackedDarkImage, decider);
                            HotpixelDetector.printInfo(hotpixels);

                            File darkNoHotpixelsFile = PathUtil.deriveFile(plan.getRootOutputDir(), FileConstants.DARK_SUBDIR,
                                    FileConstants.DARK_NO_HOTPIXELS_FILE_NAME);
                            stackedDarkImage = InterByFilterEngine.interpolate(stackedDarkImage, darkNoHotpixelsFile, hotpixels, plan.getHotpixelInterpolationDimension());
                        }

                        inputFiles = subtractDarkImages(inputFiles, stackedDarkImage, hotpixels, plan);

                        // let's subtract also base image although it will not be stacked (but maybe better alignment)
                        File outputFile = File.createTempFile("baseWithoutDark", ".tif");
                        baseCachedImage = subtractDarkImage(stackedDarkImage, hotpixels, outputFile, baseCachedImage, plan.isSubtrackDarks(), plan.getHotpixelInterpolationDimension());
                    } catch (UnableSubtrackException | IOException e) {
                        System.out.println("Unable to subtract dark image" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                }

                if (plan.getAligmentType() == AligmentType.SINGLE) {
                    try {
                        alignedImages = doSingleAlignment(baseCachedImage, inputFiles, plan);
                    } catch (UnableAlignException e) {
                        System.out.println("Unable to align" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                } else if (plan.getAligmentType() == AligmentType.MULTI) {
                    try {
                        alignedImages = doMultipleAlignment(baseCachedImage, inputFiles, plan);
                    } catch (UnableAlignException e) {
                        System.out.println("Unable to align" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                }
            } else {
                alignedImages = ImFilesUtil.filesListCached(FileConstants.getAlignOutputDir(plan.getRootOutputDir()));
            }

            if (plan.isCrop()) {
                alignedImages = CroppingTask.crop(alignedImages, plan.getCropConfig());
            }

            if (Thread.interrupted()) {
                System.out.println("Interrupted");
                return false;
            }

            CachedImage stackedImage = null;
            if (plan.isCombine()) {
                if (plan.isStack()) {
                    try {
                        stackedImage = doStack(alignedImages, plan);
                    } catch (UnableStackException e) {
                        System.out.println("Unable to stack" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                } else {
                    //stackedImage = new CachedImage(plan.getPostprocessInputFile());
                }

                if (plan.isAnimate()) {
                    try {
                        doAnimate(alignedImages, plan);
                    } catch (UnableAnimateException e) {
                        System.out.println("Unable to animate" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                        return false;
                    }
                }
            }

            CachedImage flatCachedImage = null;
            if (plan.getFlatFiles() != null && !plan.getFlatFiles().isEmpty()) {
                try {
                    flatCachedImage = doStackFlats(plan.getFlatFiles(), plan.getRootOutputDir());
                    flatCachedImage = doFlatFiltering(flatCachedImage, plan);
                    flatCachedImage = doFlatRotation(flatCachedImage, plan);
                } catch (UnableStackException e) {
                    System.out.println("Unable to stack flats" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                    return false;
                }
            }

            if (flatCachedImage != null) {
                if (stackedImage == null) {
                    File stackOutputFile = FileConstants.getStackOutputFile(plan.getRootOutputDir());
                    if (!stackOutputFile.isFile()) {
                        System.out.println("Unable to use flat file because " + stackOutputFile + " doesn't exist. Please, use stacking first.");
                        return false;
                    }
                    stackedImage = new CachedImage(stackOutputFile);
                }
                try {
                    stackedImage = useFlatImage(stackedImage, flatCachedImage, plan);
                } catch (UnableFlatException e) {
                    System.out.println("Unable to use flat image" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                    return false;
                }
            }

            if (plan.isPostprocess()) {
                if (stackedImage == null) {
                    File resultDir = new File(plan.getRootOutputDir(), FileConstants.OUTPUT_SUBDIR);
                    File file = new File(resultDir, FileConstants.FFC_FILE_NAME);
                    if (!file.isFile()) {
                        file = new File(resultDir, FileConstants.STACK_FILE_NAME);
                        if (!file.isFile()) {
                            System.out.println("Stacked file doesn't exist - nothing to postprocess");
                            return false;
                        }
                    }
                    stackedImage = new CachedImage(file);
                }
                try {
                    doPostprocess(stackedImage, plan);
                } catch (UnablePostprocessException e) {
                    System.out.println("Unable to postprocess" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
                    return false;
                }
            }

            System.out.println("=== Done ===");
            return true;
        } catch (RuntimeException e) {
            System.out.println("Error - not all tasks accomplished" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
            return false;
        }
    }

    public static CachedImage useFlatImage(CachedImage stackedImage, CachedImage flatCachedImage, ImageProcessConfig plan) throws UnableFlatException {
        System.out.println("=== Using flat image " + flatCachedImage.getFile() + " to balance " + stackedImage.getFile() + " ===");
        CachedImage balancedCachedImage = FlatEngine.flat(stackedImage, flatCachedImage, FileConstants.getFfcOutputFile(plan.getRootOutputDir()));
        System.out.println("Saved to: " + balancedCachedImage.getFile());
        return balancedCachedImage;
    }

    public static List<CachedImage> subtractDarkImages(List<CachedImage> inputFiles, CachedImage stackedDarkImage, float[][] hotpixels, ImageProcessConfig plan) throws UnableSubtrackException {
        System.out.println("=== Subtracking dark file " + stackedDarkImage.getFile() + (hotpixels != null && !plan.isSubtrackDarks() ? "only hotpixels removing " : "") + " ===");

        List<CachedImage> processedFiles = new ArrayList<>();

        File outputDir = new File(plan.getRootOutputDir(), FileConstants.SUBTRACTED_DARKS_SUBDIR);
        outputDir.mkdirs();
        ImFilesUtil.deleteDirContent(outputDir);
        Thread currentThread = Thread.currentThread();
        for (int i = 0; i < inputFiles.size(); i++) {
            if (currentThread.isInterrupted()) {
                throw new ProcessInterruptedException();
            }
            System.out.println("> [" + (i + 1) + "/" + inputFiles.size() + "] Subtracking dark from " + inputFiles.get(i).getFile() + " <");
            CachedImage inputCachedImage = inputFiles.get(i);
            File outputFile = PathUtil.deriveFileSetSuffix(plan.getRootOutputDir(), FileConstants.SUBTRACTED_DARKS_SUBDIR, inputCachedImage.getFile().getName(), ".tif");
            CachedImage subtracked = subtractDarkImage(stackedDarkImage, hotpixels, outputFile, inputCachedImage, plan.isSubtrackDarks(), plan.getHotpixelInterpolationDimension());
            processedFiles.add(subtracked);
        }
        System.out.println("Subtracked to " + new File(plan.getRootOutputDir(), FileConstants.SUBTRACTED_DARKS_SUBDIR));
        return processedFiles;
    }

    static CachedImage subtractDarkImage(CachedImage stackedDarkImage, float[][] hotpixels, File outputFile, CachedImage inputCachedImage, boolean subtractDark, Integer dim) throws UnableSubtrackException {
        int width = inputCachedImage.getRasImageRead().getWidth();
        int height = inputCachedImage.getRasImageRead().getHeight();
        int dwidth = stackedDarkImage.getRasImageRead().getWidth();
        int dheight = stackedDarkImage.getRasImageRead().getHeight();
        if (width != dwidth || height != dheight) {
            throw new UnableSubtrackException("Dark frame has different dimensions " + dwidth + "x" + dheight + " but expected " + width + "x" + height);
        }
        if (hotpixels != null) {
            inputCachedImage = InterByFilterEngine.interpolate(inputCachedImage, outputFile, hotpixels, dim);
        }
        if (subtractDark) {
            return SubtrackEngine.subtrack(inputCachedImage, stackedDarkImage, outputFile);
        }
        return inputCachedImage;
    }

    public static CachedImage doDarkFiltering(CachedImage cachedImage, ImageProcessConfig plan) {
        System.out.println("=== Smoothing dark " + cachedImage.getFile() + " ===");

        CachedImage smoothedCachedImage = FilteringEngine.filter(cachedImage, plan.getDarkSmoothWeights(), PathUtil.distinguishFile(cachedImage.getFile(), "-smooth"));
        System.out.println("Saved to " + smoothedCachedImage.getFile());
        return smoothedCachedImage;
    }

    public static CachedImage doDarkRotation(CachedImage cachedImage, ImageProcessConfig plan) {
        if (plan.getDarkRotation() == null) {
            return cachedImage;
        }
        System.out.println("=== Rotation dark " + cachedImage.getFile() + " ===");

        CachedImage rotatedCachedImage = RotateEngine.rotate(cachedImage, plan.getDarkRotation(), cachedImage.getFile());
        System.out.println("Saved to " + rotatedCachedImage.getFile());
        return rotatedCachedImage;
    }

    public static CachedImage doFlatFiltering(CachedImage cachedImage, ImageProcessConfig plan) {
        System.out.println("=== Smoothing " + cachedImage.getFile() + " ===");

        CachedImage smoothedCachedImage = FilteringEngine.filter(cachedImage, plan.getFlatSmoothWeights(), PathUtil.distinguishFile(cachedImage.getFile(), "-smooth"));
        System.out.println("Saved to " + smoothedCachedImage.getFile());
        return smoothedCachedImage;
    }

    public static CachedImage doFlatRotation(CachedImage cachedImage, ImageProcessConfig plan) {
        if (plan.getFlatRotation() == null) {
            return cachedImage;
        }
        System.out.println("=== Rotation flat " + cachedImage.getFile() + " ===");

        CachedImage rotatedCachedImage = RotateEngine.rotate(cachedImage, plan.getFlatRotation(), cachedImage.getFile());
        System.out.println("Saved to " + rotatedCachedImage.getFile());
        return rotatedCachedImage;
    }

    public static List<CachedImage> doSingleAlignment(CachedImage baseCachedImage, List<CachedImage> inputFiles, ImageProcessConfig plan) throws UnableAlignException {
        if (Thread.interrupted()) {
            throw new UnableAlignException("Interrupted");
        }
        File alignOutputDir = FileConstants.getAlignOutputDir(plan.getRootOutputDir());

        System.out.println("=== Aligning " + baseCachedImage.getFile() + " with " + inputFiles.size() + " files to " + alignOutputDir + " ===");
        alignOutputDir.mkdirs();
        ImFilesUtil.deleteDirContent(alignOutputDir);
        try {
            List<CachedImage> alignedCachedImage = SingleAligmentTask.align(baseCachedImage, inputFiles, alignOutputDir, plan.getSingleAligmentConfig());
            System.out.println("Saved to: " + alignOutputDir);
            return alignedCachedImage;
        } catch (IOException e) {
            throw new UnableAlignException("An IO error occured" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            throw new UnableAlignException("Interrupted");
        }
    }

    public static List<CachedImage> doMultipleAlignment(CachedImage baseCachedImage, List<CachedImage> inputFiles, ImageProcessConfig plan) throws UnableAlignException {
        if (Thread.interrupted()) {
            throw new UnableAlignException("Interrupted");
        }

        File alignOutputDir = FileConstants.getAlignOutputDir(plan.getRootOutputDir());

        System.out.println("=== Aligning " + baseCachedImage.getFile() + " with " + inputFiles.size() + " files ===");
        alignOutputDir.mkdirs();
        ImFilesUtil.deleteDirContent(alignOutputDir);
        try {
            plan.getMultiAligmentConfig().debug = plan.isDebug();
            List<CachedImage> cachedImages = MultiAligmentTask.alignImages(baseCachedImage, inputFiles,
                    alignOutputDir, plan.getMultiAligmentConfig());
            System.out.println("Saved to: " + alignOutputDir);
            return cachedImages;
        } catch (IOException e) {
            throw new UnableAlignException("An IO error occured" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            throw new UnableAlignException("Interrupted");
        }
    }

    public static CachedImage doStack(List<CachedImage> alignedImages, ImageProcessConfig plan) throws UnableStackException {
        File alignOutputDir = FileConstants.getAlignOutputDir(plan.getRootOutputDir());

        if (alignedImages.size() == 0) {
            throw new UnableStackException("No input files to stack. Please populate directory " + alignOutputDir + " with aligned images or start alignment first");
        }

        System.out.println("=== Stacking " + alignedImages.size() + " files ===");

        File stackFile = ImFilesUtil.replaceTo16bitFormatWhenNeeded(FileConstants.getStackOutputFile(plan.getRootOutputDir()));
        try {
            CachedImage cachedImage = StackingTask.stack(alignedImages, stackFile, plan.getStackingConfig());
            System.out.println("Saved to " + cachedImage.getFile());
            return cachedImage;
        } catch (IOException e) {
            throw new UnableStackException("An error occured during stacking" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            throw new UnableStackException("Interrupted");
        }
    }

    public static CachedImage doStackDarks(ImageProcessConfig plan) throws UnableStackException {
        List<CachedImage> darkImages = plan.getDarkFiles();

        if (darkImages.size() == 0) {
            throw new UnableStackException("No input dark files to stack");
        }

        System.out.println("=== Stacking Dark " + darkImages.size() + " files ===");

        File stackFile = null;
        try {
            stackFile = PathUtil.deriveFile(plan.getRootOutputDir(), FileConstants.DARK_SUBDIR, FileConstants.DARK_FILE_NAME);
        } catch (Exception e) {
            throw new UnableStackException("Unable to create output dark file");
        }
        try {
            CachedImage cachedImage = StackingTask.stack(darkImages, stackFile, new StackingConfig());
            System.out.println("Saved to " + cachedImage.getFile());
            return cachedImage;
        } catch (IOException e) {
            throw new UnableStackException("An error occured during stacking darks" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            throw new UnableStackException("Interrupted");
        }
    }

    public static CachedImage doStackFlats(List<CachedImage> flatImages, File inputDir) throws UnableStackException {
        if (flatImages.size() == 0) {
            throw new UnableStackException("No input flat files to stack");
        }

        System.out.println("=== Stacking Flat " + flatImages.size() + " files ===");

        File stackFile = null;
        try {
            stackFile = PathUtil.deriveFile(inputDir, FileConstants.FLAT_SUBDIR, FileConstants.FLAT_FILE_NAME);
        } catch (Exception e) {
            throw new UnableStackException("Unable to create output flat file");
        }
        try {
            CachedImage stackedCachedImage = StackingTask.stack(flatImages, stackFile, new StackingConfig());
            System.out.println("Saved to: " + stackedCachedImage.getFile());
            return stackedCachedImage;
        } catch (IOException e) {
            throw new UnableStackException("An error occured during stacking flats" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            throw new UnableStackException("Interrupted");
        }
    }

    public static void doAnimate(List<CachedImage> alignedImages, ImageProcessConfig plan) throws UnableAnimateException {
        File alignOutputDir = FileConstants.getAlignOutputDir(plan.getRootOutputDir());

        if (alignedImages.size() == 0) {
            throw new UnableAnimateException("No input files to animate. Please, populate directory " + alignOutputDir + " with aligned images or start alignment first");
        }

        System.out.println("=== Making animated video using " + alignedImages.size() + " files ===");

        try {
            CachedImage cachedImage = AnimCreationTask.animate(alignedImages, FileConstants.getAnimateOutputFile(plan.getRootOutputDir()), plan.getAnimCreationConfig());
            System.out.println("Saved to: " + cachedImage.getFile());
        } catch (IOException e) {
            new UnableStackException("An error occured during creating animated image" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        } catch (ProcessInterruptedException e) {
            new UnableStackException("Interrupted");
        }
    }

    public static CachedImage doPostprocess(CachedImage stackedImage, ImageProcessConfig plan) throws UnablePostprocessException {
        System.out.println("=== Postprocessing " + stackedImage.getFile() + " ===");
        List<PixelTransformer> postTransformers = plan.getPostTransformers();
        try {
            for (PixelTransformer pixelTransformer : postTransformers) {
                stackedImage = PostprocessTask.transformPixels(stackedImage, File.createTempFile("transforming-", ".tif"), pixelTransformer);
            }
            RasImageRead postprocessedImage = stackedImage.getRasImageRead();
            CachedImage cachedImage = CachedImage.saveAndCache(FileConstants.getPostprocessOutputFile(plan.getRootOutputDir()), postprocessedImage);
            System.out.println("Saved to " + cachedImage.getFile());
            return cachedImage;
        } catch (IOException|IllegalStateException e) {
            throw new UnablePostprocessException("An error occured during postprocess" + (e.getMessage() != null ? ": " + e.getMessage() : ""));
        }
    }
}
