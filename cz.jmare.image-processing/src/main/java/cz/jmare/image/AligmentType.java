package cz.jmare.image;

public enum AligmentType {
    /**
     * Using one dominant object for aligment
     */
    SINGLE, 
    /**
     * Use many objects for aligment like stars
     */
    MULTI
}
