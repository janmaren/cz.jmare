package cz.jmare.image;

import cz.jmare.image.align.audit.PointMatchAudit;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.ImPointInt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class DebugUtil {
    public static void saveImageShowCentroids(CachedImage cachedImage, CentroidsAnalyzer baseAnalyzer) throws IOException {
        List<Centroid> centroids = baseAnalyzer.centroidsSortedScoreDesc();
        List<ImPointInt> points = new ArrayList<>();
        for (int i = 0; i < centroids.size(); i++) {
            Centroid centroid = centroids.get(i);
            points.add(new ImPointInt((int) centroid.x, (int) centroid.y));
        }

        File outFile = getDebugFile(cachedImage, null, null);

        ImFilesUtil.save8BitImageForDebug(outFile, cachedImage.getRasImageRead(), points);
    }

    private static File getDebugFile(CachedImage cachedImage, Integer order, String hex) {
        File file = cachedImage.getFile();
        File dir = new File(file.getParentFile(), "debug");
        dir.mkdirs();
        int index = file.getName().lastIndexOf(".");
        String name = file.getName();
        if (index != -1) {
            name = name.substring(0, index);
        }
        File resultFile = new File(dir, name  + (hex != null ? "." + hex : "") + (order != null ? "." + order : "") + ".png");
        return resultFile;
    }

    public static String genHex() {
        return Integer.toHexString(ThreadLocalRandom.current().nextInt(100000, Integer.MAX_VALUE));
    }

    public static void saveImageShowMatches(CachedImage cachedImage, CentroidsAnalyzer baseAnalyzer, List<PointMatchAudit> attempt, int order, String hex) throws IOException {
        List<Centroid> centroids = baseAnalyzer.centroidsSortedScoreDesc();
        List<ImPointInt> points = new ArrayList<>();
        for (int i = 0; i < centroids.size(); i++) {
            Centroid centroid = centroids.get(i);
            points.add(new ImPointInt((int) centroid.x, (int) centroid.y));
        }

        File outFile = getDebugFile(cachedImage, order, hex);

        ImFilesUtil.save8BitImageForDebugMatches(outFile, cachedImage.getRasImageRead(), attempt);
    }
}
