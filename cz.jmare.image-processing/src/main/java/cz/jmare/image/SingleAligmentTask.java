package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import cz.jmare.image.align.SingleAlignRunnable;
import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.crop.Cropper;
import cz.jmare.image.crop.NullCropper;
import cz.jmare.image.crop.RadiusCentroidCropper;
import cz.jmare.image.filederive.FileDerivator;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

/**
 * Align images with one bigger object (no matching of some smaller objects used)
 */
public class SingleAligmentTask {

    public static List<File> align(File inputDir, File outputDir) throws IOException, UnableAlignException {
        return align(inputDir, outputDir, new SingleAligmentConfig());
    }
    
    public static List<File> align(File inputDir, File outputDir, SingleAligmentConfig aligmentConfig) throws IOException, UnableAlignException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        CachedImage baseCachedImage = null;
        if (filesListCached.size() < 2) {
            System.out.println("No files to align");
            throw new UnableAlignException("Nothing to align");
        }
        baseCachedImage = filesListCached.get(0);
        filesListCached.remove(0);
        
        List<CachedImage> alignImages = align(baseCachedImage, filesListCached, outputDir, aligmentConfig);
        return cachedToFiles(alignImages);
    }
    
    public static List<CachedImage> align(CachedImage baseFile, List<CachedImage> inputFiles, File outputDir, SingleAligmentConfig aligmentConfig) throws IOException, UnableAlignException {
        inputFiles = new ArrayList<>(inputFiles);
        List<CachedImage> outputs = new ArrayList<CachedImage>();
        if (baseFile == null) {
            baseFile = inputFiles.get(0);
            inputFiles.remove(0);
        } 
        
        if (inputFiles.size() < 1) {
            return List.of();
        }
        
        Float minPixelBrightness = aligmentConfig.getMinPixelBrightness();
        Cropper cropFactory = aligmentConfig.getCropFactory() != null ? aligmentConfig.getCropFactory() : new NullCropper();
        FileDerivator fileDerivator = aligmentConfig.getFileDerivator();
        
        RasImageRead baseImage = baseFile.getRasImageRead();
        CentroidsAnalyzer baseAnalyzer = new CentroidsAnalyzer(baseFile, minPixelBrightness != null ? minPixelBrightness : 0.5f);
        ArrayList<Centroid> baseList = new ArrayList<>(baseAnalyzer.centroids());
        Collections.sort(baseList, (o1, o2) -> Float.valueOf(o2.score).compareTo(o1.score));
        if (baseList.size() == 0) {
            throw new UnableAlignException("No base object");
        }
        Centroid baseCentroid = baseList.get(0);
        
        Function<RasImageRead, RasImageRead> alignedImageCropper = cropFactory.crop(baseCentroid, baseFile);
        
        // align all non base files
 
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        availableProcessors--;
        if (availableProcessors == 0) {
            availableProcessors = 1;
        }
        
        List<SingleAlignRunnable> aligns = new ArrayList<SingleAlignRunnable>();
        List<Thread> threads = new ArrayList<Thread>();
        int filesPerProc = inputFiles.size() / availableProcessors;
        int processed = 0;
        while (processed < inputFiles.size()) {
            List<CachedImage> sublist = inputFiles.subList(processed, processed + filesPerProc < inputFiles.size() ? processed + filesPerProc : inputFiles.size());
            SingleAlignRunnable alignRunnable = new SingleAlignRunnable(sublist, baseImage, baseCentroid, fileDerivator, outputDir, minPixelBrightness != null ? minPixelBrightness : 0.5f, alignedImageCropper);
            Thread thread = new Thread(alignRunnable);
            thread.start();
            threads.add(thread);
            aligns.add(alignRunnable);
            processed += filesPerProc;
        }

        // also base file crop
        RasImageRead baseImageCroped = alignedImageCropper.apply(baseImage);
        // and save
        File outputFileB = fileDerivator.derive(baseFile.getFile(), outputDir);
        ImFilesUtil.saveBestQualityImage(outputFileB, baseImageCroped);
        outputs.add(new CachedImage(outputFileB));
        
        // wait for all threads
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalStateException("Interrupted");
            }
        }
        
        // get all outputs
        for (SingleAlignRunnable alignRunnable : aligns) {
            outputs.addAll(alignRunnable.getOutputs());
        }
        
        return outputs;
    }
    
    public static List<File> cachedToFiles(List<CachedImage> cacheds) {
        return cacheds.stream().map(c -> c.getFile()).collect(Collectors.toList());
    }
    
    public static void main(String[] args) throws IOException, UnableAlignException {
        RadiusCentroidCropper centroidCrop = new RadiusCentroidCropper(50, 50);
        align(new File("C:\\temp\\siriusb\\tif"), new File("C:\\temp\\siriusb\\tif\\aligns"), new SingleAligmentConfig().cropFactory(centroidCrop));
    }
}
