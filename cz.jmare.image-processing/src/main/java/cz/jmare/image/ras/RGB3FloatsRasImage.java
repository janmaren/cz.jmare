package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

public class RGB3FloatsRasImage extends RasBufImage {
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        float[] pixelValue = getPixelValue(x, y);
        rgbFloat.red = pixelValue[0];
        rgbFloat.green = pixelValue[1];
        rgbFloat.blue = pixelValue[2];
        return rgbFloat;
    }

    /**
     * Optimalisation
     */
    @Override
    public RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        return RGBPixUtil.toRGBPix(getPixelValue(x, y), rgbPix);
    }

    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setPixelValue(x, y, rgb.red, rgb.green, rgb.blue);
    }

    public void setPixelValue(int x, int y,
            float r, float g, float b) {
        float data[] = new float[] { r, g, b };
        raster.setDataElements(x, y, data);
    }
    
    public float[] getPixelValue(int x, int y) {
        float[] buffer = new float[3];
        raster.getDataElements(x, y, buffer);
        return buffer;
    }
}
