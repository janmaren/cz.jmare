package cz.jmare.image.ras;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import cz.jmare.image.util.ImPathUtil;
import cz.jmare.image.util.PFMUtil;

public class RasImageFactory {
    public static List<RasImageLoader> loaders = new ArrayList<>();
    private static List<ImageConverter> converters = new ArrayList<>();

    static {
        registerDefaultLoaders();
    }
    
    public static RasImagePack get(File file) throws IOException {
        String[] strings = ImPathUtil.parseNameSuffix(file.toString());
        String format = strings[1].length() > 1 ? strings[1].substring(1) : "";
        
        for (ImageConverter imageConverter : converters) {
            if (!imageConverter.getSupportedFormats().contains(format)) {
                continue;
            }
            File tmpFile = File.createTempFile("img", ".tif");
            tmpFile.deleteOnExit();
            imageConverter.convert(file, tmpFile);
            for (RasImageLoader rasImageLoader : loaders) {
                if (!rasImageLoader.getSupportedFormats().contains("tif")) {
                    continue;
                }
                RasImage rasImage = rasImageLoader.getRasImage(tmpFile);
                return new RasImagePack(rasImage, tmpFile);
            }
        }
        
        for (RasImageLoader rasImageLoader : loaders) {
            if (!rasImageLoader.getSupportedFormats().contains(format)) {
                continue;
            }
            RasImage rasImage = rasImageLoader.getRasImage(file);
            return new RasImagePack(rasImage);
        }

        throw new IOException("Unable to load " + file);
    }
    
    public static void registerLoader(RasImageLoader rasImageLoader) {
        loaders.add(0, rasImageLoader);
    }
    
    public static void registerConverter(ImageConverter imageConverter) {
        converters.add(0, imageConverter);
    }

    public static boolean isConverterRegistered(Class<? extends ImageConverter> clazz) {
        for (ImageConverter converter : converters) {
            if (converter.getClass() == clazz) {
                return true;
            }
        }
        return false;
    }
    
    public static void registerDefaultLoaders() {
        loaders.add(new RasImageLoader() {
            @Override
            public RasImage getRasImage(File file) throws IOException {
                return RasBufImage.getInstance(file);
            }

            @Override
            public Set<String> getSupportedFormats() {
                HashSet<String> set = new HashSet<>(Set.of(ImageIO.getReaderFormatNames()));
                set.remove("FITS");
                set.remove("fits");
                set.remove("FIT");
                set.remove("fit");
                return set;
            }
        });
        
        loaders.add(new RasImageLoader() {
            @Override
            public RasImage getRasImage(File file) throws IOException {
                return PFMUtil.loadPFM(file);
            }

            @Override
            public Set<String> getSupportedFormats() {
                return Set.of("pfm");
            }
        });
    }
    
    public static void unregisterAll() {
        loaders.clear();
        converters.clear();
    }
    
    public static boolean isRas16Bit(RasImageRead rasImageRead) {
        return (rasImageRead instanceof RasImageFloat || rasImageRead instanceof RGB3ShortsRasImage);
    }
    
    /**
     * Get all supported formats for reading
     * @return
     */
    public static Set<String> getAllReadFormats() {
        Set<String> formats = new HashSet<String>();
        for (ImageConverter imageConverter : converters) {
            formats.addAll(imageConverter.getSupportedFormats());
        }
        for (RasImageLoader rasImageLoader : loaders) {
            formats.addAll(rasImageLoader.getSupportedFormats());
        }
        return formats;
    }
}
