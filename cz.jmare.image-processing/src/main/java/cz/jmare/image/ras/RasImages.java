package cz.jmare.image.ras;

import cz.jmare.image.util.ImPathUtil;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class RasImages {
    public static RasImage get(File file) throws IOException {
        String[] strings = ImPathUtil.parseNameSuffix(file.toString());
        String format = strings[1].length() > 1 ? strings[1].substring(1) : "";

        for (RasImageLoader rasImageLoader : RasImageFactory.loaders) {
            if (!rasImageLoader.getSupportedFormats().contains(format)) {
                continue;
            }
            return rasImageLoader.getRasImage(file);
        }

        throw new IOException("Unable to load " + file);
    }

    public static void registerLoader(RasImageLoader rasImageLoader) {
        RasImageFactory.loaders.add(0, rasImageLoader);
    }

    public static Integer getBitsPerPixel(RasImageRead rasImage, Integer defaultValue) {
        if (rasImage instanceof RasImageFloat) {
            return 32;
        }
        if (rasImage instanceof RasBufImage) {
            RasBufImage image = (RasBufImage) rasImage;
            BufferedImage bufferedImage = image.getBufferedImage();
            return bufferedImage.getColorModel().getPixelSize();
        }
        return defaultValue;
    }

    public static int getBitsPerPixel(RasImage rasImage) {
        return getBitsPerPixel(rasImage, Integer.MAX_VALUE);
    }
}
