package cz.jmare.image.ras;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RGBFloat {
    /**
     * Value for red color in range 0..1 including
     */
    public float red;

    /**
     * Value for green color in range 0..1 including
     */
    public float green;

    /**
     * Value for blue color in range 0..1 including
     */
    public float blue;

    /**
     * Value for alpha channel in range 0..255 including
     */
    public byte alpha;

    public RGBFloat() {
    }

    /**
     * Constructor with channel values 0..1 including
     * @param red
     * @param green
     * @param blue
     */
    public RGBFloat(float red, float green, float blue) {
        super();
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    /**
     * Constructor with channel values 0..1 including and alpha channel in range 0..255 including
     * @param alpha
     * @param red
     * @param green
     * @param blue
     */
    public RGBFloat(int alpha, float red, float green, float blue) {
        super();
        this.alpha = (byte) alpha;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getAlpha() {
        return alpha & 0xff;
    }

    @Override
    public String toString() {
        return
                "R: " + red +
                ", G: " + green +
                ", B: " + blue;
    }

    public String toRGB255String(int scale) {
        BigDecimal red = new BigDecimal(this.red * 255);
        if (red.scale() > scale) {
            red = red.setScale(scale, RoundingMode.HALF_UP);
        }
        BigDecimal green = new BigDecimal(this.green * 255);
        if (green.scale() > scale) {
            green = green.setScale(scale, RoundingMode.HALF_UP);
        }
        BigDecimal blue = new BigDecimal(this.blue * 255);
        if (blue.scale() > scale) {
            blue = blue.setScale(scale, RoundingMode.HALF_UP);
        }
        return red + ", " + green + ", " + blue;
    }
}
