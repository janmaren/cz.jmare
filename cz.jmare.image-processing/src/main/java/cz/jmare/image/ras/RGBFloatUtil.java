package cz.jmare.image.ras;

public class RGBFloatUtil {
    public static float getLuminance(RGBFloat rgbFloat) {
        float y = (0.2126f * rgbFloat.red + 0.7152f * rgbFloat.green + 0.0722f * rgbFloat.blue);
        if (y > 1) {
            y = 1;
        }
        return y;
    }

    public static float getBrightness(RGBFloat rgbFloat) {
        return (rgbFloat.red + rgbFloat.green + rgbFloat.blue) / 3f;
    }
}
