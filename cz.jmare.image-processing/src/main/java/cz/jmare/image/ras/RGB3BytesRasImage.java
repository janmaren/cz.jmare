package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

public class RGB3BytesRasImage extends RasBufImage {
    @Override
    public RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        int index = (y * width + x) * 3;
        rgbPix.red = dataBuffer.getElem(index);
        rgbPix.green = dataBuffer.getElem(index + 1);
        rgbPix.blue = dataBuffer.getElem(index + 2);
        return rgbPix;
    }

    @Override
    public void setRGB(int x, int y, RGBPix rgb) {
        int index = (y * width + x) * 3;
        dataBuffer.setElem(index, rgb.red);
        dataBuffer.setElem(index + 1, rgb.green);
        dataBuffer.setElem(index + 2, rgb.blue);
    }
    
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        return RGBPixUtil.toRGBFloat(getRGB(x, y, rgbPix), rgbFloat);
    }

    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setRGB(x, y, RGBPixUtil.toRGBPix(rgb));
    }
}
