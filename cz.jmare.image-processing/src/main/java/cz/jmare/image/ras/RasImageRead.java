package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

/**
 * Abstraction over various types of images
 */
public interface RasImageRead {
    int getHeight();

    int getWidth();

    default RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        return RGBPixUtil.toRGBPix(getRGBFloat(x, y), rgbPix);
    }

    RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix);

    default RGBFloat getRGBFloat(int x, int y) {
        return getRGBFloat(x, y, new RGBFloat(), new RGBPix());
    }
}