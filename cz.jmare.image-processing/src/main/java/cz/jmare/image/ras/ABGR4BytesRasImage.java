package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

public class ABGR4BytesRasImage extends RasBufImage {

    @Override
    public RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        int index = y * width * 4 + x * 4;
        rgbPix.alpha = dataBuffer.getElem(index);
        rgbPix.blue = dataBuffer.getElem(index + 1);
        rgbPix.green = dataBuffer.getElem(index + 2);
        rgbPix.red = dataBuffer.getElem(index + 3);
        return rgbPix;
    }

    @Override
    public void setRGB(int x, int y, RGBPix rgb) {
        int index = y * width * 4 + x * 4;
        dataBuffer.setElem(index, rgb.alpha);
        dataBuffer.setElem(index + 3, rgb.red);
        dataBuffer.setElem(index + 2, rgb.green);
        dataBuffer.setElem(index + 1, rgb.blue);
    }
    
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        return RGBPixUtil.toRGBFloat(getRGB(x, y, rgbPix), rgbFloat);
    }
    
    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setRGB(x, y, RGBPixUtil.toRGBPix(rgb));
    }
}
