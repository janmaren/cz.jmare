package cz.jmare.image.ras;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public interface RasImageLoader {
    RasImage getRasImage(File file) throws IOException;
    
    Set<String> getSupportedFormats();
}
