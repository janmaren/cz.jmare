package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

/**
 * Abstraction over various types of images
 */
public interface RasImageWrite {
    int getHeight();

    int getWidth();

    default void setRGB(int x, int y, RGBPix rgb) {
        setRGBFloat(x, y, RGBPixUtil.toRGBFloat(rgb));
    }
    
    void setRGBFloat(int x, int y, RGBFloat rgb);
}