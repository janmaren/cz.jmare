package cz.jmare.image.ras;

public class Grey2BytesRasImage extends RasBufImage {
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        short[] buffer = getPixelValue(x, y);
        float val = (0xffff & buffer[0]) / 65535f;
        rgbFloat.red = val;
        rgbFloat.green = val;
        rgbFloat.blue = val;
        return rgbFloat;
    }

    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setPixelValue(x, y, (short) (rgb.red * 0xffff));
    }

    public void setPixelValue(int x, int y, short val) {
        short data[] = new short[1];
        data[0] = val;
        raster.setDataElements(x, y, data);
    }
    
    public short[] getPixelValue(int x, int y) {
        short[] buffer = new short[1];
        raster.getDataElements(x, y, buffer);
        return buffer;
    }
}
