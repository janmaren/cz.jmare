package cz.jmare.image.ras;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public abstract class RasBufImage implements RasImage {
    protected DataBuffer dataBuffer;
    protected WritableRaster raster;
    protected BufferedImage bufferedImage;
    protected int width;
    protected int height;
    private static BufferedImage exampleBI16;

    private static BufferedImage exampleBIFloat;

    private static BufferedImage exampleBI16G;

    static {
        try {
            exampleBI16 = ImageIO.read(RasBufImage.class.getResourceAsStream("/min-48bits.tif"));
            exampleBIFloat = ImageIO.read(RasBufImage.class.getResourceAsStream("/min-float.tif"));
            exampleBI16G = ImageIO.read(RasBufImage.class.getResourceAsStream("/min-gr-16.tif"));
        } catch (IOException e) {
            throw new IllegalStateException("Unable load min-48bits.tif");
        }
    }
    
    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
    
    public static RasBufImage getInstance(File file) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        if (bufferedImage == null) {
            throw new IllegalArgumentException("File " + file + " is not supported");
        }
        try {
            return getInstance(bufferedImage);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Unable to recognize type " + bufferedImage.getType() + " in file " + file);
        }
    }
    
    public static RasBufImage getInstance(BufferedImage bufferedImage) {
        RasBufImage rasImage = null;
        if (bufferedImage.getType() == BufferedImage.TYPE_CUSTOM) {
            int[] componentSize = bufferedImage.getColorModel().getComponentSize();
            if (componentSize.length == 3 && componentSize[0] == 16 && componentSize[1] == 16 && componentSize[2] == 16) {
                rasImage = new RGB3ShortsRasImage();
            } else if (componentSize.length == 3 && componentSize[0] == 32 && componentSize[1] == 32 && componentSize[2] == 32) {
                rasImage = new RGB3FloatsRasImage();
            } else {
                if (bufferedImage.getColorModel().hasAlpha()) {
                    rasImage = new ARGB4BytesRasImage();
                } else {
                    rasImage = new RGB3BytesRasImage();
                }
            }
        } else if (bufferedImage.getType() == BufferedImage.TYPE_INT_RGB) {
            throw new IllegalStateException("Not supported type " + bufferedImage.getType()); // TODO: implement
            //rasImage = new RGB3BytesRasImage();
        } else if (bufferedImage.getType() == BufferedImage.TYPE_INT_ARGB) {
            throw new IllegalStateException("Not supported type " + bufferedImage.getType());
            //rasImage = new ARGBRasImage();
        } else if (bufferedImage.getType() == BufferedImage.TYPE_4BYTE_ABGR) {
            rasImage = new ABGR4BytesRasImage();
        } else if (bufferedImage.getType() == BufferedImage.TYPE_3BYTE_BGR) {
            rasImage = new BGR3BytesRasImage();
        } else if (bufferedImage.getType() == BufferedImage.TYPE_BYTE_GRAY) {
            rasImage = new Grey1ByteRasImage();
        } else if (bufferedImage.getType() == BufferedImage.TYPE_USHORT_GRAY) {
            rasImage = new Grey2BytesRasImage();
        } else {
            throw new IllegalArgumentException("Unsupported type of image of " + bufferedImage.getType());
        }
        rasImage.bufferedImage = bufferedImage;
        rasImage.raster = rasImage.bufferedImage.getRaster();
        rasImage.dataBuffer = rasImage.bufferedImage.getRaster().getDataBuffer();
        rasImage.width = bufferedImage.getWidth();
        rasImage.height = bufferedImage.getHeight();
        return rasImage;
    }

    public static RasBufImage createShortsInstance(int width, int height) {
        WritableRaster raster = exampleBI16.getRaster().createCompatibleWritableRaster(width, height);
        BufferedImage bufferedImage = new BufferedImage(exampleBI16.getColorModel(), raster, false, null);
        
        RasBufImage rgb3ShortsRasImage = new RGB3ShortsRasImage();
        rgb3ShortsRasImage.dataBuffer = raster.getDataBuffer();
        rgb3ShortsRasImage.raster = raster;
        rgb3ShortsRasImage.bufferedImage = bufferedImage;
        rgb3ShortsRasImage.width = width;
        rgb3ShortsRasImage.height = height;
        return rgb3ShortsRasImage;
    }

    public static RasBufImage createGreyShortsInstance(int width, int height) {
        WritableRaster raster = exampleBI16G.getRaster().createCompatibleWritableRaster(width, height);
        BufferedImage bufferedImage = new BufferedImage(exampleBI16G.getColorModel(), raster, false, null);

        RasBufImage rasImage = new Grey2BytesRasImage();
        rasImage.dataBuffer = raster.getDataBuffer();
        rasImage.raster = raster;
        rasImage.bufferedImage = bufferedImage;
        rasImage.width = width;
        rasImage.height = height;
        return rasImage;
    }

    public static RasBufImage createFloatsInstance(int width, int height) {
        WritableRaster raster = exampleBIFloat.getRaster().createCompatibleWritableRaster(width, height);
        BufferedImage bufferedImage = new BufferedImage(exampleBIFloat.getColorModel(), raster, false, null);

        RasBufImage rgb3ShortsRasImage = new RGB3FloatsRasImage();
        rgb3ShortsRasImage.dataBuffer = raster.getDataBuffer();
        rgb3ShortsRasImage.raster = raster;
        rgb3ShortsRasImage.bufferedImage = bufferedImage;
        rgb3ShortsRasImage.width = width;
        rgb3ShortsRasImage.height = height;
        return rgb3ShortsRasImage;
    }
    
    public static RasBufImage createBytesInstance(int width, int height, boolean withAlpha) {
        int type;
        RasBufImage rasBufferedImage = null;
        if (withAlpha) {
            type = BufferedImage.TYPE_4BYTE_ABGR;
            rasBufferedImage = new ABGR4BytesRasImage();
        } else {
            type = BufferedImage.TYPE_3BYTE_BGR;
            rasBufferedImage = new BGR3BytesRasImage();
        }
        BufferedImage bufferedImage = new BufferedImage(width, height, type);
        rasBufferedImage.bufferedImage = bufferedImage;
        rasBufferedImage.dataBuffer = rasBufferedImage.bufferedImage.getRaster().getDataBuffer();
        rasBufferedImage.raster = rasBufferedImage.bufferedImage.getRaster();
        rasBufferedImage.width = width;
        rasBufferedImage.height = height;
        return rasBufferedImage;
    }
    
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public static void main(String[] args) throws IOException {
//        BufferedImage read = ImageIO.read(new File("c:\\temp\\test.bmp"));
//        boolean alphaPremultiplied = read.isAlphaPremultiplied();
//        RGBPix rHighestToRGB = RGBPixUtil.arHighestToRGB(read.getRGB(0, 0));
//        System.out.println(rHighestToRGB);
//        
//        System.out.println(read.getType());
        
        RenderedImage read = new BufferedImage(50, 100, BufferedImage.TYPE_3BYTE_BGR);
        ImageIO.write(read, "tif", new File("c:\\temp\\testout.tif"));
//        
//        read = ImageIO.read(new File("c:\\temp\\testout.tif"));
//        alphaPremultiplied = read.isAlphaPremultiplied();
//        rHighestToRGB = RGBPixUtil.arHighestToRGB(read.getRGB(0, 0));
//        System.out.println(rHighestToRGB);
    }
}
