package cz.jmare.image.ras;

public class RGB3ShortsRasImage extends RasBufImage {
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        short[] buffer = getPixelValue(x, y);
        rgbFloat.red = (0xffff & buffer[0]) / 65535f;
        rgbFloat.green = (0xffff & buffer[1]) / 65535f;
        rgbFloat.blue = (0xffff & buffer[2]) / 65535f;
        return rgbFloat;
    }

    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setPixelValue(x, y, (short) (rgb.red * 0xffff), (short) (rgb.green * 0xffff), (short) (rgb.blue * 0xffff));
    }

    public void setPixelValue(int x, int y,
            short r, short g, short b) {
        short data[] = new short[] { r, g, b };
        raster.setDataElements(x, y, data);
    }
    
    public short[] getPixelValue(int x, int y) {
        short[] buffer = new short[3];
        raster.getDataElements(x, y, buffer);
        return buffer;
    }
}
