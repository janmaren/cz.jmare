package cz.jmare.image.ras;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public interface ImageConverter {
    void convert(File inputFile, File outputFile) throws IOException;
    
    Set<String> getSupportedFormats();
}
