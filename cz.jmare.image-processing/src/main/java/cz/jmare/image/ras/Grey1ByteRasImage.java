package cz.jmare.image.ras;

import cz.jmare.image.util.RGBPixUtil;

public class Grey1ByteRasImage extends RasBufImage {

    @Override
    public RGBPix getRGB(int x, int y, RGBPix rgbPix) {
        int index = y * width + x;
        int value = dataBuffer.getElem(index);
        rgbPix.red = value;
        rgbPix.green = value;
        rgbPix.blue = value;
        return rgbPix;
    }

    @Override
    public void setRGB(int x, int y, RGBPix rgb) {
        int index = y * width + x;
        dataBuffer.setElem(index, rgb.red);
    }
    
    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        return RGBPixUtil.toRGBFloat(getRGB(x, y, rgbPix), rgbFloat);
    }
    
    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        setRGB(x, y, RGBPixUtil.toRGBPix(rgb));
    }

}
