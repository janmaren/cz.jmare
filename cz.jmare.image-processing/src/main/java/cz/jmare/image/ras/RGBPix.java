package cz.jmare.image.ras;

public class RGBPix {
    public int alpha = 255;
    
    public int red;

    public int green;

    public int blue;

    public RGBPix() {
    }

    public RGBPix(int red, int green, int blue) {
        super();
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public RGBPix(int alpha, int red, int green, int blue) {
        super();
        this.alpha = alpha;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
    
    public RGBPix(String str) {
        if (str.startsWith("#")) {
            red = hex2decimal(str.substring(1, 3));
            green = hex2decimal(str.substring(3, 5));
            blue = hex2decimal(str.substring(5, 7));
        } else {
            String[] split = str.trim().split(",");
            if (split.length != 3) {
                throw new IllegalArgumentException("String " + str + " hasn't 3 numbers");
            }
            red = Integer.parseInt(split[0].trim());
            green = Integer.parseInt(split[1].trim());
            blue = Integer.parseInt(split[2].trim());
        }
    }

    public float getBrightness() {
        return (red + blue + green) / 3f;
    }
    
    @Override
    public String toString() {
        return "[r:" + red + ", g:" + green + ", b:" + blue + "]";
    };

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + blue;
        result = prime * result + green;
        result = prime * result + red;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RGBPix other = (RGBPix) obj;
        if (blue != other.blue)
            return false;
        if (green != other.green)
            return false;
        if (red != other.red)
            return false;
        return true;
    }
}
