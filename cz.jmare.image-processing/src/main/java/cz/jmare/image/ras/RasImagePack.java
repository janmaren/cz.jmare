package cz.jmare.image.ras;

import java.io.File;

public class RasImagePack {
    private RasImage rasImage;
    
    /**
     * Nonnull when image converted to another file
     */
    private File inFile;

    public RasImagePack(RasImage rasImage, File inFile) {
        super();
        this.rasImage = rasImage;
        this.inFile = inFile;
    }

    public RasImagePack(RasImage rasImage) {
        super();
        this.rasImage = rasImage;
    }

    public RasImage getRasImage() {
        return rasImage;
    }

    public File getInFile() {
        return inFile;
    }
}
