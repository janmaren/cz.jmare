package cz.jmare.image.ras;

public class RasImageFloat implements RasImage {
    private int width;
    
    private int height;
    
    private float[][] r;
    
    private float[][] g;
    
    private float[][] b;
    
    private byte[][] alpha;
    
    public RasImageFloat(int width, int height) {
        super();
        this.width = width;
        this.height = height;
        r = new float[height][];
        g = new float[height][];
        b = new float[height][];
        alpha = new byte[height][];
        for (int j = 0; j < height; j++) {
            r[j] = new float[width];
            g[j] = new float[width];
            b[j] = new float[width];
            alpha[j] = new byte[width];
        }
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public RGBFloat getRGBFloat(int x, int y, RGBFloat rgbFloat, RGBPix rgbPix) {
        rgbFloat.alpha = (byte) (alpha[y][x] & 0xff);
        rgbFloat.red = r[y][x];
        rgbFloat.green = g[y][x];
        rgbFloat.blue = b[y][x];
        return rgbFloat;
    }

    @Override
    public void setRGBFloat(int x, int y, RGBFloat rgb) {
        r[y][x] = rgb.red;
        g[y][x] = rgb.green;
        b[y][x] = rgb.blue;
        alpha[y][x] = rgb.alpha;
    }
    
    public void setRGBFloat(int x, int y, float rr, float gg, float bb) {
        r[y][x] = rr;
        g[y][x] = gg;
        b[y][x] = bb;
    }
}
