package cz.jmare.image;

import java.util.function.Supplier;

import cz.jmare.image.crop.Cropper;
import cz.jmare.image.crop.RadiusCentroidCropper;
import cz.jmare.image.filederive.FileDerivator;
import cz.jmare.image.filederive.SimpleFileDerivator;
import cz.jmare.image.resample.NearestNeighbourResampler;
import cz.jmare.image.resample.Resampler;

public class SingleAligmentConfig {
    private float minPixelBrightness = 0.5f;
    private Cropper cropFactory;
    private FileDerivator fileDerivator = new SimpleFileDerivator("tif");
    /**
     * Factory for resampler because each resampling needs own instance.
     */
    private Supplier<Resampler> resamplerProvider = NearestNeighbourResampler::new;
    
    
    public SingleAligmentConfig minPixelBrightness(float minPixelBrightness) {
        this.minPixelBrightness = minPixelBrightness;
        return this;
    }
    
    public SingleAligmentConfig cropFactory(Cropper cropFactory) {
        this.cropFactory = cropFactory;
        return this;
    }
    
    public SingleAligmentConfig fileDerivator(FileDerivator fileDerivator) {
        this.fileDerivator = fileDerivator;
        return this;
    }

    public SingleAligmentConfig resamplerProvider(Supplier<Resampler> resamplerProvider) {
        this.resamplerProvider = resamplerProvider;
        return this;
    }
    
    public float getMinPixelBrightness() {
        return minPixelBrightness;
    }

    public Cropper getCropFactory() {
        return cropFactory;
    }

    public FileDerivator getFileDerivator() {
        return fileDerivator;
    }
    
    public Supplier<Resampler> getResamplerProvider() {
        return resamplerProvider;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("brightness at least: " + (minPixelBrightness * 100) + "%");
        if (cropFactory != null) {
            sb.append(", crop: ");
            if (cropFactory instanceof RadiusCentroidCropper) {
                RadiusCentroidCropper radiusCentroidCropper = (RadiusCentroidCropper) cropFactory;
                sb.append("around centre of obj: x left, right: ").append(radiusCentroidCropper.getCropRadiusX());
                sb.append(" y up, down: ").append(radiusCentroidCropper.getCropRadiusY());
            }
        }
        return sb.toString();
    }
}
