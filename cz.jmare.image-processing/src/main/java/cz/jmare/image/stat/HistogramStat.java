package cz.jmare.image.stat;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class HistogramStat {
    public static int[] hist255(RasImageRead rasImageRead) {
        int[] hist = new int[256];
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(i, j);
                float brightness = brightness(rgbFloat);
                int index = Math.round(brightness * 255);
                hist[index]++;
            }
        }
        return hist;
    }
    
    public static int[] hist65535(RasImageRead rasImageRead) {
        int[] hist = new int[65536];
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(i, j);
                float brightness = brightness(rgbFloat);
                int index = Math.round(brightness * 65535);
                hist[index]++;
            }
        }
        return hist;
    }
    
    public static int invCdfValue(int[] hist, int number) {
        int sum = 0;
        int size = hist.length;
        for (int i = 0; i < size; i++) {
            sum += hist[i];
            if (sum >= number) {
                return i;
            }
        }
        return size - 1;
    }
    
    public static float invCdfValueFloat(RasImageRead rasImageRead, float value) {
        if (value > 1) {
            throw new IllegalArgumentException("Value must be in range 0..1");
        }
        int[] hist65535 = hist65535(rasImageRead);
        int count = rasImageRead.getHeight() * rasImageRead.getWidth();
        int bright = invCdfValue(hist65535, (int) (value * count));
        return (float) bright / 65535;
    }
    
    public static float invCdfValueFloat(int[] hist65535, int cumulativeCount) {
        int bright = invCdfValue(hist65535, cumulativeCount);
        return (float) bright / 65535;
    }
    
    public static float brightness(RGBFloat rgb) {
        return (rgb.red + rgb.green + rgb.blue) / 3f;
    }

    public static int sumToExcluding(int[] hist, int to) {
        int sum = 0;
        for (int i = 0; i < to; i++) {
            sum += hist[i];
        }
        return sum;
    }

    public static int sumToIncluding(int[] hist, int to) {
        int sum = 0;
        for (int i = 0; i <= to; i++) {
            sum += hist[i];
        }
        return sum;
    }

    public static int sumFromIncluding(int[] hist, int from) {
        int sum = 0;
        for (int i = from; i < hist.length; i++) {
            sum += hist[i];
        }
        return sum;
    }

    public static int sumFromExcluding(int[] hist, int from) {
        int sum = 0;
        for (int i = from + 1; i < hist.length; i++) {
            sum += hist[i];
        }
        return sum;
    }

    public static int sum(int[] hist) {
        int sum = 0;
        for (int i = 0; i < hist.length; i++) {
            sum += hist[i];
        }
        return sum;
    }

    public static float calcPercentilByThreshold(RasImageRead rasImageRead, float threshold) {
        int[] ints = hist65535(rasImageRead);
        int number = sumToExcluding(ints, (int) (threshold * 65535));
        return (float) number / (rasImageRead.getWidth() * rasImageRead.getHeight());
    }

    public static float calcThresholdByPercentil(RasImageRead rasImageRead, float percentil) {
        int[] ints = hist65535(rasImageRead);
        float number = percentil * (rasImageRead.getWidth() * rasImageRead.getHeight());
        int sum = 0;
        int i = 0;
        for (; i < ints.length; i++) {
            if (number < sum) {
                break;
            }
            sum += ints[i];
        }
        if (i >= ints.length) {
            return 1f;
        }
        return i / 65535f;
    }

    public static float calcPercentilByThreshold(int[] ints, int value) {
        int number = sumToExcluding(ints, value);
        int number2 = sumFromIncluding(ints, value);
        return (float) number / (number + number2);
    }

    public static int calcThresholdByPercentil(int[] ints, float percentil) {
        float number = percentil * sum(ints);
        int sum = 0;
        int i = 0;
        for (; i < ints.length; i++) {
            sum += ints[i];
            if (number < sum) {
                break;
            }
        }
        if (i >= ints.length) {
            return ints.length - 1;
        }
        return i;
    }
}
