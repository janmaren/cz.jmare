package cz.jmare.image.hotpixel;

import cz.jmare.image.ras.RasImageRead;

public interface HotpixelDecider {
    boolean isHotPixel(RasImageRead rasImageRead, int x, int y);
}
