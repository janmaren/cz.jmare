package cz.jmare.image.hotpixel;

import cz.jmare.image.ImageProcessConfig;
import cz.jmare.image.exception.UnableSubtrackException;
import cz.jmare.image.util.CachedImage;

public class HotpixelEngine {

    public static final float MATCH_THRESHOLD = 0.55f;

    public static void checkHotpixels(CachedImage baseCachedImage, ImageProcessConfig plan, float[][] hotpixels) throws UnableSubtrackException {
        Float score = HotpixelChecker.hotxpixelsMatch(baseCachedImage.getRasImageRead(), hotpixels);
        System.out.println("Dark frame hot pixels match with base by " + (score != null ? 100 * score : "N/A") + "%");
        if (score != null && score < MATCH_THRESHOLD) {
            if (plan.isNoHotPixelsMatchFail()) {
                throw new UnableSubtrackException("Dark frame not match to base image (comparing hot pixels)");
            } else {
                System.out.println("The dark frame not matches! Probably wrong rotation");
            }
        }
    }
}
