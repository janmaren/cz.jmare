package cz.jmare.image.hotpixel;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class HotpixelChecker {

    public static final float DEVIATION_THRESHOLD = 0.035f;

    public static Float hotxpixelsMatch(RasImageRead rasImageRead, float[][] hotpixels) {
        int width = hotpixels[0].length;
        int height = hotpixels.length;
        int count = 0;
        int all = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                float darkBrightness = hotpixels[i][j];
                if (darkBrightness <= 0) {
                    continue;
                }
                float score = score(rasImageRead, j, i, hotpixels);
                all++;
                if (score > DEVIATION_THRESHOLD) {
                    count++;
                }
            }
        }
        if (all == 0) {
            return null;
        }
        return (float) count / all;
    }

    private static float score(RasImageRead rasImageRead, int x, int y, float[][] hotpixels) {
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        RGBFloat rgbFloat = rasImageRead.getRGBFloat(x, y);
        float number = 0;
        float sumSq = 0;
        if (y > 0) {
            // top row
            if (x > 0 && hotpixels[y - 1][x - 1] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x - 1, y - 1), rgbFloat);
                number += 3;
            }
            if (hotpixels[y - 1][x] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x, y - 1), rgbFloat);
                number += 3;
            }
            if (x < width - 1 && hotpixels[y - 1][x + 1] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x + 1, y - 1), rgbFloat);
                number += 3;
            }
        }

        // middle row
        if (x > 0 && hotpixels[y][x - 1] <= 0) {
            sumSq += calcDev(rasImageRead.getRGBFloat(x - 1, y), rgbFloat);
            number += 3;
        }
        if (x < width - 1 && hotpixels[y][x + 1] <= 0) {
            sumSq += calcDev(rasImageRead.getRGBFloat(x + 1, y), rgbFloat);
            number += 3;
        }

        if (y < height - 1) {
            // bottom row

            if (x > 0 &&  hotpixels[y + 1][x - 1] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x - 1, y + 1), rgbFloat);
                number += 3;
            }
            if (hotpixels[y + 1][x] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x, y + 1), rgbFloat);
                number += 3;
            }
            if (x < width - 1 && hotpixels[y + 1][x + 1] <= 0) {
                sumSq += calcDev(rasImageRead.getRGBFloat(x + 1, y + 1), rgbFloat);
                number += 3;
            }
        }
        if (number == 0) {
            return 1;
        }

        float stdSquare = (float) Math.sqrt(sumSq / number);
        return stdSquare;
    }

    public static float calcDev(RGBFloat rgbFloat1, RGBFloat rgbFloat2) {
        float r = rgbFloat1.red - rgbFloat2.red;
        float g = rgbFloat1.green - rgbFloat2.green;
        float b = rgbFloat1.blue - rgbFloat2.blue;

        return r * r + g * g + b * b;
    }
}
