package cz.jmare.image.hotpixel;

import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.ArrayUtil;
import cz.jmare.image.util.CachedImage;

import java.io.File;
import java.util.Locale;

public class HotpixelDetector {
    public static float[][] detectHotpixels(CachedImage inputImage, HotpixelDecider hotPixelDecider) {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        float[][] array = ArrayUtil.create2dFloatArray(width, height);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (hotPixelDecider.isHotPixel(rasImageRead, j, i)) {
                    array[i][j] = 1; //RGBFloatUtil.getBrightness(rasImageRead.getRGBFloat(j, i));
                }
            }
        }
        return array;
    }

    public static void debugArray(float[][] hotpixels) {
        int width = hotpixels[0].length;
        int height = hotpixels.length;
        int count = 0;
        for (int i = 0; i < height; i++) {
            boolean present = false;
            for (int j = 0; j < width; j++) {
                if (hotpixels[i][j] > 0) {
                    System.out.print(j + ", " + i + "\t");
                    count++;
                    present = true;
                }
            }
            if (present) {
                System.out.println();
            }
        }
        int number = (width * height);
        System.out.println("Count is " + count + " of " + (width * height) + " which is " + (count * 100.0 / number) + "%");
    }

    public static void printInfo(float[][] hotpixels) {
        int width = hotpixels[0].length;
        int height = hotpixels.length;
        int count = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (hotpixels[i][j] > 0) {
                    count++;
                }
            }
        }
        int number = (width * height);
        System.out.println("Hotpixel removing count is " + count + " of " + (width * height) + " which is " + String.format(Locale.ENGLISH, "%.2f", count * 100.0 / number) + "%");
    }

    public static void main(String[] args) {
        var input = new CachedImage(new File("C:\\Users\\jmarencik\\temp\\hot\\DSC_0096.tiff"));
        float[][] hotpixels = detectHotpixels(input, new SimpleThresholdHotpixelDecider(0.04f));
        debugArray(hotpixels);
        InterByFilterEngine.interpolate(input, new File("C:\\Users\\jmarencik\\temp\\hot\\DSC_0096-out.tiff"), hotpixels, null);
    }
}
