package cz.jmare.image.hotpixel;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class SimpleThresholdHotpixelDecider implements HotpixelDecider {
    private float threshold = 0.05f;

    public SimpleThresholdHotpixelDecider(float threshold) {
        this.threshold = threshold;
    }

    public SimpleThresholdHotpixelDecider() {
    }

    @Override
    public boolean isHotPixel(RasImageRead rasImageRead, int x, int y) {
        RGBFloat rgbFloat = rasImageRead.getRGBFloat(x, y);
        return (rgbFloat.red + rgbFloat.green + rgbFloat.blue) / 3f > threshold;
    }
}
