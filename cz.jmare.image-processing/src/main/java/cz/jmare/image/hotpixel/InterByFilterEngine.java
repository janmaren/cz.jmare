package cz.jmare.image.hotpixel;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.ras.RasImageWrite;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImConversionsUtil;

import java.io.File;

public class InterByFilterEngine {
    public static CachedImage interpolate(CachedImage inputImage, File outputFile, float[][] pixelsToInterpolate, Integer dim) {
        RasImageRead rasImage = inputImage.getRasImageRead();

        int height = rasImage.getHeight();
        int width = rasImage.getWidth();
        RasBufImage outputImage = RasBufImage.createFloatsInstance(width, height);

        interpolate(rasImage, outputImage, pixelsToInterpolate, dim);

        CachedImage outputCachedImage = CachedImage.saveAndCache(outputFile, outputImage);
        return outputCachedImage;
    }

    public static void interpolate(RasImageRead input, RasImageWrite output, float[][] hotpixels, Integer dim) {
        int height = input.getHeight();
        int width = input.getWidth();

        ImConversionsUtil.copy(input, output);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (hotpixels[y][x] > 0) {
                    RGBFloat rgbFloat = null;
                    if (dim != null) {
                        rgbFloat = interpolatePix(input, x, y, hotpixels, dim);
                    } else {
                        rgbFloat = interpolatePix(input, x, y, hotpixels, 3);
                        if (rgbFloat == null) {
                            rgbFloat = interpolatePix(input, x, y, hotpixels, 5);
                        }
                        if (rgbFloat == null) {
                            rgbFloat = interpolatePix(input, x, y, hotpixels, 7);
                        }
                    }
                    if (rgbFloat != null) {
                        output.setRGBFloat(x, y, rgbFloat);
                    }
                }
            }
        }
    }

    static RGBFloat interpolatePix(RasImageRead rasImageRead, int interpX, int interpY, float[][] pixelsToInterpolate, int dim) {
        int wheight = dim;
        int wwidth = dim;
        int cx = wwidth / 2;
        int cy = wheight / 2;
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();

        float r = 0;
        float g = 0;
        float b = 0;
        float wsum = 0;
        for (int k = 0; k < wheight; k++) {
            for (int l = 0; l < wwidth; l++) {
                int x = interpX - cx + l;
                if (x < 0 || x >= width) {
                    continue;
                }
                int y = interpY - cy + k;
                if (y < 0 || y >= height) {
                    continue;
                }
                if (pixelsToInterpolate[y][x] > 0) {
                    continue;
                }
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(x, y);
                r += rgbFloat.red;
                g += rgbFloat.green;
                b += rgbFloat.blue;
                wsum++;
            }
        }
        if (wsum == 0) {
            return null;
        }

        float red = r / wsum;

        float green = g / wsum;

        float blue = b / wsum;

        return new RGBFloat(red, green, blue);
    }
}