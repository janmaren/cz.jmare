package cz.jmare.image.util;

public class ImPointInt {
    public int x;
    public int y;
    public ImPointInt(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }
}
