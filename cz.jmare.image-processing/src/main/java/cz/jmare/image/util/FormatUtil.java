package cz.jmare.image.util;

import java.util.Locale;

public class FormatUtil {
    public static String toPercents(float f) {
        return String.format(Locale.ENGLISH, "%.2f", f * 100) + "%";
    }

    public static String multi100(float f) {
        return String.format(Locale.ENGLISH, "%.2f", f * 100);
    }

    public static String number(float f) {
        return String.format(Locale.ENGLISH, "%.2f", f);
    }
}
