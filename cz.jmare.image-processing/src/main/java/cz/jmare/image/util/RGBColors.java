package cz.jmare.image.util;

import cz.jmare.image.ras.RGBPix;

public class RGBColors {
    /**
     * Color to grey
     * @param rgb
     * @return
     */
    public static int toGreyComponent(RGBPix rgb) {
        return (int)(rgb.red * 0.299 + rgb.green * 0.587 + rgb.blue * 0.114);
    }
    
    /**
     * Intensity in range 0-1
     * @param rgb
     * @return
     */
    public static float intensity(RGBPix rgb) {
        int greyComponent = toGreyComponent(rgb);
        float[] hsl = HSLUtil.rgbToHsl(greyComponent, greyComponent, greyComponent);
        return hsl[2];
    }
    
    public static void main(String[] args) {
        float intensity = intensity(new RGBPix(191, 16, 16));
        System.out.println(intensity);
    }
}
