package cz.jmare.image.util;

public interface Logger {
    void log(String message);
}
