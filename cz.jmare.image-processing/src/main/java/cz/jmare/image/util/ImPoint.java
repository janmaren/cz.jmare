package cz.jmare.image.util;

public class ImPoint {
    public double x;

    public double y;

    public ImPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "ImPoint [x=" + x + ", y=" + y + "]";
    }
    
    public double distance(ImPoint point) {
        double dx = point.x - x;
        double dy = point.y - y;
        return Math.sqrt(dx * dx + dy * dy);
    }
}
