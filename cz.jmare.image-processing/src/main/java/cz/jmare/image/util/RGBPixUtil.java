package cz.jmare.image.util;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RGBPix;

public class RGBPixUtil {
    public static RGBPix arHighestToRGB(int rgb) {
        int alpha = (rgb >> 24) & 0xff;
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xFF;
        return new RGBPix(alpha, red, green, blue);
    }

    public static RGBPix arHighestToRGB(int rgb, RGBPix rgbPix) {
        rgbPix.alpha = (rgb >> 24) & 0xff;
        rgbPix.red = (rgb >> 16) & 0xff;
        rgbPix.green = (rgb >> 8) & 0xff;
        rgbPix.blue = rgb & 0xFF;
        return rgbPix;
    }
    
    public static RGBPix rHighestToRGB(int rgb) {
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xFF;
        return new RGBPix(red, green, blue);
    }

    public static RGBPix rHighestToRGB(int rgb, RGBPix rgbPix) {
        rgbPix.red = (rgb >> 16) & 0xff;
        rgbPix.green = (rgb >> 8) & 0xff;
        rgbPix.blue = rgb & 0xFF;
        return rgbPix;
    }
    
    public static RGBPix arLowestToRGB(int rgb) {
        int alpha = (rgb >> 24) & 0xff;
        int blue = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int red = rgb & 0xFF;
        return new RGBPix(alpha, red, green, blue);
    }
    
    public static RGBPix rLowestToRGB(int rgb) {
        int blue = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int red = rgb & 0xFF;
        return new RGBPix(red, green, blue);
    }

    public static RGBPix rLowestToRGB(int rgb, RGBPix rgbPix) {
        rgbPix.blue = (rgb >> 16) & 0xff;
        rgbPix.green = (rgb >> 8) & 0xff;
        rgbPix.red = rgb & 0xFF;
        return rgbPix;
    }
    
    public static int rgbToRHighest(RGBPix pix) {
        return pix.red << 16 | pix.green << 8 | pix.blue;
    }
    
    public static int rgbToRLowest(RGBPix pix) {
        return pix.blue << 16 | pix.green << 8 | pix.red;
    }
    
    public static int argbToRLowest(RGBPix pix) {
        return pix.alpha << 24 | pix.blue << 16 | pix.green << 8 | pix.red;
    }
    
    public static int argbToRHighest(RGBPix pix) {
        return pix.alpha << 24 | pix.red << 16 | pix.green << 8 | pix.blue;
    }
    
    public static String toRGBHexString(RGBPix rgb) {
        return "#" + numberToHex(rgb.red) + numberToHex(rgb.green) + numberToHex(rgb.blue);
    }

    private static String numberToHex(int number) {
        String str = number < 16 ? "0" : "";
        return str + Integer.toHexString(number);
    }
    
    public static RGBPix toRGBPix(RGBFloat rgbFloat) {
        return new RGBPix(rgbFloat.getAlpha(), (int) (rgbFloat.red * 255f), (int) (rgbFloat.green * 255f), (int) (rgbFloat.blue * 255f));
    }

    public static RGBPix toRGBPix(RGBFloat rgbFloat, RGBPix rgbPix) {
        rgbPix.alpha = rgbFloat.getAlpha();
        rgbPix.red = (int) (rgbFloat.red * 255f);
        rgbPix.green = (int) (rgbFloat.green * 255f);
        rgbPix.blue = (int) (rgbFloat.blue * 255f);
        return rgbPix;
    }

    public static RGBPix toRGBPix(float[] rgb, RGBPix rgbPix) {
        rgbPix.red = (int) (rgb[0] * 255f);
        rgbPix.green = (int) (rgb[1] * 255f);
        rgbPix.blue = (int) (rgb[2] * 255f);
        return rgbPix;
    }
    
    public static RGBFloat toRGBFloat(RGBPix rgbPix) {
        return new RGBFloat(rgbPix.alpha, rgbPix.red / 255f, rgbPix.green / 255f, rgbPix.blue / 255f);
    }

    public static RGBFloat toRGBFloat(RGBPix rgbPix, RGBFloat rgbFloat) {
        rgbFloat.red = rgbPix.red / 255f;
        rgbFloat.green = rgbPix.green / 255f;
        rgbFloat.blue = rgbPix.blue / 255f;
        rgbFloat.alpha = (byte) rgbPix.alpha;
        return rgbFloat;
    }
}
