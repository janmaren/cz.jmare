package cz.jmare.image.util;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;

import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImagePack;
import cz.jmare.image.ras.RasImageRead;

/**
 * Main class holding path to file (mandatory) and providing RasImageRead which will be loaded lazilly when not present in memory (firstly loading or was garbage collected)<br>
 * The file with image must exist but may be used later.
 */
public class CachedImage {
    private File file;
    /**
     * Nullable real file. When populated it must be used instead of {@link #file}
     */
    private File internalFile;
    
    private SoftReference<RasImageRead> rasImageReadRef;

    /**
     * Create instance with image saved in a file. Such file must exist.
     * @param file existing file which contains an image
     */
    public CachedImage(File file) {
        this.file = file;
        if (!file.isFile()) {
            throw new IllegalArgumentException("File " + file + " doesn't exist");
        }
    }

    public synchronized RasImageRead getRasImageRead() {
        RasImageRead rir = rasImageReadRef == null ? null : rasImageReadRef.get();
        if (rir == null) {
            try {
                if (internalFile != null) {
                    RasImagePack rasImagePack = RasImageFactory.get(internalFile);
                    rir = rasImagePack.getRasImage();
                } else {
                    RasImagePack rasImagePack = RasImageFactory.get(file);
                    rir = rasImagePack.getRasImage();
                    internalFile = rasImagePack.getInFile();
                }
            } catch (IOException e) {
                throw new IllegalStateException("Unable to read " + file);
            }
            this.rasImageReadRef = new SoftReference<RasImageRead>(rir);
        }
        return rir;
    }

    public File getFile() {
        return file;
    }

    /**
     * For example when file content changed, needs to be reloaded
     */
    public synchronized void invalidate() {
        this.rasImageReadRef = null;
    }

    @Override
    public String toString() {
        return "CachedImage [file=" + file + "]";
    }
    
    public static CachedImage saveAndCache(File file, RasImageRead rasImageRead) {
        try {
            ImFilesUtil.saveBestQualityImage(file, rasImageRead);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to write " + file, e);
        }
        CachedImage cachedImage = new CachedImage(file);
        cachedImage.rasImageReadRef = new SoftReference<RasImageRead>(rasImageRead);
        return cachedImage;
    }
}
