package cz.jmare.image.util;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;

import cz.jmare.image.ras.*;

public class ImConversionsUtil {

    public static BufferedImage copyToBufImage(RasImageRead rasImageRead) {
        BufferedImage bufferedImage = new BufferedImage(rasImageRead.getWidth(), rasImageRead.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        RGBPix rgbPix = new RGBPix();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                int index = (j * width + i) * 3;
                RGBPix rgb = rasImageRead.getRGB(i, j, rgbPix);
                dataBuffer.setElem(index, rgb.blue);
                dataBuffer.setElem(index + 1, rgb.green);
                dataBuffer.setElem(index + 2, rgb.red);
            }
        }
        return bufferedImage;
    }

    public static BufferedImage copyToBufImageAlpha(RasImageRead rasImageRead) {
        BufferedImage bufferedImage = new BufferedImage(rasImageRead.getWidth(), rasImageRead.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        RGBPix rgbPix = new RGBPix();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                int index = (j * width + i) * 4;
                RGBPix rgb = rasImageRead.getRGB(i, j, rgbPix);
                dataBuffer.setElem(index, rgb.alpha);
                dataBuffer.setElem(index + 1, rgb.blue);
                dataBuffer.setElem(index + 2, rgb.green);
                dataBuffer.setElem(index + 3, rgb.red);
            }
        }
        return bufferedImage;
    }

    
    public static RasImage copy(RasImageRead rasImageRead) {
        RasImageFloat rasImageFloat = new RasImageFloat(rasImageRead.getWidth(), rasImageRead.getHeight());
        copy(rasImageRead, rasImageFloat);
        return rasImageFloat;
    }
    
    public static void copy(RasImageRead rasImageRead, RasImageWrite outputRasImage) {
        int width = rasImageRead.getWidth();
        int height = rasImageRead.getHeight();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                outputRasImage.setRGBFloat(i, j, rasImageRead.getRGBFloat(i, j));
            }
        }
    }
    
    
    public static BufferedImage copy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
}
