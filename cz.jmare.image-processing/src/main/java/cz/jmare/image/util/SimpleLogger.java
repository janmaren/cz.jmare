package cz.jmare.image.util;

public class SimpleLogger implements Logger {
    @Override
    public void log(String message) {
        String name = Thread.currentThread().getName();
        if (name.startsWith("main") || name.startsWith("Thread")) {
            System.out.println(message);
        } else {
            System.out.println(name + " - " + message);
        }
    }
}
