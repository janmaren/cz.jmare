package cz.jmare.image.util;

import java.io.File;

public class PathUtil {
    public static int lastSepIndex(String path) {
        int lastIndexSlash = path.lastIndexOf("/");
        int lastIndexBackSlash = path.lastIndexOf("\\");
        int slashIndex = lastIndexSlash > lastIndexBackSlash ? lastIndexSlash: lastIndexBackSlash;
        return slashIndex;
    }

    /**
     * Get dir(0) and file name(1) or empty strings
     * @param path [0] - path ending with slash or backslash, [1] - name of last part (without slash or backslash)
     * @return parsed path with indexes [0] and [1] where [0]+[1] makes full path
     */
    public static String[] toFullPathAndLastPart(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return new String[] {"", path};
        }
        return new String[] {path.substring(0, slashIndex + 1), path.substring(slashIndex + 1)};
    }

    /**
     * Get path until beginning of suffix(0) and suffix (1) or empty strings
     * @param path [0] - full path without '.', [1] - suffix starting with '.'
     * @return parsed path with indexes [0] and [1] where [0]+[1] makes full path
     */
    public static String[] toFullPathAndSuffix(String path) {
        int lastIndexOf = path.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return new String[] {path, ""};
        }
        return new String[] {path.substring(0, lastIndexOf), path.substring(lastIndexOf)};
    }

    /**
     * Build file in a subdirectory under directory with given name
     * @param directory
     * @param subdirectory
     * @param name
     * @return
     */
    public static File deriveFile(File directory, String subdirectory, String name) {
        if (!directory.isDirectory()) {
            throw new IllegalStateException(directory + " is not a directory");
        }
        File darkresult = new File(directory, subdirectory);
        darkresult.mkdirs();
        return new File(darkresult, name);
    }

    /**
     *
     * @param directory
     * @param subdirectory
     * @param name
     * @param suffix suffix starting with .
     * @return
     */
    public static File deriveFileSetSuffix(File directory, String subdirectory, String name, String suffix) {
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(directory + " must be a directory");
        }
        File darkresult = new File(directory, subdirectory);
        String[] strings = toFullPathAndSuffix(name);
        String newName = strings[0] + suffix;
        return new File(darkresult, newName);
    }

    /**
     * Create similar file where a distinguisher is inserted between name and suffix
     * @param file
     * @param distinguisher distinguish name like '-smooth'
     * @return
     */
    public static File distinguishFile(File file, String distinguisher) {
        File dir = file.getParentFile();
        String name = file.getName();
        String[] strings = toFullPathAndSuffix(name);
        String newName = strings[0] + distinguisher + strings[1];
        return new File(dir, newName);
    }
}
