package cz.jmare.image.util;

public class ImTwoPoints {
    public double x1;

    public double y1;

    public double x2;

    public double y2;

    public ImTwoPoints() {

    }

    public ImTwoPoints(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public ImTwoPoints(ImPoint point1, ImPoint point2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }

    @Override
    public String toString() {
        return "ImTwoPoints [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + "]";
    }

    public ImPoint getPoint1() {
        return new ImPoint(x1, y1);
    }

    public ImPoint getPoint2() {
        return new ImPoint(x2, y2);
    }
}
