package cz.jmare.image.util;

public class ArrayUtil {
    public static boolean[][] create2dArray(int width, int height) {
        boolean[][] array = new boolean[height][];
        for (int i = 0; i < height; i++) {
            array[i] = new boolean[width];
        }
        return array;
    }

    public static float[][] create2dFloatArray(int width, int height) {
        float[][] array = new float[height][];
        for (int i = 0; i < height; i++) {
            array[i] = new float[width];
        }
        return array;
    }
}
