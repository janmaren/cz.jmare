package cz.jmare.image.util;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;

public class PFMUtil {
    public static void saveToPFM(File outputFile, RasImageRead rasImageRead) throws IOException {
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        
        try (DataOutputStream dataOutputStream = new DataOutputStream(
                new BufferedOutputStream(new FileOutputStream(outputFile)))) {
            dataOutputStream.write("PF\n".getBytes());
            dataOutputStream.write((width + " " + height + "\n").getBytes());
            dataOutputStream.write("1\n".getBytes());
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    RGBFloat rgb = rasImageRead.getRGBFloat(i, height - j - 1);
                    dataOutputStream.writeFloat(rgb.red);
                    dataOutputStream.writeFloat(rgb.green);
                    dataOutputStream.writeFloat(rgb.blue);
                }
            }
        }
    }
    
    public static BufferedImage pfmToBufferedImage(File inputFile) throws IOException {
        RasImage rasImage = loadPFM(inputFile);
        return ImConversionsUtil.copyToBufImage(rasImage);
    }
    
    public static RasImage loadPFM(File inputFile) throws IOException {
        String format = ImPathUtil.parseNameSuffix(inputFile.toString())[1];
        if (format.length() < 2) {
            throw new IllegalArgumentException("File has no suffix");
        }
        format = format.substring(1);

        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(inputFile)))) {
            int[] widthHeight = getPFWidthHeight(dis, inputFile);
            int width = widthHeight[0];
            int height = widthHeight[1];
            
            RasImageFloat rawImage = new RasImageFloat(width, height);
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    float red = dis.readFloat();
                    float green = dis.readFloat();
                    float blue = dis.readFloat();
                    rawImage.setRGBFloat(i, j, red, green, blue);
                }
            }

            return rawImage;
        }
    }
    
    private static int[] getPFWidthHeight(InputStream dis, File inputFile) throws IOException {
        String type = readLine(dis);
        if (!"PF".equals(type)) {
            throw new IllegalStateException("Unsuported format of file " + inputFile);
        }
        String widthHeight = readLine(dis);
        String[] split = widthHeight.split(" ");
        if (split.length != 2) {
            throw new IllegalStateException("Unsuported format of file " + inputFile);
        }
        int width = Integer.parseInt(split[0]);
        int height = Integer.parseInt(split[1]);
        String scale = readLine(dis);
        float scaleFloat = Float.parseFloat(scale);
        if (scaleFloat != 1) {
            throw new IllegalStateException("Unsuported format of file " + inputFile);
        }
        return new int[] {width, height};
    }
    
    private static String readLine(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        char c;
        do {
            c = (char) is.read();
            if (c == '\n') {
                break;
            }
            sb.append(c);
        } while (c != -1);
        return sb.toString();
    }
    
    public static void main(String[] args) throws IOException {
        RasBufImage rasImage = RasBufImage.getInstance(new File("C:\\Users\\jmarencik\\Pictures\\leotriplet.png"));
        saveToPFM(new File("c:\\temp\\out.pfm"), rasImage);
        
        BufferedImage bufferedImage = pfmToBufferedImage(new File("c:\\temp\\out.pfm"));
        ImageIO.write(bufferedImage, "png", new File("c:\\temp\\out.png"));
    }
}
