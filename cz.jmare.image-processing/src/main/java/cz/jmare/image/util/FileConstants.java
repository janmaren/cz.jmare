package cz.jmare.image.util;

import java.io.File;

public class FileConstants {
    public static String DARK_SUBDIR = "result-dark";
    public static String DARK_FILE_NAME = "dark-master.tif";

    public static String DARK_NO_HOTPIXELS_FILE_NAME = "dark-master-no-hotpixels.tif";

    public static String FLAT_SUBDIR = "result-flat";

    public static String FLAT_FILE_NAME = "flat-master.tif";

    public static String SUBTRACTED_DARKS_SUBDIR = "subtracted-darks";

    public static String OUTPUT_SUBDIR = "result";

    public static String ALIGNS_SUBDIR = "aligns";

    public static String STACK_FILE_NAME = "output.tif";

    public static String ANIM_FILE_NAME = "output.gif";

    public static String POSTPROCESS_FILE_NAME = "output-postprocessed.tif";

    public static String FFC_FILE_NAME = "output-calibrated.tif";

    public static File getAlignOutputDir(File inputDir) {
        return new File(inputDir, ALIGNS_SUBDIR);
    }

    public static File getStackOutputFile(File inputDir) {
        return new File(inputDir, OUTPUT_SUBDIR + File.separator + STACK_FILE_NAME);
    }

    public static File getAnimateOutputFile(File inputDir) {
        return new File(inputDir, OUTPUT_SUBDIR + File.separator + ANIM_FILE_NAME);
    }

    public static File getPostprocessOutputFile(File inputDir) {
        return new File(inputDir, OUTPUT_SUBDIR + File.separator + POSTPROCESS_FILE_NAME);
    }

    public static File getFfcOutputFile(File inputDir) {
        return new File(inputDir, OUTPUT_SUBDIR + File.separator + FFC_FILE_NAME);
    }
}
