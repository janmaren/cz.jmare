package cz.jmare.image.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import cz.jmare.image.align.audit.PointMatchAudit;
import cz.jmare.image.ras.Grey2BytesRasImage;
import cz.jmare.image.ras.RGB3FloatsRasImage;
import cz.jmare.image.ras.RGB3ShortsRasImage;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;

public class ImFilesUtil {
    public static List<CachedImage> filesListCachedSorted(File dir) {
        Set<String> readFormats = RasImageFactory.getAllReadFormats();
        File[] listFiles = dir.listFiles();
        List<CachedImage> files = new ArrayList<CachedImage>();
        if (listFiles == null) {
            return files;
        }
        for (File file : listFiles) {
            if (file.isDirectory()) {
                continue;
            }
            String name = file.getName();
            String[] nameSuf = ImPathUtil.parseNameSuffix(name);
            if (nameSuf[1].length() <= 1 || !readFormats.contains(nameSuf[1].substring(1))) {
                continue;
            }
            files.add(new CachedImage(file.toPath().normalize().toFile()));
        }
        Collections.sort(files, (o1, o2) -> o1.getFile().getName().compareTo(o2.getFile().getName()));
        return files;
    }

    public static List<CachedImage> filesListCached(File dir) {
        return filesListCached(dir, null);
    }

    /**
     * Get cached images from dir
     * @param dir
     * @param ignoreFile file to ignore (nullable)
     * @return cached images
     */
    public static List<CachedImage> filesListCached(File dir, File ignoreFile) {
        try {
            Path realPath = ignoreFile != null ? ignoreFile.toPath().toRealPath() : null;
            Set<String> readFormats = RasImageFactory.getAllReadFormats();
            List<Path> result;
            try (Stream<Path> walk = Files.walk(dir.toPath(), 1)) {
                result = walk.filter(p -> {
                    if (!Files.isRegularFile(p)) {
                        return false;
                    }
                    String name = p.getFileName().toString();
                    String[] nameSuf = ImPathUtil.parseNameSuffix(name);
                    if (nameSuf[1].length() <= 1 || !readFormats.contains(nameSuf[1].substring(1))) {
                        return false;
                    }
                    try {
                        if (realPath != null && p.toRealPath().equals(realPath)) {
                            return false;
                        }
                    } catch (IOException e) {
                        throw new IllegalStateException("Problems to list " + dir, e);
                    }
                    return true;
                }).collect(Collectors.toList());

                return result.stream().map(p -> new CachedImage(p.toFile())).collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new IllegalStateException("Problems to list " + dir);
        }
    }

    public static List<CachedImage> getFilesListCachedByPaths(List<String> filesOrDirs) {
        return getFilesListCachedByFiles(filesOrDirs.stream().map(s -> new File(s)).collect(Collectors.toList()));
    }

    public static List<CachedImage> getFilesListCachedByFiles(List<File> filesOrDirs) {
        Set<String> readFormats = RasImageFactory.getAllReadFormats();
        Set<File> files = new LinkedHashSet<>();
        for (File fileOrDir : filesOrDirs) {
            if (fileOrDir.isFile()) {
                files.add(fileOrDir);
            } else {
                try {
                    files.addAll(Files.list(Paths.get(fileOrDir.getPath())).map(p -> p.toFile()).collect(Collectors.toList()));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return files.stream().filter(f -> {
            String[] nameSuf = ImPathUtil.parseNameSuffix(f.getName());
            return !(nameSuf[1].length() <= 1 || !readFormats.contains(nameSuf[1].substring(1)));
            }).map(f -> new CachedImage(f)).collect(Collectors.toList());
    }

    public static void saveBufImage(File file, BufferedImage bufferedImage) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    public static String getFormat(String outputFile) {
        String format = ImPathUtil.parseNameSuffix(outputFile.toString())[1];
        if (format.length() > 1) {
            format = format.substring(1);
        } else {
            throw new IllegalStateException("Undefined format of output file " + outputFile);
        }
        return format;
    }
    
    public static String getFormat(File outputFile) {
        return getFormat(outputFile.getPath());
    }
    
    /**
     * Saving as 32bit(float) per channel or 16bit per channel when possible. Otherwise as 8bit.
     * Only tiff can be saved as 32bit(float), png can be max 16bit, other formats only 8bit.
     * @param file
     * @param rasImageRead
     * @throws IOException
     */
    public static void saveBestQualityImage(File file, RasImageRead rasImageRead) throws IOException {
        String format = getFormat(file.getPath()).toLowerCase();
        if (format.equals("tif") || format.equals("tiff") || format.equals("png")) {
            if ((rasImageRead instanceof RasImageFloat || rasImageRead instanceof RGB3FloatsRasImage) && (format.equals("tif") || format.equals("tiff"))) {
                saveFloatsImage(file, rasImageRead);
                return;
            } else
            if (rasImageRead instanceof RGB3ShortsRasImage) {
                save16BitImage(file, rasImageRead);
                return;
            }
        }
        save8BitImage(file, rasImageRead);
    }
    
    public static void save16BitImage(File file, RasImageRead rasImageRead) throws IOException {
        RasBufImage rasBufImage;
        if (rasImageRead instanceof RGB3ShortsRasImage) {
            rasBufImage = (RasBufImage) rasImageRead;
        } else {
            rasBufImage = RasBufImage.createShortsInstance(rasImageRead.getWidth(), rasImageRead.getHeight());
            ImConversionsUtil.copy(rasImageRead, rasBufImage);
        }
        BufferedImage bufferedImage = rasBufImage.getBufferedImage();
        try {
            saveBufImage(file, bufferedImage);
        } catch (IOException e) {
            throw new IOException("Unable to save 48bit (3 x 16 RGB) data to " + file, e);
        }
    }

    public static void save16BitGreyImage(File file, RasImageRead rasImageRead) throws IOException {
        RasBufImage rasBufImage;
        if (rasImageRead instanceof Grey2BytesRasImage) {
            rasBufImage = (RasBufImage) rasImageRead;
        } else {
            rasBufImage = RasBufImage.createGreyShortsInstance(rasImageRead.getWidth(), rasImageRead.getHeight());
            ImConversionsUtil.copy(rasImageRead, rasBufImage);
        }
        BufferedImage bufferedImage = rasBufImage.getBufferedImage();
        try {
            saveBufImage(file, bufferedImage);
        } catch (IOException e) {
            throw new IOException("Unable to save 16bit grey data to " + file, e);
        }
    }

    public static void saveFloatsImage(File file, RasImageRead rasImageRead) throws IOException {
        RasBufImage rasBufImage;
        if (rasImageRead instanceof RGB3FloatsRasImage) {
            rasBufImage = (RasBufImage) rasImageRead;
        } else {
            rasBufImage = RasBufImage.createFloatsInstance(rasImageRead.getWidth(), rasImageRead.getHeight());
            ImConversionsUtil.copy(rasImageRead, rasBufImage);
        }
        BufferedImage bufferedImage = rasBufImage.getBufferedImage();
        try {
            saveBufImage(file, bufferedImage);
        } catch (IOException e) {
            throw new IOException("Unable to save floats (3 x 32 RGB) data to " + file, e);
        }
    }
    
    public static void save8BitImageWithAlpha(File file, RasImageRead rasImageRead) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        BufferedImage bufferedImage = ImConversionsUtil.copyToBufImageAlpha(rasImageRead);
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    public static void save8BitImage(File file, RasImageRead rasImageRead) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        BufferedImage bufferedImage = ImConversionsUtil.copyToBufImage(rasImageRead);
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    public static void save8BitImageForDebug(File file, RasImageRead rasImageRead, List<ImPointInt> markPoints) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        BufferedImage bufferedImage = ImConversionsUtil.copyToBufImage(rasImageRead);
        Graphics2D g = bufferedImage.createGraphics();
        int counter = 1;
        Color c = new Color(0, 255, 150, 255);
        g.setColor(c);
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
        for (ImPointInt markPoint : markPoints) {
            g.drawLine(markPoint.x - 2, markPoint.y, markPoint.x + 2, markPoint.y);
            g.drawLine(markPoint.x, markPoint.y - 2, markPoint.x, markPoint.y + 2);
            g.drawString(String.valueOf(counter++), markPoint.x + 5, markPoint.y + 2);
        }
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    public static void save8BitImageForDebugMatches(File file, RasImageRead rasImageRead, List<PointMatchAudit> attempt) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        BufferedImage bufferedImage = ImConversionsUtil.copyToBufImage(rasImageRead);
        Graphics2D g = bufferedImage.createGraphics();
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
        int counter = 0;
        BasicStroke basicStroke = new BasicStroke(5);
        Stroke stroke = g.getStroke();
        for (PointMatchAudit pointMatchAudit : attempt) {
            if (pointMatchAudit.resultDistance != null && pointMatchAudit.resultDistance < 10) {
                if (pointMatchAudit.resultDistance > 5) {
                    g.setColor(Color.ORANGE);
                } else if (pointMatchAudit.resultDistance > 2) {
                    g.setColor(Color.CYAN);
                } else {
                    g.setColor(Color.GREEN);
                }
            } else {
                g.setColor(Color.RED);
            }
            if (pointMatchAudit.resultDistance != null && pointMatchAudit.resultDistance < 10) {
                if (pointMatchAudit.resultDistance < 1e-8) {
                    int radius = 30;
                    int targetX = (int) (pointMatchAudit.expectedX - (radius / 2));
                    int targetY = (int) (pointMatchAudit.expectedY - (radius / 2));
                    g.setStroke(basicStroke);
                    g.drawRect(targetX, targetY, radius, radius);
                    g.setStroke(stroke);
                } else {
                    int radius2 = 30;
                    int targetX2 = (int) (pointMatchAudit.expectedX - (radius2 / 2));
                    int targetY2 = (int) (pointMatchAudit.expectedY - (radius2 / 2));
                    g.drawRect(targetX2, targetY2, radius2, radius2);

                    int radius = (int) (double) pointMatchAudit.resultDistance;
                    int targetX = (int) (pointMatchAudit.expectedX - (radius / 2));
                    int targetY = (int) (pointMatchAudit.expectedY - (radius / 2));
                    g.drawOval(targetX, targetY, radius, radius);
                }
            } else {
                int radius = 30;
                int targetX = (int) (pointMatchAudit.expectedX - (radius / 2));
                int targetY = (int) (pointMatchAudit.expectedY - (radius / 2));
                g.drawRect(targetX, targetY, radius, radius);
            }
            g.drawLine((int) (pointMatchAudit.expectedX - 2), (int) pointMatchAudit.expectedY, (int) (pointMatchAudit.expectedX + 2), (int) pointMatchAudit.expectedY);
            g.drawLine((int) pointMatchAudit.expectedX, (int) (pointMatchAudit.expectedY - 2), (int) pointMatchAudit.expectedX, (int) (pointMatchAudit.expectedY + 2));
            g.drawString(String.valueOf(counter++), (int) pointMatchAudit.expectedX, (int) pointMatchAudit.expectedY + 2);
        }
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    public static void save8BitImageForDebug(File file, RasImageRead rasImageRead, Map<String, ImPointInt> markPoints, Color color) throws IOException {
        String outputFile = file.getPath();
        String format = getFormat(outputFile);
        BufferedImage bufferedImage = ImConversionsUtil.copyToBufImage(rasImageRead);
        Graphics2D g = bufferedImage.createGraphics();
        g.setColor(color);
        for (Map.Entry<String, ImPointInt> entry : markPoints.entrySet()) {
            ImPointInt markPoint = entry.getValue();
            g.drawLine(markPoint.x - 1, markPoint.y, markPoint.x + 1, markPoint.y);
            g.drawLine(markPoint.x, markPoint.y - 1, markPoint.x, markPoint.y + 1);
            g.drawString(entry.getKey(), markPoint.x + 2, markPoint.y + 2);
        }
        if (!ImageIO.write(bufferedImage, format, file)) {
            throw new IOException("Unknown format " + format + ", unable to save image " + outputFile);
        }
    }

    /**
     * Replace suffix in file
     * @param file
     * @param suffix starting with .
     * @return
     */
    public static File replaceFileSuffix(File file, String suffix) {
        String[] parseFullPathSuffix = ImPathUtil.parseNameSuffix(file.toString());
        String format = parseFullPathSuffix[1];
        if (format.length() > 1) {
            format = format.substring(1);
        } else {
            throw new IllegalStateException("Undefined suffix of output file " + file);
        }
        return new File(parseFullPathSuffix[0] + suffix);
    }
    
    public static File replaceTo16bitFormatWhenNeeded(File file) {
        String format = getFormat(file).toLowerCase();
        if (!format.equals("tif") && !format.equals("tiff") && !format.equals("png")) {
            return replaceFileSuffix(file, ".tif");
        }
        return file;
    }

    /**
     * Delete all files in a directory (no subdirectory)
     * @param dir
     */
    public static void deleteDirContent(File dir) {
        if (!dir.isDirectory()) {
            return;
        }
        Arrays.stream(dir.listFiles())
                .filter(Predicate.not(File::isDirectory))
                .forEach(File::delete);
    }

    public static void main(String[] args) throws IOException {
        RasImage rasImage = RasImageFactory.get(new File("C:\\Users\\jmarencik\\git\\cz.jmare\\cz.jmare.image-processing\\src\\main\\resources\\abgr4-test.png")).getRasImage();
        save8BitImageWithAlpha(new File("C:\\temp\\out.png"), rasImage);
    }

}
