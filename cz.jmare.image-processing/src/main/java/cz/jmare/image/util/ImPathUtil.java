package cz.jmare.image.util;

public class ImPathUtil {

    /**
     * Get path until beginning of suffix(0) and suffix (1) or empty strings
     * @param path
     * @return parsed path with indexes [0] and [1] where [0]+[1] makes full path
     */
    public static String[] parseNameSuffix(String path) {
        int lastIndexOf = path.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return new String[] {path, ""};
        }
        return new String[] {path.substring(0, lastIndexOf), path.substring(lastIndexOf)};
    }

    public static int lastSepIndex(String path) {
        int lastIndexSlash = path.lastIndexOf("/");
        int lastIndexBackSlash = path.lastIndexOf("\\");
        int slashIndex = lastIndexSlash > lastIndexBackSlash ? lastIndexSlash: lastIndexBackSlash;
        return slashIndex;
    }

    public static String getLastPartFromPath(String path) {
        int slashIndex = lastSepIndex(path);
        if (slashIndex == -1) {
            return path;
        }
        String fileNameWithWildCard = path.substring(slashIndex + 1);
        return fileNameWithWildCard;
    }

}
