package cz.jmare.image.centroid;

import java.io.File;
import java.util.List;

import cz.jmare.image.util.CachedImage;

public class CentroidsStats {
    int count;
    float avgClusterScore;
    float avgPixelsNumber;
    float avgRoundness;
    float medClusterScore;
    float medPixelsNumber;
    
    public CentroidsStats(CentroidsAnalyzer centroidsAnalyzer) {
        List<Centroid> centroids = centroidsAnalyzer.centroidsSortedScoreDesc();
        this.count = centroids.size();
        
        float sumClusterScores = 0;
        float sumPixelsNumber = 0;
        float sumRoundness = 0;
        int roudnessCount = 0;
        for (Centroid centroid : centroids) {
            sumClusterScores += centroid.score;
            sumPixelsNumber += centroid.number;
            if (centroid.number >= 25) {
                sumRoundness += (double) centroid.minRadius / centroid.maxRadius;
                roudnessCount++;
            }
        }
        avgClusterScore = sumClusterScores / centroids.size();
        avgPixelsNumber = sumPixelsNumber / centroids.size();
        if (roudnessCount != 0) {
            avgRoundness = sumRoundness / roudnessCount;
        }
        
        double halfClusters = sumClusterScores / 2.0;
        double sum = 0;
        int i = 0;
        for (Centroid centroid : centroids) {
            i++;
            sum += centroid.score;
            if (sum >= halfClusters) {
                break;
            }
        }
        medClusterScore = (float) i / centroids.size();
        
        double halfNumbers = sumPixelsNumber / 2.0;
        i = 0;
        for (Centroid centroid : centroids) {
            i++;
            sum += centroid.number;
            if (sum >= halfNumbers) {
                break;
            }
        }
        medPixelsNumber = (float) i / centroids.size();
    }

    public double getAvgClusterScore() {
        return avgClusterScore;
    }

    public double getAvgPixelsNumber() {
        return avgPixelsNumber;
    }

    public int getCount() {
        return count;
    }

    public Float getAvgRoundness() {
        return avgRoundness;
    }

    public float getMedClusterScore() {
        return medClusterScore;
    }

    public float getMedPixelsNumber() {
        return medPixelsNumber;
    }

    public static void main(String[] args) {
        CentroidsStats centroidsStats = new CentroidsStats(
                new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m51-iso3200-20s\\DSC_0378-m51.NEF.jpg")), 350));
        System.out.println(centroidsStats);
        centroidsStats = new CentroidsStats(
                new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m51-iso3200-20s\\DSC_0378-m51.NEF.jpg")), 450));
        System.out.println(centroidsStats);

        centroidsStats = new CentroidsStats(
                new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m51-iso3200-20s\\DSC_0378-m51.NEF.jpg")), 500));
        System.out.println(centroidsStats);

        centroidsStats = new CentroidsStats(
                new CentroidsAnalyzer(new CachedImage(new File("C:\\temp\\m51-iso3200-20s\\DSC_0378-m51.NEF.jpg")), 550));
        System.out.println(centroidsStats);
    }

    @Override
    public String toString() {
        return "CentroidsStats [count=" + count + ", avgClusterScore=" + avgClusterScore + ", avgPixelsNumber="
                + avgPixelsNumber + ", avgRoundness=" + avgRoundness + ", medClusterScore=" + medClusterScore
                + ", medPixelsNumber=" + medPixelsNumber + "]";
    }
}
