package cz.jmare.image.centroid;

import cz.jmare.image.ras.RGBFloat;

public interface PixelScore {
    /**
     * Return brightness in range 0..1
     * @param rgb
     * @return
     */
    float brightness(RGBFloat rgb);
}
