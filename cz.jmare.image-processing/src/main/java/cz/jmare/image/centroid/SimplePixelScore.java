package cz.jmare.image.centroid;

import cz.jmare.image.ras.RGBFloat;

/**
 * Using brightness as prominence scaled 0..1 
 */
public class SimplePixelScore implements PixelScore {

    @Override
    public float brightness(RGBFloat rgb) {
        return (rgb.red + rgb.green + rgb.blue) / 3f;
    }
}
