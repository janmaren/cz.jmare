package cz.jmare.image.centroid;

import java.util.Objects;

import cz.jmare.image.util.ImPoint;

public class Centroid {
    public double x;
    
    public double y;
    
    /**
     * Combination of number of pixels and their brightnesses
     */
    public float score;
    
    /**
     * How much neighbour pixels together with this is present
     */
    public int number;

    /**
     * Radius in the smallest part
     */
    public float minRadius;
    
    /**
     * Radius in the biggest part
     */
    public float maxRadius;
    
    public Centroid() {
    }

    public Centroid(double x, double y, float score, int number) {
        super();
        this.x = x;
        this.y = y;
        this.score = score;
        this.number = number;
    }

    @Override
    public String toString() {
        return "Centroid [x=" + x + ", y=" + y + ", score=" + score + ", number=" + number + "]";
    }
    
    public ImPoint getCenterPoint() {
        return new ImPoint(x, y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    /**
     * Using only x and y supposing all other fields are same
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Centroid other = (Centroid) obj;
        return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
                && Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
    }
}
