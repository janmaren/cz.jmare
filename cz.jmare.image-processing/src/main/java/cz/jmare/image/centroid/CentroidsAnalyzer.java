package cz.jmare.image.centroid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class CentroidsAnalyzer {
    private CachedImage image;
    private Float minClusterScore;
    private float minPixelBrightness;
    private int minPixelsNumber = 2;
    private PixelScore pixelScore = new SimplePixelScore();
    private transient Set<Centroid> centroids;
    private int width;
    private int height;
    
    /**
     * Constructor for calculation ignoring some pixels considered to be black or ignorable cluster with small score
     * @param img
     * @param minPixelBrightness how much brightness at least the pixel must have to be considered as a pixel making cluster. Range 0..1
     */
    public CentroidsAnalyzer(CachedImage img, float minPixelBrightness) {
        super();
        this.image = img;
        this.minPixelBrightness = minPixelBrightness;
    }
    
    /**
     * Constructor for calculation ignoring some pixels considered to be black or ignorable cluster with small score
     * @param img
     * @param minPixelBrightness how much brightness at least the pixel must have to be considered as a pixel making cluster. Range 0..1
     * @param minPixelsNumber How how pixels with minPixelBrightness must an object have not to be ignored
     */
    public CentroidsAnalyzer(CachedImage img, float minPixelBrightness, int minPixelsNumber) {
        super();
        this.image = img;
        this.minPixelBrightness = minPixelBrightness;
        this.minPixelsNumber = minPixelsNumber;
    }
    
    public Set<Centroid> centroids() {
        if (centroids != null) {
            return centroids;
        }
        RasImageRead img = image.getRasImageRead();
        height = img.getHeight();
        width = img.getWidth();
        RawCluster[][] clusterId = new RawCluster[height][];
        for (int i = 0; i < height; i++) {
            clusterId[i] = new RawCluster[width];
        }

        Set<RawCluster> usedIds = new HashSet<>();
        Set<Centroid> result = new HashSet<>();
        Thread currentThread = Thread.currentThread();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                RGBFloat rgb = img.getRGBFloat(i, j);
                float brightness = pixelScore.brightness(rgb);
                if (brightness < minPixelBrightness) {
                    continue;
                }
                
                RawCluster rawCluster = null;
                
                if (j - 1 >= 0) {
                    RawCluster neighCluster = clusterId[j - 1][i];
                    if (neighCluster != null) {
                        rawCluster = neighCluster;
                        //rawCluster.minX = Math.min(i, rawCluster.minX);
                        //rawCluster.minY = Math.min(j, rawCluster.minY);
                        rawCluster.maxX = Math.max(i, rawCluster.maxX);
                        rawCluster.maxY = Math.max(j, rawCluster.maxY);
                    }
                }
                
                if (i - 1 >= 0) {
                    RawCluster neighId = clusterId[j][i - 1];
                    if (neighId != null) {
                        if (rawCluster == null) {
                            rawCluster = neighId;
                            rawCluster.maxX = Math.max(rawCluster.maxX, i);
                        } else {
                            if (neighId != rawCluster) {
                                rawCluster.minX = Math.min(neighId.minX, rawCluster.minX);
                                rawCluster.minY = Math.min(neighId.minY, rawCluster.minY);
                                rawCluster.maxX = Math.max(neighId.maxX, rawCluster.maxX);
                                rawCluster.maxY = Math.max(neighId.maxY, rawCluster.maxY);
                                //rawCluster.minX = Math.min(i, rawCluster.minX);
                                //rawCluster.minY = Math.min(j, rawCluster.minY);
                                rawCluster.maxX = Math.max(i, rawCluster.maxX);
                                rawCluster.maxY = Math.max(j, rawCluster.maxY);
                                renumber(neighId, rawCluster, clusterId);
                                usedIds.remove(neighId);
                            }
                        }
                    }
                }

                if (rawCluster == null) {
                    rawCluster = new RawCluster();
                    rawCluster.minX = i;
                    rawCluster.maxX = i;
                    rawCluster.minY = j;
                    rawCluster.maxY = j;
                    usedIds.add(rawCluster);
                }
                clusterId[j][i] = rawCluster;
                
                if (currentThread.isInterrupted()) {
                    throw new RuntimeException("Interrupted");
                }
            }
        }
        for (RawCluster rawCluster : usedIds) {
            Cluster cluster = new Cluster();
            for (int j = rawCluster.minY; j <= rawCluster.maxY; j++) {
                RawCluster[] row = clusterId[j];
                for (int i = rawCluster.minX; i <= rawCluster.maxX; i++) {
                    RawCluster id = row[i];
                    if (id == rawCluster) {
                        RGBFloat rgb = img.getRGBFloat(i, j);
                        cluster.addPoint(i, j, (rgb.red + rgb.green + rgb.blue) / 3f);
                    }
                }
            }
            if (cluster.points.size() == 0 || cluster.points.size() < minPixelsNumber) {
                continue;
            }
            Centroid centroid = calcCentroid(cluster);
            if (minClusterScore == null || centroid.score >= minClusterScore) {
                updateRadius(centroid, cluster, rawCluster);
                result.add(centroid);
            }
        }
        return centroids = result;
    }
    
    public List<Centroid> centroidsSortedScoreDesc() {
        List<Centroid> clusters =  new ArrayList<>(centroids());
        Collections.sort(clusters, (o1, o2) -> Float.valueOf(o2.score).compareTo(o1.score));
        return clusters;
    }
    
    private void renumber(RawCluster replaceCluster, RawCluster replacingId, RawCluster[][] clusterId) {
        for (int j = replaceCluster.minY; j <= replaceCluster.maxY; j++) {
            for (int i = replaceCluster.minX; i <= replaceCluster.maxX; i++) {
                if (clusterId[j][i] == replaceCluster) {
                    clusterId[j][i] = replacingId;
                }
            }
        }
    }

    private static Centroid calcCentroid(Cluster cluster) {
        List<PointScore> points = cluster.getPoints();
        float sum = 0;
        float sumX = 0;
        float sumY = 0;
        for (PointScore point : points) {
            sum += point.score;
            sumX += (point.x + 1) * point.score;
            sumY += (point.y + 1) * point.score;
        }
        double x = (double) sumX / sum - 0.5;
        double y = (double) sumY / sum - 0.5;
        return new Centroid(x, y, sum, points.size());
    }
    
    private void updateRadius(Centroid centroid, Cluster cluster, RawCluster rawCluster) {
        List<PointScore> points = cluster.getPoints();
        int cx = (int) Math.round(centroid.x);
        int cy = (int) Math.round(centroid.y);
        float max = Float.MIN_VALUE;
        for (PointScore pointScore : points) {
            float dx = pointScore.x - cx;
            float dy = pointScore.y - cy;
            float pyth = (int) Math.round(Math.sqrt(dx * dx + dy * dy));
            if (pyth > max) {
                max = pyth;
            }
        }
        centroid.maxRadius = max;
        centroid.minRadius = (float) Math.sqrt(centroid.number / Math.PI);
    }
    
    /**
     * Logic, to evaluate R, G and B. Such evaluation must be greater than clusterScoreGreater otherwise such pixel is ignored
     * @param pixelScore
     */
    public void setPixelScore(PixelScore pixelScore) {
        this.pixelScore = pixelScore;
        this.centroids = null;
    }
    
    /**
     * How much pixels (with certaint brightness) make a cluster for which the centroid is calculated
     * @param atLeastPixelsNumber
     */
    public void setMinPixelsNumber(int atLeastPixelsNumber) {
        this.minPixelsNumber = atLeastPixelsNumber;
        this.centroids = null;
    }

    public void setMinClusterScore(Float clusterScoreGreater) {
        this.minClusterScore = clusterScoreGreater;
        this.centroids = null;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float minPixelBrightness() {
        return minPixelBrightness;
    }

    private static class Cluster {
        List<PointScore> points = new ArrayList<>();
        
        public void addPoint(int x, int y, float sum) {
            points.add(new PointScore(x, y, sum));
        }
        
        public List<PointScore> getPoints() {
            return points;
        }
    }
    
    private static class RawCluster {
        int minX;
        int maxX;
        int minY;
        int maxY;
    }
    
    private static class PointScore {
        int x;
        int y;
        float score;
        
        public PointScore(int x, int y, float score) {
            super();
            this.x = x;
            this.y = y;
            this.score = score;
        }
        
        @Override
        public String toString() {
            return "PointScore [x=" + x + ", y=" + y + ", score=" + score + "]";
        }
    }
}
