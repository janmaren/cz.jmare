package cz.jmare.image;

public class CropConfig {
    private CropType cropType;
    
    /**
     * Center X of crop for {@link CropType#MANUAL_CENTER_RADIUS}
     */
    private Integer centerX;
    
    /**
     * Center Y of crop for {@link CropType#MANUAL_CENTER_RADIUS}
     */
    private Integer centerY;
    
    /**
     * Radius X of crop for {@link CropType#MANUAL_CENTER_RADIUS}, {@link CropType#DETECTED_CENTER_RADIUS}
     */
    private Integer radiusX;
    
    /**
     * Radius Y of crop for {@link CropType#MANUAL_CENTER_RADIUS}, {@link CropType#DETECTED_CENTER_RADIUS}
     */
    private Integer radiusY;
    
    public CropConfig(CropType cropType) {
        super();
        this.cropType = cropType;
    }
    
    public CropConfig centerX(int centerX) {
        this.centerX = centerX;
        return this;
    }
    
    public CropConfig centerY(int centerY) {
        this.centerY = centerY;
        return this;
    }
    
    public CropConfig radiusX(int radiusX) {
        this.radiusX = radiusX;
        return this;
    }
    
    public CropConfig radiusY(int radiusY) {
        this.radiusY = radiusY;
        return this;
    }

    public CropType getCropType() {
        return cropType;
    }

    public Integer getCenterX() {
        return centerX;
    }

    public Integer getCenterY() {
        return centerY;
    }

    public Integer getRadiusX() {
        return radiusX;
    }

    public Integer getRadiusY() {
        return radiusY;
    }
}
