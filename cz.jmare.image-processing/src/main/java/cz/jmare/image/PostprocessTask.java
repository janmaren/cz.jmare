package cz.jmare.image;

import java.io.File;
import java.io.IOException;

import cz.jmare.image.postprocess.PixelTransformer;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImage;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class PostprocessTask {
    public static CachedImage transformPixels(File inputFile, File outputFile, PixelTransformer pixelTransformer) throws IOException {
        CachedImage inputImage = new CachedImage(inputFile);
        return transformPixels(inputImage, outputFile, pixelTransformer);
    }
    
    public static CachedImage transformPixels(CachedImage inputImage, File outputFile, PixelTransformer pixelTransformer) throws IOException {
        RasImageRead inputRasImage = inputImage.getRasImageRead();
        RasImage outputImage = new RasImageFloat(inputRasImage.getWidth(), inputRasImage.getHeight());
        int width = inputRasImage.getWidth();
        int height = inputRasImage.getHeight();
        long start = System.currentTimeMillis();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                RGBFloat transform = pixelTransformer.transform(inputRasImage, i, j);
                outputImage.setRGBFloat(i, j, transform);
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("Doba: " + (end - start));
        return CachedImage.saveAndCache(outputFile, outputImage);
    }
}
