package cz.jmare.image.combine;

import cz.jmare.image.exception.ProcessInterruptedException;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.PFMUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class AvgAmplifyStacker implements Stacker {
    private float amplifyFactor = 2.0f;

    /**
     * Stack all files in in input dir and save to output file as 16bit float image (when format does not support then 8bit)
     * @param inputDir
     * @param outputFile
     * @return
     * @throws IOException
     */
    public static CachedImage stack(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        return new AvgAmplifyStacker().stack(filesListCached, outputFile);
    }
    
    /**
     * Stack aligned input files and save as float 16bit image (when format supports).<br>
     * Using average R, G and B value for pixel using all input images
     * @param inputFiles aligned input (cached or not) files
     * @param outputFile 
     * @throws IOException
     */
    @Override
    public CachedImage stack(List<CachedImage> inputFiles, File outputFile) throws IOException {
        if (inputFiles.size() == 0) {
            throw new IllegalArgumentException("No input files to stack");
        }
        
        CachedImage firstImage = inputFiles.get(0);
        RasImageRead firstRasImage = firstImage.getRasImageRead();
        int height = firstRasImage.getHeight();
        int width = firstRasImage.getWidth();
        
        float[][] red = new float[height][];
        for (int j = 0; j < height; j++) {
            red[j] = new float[width];
        }
        
        float[][] green = new float[height][];
        for (int j = 0; j < height; j++) {
            green[j] = new float[width];
        }
        
        float[][] blue = new float[height][];
        for (int j = 0; j < height; j++) {
            blue[j] = new float[width];
        }
        
        boolean exists8bit = false;
        int counter = 0;
        for (CachedImage cachedImage : inputFiles) {
            counter++;
            if (Thread.interrupted()) {
                throw new ProcessInterruptedException();
            }
            System.out.println("> ["+ counter + "/" + inputFiles.size() +"] Stacking " + cachedImage.getFile() + " <");
            RasImageRead rasImage = cachedImage.getRasImageRead();
            exists8bit |= !RasImageFactory.isRas16Bit(rasImage);
            for (int j = 0; j < height; j++) {
                float[] r = red[j];
                float[] g = green[j];
                float[] b = blue[j];
                for (int i = 0; i < width; i++) {
                    RGBFloat rgb = rasImage.getRGBFloat(i, j);
                    r[i] += rgb.red;
                    g[i] += rgb.green;
                    b[i] += rgb.blue;
                }
            }
        }
        
        if (exists8bit) {
            System.out.println("--- Warning: there is at least one input aligned file with only 8bits per channel ---");
        }
        
        RasImageFloat outputImage = new RasImageFloat(width, height);
        float size = inputFiles.size();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                float rr = red[j][i] * amplifyFactor / size;
                float gg = green[j][i] * amplifyFactor / size;
                float bb = blue[j][i] * amplifyFactor / size;
                if (rr > 1f) {
                    rr = 1f;
                }
                if (gg > 1f) {
                    gg = 1f;
                }
                if (bb > 1f) {
                    bb = 1f;
                }
                outputImage.setRGBFloat(i, j, rr, gg, bb);
            }
        }

        outputFile.getParentFile().mkdirs();
        
        
        String name = outputFile.getName().toLowerCase();
        if (!name.endsWith(".tiff") && !name.endsWith(".tif") && !name.endsWith(".png")) {
            File pfmFile = ImFilesUtil.replaceFileSuffix(outputFile, ".pfm");
            System.out.println("- Saving to " + pfmFile + " -");
            PFMUtil.saveToPFM(pfmFile, outputImage);
        }
        
        return CachedImage.saveAndCache(outputFile, outputImage);
    }
    
    @Override
    public String toString() {
        return "average with amplifying";
    }

    public static void main(String[] args) throws IOException {
        stack(new File("C:\\temp\\m42\\aligns\\"), new File("c:\\temp\\stack.png"));
    }

    public void setAmplifyFactor(float amplifyFactor) {
        this.amplifyFactor = amplifyFactor;
    }

    public float getAmplifyFactor() {
        return this.amplifyFactor;
    }
}
