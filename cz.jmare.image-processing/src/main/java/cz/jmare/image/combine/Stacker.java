package cz.jmare.image.combine;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cz.jmare.image.util.CachedImage;

public interface Stacker {
    CachedImage stack(List<CachedImage> inputFiles, File outputFile) throws IOException;
}
