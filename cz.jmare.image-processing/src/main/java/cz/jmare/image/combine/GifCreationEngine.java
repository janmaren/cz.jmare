package cz.jmare.image.combine;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

import cz.jmare.image.gif.GifSequencer;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImConversionsUtil;
import cz.jmare.image.util.ImFilesUtil;

public class GifCreationEngine {
    public static void createGif(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        createGif(filesListCached, outputFile, 50, true, 1, null);
    }
    
    public static void createGif(List<CachedImage> inputFiles, File outputFile, int delay, boolean loop, int lastFrameTimes, Consumer<Graphics2D> graphConsumer) throws IOException{
        if (inputFiles.size() < 1) {
            throw new IllegalArgumentException("At least one file needed to make a gif");
        }
        try (ImageOutputStream output = new FileImageOutputStream(outputFile)) {
            GifSequencer writer = new GifSequencer(output, BufferedImage.TYPE_3BYTE_BGR, delay, loop);
            for (CachedImage image : inputFiles) {
                System.out.println("> adding " + image.getFile() + "<");
                RasImageRead rasImageRead = image.getRasImageRead();
                BufferedImage next = ImConversionsUtil.copyToBufImage(rasImageRead);
                if (graphConsumer != null) {
                    Graphics2D graphics = next.createGraphics();
                    graphConsumer.accept(graphics);
                }
                writer.writeToSequence(next);
            }
            
            // repeat last image
            CachedImage lastCachedImage = inputFiles.get(inputFiles.size() - 1);
            RasImageRead image = lastCachedImage.getRasImageRead();
            for (int i = 1; i < lastFrameTimes; i++) {
                System.out.println("> adding " + lastCachedImage.getFile() + "<");
                BufferedImage next = ImConversionsUtil.copyToBufImage(image);
                writer.writeToSequence(next);
            }
            
            writer.close();
        }
    }
}
