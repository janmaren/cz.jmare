package cz.jmare.image.combine;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import cz.jmare.image.exception.ProcessInterruptedException;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.PFMUtil;

public class AvgStacker implements Stacker {
    private boolean clipMin;
    private boolean clipMax;

    /**
     * Stack aligned input files and save as float 16bit image (when format supports).<br>
     * Using average R, G and B value for pixel using all input images
     * @param inputFiles aligned input (cached or not) files
     * @param outputFile 
     * @throws IOException
     */
    @Override
    public CachedImage stack(List<CachedImage> inputFiles, File outputFile) throws IOException {
        if (inputFiles.size() == 0) {
            throw new IllegalArgumentException("No input files to stack");
        }
        int neededSize = 1;
        if (clipMin) {
            neededSize++;
        }
        if (clipMax) {
            neededSize++;
        }
        if (inputFiles.size() < neededSize) {
            throw new IllegalArgumentException("Needed number of input files is " + neededSize + " because clipping, provided only " + inputFiles.size());
        }
        boolean clip = neededSize > 1;
        
        CachedImage firstImage = inputFiles.get(0);
        RasImageRead firstRasImage = firstImage.getRasImageRead();
        int height = firstRasImage.getHeight();
        int width = firstRasImage.getWidth();
        
        float[][] red = new float[height][];
        for (int j = 0; j < height; j++) {
            red[j] = new float[width];
        }
        
        float[][] green = new float[height][];
        for (int j = 0; j < height; j++) {
            green[j] = new float[width];
        }
        
        float[][] blue = new float[height][];
        for (int j = 0; j < height; j++) {
            blue[j] = new float[width];
        }

        float[][] maxRed = new float[height][];
        for (int j = 0; j < height; j++) {
            maxRed[j] = new float[width];
        }

        float[][] maxGreen = new float[height][];
        for (int j = 0; j < height; j++) {
            maxGreen[j] = new float[width];
        }

        float[][] maxBlue = new float[height][];
        for (int j = 0; j < height; j++) {
            maxBlue[j] = new float[width];
        }

        float[][] minRed = new float[height][];
        for (int j = 0; j < height; j++) {
            minRed[j] = new float[width];
            Arrays.fill(minRed[j], 1);
        }

        float[][] minGreen = new float[height][];
        for (int j = 0; j < height; j++) {
            minGreen[j] = new float[width];
            Arrays.fill(minGreen[j], 1);
        }

        float[][] minBlue = new float[height][];
        for (int j = 0; j < height; j++) {
            minBlue[j] = new float[width];
            Arrays.fill(minBlue[j], 1);
        }

        boolean exists8bit = false;
        int counter = 0;
        for (CachedImage cachedImage : inputFiles) {
            counter++;
            if (Thread.interrupted()) {
                throw new ProcessInterruptedException();
            }
            System.out.println("> ["+ counter + "/" + inputFiles.size() +"] Stacking " + cachedImage.getFile() + " <");
            RasImageRead rasImage = cachedImage.getRasImageRead();
            exists8bit |= !RasImageFactory.isRas16Bit(rasImage);
            for (int j = 0; j < height; j++) {
                float[] r = red[j];
                float[] g = green[j];
                float[] b = blue[j];
                float[] mar = maxRed[j];
                float[] mag = maxGreen[j];
                float[] mab = maxBlue[j];
                float[] mir = minRed[j];
                float[] mig = minGreen[j];
                float[] mib = minBlue[j];
                for (int i = 0; i < width; i++) {
                    RGBFloat rgb = rasImage.getRGBFloat(i, j);
                    r[i] += rgb.red;
                    g[i] += rgb.green;
                    b[i] += rgb.blue;
                    if (clip) {
                        if (rgb.red > mar[i]) {
                            mar[i] = rgb.red;
                        }
                        if (rgb.green > mag[i]) {
                            mag[i] = rgb.green;
                        }
                        if (rgb.blue > mab[i]) {
                            mab[i] = rgb.blue;
                        }
                        if (rgb.red < mir[i]) {
                            mir[i] = rgb.red;
                        }
                        if (rgb.green < mig[i]) {
                            mig[i] = rgb.green;
                        }
                        if (rgb.blue < mib[i]) {
                            mib[i] = rgb.blue;
                        }
                    }
                }
            }
        }
        
        if (exists8bit) {
            System.out.println("--- Warning: there is at least one input aligned file with only 8bits per channel ---");
        }
        
        RasImageFloat outputImage = new RasImageFloat(width, height);
        int size = inputFiles.size();
        int useSize = size;
        if (clip) {
            if (clipMin) {
                useSize -= 1;
            }
            if (clipMax) {
                useSize -= 1;
            }
        }
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                float r = red[j][i];
                float g = green[j][i];
                float b = blue[j][i];
                if (clip) {
                    if (clipMax) {
                        r -= maxRed[j][i];
                        g -= maxGreen[j][i];
                        b -= maxBlue[j][i];
                    }
                    if (clipMin) {
                        r -= minRed[j][i];
                        g -= minGreen[j][i];
                        b -= minBlue[j][i];
                    }
                }
                outputImage.setRGBFloat(i, j, r / useSize, g / useSize, b / useSize);
            }
        }

        outputFile.getParentFile().mkdirs();
        
        
        String name = outputFile.getName().toLowerCase();
        if (!name.endsWith(".tiff") && !name.endsWith(".tif") && !name.endsWith(".png")) {
            File pfmFile = ImFilesUtil.replaceFileSuffix(outputFile, ".pfm");
            System.out.println("- Saving to " + pfmFile + " -");
            PFMUtil.saveToPFM(pfmFile, outputImage);
        }
        
        return CachedImage.saveAndCache(outputFile, outputImage);
    }
    
    @Override
    public String toString() {
        return "average";
    }


    public void setClipMin(boolean clipMin) {
        this.clipMin = clipMin;
    }

    public void setClipMax(boolean clipMax) {
        this.clipMax = clipMax;
    }

    public boolean isClipMin() {
        return this.clipMin;
    }

    public boolean isClipMax() {
        return this.clipMax;
    }

    /**
     * Stack all files in in input dir and save to output file as 16bit float image (when format does not support then 8bit)
     * @param inputDir
     * @param outputFile
     * @return
     * @throws IOException
     */
    public static CachedImage stack(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        return new AvgStacker().stack(filesListCached, outputFile);
    }


    public static void main(String[] args) throws IOException {
        stack(new File("C:\\temp\\m42\\aligns\\"), new File("c:\\temp\\stack.png"));
    }
}
