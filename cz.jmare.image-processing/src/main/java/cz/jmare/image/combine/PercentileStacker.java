package cz.jmare.image.combine;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;
import cz.jmare.image.util.PFMUtil;

public class PercentileStacker implements Stacker {
    private float percentFraction = 0.5f; // median
    
    public PercentileStacker() {
        super();
    }

    public PercentileStacker(float percentFraction) {
        super();
        this.percentFraction = percentFraction;
    }

    /**
     * Stack all files in in input dir and save to output file as 16bit float image (when format does not support then 8bit)
     * @param inputDir
     * @param outputFile
     * @return
     * @throws IOException
     */
    public static CachedImage stack(File inputDir, File outputFile) throws IOException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        return new PercentileStacker().stack(filesListCached, outputFile);
    }
    
    /**
     * Stack aligned input files and save as float 16bit image (when format supports).<br>
     * Using average R, G and B value for pixel using all input images
     * @param inputFiles aligned input (cached or not) files
     * @param outputFile 
     * @param pixelStack
     * @throws IOException
     */
    @Override
    public CachedImage stack(List<CachedImage> inputFiles, File outputFile) throws IOException {
        if (inputFiles.size() == 0) {
            throw new IllegalArgumentException("No input files to stack");
        }
        
        CachedImage firstImage = inputFiles.get(0);
        RasImageRead firstRasImage = firstImage.getRasImageRead();
        int height = firstRasImage.getHeight();
        int width = firstRasImage.getWidth();
        
        float[][] red = new float[height][];
        for (int j = 0; j < height; j++) {
            red[j] = new float[width];
        }
        
        float[][] green = new float[height][];
        for (int j = 0; j < height; j++) {
            green[j] = new float[width];
        }
        
        float[][] blue = new float[height][];
        for (int j = 0; j < height; j++) {
            blue[j] = new float[width];
        }
        
        Runtime runtime = Runtime.getRuntime();
        long freeMemory = runtime.freeMemory();
        int fs = inputFiles.size();
        long pixelOccupy = fs * 4 /* float */ * 3 /* channels */;
        long fitPixels = freeMemory / pixelOccupy;
        int rows = (int) (fitPixels / width);
        if (rows == 0) {
            throw new IllegalStateException("Not enough memory to stack by median");
        }
        
        float[][][] rowsR = new float[rows][][];
        float[][][] rowsG = new float[rows][][];
        float[][][] rowsB = new float[rows][][];
        
        for (int i = 0; i < rows; i++) {
            float[][] rowR = new float[width][];
            float[][] rowG = new float[width][];
            float[][] rowB = new float[width][];
            rowsR[i] = rowR;
            rowsG[i] = rowG;
            rowsB[i] = rowB;
            for (int j = 0; j < width; j++) {
                rowR[j] = new float[fs]; 
                rowG[j] = new float[fs];
                rowB[j] = new float[fs];
            }
        }
        
        int centilIndex = (int) (percentFraction * fs);
        RasImageFloat outputImage = new RasImageFloat(width, height);
        int pos = 0;
        while (pos < height) {
            int count = rows;
            if (rows + pos > height) {
                count -= (rows + pos) - height;
            }
            
            for (int i = 0; i < count; i++) {
                float[][] rowR = rowsR[i];
                float[][] rowG = rowsG[i];
                float[][] rowB = rowsB[i];
                for (int j = 0; j < width; j++) {
                    for (int k = 0; k < fs; k++) {
                        rowR[j][k] = 0; 
                        rowG[j][k] = 0;
                        rowB[j][k] = 0;
                    }
                }
            }
            
            for (CachedImage cachedImage : inputFiles) {
                RasImageRead rasImage = cachedImage.getRasImageRead();
                for (int i = 0; i < count; i++) {
                    float[][] rowR = rowsR[i];
                    float[][] rowG = rowsG[i];
                    float[][] rowB = rowsB[i];
                    for (int j = 0; j < width; j++) {
                        for (int k = 0; k < fs; k++) {
                            RGBFloat rgb = rasImage.getRGBFloat(j, pos + i);
                            rowR[j][k] = rgb.red; 
                            rowG[j][k] = rgb.green;
                            rowB[j][k] = rgb.blue;
                        }
                    }
                }    
            }
            
            for (int i = 0; i < count; i++) {
                float[][] rowR = rowsR[i];
                float[][] rowG = rowsG[i];
                float[][] rowB = rowsB[i];
                for (int j = 0; j < width; j++) {
                    Arrays.sort(rowR[j]);
                    Arrays.sort(rowG[j]);
                    Arrays.sort(rowB[j]);
                    outputImage.setRGBFloat(j, pos + i, rowR[j][centilIndex], rowG[j][centilIndex], rowB[j][centilIndex]);
                }
            }
            System.out.println("Stacked " + Math.round(100 * (pos + count) / height) + "%");
            pos += rows;
        }
        

        outputFile.getParentFile().mkdirs();
        
        
        String name = outputFile.getName().toLowerCase();
        if (!name.endsWith(".tiff") && !name.endsWith(".tif") && !name.endsWith(".png")) {
            File pfmFile = ImFilesUtil.replaceFileSuffix(outputFile, ".pfm");
            System.out.println("- Saving to " + pfmFile + " -");
            PFMUtil.saveToPFM(pfmFile, outputImage);
        }
        
        System.out.println("- Saving to " + outputFile + " -");
        return CachedImage.saveAndCache(outputFile, outputImage);
    }
    
    @Override
    public String toString() {
        return "percentile(" + percentFraction + ")" + (percentFraction == 0.5f ? " - median" : "");
    }

    public static void main(String[] args) throws IOException {
        stack(new File("C:\\temp\\m42\\aligns\\"), new File("c:\\temp\\stack.png"));
    }

    public float getPercentFraction() {
        return percentFraction;
    }
}
