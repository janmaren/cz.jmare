package cz.jmare.image;

import java.awt.Graphics2D;
import java.util.function.Consumer;

public class AnimCreationConfig {
    /**
     * How long a frame should be displayed in milliseconds
     */
    private int delay = 50;

    /**
     * True when after last frame it continues with the first one
     */
    private boolean loop = true;

    /**
     * How much times the last frame should appear. To produce a pause between last and first frame add some number of iterations
     */
    private int lastFrameTimes = 1;
    
    /**
     * Implementation to receive Graphics2D object and using it an additional
     * drawing can be made on each frame
     */
    private Consumer<Graphics2D> graphConsumer;

    public AnimCreationConfig delay(int delay) {
        this.delay = delay;
        return this;
    }

    public AnimCreationConfig loop(boolean loop) {
        this.loop = loop;
        return this;
    }

    public AnimCreationConfig graphConsumer(Consumer<Graphics2D> graphConsumer) {
        this.graphConsumer = graphConsumer;
        return this;
    }

    public AnimCreationConfig lastFrameTimes(int lastFrameTimes) {
        this.lastFrameTimes = lastFrameTimes;
        return this;
    }
    
    public int getDelay() {
        return delay;
    }

    public boolean isLoop() {
        return loop;
    }

    public Consumer<Graphics2D> getGraphConsumer() {
        return graphConsumer;
    }

    @Override
    public String toString() {
        return "delay millis: " + delay + ", loop: " + (loop ? "yes" : "no") + ", last frame times: " + lastFrameTimes;
    }

    public int getLastFrameTimes() {
        return lastFrameTimes;
    }
}
