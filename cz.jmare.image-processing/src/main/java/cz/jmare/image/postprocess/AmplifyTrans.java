package cz.jmare.image.postprocess;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RGBFloatUtil;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.HSLUtil;

public class AmplifyTrans implements PixelTransformer {

    private float blackBrightness;

    private float fromBrightness;

    private float ampFactor;
    private final int dimension;

    public AmplifyTrans(float blackBrightness, float fromBrightness, float ampFactor, int dimension) {
        super();
        this.blackBrightness = blackBrightness;
        this.fromBrightness = fromBrightness;
        this.ampFactor = ampFactor;
        this.dimension = dimension;
    }

    @Override
    public RGBFloat transform(RasImageRead rawImage, int xx, int yy) {
        int width = rawImage.getWidth();
        int height = rawImage.getHeight();
        int cx = dimension / 2;
        int cy = dimension / 2;
        int number = 0;
        float sum = 0;
        for (int k = 0; k < dimension; k++) {
            for (int l = 0; l < dimension; l++) {
                int x = xx - cx + l;
                if (x < 0 || x >= width) {
                    continue;
                }
                int y = yy - cy + k;
                if (y < 0 || y >= height) {
                    continue;
                }
                RGBFloat rgbFloat = rawImage.getRGBFloat(x, y);

                float brightness = RGBFloatUtil.getBrightness(rgbFloat);
                if (brightness > fromBrightness) {
                    brightness = fromBrightness;
                }

                number++;
                sum += brightness;
            }
        }

        float avgBrightness = sum / number;

        //float ratio = avgBrightness / fromBrightness;

        float ratio = avgBrightness < blackBrightness ? 0f : (avgBrightness - blackBrightness) / (fromBrightness - blackBrightness);

        float factor = (1 + (ampFactor - 1) * ratio);

        RGBFloat rgbFloat = rawImage.getRGBFloat(xx, yy);

        float[] floats = HSLUtil.rgbToHsl(rgbFloat.red, rgbFloat.green, rgbFloat.blue);
        float luminance = floats[2] * factor;
        if (luminance > 1) {
            luminance = 1f;
        }

        float[] rgbArray = HSLUtil.hslToRgb(floats[0], floats[1], luminance);

        return new RGBFloat(rgbArray[0], rgbArray[1], rgbArray[2]);
    }

    @Override
    public String toString() {
        return "AmplifyTrans";
    }
}
