package cz.jmare.image.postprocess;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.SplineFloatRawInterpolator;

public class CurveInterpolPixelTrans implements PixelTransformer {

    float[] table = new float[256];

    public CurveInterpolPixelTrans() {
        float[] xs =     {0,   120f,   255f};
        float[] points = {0f, 200f, 255f};
        SplineFloatRawInterpolator splineFloatSampledInterpolator = new SplineFloatRawInterpolator(xs, points);
        for (int i = 0; i <= 255; i++) {
            table[i] = (int) splineFloatSampledInterpolator.interpolate((float) i);
        }
    }

    public static void main(String[] args) {
        float[] xs = {0, 120, 120};
        float[] ys = {0, 200, 255};
        SplineFloatRawInterpolator splineFloatRawInterpolator = new SplineFloatRawInterpolator(xs, ys);
        for (int i = 0; i <= 255; i++) {
            float interpolate = splineFloatRawInterpolator.interpolate(i);
            System.out.println(i + ": " + interpolate);
        }
    }

    @Override
    public RGBFloat transform(RasImageRead rawImage, int x, int y) {
        RGBFloat rgb = rawImage.getRGBFloat(x, y);
        float brigh = (rgb.red + rgb.green + rgb.blue) / 3f * 255f;
        double resIntens = table[(int) brigh];
        double factor = resIntens / brigh;
        int red = Math.min(255, (int) (rgb.red * factor / 255f));
        int green =  Math.min(255, (int)(rgb.green * factor / 255f));
        int blue =  Math.min(255, (int)(rgb.blue * factor / 255f));
        RGBFloat rgbPix = new RGBFloat(red, green, blue);
        return rgbPix;
    }
}
