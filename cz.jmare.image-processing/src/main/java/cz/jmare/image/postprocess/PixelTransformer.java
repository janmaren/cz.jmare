package cz.jmare.image.postprocess;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public interface PixelTransformer {
    RGBFloat transform(RasImageRead rawImage, int x, int y);
}
