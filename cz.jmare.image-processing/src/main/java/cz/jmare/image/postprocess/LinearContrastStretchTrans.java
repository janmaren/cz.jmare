package cz.jmare.image.postprocess;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageRead;

public class LinearContrastStretchTrans implements PixelTransformer {

    private int r1 = 2;
    private float s1 = 5f;
    private int r2 = 100;
    private float s2 = 200;
    private float s1ToR1;
    private float s2s1Tor2r1;
    private float ts2Totr2;
    
    public LinearContrastStretchTrans(int r1, float s1, int r2, float s2) {
        super();
        this.r1 = r1;
        this.s1 = s1;
        this.r2 = r2;
        this.s2 = s2;
        s1ToR1 = (s1 / r1);
        s2s1Tor2r1 = (s2 - s1) / (r2 - r1);
        ts2Totr2 = (float)(255 - s2) / (255 - r2);
    }

    @Override
    public RGBFloat transform(RasImageRead rawImage, int x, int y) {
        RGBFloat rgb = rawImage.getRGBFloat(x, y);
        float brigh = (rgb.red + rgb.green + rgb.blue) / 3f * 255f;
        
        double resIntens = 0;
        if (brigh < r1) {
            resIntens = brigh * s1ToR1; // brigh * (s1 / r1);
        } else if (brigh < r2) {
            resIntens =  ((brigh - r1) * s2s1Tor2r1) + s1;  //((brigh - r1) * (float)(s2 - s1) / (r2 - r1)) + s1;
        } else {
            resIntens = ((brigh - r2) * ts2Totr2) + s2; //((brigh - r2) * (float)(255 - s2) / (255 - r2)) + s2;
        }
        
        double factor = resIntens / brigh;
        float red = (float) Math.min(1, (rgb.red * factor));
        float green =  (float) Math.min(1, (rgb.green * factor));
        float blue =  (float) Math.min(1, (rgb.blue * factor));
        RGBFloat rgbPix = new RGBFloat(red, green, blue);
        return rgbPix;
    }

    public int getR1() {
        return r1;
    }

    public float getS1() {
        return s1;
    }

    public int getR2() {
        return r2;
    }

    public float getS2() {
        return s2;
    }

    @Override
    public String toString() {
        return "linear contrast stretching: 0-" + r1 + "-" + r2 + "-255 to 0-" + (int) s1 + "-" + (int) s2 + "-255";
    }
}
