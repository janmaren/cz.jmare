package cz.jmare.image;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.image.crop.RadiusCropping;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class CroppingTask {
    public static List<CachedImage> crop(List<CachedImage> images, CropConfig cropConfig) {
        if (images.size() == 0) {
            return images;
        }
        CachedImage firstCachedImage = images.get(0);
        RasImageRead rasImageRead = firstCachedImage.getRasImageRead();
        RadiusCropping cropping;
        if (cropConfig.getCropType() == CropType.MANUAL_CENTER_RADIUS) {
            cropping = new RadiusCropping(cropConfig.getCenterX(), cropConfig.getCenterY(), rasImageRead.getWidth(), rasImageRead.getHeight(), cropConfig.getRadiusX(), cropConfig.getRadiusY());
        } else {
            throw new RuntimeException("Invalid crop type");
        }
        List<CachedImage> outputs = new ArrayList<CachedImage>();
        
        for (CachedImage cachedImage : images) {
            RasImageRead croped = cropping.crop(cachedImage);
            CachedImage outputImage = CachedImage.saveAndCache(cachedImage.getFile(), croped);
            outputs.add(outputImage);
        }
        
        return outputs;
    }
}
