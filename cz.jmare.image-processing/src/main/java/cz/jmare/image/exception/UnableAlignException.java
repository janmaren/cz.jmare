package cz.jmare.image.exception;

public class UnableAlignException extends Exception {
    private static final long serialVersionUID = -7827957195952991632L;

    public UnableAlignException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableAlignException(String message) {
        super(message);
    }
}
