package cz.jmare.image.exception;

public class UnableFlatException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7735793909915667373L;

	public UnableFlatException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableFlatException(String message) {
        super(message);
    }
}
