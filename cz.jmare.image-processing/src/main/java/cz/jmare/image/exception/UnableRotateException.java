package cz.jmare.image.exception;

public class UnableRotateException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4253864971298821145L;

	public UnableRotateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableRotateException(String message) {
        super(message);
    }
}
