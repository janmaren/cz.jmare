package cz.jmare.image.exception;

public class UnableAnimateException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1253379485307749052L;

	public UnableAnimateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableAnimateException(String message) {
        super(message);
    }
}
