package cz.jmare.image.exception;

public class UnableSubtrackException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4487386895646177679L;

	public UnableSubtrackException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableSubtrackException(String message) {
        super(message);
    }
}
