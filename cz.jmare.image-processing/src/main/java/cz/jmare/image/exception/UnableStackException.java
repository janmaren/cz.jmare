package cz.jmare.image.exception;

public class UnableStackException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -9211910521620876751L;

	public UnableStackException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableStackException(String message) {
        super(message);
    }
}
