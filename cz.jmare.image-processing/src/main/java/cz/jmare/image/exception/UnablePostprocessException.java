package cz.jmare.image.exception;

public class UnablePostprocessException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5576116829486139740L;

	public UnablePostprocessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnablePostprocessException(String message) {
        super(message);
    }
}
