package cz.jmare.image;

public enum CropType {
    DETECTED_CENTER_RADIUS, MANUAL_CENTER_RADIUS
}
