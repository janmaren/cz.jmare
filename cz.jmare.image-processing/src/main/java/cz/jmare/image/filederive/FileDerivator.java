package cz.jmare.image.filederive;

import java.io.File;

public interface FileDerivator {
    File derive(File inputFile, File outputDir);
    
    String getOutputFormat();
}
