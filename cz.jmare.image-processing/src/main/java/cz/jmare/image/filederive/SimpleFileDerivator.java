package cz.jmare.image.filederive;

import java.io.File;

import cz.jmare.image.util.ImPathUtil;

public class SimpleFileDerivator implements FileDerivator {

    private String outputFormat;

    private String outputNameDistinguisher = "";
    
    public SimpleFileDerivator(String outputFormat) {
        super();
        this.outputFormat = outputFormat;
    }

    public SimpleFileDerivator(String outputFormat, String outputNameDistinguisher) {
        super();
        this.outputFormat = outputFormat;
        this.outputNameDistinguisher = outputNameDistinguisher;
    }

    @Override
    public File derive(File inputFile, File outputDir) {
        String lastPart = ImPathUtil.getLastPartFromPath(inputFile.toString());
        String[] pathSuffix = ImPathUtil.parseNameSuffix(lastPart);
        File outputFile = new File(outputDir, pathSuffix[0] + outputNameDistinguisher + "." + outputFormat);
        return outputFile;
    }

    public void setOutputNameDistinguisher(String outputNameDistinguisher) {
        this.outputNameDistinguisher = outputNameDistinguisher;
    }

    @Override
    public String getOutputFormat() {
        return this.outputFormat;
    }
}
