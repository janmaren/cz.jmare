package cz.jmare.image;

import java.util.function.Supplier;

import cz.jmare.image.filederive.FileDerivator;
import cz.jmare.image.filederive.SimpleFileDerivator;
import cz.jmare.image.resample.NearestNeighbourResampler;
import cz.jmare.image.resample.Resampler;
import cz.jmare.image.util.FormatUtil;

public class MultiAligmentConfig {

    private PixelSelectionType pixelSelectionType = PixelSelectionType.PERCENTILE_BRIGHTNESS;

    /**
     * How much brightness (typically (R+G+B) / 3) the pixel must have to be counted. Range 0..1. When null then detect the value
     */
    private float minPixelBrightness = 0.6f;

    /**
     * Percentile of histogram - only the brightest pixels will be used since this threshold
     */
    private float percentileBrightness = 0.9985f;
    
    /**
     * When populated then ignore such image which has average roundness lower than this value.<br>
     * Range 0..1, 0 accept absolutely flat object, 1 means it must be absolutely round.
     */
    private Float minRoundness;
    
    /**
     * How how pixels with {@link #minPixelBrightness} must an object have not to be ignored
     */
    private int minPixelsNumber = 3;
    
    /**
     * Factory for resampler because each resampling needs own instance.
     */
    private Supplier<Resampler> resamplerProvider = NearestNeighbourResampler::new;
    
    /**
     * Derives what result file and path to generate based on input file
     */
    private FileDerivator fileDerivator = new SimpleFileDerivator("tif");


    /**
     * Result must be under this RMSE otherise image excluded
     */
    private float maxRmse = 2f;

    private ValidRmseType validRmseType = ValidRmseType.STRICT;

    boolean debug;

    public MultiAligmentConfig minPixelBrightness(float minPixelBrightness) {
        this.minPixelBrightness = minPixelBrightness;
        return this;
    }

    public MultiAligmentConfig percentileBrightness(float percentileBrightness) {
        this.percentileBrightness = percentileBrightness;
        return this;
    }

    public MultiAligmentConfig minRoundness(Float minRoundness) {
        this.minRoundness = minRoundness;
        return this;
    }
    
    public MultiAligmentConfig minPixelsNumber(int minPixelsNumber) {
        this.minPixelsNumber = minPixelsNumber;
        return this;
    }
    
    public MultiAligmentConfig debug(boolean debug) {
        this.debug = debug;
        return this;
    }
    
    public MultiAligmentConfig fileDerivator(FileDerivator fileDerivator) {
        this.fileDerivator = fileDerivator;
        return this;
    }

    public Float getMinPixelBrightness() {
        return minPixelBrightness;
    }

    public Float getMinRoundness() {
        return minRoundness;
    }

    public MultiAligmentConfig resamplerProvider(Supplier<Resampler> resamplerProvider) {
        this.resamplerProvider = resamplerProvider;
        return this;
    }
    
    public boolean isDebug() {
        return debug;
    }

    public Supplier<Resampler> getResamplerProvider() {
        return resamplerProvider;
    }

    public FileDerivator getFileDerivator() {
        return fileDerivator;
    }

    @Override
    public String toString() {
        StringBuilder sb = null;
        if (pixelSelectionType == PixelSelectionType.PERCENTILE_BRIGHTNESS) {
            sb = new StringBuilder("Min percentile: " + FormatUtil.toPercents(percentileBrightness));
        } else if (pixelSelectionType == PixelSelectionType.MIN_BRIGHTNESS) {
            sb = new StringBuilder("Min brightness: " + FormatUtil.toPercents(minPixelBrightness));
        } else if (pixelSelectionType == PixelSelectionType.AUTOMATIC) {
            sb = new StringBuilder("Auto");
        }
        sb.append(", max. rmse: ").append(maxRmse);
        sb.append(", pixels number: ").append(minPixelsNumber);
        if (minRoundness != null) {
            sb.append(", req. roundness: ").append((int) (minRoundness * 100)).append("%");
        }
        return sb.toString();
    }

    public int getMinPixelsNumber() {
        return minPixelsNumber;
    }

    public float getMaxRmse() {
        return maxRmse;
    }

    public MultiAligmentConfig maxRmse(float maxRmse) {
        this.maxRmse = maxRmse;
        return this;
    }

    public Float getPercentileBrightness() {
        return percentileBrightness;
    }

    public PixelSelectionType getPixelSelectionType() {
        return pixelSelectionType;
    }

    public MultiAligmentConfig pixelSelectionType(PixelSelectionType pixelSelectionType) {
        this.pixelSelectionType = pixelSelectionType;
        return this;
    }

    public ValidRmseType getValidRmseType() {
        return validRmseType;
    }

    public MultiAligmentConfig validRmseType(ValidRmseType validRmseType) {
        this.validRmseType = validRmseType;
        return this;
    }
}
