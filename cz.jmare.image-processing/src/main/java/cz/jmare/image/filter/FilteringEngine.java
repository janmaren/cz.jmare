package cz.jmare.image.filter;

import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.PathUtil;

import java.io.File;
import java.io.IOException;

public class FilteringEngine {
    /**
     * Filter input file by weights and save it to cached temporary file
     * @param inputImage
     * @param weights
     * @return
     */
    public static CachedImage filter(CachedImage inputImage, float[][] weights) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("filtering-", PathUtil.toFullPathAndSuffix(inputImage.getFile().getPath())[1]);
            tempFile.deleteOnExit();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return filter(inputImage, weights, tempFile);
    }

    /**
     * Filter input file by weights and save it to cached output file
     * @param inputImage
     * @param weights
     * @param outputFile
     * @return
     */
    public static CachedImage filter(File inputImage, float[][] weights, File outputFile) {
        return filter(new CachedImage(inputImage), weights, outputFile);
    }

    /**
     * Filter cached input file by weights and save it to cached output file
     * @param inputImage
     * @param weights
     * @param outputFile
     * @return
     */
    public static CachedImage filter(CachedImage inputImage, float[][] weights, File outputFile) {
        RasImageRead rasImageRead = inputImage.getRasImageRead();

        RasImageFloat output = new RasImageFloat(rasImageRead.getWidth(), rasImageRead.getHeight());
        int wheight = weights.length;
        int wwidth = weights[0].length;
        int cx = wwidth / 2;
        int cy = wheight / 2;
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < rasImageRead.getWidth(); j++) {
                float r = 0;
                float g = 0;
                float b = 0;
                float wsum = 0;
                for (int k = 0; k < wheight; k++) {
                    for (int l = 0; l < wwidth; l++) {
                        int x = j - cx + l;
                        if (x < 0 || x >= width) {
                            continue;
                        }
                        int y = i - cy + k;
                        if (y < 0 || y >= height) {
                            continue;
                        }
                        RGBFloat rgbFloat = rasImageRead.getRGBFloat(x, y);
                        float w = weights[k][l];
                        r += rgbFloat.red * w;
                        g += rgbFloat.green * w;
                        b += rgbFloat.blue * w;
                        wsum += w;
                    }
                }

                float red = r / wsum;
                if (red < 0) red = 0;
                if (red > 1) red = 1;

                float green = g / wsum;
                if (green < 0) green = 0;
                if (green > 1) green = 1;

                float blue = b / wsum;
                if (blue < 0) blue = 0;
                if (blue > 1) blue = 1;

                RGBFloat outRGB = new RGBFloat(red, green, blue);
                output.setRGBFloat(j, i, outRGB);
            }
        }

        CachedImage outputImage = CachedImage.saveAndCache(outputFile, output);
        return outputImage;
    }

    public static void main(String[] args) {
        FilteringEngine.filter(new File("d:\\Pictures\\hvezdy-20221022\\m103\\DSC_0039.JPG"), Masks.smooth7To7, new File("d:\\Pictures\\hvezdy-20221022\\m103\\DSC_0039.png"));
    }
}
