package cz.jmare.image.gif;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

public class CreateGifExample {

    public static void main(String[] args) throws Exception {
        File[] files = new File("C:\\temp\\out\\").listFiles();
        
        ImageOutputStream output = new FileImageOutputStream(new File("c:\\temp\\out.gif"));

        File first = files[0];
        BufferedImage firstImage = ImageIO.read(first);
        GifSequencer writer = new GifSequencer(output, firstImage.getType(), 10, true);
        //writer.writeToSequence(first);

//        File[] images = new File[]{
//                new File("c:\\temp\\duke-image-watermarked.jpg"),
//                new File("c:\\temp\\duke.jpg"),
//        };
        for (File image : files) {
            BufferedImage next;
            try {
                next = ImageIO.read(image);
                Graphics2D graphics = next.createGraphics();
                graphics.setColor(Color.GRAY);
                graphics.setStroke(new BasicStroke(2));
                graphics.drawLine(42, 28, 42, 36);
                graphics.drawLine(27, 44, 35, 44);
                graphics.drawLine(90, 38, 91, 16);
                graphics.setFont(new Font("Verdana", Font.PLAIN, 9));
                graphics.drawString("Sirius B, 22-03-02", 2, 90);
                graphics.drawString("N", 87, 10);
                writer.writeToSequence(next);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(image.toString());
            }
        }

        writer.close();
        output.close();
    }
}
