package cz.jmare.image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.maren.statis.affine.AffineElements;

import cz.jmare.image.align.MultiAlignUniEngine;
import cz.jmare.image.align.validrmse.RmseResult;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.exception.ProcessInterruptedException;
import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.filederive.FileDerivator;
import cz.jmare.image.ras.RasImageFactory;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.FormatUtil;
import cz.jmare.image.util.ImFilesUtil;

public class MultiAligmentTask {
    public static List<CachedImage> alignImages(File inputDir, File outputDir) throws IOException, UnableAlignException {
        return alignImages(inputDir, outputDir, new MultiAligmentConfig());
    }
    
    public static List<CachedImage> alignImages(File inputDir, File outputDir, MultiAligmentConfig alignConfig) throws IOException, UnableAlignException {
        List<CachedImage> filesListCached = ImFilesUtil.filesListCached(inputDir);
        CachedImage baseCachedImage = null;
        if (filesListCached.size() < 2) {
            System.out.println("No files to align");
            throw new UnableAlignException("Nothing to align");
        }
        baseCachedImage = filesListCached.get(0);
        filesListCached.remove(0);
        return alignImages(baseCachedImage, filesListCached, outputDir, alignConfig);
    }
    
    /**
     * Align images. Base image returned as first (0)
     * @param baseFile
     * @param inputFiles
     * @param outputDir
     * @param alignConfig
     * @return
     * @throws IOException
     * @throws UnableAlignException
     */
    public static List<CachedImage> alignImages(CachedImage baseFile, List<CachedImage> inputFiles, File outputDir, MultiAligmentConfig alignConfig) throws IOException, UnableAlignException {
        if (inputFiles.isEmpty()) {
            return List.of();
        }
        RasImageRead baseImage = baseFile.getRasImageRead();
        
        List<CachedImage> resultImages = new ArrayList<CachedImage>();
        
        FileDerivator fileDerivator = alignConfig.getFileDerivator();

        List<CachedImage> validInputFiles = new ArrayList<CachedImage>();
        boolean exists8bit = false;
        int counter = 0;
        for (CachedImage cachedImage : inputFiles) {
            counter++;
            System.out.println("> [" + counter + "/" + inputFiles.size() + "] Processing " + cachedImage.getFile() + " <");
            try {
                MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseFile, cachedImage, alignConfig);
                if (alignConfig.isDebug()) {
                    System.out.println("Min pixel brightness" + (multiAlignEngine.getPercentileBrightness() != null ? " for percentile " + FormatUtil.toPercents(multiAlignEngine.getPercentileBrightness()) : "") + " is " + FormatUtil.toPercents(multiAlignEngine.getMinPixelBrightness()));
                }
                AffineElements affine = multiAlignEngine.calcAffineElements();
                if (multiAlignEngine.getRmseResult().rmse > alignConfig.getMaxRmse()) {
                    System.out.println(rmseResultString(multiAlignEngine.getRmseResult()) + ", rmse > " + alignConfig.getMaxRmse() + "pix => skipping");
                    continue;
                }
                validInputFiles.add(cachedImage);
                System.out.println(rmseResultString(multiAlignEngine.getRmseResult()));
                exists8bit |= !RasImageFactory.isRas16Bit(cachedImage.getRasImageRead());

                RasImageRead transformed = ResamplerEngine.transform(cachedImage.getRasImageRead(), baseImage.getWidth(), baseImage.getHeight(), alignConfig.getResamplerProvider(), affine);

                File outputFile = fileDerivator.derive(cachedImage.getFile(), outputDir);
                outputFile.getParentFile().mkdirs();
                CachedImage outputCachedImage = CachedImage.saveAndCache(outputFile, transformed);
                resultImages.add(outputCachedImage);
            } catch (ProcessInterruptedException e) {
                throw e;
            } catch (UnableAlignException e) {
                System.out.println("Unable to align " + (e.getMessage() != null ? " - " + e.getMessage() : ""));
            } catch (Exception e) {
                System.out.println("Error processing " + (e.getMessage() != null ? " - " + e.getMessage() : ""));
            }
            if (Thread.interrupted()) {
                throw new ProcessInterruptedException();
            }
        }
        if (exists8bit) {
            System.out.println("- Warning: there is at least one input file with only 8bits per channel -");
        }

        return resultImages;
    }

    public static String rmseResultString(RmseResult rmseResult) {
        String str = "RMSE: " + FormatUtil.number(rmseResult.rmse) + "pix";
        int overallCount = rmseResult.count + rmseResult.notFoundCount;
        int match = rmseResult.count * 100 / overallCount;
        str += ", match: " + match + "% of " + overallCount;
        return str;
    }

    public static float calcMinPixelBrightness(CachedImage baseFile, int minPixelsNumber) {
        float minBright = 0.5f;
        float threshold = 0.3e-4f;
        Integer lastSize = null;
        while (threshold < 2.1e-4f) {
            minBright = HistogramStat.invCdfValueFloat(baseFile.getRasImageRead(), 1f - threshold);
            CentroidsAnalyzer analyzer = new CentroidsAnalyzer(baseFile, minBright, minPixelsNumber);
            Set<Centroid> centroids = analyzer.centroids();
            if (centroids.size() >= 50) {
                break;
            }
            if (minBright <= 0.45f) {
                break;
            }
            if (lastSize != null) {
                if (lastSize > centroids.size()) {
                    break;
                }
            }
            lastSize = centroids.size();
            threshold += 0.1e-4f;
        }
        return minBright;
    }
    
    public static void main(String[] args) throws IOException, UnableAlignException {
        alignImages(new File("C:\\temp\\m42\\"), new File("C:\\temp\\m42\\aligns"));
        //alignImages(new File("C:\\temp\\trailing\\"), new File("C:\\temp\\trailing\\out"), null, 550, new SimpleFileDerivator("png"));
    }
}
