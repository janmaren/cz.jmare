package cz.jmare.image;

public enum PixelSelectionType {
    MIN_BRIGHTNESS, PERCENTILE_BRIGHTNESS, AUTOMATIC
}
