package cz.jmare.image.align.validrmse;

import cz.jmare.image.centroid.Centroid;

import java.util.List;

public class BelieveMatchValidRmse implements ValidRmse {
    private int needRmsCountFound;

    public BelieveMatchValidRmse(List<Centroid> bcentroids, List<Centroid> acentroids) {
        int min = Math.min(bcentroids.size(), acentroids.size());
        needRmsCountFound = (int) (min * 0.25);
        if (needRmsCountFound < 4) {
            needRmsCountFound = 4;
        }
    }

    @Override
    public boolean isValidRmse(RmseResult rms) {
        return rms.count >= needRmsCountFound;
    }
}
