package cz.jmare.image.align;

import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;

import java.util.List;

public class CompareTriangleUtil {
    public static double rank(List<ControlPoint> refs) {
        ControlPoint cp0 = refs.get(0);
        ControlPoint cp1 = refs.get(1);
        ControlPoint cp2 = refs.get(2);
        double r1 = rank(cp0.getPoint1(), cp1.getPoint1(), cp2.getPoint1(),
                cp0.getPoint2(), cp1.getPoint2(), cp2.getPoint2());
        double r2 = rank(cp0.getPoint1(), cp1.getPoint1(), cp2.getPoint1(),
                cp1.getPoint2(), cp2.getPoint2(), cp0.getPoint2());
        double r3 = rank(cp0.getPoint1(), cp1.getPoint1(), cp2.getPoint1(),
                cp2.getPoint2(), cp0.getPoint2(), cp1.getPoint2());
        return Math.min(Math.min(r1, r2), r3);
    }

    private static double rank(Coordinate pointb0, Coordinate pointb1, Coordinate pointb2,
                               Coordinate pointa0, Coordinate pointa1, Coordinate pointa2) {
        double ratio = distance(pointb0, pointb1) / distance(pointa0, pointa1);

        double expected1 = distance(pointa1, pointa2) * ratio;
        double real1 = distance(pointb1, pointb2);

        double expected2 = distance(pointa2, pointa0) * ratio;
        double real2 = distance(pointb2, pointb0);

        double d1 = expected1 - real1;
        double d2 = expected2 - real2;

        return Math.sqrt(d1 * d1 + d2 * d2);
    }

    private static double distance(Coordinate point1, Coordinate point2) {
        double d1 = point1.x - point2.x;
        double d2 = point1.y - point2.y;

        return Math.sqrt(d1 * d1 + d2 * d2);
    }
}
