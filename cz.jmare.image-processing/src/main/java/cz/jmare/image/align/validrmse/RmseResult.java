package cz.jmare.image.align.validrmse;

import java.util.List;

import cz.jmare.image.align.audit.PointMatchAudit;

public class RmseResult {
    public Float rmse;

    public int count;

    public int notFoundCount;
    public List<PointMatchAudit> auPoints;

    public RmseResult(Float rmse, Integer count) {
        this.rmse = rmse;
        this.count = count;
    }

    public RmseResult(Float rmse, Integer count, int notFoundCount) {
        this.rmse = rmse;
        this.count = count;
        this.notFoundCount = notFoundCount;
    }

    @Override
    public String toString() {
        return "rmse=" + (rmse != Float.MAX_VALUE ? rmse : "") + " (matching count is " + count + "), (not found count is " + notFoundCount + ")";
    }
}
