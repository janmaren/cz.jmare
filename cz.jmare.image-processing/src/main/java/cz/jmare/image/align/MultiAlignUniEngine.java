package cz.jmare.image.align;

import cz.jmare.image.DebugUtil;
import cz.jmare.image.MultiAligmentConfig;
import cz.jmare.image.PixelSelectionType;
import cz.jmare.image.ValidRmseType;
import cz.jmare.image.align.audit.PointMatchAudit;
import cz.jmare.image.align.selector.CenSelectorUtil;
import cz.jmare.image.align.selector.CentroSelector;
import cz.jmare.image.align.selector.CircleSelector;
import cz.jmare.image.align.selector.NextSeqCentroSelector;
import cz.jmare.image.align.validrmse.BelieveMatchValidRmse;
import cz.jmare.image.align.validrmse.RmseResult;
import cz.jmare.image.align.validrmse.StrictNotFoundValidRmse;
import cz.jmare.image.align.validrmse.ValidRmse;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.centroid.CentroidsStats;
import cz.jmare.image.exception.ProcessInterruptedException;
import cz.jmare.image.exception.UnableAlignException;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.*;
import org.maren.statis.affine.AffineElements;
import org.maren.statis.affine.AffineTransCoefsCalculator;
import org.maren.statis.entity.ControlPoint;
import org.maren.statis.entity.Coordinate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MultiAlignUniEngine {
    private final static int[][] COMBINES = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};

    /**
     * Matching star must be found in this limit in pixels
     */
    private static final float RMS_DIST_PIX_LIMIT = 6;
    /**
     * Stars with distance greater than limit will be omitted from calculation of affine coeficients
     */
    private static final float AFFIN_DIST_PIX_LIMIT = 2;

    public static final int LIMIT_OBJECTS_NUMBER = 100;

    /**
     * Count of ref. points used for affine coefficients calculation
     */
    private int refPointsCount;

    private int baseCentroidsCount;

    /**
     * The overall RMS
     */
    private RmseResult rmsResult;

    private CentroSelector centroSelector = new NextSeqCentroSelector(6, 2);

    List<List<PointMatchAudit>> audits = new ArrayList<>();

    @SuppressWarnings("unused")
	private final CachedImage baseImage;

    private final CachedImage aligningImage;

    private final MultiAligmentConfig alignConfig;


    private CentroidsAnalyzer baseAnalyzer;
    private CentroidsAnalyzer aligningAnalyzer;

    private Logger logger = new SimpleLogger();
    private Float percentileBrightness;
    private Float minPixelBrightness;

    public static CachedImage align(CachedImage baseImage, CachedImage aligningImage, File outputFile) throws UnableAlignException {
        return align(baseImage, aligningImage, outputFile, new MultiAligmentConfig());
    }

    public static CachedImage align(CachedImage baseImage, CachedImage aligningImage, File outputFile, MultiAligmentConfig alignConfig) throws UnableAlignException {
        MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseImage, aligningImage, alignConfig);
        AffineElements affineElements = multiAlignEngine.calcAffineElements();
        if (affineElements == null) {
            throw new UnableAlignException("Align not found");
        }
        RasImageRead rasImageRead = baseImage.getRasImageRead();
        RasImageRead transformed = ResamplerEngine.transform(aligningImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), alignConfig.getResamplerProvider(), affineElements);
        CachedImage cachedImage = CachedImage.saveAndCache(outputFile, transformed);
        return cachedImage;
    }

    public MultiAlignUniEngine(CachedImage baseImage, CachedImage aligningImage) throws UnableAlignException {
        this(baseImage, aligningImage, new MultiAligmentConfig());
    }

    public MultiAlignUniEngine(CachedImage baseImage, CachedImage aligningImage, MultiAligmentConfig alignConfig) {
        this.baseImage = baseImage;
        this.aligningImage = aligningImage;
        this.alignConfig = alignConfig;

        minPixelBrightness = alignConfig.getMinPixelBrightness();
        percentileBrightness = alignConfig.getPercentileBrightness();
        if (alignConfig.getPixelSelectionType() == PixelSelectionType.AUTOMATIC) {
            baseAnalyzer = FindAnalyzerUtil.findAnalyzer(baseImage, alignConfig);
            aligningAnalyzer = FindAnalyzerUtil.findAnalyzer(aligningImage, alignConfig);
            minPixelBrightness = aligningAnalyzer.minPixelBrightness();
        } else {
            Float baseMinPixelBrightness = minPixelBrightness;
            if (alignConfig.getPixelSelectionType() == PixelSelectionType.PERCENTILE_BRIGHTNESS) {
                minPixelBrightness = HistogramStat.calcThresholdByPercentil(aligningImage.getRasImageRead(), alignConfig.getPercentileBrightness());
                baseMinPixelBrightness = HistogramStat.calcThresholdByPercentil(baseImage.getRasImageRead(), alignConfig.getPercentileBrightness());
            } else {
                percentileBrightness = HistogramStat.calcPercentilByThreshold(aligningImage.getRasImageRead(), minPixelBrightness);
            }
            baseAnalyzer = new CentroidsAnalyzer(baseImage, baseMinPixelBrightness, alignConfig.getMinPixelsNumber());
            aligningAnalyzer = new CentroidsAnalyzer(aligningImage, minPixelBrightness, alignConfig.getMinPixelsNumber());
        }
    }

    public AffineElements calcAffineElements() throws UnableAlignException {
        if (alignConfig.isDebug()) {
            log("Count of centroids, base: " + baseAnalyzer.centroids().size() + ", aligning: " + aligningAnalyzer.centroids().size());
        }

        List<Centroid> bAllcentroids = baseAnalyzer.centroidsSortedScoreDesc();
        List<Centroid> aAllcentroids = aligningAnalyzer.centroidsSortedScoreDesc();

        int bindex = truncateToIndex(bAllcentroids, 10f, 10);
        bAllcentroids = bAllcentroids.subList(bindex, bAllcentroids.size());
        int aindex = truncateToIndex(aAllcentroids, 10f, 10);
        aAllcentroids = aAllcentroids.subList(aindex, aAllcentroids.size());
        List<Centroid> bcentroids = bAllcentroids;
        List<Centroid> acentroids = aAllcentroids;
        int bsize = bcentroids.size();
        int asize = acentroids.size();
        if (bsize > LIMIT_OBJECTS_NUMBER) {
            bcentroids = bcentroids.subList(0, LIMIT_OBJECTS_NUMBER);
        }
        if (asize > LIMIT_OBJECTS_NUMBER) {
            acentroids = acentroids.subList(0, LIMIT_OBJECTS_NUMBER);
        }
        baseCentroidsCount = bcentroids.size();

        if (alignConfig.getMinRoundness() != null) {
            CentroidsStats centroidsStats = new CentroidsStats(aligningAnalyzer);
            if (centroidsStats.getAvgRoundness() == null) {
                throw new UnableAlignException("Too small objects for which the roundness can't be calculated");
            }
            if (centroidsStats.getAvgRoundness() < alignConfig.getMinRoundness()) {
                throw new UnableAlignException("Trailing - skipped");
            }
        }

        Centroid[] baseRefs = new Centroid[3];
        Centroid[] aligRefs = new Centroid[3];
        float bestRmse = Float.MAX_VALUE;
        List<ControlPoint> bestRefs = null;
        AffineElements bestCoefsAll = null;
        RmseResult rmseResultAll = null;
        boolean tryiedRms = false;
        Integer bestMatchingCount = null;
        int maxMatchingCount = 0;
        Float maxMatchingCountRmse = null;

        int[][] centroIndexes = centroSelector.getCentroIndexes(bcentroids);

        CircleSelector circleSelector = new CircleSelector(baseAnalyzer.getWidth(), baseAnalyzer.getHeight());
        int[][] circleIndexes = circleSelector.getCentroIndexes(bcentroids);
        centroIndexes = CenSelectorUtil.concatIndexesNoDupl(circleIndexes, centroIndexes);

        ValidRmse validRms = null;
        if (alignConfig.getValidRmseType() == ValidRmseType.BELIEVE) {
            validRms = new BelieveMatchValidRmse(bcentroids, acentroids);
        } else {
            validRms = new StrictNotFoundValidRmse(bcentroids, acentroids);
        }

        Thread currentThread = Thread.currentThread();

        mainLoop: for (int[] centroIndex : centroIndexes) {
            baseRefs[0] = bcentroids.get(centroIndex[0]);
            baseRefs[1] = bcentroids.get(centroIndex[1]);
            baseRefs[2] = bcentroids.get(centroIndex[2]);

            for (int l = 0; l < acentroids.size() - 2; l++) {
                aligRefs[0] = acentroids.get(l);
                for (int m = l + 1; m < acentroids.size() - 1; m++) {
                    aligRefs[1] = acentroids.get(m);
                    for (int n = m + 1; n < acentroids.size(); n++) {
                        aligRefs[2] = acentroids.get(n);

                        for (int[] combine : COMBINES) {
                            List<ControlPoint> refs = new ArrayList<>();
                            refs.add(new ControlPoint(baseRefs[0].x, baseRefs[0].y, aligRefs[combine[0]].x, aligRefs[combine[0]].y));
                            refs.add(new ControlPoint(baseRefs[1].x, baseRefs[1].y, aligRefs[combine[1]].x, aligRefs[combine[1]].y));
                            refs.add(new ControlPoint(baseRefs[2].x, baseRefs[2].y, aligRefs[combine[2]].x, aligRefs[combine[2]].y));

                            if (CompareTriangleUtil.rank(refs) > 3.0) {
                                continue; // optimalisation - not 2 simliar triangles
                            }

                            AffineElements coefs = AffineTransCoefsCalculator.calcCoefs(refs);
                            if (coefs == null) {
                                // all 3 point in one line
                                continue;
                            }

                            tryiedRms = true;
                            
                            RmseResult rmseResult = calcRmse(coefs, bcentroids, acentroids, baseAnalyzer.getWidth(), baseAnalyzer.getHeight(), alignConfig.isDebug());

                            if (rmseResult.count > maxMatchingCount) {
                                maxMatchingCount = rmseResult.count;
                                maxMatchingCountRmse = rmseResult.rmse;
                            }

                            if (rmseResult.rmse < bestRmse && validRms.isValidRmse(rmseResult)) {
                                if (alignConfig.isDebug()) {
                                    log("Found: " + rmseResult);
                                }

                                bestRmse = rmseResult.rmse;
                                bestRefs = refs;

                                if (alignConfig.isDebug()) {
                                    audits.add(rmseResult.auPoints);
                                }

                                bestCoefsAll = calcAffineUsingAll(coefs, bcentroids, acentroids);
                                rmseResultAll = calcRmse(bestCoefsAll, bcentroids, acentroids, baseAnalyzer.getWidth(), baseAnalyzer.getHeight(), alignConfig.isDebug());
                                if (rmseResultAll.rmse < 0.5) {
                                    if (alignConfig.isDebug()) {
                                        log("RMSE using all points is low enough: " + rmseResultAll.rmse + ", breaking");
                                    }
                                    break mainLoop;
                                }
                            }
                            if (bestMatchingCount == null) {
                                bestMatchingCount = rmseResult.count;
                            } else {
                                if (bestMatchingCount < rmseResult.count) {
                                    bestMatchingCount = rmseResult.count;
                                }
                            }
                        }
                    }
                }
                if (currentThread.isInterrupted()) {
                    throw new ProcessInterruptedException();
                }
            }
        }

        if (alignConfig.isDebug()) {
            log("Best rmse: " + bestRmse + (rmseResultAll != null ? ", using all: " + rmseResultAll : ""));
        }

        if (alignConfig.isDebug()) {
            AlignDebugUtil.debugCentroidsToImageFile(aligningImage, aAllcentroids, acentroids, bestRefs, bestRmse, rmseResultAll, maxMatchingCount, maxMatchingCountRmse, bAllcentroids.size(), aAllcentroids.size());
        }

        if (rmseResultAll == null) {
            if (tryiedRms) {
                if (alignConfig.isDebug()) {
                    log("Found some matching pairs (" + bestMatchingCount + ") but it's not enough");
                }
                throw new UnableAlignException("Found some matching pairs (" + bestMatchingCount + ") but it's not enough");
            } else {
                if (alignConfig.isDebug()) {
                    log("Not found 3 similar matching objects which could be used to calculate transformation");
                }
                throw new UnableAlignException("Not found 3 similar matching objects which could be used to calculate transformation");
            }
        }
        
        if (alignConfig.isDebug()) {
            log("Using ref. points with RMS " + bestRmse + ":");
            log(bestRefs);
        }
        
        if (alignConfig.isDebug()) {
            log("RMSE using all is: " + rmseResultAll);
            log("Result coefs: " + bestCoefsAll);
            
            if (rmseResultAll.rmse > 1) {
                log("RMS bigger than 1: " + rmseResultAll.rmse);
            }
        }

        this.rmsResult = rmseResultAll;
        return bestCoefsAll;
    }

    /**
     *
     * @param centroids
     * @param factor
     * @return up to index to truncate excluding
     */
    private static int truncateToIndex(List<Centroid> centroids, float factor, int number) {
        if (centroids.size() == 0) {
            return 0;
        }
        float prevScore = centroids.get(0).score;
        int i = 1;
        int count = Math.min(number, centroids.size());
        for (; i < count; i++) {
            float score = centroids.get(i).score;
            if (prevScore / score > factor) {
                return i;
            }
            prevScore = score;
        }
        return 0;
    }

    private AffineElements calcAffineUsingAll(AffineElements coefs, List<Centroid> bcentroids, List<Centroid> acentroids) throws UnableAlignException {
        List<ControlPoint> refs = new ArrayList<ControlPoint>();
        for (Centroid bCentroid : bcentroids) {
            Coordinate transformed = coefs.transform(bCentroid.x, bCentroid.y);
            NearResult nearestAligning = findNearestCentroid(transformed.x, transformed.y, acentroids, 0);
            if (nearestAligning == null) {
                continue;
            }
            if (nearestAligning.distance > AFFIN_DIST_PIX_LIMIT) {
                continue;
            }
            Centroid aCentroid = acentroids.get(nearestAligning.index);
            refs.add(new ControlPoint(bCentroid.x, bCentroid.y, aCentroid.x, aCentroid.y));
        }
        AffineElements resutCoefs = AffineTransCoefsCalculator.calcCoefs(refs);
        if (resutCoefs == null) {
            throw new UnableAlignException("Expected to find result coefficients using " + bcentroids.size() + " points but not calculated. Input coefs.: " + coefs);
        }
        this.refPointsCount = refs.size();
        return resutCoefs;
    }

    private static RmseResult calcRmse(AffineElements coefs, List<Centroid> bcentroids, List<Centroid> acentroids, int width, int height, boolean audit) {
        List<PointMatchAudit> auPoints = audit ? new ArrayList<>() : null;
        double sum = 0;
        int count = 0;
        int notFound = 0;
        for (Centroid bcentroid : bcentroids) {
            Coordinate transformed = coefs.transform(bcentroid.x, bcentroid.y);
            if (transformed.x < 0 || transformed.x >= width || transformed.y < 0 || transformed.y >= height) {
                continue;
            }
            NearResult nearestAligning = findNearestCentroid(transformed.x, transformed.y, acentroids, 1.5);
            if (audit) {
                PointMatchAudit pointMatchAudit = new PointMatchAudit(transformed.x, transformed.y, nearestAligning != null ? nearestAligning.distance : null);
                auPoints.add(pointMatchAudit);
            }
            if (nearestAligning == null || nearestAligning.distance > RMS_DIST_PIX_LIMIT) {
                // we don't count when it's at the edge
                if (transformed.x - RMS_DIST_PIX_LIMIT >= 0 && transformed.y - RMS_DIST_PIX_LIMIT >= 0 && transformed.x + RMS_DIST_PIX_LIMIT < width && transformed.y + RMS_DIST_PIX_LIMIT < height) {
                    notFound++;
                }
                continue;
            }
            sum += nearestAligning.distance * nearestAligning.distance;
            count++;
        }

        if (count == 0) {
            RmseResult rmseResult = new RmseResult(Float.MAX_VALUE, count, notFound);
            rmseResult.auPoints = auPoints;
            return rmseResult;
        }

        float rms = (float) Math.sqrt(sum / count);
        RmseResult rmseResult = new RmseResult(rms, count, notFound);
        rmseResult.auPoints = auPoints;
        return rmseResult;
    }

    /**
     * For base centroid on x and y and with baseClusterScore find nearest object with similar size
     *
     * @param x          coordinate of base centroid
     * @param y          coordinate of base centroid
     * @param acentroids centroids to be searched
     * @param lesserBreakThreshold the search is terminated immediatelly when found distance lesser than this threshold. Use 0 when rather wishing to loop all items
     * @return
     */
    public static NearResult findNearestCentroid(double x, double y, List<Centroid> acentroids, double lesserBreakThreshold) {
        double nearestValue = Double.MAX_VALUE;
        Integer nearestIndex = null;
        for (int i = 0; i < acentroids.size(); i++) {
            Centroid centroid = acentroids.get(i);
            double dx = x - centroid.x;
            double dy = y - centroid.y;
            double pyth = Math.sqrt(dx * dx + dy * dy);
            if (pyth < nearestValue) {
                nearestValue = pyth;
                nearestIndex = i;
                if (nearestValue < lesserBreakThreshold) {
                    break;
                }
            }
        }
        if (nearestIndex == null) {
            return null;
        }
        return new NearResult(nearestIndex, nearestValue);
    }

    public int getRefPointsCount() {
        return refPointsCount;
    }

    public RmseResult getRmseResult() {
        return rmsResult;
    }

    public int getBaseCentroidsCount() {
        return baseCentroidsCount;
    }

    public List<List<PointMatchAudit>> getAudits() {
        return audits;
    }

    public CentroidsAnalyzer getBaseAnalyzer() {
        return baseAnalyzer;
    }

    public CentroidsAnalyzer getAligningAnalyzer() {
        return aligningAnalyzer;
    }

    public static class NearResult {
        public int index;
        public double distance;
        public NearResult(int index, double distance) {
            super();
            this.index = index;
            this.distance = distance;
        }
    }
    
    private void log(String message) {
        logger.log(message);
    }

    private void log(Object message) {
        if (message == null) {
            return;
        }
        logger.log(message.toString());
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public static void main(String[] args) throws IOException, UnableAlignException {
        CachedImage baseImage = new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\ali5\\DSC_0040-red.png"));
        CachedImage aligningImage = new CachedImage(new File("C:\\Users\\jmarencik\\Pictures\\ali5\\DSC_0040-green.png"));
        MultiAligmentConfig alignConfig = new MultiAligmentConfig();
        //alignConfig.minPixelBrightness(0.2f);
        alignConfig.debug(true);

        //DebugUtil.saveImageShowCentroids(baseImage, baseAnalyzer);

        MultiAlignUniEngine multiAlignEngine = new MultiAlignUniEngine(baseImage, aligningImage, alignConfig);
        AffineElements affineElements = null;
        try {
            affineElements = multiAlignEngine.calcAffineElements();

            RasImageRead rasImageRead = baseImage.getRasImageRead();
            RasImageRead transformed = ResamplerEngine.transform(aligningImage.getRasImageRead(), rasImageRead.getWidth(), rasImageRead.getHeight(), alignConfig.getResamplerProvider(), affineElements);
            @SuppressWarnings("unused")
			CachedImage cachedImage = CachedImage.saveAndCache(new File("C:\\Users\\jmarencik\\Pictures\\ali5\\DSC_0040-green-out.png"), transformed);
        } catch (UnableAlignException e) {
            System.out.println("--- ERROR ---");
        }
        List<List<PointMatchAudit>> attempts = multiAlignEngine.getAudits();
        String hex = DebugUtil.genHex();
        for (int i = 0; i < attempts.size(); i++) {
            List<PointMatchAudit> auPoints = attempts.get(i);
            System.out.println("----------------");
            System.out.println(auPoints);
            DebugUtil.saveImageShowMatches(aligningImage, multiAlignEngine.getBaseAnalyzer(), auPoints, i, hex);
        }
    }

    public Float getPercentileBrightness() {
        return percentileBrightness;
    }

    public Float getMinPixelBrightness() {
        return minPixelBrightness;
    }
}
