package cz.jmare.image.align;

import java.util.ArrayList;
import java.util.Collections;

import cz.jmare.image.exception.UnableAlignException;
import org.maren.statis.affine.AffineElements;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.resample.BilinearResampler;
import cz.jmare.image.resample.ResamplerEngine;
import cz.jmare.image.util.CachedImage;

public class SingleAlignEngine {
    
    /**
     * Suitable for aligning by one ref. point which is a centroid on both images (e.g. a center in planet). Image for aligning is
     * moved by the delta x, y of both centroids.
     * @param baseCentroid
     * @param baseWidth
     * @param baseHeight
     * @param toTransImage
     * @return
     * @throws UnableAlignException
     */
    public static RasImageRead alignByDelta(Centroid baseCentroid, int baseWidth, int baseHeight, CachedImage toTransImage, float minPixelBrightness) throws UnableAlignException {
        CentroidsAnalyzer toTransAnalyzer = new CentroidsAnalyzer(toTransImage, minPixelBrightness);
        ArrayList<Centroid> toTransList = new ArrayList<>(toTransAnalyzer.centroids());
        Collections.sort(toTransList, (o1, o2) -> Float.valueOf(o2.score).compareTo(o1.score));
        if (toTransList.size() == 0) {
            throw new UnableAlignException("No trans. object");
        }
        Centroid toTransCentroid = toTransList.get(0);
    
        double dx = toTransCentroid.x - baseCentroid.x;
        double dy = toTransCentroid.y - baseCentroid.y;
        int dintx = (int) Math.round(dx);
        int dinty = (int) Math.round(dy);
        
        AffineElements affineElements = new AffineElements(1, 0, dintx, 0, 1, dinty);
        RasImageRead transformed = ResamplerEngine.transform(toTransImage.getRasImageRead(), baseWidth, baseHeight, BilinearResampler::new, affineElements);
        return transformed;
    }
}
