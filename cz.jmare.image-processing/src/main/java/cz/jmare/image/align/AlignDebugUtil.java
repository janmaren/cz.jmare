package cz.jmare.image.align;

import cz.jmare.image.align.validrmse.RmseResult;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasBufImage;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImConversionsUtil;
import cz.jmare.image.util.ImPathUtil;
import org.maren.statis.entity.ControlPoint;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.StringJoiner;

public class AlignDebugUtil {
    public static void debugCentroidsToImageFile(CachedImage aligningImage, List<Centroid> aAllcentroids, List<Centroid> aCentroids,
                                                 List<ControlPoint> bestRefs, Float bestRmse, RmseResult rmseResultAll,
                                                 int maxMatchingCount, Float maxMatchingCountRmse,
                                                 int bAllNumber, int aAllNumber) {
        int width = aligningImage.getRasImageRead().getWidth();
        int height = aligningImage.getRasImageRead().getHeight();
        RasBufImage outputDebugImage = RasBufImage.createShortsInstance(width, height);
        ImConversionsUtil.copy(aligningImage.getRasImageRead(), outputDebugImage);
        RGBFloat debugPix = new RGBFloat(0 / 255f, 255 / 255f, 64 / 255f);
        for (Centroid centroid : aAllcentroids) {
            outputDebugImage.setRGBFloat((int) centroid.x, (int) centroid.y, debugPix);
        }
        for (Centroid centroid : aCentroids) {
            if (centroid.x > 0) {
                outputDebugImage.setRGBFloat((int) centroid.x - 1, (int) centroid.y, debugPix);
            }
            outputDebugImage.setRGBFloat((int) centroid.x, (int) centroid.y, debugPix);
            if (centroid.x + 1< width) {
                outputDebugImage.setRGBFloat((int) centroid.x + 1, (int) centroid.y, debugPix);
            }
            if (centroid.y > 0) {
                outputDebugImage.setRGBFloat((int) centroid.x, (int) centroid.y - 1, debugPix);
            }
            if (centroid.y + 1 < height) {
                outputDebugImage.setRGBFloat((int) centroid.x, (int) centroid.y + 1, debugPix);
            }
        }
        File debugDir = aligningImage.getFile().getParentFile();
        File debCentrs = new File(debugDir, "debug-centroids");
        debCentrs.mkdirs();
        String[] strings = ImPathUtil.parseNameSuffix(aligningImage.getFile().getName());
        BufferedImage bufferedImage = outputDebugImage.getBufferedImage();
        Graphics g = bufferedImage.getGraphics();
        Font font = g.getFont().deriveFont(50f);
        g.setFont(font);
        Color textColor = new Color(255, 127, 39);
        g.setColor(textColor);
        StringJoiner firstLine = new StringJoiner(", ");
        if (bestRmse != null) {
            firstLine.add("Best RMSE: " + bestRmse);
        }
        firstLine.add("Max matching count: " + maxMatchingCount);
        firstLine.add("Max matchin RMSE: " + (maxMatchingCountRmse != null ? maxMatchingCountRmse: ""));
        g.drawString(firstLine.toString(), 10, 50);
        if (rmseResultAll != null) {
            g.drawString("RMSE using all: " + rmseResultAll, 10, 100);
        }
        g.drawString("Base All Count: " + bAllNumber + ", Align All Count: " + aAllNumber, 10, 150);
        Color alignColor = new Color(0, 255, 255);
        Color baseColor = new Color(255, 0, 0);
        if (bestRefs != null) {
            int i = 1;
            for (ControlPoint controlPoint : bestRefs) {
                g.setColor(baseColor);
                g.drawString(String.valueOf(i), (int) controlPoint.x1, (int) controlPoint.y1);
                g.setColor(alignColor);
                g.drawString(String.valueOf(i), (int) controlPoint.x2, (int) controlPoint.y2);
                i++;
            }
        }
        g.dispose();
        g.dispose();
        CachedImage.saveAndCache(new File(debCentrs, strings[0] + ".tif"), outputDebugImage);
    }
}
