package cz.jmare.image.align.selector;

import cz.jmare.image.centroid.Centroid;

import java.util.List;

public class NextSeqCentroSelector implements CentroSelector {
    int maxNumber = 6;

    int step = 3;

    public NextSeqCentroSelector(int maxNumber, int step) {
        this.maxNumber = maxNumber;
        this.step = step;
    }

    public NextSeqCentroSelector(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public NextSeqCentroSelector() {
    }

    @Override
    public int[][] getCentroIndexes(List<Centroid> centroids) {
        int number = centroids.size() / step;
        if (number > maxNumber) {
            number = maxNumber;
        }
        int[][] res = new int[number][];
        int index = 0;
        for (int i = 0; i < number; i++) {
            res[i] = new int[] {index, index + 1, index + 2};
            index += step;
        }
        return res;
    }
}
