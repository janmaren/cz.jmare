package cz.jmare.image.align.audit;

public class PointMatchAudit {
    public double expectedX;

    public double expectedY;

    public Double resultDistance;

    public PointMatchAudit(double expectedX, double expectedY, Double resultDistance) {
        this.expectedX = expectedX;
        this.expectedY = expectedY;
        this.resultDistance = resultDistance;
    }

    @Override
    public String toString() {
        return "PointMatchAudit{" +
                "expectedX=" + expectedX +
                ", expectedY=" + expectedY +
                ", resultDistance=" + resultDistance +
                '}';
    }
}
