package cz.jmare.image.align.selector;

import java.util.ArrayList;
import java.util.List;

public class CenSelectorUtil {
    public static boolean existsOther(List<int[]> triples, int[] triple, int besidesIndex) {
        for (int i = 0; i < triples.size(); i++) {
            if (i == besidesIndex) {
                continue;
            }
            int[] ints = triples.get(i);
            boolean allMatch = true;
            for (int j = 0; j < 3; j++) {
                if (findElementIndex(ints[j], triple) == -1) {
                    allMatch = false;
                    break;
                }
            }
            if (allMatch) {
                return true;
            }
        }
        return false;
    }

    public static int findElementIndex(int value, int[] theArray) {
        for (int i = 0; i < theArray.length; i++) {
            if (value == theArray[i]) {
                return i;
            }
        }
        return -1;
    }

    public static int[][] concatIndexesNoDupl(int[][] origIndexes, int[][] appendIndexes) {
        List<int[]> triplets = new ArrayList<>();
        for (int[] origIndex : origIndexes) {
            triplets.add(origIndex);
        }

        for (int[] appendIndex : appendIndexes) {
            if (!CenSelectorUtil.existsOther(triplets, appendIndex, -1)) {
                triplets.add(appendIndex);
            }
        }

        return triplets.toArray(new int[triplets.size()][]);
    }
}
