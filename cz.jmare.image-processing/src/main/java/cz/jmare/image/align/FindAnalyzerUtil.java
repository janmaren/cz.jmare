package cz.jmare.image.align;

import cz.jmare.image.MultiAligmentConfig;
import cz.jmare.image.centroid.CentroidsAnalyzer;
import cz.jmare.image.stat.HistogramStat;
import cz.jmare.image.util.CachedImage;

public class FindAnalyzerUtil {

    private final static float START_PERCENTIL = 0.9985f;

    private final static float END_PERCENTIL = 0.99f;
    public static final float STEP = 0.0005f;

    public static final int REQUIRED_NUMBER = 100;

    public static CentroidsAnalyzer findAnalyzer(CachedImage cachedImage, MultiAligmentConfig alignConfig) {
        float percentile = START_PERCENTIL;

        while (percentile >= END_PERCENTIL) {
            float minPixelBrightness = HistogramStat.calcThresholdByPercentil(cachedImage.getRasImageRead(), percentile);
            CentroidsAnalyzer analyzer = new CentroidsAnalyzer(cachedImage, minPixelBrightness, alignConfig.getMinPixelsNumber());
            if (analyzer.centroids().size() >= REQUIRED_NUMBER) {
                return analyzer;
            }
            percentile -= STEP;
        }
        return new CentroidsAnalyzer(cachedImage, percentile, alignConfig.getMinPixelsNumber());
    }
}
