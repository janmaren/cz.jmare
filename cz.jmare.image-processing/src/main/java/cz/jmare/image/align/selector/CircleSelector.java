package cz.jmare.image.align.selector;

import cz.jmare.image.align.MultiAlignUniEngine;
import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.util.ImPointInt;

import java.util.ArrayList;
import java.util.List;

public class CircleSelector implements CentroSelector {
    private int width;

    private int height;

    public CircleSelector(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int[][] getCentroIndexes(List<Centroid> centroids) {
        int cX = width / 2;
        int cY = height / 2;
        int radius = Math.min(cX, cY) - 50;
        if (radius < 50) {
            radius = Math.min(cX, cY) - 5;
        }

        List<int[]> triples = new ArrayList<>();
        for (int i = 0; i < 60; i += 10) {
            ImPointInt imPointInt0 = pointAtAngle(cX, cY, radius, i);
            MultiAlignUniEngine.NearResult nearestCentroid0 = MultiAlignUniEngine.findNearestCentroid(imPointInt0.x, imPointInt0.y, centroids, 10);
            if (nearestCentroid0 == null) {
                return new int[0][];
            }
            ImPointInt imPointInt1 = pointAtAngle(cX, cY, radius, i + 60);
            MultiAlignUniEngine.NearResult nearestCentroid1 = MultiAlignUniEngine.findNearestCentroid(imPointInt1.x, imPointInt1.y, centroids, 10);
            if (nearestCentroid1 == null) {
                return new int[0][];
            }
            if (nearestCentroid1.index == nearestCentroid0.index) {
                continue;
            }
            ImPointInt imPointInt2 = pointAtAngle(cX, cY, radius, i + 120);
            MultiAlignUniEngine.NearResult nearestCentroid2 = MultiAlignUniEngine.findNearestCentroid(imPointInt2.x, imPointInt2.y, centroids, 10);
            if (nearestCentroid2 == null) {
                return new int[0][];
            }
            if (nearestCentroid2.index == nearestCentroid0.index || nearestCentroid2.index == nearestCentroid1.index) {
                continue;
            }
            triples.add(new int[] {nearestCentroid0.index, nearestCentroid1.index, nearestCentroid2.index});
        }

        List<int[]> triples2 = new ArrayList<>();
        for (int i = 0; i < triples.size(); i++) {
            int[] triple = triples.get(i);
            if (!CenSelectorUtil.existsOther(triples, triple, i)) {
                triples2.add(triple);
            }
        }

        int[][] ints = triples2.toArray(new int[triples2.size()][]);

        return ints;
    }

    public static ImPointInt pointAtAngle(int cX, int cY, int radius, double angleDeg) {
        double angleRad = Math.toRadians(angleDeg);
        int x = cX + (int) (radius * Math.cos(angleRad));
        int y = cY - (int) (radius * Math.sin(angleRad));
        return new ImPointInt(x, y);
    }
}
