package cz.jmare.image.align.selector;

import cz.jmare.image.centroid.Centroid;

import java.util.List;

public interface CentroSelector {
    /**
     * Get indexes of centroids to be used for searching matching pairs
     * @param centroids centroids sorted by importance (like score) descending
     * @return
     */
    int[][] getCentroIndexes(List<Centroid> centroids);
}
