package cz.jmare.image.align;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import cz.jmare.image.centroid.Centroid;
import cz.jmare.image.filederive.FileDerivator;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class SingleAlignRunnable implements Runnable {
    private List<CachedImage> filesList;
    private RasImageRead baseImage;
    private Centroid baseCentroid;
    private FileDerivator fileDerivator;
    private File outputDir;
    private float minPixelBrightness;
    private Function<RasImageRead, RasImageRead> alignedImageCropper;
    private List<CachedImage> outputs = new ArrayList<CachedImage>();
    
    public SingleAlignRunnable(List<CachedImage> filesList, RasImageRead baseImage, Centroid baseCentroid,
            FileDerivator fileDerivator, File outputDir, float minPixelBrightness,
            Function<RasImageRead, RasImageRead> alignedImageCropper) {
        super();
        this.filesList = filesList;
        this.baseImage = baseImage;
        this.baseCentroid = baseCentroid;
        this.fileDerivator = fileDerivator;
        this.outputDir = outputDir;
        this.minPixelBrightness = minPixelBrightness;
        this.alignedImageCropper = alignedImageCropper;
    }
    
    @Override
    public void run() {
        for (int i = 0; i < filesList.size(); i++) {
            CachedImage inputFile = filesList.get(i);
            try {
                System.out.println("--- Aligning " + inputFile.getFile() + " ---");
                File outputFile = fileDerivator.derive(inputFile.getFile(), outputDir);
                
                RasImageRead transformed = SingleAlignEngine.alignByDelta(baseCentroid, baseImage.getWidth(), baseImage.getHeight(), inputFile, minPixelBrightness);
                RasImageRead corrected = alignedImageCropper.apply(transformed);
                outputs.add(CachedImage.saveAndCache(outputFile, corrected));
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error aligning " + filesList.get(i) + (e.getMessage() != null ? " - " + e.getMessage() : ""));
            }
        }
    }
    
    public List<CachedImage> getOutputs() {
        return outputs;
    }
}
