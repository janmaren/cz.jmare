package cz.jmare.image.align.validrmse;

public interface ValidRmse {
    boolean isValidRmse(RmseResult rms);
}
