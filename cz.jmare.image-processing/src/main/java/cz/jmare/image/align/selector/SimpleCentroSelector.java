package cz.jmare.image.align.selector;

import cz.jmare.image.centroid.Centroid;

import java.util.List;

public class SimpleCentroSelector implements CentroSelector {
    int maxNumber = 6;

    public SimpleCentroSelector(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public SimpleCentroSelector() {
    }

    @Override
    public int[][] getCentroIndexes(List<Centroid> centroids) {
        int number = centroids.size() - 2;
        if (number > maxNumber) {
            number = maxNumber;
        }
        int[][] res = new int[number][];
        for (int i = 0; i < number; i++) {
            res[i] = new int[] {i, i + 1, i + 2};
        }
        return res;
    }
}
