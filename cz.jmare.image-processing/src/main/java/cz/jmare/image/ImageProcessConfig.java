package cz.jmare.image;

import java.io.File;
import java.util.List;

import cz.jmare.image.postprocess.PixelTransformer;
import cz.jmare.image.util.CachedImage;
import cz.jmare.image.util.ImFilesUtil;

public class ImageProcessConfig {
    private boolean align;

    private boolean combine;

    private boolean stack;

    private boolean animate;

    private boolean crop;

    private boolean post;

    private CachedImage baseFile;

    private List<CachedImage> darkFiles;

    private List<CachedImage> flatFiles;

    private List<CachedImage> lightFiles;

    private File rootOutputDir;

    private boolean debug;
    private AligmentType aligmentType;
    private SingleAligmentConfig singleAligmentConfig = new SingleAligmentConfig();
    private MultiAligmentConfig multiAligmentConfig = new MultiAligmentConfig();
    private StackingConfig stackingConfig = new StackingConfig();
    private AnimCreationConfig animCreationConfig = new AnimCreationConfig();
    private CropConfig cropConfig;
    private List<PixelTransformer> postTransformers = List.of();

    private float[][] darkSmoothWeights = defaultWeights(21);

    private float[][] flatSmoothWeights = defaultWeights(21);

    /**
     * When not null rotate master dark image
     */
    private RotationCounterClockwise darkRotation;

    /**
     * When not null rotate master flat image
     */
    private RotationCounterClockwise flatRotation;

    private boolean removeHotPixels = true;

    private boolean subtrackDarks = true;

    private boolean noHotPixelsMatchFail;

    /**
     * How much brightness (typically (R+G+B) / 3) the pixel must have to be a hotpixel. Range 0..1.
     * When null then automatically set a value
     */
    private float minHotPixelsBrightness = 0.05f;

    /**
     * Dimension of matrix to use for interpolation
     */
    private Integer hotpixelInterpolationDimension = null;

    public ImageProcessConfig() {

    }

    public ImageProcessConfig align(boolean align) {
        this.align = align;
        return this;
    }

    public ImageProcessConfig align(AligmentType aligmentType) {
        this.align = true;
        this.aligmentType = aligmentType;
        return this;
    }
    
    public ImageProcessConfig align(SingleAligmentConfig singleAligmentConfig) {
        this.align = true;
        this.aligmentType = AligmentType.SINGLE;
        this.singleAligmentConfig = singleAligmentConfig;
        return this;
    }
    
    public ImageProcessConfig align(MultiAligmentConfig multiAligmentConfig) {
        this.align = true;
        this.aligmentType = AligmentType.MULTI;
        this.multiAligmentConfig = multiAligmentConfig;
        return this;
    }
    
    public ImageProcessConfig crop(CropConfig cropConfig) {
        this.crop = true;
        this.cropConfig = cropConfig;
        return this;
    }
    
    public ImageProcessConfig stack(StackingConfig stackingConfig) {
        this.stack = true;
        this.stackingConfig = stackingConfig;
        return this;
    }
    
    public ImageProcessConfig stack(boolean stack) {
        this.stack = stack;
        return this;
    }

    public ImageProcessConfig animate(boolean animate) {
        this.animate = animate;
        return this;
    }
    
    public ImageProcessConfig animate(AnimCreationConfig animCreationConfig) {
        this.animate = true;
        this.animCreationConfig = animCreationConfig;
        return this;
    }
    
    public ImageProcessConfig postprocess(boolean postprocess) {
        this.post = postprocess;
        return this;
    }
    
    public ImageProcessConfig postTransformers(List<PixelTransformer> postTransformers) {
        this.post = true;
        this.postTransformers = postTransformers;
        return this;
    }
    
    public ImageProcessConfig debug(boolean debug) {
        this.debug = debug;
        return this;
    }
    

    public List<CachedImage> getLightFiles() {
        return lightFiles;
    }

    public ImageProcessConfig lightFiles(List<CachedImage> lightFiles) {
        this.lightFiles = lightFiles;
        return this;
    }

    public CachedImage getBaseFile() {
        return baseFile;
    }

    public ImageProcessConfig baseFile(CachedImage baseFile) {
        this.baseFile = baseFile;
        return this;
    }
    

    public List<PixelTransformer> getPostTransformers() {
        return postTransformers;
    }

    public AligmentType getAligmentType() {
        return aligmentType;
    }

    public boolean isAligment() {
        return align;
    }
    public boolean isStack() {
        return stack;
    }
    
    public boolean isPostprocess() {
        return post;
    }

    public boolean isDebug() {
        return debug;
    }

    public MultiAligmentConfig getMultiAligmentConfig() {
        return multiAligmentConfig;
    }

    public StackingConfig getStackingConfig() {
        return stackingConfig;
    }

    public SingleAligmentConfig getSingleAligmentConfig() {
        return singleAligmentConfig;
    }

    public AnimCreationConfig getAnimCreationConfig() {
        return animCreationConfig;
    }

    public CropConfig getCropConfig() {
        return cropConfig;
    }

    public boolean isAnimate() {
        return animate;
    }

    public boolean isCrop() {
        return crop;
    }

    public List<CachedImage> getDarkFiles() {
        return darkFiles;
    }

    public void darkDirectory(File dir) {
        if (dir == null) {
            darkFiles = null;
            return;
        }
        darkFiles = ImFilesUtil.filesListCached(dir);
    }

    public ImageProcessConfig darkFiles(List<CachedImage> darkFiles) {
        this.darkFiles = darkFiles;
        return this;
    }

    public float[][] getDarkSmoothWeights() {
        return darkSmoothWeights;
    }

    public ImageProcessConfig darkSmoothWeights(float[][] darkSmoothWeights) {
        this.darkSmoothWeights = darkSmoothWeights;
        return this;
    }

    public boolean isCombine() {
        return combine;
    }

    public ImageProcessConfig combine(boolean combine) {
        this.combine = combine;
        return this;
    }

    public List<CachedImage> getFlatFiles() {
        return flatFiles;
    }

    public void flatDirectory(File dir) {
        if (dir == null) {
            flatFiles = null;
            return;
        }
        flatFiles = ImFilesUtil.filesListCached(dir);
    }

    public ImageProcessConfig flatFiles(List<CachedImage> flatFiles) {
        if (flatFiles == null) {
            this.flatFiles = null;
            return null;
        }
        this.flatFiles = flatFiles;
        return this;
    }

    public ImageProcessConfig flatSmoothWeights(float[][] flatSmoothWeights) {
        this.flatSmoothWeights = flatSmoothWeights;
        return this;
    }

    public File getRootOutputDir() {
        if (rootOutputDir != null) {
            return rootOutputDir;
        }
        if (lightFiles == null || lightFiles.isEmpty()) {
            return null;
        }
        return lightFiles.get(0).getFile().getParentFile();
    }

    public float[][] getFlatSmoothWeights() {
        return flatSmoothWeights;
    }

    public RotationCounterClockwise getDarkRotation() {
        return darkRotation;
    }

    public ImageProcessConfig darkRotation(RotationCounterClockwise darkRotation) {
        this.darkRotation = darkRotation;
        return this;
    }

    public RotationCounterClockwise getFlatRotation() {
        return flatRotation;
    }

    public ImageProcessConfig flatRotation(RotationCounterClockwise flatRotation) {
        this.flatRotation = flatRotation;
        return this;
    }

    private final static float[][] defaultWeights(int dim) {
        float[][] matrix = new float[21][];
        for (int i = 0; i < dim; i++) {
            matrix[i] = new float[dim];
            for (int j = 0; j < dim; j++) {
                matrix[i][j] = 1;
            }
        }
        return matrix;
    }

    public boolean isRemoveHotPixels() {
        return removeHotPixels;
    }

    public ImageProcessConfig removeHotPixels(boolean removeHotPixels) {
        this.removeHotPixels = removeHotPixels;
        return this;
    }

    public Float getMinHotPixelsBrightness() {
        return minHotPixelsBrightness;
    }

    public ImageProcessConfig minHotPixelsBrightness(float minHotPixelsBrightness) {
        this.minHotPixelsBrightness = minHotPixelsBrightness;
        return this;
    }

    public boolean isNoHotPixelsMatchFail() {
        return noHotPixelsMatchFail;
    }

    public ImageProcessConfig noHotPixelsMatchFail(boolean noHotPixelsMatchFail) {
        this.noHotPixelsMatchFail = noHotPixelsMatchFail;
        return this;
    }

    public boolean isSubtrackDarks() {
        return subtrackDarks;
    }

    public ImageProcessConfig subtrackDarks(boolean subtrackDarks) {
        this.subtrackDarks = subtrackDarks;
        return this;
    }

    public Integer getHotpixelInterpolationDimension() {
        return hotpixelInterpolationDimension;
    }

    public ImageProcessConfig hotpixeInterpolationDimension(Integer hotpixeInterpolationDimension) {
        this.hotpixelInterpolationDimension = hotpixeInterpolationDimension;
        return this;
    }

    public ImageProcessConfig rootOutputDir(File rootOutputDir) {
        this.rootOutputDir = rootOutputDir;
        return this;
    }
}
