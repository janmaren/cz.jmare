package cz.jmare.image.oper;

import java.io.File;

import cz.jmare.image.exception.UnableSubtrackException;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class SubtrackEngine {
    public static CachedImage subtrack(File inputImage, CachedImage subtractImage, File outputFile) throws UnableSubtrackException {
        return subtrack(new CachedImage(inputImage), subtractImage, outputFile);
    }

    public static CachedImage subtrack(CachedImage inputImage, CachedImage subtractImage, File outputFile) throws UnableSubtrackException {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        RasImageRead subtractRead = subtractImage.getRasImageRead();
        RasImageFloat output = new RasImageFloat(rasImageRead.getWidth(), rasImageRead.getHeight());
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        if (subtractRead.getWidth() != width || subtractRead.getHeight() != height) {
            throw new UnableSubtrackException("The subtracked image has different dimensions " + subtractRead.getWidth() + "x" + subtractRead.getHeight() + " vs expected " + width + "x" + height);
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(j, i);
                RGBFloat subtractFloat = subtractRead.getRGBFloat(j, i);
                float red = rgbFloat.red - subtractFloat.red;
                if (red < 0) {
                    red = 0;
                }
                float green = rgbFloat.green - subtractFloat.green;
                if (green < 0) {
                    green = 0;
                }
                float blue = rgbFloat.blue - subtractFloat.blue;
                if (blue < 0) {
                    blue = 0;
                }
                RGBFloat outRGB = new RGBFloat(red, green, blue);
                output.setRGBFloat(j, i, outRGB);
            }
        }

        CachedImage outputImage = CachedImage.saveAndCache(outputFile, output);
        return outputImage;
    }
}
