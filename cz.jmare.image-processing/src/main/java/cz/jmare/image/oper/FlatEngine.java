package cz.jmare.image.oper;

import java.io.File;

import cz.jmare.image.exception.UnableFlatException;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

public class FlatEngine {
    public static CachedImage flat(CachedImage inputImage, CachedImage flatImage, File outputFile) throws UnableFlatException {
        return flat(inputImage, flatImage, outputFile, false);
    }

    public static CachedImage flat(CachedImage inputImage, CachedImage subtractImage, File outputFile, boolean useAvg) throws UnableFlatException {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        RasImageRead subtractRead = subtractImage.getRasImageRead();
        RasImageFloat output = new RasImageFloat(rasImageRead.getWidth(), rasImageRead.getHeight());
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        if (subtractRead.getWidth() != width || subtractRead.getHeight() != height) {
            throw new UnableFlatException("The flat image has different dimensions " + subtractRead.getWidth() + "x" + subtractRead.getHeight() + " vs expected " + width + "x" + height);
        }
        float max = 0;
        float min = 1;
        float avgSum = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = subtractRead.getRGBFloat(j, i);
                float sum = brightness(rgbFloat);
                if (sum > max) {
                    max = sum;
                }
                if (sum < min) {
                    min = sum;
                }
                avgSum += sum;
            }
        }
        float avg = avgSum / (height * width);
        float level = useAvg ? avg : max;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(j, i);
                RGBFloat flatFloat = subtractRead.getRGBFloat(j, i);
                float flatBrightness = brightness(flatFloat);
                float factor = level / flatBrightness;
                float red = rgbFloat.red * factor;
                if (red > 1) {
                    red = 1;
                }
                float green = rgbFloat.green * factor;
                if (green > 1) {
                    green = 1;
                }
                float blue = rgbFloat.blue * factor;
                if (blue > 1) {
                    blue = 1;
                }
                RGBFloat outRGB = new RGBFloat(red, green, blue);
                output.setRGBFloat(j, i, outRGB);
            }
        }

        CachedImage outputImage = CachedImage.saveAndCache(outputFile, output);
        return outputImage;
    }

    private static float brightness(RGBFloat rgbFloat) {
        return (rgbFloat.red + rgbFloat.green + rgbFloat.blue) / 3f;
    }
}
