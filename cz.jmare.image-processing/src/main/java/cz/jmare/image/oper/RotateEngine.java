package cz.jmare.image.oper;

import cz.jmare.image.RotationCounterClockwise;
import cz.jmare.image.ras.RGBFloat;
import cz.jmare.image.ras.RasImageFloat;
import cz.jmare.image.ras.RasImageRead;
import cz.jmare.image.util.CachedImage;

import java.io.File;

public class RotateEngine {
    public static CachedImage flat(File image, RotationCounterClockwise degreesCounterClockwise, File outputFile) {
        return rotate(new CachedImage(image), degreesCounterClockwise, outputFile);
    }

    public static CachedImage rotate(CachedImage inputImage, RotationCounterClockwise degreesCounterClockwise, File outputFile) {
        if (degreesCounterClockwise == RotationCounterClockwise.ROT_270) {
            return rotate90(inputImage, outputFile);
        } else if (degreesCounterClockwise == RotationCounterClockwise.ROT_180) {
            return rotate180(inputImage, outputFile);
        } else if (degreesCounterClockwise == RotationCounterClockwise.ROT_90) {
            return rotate270(inputImage, outputFile);
        } else throw new IllegalArgumentException("Rotation by " + degreesCounterClockwise + " degrees is not supported, only 90, 180 and 270 degrees");
    }


    private static CachedImage rotate90(CachedImage inputImage, File outputFile) {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        int heightM1 = height - 1;
        RasImageFloat output = new RasImageFloat(height, width);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(j, i);
                output.setRGBFloat(heightM1 - i, j, rgbFloat);
            }
        }

        return CachedImage.saveAndCache(outputFile, output);
    }

    private static CachedImage rotate180(CachedImage inputImage, File outputFile) {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        int heightM1 = height - 1;
        int widthM1 = width - 1;
        RasImageFloat output = new RasImageFloat(width, height);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(j, i);
                output.setRGBFloat(widthM1 - j, heightM1 - i, rgbFloat);
            }
        }

        return CachedImage.saveAndCache(outputFile, output);
    }


    private static CachedImage rotate270(CachedImage inputImage, File outputFile) {
        RasImageRead rasImageRead = inputImage.getRasImageRead();
        int height = rasImageRead.getHeight();
        int width = rasImageRead.getWidth();
        int widthM1 = width - 1;
        RasImageFloat output = new RasImageFloat(height, width);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                RGBFloat rgbFloat = rasImageRead.getRGBFloat(j, i);
                output.setRGBFloat(i, widthM1 - j, rgbFloat);
            }
        }

        return CachedImage.saveAndCache(outputFile, output);
    }
}
