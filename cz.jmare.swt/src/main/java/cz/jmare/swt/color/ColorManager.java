package cz.jmare.swt.color;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;

public class ColorManager {
    private static ColorManager colorManager;

    private ColorManager() {
        super();
    }

    @Deprecated
    public void init(Device device) {
    }

    /**
     * Use {@link ColorUtil#getColorByRGB(int, int, int)}
     */
    @Deprecated
    public static Color getColor(int red, int green, int blue) {
        return ColorUtil.getColorByRGB(red, green, blue);
    }

    public static Color getColor(String str) {
        if (!str.startsWith("#")) {
            str = "#" + str;
        }
        return ColorUtil.getColorByRGB(hex2decimal(str.substring(1, 3)), hex2decimal(str.substring(3, 5)), hex2decimal(str.substring(5, 7)));
    }

    public static Color getValidColor(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Color not defined");
        }
        if (!str.startsWith("#")) {
            throw new IllegalArgumentException("Color doesn't start with #");
        }
        if (str.length() != 7) {
            throw new IllegalArgumentException("Color definition must have 7 characters, as #RRGGBB");
        }
        return ColorUtil.getColorByRGB(hex2decimal(str.substring(1, 3)), hex2decimal(str.substring(3, 5)), hex2decimal(str.substring(5, 7)));
    }

    @Deprecated
    public void disposeAll() {
    }

    public static synchronized ColorManager getInstance() {
        if (colorManager == null) {
            colorManager = new ColorManager();
        }
        return colorManager;
    }

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            if (d == -1) {
                throw new IllegalArgumentException("Only hex characters 0-9 and A-F allowed");
            }
            val = 16*val + d;
        }
        return val;
    }
}
