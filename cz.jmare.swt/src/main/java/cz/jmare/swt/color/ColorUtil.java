package cz.jmare.swt.color;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

public class ColorUtil {
    public static ColorRegistry colorReg;

    static {
        try {
            colorReg = JFaceResources.getColorRegistry();
        } catch (Exception e) {
            System.err.println("ColorUtil probably not called from main thread for the first time");
            e.printStackTrace();
        }
    }

    public static void init() {
        if (colorReg == null) {
            throw new IllegalStateException("ColorUtil not initialized. Have you called this init routine first and from main thread?");
        }
    }

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    /**
     * Get color by symbolic name. Supported also name like HTML color for example '#112233' or '#123'
     * @param symbolicName
     * @return
     */
    public static Color getColor(String symbolicName) {
        if (colorReg == null) colorReg = JFaceResources.getColorRegistry();
        Color color = colorReg.get(symbolicName);
        if (color != null) {
            return color;
        }
        if (!symbolicName.startsWith("#")) {
            throw new IllegalArgumentException("Not valid symbolicName " + symbolicName);
        }
        RGB rgb;
        if (symbolicName.length() == 4) {
            rgb = new RGB(hex2decimal(symbolicName.substring(1, 2) + symbolicName.substring(1, 2)), 
                    hex2decimal(symbolicName.substring(2, 3) + symbolicName.substring(2, 3)), 
                    hex2decimal(symbolicName.substring(3, 4) + symbolicName.substring(3, 4)));
        } else {
            rgb = new RGB(hex2decimal(symbolicName.substring(1, 3)), hex2decimal(symbolicName.substring(3, 5)), hex2decimal(symbolicName.substring(5, 7)));
        }
        colorReg.put(symbolicName, rgb);
        return colorReg.get(symbolicName);
    }

    public static Color getColor(RGB rgb) {
        if (colorReg == null) colorReg = JFaceResources.getColorRegistry();
        String symbolicName = "#" + numberToHex(rgb.red) + numberToHex(rgb.green) + numberToHex(rgb.blue);
        colorReg.put(symbolicName, rgb);
        return colorReg.get(symbolicName);
    }

    public static String numberToHex(int number) {
        String str = number < 16 ? "0" : "";
        return str + Integer.toHexString(number);
    }

    public static Color getColorByRGB(int rLowGB) {
        if (colorReg == null) colorReg = JFaceResources.getColorRegistry();
        Color color = colorReg.get("jmare-rgb-" + rLowGB);
        if (color != null) {
            return color;
        }
        int red = rLowGB & 0xFF;
        int green = (rLowGB >> 8) & 0xff;
        int blue = (rLowGB >> 16) & 0xff;
        RGB rgbIns = new RGB(red, green, blue);
        colorReg.put("jmare-rgb-" + rLowGB, rgbIns);
        return colorReg.get("jmare-rgb-" + rLowGB);
    }

    public static Color getColorByRGB(int red, int green, int blue) {
        int color = red;
        color |= green << 8;
        color |= blue << 16;
        return getColorByRGB(color);
    }
}
