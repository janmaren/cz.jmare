package cz.jmare.swt.table;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class TableEditing<T> {
    private Table table;
    private BiConsumer<T, String>[] setters;
    private BiConsumer<TableItem, Boolean> objChangedConsumer;
    /**
     * Consume row index and column index
     */
    private BiConsumer<Integer, Integer> objColumnConsumer;
    private Consumer<String> errorMessageConsumer;
    private Color errorTextColor;
    
    @SafeVarargs
    public TableEditing(Table table, BiConsumer<T, String>... setters) {
        super();
        this.table = table;
        this.setters = setters;
        
        FontData[] fontData = table.getFont().getFontData();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("error-text-font", new FontData[]{new FontData("error-text-font", fontData[0].getHeight(), SWT.BOLD)});
        errorTextColor = Display.getCurrent().getSystemColor(SWT.COLOR_RED);
    }

    public void applyEditor() {
        final TableEditor editor = new TableEditor(table);
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        table.addListener(SWT.MouseDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                if (event.button != 1) return;
                Rectangle clientArea = table.getClientArea();
                org.eclipse.swt.graphics.Point pt = new org.eclipse.swt.graphics.Point(event.x, event.y);
                int index = table.getTopIndex();
                while (index < table.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = table.getItem(index);
                    for (int i = 0; i < table.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            BiConsumer<T, String> settingAction = setters[i];
                            if (settingAction == null) {
                                break;
                            }
                            final int finalIndex = index;
                            final int column = i;
                            final Text text = new Text(table, SWT.NONE);
                            Listener textListener = new Listener() {
                                @Override
                                public void handleEvent(final Event e) {
                                    switch (e.type) {
                                    case SWT.FocusOut:
                                        controlToColumnValue(finalIndex, column, text);
                                        evaluate(finalIndex, column);
                                        break;
                                    case SWT.Traverse:
                                        switch (e.detail) {
                                        case SWT.TRAVERSE_RETURN:
                                            controlToColumnValue(finalIndex, column, text);
                                            evaluate(finalIndex, column);
                                        case SWT.TRAVERSE_ESCAPE:
                                            text.dispose();
                                            e.doit = false;
                                        }
                                        break;
                                    }
                                }

                            };
                            text.addListener(SWT.FocusOut, textListener);
                            text.addListener(SWT.Traverse, textListener);
                            editor.setEditor(text, item, i);
                            text.setText(item.getText(i));
                            text.selectAll();
                            text.setFocus();
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible)
                        return;
                    index++;
                }
            }
        });
    }
    
    public void evaluate(int row, int column) {
        TableItem tableItem = table.getItem(row);
        @SuppressWarnings("unchecked")
        T obj = (T) tableItem.getData();
        boolean error = false;
        for (int i = 0; i < setters.length; i++) {
            BiConsumer<T, String> setter = setters[i];
            if (setter == null) {
                continue;
            }
            String columnString = tableItem.getText(i);
            try {
                setter.accept(obj, columnString.isEmpty() ? null : columnString);
                setText(table, row, i, columnString, false);
            } catch (Exception e) {
                error = true;
                setText(table, row, i, columnString, true);
                if (errorMessageConsumer != null && i == column) {
                    errorMessageConsumer.accept(e.getMessage());
                }
            }
        }
        
        if (objChangedConsumer != null) {
            objChangedConsumer.accept(tableItem, !error);
        }
        
        if (objColumnConsumer != null) {
            objColumnConsumer.accept(row, column);
        }
    }

    public void controlToColumnValue(int row, int column, Control control) {
        String string;
        if (control instanceof Text text) {
            string = text.getText();
        } else {
            throw new IllegalStateException("Unsupported control " + control.getClass());
        }
        
        TableItem item = table.getItem(row);
        item.setText(column, string);
        
        control.dispose();
    }
    
    private void setText(Table table, int row, int column, String str, boolean error) {
        TableItem item = table.getItem(row);
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        if (!error) {
            item.setForeground(column, null);
            item.setText(column, str);
            item.setFont(column, null);
        } else {
            item.setForeground(column, errorTextColor);
            item.setText(column, str);
            item.setFont(column, fontRegistry.get("error-text-font"));
        }
    }

    public void setObjChangedConsumer(BiConsumer<TableItem, Boolean> objChangedConsumer) {
        this.objChangedConsumer = objChangedConsumer;
    }

    public void setErrorMessageConsumer(Consumer<String> errorMessageConsumer) {
        this.errorMessageConsumer = errorMessageConsumer;
    }

    public void setObjColumnConsumer(BiConsumer<Integer, Integer> objColumnConsumer) {
        this.objColumnConsumer = objColumnConsumer;
    }

    /**
     * Error color or null when not wishing to have a color (using default)
     */
    public void setErrorTextColor(Color errorTextColor) {
        this.errorTextColor = errorTextColor;
    }

    public Color getErrorTextColor() {
        return errorTextColor;
    }
}
