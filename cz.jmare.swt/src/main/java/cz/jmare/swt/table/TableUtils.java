package cz.jmare.swt.table;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class TableUtils {
    public static void traverseTable(Table table, BiConsumer<TableItem, Integer> consumer) {
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            consumer.accept(item, i);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static <T> List<T> getAllObjs(Table table, Class<T> clazz) {
        List<T> result = new ArrayList<>();
        
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            result.add((T) item.getData());
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> List<T> getEnabledObjs(Table table, Class<T> clazz) {
        List<T> result = new ArrayList<>();
        
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem item = table.getItem(i);
            if (item.getChecked()) {
                result.add((T) item.getData());
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T getObj(Table table, int row, Class<T> clazz) {
        return (T) table.getItem(row).getData();
    }
}
