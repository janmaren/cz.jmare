package cz.jmare.swt.table;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class TablePopulator {
    public static <T> void repopulate(Table table, List<T> objects, Function<T, Object[]> remap) {
        boolean hasCheckColumn = (table.getStyle() & SWT.CHECK) != 0;
        repopulate(table, objects, remap, hasCheckColumn ? 1 : 0);
    }
    
    /**
     * Populate table with data. For new data by default created items with check enabled.
     * @param table table where to show data
     * @param objects model objects by which to populate table
     * @param remap for object return columns data (Strings)
     * @param firstColumnValueId which is the first value index to be populated. 0 or 1 when first column is an technical column for checkbox etc.
     */
    @SuppressWarnings("unchecked")
    public static <T> void repopulate(Table table, List<T> objects, Function<T, Object[]> remap, int firstColumnValueId) {
        // remember selection and enablement
        List<T> selections = new ArrayList<>();
        List<T> disablements = new ArrayList<>();
        TableItem[] origItems = table.getItems();
        for (int i = 0; i < origItems.length; i++) {
            TableItem tableItem = origItems[i];
            if (table.isSelected(i)) {
                selections.add((T) tableItem.getData());
            }
            if (!tableItem.getChecked()) {
                disablements.add((T) tableItem.getData());
            }
        }
        
        table.setSelection(new int[] {});
        
        // prepare right number
        int itemCount = table.getItemCount();
        if (itemCount < objects.size()) {
            int count = objects.size() - itemCount;
            for (int i = 0; i < count; i++) {
                new TableItem(table, SWT.NONE);
            }
        }
        while (table.getItemCount() > objects.size()) {
            table.remove(table.getItemCount() - 1);
        }
        
        // populate
        TableItem[] items = table.getItems();
        for (int i = 0; i < objects.size(); i++) {
            T obj = objects.get(i);
            TableItem item = items[i];
            Object[] columnsValues = remap.apply(obj);
            if (columnsValues.length + firstColumnValueId != table.getColumnCount()) {
                throw new IllegalStateException("Wrong column size " + columnsValues.length + " vs " + table.getColumnCount());
            }
            for (int j = 0; j < columnsValues.length; j++) {
                Object object = columnsValues[j];
                String value = "";
                if (object != null) {
                    if (object instanceof String) {
                        value = (String) object;
                    } else {
                        value = String.valueOf(object);
                    }
                }
                item.setText(firstColumnValueId + j, value);
                item.setData(obj);
            }
        }
        
        // select and enable
        TableItem[] itemsNow = table.getItems();
        for (int i = 0; i < itemsNow.length; i++) {
            TableItem tableItem = itemsNow[i];
            if (selections.contains((T) tableItem.getData())) {
                table.select(i);
            }
            tableItem.setChecked(!disablements.contains((T) tableItem.getData()));
        }
    }
}
