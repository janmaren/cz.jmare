package cz.jmare.swt.image;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.ImageData;

import cz.jmare.file.PathUtil;

public class SwtImageFileUtil {
    public static int getSwtImageType(String filename) {
        String[] strings = PathUtil.parseFullPathSuffix(filename);
        String extension = strings[1].toLowerCase();
        return switch (extension) {
            case ".jpg", ".jpeg" -> SWT.IMAGE_JPEG;
            case ".bmp" -> SWT.IMAGE_BMP;
            case ".ico" -> SWT.IMAGE_ICO;
            case ".png" -> SWT.IMAGE_PNG;
            default -> throw new IllegalStateException("Unexpected value: " + extension);
        };
    }
    
    public static ImageData loadImageData(File filename) throws IOException {
        return new ImageData(filename.toString());
    }
}
