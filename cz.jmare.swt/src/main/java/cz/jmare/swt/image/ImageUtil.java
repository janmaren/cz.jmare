package cz.jmare.swt.image;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;

import cz.jmare.file.PathUtil;

public class ImageUtil {
    public final static Map<String, Integer> SUFFIX_TO_SWT_IMAGE_TYPE = Map.of(
            ".png", SWT.IMAGE_PNG, ".tiff", SWT.IMAGE_TIFF, ".bmp", SWT.IMAGE_BMP,
            ".jpg", SWT.IMAGE_JPEG, ".jpeg", SWT.IMAGE_JPEG);

    public static ImageRegistry imgReg = JFaceResources.getImageRegistry();

    public static Image get(String key) {
        Image image = imgReg.get(key);
        if (image == null) {
            System.err.println("Not foud image under key " + key);
            throw new RuntimeException("Not foud image under key " + key);
        }
        return image;
    }

    public static void put(String key, String path) {
        Image image = imgReg.get(key);
        if (image == null) {
            ImageDescriptor createFromURL;
            URL url = null;
            try {
                url = getURL(path);
                createFromURL = ImageDescriptor.createFromURL(url);
                imgReg.put(key, createFromURL);
            } catch (Exception e) {
                System.err.println("Unable to find " + url);
                throw new RuntimeException("Unable to find " + url, e);
            }
        }
    }

    public static void put(String key, String path, int width, int height) {
        Image image = imgReg.get(key);
        if (image == null) {
            ImageDescriptor imageDescriptor;
            URL url = null;
            try {
                url = getURL(path);
                imageDescriptor = ImageDescriptor.createFromURL(url);
                Image origImage = imageDescriptor.createImage();
                image = createResized(origImage.getImageData(), width, height);
                origImage.dispose();
                imgReg.put(key, image);
            } catch (Exception e) {
                System.err.println("Unable to find " + url);
                throw new RuntimeException("Unable to find " + url, e);
            }
        }
    }

    public static URL getURL(String resource) {
        URL url = ImageUtil.class.getResource(resource);
        if (url == null) {
            try {
                url = new URI("file:" + resource).toURL();
            } catch (MalformedURLException | URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
        return url;
    }

//    private static Image resize(Image image, int width, int height) {
//        Image scaled = new Image(Display.getCurrent(), width, height);
//        GC gc = new GC(scaled);
//        gc.setAntialias(SWT.ON);
//        gc.setInterpolation(SWT.HIGH);
//        gc.drawImage(image, 0, 0, image.getBounds().width, image.getBounds().height, 0, 0, width, height);
//        gc.dispose();
//        image.dispose();
//        return scaled;
//    }

    public static Image createResized(ImageData data, int width, int height) {
        float img_height = data.height;
        float img_width = data.width;
        float container_height = height;
        float container_width = width;

        float dest_height_f = container_height;
        float factor = img_height / dest_height_f;

        int dest_width = (int) Math.floor(img_width / factor);
        int dest_height = (int) dest_height_f;

        if (dest_width > container_width) {
            dest_width = (int) container_width;
            factor = img_width / dest_width;
            dest_height = (int) Math.floor(img_height / factor);
        }

        data = data.scaledTo(dest_width, dest_height);
        Image scaled = new Image(Display.getCurrent(), data);
        return scaled;
    }

    public static Image createResizedImage(Image image, int width, int height) {
        return createResizedImage(image, width, height, false, false);
    }

    public static Image createResizedImage(Image image, int width, int height, boolean interpolate, boolean antialias) {
        Image scaled = new Image(Display.getCurrent(), width, height);
        GC gc = new GC(scaled);
        gc.setAntialias(antialias ? SWT.ON : SWT.NONE);
        gc.setInterpolation(interpolate ? SWT.HIGH : SWT.NONE);
        gc.drawImage(image, 0, 0, image.getBounds().width, image.getBounds().height, 0, 0, width, height);
        gc.dispose();
        return scaled;
    }

    public static void saveImageData(ImageData imageData, String fileName) {
        String suffix = PathUtil.getSuffixOrEmpty(fileName);
        Integer integer = SUFFIX_TO_SWT_IMAGE_TYPE.get(suffix);
        if (integer == null) {
            throw new IllegalArgumentException("Not supported suffix " + suffix);
        }
        ImageLoader saver = new ImageLoader();
        saver.data = new ImageData[] { imageData };
        saver.save(fileName, integer);
    }

    public static Image getSystemImage16(int style) {
        String key = null;
        String path = null;
        if (style == SWT.ICON_ERROR) {
            key = "icon_error_16";
            path = "/image/dialog-error.png";
        } else if (style == SWT.ICON_WARNING) {
            key = "icon_warn_16";
            path = "/image/dialog-warning.png";
        } else if (style == SWT.ICON_INFORMATION) {
            key = "icon_info_16";
            path = "/image/dialog-information.png";
        } else {
            throw new IllegalArgumentException("Style " + style + " not supported");
        }
        Image image = imgReg.get(key);
        if (image == null) {
            put(key, path);
        }
        return imgReg.get(key);
    }
}
