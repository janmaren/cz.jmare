package cz.jmare.swt.image;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public class ImageStore {
    List<Image> images = new ArrayList<Image>();

    public Image registerImage(String path) {
        try {
            URL url = getURL(path);
            ImageDescriptor createFromURL = ImageDescriptor.createFromURL(url);
            Image image = createFromURL.createImage();
            images.add(image);
            return image;
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to register image " + path, e);
        }
    }

    public void dispose() {
        for (Image image : images) {
            image.dispose();
        }
    }

    public static URL getURL(String resource) {
        URL url = ImageUtil.class.getResource(resource);
        if (url == null) {
            try {
                url = new URI("file:" + resource).toURL();
            } catch (MalformedURLException | URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
        return url;
    }
}
