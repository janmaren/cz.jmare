package cz.jmare.swt.image;

import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.util.function.Function;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

public class AwtSwtUtil {
    public static void revertRows(ImageData imageData) {
        int bytesPerLine = imageData.bytesPerLine;
        byte[] data = imageData.data;
        int height = imageData.height;
        byte[] buffer = new byte[bytesPerLine];
        int upto = height / 2;
        for (int i = 0; i < upto; i++) {
            System.arraycopy(data, i * bytesPerLine, buffer, 0, bytesPerLine);
            System.arraycopy(data, (height - i - 1) * bytesPerLine, data, i * bytesPerLine, bytesPerLine);
            System.arraycopy(buffer, 0, data, (height - i - 1) * bytesPerLine, bytesPerLine);
        }
    }

    public static ImageData convertToSWT(BufferedImage bufferedImage) {
        if (bufferedImage.getColorModel() instanceof DirectColorModel) {
            DirectColorModel colorModel = (DirectColorModel)bufferedImage.getColorModel();
            PaletteData palette = new PaletteData(0xFF0000, 0xFF00, 0xFF);
            ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
                    colorModel.getPixelSize(), palette);
            for (int y = 0; y < data.height; y++) {
                for (int x = 0; x < data.width; x++) {
                    int rgb = bufferedImage.getRGB(x, y);
                    int pixel = palette.getPixel(new RGB((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF));
                    data.setPixel(x, y, pixel);
                    if (colorModel.hasAlpha()) {
                        data.setAlpha(x, y, (rgb >> 24) & 0xFF);
                    }
                }
            }
            return data;
        }
        else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
            IndexColorModel colorModel = (IndexColorModel)bufferedImage.getColorModel();
            int size = colorModel.getMapSize();
            byte[] reds = new byte[size];
            byte[] greens = new byte[size];
            byte[] blues = new byte[size];
            colorModel.getReds(reds);
            colorModel.getGreens(greens);
            colorModel.getBlues(blues);
            RGB[] rgbs = new RGB[size];
            for (int i = 0; i < rgbs.length; i++) {
                rgbs[i] = new RGB(reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF);
            }
            PaletteData palette = new PaletteData(rgbs);
            ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
                    colorModel.getPixelSize(), palette);
            data.transparentPixel = colorModel.getTransparentPixel();
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[1];
            for (int y = 0; y < data.height; y++) {
                for (int x = 0; x < data.width; x++) {
                    raster.getPixel(x, y, pixelArray);
                    data.setPixel(x, y, pixelArray[0]);
                }
            }
            return data;
        }
        else if (bufferedImage.getColorModel() instanceof ComponentColorModel) {
            ComponentColorModel colorModel = (ComponentColorModel)bufferedImage.getColorModel();
            if (colorModel.getPixelSize() == 48) {
                PaletteData palette = new PaletteData(0x0000FF, 0x00FF00,0xFF0000);
                ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
                        24, palette);
                data.transparentPixel = -1;
                WritableRaster raster = bufferedImage.getRaster();
                int[] pixelArray = new int[3];
                for (int y = 0; y < data.height; y++) {
                    for (int x = 0; x < data.width; x++) {
                        raster.getPixel(x, y, pixelArray);
                        int pixel = palette.getPixel(new RGB((int) (pixelArray[0] * 255.0 / 65535), (int) (pixelArray[1] * 255.0 / 65535), (int) (pixelArray[2] * 255.0 / 65535)));
                        data.setPixel(x, y, pixel);
                    }
                }
                return data;
            } else {
                //ASSUMES: 3 BYTE BGR IMAGE TYPE
                PaletteData palette = new PaletteData(0x0000FF, 0x00FF00,0xFF0000);
                ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
                        colorModel.getPixelSize(), palette);
                //This is valid because we are using a 3-byte Data model with no transparent pixels
                data.transparentPixel = -1;
                WritableRaster raster = bufferedImage.getRaster();
                int[] pixelArray = new int[3];
                for (int y = 0; y < data.height; y++) {
                    for (int x = 0; x < data.width; x++) {
                        raster.getPixel(x, y, pixelArray);
                        int pixel = palette.getPixel(new RGB(pixelArray[0], pixelArray[1], pixelArray[2]));
                        data.setPixel(x, y, pixel);
                    }
                }
                return data;
            }
        }
        throw new IllegalStateException("Unable to convert bufferedImage to imageData");
    }

    public static ImageData convertToSWT(BufferedImage bufferedImageR, BufferedImage bufferedImageG, BufferedImage bufferedImageB) {
        if (bufferedImageR.getColorModel() instanceof ComponentColorModel) {
            ComponentColorModel colorModelR = (ComponentColorModel) bufferedImageR.getColorModel();
            ComponentColorModel colorModelG = (ComponentColorModel) bufferedImageR.getColorModel();
            ComponentColorModel colorModelB = (ComponentColorModel) bufferedImageR.getColorModel();
            if (colorModelR.getPixelSize() == 24 && colorModelG.getPixelSize() == 24 && colorModelB.getPixelSize() == 24) {
                if (bufferedImageR.getWidth() != bufferedImageG.getWidth() || bufferedImageR.getWidth() != bufferedImageB.getWidth() ||
                        bufferedImageR.getHeight() != bufferedImageG.getHeight() || bufferedImageR.getHeight() != bufferedImageB.getHeight()) {
                    throw new IllegalStateException("RGB images size dimensions don't match");
                }
                return getImageDataInts(bufferedImageR, bufferedImageG, bufferedImageB, ints -> ints[0]);
            } else
            if (colorModelR.getPixelSize() == 48 && colorModelG.getPixelSize() == 48 && colorModelB.getPixelSize() == 48) {
                if (bufferedImageR.getWidth() != bufferedImageG.getWidth() || bufferedImageR.getWidth() != bufferedImageB.getWidth() ||
                        bufferedImageR.getHeight() != bufferedImageG.getHeight() || bufferedImageR.getHeight() != bufferedImageB.getHeight()) {
                    throw new IllegalStateException("RGB images size dimensions don't match");
                }
                return getImageDataInts(bufferedImageR, bufferedImageG, bufferedImageB, ints -> (int) (ints[0] * 255.0 / 65535));
            } else
            if (colorModelR.getPixelSize() == 96 && colorModelG.getPixelSize() == 96 && colorModelB.getPixelSize() == 96) {
                if (bufferedImageR.getWidth() != bufferedImageG.getWidth() || bufferedImageR.getWidth() != bufferedImageB.getWidth() ||
                        bufferedImageR.getHeight() != bufferedImageG.getHeight() || bufferedImageR.getHeight() != bufferedImageB.getHeight()) {
                    throw new IllegalStateException("RGB images size dimensions don't match");
                }
                return getImageDataFloats(bufferedImageR, bufferedImageG, bufferedImageB, floats -> (float) (floats[0]));
            }
        }
        throw new IllegalStateException("Unable to convert bufferedImage to imageData");
    }

    private static ImageData getImageDataInts(BufferedImage bufferedImageR, BufferedImage bufferedImageG, BufferedImage bufferedImageB, Function<int[], Integer> function) {
        PaletteData palette = new PaletteData(0x0000FF, 0x00FF00,0xFF0000);
        ImageData data = new ImageData(bufferedImageR.getWidth(), bufferedImageR.getHeight(),
                24, palette);
        data.transparentPixel = -1;
        WritableRaster rasterR = bufferedImageR.getRaster();
        WritableRaster rasterG = bufferedImageG.getRaster();
        WritableRaster rasterB = bufferedImageB.getRaster();
        int[] pixelArrayR = new int[3];
        int[] pixelArrayG = new int[3];
        int[] pixelArrayB = new int[3];
        for (int y = 0; y < data.height; y++) {
            for (int x = 0; x < data.width; x++) {
                rasterR.getPixel(x, y, pixelArrayR);
                rasterG.getPixel(x, y, pixelArrayG);
                rasterB.getPixel(x, y, pixelArrayB);
                int pixel = palette.getPixel(new RGB(function.apply(pixelArrayR), function.apply(pixelArrayG), function.apply(pixelArrayB)));
                data.setPixel(x, y, pixel);
            }
        }
        return data;
    }

    private static ImageData getImageDataFloats(BufferedImage bufferedImageR, BufferedImage bufferedImageG, BufferedImage bufferedImageB, Function<float[], Float> function) {
        PaletteData palette = new PaletteData(0x0000FF, 0x00FF00,0xFF0000);
        ImageData data = new ImageData(bufferedImageR.getWidth(), bufferedImageR.getHeight(),
                24, palette);
        data.transparentPixel = -1;
        WritableRaster rasterR = bufferedImageR.getRaster();
        WritableRaster rasterG = bufferedImageG.getRaster();
        WritableRaster rasterB = bufferedImageB.getRaster();
        float[] pixelArrayR = new float[3];
        float[] pixelArrayG = new float[3];
        float[] pixelArrayB = new float[3];
        for (int y = 0; y < data.height; y++) {
            for (int x = 0; x < data.width; x++) {
                rasterR.getPixel(x, y, pixelArrayR);
                rasterG.getPixel(x, y, pixelArrayG);
                rasterB.getPixel(x, y, pixelArrayB);
                int pixel = palette.getPixel(new RGB(function.apply(pixelArrayR), function.apply(pixelArrayG), function.apply(pixelArrayB)));
                data.setPixel(x, y, pixel);
            }
        }
        return data;
    }
}
