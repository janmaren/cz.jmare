package cz.jmare.swt.image;

import java.util.Arrays;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;

public class ImageDataUtil {
    public static ImageData createImageData(int width, int height, int color, byte alpha) {
        ImageData imageData = createImageData(width, height);
        int size = width * height;
        int[] pixels = new int[size];
        byte[] alphas = new byte[size];
        for (int x = 0; x < size; x++) {
            pixels[x] = color;
            alphas[x] = alpha;
        }
        for (int y = 0; y < height; y++) {
            imageData.setPixels(0, y, width, pixels, 0);
            imageData.setAlphas(0, y, width, alphas, 0);
        }
        return imageData;
    }

    public static ImageData createImageData(int width, int height, byte alpha) {
        ImageData imageData = createImageData(width, height);
        int size = width * height;
        byte[] alphas = new byte[size];
        Arrays.fill(alphas, alpha);
        imageData.setAlphas(0, 0, size, alphas, 0);
        return imageData;
    }

    public static ImageData createImageData(int width, int height) {
        ImageData imageData = new ImageData(width, height, 24,
                new PaletteData(0xFF0000, 0x00FF00, 0x0000FF));
        return imageData;
    }

    public static void main(String[] args) {
        ImageLoader imageLoader = new ImageLoader();
        ImageData imageData = createImageData(20, 10, 1000, (byte) 0);
        imageLoader.data = new ImageData[] {imageData};
        imageLoader.save("C://Users//E_JMAREN//data.png", 5);
    }
}
