package cz.jmare.swt.key;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import cz.jmare.swt.status.StatusUtil;

public class KeyLogger {
    public static volatile boolean shiftPressed;
    public static volatile boolean ctrlPressed;
    public static volatile boolean altPressed;

    public static void init() {
        Display current = Display.getCurrent();
        current.addFilter(SWT.KeyDown, new Listener() {
            @Override
            public void handleEvent(Event e) {
                StatusUtil.clearMessage();
                if (((e.keyCode & SWT.SHIFT) == SWT.SHIFT)) {
                    shiftPressed = true;
                }
                if (((e.keyCode & SWT.CTRL) == SWT.CTRL)) {
                    ctrlPressed = true;
                }
                if (((e.keyCode & SWT.ALT) == SWT.ALT)) {
                    altPressed = true;
                }
            }
        });
        current.addFilter(SWT.KeyUp, new Listener() {
            @Override
            public void handleEvent(Event e) {
                if (((e.keyCode & SWT.SHIFT) == SWT.SHIFT)) {
                    shiftPressed = false;
                }
                if (((e.keyCode & SWT.CTRL) == SWT.CTRL)) {
                    ctrlPressed = false;
                }
                if (((e.keyCode & SWT.ALT) == SWT.ALT)) {
                    altPressed = false;
                }
            }
        });
    }
}
