package cz.jmare.swt.key;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;

public class FocusForCursor implements MouseTrackListener {
    private StyledText editor;

    public FocusForCursor(StyledText editor) {
        editor.addMouseTrackListener(this);

        this.editor = editor;
    }

    @Override
    public void mouseEnter(MouseEvent arg0) {
        editor.setFocus();
    }

    @Override
    public void mouseExit(MouseEvent arg0) {
    }

    @Override
    public void mouseHover(MouseEvent arg0) {
    }
}
