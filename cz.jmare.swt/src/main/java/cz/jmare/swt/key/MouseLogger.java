package cz.jmare.swt.key;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import cz.jmare.swt.status.StatusUtil;

public class MouseLogger {
    public static volatile boolean leftPressed;
    public static volatile boolean rightPressed;
    public static volatile boolean middlePressed;

    public static void init() {
        initWithStatus(StatusUtil.getStatusLineManager());
    }
    
    public static void initWithStatus(IStatusLineManager statusLineManager) {
        Display current = Display.getCurrent();
        MouseLisWithStatus mouseLisWithStatus = new MouseLisWithStatus(statusLineManager);
        current.addFilter(SWT.MouseDown, mouseLisWithStatus);
        current.addFilter(SWT.MouseUp, new Listener() {
            @Override
            public void handleEvent(Event event) {
                leftPressed = (event.stateMask & SWT.BUTTON1) == SWT.BUTTON1;
                middlePressed = (event.stateMask & SWT.BUTTON2) == SWT.BUTTON2;
                rightPressed = (event.stateMask & SWT.BUTTON3) == SWT.BUTTON3;
            }
        });
    }
    
    public static class MouseLisWithStatus implements Listener {
        IStatusLineManager statusLineManager;
        
        public MouseLisWithStatus(IStatusLineManager statusLineManager) {
            super();
            this.statusLineManager = statusLineManager;
        }

        public MouseLisWithStatus() {
            super();
        }

        @Override
        public void handleEvent(Event event) {
            if (statusLineManager != null) {
                statusLineManager.setErrorMessage("");
                statusLineManager.setMessage("");
            }
            
            leftPressed = (event.stateMask & SWT.BUTTON1) == SWT.BUTTON1;
            middlePressed = (event.stateMask & SWT.BUTTON2) == SWT.BUTTON2;
            rightPressed = (event.stateMask & SWT.BUTTON3) == SWT.BUTTON3;
        }
    }
}
