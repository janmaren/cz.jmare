package cz.jmare.swt.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.image.ImageUtil;

public class ActionOperation extends Action {

    private Runnable runnable;

    public ActionOperation(String text, String image, int keycode, Runnable runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionOperation(String text, int keycode, Runnable runnable) {
        super(text);
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionOperation(String text, String image, Runnable runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.runnable = runnable;
    }

    @Override
    public void run() {
        try {
            runnable.run();
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error in " + getText(), e);
            throw e;
        }
    }
}
