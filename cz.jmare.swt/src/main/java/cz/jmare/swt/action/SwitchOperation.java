package cz.jmare.swt.action;

import java.util.function.Consumer;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;

import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.image.ImageUtil;

public class SwitchOperation extends Action {

    private Consumer<Boolean> runnable;

    public SwitchOperation(String text, String image, int keycode, Consumer<Boolean> runnable) {
        super(text, IAction.AS_CHECK_BOX);
        this.runnable = runnable;
        setAccelerator(keycode);
        setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(image)));
    }

    public SwitchOperation(String text, String image, int keycode, Consumer<Boolean> runnable, boolean defaultChecked) {
        super(text, IAction.AS_CHECK_BOX);
        this.runnable = runnable;
        setAccelerator(keycode);
        setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(image)));
        setChecked(defaultChecked);
    }

    public SwitchOperation(String text, int keycode, Consumer<Boolean> runnable) {
        super(text, IAction.AS_CHECK_BOX);
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public SwitchOperation(String text, String image, Consumer<Boolean> runnable) {
        super(text, IAction.AS_CHECK_BOX);
        this.runnable = runnable;
        setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(image)));
    }

    public SwitchOperation(String text, String image, Consumer<Boolean> runnable, boolean defaultChecked) {
        super(text, IAction.AS_CHECK_BOX);
        this.runnable = runnable;
        setImageDescriptor(ImageDescriptor.createFromImage(ImageUtil.get(image)));
        setChecked(defaultChecked);
    }

    @Override
    public void run() {
        try {
            runnable.accept(isChecked());
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error in " + getText(), e);
            throw e;
        }
    }
}
