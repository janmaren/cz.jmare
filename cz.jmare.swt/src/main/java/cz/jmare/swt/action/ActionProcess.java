package cz.jmare.swt.action;

import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.image.ImageUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;

public class ActionProcess extends Action {

    private final StatusLineManager slm;

    private Runnable runnable;

    public ActionProcess(StatusLineManager slm, String text, String image, int keycode, Runnable runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.slm = slm;
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionProcess(StatusLineManager slm, String text, int keycode, Runnable runnable) {
        super(text);
        this.slm = slm;
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionProcess(StatusLineManager slm, String text, String image, Runnable runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.slm = slm;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        try {
            runnable.run();
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error in " + getText(), e);
            slm.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), e.getMessage() != null ? e.getMessage() : "Operation failed");
        }
    }
}
