package cz.jmare.swt.action;

import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.image.ImageUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Event;

import java.util.function.Consumer;

public class ActionEventOperation extends Action {

    private Consumer<Event> runnable;

    public ActionEventOperation(String text, String image, int keycode, Consumer<Event> runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionEventOperation(String text, int keycode, Consumer<Event> runnable) {
        super(text);
        this.runnable = runnable;
        setAccelerator(keycode);
    }

    public ActionEventOperation(String text, String image, Consumer<Event> runnable) {
        super(text, ImageDescriptor.createFromImage(ImageUtil.get(image)));
        this.runnable = runnable;
    }

    @Override
    public void runWithEvent(Event event) {
        try {
            runnable.accept(event);
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error in " + getText(), e);
            throw e;
        }
    }
}
