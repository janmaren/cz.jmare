package cz.jmare.swt.exception;

public class UnableToDoException extends RuntimeException {
    private static final long serialVersionUID = -5815444400265119009L;

    public UnableToDoException(String message) {
        super(message);
    }

}
