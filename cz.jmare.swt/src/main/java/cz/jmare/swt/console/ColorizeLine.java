package cz.jmare.swt.console;

import org.eclipse.swt.custom.StyledText;

public interface ColorizeLine {
    void colorize(StyledText styledText, int offset, int length);
}
