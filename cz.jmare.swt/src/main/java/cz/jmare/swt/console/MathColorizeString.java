package cz.jmare.swt.console;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;

public class MathColorizeString implements ColorizeString {
    private final static Pattern TITLE_PATTERN = Pattern.compile("^[A-Za-z\\._ \\-\\(\\)]+:");
    private final static Pattern double_PATTERN = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?");

    @Override
    public void colorize(StyledText styledText, int offset, String s) {
        Matcher matcher = TITLE_PATTERN.matcher(s);
        if (matcher.find()) {
            StyleRange styleRange = new StyleRange(offset, matcher.end(), Display.getCurrent().getSystemColor(SWT.COLOR_DARK_MAGENTA), null);
            styledText.setStyleRange(styleRange);
        }

        matcher = double_PATTERN.matcher(s);
        while (matcher.find()) {
            StyleRange styleRange = new StyleRange(offset + matcher.start(), matcher.end() - matcher.start(), Display.getCurrent().getSystemColor(SWT.COLOR_DARK_CYAN), null);
            styledText.setStyleRange(styleRange);
        }
    }

}
