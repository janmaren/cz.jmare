package cz.jmare.swt.console;

import java.io.OutputStream;
import java.io.PrintStream;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;

public class ConsoleStyledTextPrintStream  extends PrintStream {
    private StyledText consoleText;
    private boolean stderr;
    private ColorizeString colorizeStandartOut = new NoColorizeString();
    private ColorizeLine colorizeStandartError = new EmphasizeColorizeLine();

    public ConsoleStyledTextPrintStream(OutputStream out, StyledText consoleText, boolean stderr) {
        super(out);
        this.consoleText = consoleText;
        this.stderr = stderr;
    }

    @Override
    public void println(String s) {
        printlnInternal(s);
    }

    @Override
    public void print(String s) {
        printInternal(s);
    }

    @Override
    public void println(Object s) {
        printlnInternal(s.toString());
    }

    public void printlnInternal(String s) {
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                int charCount = consoleText.getCharCount();
                consoleText.append(s);
                consoleText.append("\n");
                if (stderr) {
                    colorizeStandartError.colorize(consoleText, charCount, s.length() + 1);
                } else {
                    colorizeStandartOut.colorize(consoleText, charCount, s);
                }

                consoleText.setSelection(consoleText.getCharCount());
            }
        });
    }

    public void printInternal(String s) {
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                int charCount = consoleText.getCharCount();
                consoleText.append(s);
                if (stderr) {
                    colorizeStandartError.colorize(consoleText, charCount, s.length() + 1);
                } else {
                    colorizeStandartOut.colorize(consoleText, charCount, s);
                }

                consoleText.setSelection(consoleText.getCharCount());
            }});
    }

    @Override
    public void println(int x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void print(int x) {
        printInternal(String.valueOf(x));
    }

    @Override
    public void print(boolean b) {
        printInternal(String.valueOf(b));
    }

    @Override
    public void print(char c) {
        printInternal(String.valueOf(c));
    }

    @Override
    public void print(long l) {
        printInternal(String.valueOf(l));
    }

    @Override
    public void print(float f) {
        printInternal(String.valueOf(f));
    }

    @Override
    public void print(double d) {
        printInternal(String.valueOf(d));
    }

    @Override
    public void print(char[] s) {
        printInternal(String.valueOf(s));
    }

    @Override
    public void print(Object obj) {
        printInternal(obj.toString());
    }

    @Override
    public void println() {
        printlnInternal("");
    }

    @Override
    public void println(boolean x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void println(char x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void println(long x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void println(float x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void println(double x) {
        printlnInternal(String.valueOf(x));
    }

    @Override
    public void println(char[] x) {
        printlnInternal(String.valueOf(x));
    }

    public void setColorizeStandartOut(ColorizeString colorizeStandartOut) {
        this.colorizeStandartOut = colorizeStandartOut;
    }

    public void setColorizeStandartError(ColorizeLine colorizeStandartError) {
        this.colorizeStandartError = colorizeStandartError;
    }
}
