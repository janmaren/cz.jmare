package cz.jmare.swt.console;

import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import cz.jmare.swt.key.ActionKeys;

public class ConsoleCompositeSimple extends Composite {

    private StyledText consoleText;
    private PrintStream origSysout;
    private PrintStream origSyserr;
    private ConsoleStyledTextPrintStream out;
    private ConsoleStyledTextPrintStream err;

    public ConsoleCompositeSimple(Composite parent, int style) {
        super(parent, style);

        GridLayout gl = new GridLayout();
        gl.marginLeft = 0;
        gl.marginHeight = 0;
        gl.marginWidth = 0;
        gl.marginBottom = 0;
        gl.marginRight = 0;
        gl.numColumns = 1;
        this.setLayout(gl);

        final Clipboard cb = new Clipboard(Display.getCurrent());

        consoleText = new StyledText(this, SWT.MULTI |SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL);
        consoleText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        new ActionKeys(consoleText);
        //new FocusForCursor(consoleText);

        Menu menu = new Menu(consoleText);

        MenuItem clearItem = new MenuItem(menu, SWT.PUSH);
        clearItem.setText("Clear");
        clearItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {clear();}));

        MenuItem copySelectionItem = new MenuItem(menu, SWT.PUSH);
        copySelectionItem.setText("Copy Selection");
        copySelectionItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent selectionEvent) {
                Point selection = consoleText.getSelection();
                String selText = consoleText.getText().substring(selection.x, selection.y);
                TextTransfer textTransfer = TextTransfer.getInstance();
                cb.setContents(new Object[]{selText}, new Transfer[]{textTransfer});
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
            }
        });

        consoleText.setMenu(menu);

        origSysout = System.out;
        origSyserr = System.err;

        if (out == null) {
            out = new ConsoleStyledTextPrintStream(System.out, consoleText, false);
        }
        System.setOut(out);
        if (err == null) {
            err = new ConsoleStyledTextPrintStream(System.err, consoleText, true);
        }
        System.setErr(err);
    }

    @Override
    public void dispose() {
        super.dispose();
        System.setOut(origSysout);
        System.setOut(origSyserr);
    }

    public void setOut(ConsoleStyledTextPrintStream out) {
        this.out = out;
    }

    public void setErr(ConsoleStyledTextPrintStream err) {
        this.err = err;
    }

    public ConsoleStyledTextPrintStream getOut() {
        return out;
    }

    public ConsoleStyledTextPrintStream getErr() {
        return err;
    }

    public void clear() {
        consoleText.setText("");
    }
}
