package cz.jmare.swt.console;

import org.eclipse.swt.custom.StyledText;

public interface ColorizeString {
    void colorize(StyledText styledText, int offset, String s);
}
