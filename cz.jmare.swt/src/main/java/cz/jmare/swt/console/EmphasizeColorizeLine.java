package cz.jmare.swt.console;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;

public class EmphasizeColorizeLine implements ColorizeLine {
    @Override
    public void colorize(StyledText styledText, int offset, int length) {
        StyleRange styleRange = new StyleRange(offset, length, Display.getCurrent().getSystemColor(SWT.COLOR_RED), null);
        styledText.setStyleRange(styleRange);
    }
}
