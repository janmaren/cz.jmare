package cz.jmare.swt.status;

import cz.jmare.progress.ProgressStatus;

public interface ProgressableBlock {
    void run(StatusLineRepeatableDisplay statusLineRepeatableDisplay, ProgressStatus<Long> progressStatus) throws Exception;
}
