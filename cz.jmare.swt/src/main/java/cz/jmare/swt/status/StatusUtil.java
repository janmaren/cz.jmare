package cz.jmare.swt.status;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.exception.UnableToDoException;
import cz.jmare.swt.image.ImageUtil;

public class StatusUtil {
    static IStatusLineManager statusLineManager;

    public static void setStatusLineManager(IStatusLineManager statusLineManager) {
        StatusUtil.statusLineManager = statusLineManager;
    }

    public static IStatusLineManager getStatusLineManager() {
        return statusLineManager;
    }

    public static void clearMessage() {
        if (statusLineManager != null) {
            statusLineManager.setErrorMessage("");
            statusLineManager.setMessage("");
        }
    }

    public static void infoMessage(String str) {
        if (statusLineManager != null) {
            statusLineManager.setMessage(str);
        } else if (Display.getCurrent().getActiveShell() != null) {
            MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.CANCEL | SWT.ICON_INFORMATION);
            messageBox.setMessage(str);
            messageBox.open();
        } else {
            LoggerSlf4jOptional.logInfo(str);
        }
    }

    public static void warnMessage(String str) {
        if (statusLineManager != null) {
            statusLineManager.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), str);
        } else if (Display.getCurrent().getActiveShell() != null) {
            MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.CANCEL | SWT.ICON_WARNING);
            messageBox.setMessage(str);
            messageBox.open();
        } else {
            LoggerSlf4jOptional.logWarn(str);
        }
    }

    public static void errorMessage(String str) {
        if (statusLineManager != null) {
            statusLineManager.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_ERROR), str);
        } else if (Display.getCurrent().getActiveShell() != null) {
            MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.CANCEL | SWT.ICON_ERROR);
            messageBox.setMessage(str);
            messageBox.open();
        } else {
            System.err.println(str);
        }
    }

    public static void errorMessage(StatusLineManager slm, String str) {
        slm.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_ERROR), str);
    }
    
    public static void errorMessage(Exception e) {
        if (e instanceof UnableToDoException) {
            String combinedMessage = e.getMessage();
            warnMessage(combinedMessage);
        } else {
            String combinedMessage = ExceptionMessage.getCombinedMessage(e);
            LoggerSlf4jOptional.logError(combinedMessage, e);
            errorMessage(combinedMessage);
        }
    }

    public static void withErrorMessage(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            if (e instanceof UnableToDoException) {
                String combinedMessage = e.getMessage();
                warnMessage(combinedMessage);
            } else {
                String combinedMessage = ExceptionMessage.getCombinedMessage(e);
                errorMessage(combinedMessage);
            }
        }
    }

    public static void errorMessage(StatusLineManager slm, Exception e) {
        String combinedMessage = ExceptionMessage.getCombinedMessage(e);
        LoggerSlf4jOptional.logError(combinedMessage, e);
        errorMessage(slm, combinedMessage);
    }
}
