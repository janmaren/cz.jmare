package cz.jmare.swt.status;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

import cz.jmare.progress.ReadableProgressStatus;
import cz.jmare.swt.image.ImageUtil;

/**
 * way how to stop running<br>
 * 1) stop = true<br>
 */
public class StatusLineRepeatableDisplay implements Runnable {
    private ReadableProgressStatus<Long> readableProgressStatus;

    private IStatusLineManager statusLineManager;

    private volatile String prependString;

    private Display display;

    private volatile boolean stop;

    private volatile boolean finished;

    public StatusLineRepeatableDisplay(ReadableProgressStatus<Long> simpleProgressStatus, Display display, IStatusLineManager statusLineManager) {
        super();
        this.readableProgressStatus = simpleProgressStatus;
        this.display = display;
        this.statusLineManager = statusLineManager;
    }

    @Override
    public void run() {
        while (!stop) {
            long progressValue = readableProgressStatus.getProgressValue();
            Long maxValue = readableProgressStatus.getMaxValue();
            if (maxValue > 0 && progressValue >= 0) {
                int relatio = (int) (100 * (progressValue /*+ 1*/) / maxValue);
                display.asyncExec(() -> {
                    if (prependString != null) {
                        statusLineManager.setMessage(prependString + " " + relatio + "%");
                    } else {
                        statusLineManager.setMessage(relatio + "%");
                    }
                });
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                return;
            }
        }
        finished = true;
    }

    public void stop(boolean clearStatus) {
        if (this.stop) {
            return;
        }
        this.stop = true;
        waitForFinished();
        if (clearStatus) {
            display.asyncExec(() -> {
                statusLineManager.setMessage("");
            });
        }
    }

    public void stopAfterSuccess(String message) {
        if (this.stop) {
            return;
        }
        this.stop = true;
        if (message != null) {
            display.asyncExec(() -> {
                if (prependString != null) {
                    statusLineManager.setMessage(prependString + " 100%");
                } else {
                    statusLineManager.setMessage("100%");
                }
            });
            waitForFinished();
            display.asyncExec(() -> {
                statusLineManager.setMessage(message);
            });
        }
    }

    public void stopAfterError(String message) {
        if (this.stop) {
            return;
        }
        this.stop = true;
        if (message != null) {
            waitForFinished();
            display.asyncExec(() -> {
                statusLineManager.setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), message);
            });
        }
    }

    public void waitForFinished() {
        long start = System.currentTimeMillis();
        while (!finished && System.currentTimeMillis() - start < 1000) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
    }

    public void setPrependString(String prependString) {
        this.prependString = prependString;
    }
}
