package cz.jmare.swt.status;

import java.util.function.Consumer;

import org.eclipse.swt.widgets.Display;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.function.RunnableCheckException;
import cz.jmare.progress.OperationInterruptedException;
import cz.jmare.progress.ReadableProgressStatus;
import cz.jmare.progress.SimpleLongReadableProgressStatus;

public class ProgressUtil {

    public static void withProgress(String progressPrependString, ProgressableBlock progressableBlock, String successfulMessage, String errorMessage) {
        final Display display = Display.getCurrent();
        ReadableProgressStatus<Long> progressStatus = new SimpleLongReadableProgressStatus();
        StatusLineRepeatableDisplay statusLineRepeatableDisplay = new StatusLineRepeatableDisplay(progressStatus, display, StatusUtil.getStatusLineManager());
        statusLineRepeatableDisplay.setPrependString(progressPrependString);
        new Thread(() -> {
            try {
                progressableBlock.run(statusLineRepeatableDisplay, progressStatus);
                statusLineRepeatableDisplay.stopAfterSuccess(successfulMessage);
            } catch (OperationInterruptedException e) {
                statusLineRepeatableDisplay.stopAfterError("Operation interrupted");
            } catch (Exception e) {
                statusLineRepeatableDisplay.stopAfterError(ExceptionMessage.getCombinedMessage(errorMessage, e));
            }
        }, "short time job").start();
        new Thread(statusLineRepeatableDisplay, "short time job - display").start();
    }

    public static void withProgress(ProgressableBlock progressableBlock, String successfulMessage, String errorMessage, RunnableCheckException onSuccessBlock) {
        final Display display = Display.getCurrent();
        ReadableProgressStatus<Long> progressStatus = new SimpleLongReadableProgressStatus();
        StatusLineRepeatableDisplay statusLineRepeatableDisplay = new StatusLineRepeatableDisplay(progressStatus, display, StatusUtil.getStatusLineManager());
        new Thread(() -> {
            try {
                progressableBlock.run(statusLineRepeatableDisplay, progressStatus);
                statusLineRepeatableDisplay.stopAfterSuccess(successfulMessage);
                display.asyncExec(() -> {
                    try {
                        onSuccessBlock.run();
                    } catch (Exception e) {
                        display.asyncExec(() -> {
                            StatusUtil.statusLineManager.setErrorMessage(errorMessage);
                        });
                    }
                });
            } catch (OperationInterruptedException e) {
                statusLineRepeatableDisplay.stopAfterError("Operation interrupted");
            } catch (Exception e) {
                e.printStackTrace();
                statusLineRepeatableDisplay.stopAfterError(ExceptionMessage.getCombinedMessage(errorMessage, e));
            }
        }, "short time job").start();
        new Thread(statusLineRepeatableDisplay, "short time job - display").start();
    }

    public static void withProgress(ProgressableBlock progressableBlock, String successfulMessage, String errorMessage) {
        final Display display = Display.getCurrent();
        ReadableProgressStatus<Long> progressStatus = new SimpleLongReadableProgressStatus();
        StatusLineRepeatableDisplay statusLineRepeatableDisplay = new StatusLineRepeatableDisplay(progressStatus, display, StatusUtil.getStatusLineManager());
        new Thread(() -> {
            try {
                progressableBlock.run(statusLineRepeatableDisplay, progressStatus);
                statusLineRepeatableDisplay.stopAfterSuccess(successfulMessage);
            } catch (OperationInterruptedException e) {
                statusLineRepeatableDisplay.stopAfterError("Operation interrupted");
            } catch (Exception e) {
                statusLineRepeatableDisplay.stopAfterError(ExceptionMessage.getCombinedMessage(errorMessage, e));
            }
        }, "short time job").start();
        new Thread(statusLineRepeatableDisplay, "short time job - display").start();
    }

    public static void withProgress(ProgressableBlock progressableBlock, Consumer<Exception> exceptionConsumer) {
        final Display display = Display.getCurrent();
        ReadableProgressStatus<Long> progressStatus = new SimpleLongReadableProgressStatus();
        StatusLineRepeatableDisplay statusLineRepeatableDisplay = new StatusLineRepeatableDisplay(progressStatus, display, StatusUtil.getStatusLineManager());
        new Thread(() -> {
            try {
                progressableBlock.run(statusLineRepeatableDisplay, progressStatus);
                statusLineRepeatableDisplay.stop(true);
            } catch (OperationInterruptedException e) {
                statusLineRepeatableDisplay.stopAfterError("Operation interrupted");
            } catch (Exception e) {
                statusLineRepeatableDisplay.stop(true);
                exceptionConsumer.accept(e);
            }
        }, "short time job").start();
        new Thread(statusLineRepeatableDisplay, "short time job - display").start();
    }
}
