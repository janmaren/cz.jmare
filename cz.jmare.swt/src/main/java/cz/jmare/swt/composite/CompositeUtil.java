package cz.jmare.swt.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;

public class CompositeUtil {
    public static Composite noMarginComposite(Composite parent, int columns) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        composite.setLayout(gridLayout);
        return composite;
    }

    /**
     * Not works...
     * @param parent
     * @return
     */
    public static Composite noMarginRowComposite(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        composite.setLayout(rowLayout);
        return composite;
    }
}
