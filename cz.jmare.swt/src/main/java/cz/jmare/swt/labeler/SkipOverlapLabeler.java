package cz.jmare.swt.labeler;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class SkipOverlapLabeler implements Labeler {
    private List<Rectangle> rectangles = new ArrayList<>();

    @Override
    public boolean drawLabel(GC gc, String text, int x, int y, boolean transparent) {
        Point point = gc.textExtent(text);
        Rectangle drawRect = new Rectangle(x, y, point.x, point.y);


        for (Rectangle rectangle : rectangles) {
            if (rectangle.intersects(drawRect)) {
                return false;
            }
        }

        gc.drawText(text, x, y, transparent);
        rectangles.add(drawRect);
        return true;
    }
}
