package cz.jmare.swt.labeler;

import org.eclipse.swt.graphics.GC;

public interface Labeler {
    boolean drawLabel(GC gc, String text, int x, int y, boolean transparent);
    
    default boolean drawLabel(GC gc, String text, int x, int y) {
        return drawLabel(gc, text, x, y, true);
    }
}
