package cz.jmare.swt.util;

import org.eclipse.swt.graphics.RGB;

import cz.jmare.swt.color.ColorManager;

public class RGBSwtUtil {
    public static RGB createRGB(String str) {
        if (!str.startsWith("#")) {
            str = "#" + str;
        }
        return new RGB(ColorManager.hex2decimal(str.substring(1, 3)), ColorManager.hex2decimal(str.substring(3, 5)), ColorManager.hex2decimal(str.substring(5, 7)));
    }

    /**
     * Parse RGB writen as string 'r, g, beta', like '255, 0, 128'
     * @param str
     * @return
     */
    public static RGB parseRGBNumbers(String str) {
        String[] split = str.trim().split(",");
        if (split.length != 3) {
            throw new IllegalArgumentException("String " + str + " hasn't 3 numbers");
        }
        int r = Integer.parseInt(split[0].trim());
        int g = Integer.parseInt(split[1].trim());
        int b = Integer.parseInt(split[2].trim());
        return new RGB(r, g, b);
    }

    public static String toRGBNumbersString(RGB rgb) {
        return rgb.red + ", " + rgb.green + ", " + rgb.blue;
    }


    public static RGB rLowestToRGB(int rgb) {
        int red = rgb & 0xFF;
        int green = (rgb >> 8) & 0xff;
        int blue = (rgb >> 16) & 0xff;
        return new RGB(red, green, blue);
    }

    public static RGB rHighestToRGB(int rgb) {
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xFF;
        return new RGB(red, green, blue);
    }

    public static int rHighestToRGBInt(RGB rgb) {
        return (0xFF << 24) | rgb.red << 16 | rgb.green << 8 | rgb.blue;
    }
}
