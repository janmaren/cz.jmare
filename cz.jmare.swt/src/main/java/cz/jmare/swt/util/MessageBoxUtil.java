package cz.jmare.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageBoxUtil {
    public static boolean isYes(Shell shell, String question) {
        MessageBox messageBox = new MessageBox(shell, SWT.YES | SWT.NO | SWT.ICON_WARNING);
        messageBox.setMessage(question);
        return SWT.YES == messageBox.open();
    }
}
