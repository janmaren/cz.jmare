package cz.jmare.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

public class STextInput {
    public static double[] validateAndGetDouble(Text scaleText, double fallbackValue) {
        String text = scaleText.getText();
        try {
            double[] parseDoubles = parseDoubles(text);
            scaleText.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
            return parseDoubles;
        } catch (Exception e) {
            scaleText.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
            return new double[] {fallbackValue, fallbackValue};
        }
    }

    static double[] parseDoubles(String str) throws Exception {
        int indexOf = str.indexOf(",");
        if (indexOf == -1) {
            return new double[] {Double.valueOf(str.trim()), Double.valueOf(str.trim())};
        } else {
            String str1 = str.substring(0, indexOf).trim();
            String str2 = str.substring(indexOf + 1).trim();
            return new double[] {Double.valueOf(str1), Double.valueOf(str2)};
        }
    }
}
