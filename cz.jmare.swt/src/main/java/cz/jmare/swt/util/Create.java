package cz.jmare.swt.util;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;

import cz.jmare.function.ConsumerCheckException;
import cz.jmare.function.RunnableCheckException;

public class Create {
    public static Button buttonStatus(Composite parent, String text, RunnableCheckException runnableCheckException) {
        return buttonStatus(parent, SWT.NONE, text, runnableCheckException);
    }

    public static Button buttonStatus(Composite parent, String text, Consumer<Button> consumer, RunnableCheckException runnableCheckException) {
        Button button = buttonStatus(parent, text, runnableCheckException);
        consumer.accept(button);
        return button;
    }

    public static Button buttonStatus(Composite parent, int style, String text, RunnableCheckException runnableCheckException) {
        Button button = new Button(parent, style);
        button.setText(text);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorStatus(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return button;
    }

    public static Button button(Composite parent, String text, RunnableCheckException runnableCheckException) {
        return button(parent, SWT.NONE, text, runnableCheckException);
    }

    public static Button button(Composite parent, int style, String text, RunnableCheckException runnableCheckException) {
        Button button = new Button(parent, style);
        button.setText(text);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return button;
    }

    public static Button button(Composite parent, String text, Consumer<Button> consumer, RunnableCheckException runnableCheckException) {
        Button button = button(parent, text, runnableCheckException);
        consumer.accept(button);
        return button;
    }

    public static Button button(Composite parent, Consumer<Button> consumer) {
        Button button = new Button(parent, SWT.NONE);
        consumer.accept(button);
        return button;
    }

    public static Button button(Composite parent, int style, Consumer<Button> consumer) {
        Button button = new Button(parent, style);
        consumer.accept(button);
        return button;
    }

    public static Label label(Composite parent) {
        Label label = new Label(parent, SWT.NONE);
        return label;
    }

    public static Label label(Composite parent, String text) {
        Label label = new Label(parent, SWT.NONE);
        label.setText(text);
        return label;
    }

    public static Label label(Composite parent, String text, Consumer<Label> consumer) {
        Label label = new Label(parent, SWT.NONE);
        label.setText(text);
        consumer.accept(label);
        return label;
    }

    public static GridData gridData(Control c, Consumer<GridData> consumer) {
        GridData gridData = new GridData();
        consumer.accept(gridData);
        c.setLayoutData(gridData);
        return gridData;
    }

    public static RowData rowData(Control c, Consumer<RowData> consumer) {
        RowData rowData = new RowData();
        consumer.accept(rowData);
        c.setLayoutData(rowData);
        return rowData;
    }

    public static FormData formData(Composite c, Consumer<FormData> consumer) {
        FormData formData = new FormData();
        consumer.accept(formData);
        c.setLayoutData(formData);
        return formData;
    }

    public static Combo combo(Composite parent) {
        Combo combo = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
        return combo;
    }

    public static Combo combo(Composite parent, String toolTipText) {
        Combo combo = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
        combo.setToolTipText(toolTipText);
        return combo;
    }

    public static Combo combo(Composite parent, RunnableCheckException runnableCheckException) {
        Combo combo = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
        combo.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return combo;
    }

    public static Combo combo(Composite parent, String toolTipText, RunnableCheckException runnableCheckException) {
        Combo combo = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
        combo.setToolTipText(toolTipText);
        combo.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return combo;
    }

    public static Combo combo(Composite parent, int style, Consumer<Combo> consumer) {
        Combo combo = new Combo(parent, style);
        consumer.accept(combo);
        return combo;
    }

    public static Text text(Composite parent) {
        Text text = new Text(parent, SWT.BORDER);
        return text;
    }

    public static Text text(Composite parent, String value) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        return text;
    }

    public static Text text(Composite parent, String value, int widthInGrid) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        GridData gridData = new GridData();
        gridData.widthHint = widthInGrid;
        text.setLayoutData(gridData);
        return text;
    }

    public static Text text(Composite parent, String value, int widthInGrid, String toolTipText) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        text.setToolTipText(toolTipText);
        GridData gridData = new GridData();
        gridData.widthHint = widthInGrid;
        text.setLayoutData(gridData);
        return text;
    }

    public static Text text(Composite parent, String value, Consumer<Text> consumer) {
        return text(parent, SWT.BORDER, value, consumer);
    }

    public static Text text(Composite parent, int style, String value, Consumer<Text> consumer) {
        Text text = new Text(parent, style);
        text.setText(value);
        consumer.accept(text);
        return text;
    }

    public static Button checkButton(Composite parent, boolean select, ConsumerCheckException<Button> runnableCheckException) {
        Button button = new Button(parent, SWT.CHECK);
        button.setSelection(select);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.accept(button);
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return button;
    }

    public static Button checkButton(Composite parent, boolean select, Consumer<Button> consumer, ConsumerCheckException<Button> runnableCheckException) {
        Button button = checkButton(parent, select, runnableCheckException);
        consumer.accept(button);
        return button;
    }

    public static Button checkButton(Composite parent, Consumer<Button> consumer, boolean select) {
        Button button = new Button(parent, SWT.CHECK);
        button.setSelection(select);
        consumer.accept(button);
        return button;
    }

    public static Button checkButton(Composite parent, boolean select) {
        Button button = new Button(parent, SWT.CHECK);
        button.setSelection(select);
        return button;
    }

    public static Button radioButton(Composite parent, Consumer<Button> consumer, boolean select) {
        Button button = new Button(parent, SWT.RADIO);
        button.setSelection(select);
        consumer.accept(button);
        return button;
    }

    public static Button radioButton(Composite parent, Consumer<Button> consumer, boolean select, ConsumerCheckException<Button> runnableCheckException) {
        Button button = new Button(parent, SWT.RADIO);
        button.setSelection(select);
        consumer.accept(button);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.accept(button);
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return button;
    }

    public static Label createHorizontalLine(Composite parent) {
        Label horizLineLabel = new Label(parent, SWT.SEPARATOR | SWT.SHADOW_OUT | SWT.HORIZONTAL);
        Layout parentLayout = parent.getLayout();
        if (parentLayout instanceof GridLayout) {
            GridLayout gridLayout = (GridLayout) parentLayout;
            int columns = gridLayout.numColumns;
            Create.gridData(horizLineLabel, (c) -> {
                c.horizontalSpan = columns;
                c.grabExcessHorizontalSpace = true;
                c.horizontalAlignment = SWT.FILL;
            });
        } else {
            Create.gridData(horizLineLabel, (c) -> {
                c.horizontalSpan = 1;
                c.grabExcessHorizontalSpace = true;
                c.horizontalAlignment = SWT.FILL;
            });
        }
        return horizLineLabel;
    }

    public static Button imageButton(Composite parent, int style, Image image, String text, Consumer<Button> consumer, RunnableCheckException runnableCheckException) {
        Button button = new Button(parent, style);
        button.setToolTipText(text);
        button.setImage(image);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        consumer.accept(button);
        return button;
    }

    public static Button imageButton(Composite parent, Image image, String text, RunnableCheckException runnableCheckException) {
        return imageButton(parent, SWT.NONE, image, text, b -> {}, runnableCheckException);
    }

    public static Button imageButtonStatus(Composite parent, int style, Image image, String text, Consumer<Button> consumer, RunnableCheckException runnableCheckException) {
        Button button = new Button(parent, style);
        button.setToolTipText(text);
        button.setImage(image);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorStatus(() -> {
                    runnableCheckException.run();
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        consumer.accept(button);
        return button;
    }

    public static Button imageButtonStatus(Composite parent, Image image, String text, RunnableCheckException runnableCheckException) {
        return imageButtonStatus(parent, SWT.NONE, image, text, b -> {}, runnableCheckException);
    }

    public static Button imageButtonStatus(Composite parent, int style, Image image, String text, Consumer<Button> consumer) {
        Button button = new Button(parent, style);
        button.setToolTipText(text);
        button.setImage(image);
        consumer.accept(button);
        return button;
    }

    public static Composite composite(Composite parent, int style, Consumer<Composite> consumer) {
        Composite composite = new Composite(parent, style);
        consumer.accept(composite);
        return composite;
    }

    public static Composite composite(Composite parent, Consumer<Composite> consumer) {
        return composite(parent, SWT.NONE, consumer);
    }

    public static Composite noMarginGridComposite(Composite parent, int columns) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        composite.setLayout(gridLayout);
        return composite;
    }

    public static Composite noMarginGridComposite(Composite parent, int columns, Consumer<Composite> consumer) {
        Composite composite = noMarginGridComposite(parent, columns);
        consumer.accept(composite);
        return composite;
    }

    public static void selectionListener(Combo combo, Consumer<SelectionEvent> eventConsumer) {
        combo.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventConsumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
    }

    public static void selectionListener(ToolItem toolItem, Consumer<SelectionEvent> eventConsumer) {
        toolItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventConsumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
    }

    public static void selectionListener(MenuItem menuItem, Consumer<SelectionEvent> eventConsumer) {
        menuItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventConsumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
    }

    public static void selectionListener(ToolItem toolItem, BiConsumer<SelectionEvent, ToolItem> eventConsumer) {
        toolItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventConsumer.accept(e, toolItem);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
    }
    
    public static Text text(Composite parent, String value, int widthInGrid, String toolTipText, int heightInGrid) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        text.setToolTipText(toolTipText);
        GridData gridData = new GridData();
        gridData.widthHint = widthInGrid;
        gridData.heightHint = heightInGrid;
        text.setLayoutData(gridData);
        return text;
    }
    
    public static Text text(Composite parent, String value, int widthInGrid, int heightInGrid) {
        Text text = new Text(parent, SWT.BORDER);
        text.setText(value);
        GridData gridData = new GridData();
        gridData.widthHint = widthInGrid;
        gridData.heightHint = heightInGrid;
        text.setLayoutData(gridData);
        return text;
    }
    
    public static Button checkButton(Composite parent, boolean select, String label) {
        Button button = new Button(parent, SWT.CHECK);
        button.setSelection(select);
        button.setText(label);
        return button;
    }
    
    public static Button checkButton(Composite parent, boolean select, String label, Consumer<Boolean> pressed) {
        Button button = new Button(parent, SWT.CHECK);
        button.setSelection(select);
        button.setText(label);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                pressed.accept(button.getSelection());
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                
            }
        });
        return button;
    }
    
    public static Button radioButton(Composite parent, boolean select, String label, Consumer<Boolean> pressed) {
        Button button = new Button(parent, SWT.RADIO);
        button.setSelection(select);
        button.setText(label);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                pressed.accept(button.getSelection());
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                
            }
        });
        return button;
    }
    
    public static Button radioButton(Composite parent, boolean select, String label) {
        Button button = new Button(parent, SWT.RADIO);
        button.setSelection(select);
        button.setText(label);
        return button;
    }
    
    public static Button button(Composite parent, int style, String text, Consumer<Button> consumer, Consumer<SelectionEvent> runnableCheckException) {
        Button button = new Button(parent, style);
        button.setText(text);
        button.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.accept(e);
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        consumer.accept(button);
        return button;
    }
    
    public static MenuItem menuItem(Menu menu, String label, Consumer<SelectionEvent> runnableCheckException) {
        return menuItem(menu, SWT.NONE, label, runnableCheckException);
    }
    
    public static MenuItem menuItem(Menu menu, int style, String label, Consumer<SelectionEvent> runnableCheckException) {
        MenuItem menuItem = new MenuItem(menu, style);
        menuItem.setText(label);
        menuItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                SMessage.withErrorDialog(() -> {
                    runnableCheckException.accept(e);
                });
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });
        return menuItem;
    }
}
