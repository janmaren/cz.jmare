package cz.jmare.swt.util;

import java.util.function.Consumer;

import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;

public class SListener {
    public static void addSelectionListener(Button button, Consumer<SelectionEvent> consumer) {
        SelectionListener listener = new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                consumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        };
        button.addSelectionListener(listener);
    }

    public static void addModifyListener(Text text, Consumer<ModifyEvent> consumer) {
        ModifyListener listener = new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                consumer.accept(e);
            }
        };
        text.addModifyListener(listener);
    }

    public static void addSelectionListener(CCombo combo, Consumer<SelectionEvent> consumer) {
        SelectionListener listener = new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                consumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        };
        combo.addSelectionListener(listener);
    }

    public static void addSelectionListener(Combo combo, Consumer<SelectionEvent> consumer) {
        SelectionListener listener = new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                consumer.accept(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        };
        combo.addSelectionListener(listener);
    }

    public static void addModifyListener(CCombo combo, Consumer<ModifyEvent> consumer) {
        ModifyListener listener = new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                consumer.accept(e);
            }
        };
        combo.addModifyListener(listener);
    }

    public static void addKeyListener(CCombo combo, Consumer<ModifyEvent> consumer) {
        ModifyListener listener = new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                consumer.accept(e);
            }
        };
        combo.addModifyListener(listener);
    }

    public static void addKeyListener(Combo combo, Consumer<ModifyEvent> consumer) {
        ModifyListener listener = new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                consumer.accept(e);
            }
        };
        combo.addModifyListener(listener);
    }
}
