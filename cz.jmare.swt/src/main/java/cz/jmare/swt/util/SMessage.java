package cz.jmare.swt.util;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import cz.jmare.exception.ExceptionMessage;
import cz.jmare.function.RunnableCheckException;
import cz.jmare.logging.LoggerSlf4jOptional;
import cz.jmare.swt.status.StatusUtil;

public class SMessage {
    public static void withErrorConsole(RunnableCheckException runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            System.err.println(ExceptionMessage.getCombinedMessage(e));
            e.printStackTrace();
        }
    }

    public static void withErrorDialog(RunnableCheckException runnable) {
        Display display = Display.getDefault();
        if (display == null) {
            display = Display.getCurrent();
        }
        Shell activeShell = display.getActiveShell();
        withErrorDialog(activeShell, runnable);
    }

    public static void withErrorStatus(RunnableCheckException runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error occured", e);
            StatusUtil.errorMessage(e);
        }
    }
    
    public static void withErrorStatus(StatusLineManager slm, RunnableCheckException runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error occured", e);
            StatusUtil.errorMessage(slm, e);
        }
    }

    public static void withErrorDialog(Shell shell, RunnableCheckException runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            LoggerSlf4jOptional.logError("Error occured", e);
            MessageBox messageBox = new MessageBox(shell, SWT.CANCEL | SWT.ICON_ERROR);
            messageBox.setMessage(ExceptionMessage.getCombinedMessage(e));
            messageBox.open();
        }
    }

    public static void reportException(Exception e, Shell shell) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getClass().getCanonicalName()).append("\n");
        if (e.getMessage() != null) {
            sb.append(e.getMessage()).append("\n");
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        if (stackTrace != null) {
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(" ");
                sb.append(stackTraceElement.toString());
                sb.append("\n");
            }
        }
        System.err.println(sb.toString());
        MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
        messageBox.setMessage(sb.toString());
        messageBox.open();
    }

    public static void reportFriendlyException(String mainMessage, Exception e, Shell shell) {
        StringBuilder sb = new StringBuilder();
        sb.append(mainMessage).append("\n");
        if (e.getMessage() != null) {
            sb.append(e.getMessage()).append("\n");
        } else {
            StackTraceElement[] stackTrace = e.getStackTrace();
            if (stackTrace != null) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    sb.append(" ");
                    sb.append(stackTraceElement.toString());
                    sb.append("\n");
                }
            }
        }
        System.err.println(sb.toString());
        e.printStackTrace();
        MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
        messageBox.setMessage(sb.toString());
        messageBox.open();
    }
}
