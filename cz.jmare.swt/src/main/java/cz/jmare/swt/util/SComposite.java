package cz.jmare.swt.util;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

public class SComposite {
    public static Composite createFillCompositeIntoGridComposite(Composite parentComposite) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        FillLayout fillLayout = new FillLayout();
        fillLayout.marginHeight = 0;
        fillLayout.marginWidth = 0;
        panelComposite.setLayout(fillLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public static Composite createGridCompositeIntoGridComposite(Composite parentComposite, int columns) {
        return createGridCompositeIntoGridComposite(parentComposite, columns, gridData -> {});
    }

    public static Composite createGridCompositeIntoGridComposite(Composite parentComposite, int columns, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public static Composite createGridCompositeIntoGridComposite(Composite parentComposite, int columns, Consumer<GridLayout> gridLayoutConsumer, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        gridLayoutConsumer.accept(gridLayout);
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public static Composite createGridGroupIntoGridComposite(Composite parentComposite, int columns, String groupLabel, Consumer<GridLayout> gl, Consumer<GridData> gdConsumer) {
        Group panelComposite = new Group(parentComposite, SWT.NONE);
        panelComposite.setText(groupLabel);
        GridLayout gridLayout = new GridLayout(columns, false);
        gl.accept(gridLayout);
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }

    public static Composite createRowCompositeIntoGridComposite(Composite parentComposite, Consumer<GridData> gdConsumer) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginHeight = 0;
        rowLayout.marginWidth = 0;
        rowLayout.center = true;
        panelComposite.setLayout(rowLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.grabExcessVerticalSpace = true;
        gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }
}
