package cz.jmare.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

public class ActionKeys implements KeyListener {
    private StyledText editor;

    public ActionKeys(StyledText editor) {
        editor.addKeyListener(this);

        this.editor = editor;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        boolean isCtrl = (e.stateMask & SWT.CTRL) > 0;
        boolean isAlt = (e.stateMask & SWT.ALT) > 0;
        boolean isShift = (e.stateMask & SWT.SHIFT) > 0;
        if (isCtrl && !isAlt && !isShift) {
            if (e.keyCode == 'a') {
                editor.selectAll();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
    }
}
