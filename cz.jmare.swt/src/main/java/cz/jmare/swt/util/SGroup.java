package cz.jmare.swt.util;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

public class SGroup {
    public static Group createGridGroupIntoGridComposite(Composite parentComposite, String label, int columns, Consumer<GridData> gdConsumer) {
        Group panelComposite = new Group(parentComposite, SWT.NONE);
        panelComposite.setText(label);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 4;
        gridLayout.marginWidth = 4;
        panelComposite.setLayout(gridLayout);
        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        //gridData.grabExcessVerticalSpace = true;
        //gridData.verticalAlignment = SWT.FILL;
        gridData.horizontalAlignment = SWT.FILL;
        gdConsumer.accept(gridData);
        panelComposite.setLayoutData(gridData);
        return panelComposite;
    }
    
    public static Composite createNoMarginGridComposite(Composite parentComposite, int columns) {
        Composite panelComposite = new Composite(parentComposite, SWT.NONE);
        GridLayout gridLayout = new GridLayout(columns, false);
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        panelComposite.setLayout(gridLayout);
        return panelComposite;
    }
}
