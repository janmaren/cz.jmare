package org.maren.gis.demo1earth;

import static java.lang.Math.toRadians;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.maren.gis.projection.OrthoEarthSphProjection;
import org.maren.gis.projection.SphProjection;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.DrawListener;
import org.maren.gis.sphericcanvas.SphericCanvas;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.draw.BorderDraw;
import org.maren.gis.util.draw.SphericLinesDraw;

public class Demo1Prague implements DrawListener {
    public Demo1Prague() {
        super();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});
    }

    public void createContent(Shell shell) {
        final Composite composite = new Composite(shell, SWT.BORDER);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        composite.setLayout(new FillLayout());
        SphProjection projection = new OrthoEarthSphProjection();
        SphericCanvas sphericCanvas = new SphericCanvas(composite, projection, this);
        sphericCanvas.setSpheric0(new Spheric(toRadians(14.4156278), toRadians(50.0763203)));
        sphericCanvas.setZoom(80);
    }

    @Override
    public void draw(GC gc, CoordState coordState) {
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        
        SphericLinesDraw.drawGreyDarkEarthLines(gc, coordState, true);
        BorderDraw.drawGreyDarkSphericBorder(gc, coordState);
        Spheric prague = new Spheric(toRadians(14.4156278), toRadians(50.0763203));
        ScreenPoint praguePoint = coordState.toScreenCoordinates(prague);
        if (praguePoint.visibleHemis) { 
            gc.drawLine(praguePoint.point.x - 3, praguePoint.point.y, praguePoint.point.x + 3, praguePoint.point.y);
            gc.drawLine(praguePoint.point.x, praguePoint.point.y - 3, praguePoint.point.x, praguePoint.point.y + 3);
            gc.drawString("Prague", praguePoint.point.x, praguePoint.point.y, true);
        }
        
        gc.setFont(origFont);
    }
    
    public static void main(String[] args) {
        Display display = new Display();
        final Shell shell = new Shell(display);
        shell.setSize(300, 300);
        shell.setLayout(new FillLayout());

        shell.setText("Demo1Prague");
        
        Demo1Prague sphCanDemo = new Demo1Prague();
        sphCanDemo.createContent(shell);
        
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }
}
