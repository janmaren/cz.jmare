package org.maren.gis.etc;

import static java.lang.Math.toRadians;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.geotools.data.shapefile.files.ShpFiles;
import org.geotools.data.shapefile.shp.ShapefileException;
import org.geotools.data.shapefile.shp.ShapefileReader;
import org.geotools.data.shapefile.shp.ShapefileReader.Record;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiLineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.maren.gis.util.GonUtil;

public class Conv {

    public static void main(String[] args) throws ShapefileException, IOException {
        ShpFiles shps = new ShpFiles("file://C:\\Users\\jmarencik\\git\\cz.jmare\\cz.jmare.gisdemo\\src\\main\\resources\\Europe\\Europe.shp");
        
        double lat = toRadians(50.4161836);
        double lon = Math.toRadians(12.6227961);
        int i = 0;
        List<Integer> minDists = new ArrayList<>(); 
        try (ShapefileReader shapefileReader = new ShapefileReader(shps, true, true, new GeometryFactory())) {
            while (shapefileReader.hasNext()) {
                Record nextRecord = shapefileReader.nextRecord();
                Object shape = nextRecord.shape();
                if (shape instanceof MultiLineString) {
                    System.out.println("==================");
                    MultiLineString multiLineString = (MultiLineString) shape;
                    Coordinate[] coordinates = multiLineString.getCoordinates();
                    for (Coordinate coordinate : coordinates) {
                        System.out.print(coordinate);
                    }
                } else if (shape instanceof MultiPolygon) {
                    MultiPolygon multiPolygon = (MultiPolygon) shape;
                    System.out.println("-------------------");
                    Coordinate[] coordinates = multiPolygon.getCoordinates();
                    for (Coordinate coordinate : coordinates) {
                        double dist = GonUtil.angularDistanceRad(lon, lat, toRadians(coordinate.x), toRadians(coordinate.y));
                        if (Math.toDegrees(dist) < 1) {
                            minDists.add(i);
                        }
                    }
                    if (i == 6 || i == 11) {
                        System.out.println(i);
                        for (Coordinate coordinate : coordinates) {
                            System.out.print(coordinate);
                        }
                    }
                }
                i++;
            }
        }
        System.out.println(minDists);
    }

}
