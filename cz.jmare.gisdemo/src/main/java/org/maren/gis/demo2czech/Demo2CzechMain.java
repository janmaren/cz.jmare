package org.maren.gis.demo2czech;

public class Demo2CzechMain {
    public static void main(String[] args) {
        Demo2CzechApp app = new Demo2CzechApp();
        app.setBlockOnOpen(true);
        try {
            app.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
