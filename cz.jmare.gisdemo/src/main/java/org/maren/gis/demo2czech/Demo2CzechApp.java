package org.maren.gis.demo2czech;

import static java.lang.Math.toRadians;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.maren.gis.projection.OrthoEarthSphProjection;
import org.maren.gis.projection.SphProjection;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.CursorChangedListener;
import org.maren.gis.sphericcanvas.DrawListener;
import org.maren.gis.sphericcanvas.SphericCanvas;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;
import org.maren.gis.util.draw.BorderDraw;
import org.maren.gis.util.draw.SphericLinesDraw;

public class Demo2CzechApp extends ApplicationWindow implements DrawListener, CursorChangedListener {
    private Text coordsText;

    public Demo2CzechApp() {
        super(null);
    }

    @Override
    protected Control createContents(Composite parent) {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());
        
        SphProjection projection = new OrthoEarthSphProjection();
        SphericCanvas sphericCanvas = new SphericCanvas(composite, projection, this);
        sphericCanvas.setCursorChangedListener(this);
        sphericCanvas.setZoom(500);
        sphericCanvas.setSpheric0(new Spheric(toRadians(14.4156278), toRadians(50.0763203))); // prague
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
        sphericCanvas.setLayoutData(layoutData);
        
        coordsText = new Text(composite, SWT.BORDER);
        GridData cooLayoutData = new GridData();
        cooLayoutData.grabExcessHorizontalSpace = true;
        cooLayoutData.horizontalAlignment = SWT.FILL;
        cooLayoutData.heightHint = 100;
        coordsText.setLayoutData(cooLayoutData);
        
        return composite;
    }

    @Override
    protected void initializeBounds() {
        getShell().setText("Demo3Europe");
        getShell().setSize(1100, 750);
        getShell().setMinimumSize(900, 700);
    }

    @Override
    public void onCursorChanged(Spheric newCursorPosition, int stateMask) {
        if (newCursorPosition != null) {
            coordsText.setText(newCursorPosition.toString());
        }
    }

    @Override
    public void draw(GC gc, CoordState coordState) {
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        
        SphericLinesDraw.drawGreyDarkEarthLines(gc, coordState, true);
        BorderDraw.drawGreyDarkSphericBorder(gc, coordState);
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
        drawCountry(gc, coordState, CzechData.czech);
        
        gc.setFont(origFont);
    }

    protected void drawCountry(GC gc, CoordState coordState, double[] data) {
        PointInt lastPoint = null;
        for (int i = 0; i < data.length; i += 2) {
            PointInt coo = null;
            ScreenPoint point = coordState.toScreenCoordinates(new Spheric(toRadians(data[i]), toRadians(data[i + 1])));
            if (point.visibleHemis) {
                coo = point.point;
            } else {
                coo = null;
            }
            
            if (coo != null && lastPoint != null) {
                gc.drawLine(coo.x, coo.y, lastPoint.x, lastPoint.y);
            }
            
            lastPoint = coo;
        }
    }
}
