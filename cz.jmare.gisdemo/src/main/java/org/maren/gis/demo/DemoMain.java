package org.maren.gis.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.maren.gis.demo1earth.Demo1Prague;
import org.maren.gis.demo2czech.Demo2CzechMain;
import org.maren.gis.demo3europe.Demo3EuropeMain;

public class DemoMain {
    public static void main(String[] args) throws IOException {
        System.out.println("Select number of demo and press enter");
        System.out.println("1. Demo1Prague - showing city Prague on spheroid");
        System.out.println("2. Demo2CzechMain - showing border of Czech Republic - polyline");
        System.out.println("3. Demo3EuropeMain - showing borders of European countries - polyline - reading embedded shapefile");
        System.out.println("=============");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number = reader.readLine();
        if ("1".equals(number)) {
            Demo1Prague.main(args);
        } else
        if ("2".equals(number)) {
            Demo2CzechMain.main(args);
        } else
        if ("3".equals(number)) {
            Demo3EuropeMain.main(args);
        }
    }
}
