package org.maren.gis.demo3europe;

import java.io.IOException;

import org.geotools.data.shapefile.shp.ShapefileException;

public class Demo3EuropeMain {
    public static void main(String[] args) throws ShapefileException, IOException {
        Demo3Europe app = new Demo3Europe();
        app.setBlockOnOpen(true);
        try {
            app.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
