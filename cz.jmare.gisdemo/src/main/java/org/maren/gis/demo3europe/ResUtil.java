package org.maren.gis.demo3europe;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;

public class ResUtil {

    public static File getResourceFile(String name) {
        String classesPath = null;
        try {
            classesPath = URLDecoder.decode(Demo3EuropeMain.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        int indexOf = classesPath.indexOf("!");
        if (indexOf != -1) {
            classesPath = classesPath.substring(0, indexOf);
        }
        if (classesPath.endsWith(".jar")) {
            classesPath = classesPath.substring(0, classesPath.lastIndexOf("/"));
            URI uri = null;
            try {
                uri = new URI(classesPath);
            } catch (URISyntaxException e) {
                throw new IllegalStateException("Bad uri " + classesPath);
            }
            File file = new File(uri);
            return new File(file, "classes/" + name);
        } else {
            URL resource = Demo3EuropeMain.class.getResource("/" + name);
            try {
                return new File(resource.toURI());
            } catch (URISyntaxException e) {
                throw new IllegalStateException("Unable to load " + name, e);
            }
        }
    }

}
