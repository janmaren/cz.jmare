package org.maren.gis.demo3europe;

public class CountryEntity {
    private String name;

    public CountryEntity(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
