package org.maren.gis.demo3europe;

import static java.lang.Math.toRadians;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.geotools.data.shapefile.dbf.DbaseFileReader;
import org.geotools.data.shapefile.files.ShpFiles;
import org.geotools.data.shapefile.shp.ShapefileException;
import org.geotools.data.shapefile.shp.ShapefileReader;
import org.geotools.data.shapefile.shp.ShapefileReader.Record;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.maren.gis.projection.OrthoEarthSphProjection;
import org.maren.gis.projection.SphProjection;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.sphericcanvas.CursorChangedListener;
import org.maren.gis.sphericcanvas.DrawListener;
import org.maren.gis.sphericcanvas.SphericCanvas;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;
import org.maren.gis.util.draw.BorderDraw;
import org.maren.gis.util.draw.SphericLinesDraw;

public class Demo3Europe extends ApplicationWindow implements DrawListener, CursorChangedListener {
    private Text coordsText;
    
    private List<MultiPolygon> multiPolygons;
    private List<CountryEntity> countryEntities;
    
    public Demo3Europe() throws ShapefileException, IOException {
        super(null);
        
        ShpFiles shps = new ShpFiles(ResUtil.getResourceFile("Europe/Europe.shp"));
        multiPolygons = new ArrayList<>();
        try (ShapefileReader shapefileReader = new ShapefileReader(shps, true, true, new GeometryFactory())) {
            while (shapefileReader.hasNext()) {
                Record nextRecord = shapefileReader.nextRecord();
                Object shape = nextRecord.shape();
                if (shape instanceof MultiPolygon) {
                    MultiPolygon multiPolygon = (MultiPolygon) shape;
                    multiPolygons.add(multiPolygon);
                }
            }
        }

        countryEntities = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(ResUtil.getResourceFile("Europe/Europe.dbf"));
                DbaseFileReader dbfReader = new DbaseFileReader(fis.getChannel(), false,
                        Charset.forName("ISO-8859-1"))) {
            while (dbfReader.hasNext()) {
                final Object[] fields = dbfReader.readEntry();
                CountryEntity countryEntity = new CountryEntity((String) fields[0]);
                countryEntities.add(countryEntity);
            }
        }
    }

    @Override
    protected Control createContents(Composite parent) {
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        fontRegistry.put("smallest-font", new FontData[]{new FontData("smaller-font", 4, SWT.NORMAL)});
        fontRegistry.put("smaller-font", new FontData[]{new FontData("smaller-font", 7, SWT.NORMAL)});

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());
        
        SphProjection projection = new OrthoEarthSphProjection();
        SphericCanvas sphericCanvas = new SphericCanvas(composite, projection, this);
        sphericCanvas.setCursorChangedListener(this);
        sphericCanvas.setZoom(500);
        sphericCanvas.setSpheric0(new Spheric(toRadians(14.4156278), toRadians(50.0763203))); // prague
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
        sphericCanvas.setLayoutData(layoutData);
        
        coordsText = new Text(composite, SWT.BORDER);
        GridData cooLayoutData = new GridData();
        cooLayoutData.grabExcessHorizontalSpace = true;
        cooLayoutData.horizontalAlignment = SWT.FILL;
        cooLayoutData.heightHint = 100;
        coordsText.setLayoutData(cooLayoutData);
        
        return composite;
    }

    @Override
    protected void initializeBounds() {
        getShell().setText("Demo3Europe");
        getShell().setSize(1100, 750);
        getShell().setMinimumSize(900, 700);
    }

    @Override
    public void onCursorChanged(Spheric newCursorPosition, int stateMask) {
        if (newCursorPosition != null) {
            coordsText.setText(newCursorPosition.toString());
        }
    }

    @Override
    public void draw(GC gc, CoordState coordState) {
        Font origFont = gc.getFont();
        FontRegistry fontRegistry = JFaceResources.getFontRegistry();
        gc.setFont(fontRegistry.get(coordState.getZoom() < 200 ? "smallest-font" : "smaller-font"));
        
        SphericLinesDraw.drawGreyDarkEarthLines(gc, coordState, true);
        BorderDraw.drawGreyDarkSphericBorder(gc, coordState);
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
        
        for (int i = 0; i < multiPolygons.size(); i++) {
            MultiPolygon multiPolygon = multiPolygons.get(i);
            int numGeometries = multiPolygon.getNumGeometries();
            for (int g = 0; g < numGeometries; g++) {
                Geometry geometry = multiPolygon.getGeometryN(g);
                if (geometry instanceof org.locationtech.jts.geom.Polygon) {
                    org.locationtech.jts.geom.Polygon polygon = (org.locationtech.jts.geom.Polygon) geometry;
                    LinearRing exteriorRing = polygon.getExteriorRing(); // simplification: ignoring holes
                    drawCountry(gc, coordState, exteriorRing.getCoordinates());
                }
            }
            if (coordState.getZoom() > 2000) {
                Point centroid = multiPolygon.getCentroid();
                ScreenPoint point = coordState.toScreenCoordinates(new Spheric(toRadians(centroid.getX()), toRadians(centroid.getY())));
                if (point.visibleHemis) {
                    gc.drawString(countryEntities.get(i).getName(), point.point.x, point.point.y);
                }
            }
        }
        
        gc.setFont(origFont);
    }
    
    protected void drawCountry(GC gc, CoordState coordState, Coordinate[] coordinates) {
        PointInt lastPoint = null;
        for (int i = 0; i < coordinates.length; i++) {
            PointInt coo = null;
            ScreenPoint point = coordState.toScreenCoordinates(new Spheric(toRadians(coordinates[i].x), toRadians(coordinates[i].y)));
            if (point.visibleHemis) {
                coo = point.point;
            } else {
                coo = null;
            }
            
            if (coo != null && lastPoint != null) {
                gc.drawLine(coo.x, coo.y, lastPoint.x, lastPoint.y);
            }
            lastPoint = coo;
        }
    }
}
