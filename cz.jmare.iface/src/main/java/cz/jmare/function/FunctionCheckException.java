package cz.jmare.function;

public interface FunctionCheckException<T, R> {
    R apply(T t) throws Exception;
}
