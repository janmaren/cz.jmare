package cz.jmare.function;

public interface RunnableCheckException {
    void run() throws Exception;
}
