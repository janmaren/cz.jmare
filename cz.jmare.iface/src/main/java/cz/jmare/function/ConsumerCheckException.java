package cz.jmare.function;

public interface ConsumerCheckException<T> {
    void accept(T elem) throws Exception;
}
