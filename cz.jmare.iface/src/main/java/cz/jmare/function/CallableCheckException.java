package cz.jmare.function;

public interface CallableCheckException<V> {
    V call() throws Exception;
}
