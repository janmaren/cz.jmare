package cz.jmare.progress;

public class NullIntProgressStatus implements ProgressStatus<Integer> {

    @Override
    public void setProgressValue(Integer progress) {
    }

    @Override
    public void setMaxValue(Integer max) {
    }

}
