package cz.jmare.progress;

import java.util.concurrent.atomic.AtomicLong;

public class SimpleLongProgressStatus implements ProgressStatus<Long> {
    private AtomicLong progressValue = new AtomicLong(-1L);
    private AtomicLong maxValue = new AtomicLong(-1L);

    @Override
    public void setProgressValue(Long progressValue) {
        this.progressValue.set(progressValue);
    }

    @Override
    public void setMaxValue(Long maxValue) {
        this.progressValue.set(0);
        this.maxValue.set(maxValue);
    }

    public long getProgressValue() {
        return progressValue.get();
    }

    public long getMaxValue() {
        return maxValue.get();
    }
}
