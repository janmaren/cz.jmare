package cz.jmare.progress;

public interface ReadableProgressStatus<T extends Number> extends ProgressStatus<T> {
    /**
     * -1 means not started run
     * @return
     */
    T getProgressValue();

    /**
     * -1 means not started run. 0 doesn't make sense
     * @return
     */
    T getMaxValue();
}
