package cz.jmare.progress;

import java.util.concurrent.atomic.AtomicInteger;


public class SimpleIntProgressStatus implements ProgressStatus<Integer> {
    private AtomicInteger progressValue = new AtomicInteger(-1);
    private AtomicInteger maxValue = new AtomicInteger(-1);

    @Override
    public void setProgressValue(Integer progressValue) {
       this.progressValue.set(progressValue);
    }

    @Override
    public void setMaxValue(Integer maxValue) {
       this.maxValue.set(maxValue);
    }

    public long getProgressValue() {
       return progressValue.get();
    }

    public long getMaxValue() {
       return maxValue.get();
    }
}
