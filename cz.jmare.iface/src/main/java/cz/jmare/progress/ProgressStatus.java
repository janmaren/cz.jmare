package cz.jmare.progress;

public interface ProgressStatus<T extends Number> {
    /**
     * Progress between 0 and (max - 1). Note: (max - 1) is 100%
     * @param progress
     */
    default void setProgressValue(T progress) {

    }

    /**
     * Typically as size or length of a collection. Note: the progressValue should be set to zero in this method
     * @param max
     */
    default void setMaxValue(T max) {

    }
}
