package cz.jmare.progress;

public class NullLongProgressStatus implements ProgressStatus<Long> {

    @Override
    public void setProgressValue(Long progress) {
    }

    @Override
    public void setMaxValue(Long max) {
    }

}
