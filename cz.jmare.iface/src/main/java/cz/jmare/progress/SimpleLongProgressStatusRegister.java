package cz.jmare.progress;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SimpleLongProgressStatusRegister {
    private static Set<WeakReference<SimpleLongReadableProgressStatus>> registeredObjects = new HashSet<>();

    public void register(SimpleLongReadableProgressStatus object) {
        registeredObjects.add(new WeakReference<>(object));
    }

    public static void addSimpleLongProgressStatus(SimpleLongReadableProgressStatus simpleLongProgressStatus) {
        registeredObjects.add(new WeakReference<SimpleLongReadableProgressStatus>(simpleLongProgressStatus));
    }

    public static void interruptAllOperations() {
        for (Iterator<WeakReference<SimpleLongReadableProgressStatus>> iterator = registeredObjects.iterator(); iterator.hasNext();) {
            WeakReference<SimpleLongReadableProgressStatus> weakReference = iterator.next();
            SimpleLongReadableProgressStatus simpleLongReadableProgressStatus = weakReference.get();
            if (simpleLongReadableProgressStatus == null) {
                iterator.remove();
            } else {
                simpleLongReadableProgressStatus.interruptOperation();
            }
        }
    }
}
