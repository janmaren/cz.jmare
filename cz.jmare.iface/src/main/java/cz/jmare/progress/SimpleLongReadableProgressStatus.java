package cz.jmare.progress;

import java.util.concurrent.atomic.AtomicLong;

public class SimpleLongReadableProgressStatus implements ReadableProgressStatus<Long> {
    private AtomicLong progress = new AtomicLong(-1L);
    private AtomicLong max = new AtomicLong(-1L);
    private volatile boolean operationInterrupted;

    public SimpleLongReadableProgressStatus() {
        SimpleLongProgressStatusRegister.addSimpleLongProgressStatus(this);
    }

    @Override
    public void setProgressValue(Long progress) {
        if (operationInterrupted) {
            throw new OperationInterruptedException();
        }
        this.progress.set(progress);
    }

    @Override
    public void setMaxValue(Long max) {
        this.progress.set(0);
        this.max.set(max);
    }

    @Override
    public Long getProgressValue() {
        return progress.get();
    }

    @Override
    public Long getMaxValue() {
        return max.get();
    }

    public void interruptOperation() {
        this.operationInterrupted = true;
    }
}
