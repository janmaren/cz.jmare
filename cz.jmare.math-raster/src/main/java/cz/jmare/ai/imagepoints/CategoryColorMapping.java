package cz.jmare.ai.imagepoints;

import java.util.List;

public interface CategoryColorMapping extends CategoryColorMapper, ImagePointsMapping {
    List<CategoryAndLabel> getCategoryLabels();
}
