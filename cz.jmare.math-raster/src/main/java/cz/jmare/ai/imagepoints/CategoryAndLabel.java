package cz.jmare.ai.imagepoints;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

public class CategoryAndLabel {
    public int category;

    public String label;

    public RGBEntity rgb;

    public CategoryAndLabel(int category, String label, RGBEntity rgb) {
        super();
        this.category = category;
        this.label = label;
        this.rgb = rgb;
    }

    public CategoryAndLabel(RGBEntity rgb) {
        super();
        this.category = RGBEntUtil.rgbToRHighest(rgb);
        this.label = RGBEntUtil.toRGBHexString(rgb);
        this.rgb = rgb;
    }
}
