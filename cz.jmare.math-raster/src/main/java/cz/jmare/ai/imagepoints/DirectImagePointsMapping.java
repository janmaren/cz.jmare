package cz.jmare.ai.imagepoints;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import cz.jmare.math.raster.entity.AreaCategories;
import cz.jmare.math.raster.entity.PixelCat;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;

public class DirectImagePointsMapping implements CategoryColorMapping {

    @Override
    public RGBEntity toRGB(int category) {
        return RGBEntUtil.rHighestToRGB(category);
    }

    @Override
    public AreaCategories imageToCategories(BufferedImage bufferedImage) {
        HashSet<PixelCat> points = new HashSet<>();
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                int extractAlpha = RGBEntUtil.extractAlpha(rgb);
                if (extractAlpha > 0) {
                    PixelCat pixelCat = new PixelCat(j, height - i - 1, rgb);
                    points.add(pixelCat);
                }
            }
        }
        AreaCategories pixelsCategories = new AreaCategories(points, width, height);
        return pixelsCategories;
    }

    @Override
    public BufferedImage categoriesToImage(RemappableArea areaCategories, Integer alphaCategory) {
        int width = areaCategories.getWidth();
        int height = areaCategories.getHeight();
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        int transparentWhite = RGBEntUtil.rHighestAlphaToRGBInt(255, 255, 255, 0);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                bufferedImage.setRGB(j, i, transparentWhite);
            }
        }
        Iterator<PixelCat> iterator = areaCategories.iterator();
        while (iterator.hasNext()) {
            PixelCat pixelCat = iterator.next();
            int rgb = pixelCat.category;
            int rgbInt;
            if (alphaCategory != null && rgb == alphaCategory) {
                rgbInt = RGBEntUtil.setAlpha(rgb, 0);
            } else {
                rgbInt = RGBEntUtil.setAlpha(rgb, 255);
            }
            bufferedImage.setRGB(pixelCat.x, height - pixelCat.y - 1, rgbInt);
        }
        return bufferedImage;
    }

    @Override
    public void addCategoryCorrection(RGBEntity rgb, int category) {
    }

    @Override
    public void fromProperties(Properties properties) {
    }

    @Override
    public void toProperties(Properties properties) {
    }

    @Override
    public List<CategoryAndLabel> getCategoryLabels() {
        return new ArrayList<CategoryAndLabel>();
    }
}
