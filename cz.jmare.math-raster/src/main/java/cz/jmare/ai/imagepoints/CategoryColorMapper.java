package cz.jmare.ai.imagepoints;

import cz.jmare.math.raster.entity.RGBEntity;

public interface CategoryColorMapper {
    RGBEntity toRGB(int category);
}
