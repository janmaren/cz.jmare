package cz.jmare.ai.imagepoints;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import cz.jmare.math.raster.entity.RGBEntity;

public class CategoryColorMappingFactory {
    public static CategoryColorMapping fromProperties(Properties properties) {
        String mappingClass = properties.getProperty("categoryColorMapping.class");
        if (mappingClass == null) {
            mappingClass = properties.getProperty("mappingClass");
            if (mappingClass != null) {
                System.err.println("Please use categoryColorMapping.class instead of mappingClass in properties");
            }
        }
        if (mappingClass != null) {
            try {
                Class<?> clazz = Class.forName(mappingClass);
                Constructor<?> declaredConstructor = clazz.getDeclaredConstructor();
                CategoryColorMapping newInstance = (CategoryColorMapping) declaredConstructor.newInstance();
                if (newInstance instanceof CatImagePointsMapping) {
                    CatImagePointsMapping catImagePointsMapping = (CatImagePointsMapping) newInstance;
                    String property = properties.getProperty("categoryColorMapping.alphaCategory");
                    if (property != null) {
                        catImagePointsMapping.setAlphaCategory(Integer.valueOf(property));
                    }
                    property = properties.getProperty("categoryColorMapping.alphaColor");
                    if (property != null) {
                        catImagePointsMapping.setAlphaColor(new RGBEntity(property));
                    }
                }
                newInstance.fromProperties(properties);
                return newInstance;
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalStateException("Unable to load color mapping", e);
            }
        } else {
            throw new IllegalStateException("No color mapping present in properties");
        }
    }

    public static void toProperties(CategoryColorMapping categoryColorMapping, Properties properties) {
        categoryColorMapping.toProperties(properties);
    }
}
