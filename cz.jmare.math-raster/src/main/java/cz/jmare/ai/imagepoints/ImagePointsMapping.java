package cz.jmare.ai.imagepoints;

import java.awt.image.BufferedImage;
import java.util.Properties;

import cz.jmare.math.raster.entity.AreaCategories;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;

public interface ImagePointsMapping {
    AreaCategories imageToCategories(BufferedImage bufferedImage);

    /**
     *
     * @param areaCategories
     * @param alphaCategory when set it's a category for which the alpha should be 0. Nullable
     * @return
     */
    BufferedImage categoriesToImage(RemappableArea areaCategories, Integer alphaCategory);

    void addCategoryCorrection(RGBEntity rgb, int category);

    void fromProperties(Properties properties);

    void toProperties(Properties properties);
}
