package cz.jmare.ai.imagepoints;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import cz.jmare.ai.processor.ConvertCategoryUtil;
import cz.jmare.ai.processor.UnknownRGBException;
import cz.jmare.math.raster.entity.AreaCategories;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;

public class CatImagePointsMapping implements CategoryColorMapping {
    private static LinkedHashMap<Integer, RGBEntity> DEFAULT_CAT_TO_RGB = new LinkedHashMap<Integer, RGBEntity>();
    static {
        DEFAULT_CAT_TO_RGB.put(0, new RGBEntity(255, 0, 0));
        DEFAULT_CAT_TO_RGB.put(1, new RGBEntity("#ffff00"));
        DEFAULT_CAT_TO_RGB.put(2, new RGBEntity("#000090"));
        DEFAULT_CAT_TO_RGB.put(3, new RGBEntity("#00ff00"));
        DEFAULT_CAT_TO_RGB.put(4, new RGBEntity("#ff00ff"));
        DEFAULT_CAT_TO_RGB.put(5, new RGBEntity("#ffe4c4"));
        DEFAULT_CAT_TO_RGB.put(6, new RGBEntity("#00bfff"));
        DEFAULT_CAT_TO_RGB.put(7, new RGBEntity("#b03060"));
        DEFAULT_CAT_TO_RGB.put(8, new RGBEntity("#008000"));
        DEFAULT_CAT_TO_RGB.put(9, new RGBEntity("#7c3f1d"));
        DEFAULT_CAT_TO_RGB.put(Integer.MAX_VALUE, new RGBEntity(192, 192, 192));
    }

    private Map<Integer, RGBEntity> catToRGB = DEFAULT_CAT_TO_RGB;
    private Map<RGBEntity, Integer> RGBToCat = CatImagePointsMapping.getInvertedMap(getCatToRGB());
    private Map<RGBEntity, Integer> correctionsRGBToCat = new HashMap<RGBEntity, Integer>();
    private Integer alphaCategory;
    private RGBEntity alphaColor;

    public CatImagePointsMapping(Properties properties) {
        super();
        fromProperties(properties);
    }

    public CatImagePointsMapping() {
    }

    @Override
    public AreaCategories imageToCategories(BufferedImage bufferedImage) {
        try {
            return ConvertCategoryUtil.imageToAreaCategories(bufferedImage, RGBToCat);
        } catch (UnknownRGBException e) {
            List<Integer> cats = new ArrayList<>(getCatToRGB().keySet());
            Collections.sort(cats);
            e.setSupportedCategories(cats);
            throw e;
        }
    }

    @Override
    public BufferedImage categoriesToImage(RemappableArea areaCategories, Integer alphaCategory) {
        BufferedImage bufferedImage = ConvertCategoryUtil.toBufferedImage(areaCategories, getCatToRGB(), alphaCategory, alphaColor);
        return bufferedImage;
    }

    @Override
    public void addCategoryCorrection(RGBEntity rgb, int category) {
        RGBToCat.put(rgb, category);
        correctionsRGBToCat.put(rgb, category);
    }

    private static LinkedHashMap<Integer, RGBEntity> loadCategoryProperties(Properties properties) {
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        Map<Integer, RGBEntity> mapUnsorted = new HashMap<Integer, RGBEntity>();
        for (Entry<Object, Object> entry : entrySet) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.startsWith("category.")) {
                String catStr = key.substring("category.".length());
                int category = Integer.parseInt(catStr);
                RGBEntity rgb = new RGBEntity(value);
                mapUnsorted.put(category, rgb);
            }
        }
        List<Integer> keys = new ArrayList<>(mapUnsorted.keySet());
        Collections.sort(keys);
        LinkedHashMap<Integer, RGBEntity> mapSorted = new LinkedHashMap<Integer, RGBEntity>();
        for (Integer key : keys) {
            mapSorted.put(key, mapUnsorted.get(key));
        }
        return mapSorted;
    }

    @Override
    public RGBEntity toRGB(int category) {
        return getCatToRGB().get(category);
    }

    public Map<Integer, RGBEntity> getCatToRGB() {
        return catToRGB;
    }

    @Override
    public void fromProperties(Properties properties) {
        catToRGB = loadCategoryProperties(properties);
        RGBToCat = CatImagePointsMapping.getInvertedMap(getCatToRGB());

        correctionsRGBToCat = new HashMap<RGBEntity, Integer>();
        Set<Entry<Object, Object>> entrySet = properties.entrySet();
        for (Entry<Object, Object> entry : entrySet) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.startsWith("fix.")) {
                String rgbStr = key.substring("fix.".length());
                String rgbS = rgbStr.replace(".", ",");
                RGBEntity rgb = new RGBEntity(rgbS);
                int category = Integer.parseInt(value);
                RGBToCat.put(rgb, category);
                correctionsRGBToCat.put(rgb, category);
            }
        }

        if (properties.getProperty("categoryColorMapping.alphaCategory") != null) {
            this.alphaCategory = Integer.valueOf(properties.getProperty("categoryColorMapping.alphaCategory"));
        }
        if (properties.getProperty("categoryColorMapping.alphaColor") != null) {
            this.alphaColor = new RGBEntity(properties.getProperty("categoryColorMapping.alphaColor"));
        }
    }

    @Override
    public void toProperties(Properties properties) {
        for (Entry<Integer, RGBEntity> entry : catToRGB.entrySet()) {
            Integer key = entry.getKey();
            RGBEntity value = entry.getValue();
            properties.put("category." + key, RGBEntUtil.toRGBNumbersString(value));
        }
        for (Entry<RGBEntity, Integer> entry : correctionsRGBToCat.entrySet()) {
            RGBEntity rgb = entry.getKey();
            Integer category = entry.getValue();
            properties.put("fix." + rgb.red + "." + rgb.green + "." + rgb.blue, category.toString());
        }
        if (alphaCategory != null) {
            properties.put("categoryColorMapping.alphaCategory", String.valueOf(alphaCategory));
        }
        if (alphaColor != null) {
            properties.put("categoryColorMapping.alphaColor", RGBEntUtil.toRGBNumbersString(alphaColor));
        }
    }

    @Override
    public List<CategoryAndLabel> getCategoryLabels() {
        List<CategoryAndLabel> list = catToRGB.entrySet().stream().map(e -> new CategoryAndLabel(e.getKey(), e.getKey().toString(), e.getValue())).collect(Collectors.toList());
        return list;
    }

    public void setAlphaCategory(Integer alphaCategory) {
        this.alphaCategory = alphaCategory;
    }

    public void setAlphaColor(RGBEntity alphaColor) {
        this.alphaColor = alphaColor;
    }

    public static Map<RGBEntity, Integer> getInvertedMap(Map<Integer, RGBEntity> catToRGB) {
        Map<RGBEntity, Integer> rgbToCat = new HashMap<>();
        for (Entry<Integer, RGBEntity> entry : catToRGB.entrySet()) {
            Integer key = entry.getKey();
            RGBEntity value = entry.getValue();
            rgbToCat.put(value, key);
        }
        return rgbToCat;
    }
}
