package cz.jmare.ai.processor;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import cz.jmare.math.raster.entity.AllAreaCategories;
import cz.jmare.math.raster.entity.AreaCategories;
import cz.jmare.math.raster.entity.PixelCat;
import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;

public class ConvertCategoryUtil {
    public static AreaCategories imageToAreaCategories(BufferedImage bufferedImage, Map<RGBEntity, Integer> rgbToCat) throws UnknownRGBException {
        HashSet<PixelCat> points = new HashSet<>();
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                int extractAlpha = RGBEntUtil.extractAlpha(rgb);
                if (extractAlpha > 0) {
                    RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                    Integer cat = rgbToCat.get(rHighestToRGB);
                    if (cat == null) {
                        throw new UnknownRGBException("Not found category for color " + rHighestToRGB.red + ", " + rHighestToRGB.green + ", " + rHighestToRGB.blue, rHighestToRGB);
                    }
                    PixelCat pixelCat = new PixelCat(j, height - i - 1, cat);
                    points.add(pixelCat);
                }
            }
        }
        AreaCategories pixelsCategories = new AreaCategories(points, width, height);
        return pixelsCategories;
    }

    public static AllAreaCategories imageToAllAreaCategories(BufferedImage bufferedImage, Map<RGBEntity, Integer> rgbToCat) throws UnknownRGBException {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        AllAreaCategories pixelsCategories = new AllAreaCategories(width, height);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                int extractAlpha = RGBEntUtil.extractAlpha(rgb);
                if (extractAlpha > 0) {
                    RGBEntity rHighestToRGB = RGBEntUtil.rHighestToRGB(rgb);
                    Integer cat = rgbToCat.get(rHighestToRGB);
                    if (cat == null) {
                        throw new UnknownRGBException("Not found category for color " + rHighestToRGB.red + ", " + rHighestToRGB.green + ", " + rHighestToRGB.blue, rHighestToRGB);
                    }
                    pixelsCategories.setCategory(j, height - i - 1, cat);
                }
            }
        }
        return pixelsCategories;
    }

    public static BufferedImage toBufferedImage(RemappableArea areaCategories, Map<Integer, RGBEntity> catToRGB, Integer alphaCategory, RGBEntity alphaColor) throws UnknownCategoryException {
        int width = areaCategories.getWidth();
        int height = areaCategories.getHeight();
        Integer alphaColorInt = alphaColor != null ? RGBEntUtil.rgbToRHighest(alphaColor, 0) : null;

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        int transparentWhite = RGBEntUtil.rHighestAlphaToRGBInt(255, 255, 255, 0);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                bufferedImage.setRGB(j, i, transparentWhite);
            }
        }
        Iterator<PixelCat> iterator = areaCategories.iterator();
        while (iterator.hasNext()) {
            PixelCat pixelCat = iterator.next();
            int red = 0;
            int green = 0;
            int blue = 0;
            RGBEntity rgb = catToRGB.get(pixelCat.category);
            if (rgb == null) {
                throw new UnknownCategoryException("Not found category " + pixelCat.category + " to generate result", pixelCat.category);
            } else {
                red = rgb.red;
                green = rgb.green;
                blue = rgb.blue;
            }
            int rgbInt;
            if (alphaCategory != null && pixelCat.category == alphaCategory) {
                if (alphaColorInt != null) {
                    rgbInt = alphaColorInt;
                } else {
                    rgbInt = RGBEntUtil.rHighestAlphaToRGBInt(red, green, blue, 0);
                }
            } else {
                rgbInt = RGBEntUtil.rHighestAlphaToRGBInt(red, green, blue, 255);
            }
            bufferedImage.setRGB(pixelCat.x, height - pixelCat.y - 1, rgbInt);
        }
        return bufferedImage;
    }
}
