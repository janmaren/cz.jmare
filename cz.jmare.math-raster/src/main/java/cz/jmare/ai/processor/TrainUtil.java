package cz.jmare.ai.processor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;

public class TrainUtil {
    public static List<BaseAndSelection> of(Map<RGBEntity, Integer> rgbToCat, String... basesAndSelections) {
        if (basesAndSelections.length % 2 != 0) {
            throw new IllegalArgumentException("Not even number of files");
        }

        List<BaseAndSelection> baseAndSelections = new ArrayList<BaseAndSelection>();
        int pos = 0;
        while (pos < basesAndSelections.length) {
            BufferedImage baseimage;
            try {
                baseimage = ImageIO.read(new File(basesAndSelections[pos++]));
            } catch (IOException e) {
                throw new IllegalStateException("Unable to load file", e);
            }
            BufferedImage selectionImage;
            try {
                selectionImage = ImageIO.read(new File(basesAndSelections[pos++]));
            } catch (IOException e) {
                throw new IllegalStateException("Unable to load file", e);
            }

            AreaPixels pixelsRGBs = new AreaPixels(baseimage);
            BaseAndSelection baseAndSelection = new BaseAndSelection();
            baseAndSelection.imageBase = pixelsRGBs;
            RemappableArea pixelsCategories = ConvertCategoryUtil.imageToAreaCategories(selectionImage, rgbToCat);
            baseAndSelection.imageCateg = pixelsCategories;

            baseAndSelections.add(baseAndSelection);
        }
        return baseAndSelections;
    }

    public static List<BaseAndSelection> of(Map<RGBEntity, Integer> rgbToCat, InputStream... basesAndSelections) {
        if (basesAndSelections.length % 2 != 0) {
            throw new IllegalArgumentException("Not even number of files");
        }

        List<BaseAndSelection> baseAndSelections = new ArrayList<BaseAndSelection>();
        int pos = 0;
        while (pos < basesAndSelections.length) {
            BufferedImage baseimage;
            try {
                baseimage = ImageIO.read(basesAndSelections[pos++]);
            } catch (IOException e) {
                throw new IllegalStateException("Unable to load file", e);
            }
            BufferedImage selectionImage;
            try {
                selectionImage = ImageIO.read(basesAndSelections[pos++]);
            } catch (IOException e) {
                throw new IllegalStateException("Unable to load file", e);
            }

            AreaPixels pixelsRGBs = new AreaPixels(baseimage);
            BaseAndSelection baseAndSelection = new BaseAndSelection();
            baseAndSelection.imageBase = pixelsRGBs;
            RemappableArea pixelsCategories = ConvertCategoryUtil.imageToAreaCategories(selectionImage, rgbToCat);
            baseAndSelection.imageCateg = pixelsCategories;

            baseAndSelections.add(baseAndSelection);
        }
        return baseAndSelections;
    }
}
