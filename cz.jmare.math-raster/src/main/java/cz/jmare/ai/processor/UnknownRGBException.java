package cz.jmare.ai.processor;

import java.util.List;

import cz.jmare.math.raster.entity.RGBEntity;

public class UnknownRGBException extends RuntimeException {
    private static final long serialVersionUID = 5401104523854488488L;

    private RGBEntity rgb;

    private List<Integer> supportedCategories;

    public UnknownRGBException(String message, RGBEntity rgb) {
        super(message);
        this.rgb = rgb;
    }

    public UnknownRGBException(String message, RGBEntity rgb, List<Integer> usedCategories) {
        super(message);
        this.rgb = rgb;
        this.setSupportedCategories(usedCategories);
    }

    public RGBEntity getRgb() {
        return rgb;
    }

    public List<Integer> getSupportedCategories() {
        return supportedCategories;
    }

    public void setSupportedCategories(List<Integer> supportedCategories) {
        this.supportedCategories = supportedCategories;
    }
}
