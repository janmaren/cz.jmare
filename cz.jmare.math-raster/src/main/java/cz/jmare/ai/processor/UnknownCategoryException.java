package cz.jmare.ai.processor;

public class UnknownCategoryException extends RuntimeException {
    private static final long serialVersionUID = -8916006329543598145L;

    private Integer category;

    public UnknownCategoryException(String message, Integer category) {
        super(message);
        this.category = category;
    }

    public Integer getCategory() {
        return category;
    }
}
