package cz.jmare.ai.processor;

import java.util.ArrayList;
import java.util.List;

public class StatisHistogram {
    private double from;

    private double step;

    private double toExc;

    private long[] counts;

    private long countUnder;

    private long countAbove;

    public StatisHistogram(double from, double toExc, double step) {
        super();
        this.from = from;
        this.toExc = toExc;
        this.step = step;
        counts = new long[(int) ((toExc - from) / step)];
    }

    public void addValue(double value) {
        if (value < from) {
            countUnder = getCountUnder() + 1;
            return;
        }
        if (value >= toExc) {
            countAbove = getCountAbove() + 1;
            return;
        }
        counts[(int) ((value - from) / step)]++;
    }

    public void addValues(StatisHistogram statisHistogram) {
        countUnder += statisHistogram.countUnder;
        countAbove += statisHistogram.countAbove;
        for (int i = 0; i < counts.length; i++) {
            counts[i] += statisHistogram.counts[i];
        }
    }

    public void addValues(StatisHistogram... statisHistograms) {
        for (int i = 0; i < statisHistograms.length; i++) {
            addValues(statisHistograms[i]);
        }
    }

    public List<Range> getRanges() {
        List<Range> ranges = new ArrayList<Range>();
        for (int i = 0; i < counts.length; i++) {
            long count = counts[i];
            ranges.add(new Range(i * step + from, count));
        }
        return ranges;
    }

    public long getCountUnder() {
        return countUnder;
    }

    public long getCountAbove() {
        return countAbove;
    }

    @Override
    public String toString() {
        return getRanges() + "[<" + from + ": " + countUnder + " and >=" + toExc + ": " + countAbove + "]";
    }

    public static class Range {
        public double from;

        public long count;

        public Range(double from, long count) {
            super();
            this.from = from;
            this.count = count;
        }

        @Override
        public String toString() {
            return "Range [from=" + from + ", count=" + count + "]";
        }
    }

    public static void main(String[] args) {
        StatisHistogram h = new StatisHistogram(0, 1, 0.1);
        h.addValue(0.1);
        h.addValue(0.23);
        h.addValue(0.4);
        h.addValue(0.51);
        h.addValue(0.52);
        h.addValue(0.6);
        h.addValue(0.7);
        h.addValue(0.8);
        h.addValue(0.81);
        h.addValue(0.8);
        h.addValue(1);

        System.out.println(h);
    }
}
