package cz.jmare.ai.processor;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cz.jmare.ai.LearningState;
import cz.jmare.ai.resolver.AIDataResolver;
import cz.jmare.aid.DAISolver;
import cz.jmare.math.raster.entity.AllAreaCategories;
import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class NeuralRasterParallelResult {
    private static int divisions = Runtime.getRuntime().availableProcessors() - 1;
    static {
        if (divisions < 1) {
            divisions = 1;
        }
    }
    private static ExecutorService executorService = Executors.newFixedThreadPool(divisions);

    private static AllAreaCategories mergeAreaCategories(AllAreaCategories... areaCategories) {
        int height = 0;
        int width = 0;
        for (AllAreaCategories allAreaCategories : areaCategories) {
            height += allAreaCategories.getHeight();
            width = allAreaCategories.getWidth();
        }
        if (areaCategories.length == 0) {
            throw new IllegalArgumentException("No AreaCategories provided");
        }
        AllAreaCategories areaCategoriesRes = new AllAreaCategories(width, height);
        int lines = 0;
        for (int i = 0; i < areaCategories.length; i++) {
            AllAreaCategories allAreaCategories = areaCategories[i];
            for (int j = 0; j < allAreaCategories.getHeight(); j++) {
                for (int k = 0; k < allAreaCategories.getWidth(); k++) {
                    areaCategoriesRes.setCategory(k, lines + j, allAreaCategories.getCategory(k, j));
                }
            }
            lines += allAreaCategories.getHeight();
        }

        return areaCategoriesRes;
    }

    public static synchronized AllAreaCategories calculateResult(LearningState learningState, AIDataResolver aiDataResolver, AreaPixels imageBase, ProgressStatus<Long> progressStatus) {
        int height = imageBase.getHeight();
        int divs = divisions;
        if (height < divs) {
            divs = height;
        }
        int linesCount = (int) Math.ceil((double)height / divs);
        while ((divs - 1) * linesCount >= height) {
            divs--;
        }
        @SuppressWarnings("unchecked")
        Future<AllAreaCategories>[] futures = new Future[divs];

        for (int i = 0; i < divs; i++) {
            int from = i * linesCount;
            int to = (i + 1) * linesCount;
            if (to > height) {
                to = height;
            }
            int toFinal = to;
            int iFinal = i;
            futures[i] = executorService.submit(() -> {
                ProgressStatus<Long> useProgressStatus = null;
                if (iFinal == 0) {
                    useProgressStatus = progressStatus;
                } else {
                    useProgressStatus = new NullLongProgressStatus();
                }
                AllAreaCategories areaCategories = NeuralRasterSolver.calculateResult(new DAISolver(learningState),
                        aiDataResolver, Integer.MAX_VALUE, imageBase, from, toFinal, null, useProgressStatus);
                return areaCategories;
            });
        }
        AllAreaCategories[] cats = new AllAreaCategories[divs];
        try {
            for (int i = 0; i < divs; i++) {
                AllAreaCategories areaCategories = futures[i].get();
                cats[i] = areaCategories;
            }
        } catch (InterruptedException e) {
            for (int i = 0; i < divs; i++) {
                futures[i].cancel(true);
            }
            throw new RunInterruptedException();
        } catch (ExecutionException e) {
            throw new IllegalStateException("Solving failed", e);
        }
        AllAreaCategories mergeAreaCategories = mergeAreaCategories(cats);
        return mergeAreaCategories;
    }

    public static void dispose() {
        executorService.shutdownNow();
    }
}
