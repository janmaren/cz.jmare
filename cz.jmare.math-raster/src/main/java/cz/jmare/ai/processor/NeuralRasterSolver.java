package cz.jmare.ai.processor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import cz.jmare.ai.BPSolver;
import cz.jmare.ai.resolver.AIDataResolver;
import cz.jmare.aid.DAISolver;
import cz.jmare.math.raster.entity.AllAreaCategories;
import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.math.raster.entity.PixelCat;
import cz.jmare.math.raster.entity.RemappableArea;
import cz.jmare.progress.ProgressStatus;
import cz.jmare.util.NPattern;

public class NeuralRasterSolver  {
    /**
     * Train BPSolver
     * @param bpSolver
     * @param aiDataResolver
     * @param baseAndSelections
     * @param mseConsumer
     * @return
     */
    public static TrainMetadata train(BPSolver bpSolver, AIDataResolver aiDataResolver, List<BaseAndSelection> baseAndSelections, BiConsumer<Integer, Double> mseConsumer) {
        List<NPattern> nPatterns = toNPatterns(baseAndSelections, aiDataResolver);
        bpSolver.randomizeWeights();
        bpSolver.setMseConsumer(mseConsumer);
        long start = System.currentTimeMillis();
        bpSolver.train(nPatterns);
        long end = System.currentTimeMillis();

        TrainMetadata trainMetadata = new TrainMetadata();
        trainMetadata.lastMse = bpSolver.getLastMse();
        trainMetadata.lastEpochNumber = bpSolver.getLastEpochNumber();
        trainMetadata.durationMillis = end - start;

        return trainMetadata;
    }

    private static List<NPattern> toNPatterns(List<BaseAndSelection> baseAndSelections, AIDataResolver aiDataResolver) {
        List<NPattern> nPatterns = new ArrayList<NPattern>();
        for (BaseAndSelection baseAndSelection : baseAndSelections) {
            AreaPixels imageBase = baseAndSelection.imageBase;
            RemappableArea imageCateg = baseAndSelection.imageCateg;
            Iterator<PixelCat> iterator = imageCateg.iterator();
            while (iterator.hasNext()) {
                PixelCat pixelCat = iterator.next();
                NPattern nPattern = toTrainingShape(pixelCat, imageBase, aiDataResolver);
                if (nPattern == null) {
                    continue;
                }
                nPatterns.add(nPattern);
            }
        }
        return nPatterns;
    }

    private static NPattern toTrainingShape(PixelCat pixelCat, AreaPixels imageBase, AIDataResolver aiDataResolver) {
        int x = pixelCat.x;
        int y = pixelCat.y;
        double[] target = aiDataResolver.getTargetData(pixelCat.category);
        double[] input = aiDataResolver.getInputData(imageBase, x, y);
        if (input == null) {
            return null;
        }
        return new NPattern(input, target);
    }

    /**
     * Calculate result using BPSolver which is in learned state
     * @param bpSolver
     * @param aiDataResolver
     * @param noSolutionCategory
     * @param areaPixels
     * @param aiOutputs
     * @return
     */
    public static AllAreaCategories calculateResult(BPSolver bpSolver, AIDataResolver aiDataResolver, int noSolutionCategory, AreaPixels areaPixels, Consumer<AIOutput> aiOutputConsumer) {
        int height = areaPixels.getHeight();
        int width = areaPixels.getWidth();
        AllAreaCategories areaCategories = new AllAreaCategories(width, height);
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                double[] input = aiDataResolver.getInputData(areaPixels, i, j);
                if (input == null) {
                    areaCategories.setCategory(i, j, noSolutionCategory);
                } else {
                    double[] output = bpSolver.solve(input);
                    int category = aiDataResolver.getCategory(output);
                    areaCategories.setCategory(i, j, category);
                    if (aiOutputConsumer != null) {
                        aiOutputConsumer.accept(new AIOutput(i, j, category, output));
                    }
                }
            }
        }
        return areaCategories;
    }

    public static AllAreaCategories calculateResult(DAISolver daiSolver, AIDataResolver aiDataResolver, int noSolutionCategory, AreaPixels areaPixels, Consumer<AIOutput> aiOutputConsumer, ProgressStatus<Long> progressStatus) {
        return calculateResult(daiSolver, aiDataResolver, noSolutionCategory, areaPixels, 0, areaPixels.getHeight(), aiOutputConsumer, progressStatus);
    }

    /**
     * Calculate result using DAISolver which is in the learned state
     * @param daiSolver can be obtained by: learningState = bpSolver.getLearningState(); daiSolver = new DAISolver(learningState);
     * @param aiDataResolver
     * @param noSolutionCategory
     * @param areaPixels
     * @param fromYIncl
     * @param toYExcl
     * @param aiOutputs
     * @return
     */
    public static AllAreaCategories calculateResult(DAISolver daiSolver, AIDataResolver aiDataResolver, int noSolutionCategory, AreaPixels areaPixels, int fromYIncl, int toYExcl, Consumer<AIOutput> aiOutputConsumer, ProgressStatus<Long> progressStatus) {
        int width = areaPixels.getWidth();
        int height = toYExcl - fromYIncl;
        progressStatus.setMaxValue((long) height);
        AllAreaCategories areaCategories = new AllAreaCategories(width, height);
        Thread currentThread = Thread.currentThread();
        for (int j = fromYIncl; j < toYExcl; j++) {
            progressStatus.setProgressValue((long) (j - fromYIncl));
            for (int i = 0; i < width; i++) {
                double[] input = aiDataResolver.getInputData(areaPixels, i, j);
                if (input == null) {
                    areaCategories.setCategory(i, j - fromYIncl, noSolutionCategory);
                } else {
                    double[] output = daiSolver.solve(input);
                    int category = aiDataResolver.getCategory(output);
                    areaCategories.setCategory(i, j - fromYIncl, category);
                    if (aiOutputConsumer != null) {
                        double[] outputCopy = new double[output.length];
                        System.arraycopy(output, 0, outputCopy, 0, output.length);
                        aiOutputConsumer.accept(new AIOutput(i, j, category, outputCopy));
                    }
                }
            }
            if (currentThread.isInterrupted()) {
                throw new IllegalStateException("Interrupted");
            }
        }
        progressStatus.setProgressValue((long) height);
        return areaCategories;
    }
}
