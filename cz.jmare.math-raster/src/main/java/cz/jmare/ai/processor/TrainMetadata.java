package cz.jmare.ai.processor;

public class TrainMetadata {
    public double lastMse;

    public int lastEpochNumber;

    public long durationMillis;

    @Override
    public String toString() {
        return "TrainMetadata [lastMse=" + lastMse + ", lastEpochNumber=" + lastEpochNumber + ", durationMillis="
                + durationMillis + "]";
    }
}
