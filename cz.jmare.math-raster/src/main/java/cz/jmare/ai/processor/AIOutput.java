package cz.jmare.ai.processor;

import java.util.Arrays;

public class AIOutput {
    public int x;

    public int y;

    public int category;

    public double[] outputs;

    public AIOutput(int x, int y, int category, double[] outputs) {
        super();
        this.x = x;
        this.y = y;
        this.category = category;
        this.outputs = outputs;
    }

    @Override
    public String toString() {
        return "AIOutput [category=" + category + ", outputs=" + Arrays.toString(outputs) + "]";
    }
}
