package cz.jmare.ai.alg;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class CircleGradient {
    /**
     * Top, bottom, left and right points of circle don't have rate 1 but rather about 0.96<br>
     * @param radius
     * @return
     */
    public static Set<CircleRasterPoint> pixelsInCircleRateMedium(double radius) {
        return pixelsInCircleRate(radius, 0.51);
    }

    /**
     * Top, bottom, left and right points of circle don't have rate 1 but much less somewhere about 0.55<br>
     * Prefered antialiasing
     * @param radius
     * @return
     */
    public static Set<CircleRasterPoint> pixelsInCircleRateStrict(double radius) {
        return pixelsInCircleRate(radius, 0.1);
    }

    /**
     * Top, bottom, left and right points of circle have rate 1<br>
     * @param radius
     * @return
     */
    public static Set<CircleRasterPoint> pixelsInCircleRateBenevolent(double radius) {
        return pixelsInCircleRate(radius, 0.7);
    }

    public static Set<CircleRasterPoint> pixelsInCircleRate(double radius, double tolerance) {
        double powerR = (radius + tolerance) * (radius + tolerance);

        Set<CircleRasterPoint> points = new HashSet<CircleGradient.CircleRasterPoint>();
        int radiusInt = (int) Math.round(radius);
        for (int i = 0 - radiusInt; i <= radiusInt; i++) {
            for (int j = 0 - radiusInt; j <= radiusInt; j++) {
                if (((j - 0.5) * (j - 0.5) + (i - 0.5) * (i - 0.5)) <= powerR &&
                        ((j + 0.5) * (j + 0.5) + (i - 0.5) * (i - 0.5)) <= powerR &&
                        ((j - 0.5) * (j - 0.5) + (i + 0.5) * (i + 0.5)) <= powerR &&
                        ((j + 0.5) * (j + 0.5) + (i + 0.5) * (i + 0.5)) <= powerR) {
                    // when all corners of pixel inside the whole pixel is inside
                    points.add(new CircleRasterPoint(j, i, 1.0));
                } else {
                    // how much of pixel is inside
                    points.add(new CircleRasterPoint(j, i, rate(j, i, powerR)));
                }
            }
        }

        return points;
    }

    private static double rate(int x, int y, double powerR) {
        int count = 0;
        int inCount = 0;
        for (int i = 0; i <= 10; i++) {
            double dy = i * 0.1 - 0.5;
            for (int j = 0; j <= 10; j++) {
                double dx = j * 0.1 - 0.5;
                if (((x + dx) * (x + dx) + (y + dy) * (y + dy)) <= powerR) {
                    inCount++;
                }
                count++;
            }
        }
        return (double) inCount / count;
    }

    public static class CircleRasterPoint {
        public int x;

        public int y;

        /**
         * 1 = pixel is fully in circle, 0 = pixel is out of circle, 0.5 - is half...
         */
        public double presenceRatio;

        public CircleRasterPoint(int x, int y, double presenceRatio) {
            super();
            this.x = x;
            this.y = y;
            this.presenceRatio = presenceRatio;
        }

        @Override
        public String toString() {
            return "CircleRasterPoint [x=" + x + ", y=" + y + ", presenceRatio=" + presenceRatio + "]";
        }

    }

    public static void main(String[] args) {
        int radius = 3;
        Set<CircleRasterPoint> circlePixelPoints = pixelsInCircleRateMedium(radius);
        for (CircleRasterPoint circleRasterPoint : circlePixelPoints) {
            System.out.println(circleRasterPoint);
        }
        System.out.println("Count: " + circlePixelPoints.size());

        Optional<CircleRasterPoint> findFirst = circlePixelPoints.stream().filter(p -> p.x == radius && p.y == 0).findFirst();
        System.out.println(findFirst);
    }
}
