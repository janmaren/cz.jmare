package cz.jmare.ai.data;

import java.util.Properties;

public class PropUtil {
    public static Integer getInteger(String propertyName, Properties properties, Integer defaultValue) {
        String value = extractPropertyValue(propertyName, properties);
        if (value == null) {
            return defaultValue;
        }
        return Integer.parseInt(value);
    }

    public static void putInteger(String propertyName, Integer propertyValue, Properties properties) {
        properties.put(propertyName, String.valueOf(propertyValue));
    }

    public static Long getLong(String propertyName, Properties properties, Long defaultValue) {
        String value = extractPropertyValue(propertyName, properties);
        if (value == null) {
            return defaultValue;
        }
        return Long.parseLong(value);
    }

    public static void putLong(String propertyName, Long propertyValue, Properties properties) {
        properties.put(propertyName, String.valueOf(propertyValue));
    }

    public static Double getDouble(String propertyName, Properties properties, Double defaultValue) {
        String value = extractPropertyValue(propertyName, properties);
        if (value == null) {
            return defaultValue;
        }
        return Double.parseDouble(value);
    }

    public static void putDouble(String propertyName, Double propertyValue, Properties properties) {
        properties.put(propertyName, String.valueOf(propertyValue));
    }

    public static String extractPropertyValue(String propertyName,
            Properties properties) {
        String value = properties.getProperty(propertyName);
        if (value == null) {
            return null;
        }
        value = value.trim();
        if ("".equals(value)) {
            return null;
        }
        return value;
    }
}
