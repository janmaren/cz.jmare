package cz.jmare.ai.data;

import java.util.Properties;

import cz.jmare.ai.LearningState;
import cz.jmare.ai.resolver.AIDataResolver;
import cz.jmare.ai.resolver.AIDataResolverFactory;
import cz.jmare.util.LearningStatePersist;

public class LearningResultInfo {
    public LearningState learningState;

    public AIDataResolver aiDataResolver;

    public String name;

    public String description;

    public Long learningDurationMillis;

    public Double mse;

    public Integer epochs;

    public Integer alphaCategory;

    public LearningResultInfo(LearningState learningState, AIDataResolver aiDataResolver) {
        super();
        this.learningState = learningState;
        this.aiDataResolver = aiDataResolver;
    }

    public static LearningResultInfo fromProperties(Properties properties) {
        LearningState learningState = LearningStatePersist.fromProperties(properties);
        AIDataResolver aiDataResolver = AIDataResolverFactory.fromProperties(properties);
        LearningResultInfo learningResultInfo = new LearningResultInfo(learningState, aiDataResolver);
        learningResultInfo.name = properties.getProperty("ai.name");
        learningResultInfo.description = properties.getProperty("ai.description");
        learningResultInfo.epochs = PropUtil.getInteger("learning.epochs", properties, null);
        learningResultInfo.mse = PropUtil.getDouble("learning.mse", properties, null);
        learningResultInfo.learningDurationMillis = PropUtil.getLong("learning.duration", properties, null);
        learningResultInfo.alphaCategory = PropUtil.getInteger("learning.alphaCategory", properties, null);
        return learningResultInfo;
    }

    public static void toProperties(LearningResultInfo learningResultInfo, Properties properties) {
        LearningStatePersist.toProperties(learningResultInfo.learningState, properties);
        AIDataResolverFactory.toProperties(learningResultInfo.aiDataResolver, properties);
        properties.setProperty("ai.name", learningResultInfo.name);
        if (learningResultInfo.description != null) {
            properties.setProperty("ai.description", learningResultInfo.description);
        }
        if (learningResultInfo.epochs != null) {
            properties.put("learning.epochs", learningResultInfo.epochs.toString());
        }
        if (learningResultInfo.mse != null) {
            properties.put("learning.mse", learningResultInfo.mse.toString());
        }
        if (learningResultInfo.learningDurationMillis != null) {
            properties.put("learning.duration", learningResultInfo.learningDurationMillis.toString());
        }
        if (learningResultInfo.alphaCategory != null) {
            properties.put("learning.alphaCategory", learningResultInfo.alphaCategory.toString());
        }
    }

}
