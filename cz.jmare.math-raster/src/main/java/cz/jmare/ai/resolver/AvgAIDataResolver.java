package cz.jmare.ai.resolver;

import cz.jmare.math.raster.entity.RGBEntity;

public class AvgAIDataResolver extends AroundPixelResolver {
    private static final int INTS_FOR_PIXEL_COUNT = 1;
    private double trueThreshold = 0.98;

    public AvgAIDataResolver(int neighbourPoints) {
        super(neighbourPoints);
    }

    @Override
    protected double[] getInputData(RGBEntity rgbEntity) {
        double[] input = new double[1];
        double r = rgbEntity.red / 255.0;
        double g = rgbEntity.green / 255.0;
        double b = rgbEntity.blue / 255.0;
        input[0] = (r + g + b) / 3.0;
        return input;
    }

    /**
     * Return target data for given category. This implementation assigns 1.0 for appropriate neuron,
     * or all are 0 when category 0.
     * The returned array must have the same length as output layer
     * @param category
     */
    @Override
    public double[] getTargetData(int category) {
        double[] target = new double[INTS_FOR_PIXEL_COUNT];
        if (category == 1) {
            target[0] = 1.0;
        } else {
            target[0] = 0.0;
        }
        return target;
    }

    @Override
    public int getCategory(double[] outputData) {
        if (outputData[0] > getTrueThreshold()) {
            return 1;
        }
        return 0;
    }

    public double getTrueThreshold() {
        return trueThreshold;
    }

    public void setTrueThreshold(double trueThreshold) {
        this.trueThreshold = trueThreshold;
    }
}
