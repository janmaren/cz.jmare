package cz.jmare.ai.resolver;

import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.math.raster.entity.RGBEntity;

public abstract class AroundPixelResolver implements AIDataResolver {
    private int neighbourPoints;

    public AroundPixelResolver(int neighbourPoints) {
        super();
        this.neighbourPoints = neighbourPoints;
    }

    /**
     * Build input data using N pixels around each pixel
     */
    @Override
    public double[] getInputData(AreaPixels imageBase, int x, int y) {
        if (x - getNeighbourPoints() < 0 || x + getNeighbourPoints() >= imageBase.getWidth() || y - getNeighbourPoints() < 0 || y + getNeighbourPoints() >= imageBase.getHeight()) {
            return null;
        }
        double[] inputDataDetect = getInputData(new RGBEntity(255, 255, 255)); // detect input doubles count for one pixel
        int inputDataLength = inputDataDetect.length;
        int expectedInputLength = (2 * getNeighbourPoints() + 1) * (2 * getNeighbourPoints() + 1) * inputDataLength;
        int inputIndex = 0;
        double[] input = new double[expectedInputLength];
        for (int j = y - getNeighbourPoints(); j <= y + getNeighbourPoints(); j++) {
            for (int i = x - getNeighbourPoints(); i <= x + getNeighbourPoints(); i++) {
                RGBEntity rgbEntity = imageBase.rgbEntity(i, j);
                double[] inputData = getInputData(rgbEntity);
                System.arraycopy(inputData, 0, input, inputIndex, inputData.length);
                inputIndex += inputData.length;
            }
        }
        return input;
    }

    /**
     * Return the input double data representing one pixel. This implementation represents
     * one pixel as array with one double with - 1.0 (nonewhite) or 0.0 (white).
     * Override if wou need different representation or multiple doubles to return
     * @param rgbEntity
     * @return
     */
    protected abstract double[] getInputData(RGBEntity rgbEntity);

    public int getNeighbourPoints() {
        return neighbourPoints;
    }

    @Override
    public Integer getAlphaCategory() {
        return null;
    }
}
