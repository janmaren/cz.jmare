package cz.jmare.ai.resolver;

import cz.jmare.math.raster.entity.RGBEntity;

public class DiscreteAIDataResolver extends AroundPixelResolver {
    private static final int INTS_FOR_PIXEL_COUNT = 1;
    private double trueThreshold = 0.98;

    public DiscreteAIDataResolver(int neighbourPoints) {
        super(neighbourPoints);
    }
    /**
     * Return the input double data representing one pixel. This implementation represents
     * one pixel as array with one double with - 1.0 (nonewhite) or 0.0 (white).
     * @param rgbEntity
     * @return
     */
    @Override
    protected double[] getInputData(RGBEntity rgbEntity) {
        double[] input = new double[INTS_FOR_PIXEL_COUNT];
        input[0] = (rgbEntity.red != 255 || rgbEntity.green != 255 || rgbEntity.blue != 255)
                && !(rgbEntity.red == 0 && rgbEntity.green == 0 && rgbEntity.blue == 0) ? 1.0 : 0.0;
        return input;
    }

    /**
     * Return target data for given category. This implementation assigns 1.0 for appropriate neuron,
     * or all are 0 when category 0.
     * The returned array must have the same length as output layer
     * @param category
     */
    @Override
    public double[] getTargetData(int category) {
        double[] target = new double[INTS_FOR_PIXEL_COUNT];
        if (category == 1) {
            target[0] = 1.0;
        } else {
            target[0] = 0.0;
        }
        return target;
    }

    @Override
    public int getCategory(double[] outputData) {
        if (outputData[0] > getTrueThreshold()) {
            return 1;
        }
        return 0;
    }
    public double getTrueThreshold() {
        return trueThreshold;
    }
    public void setTrueThreshold(double trueThreshold) {
        this.trueThreshold = trueThreshold;
    }
}
