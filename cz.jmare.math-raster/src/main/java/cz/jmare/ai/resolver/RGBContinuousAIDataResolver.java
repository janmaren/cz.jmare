package cz.jmare.ai.resolver;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

public class RGBContinuousAIDataResolver extends AroundPixelResolver {
    private static final int INTS_FOR_PIXEL_COUNT = 3;

    public RGBContinuousAIDataResolver(int neighbourPoints) {
        super(neighbourPoints);
    }

    @Override
    protected double[] getInputData(RGBEntity rgbEntity) {
        double[] input = new double[3];
        input[0] = rgbEntity.red / 255.0;
        input[1] = rgbEntity.green / 255.0;
        input[2] = rgbEntity.blue / 255.0;
        return input;
    }

    @Override
    public double[] getTargetData(int category) {
        double[] target = new double[INTS_FOR_PIXEL_COUNT];
        RGBEntity rgbEntity = RGBEntUtil.rHighestToRGB(category);
        target[0] = intToDouble(rgbEntity.red);
        target[1] = intToDouble(rgbEntity.green);
        target[2] = intToDouble(rgbEntity.blue);
        return target;
    }

    @Override
    public int getCategory(double[] outputData) {
        if (outputData.length != INTS_FOR_PIXEL_COUNT) {
            throw new IllegalStateException("Expected " + INTS_FOR_PIXEL_COUNT + " array but the length is " + outputData.length);
        }
        return RGBEntUtil.rHighestOpaqToRGBInt(doubleToInt(outputData[0]), doubleToInt(outputData[1]), doubleToInt(outputData[2]));
    }

    public static int doubleToInt(double value) {
        if (value > 1.0) {
            return 255;
        }
        if (value < 0) {
            return 0;
        }
        return (int) (value * 255.0);
    }

    public static double intToDouble(int value) {
        if (value > 255) {
            return 1;
        }
        if (value < 0) {
            return 0;
        }
        return (value / 255.0);
    }
}
