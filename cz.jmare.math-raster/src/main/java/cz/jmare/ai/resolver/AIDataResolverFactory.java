package cz.jmare.ai.resolver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class AIDataResolverFactory {
    public static AIDataResolver fromProperties(Properties properties) {
        String aiDataResolverClass = properties.getProperty("aiDataResolver.class");
        if (aiDataResolverClass != null) {
            Class<?> clazz;
            try {
                clazz = Class.forName(aiDataResolverClass);
            } catch (ClassNotFoundException e1) {
                throw new IllegalStateException("Unable to load aiDataResolver", e1);
            }

            Object[] args = new Object[] {};
            @SuppressWarnings("rawtypes")
            Class[] classes = new Class[] {};
            if (AroundPixelResolver.class.isAssignableFrom(clazz)) {
                classes = new Class[] {Integer.TYPE};
                args = new Object[] {Integer.valueOf(getMandatoryProp(properties, "aiDataResolver.neighbourPoints"))};
            }

            // support of other AIDataResolver instances here

            try {
                Constructor<?> declaredConstructor = clazz.getDeclaredConstructor(classes);
                AIDataResolver aiDataResolver = (AIDataResolver) declaredConstructor.newInstance(args);

                String trueThresholdProperty = properties.getProperty("aiDataResolver.trueThreshold");
                if (trueThresholdProperty != null) {
                    if (aiDataResolver instanceof AvgAIDataResolver) {
                        AvgAIDataResolver avgAIDataResolver = (AvgAIDataResolver) aiDataResolver;
                        avgAIDataResolver.setTrueThreshold(Double.valueOf(trueThresholdProperty));
                    }
                    if (aiDataResolver instanceof DiscreteAIDataResolver) {
                        DiscreteAIDataResolver discreteAIDataResolver = (DiscreteAIDataResolver) aiDataResolver;
                        discreteAIDataResolver.setTrueThreshold(Double.valueOf(trueThresholdProperty));
                    }
                    if (aiDataResolver instanceof RGBAIDataResolver) {
                        RGBAIDataResolver rgbaiDataResolver = (RGBAIDataResolver) aiDataResolver;
                        rgbaiDataResolver.setTrueThreshold(Double.valueOf(trueThresholdProperty));
                    }
                }

                // support of other AIDataResolver instances here

                return aiDataResolver;
            } catch (NoSuchMethodException | SecurityException | InstantiationException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new IllegalStateException("Unable to load aiDataResolver", e);
            }
        } else {
            throw new IllegalStateException("No aiDataResolver.class present in properties");
        }
    }

    public static void toProperties(AIDataResolver aiDataResolver, Properties properties) {
        properties.put("aiDataResolver.class", aiDataResolver.getClass().getCanonicalName());
        if (aiDataResolver instanceof AroundPixelResolver) {
            AroundPixelResolver simpleAIDataResolver = (AroundPixelResolver) aiDataResolver;
            properties.put("aiDataResolver.neighbourPoints", String.valueOf(simpleAIDataResolver.getNeighbourPoints()));
        }
        if (aiDataResolver instanceof AvgAIDataResolver) {
            AvgAIDataResolver avgAIDataResolver = (AvgAIDataResolver) aiDataResolver;
            properties.put("aiDataResolver.trueThreshold", String.valueOf(avgAIDataResolver.getTrueThreshold()));
        }
        if (aiDataResolver instanceof DiscreteAIDataResolver) {
            DiscreteAIDataResolver discreteAIDataResolver = (DiscreteAIDataResolver) aiDataResolver;
            properties.put("aiDataResolver.trueThreshold", String.valueOf(discreteAIDataResolver.getTrueThreshold()));
        }
        if (aiDataResolver instanceof RGBAIDataResolver) {
            RGBAIDataResolver rgbaiDataResolver = (RGBAIDataResolver) aiDataResolver;
            properties.put("aiDataResolver.trueThreshold", String.valueOf(rgbaiDataResolver.getTrueThreshold()));
        }
    }

    private static String getMandatoryProp(Properties properties, String propName) {
        String property = properties.getProperty(propName);
        if (property == null) {
            throw new IllegalStateException("Property " + propName + " doesn't exist");
        }
        return property;
    }
}
