package cz.jmare.ai.resolver;

import cz.jmare.math.raster.entity.AreaPixels;

public interface AIDataResolver {
    /**
     * Return input data for given pixel.
     * The returned array must have the same length as input layer
     * @param imageBase
     * @param x
     * @param y
     * @return
     */
    double[] getInputData(AreaPixels imageBase, int x, int y);

    /**
     * Return target data for given category.
     * The returned array must have the same length as output layer
     * @param category
     */
    double[] getTargetData(int category);

    /**
     * Interpret category from calculated output of result.
     * @param outputData data returned from output layer.
     * @return
     */
    int getCategory(double[] outputData);

    Integer getAlphaCategory();
}
