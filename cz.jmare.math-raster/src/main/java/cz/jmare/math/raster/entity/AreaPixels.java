package cz.jmare.math.raster.entity;

import java.awt.image.BufferedImage;

/**
 * Represents structure with array where coordinates [0][0] is left bottom point, [height - 1][0] is left
 * top and [height - 1][width - 1] is right top
 */
public class AreaPixels {
    private int[][] raster;

    public AreaPixels(BufferedImage bufferedImage) {
        fromBufferedImage(bufferedImage);
    }

    /**
     * Get pixel using x, y coordinates
     * @param x
     * @param y
     * @return
     */
    public RGBEntity rgbEntity(int x, int y) {
        RGBEntity rgb = RGBEntUtil.rHighestToRGB(raster[y][x]);
        return rgb;
    }

    public int getWidth() {
        return raster[0].length;
    }

    public int getHeight() {
        return raster.length;
    }

    public BufferedImage toBufferedImage() {
        int width = getWidth();
        int height = getHeight();
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int pixel = raster[height - i - 1][j];
                bufferedImage.setRGB(j, i, pixel);
            }
        }
        return bufferedImage;
    }

    public void fromBufferedImage(BufferedImage bufferedImage) {
        if (bufferedImage.getHeight() == 0 || bufferedImage.getWidth() == 0) {
            throw new IllegalArgumentException("Empty image passed");
        }
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        raster = new int[height][];
        for (int i = 0; i < height; i++) {
            raster[height - i - 1] = new int[width];
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                raster[height - i - 1][j] = rgb;
            }
        }
    }

    /**
     * To int with red highest weight and opaque to alpha byte.
     * Corresponds to {@link BufferedImage#TYPE_INT_RGB}
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static int rHighestOpaqToRGBInt(int red, int green, int blue) {
        return (0xFF << 24) | red << 16 | green << 8 | blue;
    }
}
