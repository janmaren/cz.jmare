package cz.jmare.math.raster.entity;

import java.util.Iterator;

public class AllAreaCategories implements RemappableArea {
    private int[][] raster;
    private int width;
    private int height;

    public AllAreaCategories(int width, int height) {
        this.width = width;
        this.height = height;
        raster = new int[height][];
        for (int i = 0; i < height; i++) {
            raster[i] = new int[width];
            for (int j = 0; j < width; j++) {
                raster[i][j] = -1;
            }
        }
    }

    public void setCategory(int x, int y, int category) {
        raster[y][x] = category;
    }

    public int getCategory(int x, int y) {
        return raster[y][x];
    }

    @Override
    public Iterator<PixelCat> iterator() {
        AllAreaCategoriesIterator allAreaCategoriesIterator = new AllAreaCategoriesIterator(raster, width, height);
        return allAreaCategoriesIterator;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public static class AllAreaCategoriesIterator implements Iterator<PixelCat> {
        private int[][] raster;
        private int width;
        private int height;
        private int currentX;
        private int currentY;
        private PixelCat nextPixelCat;

        public AllAreaCategoriesIterator(int[][] raster, int width, int height) {
            this.raster = raster;
            this.width = width;
            this.height = height;
            prepareNextPixelCat();
        }

        @Override
        public boolean hasNext() {
            return nextPixelCat != null;
        }

        private void prepareNextPixelCat() {
            while (currentY < height) {
                int category = raster[currentY][currentX];
                int x = currentX;
                int y = currentY;
                currentX++;
                if (currentX >= width) {
                    currentX = 0;
                    currentY++;
                }
                if (category != -1) {
                    PixelCat pixelCat = new PixelCat(x, y, category);
                    nextPixelCat = pixelCat;
                    return;
                }
            }
            nextPixelCat = null;
        }

        @Override
        public PixelCat next() {
            if (nextPixelCat == null) {
                throw new ArrayIndexOutOfBoundsException();
            }
            PixelCat pixCat = nextPixelCat;
            prepareNextPixelCat();
            return pixCat;
        }
    }
}
