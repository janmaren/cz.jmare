package cz.jmare.math.raster.entity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class AreaCategories implements RemappableArea {
    private Set<PixelCat> points = new HashSet<>();

    private int width;

    private int height;

    public AreaCategories() {
        this.width = 0;
        this.height = 0;
    }

    public AreaCategories(Set<PixelCat> points, int width, int height) {
        this.width = width;
        this.height = height;
        this.setPoints(points);
    }

    public void setPoints(Set<PixelCat> points) {
        this.points = points;
    }

    public Set<PixelCat> getPoints() {
        return points;
    }

    @Override
    public Iterator<PixelCat> iterator() {
        return points.iterator();
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public boolean containsPixel() {
        return points.size() > 0;
    }

    public void remove(PixelCat pixelCat) {
        points.remove(pixelCat);
    }

    public void add(PixelCat pixelCat) {
        points.add(pixelCat);
    }

    public boolean contains(PixelCat point) {
        return points.contains(point);
    }
}
