package cz.jmare.math.raster.entity;

import java.util.Iterator;

public interface RemappableArea {
    public Iterator<PixelCat> iterator();

    public int getWidth();

    public int getHeight();
}
