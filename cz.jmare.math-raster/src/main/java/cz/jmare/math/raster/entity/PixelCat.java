package cz.jmare.math.raster.entity;

/**
 * Holds left to right and bottom to top coordinates and category which is often an RGB value
 */
public class PixelCat {
    public int x;

    public int y;

    public int category;

    public PixelCat(int x, int y, int category) {
        this.x = x;
        this.y = y;
        this.category = category;
    }

    @Override
    public String toString() {
        return "PixelCat [x=" + x + ", y=" + y + ", cat=" + category + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PixelCat other = (PixelCat) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }
}
