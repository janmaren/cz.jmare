package cz.jmare.ai.processor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;

import cz.jmare.ai.BPSolver;
import cz.jmare.ai.LearningState;
import cz.jmare.ai.actfunc.ActFunc;
import cz.jmare.ai.actfunc.Logsig;
import cz.jmare.ai.imagepoints.CatImagePointsMapping;
import cz.jmare.ai.resolver.RGBAIDataResolver;
import cz.jmare.aid.DAISolver;
import cz.jmare.aif.FAISolver;
import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.math.raster.entity.RemappableArea;
import cz.jmare.progress.NullLongProgressStatus;

public class NeuralProcessorTest {
    private static final ActFunc DEFAULT_HIDDEN_ACT_FUNC = new Logsig();
    public static final ActFunc DEFAULT_OUTPUT_ACT_FUNC = new Logsig();
    private static LinkedHashMap<Integer, RGBEntity> DEFAULT_CAT_TO_RGB = new LinkedHashMap<Integer, RGBEntity>();
    static {
        DEFAULT_CAT_TO_RGB.put(0, new RGBEntity(255, 0, 0));
        DEFAULT_CAT_TO_RGB.put(1, new RGBEntity("#ffff00"));
        DEFAULT_CAT_TO_RGB.put(2, new RGBEntity("#000090"));
        DEFAULT_CAT_TO_RGB.put(3, new RGBEntity("#00ff00"));
        DEFAULT_CAT_TO_RGB.put(4, new RGBEntity("#ff00ff"));
        DEFAULT_CAT_TO_RGB.put(5, new RGBEntity("#ffe4c4"));
        DEFAULT_CAT_TO_RGB.put(6, new RGBEntity("#00bfff"));
        DEFAULT_CAT_TO_RGB.put(7, new RGBEntity("#b03060"));
        DEFAULT_CAT_TO_RGB.put(8, new RGBEntity("#008000"));
        DEFAULT_CAT_TO_RGB.put(9, new RGBEntity("#7c3f1d"));
        DEFAULT_CAT_TO_RGB.put(Integer.MAX_VALUE, new RGBEntity(192, 192, 192));
    }
    private static Map<RGBEntity, Integer> DEFAULT_RGB_TO_CAT = getInvertedMap(DEFAULT_CAT_TO_RGB);

    @Test
    public void trainResult() throws IOException {
        // definition
        BPSolver bpSolver = new BPSolver(27, 30, 30, 1);
        RGBAIDataResolver aiDataResolver = new RGBAIDataResolver(1);
        bpSolver.setMaxMse(Math.pow(10, -5));
        bpSolver.setMaxIterations(2000);
        bpSolver.setOutputLayerActFunc(DEFAULT_OUTPUT_ACT_FUNC);
        bpSolver.setHiddenLayeraActFunc(DEFAULT_HIDDEN_ACT_FUNC);
        aiDataResolver.setTrueThreshold(0.9);

        // prepare training - base and selection image
        List<BaseAndSelection> baseAndSelections;
        try (InputStream is1 = NeuralProcessorTest.class.getResourceAsStream("img-01.png"); InputStream is2 = NeuralProcessorTest.class.getResourceAsStream("img-01-selection.png")) {
            baseAndSelections = TrainUtil.of(DEFAULT_RGB_TO_CAT, is1, is2);
        }

        // training
        NeuralRasterSolver.train(bpSolver, aiDataResolver, baseAndSelections, null);

        // results using training
        List<AIOutput> outputs = new ArrayList<>();
        Consumer<AIOutput> outputConsumer = new Consumer<AIOutput>() {
            @Override
            public void accept(AIOutput aiOutput) {
                outputs.add(aiOutput);
            }};
        RemappableArea areaCategories = NeuralRasterSolver.calculateResult(bpSolver, aiDataResolver, Integer.MAX_VALUE, baseAndSelections.get(0).imageBase, outputConsumer);

        // write result
        CatImagePointsMapping catImagePoints = new CatImagePointsMapping();
        BufferedImage bufferedImage = catImagePoints.categoriesToImage(areaCategories, null);
        ImageIO.write(bufferedImage, "png", new File("target/result.png"));

        // assert
        try (FileInputStream is1 = new FileInputStream("target/result.png");
                InputStream is2 = NeuralProcessorTest.class.getResourceAsStream("img-01-result.png")) {
            boolean filesSame = FileCompare.filesSame(is1, is2);
            assertTrue(filesSame);
        }
    }

    @Test
    public void trainResultParallel() throws IOException, InterruptedException, ExecutionException {
        // definition
        BPSolver bpSolver = new BPSolver(27, 30, 30, 1);
        RGBAIDataResolver aiDataResolver = new RGBAIDataResolver(1);
        bpSolver.setMaxMse(Math.pow(10, -5));
        bpSolver.setMaxIterations(2000);
        bpSolver.setOutputLayerActFunc(DEFAULT_OUTPUT_ACT_FUNC);
        bpSolver.setHiddenLayeraActFunc(DEFAULT_HIDDEN_ACT_FUNC);
        aiDataResolver.setTrueThreshold(0.9);

        // prepare training - base and selection image
        List<BaseAndSelection> baseAndSelections;
        try (InputStream is1 = NeuralProcessorTest.class.getResourceAsStream("img-01.png"); InputStream is2 = NeuralProcessorTest.class.getResourceAsStream("img-01-selection.png")) {
            baseAndSelections = TrainUtil.of(DEFAULT_RGB_TO_CAT, is1, is2);
        }

        // training
        NeuralRasterSolver.train(bpSolver, aiDataResolver, baseAndSelections, null);

        // results using training
        AreaPixels imageBase = baseAndSelections.get(0).imageBase;

        List<AIOutput> aiOutputs = new ArrayList<AIOutput>();
        Consumer<AIOutput> outputConsumer = new Consumer<AIOutput>() {
            @Override
            public void accept(AIOutput aiOutput) {
                aiOutputs.add(aiOutput);
            }};
        NeuralRasterSolver.calculateResult(bpSolver, aiDataResolver, Integer.MAX_VALUE, baseAndSelections.get(0).imageBase, outputConsumer);

        RemappableArea mergedAreaCategories = NeuralRasterParallelResult.calculateResult(bpSolver.getLearningState(), aiDataResolver, imageBase, new NullLongProgressStatus());

        // write result
        CatImagePointsMapping catImagePoints = new CatImagePointsMapping();
        BufferedImage bufferedImage = catImagePoints.categoriesToImage(mergedAreaCategories, null);
//        BufferedImage bufferedImage = ConvertCategoryUtil.toBufferedImage(mergedAreaCategories.getPoints(), mergedAreaCategories.getWidth(), mergedAreaCategories.getHeight(), DEFAULT_CAT_TO_RGB, true);
        ImageIO.write(bufferedImage, "png", new File("target/result.png"));

        // assert
        try (FileInputStream is1 = new FileInputStream("target/result.png");
                InputStream is2 = NeuralProcessorTest.class.getResourceAsStream("img-01-result.png")) {
            boolean filesSame = FileCompare.filesSame(is1, is2);
            assertTrue(filesSame);
        }
    }

    @Test
    public void trainFAIResult() throws IOException {
        // definition
        BPSolver bpSolver = new BPSolver(27, 30, 30, 1);
        RGBAIDataResolver aiDataResolver = new RGBAIDataResolver(1);
        bpSolver.setMaxMse(Math.pow(10, -5));
        bpSolver.setMaxIterations(2000);
        bpSolver.setOutputLayerActFunc(DEFAULT_OUTPUT_ACT_FUNC);
        bpSolver.setHiddenLayeraActFunc(DEFAULT_HIDDEN_ACT_FUNC);
        aiDataResolver.setTrueThreshold(0.9);

        // prepare training - base and selection image
        List<BaseAndSelection> baseAndSelections;
        try (InputStream is1 = NeuralProcessorTest.class.getResourceAsStream("img-01.png"); InputStream is2 = NeuralProcessorTest.class.getResourceAsStream("img-01-selection.png")) {
            baseAndSelections = TrainUtil.of(DEFAULT_RGB_TO_CAT, is1, is2);
        }

        // training
        NeuralRasterSolver.train(bpSolver, aiDataResolver, baseAndSelections, null);

        LearningState learningState = bpSolver.getLearningState();
        double[] inputData = aiDataResolver.getInputData(baseAndSelections.get(0).imageBase, 2, 2);

        double[] output = bpSolver.solve(inputData);
        //System.out.println("BPSolver result: " + Arrays.toString(output));

        DAISolver daiSolver = new DAISolver(learningState);
        double[] solve3 = daiSolver.solve(inputData);
        //System.out.println("DAISolver result: " + Arrays.toString(solve3));

        assertEquals(output[0], solve3[0], 0.0000000001);

        float[] floatValues = toFloats(inputData);
        FAISolver faiSolver = new FAISolver(learningState);
        float[] solve2 = faiSolver.solve(floatValues);
        //System.out.println("FAISolver result: " + Arrays.toString(solve2));

        assertEquals(output[0], solve2[0], 0.0000001);
    }

    public static float[] toFloats(double[] doubles) {
        float[] floats = new float[doubles.length];
        for (int i = 0; i < floats.length; i++) {
            floats[i] = (float) doubles[i];
        }
        return floats;
    }

    private static Map<RGBEntity, Integer> getInvertedMap(Map<Integer, RGBEntity> catToRGB) {
        Map<RGBEntity, Integer> rgbToCat = new HashMap<>();
        for (Entry<Integer, RGBEntity> entry : catToRGB.entrySet()) {
            Integer key = entry.getKey();
            RGBEntity value = entry.getValue();
            rgbToCat.put(value, key);
        }
        return rgbToCat;
    }
}
