package cz.jmare.ai.processor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class FileCompare {
    private static final int DEFAULT_BUFFER_SIZE = 8192;

    /**
     * Compare files
     * Because of buffer not thread safe!
     * @param path1
     * @param path2
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static boolean filesSame(String path1, String path2) throws FileNotFoundException, IOException {
        try (InputStream input1 = new FileInputStream(path1); InputStream input2 = new FileInputStream(path2)) {
            return filesSame(input1, input2);
        }
    }

    public static boolean filesSame(InputStream input1, InputStream input2) throws FileNotFoundException, IOException {
        byte[] buffer1 = new byte[DEFAULT_BUFFER_SIZE];
        byte[] buffer2 = new byte[DEFAULT_BUFFER_SIZE];

        int numRead1 = 0;
        int numRead2 = 0;
        while (true) {
            numRead1 = input1.read(buffer1);
            numRead2 = input2.read(buffer2);
            if (numRead1 > -1) {
                if (numRead2 != numRead1)
                    return false;
                // Otherwise same number of bytes read
                if (!Arrays.equals(buffer1, buffer2))
                    return false;
                // Otherwise same bytes read, so continue ...
            } else {
                // Nothing more in stream 1 ...
                return numRead2 < 0;
            }
        }
    }
}
