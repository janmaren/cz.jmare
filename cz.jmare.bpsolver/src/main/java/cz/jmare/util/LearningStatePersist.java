package cz.jmare.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

import cz.jmare.ai.LearningState;

public class LearningStatePersist {

    public static void toProperties(LearningState learningState, Properties properties) {
        int[] layers = learningState.layers;
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (int layer : layers) {
            stringJoiner.add(String.valueOf(layer));
        }
        properties.put("layers", stringJoiner.toString());

        Class<?>[] actFuncs = learningState.actFuncs;
        stringJoiner = new StringJoiner(", ");
        for (Class<?> class1 : actFuncs) {
            stringJoiner.add(class1.getCanonicalName());
        }
        properties.put("actFuncs", stringJoiner.toString());

        double[][][] weights = learningState.weights;
        for (int i = 0; i < weights.length; i++) {
            double[][] weightsLayer = weights[i];
            StringJoiner stringJoinerLayer = new StringJoiner("; ");
            for (int j = 0; j < weightsLayer.length; j++) {
                double[] weightsNeuron = weightsLayer[j];
                StringJoiner stringJoinerNeuron = new StringJoiner(", ");
                for (int k = 0; k < weightsNeuron.length; k++) {
                    stringJoinerNeuron.add(String.valueOf(weightsNeuron[k]));
                }
                stringJoinerLayer.add(stringJoinerNeuron.toString());
            }
            properties.put("layers.weights." + (i + 1) , stringJoinerLayer.toString());
        }
    }

    public static LearningState fromProperties(Properties properties) {
        String layers = properties.getProperty("layers");
        if (layers == null) {
            throw new IllegalStateException("Property layers doesn't exist");
        }
        String[] layersSplit = layers.trim().split(",");
        List<Integer> layersList = new ArrayList<Integer>();
        for (String string : layersSplit) {
            layersList.add(Integer.valueOf(string.trim()));
        }
        LearningState learningState = new LearningState();
        learningState.layers = layersList.stream()
                .mapToInt(Integer::intValue)
                .toArray();

        double[][][] weightsValues = new double[learningState.layers.length - 1][][];

        for (int i = 1; i < learningState.layers.length; i++) {
            String weights = properties.getProperty("layers.weights." + i);
            if (weights == null) {
                throw new IllegalStateException("Property layers.weights." + i + " doesn't exist");
            }
            String[] neurons = weights.split(";");
            if (neurons.length != learningState.layers[i]) {
                throw new IllegalArgumentException("Not consistent properties, for " + "layers.weights." + i + " expected to have " + learningState.layers[i] + " but it is " + neurons.length);
            }
            double[][] neuronsArr = new double[neurons.length][];

            for (int j = 0; j < neurons.length; j++) {
                String neuron = neurons[j];
                String[] synapsesSplit = neuron.split(",");
                if (synapsesSplit.length != learningState.layers[i - 1] + 1) {
                    throw new IllegalArgumentException("Not consistent properties, for " + "layers.weights." + i + " expected to have synapses " + learningState.layers[i - 1] + " but it is " + synapsesSplit.length);
                }
                double[] synapsesArr = new double[synapsesSplit.length];

                for (int k = 0; k < synapsesSplit.length; k++) {
                    String synapse = synapsesSplit[k];
                    synapsesArr[k] = Double.valueOf(synapse.trim());
                }

                neuronsArr[j] = synapsesArr;
            }

            weightsValues[i - 1] = neuronsArr;
        }
        learningState.weights = weightsValues;

        String actFuncsStr = properties.getProperty("actFuncs");
        if (actFuncsStr == null) {
            throw new IllegalStateException("Property actFuncs doesn't exist");
        }
        String[] actFuncsSplit = actFuncsStr.split(",");
        if (actFuncsSplit.length != learningState.layers.length - 1) {
            throw new IllegalArgumentException("Not consistent properties, for " + "actFuncs expected to have number " + (learningState.layers.length - 1) + " but it is " + actFuncsSplit.length);

        }
        Class<?>[] actFuncs = new Class<?>[learningState.layers.length - 1];
        for (int i = 0; i < actFuncsSplit.length; i++) {
            String actFunc = actFuncsSplit[i];
            try {
                Class<?> clazz = Class.forName(actFunc.trim());
                actFuncs[i] = clazz;
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Unable to find class " + actFunc + " for activation function");
            }
        }
        learningState.actFuncs = actFuncs;

        return learningState;
    }
}
