package cz.jmare.util;

public class BPInconsistentData extends RuntimeException {
    private static final long serialVersionUID = 1043328725320960941L;

    public BPInconsistentData(String message) {
        super(message);
    }

}
