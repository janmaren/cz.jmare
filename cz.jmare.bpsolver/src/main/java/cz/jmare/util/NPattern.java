package cz.jmare.util;

import java.util.Arrays;

public class NPattern {
    public double[] input;
    public double[] target;

    public NPattern(double[] input, double[] target) {
        super();
        this.input = input;
        this.target = target;
    }

    @Override
    public String toString() {
        return "NPattern [input: " + Arrays.toString(input) + ", target: " + Arrays.toString(target) + "]";
    }
}
