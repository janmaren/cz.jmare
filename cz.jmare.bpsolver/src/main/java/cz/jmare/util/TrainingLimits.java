package cz.jmare.util;

/**
 * All fields are nullable. For interpretation, there is logical OR, at least one criteria must be accomplished
 */
public class TrainingLimits {
    private Integer maxSeconds;

    private Integer maxEpochs;

    private Double maxMse;

    public Integer getMaxSeconds() {
        return maxSeconds;
    }

    public TrainingLimits maxSeconds(Integer maxSeconds) {
        this.maxSeconds = maxSeconds;
        return this;
    }

    public Integer getMaxEpochs() {
        return maxEpochs;
    }

    public TrainingLimits maxEpochs(Integer maxEpochs) {
        this.maxEpochs = maxEpochs;
        return this;
    }

    public Double getMaxMse() {
        return maxMse;
    }

    public TrainingLimits maxMse(Double maxMse) {
        this.maxMse = maxMse;
        return this;
    }
}
