package cz.jmare.util;

public interface LearningProgressConsumer {
    void accept(int epochs, double mse, long durationMillis);
}
