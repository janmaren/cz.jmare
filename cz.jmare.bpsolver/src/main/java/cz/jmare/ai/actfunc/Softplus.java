package cz.jmare.ai.actfunc;

/**
 * (0, inf)
 */
public class Softplus implements ActFunc {

    @Override
    public double apply(double x) {
        return Math.log(1 + Math.exp(x));
    }

    @Override
    public double applyDerivation(double x) {
        return 1.0 / (1 + Math.exp(-x));
    }
}
