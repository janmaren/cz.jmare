package cz.jmare.ai.actfunc;

public interface ActFunc {
    double apply(double n);

    /**
     * Derivation for backward propagation
     * @param n
     * @return
     */
    double applyDerivation(double n);
}
