package cz.jmare.ai.actfunc;

public class Hardlims implements ActFunc {

    @Override
    public double apply(double n) {
        if (n < 0) {
            return -1;
        }
        return 1;
    }

    @Override
    public double applyDerivation(double n) {
        return 0;
    }

}
