package cz.jmare.ai.actfunc;

/**
 * {-1, 0, 1}
 */
public class Satlins implements ActFunc {

    @Override
    public double apply(double n) {
        if (n < 0) {
            return -1;
        }
        if (n > 1) {
            return 1;
        }
        return n;
    }

    @Override
    public double applyDerivation(double n) {
        if (-1 <= n && n <= 1) {
            return 1;
        }
        return 0;
    }

}
