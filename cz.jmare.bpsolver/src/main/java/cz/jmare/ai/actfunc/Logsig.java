package cz.jmare.ai.actfunc;

/**
 * Logistic (sigmoid)
 * (0, 1)
 */
public class Logsig implements ActFunc {

    @Override
    public double apply(double n) {
        return 1.0d / (1.0d + Math.exp(-n));
    }

    @Override
    public double applyDerivation(double n) {
        double a = apply(n);
        return a * (1 - a);
    }
}
