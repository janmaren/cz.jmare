package cz.jmare.ai.actfunc;

/**
 * (-1, 1)
 * Maybe same as Tansig
 */
public class Tanh implements ActFunc {

    @Override
    public double apply(double x) {
        double ex = Math.exp(x);
        double emx = Math.exp(-x);
        return (ex - emx) / (ex + emx);
    }

    @Override
    public double applyDerivation(double n) {
        double a = apply(n);
        return 1.0d - a * a;
    }
}
