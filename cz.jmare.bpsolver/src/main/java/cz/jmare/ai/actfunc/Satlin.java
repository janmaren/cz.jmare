package cz.jmare.ai.actfunc;

public class Satlin implements ActFunc {

    @Override
    public double apply(double n) {
        if (n < 0) {
            return 0;
        }
        if (n > 1) {
            return 1;
        }
        return n;
    }

    @Override
    public double applyDerivation(double n) {
        if (0 <= n && n <= 1) {
            return 1;
        }
        return 0;
    }

}
