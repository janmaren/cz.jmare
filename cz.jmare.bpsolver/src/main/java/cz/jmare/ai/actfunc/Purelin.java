package cz.jmare.ai.actfunc;

/**
 * Identity
 * (-inf, inf)
 */
public class Purelin implements ActFunc {

    @Override
    public double apply(double n) {
        return n;
    }

    @Override
    public double applyDerivation(double n) {
        return 1;
    }

}
