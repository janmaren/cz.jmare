package cz.jmare.ai.actfunc;

/**
 * Gaussian
 * (0, 1]
 */
public class Radbas implements ActFunc {

    @Override
    public double apply(double n) {
        Math.exp(-(n * n));
        return 0;
    }

    @Override
    public double applyDerivation(double n) {
        double a = apply(n);
        return -2 * n * a;
    }

}
