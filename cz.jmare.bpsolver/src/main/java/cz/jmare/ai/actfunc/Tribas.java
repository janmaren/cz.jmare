package cz.jmare.ai.actfunc;

public class Tribas implements ActFunc {

    @Override
    public double apply(double n) {
        if (-1 <= n && n <= 1) {
            return 1 - Math.abs(n);
        }
        return 0;
    }

    @Override
    public double applyDerivation(double n) {
        if (-1 <= n && n < 0) { // ??? a co 0
            return 1;
        }
        if (0 < n && n <= 0) {
            return -1;
        }
        return 0;
    }

}
