package cz.jmare.ai.actfunc;

/**
 * (-1, 1) ???
 */
public class Tansig implements ActFunc {

    @Override
    public double apply(double n) {
        double exp2t = Math.exp(2 * n);
        return (exp2t - 1) / (exp2t + 1);
    }

    @Override
    public double applyDerivation(double n) {
        double a = apply(n);
        return 1.0d - a * a;
    }

}
