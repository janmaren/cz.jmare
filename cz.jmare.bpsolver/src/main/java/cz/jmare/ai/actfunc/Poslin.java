package cz.jmare.ai.actfunc;

/**
 * Rectified linear unit
 * [0, inf)
 */
public class Poslin implements ActFunc {

    @Override
    public double apply(double n) {
        if (n < 0) {
            return 0;
        }
        return n;
    }

    @Override
    public double applyDerivation(double n) {
        if (0 <= n) {
            return 1;
        }
        return 0;
    }

}
