package cz.jmare.ai.actfunc;

/**
 * Binary step
 * {0, 1}
 */
public class Hardlim implements ActFunc {

    @Override
    public double apply(double n) {
        if (n < 0) {
            return 0;
        }
        return 1;
    }

    @Override
    public double applyDerivation(double n) {
        return 0;
    }

}
