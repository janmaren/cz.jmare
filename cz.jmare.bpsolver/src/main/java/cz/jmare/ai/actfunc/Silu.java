package cz.jmare.ai.actfunc;

/**
 * Sigmoid linear unit
 * [-0.278, inf)
 */
public class Silu implements ActFunc {

    @Override
    public double apply(double x) {
        return x / (1.0d + Math.exp(-x));
    }

    @Override
    public double applyDerivation(double x) {
        return (1.0d + Math.exp(-x) + x * Math.exp(-x)) / ((1.0d + Math.exp(-x)) * (1.0d + Math.exp(-x)));
    }
}
