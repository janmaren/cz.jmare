package cz.jmare.ai;

import java.util.Arrays;
import java.util.stream.Collectors;

public class LearningState {
    public int[] layers;
    public double[][][] weights;
    public Class<?>[] actFuncs;

    @Override
    public String toString() {
        return "LearningState [layers=" + Arrays.toString(layers)
                + ", actFuncs=" + Arrays.asList(actFuncs).stream().map(cl -> cl.getSimpleName()).collect(Collectors.joining(", ")) + "]";
    }
}
