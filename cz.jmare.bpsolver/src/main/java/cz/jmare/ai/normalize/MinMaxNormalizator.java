package cz.jmare.ai.normalize;

public class MinMaxNormalizator implements Normalizator {

    private double[] mins;
    private double[] maxs;
    /**
     * A value between 0 and 1 for values where min and max is same
     */
    private double defaultValue;

    public MinMaxNormalizator(double[][] rawData) {
        if (rawData.length == 0) {
            throw new IllegalArgumentException("Empty array passed as rawData");
        }
        int columnsCount = rawData[0].length;
        double[] mins = new double[columnsCount];
        double[] maxs = new double[columnsCount];
        for (int i = 0; i < maxs.length; i++) {
            maxs[i] = -Double.MAX_VALUE;
        }
        for (int i = 0; i < mins.length; i++) {
            mins[i] = Double.MAX_VALUE;
        }

        for (int i = 0; i < rawData.length; i++) {
            double[] input = rawData[i];
            for (int j = 0; j < input.length; j++) {
                double d = input[j];
                if (d < mins[j]) {
                    mins[j] = d;
                }
                if (d > maxs[j]) {
                    maxs[j] = d;
                }
            }
        }
        this.mins = mins;
        this.maxs = maxs;
    }

    public MinMaxNormalizator(double[] mins, double[] maxs) {
        this.mins = mins;
        this.maxs = maxs;
    }

    public MinMaxNormalizator(int count, double min, double max) {
        double[] mins = new double[count];
        double[] maxs = new double[count];
        for (int i = 0; i < maxs.length; i++) {
            maxs[i] = max;
        }
        for (int i = 0; i < mins.length; i++) {
            mins[i] = min;
        }
    }

    @Override
    public double[][] normalize(double[][] rawData) {
        double[][] output = new double[rawData.length][];
        for (int i = 0; i < rawData.length; i++) {
            double[] input = rawData[i];
            output[i] = new double[rawData[0].length];
            for (int j = 0; j < input.length; j++) {
                double dif = maxs[j] - mins[j];
                double normalized = defaultValue;
                if (dif > 0) {
                    if (input[j] < mins[j]) {
                        normalized = 0;
                    } else if (input[j] > maxs[j]) {
                        normalized = 1;
                    } else {
                        normalized = (input[j] - mins[j]) / (maxs[j] - mins[j]);
                    }
                }
                output[i][j] = normalized;
            }
        }
        return output;
    }

    @Override
    public double[] getMins() {
        return mins;
    }

    @Override
    public double[] getMaxs() {
        return maxs;
    }

    public void setDefaultValue(double defaultValue) {
        this.defaultValue = defaultValue;
    }

}
