package cz.jmare.ai.normalize;

public interface Normalizator {
    double[][] normalize(double[][] rawData);

    double[] getMins();

    double[] getMaxs();
}
