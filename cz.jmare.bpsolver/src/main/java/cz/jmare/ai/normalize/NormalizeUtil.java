package cz.jmare.ai.normalize;

public class NormalizeUtil {
    public static double[][] normalize01(double[][] inputs) {
        double[][] output = new double[inputs.length][];
        if (inputs.length == 0) {
            return output;
        }
        int columnsCount = inputs[0].length;
        double[] mins = new double[columnsCount];
        double[] maxs = new double[columnsCount];
        for (int i = 0; i < maxs.length; i++) {
            maxs[i] = -Double.MAX_VALUE;
        }
        for (int i = 0; i < mins.length; i++) {
            mins[i] = Double.MAX_VALUE;
        }

        for (int i = 0; i < inputs.length; i++) {
            double[] input = inputs[i];
            for (int j = 0; j < input.length; j++) {
                double d = input[j];
                if (d < mins[j]) {
                    mins[j] = d;
                }
                if (d > maxs[j]) {
                    maxs[j] = d;
                }
            }
        }

        for (int i = 0; i < inputs.length; i++) {
            double[] input = inputs[i];
            output[i] = new double[columnsCount];
            for (int j = 0; j < input.length; j++) {
                double dif = maxs[j] - mins[j];
                double normalized = 0;
                if (dif > 0) {
                    normalized = (input[j] - mins[j]) / (maxs[j] - mins[j]);
                }
                output[i][j] = normalized;
            }
        }

        return output;
    }

    public static void main(String[] args) {
        double[][] data = new double[3][3];
        data[0] = new double[] {1, 2, 3};
        data[1] = new double[] {-1, 2, -3};
        data[2] = new double[] {-0.5, 1, -2};

        MinMaxNormalizator minMaxNormalizator = new MinMaxNormalizator(data);

        double[][] data2 = new double[4][3];
        data2[0] = new double[] {1, 2, 3};
        data2[1] = new double[] {-1, 2, -3};
        data2[2] = new double[] {-0.5, 1, -2};
        data2[3] = new double[] {-5, 5, 0};

        double[][] normalize01 = minMaxNormalizator.normalize(data2);
        //double[][] normalize01 = normalize01(data);
        for (int i = 0; i < normalize01.length; i++) {
            double[] ds = normalize01[i];
            for (int j = 0; j < ds.length; j++) {
                double d = ds[j];
                System.out.println("[" + i + "][" + j + "]=" + d);
            }
        }
    }
}
