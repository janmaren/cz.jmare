package cz.jmare.ai.util;

import cz.jmare.ai.BPSolver;
import cz.jmare.ai.actfunc.Logsig;
import cz.jmare.ai.actfunc.Tansig;

public class BPSolverUtil {
    public static void applySmartParameters(BPSolver bpSolver) {
        bpSolver.setMaxIterations(500000);
        bpSolver.setMaxMse(0.0001);
        bpSolver.setHiddenLayeraActFunc(new Tansig());
        bpSolver.setOutputLayerActFunc(new Logsig());
        bpSolver.setCheckConvergenceAfterEpochs(1000);
        bpSolver.setConvergenceThreshold(0.15);
    }
}
