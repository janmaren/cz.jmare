package cz.jmare.ai.util;

import cz.jmare.ai.layer.InputNodeLayer;
import cz.jmare.ai.layer.NeuronNodeLayer;
import cz.jmare.util.NPattern;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

public class PrintBPStateUtil {
    public static void printTraining(InputNodeLayer inputNodeLayer, NeuronNodeLayer[] neuronNodeLayers,
                                     double[][] inputs, double[][] targets) {
        for (int i = 0; i < inputs.length; i++) {
            for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
                neuronNodeLayer.clearOutputs();
            }
            inputNodeLayer.setValues(inputs[i]);
            String[] inputStrs = Arrays.stream(inputs[i]).mapToObj(d -> toScaledDouble(d, 5)).toArray(String[]::new);
            String[] targetStrs = Arrays.stream(targets[i]).mapToObj(d -> toScaledDouble(d, 5)).toArray(String[]::new);
            double[] results = Arrays.stream(neuronNodeLayers[neuronNodeLayers.length - 1].getNodes()).map(n -> n.getOutput()).mapToDouble(Double::doubleValue).toArray();
            String[] resultsStrs = Arrays.stream(neuronNodeLayers[neuronNodeLayers.length - 1].getNodes()).map(n -> toScaledDouble(n.getOutput(), 5)).toArray(String[]::new);
            double mse = calcMse(targets[i], results);
            System.out.println("Inputs:" + Arrays.toString(inputStrs) + ", result: " + Arrays.toString(resultsStrs) + ", target: "
                    + Arrays.toString(targetStrs) + ", error: " + toScaledDouble(mse, 5) + "/" + toScaledDouble(Math.sqrt(mse), 5));
        }
    }

    public static void printTraining(InputNodeLayer inputNodeLayer, NeuronNodeLayer[] neuronNodeLayers,
                                     List<NPattern> patterns) {
        for (NPattern pattern : patterns) {
            for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
                neuronNodeLayer.clearOutputs();
            }
            inputNodeLayer.setValues(pattern.input);
            String[] inputStrs = Arrays.stream(pattern.input).mapToObj(d -> toScaledDouble(d, 5)).toArray(String[]::new);
            String[] targetStrs = Arrays.stream(pattern.target).mapToObj(d -> toScaledDouble(d, 5)).toArray(String[]::new);
            double[] results = Arrays.stream(neuronNodeLayers[neuronNodeLayers.length - 1].getNodes()).map(n -> n.getOutput()).mapToDouble(Double::doubleValue).toArray();
            String[] resultsStrs = Arrays.stream(neuronNodeLayers[neuronNodeLayers.length - 1].getNodes()).map(n -> toScaledDouble(n.getOutput(), 5)).toArray(String[]::new);
            double mse = calcMse(pattern.target, results);
            System.out.println("Inputs:" + Arrays.toString(inputStrs) + ", result: " + Arrays.toString(resultsStrs) + ", target: "
                    + Arrays.toString(targetStrs) + ", error: " + toScaledDouble(mse, 5) + "/" + toScaledDouble(Math.sqrt(mse), 5));
        }
    }

    public static double calcMse(double[] target, double result[]) {
        double mse = 0;
        for (int i = 0; i < target.length; i++) {
            double diff = target[i] - result[i];
            mse += diff * diff;
        }
        return mse / target.length;
    }

    public static String toScaledDouble(double value, int scale) {
        BigDecimal bd = new BigDecimal(value);
        if (bd.scale() > scale) {
            bd = bd.setScale(scale, RoundingMode.HALF_UP);
        }
        return bd.toString();
    }
}
