package cz.jmare.ai.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ToStringUtil {
    public static String format(double value) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("0.0000", decimalFormatSymbols);
        return (value >= 0 ? " " : "") + decimalFormat.format(value);
    }
}
