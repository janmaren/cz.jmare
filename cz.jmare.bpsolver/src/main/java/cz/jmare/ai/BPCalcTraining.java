package cz.jmare.ai;

import cz.jmare.ai.util.PrintBPStateUtil;
import cz.jmare.util.BPInterruptedException;
import cz.jmare.util.LearningProgressConsumer;
import cz.jmare.util.NPattern;
import cz.jmare.util.TrainingLimits;
import cz.jmare.util.TrainingResult;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BPCalcTraining {
    /**
     * Train using input and target patterns. The result state will be present in the bpCalc instance
     * @param bpCalc configured BPCalc instance
     * @param patterns inputs and targets
     * @param trainingLimits limits after which the processing will be stopped
     * @param consumer consumer called after each training epoch together with MSE
     * @return TrainingResult with last calculated values
     */
    public static TrainingResult train(BPCalc bpCalc, List<NPattern> patterns,
                                       TrainingLimits trainingLimits, LearningProgressConsumer consumer) {
        double lastMse = 0;
        Thread currentThread = Thread.currentThread();
        int maxEpochs = trainingLimits == null || trainingLimits.getMaxEpochs() == null ? Integer.MAX_VALUE : trainingLimits.getMaxEpochs();
        double maxMse = trainingLimits == null || trainingLimits.getMaxMse() == null ? 0 : trainingLimits.getMaxMse();
        long end = Long.MAX_VALUE;
        long startMillis = System.currentTimeMillis();
        if (trainingLimits != null && trainingLimits.getMaxSeconds() != null) {
            end = startMillis + trainingLimits.getMaxSeconds() * 1000;
        }
        TrainingResult trainingResult = new TrainingResult();

        int epochNumber = 1;
        for (; epochNumber <= maxEpochs; epochNumber++) {
            double mses = 0;
            for (NPattern pattern : patterns) {
                mses += bpCalc.train(pattern.input, pattern.target);
            }
            lastMse = mses / patterns.size();

            long nowMillis = System.currentTimeMillis();
            if (consumer != null) {
                consumer.accept(epochNumber, lastMse, nowMillis - startMillis);
            }

            if (lastMse < maxMse) {
                break;
            }

            if (nowMillis >= end) {
                break;
            }

            if (currentThread.isInterrupted()) {
                trainingResult.setInterrupted(true);
                break;
            }
        }

        trainingResult.setEpochs(epochNumber);
        trainingResult.setMse(lastMse);
        trainingResult.setSeconds((System.currentTimeMillis() - startMillis) / 1000.0);

        return trainingResult;
    }


    public static void debugTrainForSeconds(BPCalc bpCalc, List<NPattern> patterns,
                                         int seconds, int stepEpochs) throws BPInterruptedException {
        System.out.println("========== start ============");
        AtomicInteger ite = new AtomicInteger();
        TrainingResult result = train(bpCalc, patterns, new TrainingLimits().maxSeconds(seconds), (epochs, mse, duration) -> {
            ite.set(epochs);
            if (epochs % stepEpochs != 0) {
                return;
            }
            System.out.println("Epochs: " + epochs);
            System.out.println("MSE: " + mse + " / " + Math.sqrt(mse));
            PrintBPStateUtil.printTraining(bpCalc.getInputNodeLayer(), bpCalc.getNeuronNodeLayers(), patterns);
        });
        System.out.println("---------------------");

        System.out.println("Epochs: " + result.getEpochs());
        System.out.println("MSE: " + result.getMse());
        System.out.println("Seconds: " + result.getSeconds());

        System.out.println("========== end ============");
    }
}
