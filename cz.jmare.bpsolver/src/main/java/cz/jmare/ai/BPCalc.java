package cz.jmare.ai;

import cz.jmare.ai.actfunc.ActFunc;
import cz.jmare.ai.actfunc.Logsig;
import cz.jmare.ai.actfunc.Tanh;
import cz.jmare.ai.layer.InputNodeLayer;
import cz.jmare.ai.layer.NeuronNodeLayer;
import cz.jmare.ai.layer.NodeLayer;
import cz.jmare.ai.node.NeuronNode;
import cz.jmare.alg.BackpropagateAlgorithm;

import java.lang.reflect.InvocationTargetException;

/**
 * Main AI class using back propagation algorithm<br>
 */
public class BPCalc {
    private InputNodeLayer inputNodeLayer;

    private NeuronNodeLayer[] neuronNodeLayers;

    private BackpropagateAlgorithm backpropagateAlgorithm;

    public BPCalc(int... layersDimensions) {
        if (layersDimensions.length < 2) {
            throw new IllegalArgumentException("There must be at least 2 layers");
        }
        inputNodeLayer = new InputNodeLayer(layersDimensions[0]);
        NodeLayer nodeLayer = inputNodeLayer;
        neuronNodeLayers = new NeuronNodeLayer[layersDimensions.length - 1];
        for (int i = 1; i < layersDimensions.length; i++) {
            nodeLayer = new NeuronNodeLayer(nodeLayer, layersDimensions[i]);
            neuronNodeLayers[i - 1] = (NeuronNodeLayer) nodeLayer;
        }

        backpropagateAlgorithm = new BackpropagateAlgorithm(neuronNodeLayers);

        setHiddenLayeraActFunc(new Tanh());
        setOutputLayerActFunc(new Logsig());
        setLearningRate(0.4 + 2 / 3.0);
    }

    public BPCalc(LearningState learningState) {
        this(learningState.layers);
        setLearningState(learningState);
    }

    public BPCalc(InputNodeLayer inputNodeLayer, NeuronNodeLayer[] neuronNodeLayers) {
        this.inputNodeLayer = inputNodeLayer;
        this.neuronNodeLayers = neuronNodeLayers;
        backpropagateAlgorithm = new BackpropagateAlgorithm(neuronNodeLayers);
    }
    
    public LearningState getLearningState() {
        LearningState learningState = new LearningState();
        learningState.actFuncs = new Class[neuronNodeLayers.length];
        learningState.layers = new int[neuronNodeLayers.length + 1];
        learningState.layers[0] = inputNodeLayer.getNodes().length;
        double[][][] weights = new double[neuronNodeLayers.length][][];
        for (int i = 0; i < neuronNodeLayers.length; i++) {
            weights[i] = neuronNodeLayers[i].getWeights();
            learningState.actFuncs[i] = neuronNodeLayers[i].getActFunc().getClass();
            learningState.layers[i + 1] = neuronNodeLayers[i].getNodes().length;
        }
        learningState.weights = weights;
        return learningState;
    }

    public void setLearningState(LearningState learningState) {
        if (neuronNodeLayers.length != learningState.layers.length - 1) {
            throw new IllegalStateException("Not setting the same layers dimensions");
        }
        for (int i = 0; i < neuronNodeLayers.length; i++) {
            NeuronNodeLayer neuronNodeLayer = neuronNodeLayers[i];
            neuronNodeLayer.setWeights(learningState.weights[i]);

            Class<?> actFuncClass = learningState.actFuncs[i];
            try {
                ActFunc actFunc = (ActFunc) actFuncClass.getDeclaredConstructor().newInstance();
                neuronNodeLayer.setActFunc(actFunc);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                throw new IllegalStateException("Unable to create instance of " + actFuncClass);
            }
        }
    }

    /**
     * Train using one pattern and return MSE
     * @param input data for input layer
     * @param output target values
     * @return MSE
     */
    public double train(double[] input, double[] output) {
        for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
            neuronNodeLayer.clearOutputs();
        }
        inputNodeLayer.setValues(input);
        return backpropagateAlgorithm.run(output);
    }

    public double[] solve(double... input) {
        for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
            neuronNodeLayer.clearOutputs();
        }
        inputNodeLayer.setValues(input);
        NeuronNode[] neuronNodes = neuronNodeLayers[neuronNodeLayers.length - 1].getNodes();
        double[] output = new double[neuronNodes.length];
        for (int i = 0; i < neuronNodes.length; i++) {
            output[i] = neuronNodes[i].getOutput();
        }
        return output;
    }

    public InputNodeLayer getInputNodeLayer() {
        return inputNodeLayer;
    }

    public void setInputNodeLayer(InputNodeLayer inputNodeLayer) {
        this.inputNodeLayer = inputNodeLayer;
    }

    public NeuronNodeLayer[] getNeuronNodeLayers() {
        return neuronNodeLayers;
    }

    public void setNeuronNodeLayers(NeuronNodeLayer[] neuronNodeLayers) {
        this.neuronNodeLayers = neuronNodeLayers;
    }

    public void setLearningRate(double learningRate) {
        backpropagateAlgorithm.setLearningRate(learningRate);
    }

    public void setMomentum(double momentum) {
        backpropagateAlgorithm.setMomentum(momentum);
    }

    public double getLearningRate() {
        return backpropagateAlgorithm.getLearningRate();
    }

    public double getMomentum() {
        return backpropagateAlgorithm.getMomentum();
    }

    public void setHiddenLayeraActFunc(ActFunc actFunc) {
        for (int i = 0; i < neuronNodeLayers.length - 1; i++) {
            neuronNodeLayers[i].setActFunc(actFunc);
        }
    }

    public void setOutputLayerActFunc(ActFunc actFunc) {
        neuronNodeLayers[neuronNodeLayers.length - 1].setActFunc(actFunc);
    }

    /**
     * Main method to initialize network with default (random) values. Method can be used to reset BPCalc before
     * new training
     */
    public void randomizeWeights() {
        for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
            neuronNodeLayer.randomizeWeights();
        }
    }
}
