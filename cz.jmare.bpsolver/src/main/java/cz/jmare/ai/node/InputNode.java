package cz.jmare.ai.node;

import cz.jmare.ai.util.ToStringUtil;

public class InputNode extends Node {
    private double value;

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public double getOutput() {
        return value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("InputNode o: ").append(ToStringUtil.format(value)).append("\n");
        return sb.toString();
    }

    @Override
    public void addError(double increment) {
        // input nodes have no weights so error needn't be prepared
    }
}
