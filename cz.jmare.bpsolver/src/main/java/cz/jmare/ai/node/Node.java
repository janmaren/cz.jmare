package cz.jmare.ai.node;

public abstract class Node {
    public abstract double getOutput();

    /**
     * Error for backpropagation algorithm
     * This is not used to solve output
     */
    private double error;


    public double getError() {
        return error;
    }

    public void addError(double increment) {
        error += increment;
    }

    public void clearError() {
        error = 0;
    }
}
