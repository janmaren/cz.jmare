package cz.jmare.ai.node;

import cz.jmare.ai.actfunc.ActFunc;
import cz.jmare.ai.actfunc.Tansig;
import cz.jmare.ai.util.ToStringUtil;

public class NeuronNode extends Node {
    /**
     * All input synapses<br>
     * Last synapse is bias with input value 1
     */
    private Synapse[] synapses;

    private ActFunc actFunc = new Tansig();

    /**
     * Cache for output. To solve new input values it should be cleared first.
     */
    private Double output;

    private Double rawOutput;

    public NeuronNode(Node[] leftNodes) {
        synapses = new Synapse[leftNodes.length + 1];

        for (int i = 0; i < leftNodes.length; i++) {
            synapses[i] = new Synapse(leftNodes[i]);
        }

        Synapse biasSynapse = new Synapse();
        biasSynapse.inputNode = new BiasNode();
        synapses[synapses.length - 1] = biasSynapse;

        randomizeWeights();
    }

    public void randomizeWeights() {
        for (Synapse synapse : synapses) {
            synapse.weight = (Math.random() - 0.5f) * 4d;
        }
    }

    public double getOutput() {
        if (output != null) {
            return output;
        }
        rawOutput = 0d;
        for (Synapse synapse : synapses) {
            rawOutput += synapse.inputNode.getOutput() * synapse.weight;
        }
        output = actFunc.apply(rawOutput);
        return output;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("NeuronNode o: ").append(ToStringUtil.format(getOutput())).append("\n");
        for (Synapse synapse : synapses) {
            sb.append("    ").append(synapse).append("\n");
        }
        return sb.toString();
    }

    public void clearOutput() {
        output = null;
        rawOutput = null;
    }

    public static class Synapse {
        public Node inputNode;

        public double weight;

        public double dweight;

        public Synapse() {
        }

        public Synapse(Node inputNode) {
            super();
            this.inputNode = inputNode;
        }

        public Synapse(Node node, double weight) {
            super();
            this.inputNode = node;
            this.weight = weight;
        }

        @Override
        public String toString() {
            double output = inputNode.getOutput();
            return "[x:" + ToStringUtil.format(output) + "  *  w:" + ToStringUtil.format(weight) + "  = " + ToStringUtil.format(output * weight) + "]";
        }
    }

    public void setActFunc(ActFunc actFunc) {
        this.actFunc = actFunc;
    }

    public double getDerivatedOutput() {
        return actFunc.applyDerivation(rawOutput);
    }

    public Synapse[] getSynapses() {
        return synapses;
    }

    public ActFunc getActFunc() {
        return actFunc;
    }
}
