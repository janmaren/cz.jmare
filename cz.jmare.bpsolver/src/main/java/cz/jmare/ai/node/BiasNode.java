package cz.jmare.ai.node;

public class BiasNode extends Node {

    @Override
    public double getOutput() {
        return 1;
    }

}
