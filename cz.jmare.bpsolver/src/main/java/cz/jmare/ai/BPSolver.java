package cz.jmare.ai;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.BiConsumer;

import cz.jmare.ai.actfunc.ActFunc;
import cz.jmare.ai.layer.InputNodeLayer;
import cz.jmare.ai.layer.NeuronNodeLayer;
import cz.jmare.ai.layer.NodeLayer;
import cz.jmare.ai.node.NeuronNode;
import cz.jmare.alg.BackpropagateAlgorithm;
import cz.jmare.util.BPInconsistentData;
import cz.jmare.util.BPInterruptedException;
import cz.jmare.util.NPattern;

/**
 * Main AI class using back propagation algorithm<br>
 * Use:
 * <pre>
 *      BPSolver bpSolver = new BPSolver(2, 2, 1); // layers dimensions. The first is input and the last is output
 *      BPSolverUtil.applySmartParameters(bpSolver); // do not use default values but anothers
 *      bpSolver.train(inputs, targets); // train using input values and desired output values
 *      double[] outputs = bpSolver.solve(0, 1); // use some input values, like 0, 1 and get output 
 * </pre>
 * But for solving trained network use rather DAISolver with much better performance to evaluate:
 * <pre>
 *      BPSolver bpSolver = new BPSolver(2, 2, 1); // layers dimensions. The first is input and the last is output
 *      BPSolverUtil.applySmartParameters(bpSolver); // do not use default values but anothers
 *      bpSolver.train(inputs, targets); // train using input values and desired output values
 *      DAISolver daiSolver = new DAISolver(bpSolver.getLearningState());
 *      double[] outputs = daiSolver.solve(0, 1); // use some input values, like 0, 1 and get output 
 * </pre> 
 */
public class BPSolver {
    private InputNodeLayer inputNodeLayer;

    private NeuronNodeLayer[] neuronNodeLayers;

    private double learningRate = 0.3;

    private double momentum = 0.6;

    private Double maxMse;

    private int maxIterations = 10000;

    private BackpropagateAlgorithm backpropagateAlgorithm;

    private double lastMse;

    private int lastEpochNumber;

    /**
     * After how many iterations to check MSE is under threshold
     */
    private Integer checkConvergenceAfterEpochs;

    /**
     * MSE must be under threshold after {@link #checkConvergenceAfterEpochs} otherwise rerandomize weights
     */
    private double convergenceThreshold = 0.15;

    private BiConsumer<Integer, Double> mseConsumer;

    public BPSolver(int... layersDimensions) {
        if (layersDimensions.length < 2) {
            throw new IllegalArgumentException("There must be at least 2 layers");
        }
        inputNodeLayer = new InputNodeLayer(layersDimensions[0]);
        NodeLayer nodeLayer = inputNodeLayer;
        neuronNodeLayers = new NeuronNodeLayer[layersDimensions.length - 1];
        for (int i = 1; i < layersDimensions.length; i++) {
            nodeLayer = new NeuronNodeLayer(nodeLayer, layersDimensions[i]);
            neuronNodeLayers[i - 1] = (NeuronNodeLayer) nodeLayer;
        }

        backpropagateAlgorithm = new BackpropagateAlgorithm(neuronNodeLayers);
    }

    public BPSolver(LearningState learningState) {
        this(learningState.layers);
        setLearningState(learningState);
    }

    public BPSolver(InputNodeLayer inputNodeLayer, NeuronNodeLayer[] neuronNodeLayers) {
        this.inputNodeLayer = inputNodeLayer;
        this.neuronNodeLayers = neuronNodeLayers;
        backpropagateAlgorithm = new BackpropagateAlgorithm(neuronNodeLayers);
    }
    
    public LearningState getLearningState() {
        LearningState learningState = new LearningState();
        learningState.actFuncs = new Class[neuronNodeLayers.length];
        learningState.layers = new int[neuronNodeLayers.length + 1];
        learningState.layers[0] = inputNodeLayer.getNodes().length;
        double[][][] weights = new double[neuronNodeLayers.length][][];
        for (int i = 0; i < neuronNodeLayers.length; i++) {
            weights[i] = neuronNodeLayers[i].getWeights();
            learningState.actFuncs[i] = neuronNodeLayers[i].getActFunc().getClass();
            learningState.layers[i + 1] = neuronNodeLayers[i].getNodes().length;
        }
        learningState.weights = weights;
        return learningState;
    }

    public void setLearningState(LearningState learningState) {
        if (neuronNodeLayers.length != learningState.layers.length - 1) {
            throw new IllegalStateException("Not setting the same layers dimensions");
        }
        for (int i = 0; i < neuronNodeLayers.length; i++) {
            NeuronNodeLayer neuronNodeLayer = neuronNodeLayers[i];
            neuronNodeLayer.setWeights(learningState.weights[i]);

            Class<?> actFuncClass = learningState.actFuncs[i];
            try {
                ActFunc actFunc = (ActFunc) actFuncClass.getDeclaredConstructor().newInstance();
                neuronNodeLayer.setActFunc(actFunc);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                throw new IllegalStateException("Unable to create instance of " + actFuncClass);
            }
        }
    }

    public void train(List<NPattern> nPatterns) {
        double[][] inputs = new double[nPatterns.size()][];
        double[][] targets = new double[nPatterns.size()][];
        for (int i = 0; i < targets.length; i++) {
            NPattern nPattern = nPatterns.get(i);
            inputs[i] = nPattern.input;
            targets[i] = nPattern.target;
        }

        train(inputs, targets);
    }

    /**
     * Train using as much epochs as MSE is low enough or max number of iterations reached
     * @param inputs
     * @param targets
     * @throws BPInterruptedException thrown when interrupt signal detected
     */
    public void train(double[][] inputs, double[][] targets) throws BPInterruptedException, BPInconsistentData {
        if (inputs.length == 0 || targets.length == 0) {
            throw new BPInconsistentData("No training data provided");
        }
        if (inputs.length != targets.length) {
            throw new BPInconsistentData("Count of inputs is " + inputs.length + " but count of targets is " + targets.length);
        }
        if (targets[0].length != neuronNodeLayers[neuronNodeLayers.length - 1].getNodes().length) {
            throw new BPInconsistentData("Count of target categories " + targets[0].length + " is different than count of neurons in output layer " + neuronNodeLayers[neuronNodeLayers.length - 1].getNodes().length);
        }
        backpropagateAlgorithm.setLearningRate(learningRate);
        backpropagateAlgorithm.setMomentum(momentum);
        lastMse = 0;
        lastEpochNumber = 0;
        int iterationsToCheckConvergence = 0;
        Thread currentThread = Thread.currentThread();
        for (; lastEpochNumber < maxIterations; lastEpochNumber++, iterationsToCheckConvergence++) {
            double mses = 0;
            for (int j = 0; j < targets.length; j++) {
                for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
                    neuronNodeLayer.clearOutputs();
                }
                inputNodeLayer.setValues(inputs[j]);
                mses += backpropagateAlgorithm.run(targets[j]);
            }
            lastMse = mses / targets.length;
            if (getCheckConvergenceAfterEpochs() != null && iterationsToCheckConvergence >= getCheckConvergenceAfterEpochs() && lastMse > getConvergenceThreshold()) {
                for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
                    neuronNodeLayer.randomizeWeights();
                }
                iterationsToCheckConvergence = 0;
            }
            if (mseConsumer != null) {
                mseConsumer.accept(lastEpochNumber, lastMse);
            }

            if (maxMse != null && lastMse < maxMse) {
                break;
            }

            if (currentThread.isInterrupted()) {
                throw new BPInterruptedException();
            }
        }
    }

    public double[] solve(double... input) {
        for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
            neuronNodeLayer.clearOutputs();
        }
        inputNodeLayer.setValues(input);
        NeuronNode[] neuronNodes = neuronNodeLayers[neuronNodeLayers.length - 1].getNodes();
        double[] output = new double[neuronNodes.length];
        for (int i = 0; i < neuronNodes.length; i++) {
            output[i] = neuronNodes[i].getOutput();
        }
        return output;
    }

    public InputNodeLayer getInputNodeLayer() {
        return inputNodeLayer;
    }

    public void setInputNodeLayer(InputNodeLayer inputNodeLayer) {
        this.inputNodeLayer = inputNodeLayer;
    }

    public NeuronNodeLayer[] getNeuronNodeLayers() {
        return neuronNodeLayers;
    }

    public void setNeuronNodeLayers(NeuronNodeLayer[] neuronNodeLayers) {
        this.neuronNodeLayers = neuronNodeLayers;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getMomentum() {
        return momentum;
    }

    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public Double getMaxMse() {
        return maxMse;
    }

    public void setMaxMse(Double maxMse) {
        this.maxMse = maxMse;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public BackpropagateAlgorithm getBackpropagateAlgorithm() {
        return backpropagateAlgorithm;
    }

    public void setBackpropagateAlgorithm(BackpropagateAlgorithm backpropagateAlgorithm) {
        this.backpropagateAlgorithm = backpropagateAlgorithm;
    }

    public double getLastMse() {
        return lastMse;
    }

    public void setLastMse(double lastMseError) {
        this.lastMse = lastMseError;
    }

    public int getLastEpochNumber() {
        return lastEpochNumber;
    }

    public void setLastEpochNumber(int lastEpochNumber) {
        this.lastEpochNumber = lastEpochNumber;
    }

    public void setHiddenLayeraActFunc(ActFunc actFunc) {
        for (int i = 0; i < neuronNodeLayers.length - 1; i++) {
            neuronNodeLayers[i].setActFunc(actFunc);
        }
    }

    public void setOutputLayerActFunc(ActFunc actFunc) {
        neuronNodeLayers[neuronNodeLayers.length - 1].setActFunc(actFunc);
    }

    public double getConvergenceThreshold() {
        return convergenceThreshold;
    }

    public void setConvergenceThreshold(double convergenceThreshold) {
        this.convergenceThreshold = convergenceThreshold;
    }

    public Integer getCheckConvergenceAfterEpochs() {
        return checkConvergenceAfterEpochs;
    }

    public void setCheckConvergenceAfterEpochs(Integer checkConvergenceAfterEpochs) {
        this.checkConvergenceAfterEpochs = checkConvergenceAfterEpochs;
    }

    public void setMseConsumer(BiConsumer<Integer, Double> mseConsumer) {
        this.mseConsumer = mseConsumer;
    }

    /**
     * Main method to initialize network with default (random) values. Method can be used to reset BPSolver before
     * new training
     */
    public void randomizeWeights() {
        for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
            neuronNodeLayer.randomizeWeights();
        }
    }
}
