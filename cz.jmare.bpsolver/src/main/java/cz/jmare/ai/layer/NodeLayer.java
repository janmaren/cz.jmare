package cz.jmare.ai.layer;

import cz.jmare.ai.node.Node;

public interface NodeLayer {
    Node[] getNodes();
}
