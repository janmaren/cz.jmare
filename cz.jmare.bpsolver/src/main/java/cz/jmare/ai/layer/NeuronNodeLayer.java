package cz.jmare.ai.layer;

import cz.jmare.ai.actfunc.ActFunc;
import cz.jmare.ai.node.NeuronNode;
import cz.jmare.ai.node.NeuronNode.Synapse;
import cz.jmare.ai.node.Node;

/**
 * Layer for hidden or output neurons
 */
public class NeuronNodeLayer implements NodeLayer {
    private NeuronNode[] nodes;

    public NeuronNodeLayer(NodeLayer leftLayer, int number) {
        nodes = new NeuronNode[number];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new NeuronNode(leftLayer.getNodes());
        }
    }

    @Override
    public NeuronNode[] getNodes() {
        return nodes;
    }

    public void clearOutputs() {
        for (NeuronNode neuronNode : nodes) {
            neuronNode.clearOutput();
        }
    }

    public void clearErrors() {
        for (Node neuronNode : nodes) {
            neuronNode.clearError();
        }
    }

    public void setActFunc(ActFunc actFunc) {
        for (NeuronNode neuronNode : nodes) {
            neuronNode.setActFunc(actFunc);
        }
    }

    public ActFunc getActFunc() {
        ActFunc actFunc = nodes[0].getActFunc();
        for (int i = 1; i < nodes.length; i++) {
            if (!nodes[i].getActFunc().getClass().equals(actFunc.getClass())) {
                throw new IllegalStateException("Act functions in layer are not same");
            }
        }
        return actFunc;
    }

    public double[][] getWeights() {
        double[][] weights = new double[nodes.length][];
        for (int i = 0; i < nodes.length; i++) {
            NeuronNode neuronNode = nodes[i];
            Synapse[] synapses = neuronNode.getSynapses();
            weights[i] = new double[synapses.length];
            for (int j = 0; j < synapses.length; j++) {
                weights[i][j] = synapses[j].weight;
            }
        }
        return weights;
    }

    public void setWeights(double[][] weights) {
        if (weights.length != nodes.length) {
            throw new IllegalStateException("Not setting the same number of nodes");
        }
        for (int i = 0; i < nodes.length; i++) {
            NeuronNode neuronNode = nodes[i];
            Synapse[] synapses = neuronNode.getSynapses();
            if (synapses.length != weights[i].length) {
                throw new IllegalStateException("Not setting the same number of synapses");
            }
            for (int j = 0; j < synapses.length; j++) {
                synapses[j].weight = weights[i][j];
            }
        }
    }

    public void randomizeWeights() {
        for (int i = 0; i < nodes.length; i++) {
            NeuronNode neuronNode = nodes[i];
            neuronNode.randomizeWeights();
        }
    }
}
