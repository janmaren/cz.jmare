package cz.jmare.ai.layer;

import cz.jmare.ai.node.InputNode;
import cz.jmare.ai.node.Node;

/**
 * Layer for input values
 */
public class InputNodeLayer implements NodeLayer {
    private InputNode[] nodes;


    public InputNodeLayer(int number) {
        nodes = new InputNode[number];
        for (int i = 0; i < number; i++) {
            nodes[i] = new InputNode();
        }
    }

    public void setValues(double... values) {
        if (nodes.length != values.length) {
            throw new IllegalArgumentException("Argument has invalid length of double values " + values.length + " but it should be " + nodes.length);
        }
        for (int i = 0; i < values.length; i++) {
            nodes[i].setValue(values[i]);
        }
    }

    @Override
    public Node[] getNodes() {
        return nodes;
    }

    public void clearErrors() {
        for (Node neuronNode : nodes) {
            neuronNode.clearError();
        }
    }
}
