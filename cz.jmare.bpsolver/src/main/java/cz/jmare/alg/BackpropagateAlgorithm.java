package cz.jmare.alg;

import cz.jmare.ai.layer.NeuronNodeLayer;
import cz.jmare.ai.node.NeuronNode;
import cz.jmare.ai.node.NeuronNode.Synapse;

public class BackpropagateAlgorithm {
    private NeuronNodeLayer[] neuronNodeLayers;

    private double learningRate = 0.3;

    private double momentum = 0.6;

    public BackpropagateAlgorithm(NeuronNodeLayer[] neuronNodeLayers) {
        super();
        this.neuronNodeLayers = neuronNodeLayers;
    }

    public double run(double[] target) {
        NeuronNodeLayer outputLayer = neuronNodeLayers[neuronNodeLayers.length - 1];
        NeuronNode[] outputNeuronNodes = outputLayer.getNodes();

        double errorSum = 0;

        for (int i = 0; i < neuronNodeLayers.length; i++) {
            neuronNodeLayers[i].clearErrors();
        }

        for (int i = 0; i < outputNeuronNodes.length; i++) {
            NeuronNode neuronNode = outputNeuronNodes[i];
            double neuronError = target[i] - neuronNode.getOutput();
            errorSum += neuronError * neuronError;
            neuronNode.addError(neuronError);
        }

        for (int i = neuronNodeLayers.length - 1; i >= 0; i--) {
            backpropagateLayer(neuronNodeLayers[i]);
        }

        return errorSum / outputNeuronNodes.length; // useful MSE
    }

    private void backpropagateLayer(NeuronNodeLayer neuronNodeLayer) {
        NeuronNode[] nodes = neuronNodeLayer.getNodes();
        for (int i = 0; i < nodes.length; i++) {
            NeuronNode neuronNode = nodes[i];

            double d = neuronNode.getError();
            double derivatedOutput = neuronNode.getDerivatedOutput();
            d *= derivatedOutput;

            Synapse[] synapses = neuronNode.getSynapses();
            for (Synapse synapse : synapses) {
                double weightError = synapse.weight * d;
                synapse.inputNode.addError(weightError);
                double dw = synapse.inputNode.getOutput() * d * learningRate;
                synapse.weight += synapse.dweight * momentum + dw;
                synapse.dweight = dw;
            }
        }
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public double getMomentum() {
        return momentum;
    }
}
