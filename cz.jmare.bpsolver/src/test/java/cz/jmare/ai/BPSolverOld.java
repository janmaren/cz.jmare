package cz.jmare.ai;

import java.util.Arrays;
import java.util.function.BiConsumer;

import cz.jmare.ai.layer.InputNodeLayer;
import cz.jmare.ai.layer.NeuronNodeLayer;
import cz.jmare.ai.util.BPSolverUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BPSolverOld {
    static double[][] inputs = new double[][] { new double[] { 0, 1 }, new double[] { 0, 0 }, new double[] { 1, 0 },
            new double[] { 1, 1 } };
    static double[][] targets = new double[][] { new double[] { 1 }, new double[] { 0 }, new double[] { 1 },
            new double[] { 0 } };

    @Test
    public void xorTest() {
        boolean print = true;
        
        BPSolver bpSolver = new BPSolver(2, 3, 1);
        BPSolverUtil.applySmartParameters(bpSolver);
        bpSolver.setMseConsumer(new BiConsumer<Integer, Double>() {
            @Override
            public void accept(Integer epoch, Double mse) {
                if (print) {
                    System.out.println("Epoch: " + epoch + ", mse:" + mse);
                }
            }
        });
        bpSolver.train(inputs, targets);
        Assertions.assertEquals(1, bpSolver.solve(0, 1)[0], 0.1);
        
        if (print) {
            System.out.println("Last MSE: " + bpSolver.getLastMse());
            System.out.println("Iterations: " + bpSolver.getLastEpochNumber());
            printTraining(bpSolver.getInputNodeLayer(), bpSolver.getNeuronNodeLayers(), inputs, targets);
        }

        
        LearningState learningState = bpSolver.getLearningState();
        if (print) {
            System.out.println(learningState.toString().replace("[", "{").replace("]", "}"));
        }

        bpSolver = new BPSolver(learningState);
        Assertions.assertEquals(1, bpSolver.solve(0, 1)[0], 0.1);
        
        if (print) {
            printTraining(bpSolver.getInputNodeLayer(), bpSolver.getNeuronNodeLayers(), inputs, targets);
            learningState = bpSolver.getLearningState();
            System.out.println(learningState.toString().replace("[", "{").replace("]", "}"));
        }
    }

    @Test
    public void tanhTest() {
        double[][] inputs = new double[][] {
                new double[] { 0 },
                new double[] { 0.1 },
                new double[] { 0.2 },
                new double[] { 0.3 },
                new double[] { 0.4 },
                new double[] { 0.5 },
                new double[] { 0.6 },
                new double[] { 0.7 },
                new double[] { 0.8 },
                new double[] { 0.9 },
                new double[] { 1 },
        };
        double[][] targets = new double[][] {
                new double[] { 0 },
                new double[] { 0.09966799462496 },
                new double[] { 0.1973753202249 },
                new double[] { 0.2913126124516 },
                new double[] { 0.3799489622552 },
                new double[] { 0.46211715726 },
                new double[] { 0.537049566998 },
                new double[] { 0.6043677771172 },
                new double[] { 0.6640367702678 },
                new double[] { 0.716297870199 },
                new double[] { 0.7615941559558 },
        };

        boolean print = true;

        BPSolver bpSolver = new BPSolver(1, 5, 1);
        BPSolverUtil.applySmartParameters(bpSolver);
        bpSolver.setMseConsumer(new BiConsumer<Integer, Double>() {
            @Override
            public void accept(Integer epoch, Double mse) {
                if (print) {
                    System.out.println("Epoch: " + epoch + ", mse:" + mse);
                }
            }
        });
        bpSolver.train(inputs, targets);
        Assertions.assertEquals(0.5005202111902, bpSolver.solve(0.55)[0], 0.001);

        if (print) {
            System.out.println("Last MSE: " + bpSolver.getLastMse());
            System.out.println("Iterations: " + bpSolver.getLastEpochNumber());
            printTraining(bpSolver.getInputNodeLayer(), bpSolver.getNeuronNodeLayers(), inputs, targets);
        }


        LearningState learningState = bpSolver.getLearningState();
        if (print) {
            System.out.println(learningState.toString().replace("[", "{").replace("]", "}"));
        }

        bpSolver = new BPSolver(learningState);
        Assertions.assertEquals(0.5005202111902, bpSolver.solve(0.55)[0], 0.01);

        if (print) {
            printTraining(bpSolver.getInputNodeLayer(), bpSolver.getNeuronNodeLayers(), inputs, targets);
            learningState = bpSolver.getLearningState();
            System.out.println(learningState.toString().replace("[", "{").replace("]", "}"));
        }
    }

    public static void printTraining(InputNodeLayer inputNodeLayer, NeuronNodeLayer[] neuronNodeLayers,
            double[][] inputs, double[][] targets) {
        for (int i = 0; i < inputs.length; i++) {
            for (NeuronNodeLayer neuronNodeLayer : neuronNodeLayers) {
                neuronNodeLayer.clearOutputs();
            }
            inputNodeLayer.setValues(inputs[i]);
            double output = neuronNodeLayers[neuronNodeLayers.length - 1].getNodes()[0].getOutput();
            System.out.println("Inputs:" + Arrays.toString(inputs[i]) + ", result: " + output + ", target: "
                    + targets[i][0] + ", error: " + (output - targets[i][0]));

        }
    }
}
