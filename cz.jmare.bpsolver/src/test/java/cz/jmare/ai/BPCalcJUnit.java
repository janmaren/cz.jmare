package cz.jmare.ai;

import cz.jmare.ai.actfunc.Logsig;
import cz.jmare.ai.actfunc.Tanh;
import cz.jmare.ai.actfunc.Tansig;
import cz.jmare.ai.util.BPSolverUtil;
import cz.jmare.util.NPattern;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class BPCalcJUnit {
    double[][] inputs = new double[][] {
            new double[] { 0, 0 },
            new double[] { 0, 1 },
            new double[] { 1, 1 },
            new double[] { 0.2, 0 },
            new double[] { 0.5, 0 },
            new double[] { 0.7, 1 },
            new double[] { 0.7, 0 },
            new double[] { 0.13, 13 },
    };
    double[][] targets = new double[][] {
            new double[] { 0, 0, 0 },
            new double[] { 0, 1, 0 },
            new double[] { 0, 1, 0 },
            new double[] { 0.04, 0, 0 },
            new double[] { 0.25, 0, 0 },
            new double[] { 0, 1, 0 },
            new double[] { 0.49, 0, 0 },
            new double[] { 1, 1, 1 },
    };

    @Test
    public void bpSolverTest() {
        BPSolver bpSolver = new BPSolver(2, 7, 5, 3);
        BPSolverUtil.applySmartParameters(bpSolver);
        bpSolver.setMaxMse(0.0001);
        bpSolver.setMaxIterations(10000000);
        bpSolver.setMseConsumer(new BiConsumer<Integer, Double>() {
            @Override
            public void accept(Integer epoch, Double mse) {
                if ((epoch % 100) == 0) {
                    System.out.println("Epoch: " + epoch + ", mse:" + mse);
                }
            }
        });
        bpSolver.train(inputs, targets);

        System.out.println(Arrays.toString(bpSolver.solve(1, 1)));
    }

    @Test
    public void bpCalcTest() {
        BPCalc bpCalc = new BPCalc(2, 7, 5, 3);
        bpCalc.setHiddenLayeraActFunc(new Tansig());
        bpCalc.setOutputLayerActFunc(new Logsig());

        BPCalcTraining.debugTrainForSeconds(bpCalc, toNPatterns(inputs, targets), 2, 1000);
    }

    @Test
    public void bpCalcTestSeconds() {
        BPCalc bpCalc = new BPCalc(2, 7, 5, 3);
        bpCalc.setHiddenLayeraActFunc(new Tansig());
        bpCalc.setOutputLayerActFunc(new Logsig());

        BPCalcTraining.debugTrainForSeconds(bpCalc, toNPatterns(inputs, targets), 2, 1000);
    }

    @Test
    public void bpCalcTestSecondsTanhLearningRate() {
        BPCalc bpCalc = new BPCalc(2, 6, 5, 3);
        bpCalc.setLearningRate(1.4 + 2 / 3.0);
        bpCalc.setHiddenLayeraActFunc(new Tanh());
        bpCalc.setOutputLayerActFunc(new Logsig());

        BPCalcTraining.debugTrainForSeconds(bpCalc, toNPatterns(inputs, targets), 2, 1000);
    }

    private static List<NPattern> toNPatterns(double[][] input, double[][] target) {
        List<NPattern> patterns = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            patterns.add(new NPattern(input[i], target[i]));
        }
        return patterns;
    }
}
