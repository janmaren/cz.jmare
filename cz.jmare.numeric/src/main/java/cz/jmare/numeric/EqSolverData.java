package cz.jmare.numeric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.equation.util.SolveRange;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;
import cz.jmare.numeric.range.AutoSolutionRange;
import cz.jmare.numeric.range.FixedSolutionRange;
import cz.jmare.numeric.range.SolutionRange;

public class EqSolverData {
    private List<String> definitions;

    private Config config = new Config();

    private int eqDefIndex;

    private Map<String, Supplier<?>> predefinedVariableSuppliers = new HashMap<String, Supplier<?>>();

    private String solveVariable;

    private SolutionRange solutionRange = new AutoSolutionRange();

    private SolveRange solveRange = SolveRange.wholeRange();

    private Double nearTolerance;

    public EqSolverData() {
        super();
    }

    public EqSolverData(String definition) {
        withDefinition(definition);
    }

    public List<String> getDefinitions() {
        return definitions;
    }

    public EqSolverData withDefinitions(List<String> definitions) {
        this.definitions = definitions;
        return this;
    }

    public EqSolverData withDefinition(String definition) {
        this.definitions = new ArrayList<String>();
        this.definitions.add(definition);
        return this;
    }

    public Config getConfig() {
        return config;
    }

    public EqSolverData withConfig(Config config) {
        this.config = config;
        return this;
    }

    public int getEqDefIndex() {
        return eqDefIndex;
    }

    public EqSolverData withEqDefIndex(int eqDefIndex) {
        this.eqDefIndex = eqDefIndex;
        return this;
    }

    public Map<String, Supplier<?>> getPredefinedVariableSuppliers() {
        return predefinedVariableSuppliers;
    }

    public EqSolverData withPredefinedVariableSuppliers(Map<String, Supplier<?>> predefinedVariableSuppliers) {
        this.predefinedVariableSuppliers = predefinedVariableSuppliers;
        return this;
    }

    public String getSolveVariable() {
        return solveVariable;
    }

    public EqSolverData withSolveVariable(String solveVariable) {
        this.solveVariable = solveVariable;
        return this;
    }

    public SolutionRange getSolutionRange() {
        return solutionRange;
    }

    public EqSolverData withSolutionRange(SolutionRange solutionRange) {
        this.solutionRange = solutionRange;
        return this;
    }

    public EqSolverData withRange(double inputXFromIncluding, double inputXToExcluding) {
        this.solutionRange = new FixedSolutionRange(inputXFromIncluding, inputXToExcluding);
        return this;
    }

    public SolveRange getSolveRange() {
        return solveRange;
    }

    public Double getNearTolerance() {
        return nearTolerance;
    }

    public EqSolverData withSolveRange(SolveRange solveRange) {
        this.solveRange = solveRange;
        return this;
    }

    public EqSolverData withSolveRange(double from, double to) {
        this.solveRange = new SolveRange(from, to);
        return this;
    }

    public EqSolverData withNearTolerance(Double nearTolerance) {
        this.nearTolerance = nearTolerance;
        return this;
    }

    public EquationData toEquationData() {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(this);
        return equationDataPreparer.getEquationData();
    }
}
