package cz.jmare.numeric.solution;

import java.util.function.Function;

@Deprecated
public class BisectionAlgorithm {
    /**
     * Return one result value by bysection algorithm. Such value is as accurate as precision parameter is low.
     * @param yDiffProvider difference between left and right side of equation. For example for x ^ 2 = 100 the function
     * must be defined as x ^ 2 - 100
     * @param fromXIncluding min interval of solution inclusive
     * @param toXIncluding max interval of solution inclusive
     * @param precision precision - lower is more accurate
     * @return
     */
    public static Double findResult(Function<Double, Double> yDiffProvider, double fromXIncluding, double toXIncluding, double precision) {
        return findResult(yDiffProvider, fromXIncluding, toXIncluding, precision, new ResultMetadata());
    }

    /**
     * Return one result value by bysection algorithm. Such value is as accurate as precision parameter is low.
     * @param yDiffProvider difference between left and right side of equation. For example for x ^ 2 = 100 the function
     * must be defined as x ^ 2 - 100
     * @param fromXIncluding min interval of solution inclusive
     * @param toXIncluding max interval of solution inclusive
     * @param precision precision - lower is more accurate
     * @param resultMetadata - metadata about result, like achieved precision, number of iterations
     * @return
     */
    public static Double findResult(Function<Double, Double> yDiffProvider, double fromXIncluding, double toXIncluding, double precision, ResultMetadata resultMetadata) {
        double from = fromXIncluding;
        double to = toXIncluding;

        double originFromDiff = yDiffProvider.apply(fromXIncluding);
        if (originFromDiff == 0) {
            return from;
        }

        double originToDiff = yDiffProvider.apply(toXIncluding);
        if (originToDiff == 0) {
            return to;
        }

        if (originFromDiff < 0 && originToDiff < 0 || originFromDiff > 0 && originToDiff > 0) {
            // no cross in interval (fromXIncluding, toXIncluding) or 2 crosses
            return null;
        }

        boolean leftLessThanRight = Double.isNaN(originFromDiff) ? originToDiff > 0 : originFromDiff < 0;
        double absDelta = 0;
        double middle = 0;
        double lastDelta = Double.MAX_VALUE;
        int iterations = 0;
        do {
            middle = (from + to) / 2;
            double delta = yDiffProvider.apply(middle);
            if (Double.isNaN(delta)) {
                return null;
            }
            absDelta = Math.abs(delta);
//            if (Double.isNaN(absDelta)) {
//                //absDelta = Double.MAX_VALUE;
//                return null;
//            }

            // test whether diverges, e.g. f(x) = 1 / x = 0;
            if (++iterations % 10 == 0) {
                if (absDelta > lastDelta) {
                    return null; // diverging
                }
                if (iterations > 1000) {
                    return null;
                }
                lastDelta = absDelta;
            }

            if (leftLessThanRight) {
                if (delta < 0) {
                    from = middle;
                } else {
                    to = middle;
                }
            } else {
                if (delta > 0) {
                    from = middle;
                } else {
                    to = middle;
                }
            }
        } while (absDelta > precision);

        resultMetadata.xError = Math.abs(to - from);
        resultMetadata.yError = absDelta;
        resultMetadata.iterations = iterations;

        return middle;
    }

    /**
     * Return a value by bysection algorithm. Such value is as accurate as precision parameter is low.
     * @param yDiffProvider difference between left and right side of equation. For example for x ^ 2 = 100 the function
     * must be defined as x ^ 2 - 100
     * @param fromXIncluding min interval of solution inclusive
     * @param toXIncluding max interval of solution inclusive
     * @param precision precision - lower is more accurate
     * @return
     */
    public static Double findResultXPrecision(Function<Double, Double> yDiffProvider, double fromXIncluding, double toXIncluding, double precision) {
        double from = fromXIncluding;
        double to = toXIncluding;

        double originFromDiff = yDiffProvider.apply(fromXIncluding);
        if (originFromDiff == 0) {
            return from;
        }

        double originToDiff = yDiffProvider.apply(toXIncluding);
        if (originToDiff == 0) {
            return from;
        }

        if (originFromDiff < 0 && originToDiff < 0 || originFromDiff > 0 && originToDiff > 0) {
            // no cross in interval (fromXIncluding, toXIncluding) or 2 crosses
            return null;
        }

        boolean leftLessThanRight = originFromDiff < 0;
        double absDelta = 0;
        double middle = 0;
        double lastDelta = Double.MAX_VALUE;
        int iterations = 0;
        do {
            middle = (from + to) / 2;
            double delta = yDiffProvider.apply(middle);
            absDelta = Math.abs(delta);

            // test whether diverges, e.g. f(x) = 1 / x = 0;
            if (++iterations % 10 == 0) {
                if (absDelta > lastDelta) {
                    return null; // diverging
                }
                if (iterations > 1000) {
                    return null;
                }
                lastDelta = absDelta;
            }

            if (leftLessThanRight) {
                if (delta < 0) {
                    from = middle;
                } else {
                    to = middle;
                }
            } else {
                if (delta > 0) {
                    from = middle;
                } else {
                    to = middle;
                }
            }
        } while (Math.abs(to - from) / 2.0 > precision);

        return middle;
    }

    public static Double findNearestResult(Function<Double, Double> yDiffProvider, double fromXIncluding, double toXIncluding, double precision) {
        return findNearestResult(yDiffProvider, fromXIncluding, toXIncluding, precision, new ResultMetadata());
    }

    public static Double findNearestResult(Function<Double, Double> yDiffProvider, double fromXIncluding, double toXIncluding, double precision, ResultMetadata resultMetadata) {
        double from = fromXIncluding;
        double to = toXIncluding;

        double middle = (from + to) / 2;
        double deltaMiddle = yDiffProvider.apply(middle);
        double deltaFrom = yDiffProvider.apply(from);
        double deltaTo = yDiffProvider.apply(to);
        boolean opposite = false;
        if ((deltaTo + deltaFrom) / 2 < deltaMiddle) {
            opposite = true;
        }

        int iterations = 0;

        do {
            iterations++;
            if (opposite) {
                if (deltaFrom < deltaMiddle) {
                    from = (from + middle) / 2.0;
                    deltaFrom = yDiffProvider.apply(from);
                } else {
                    to = (to + middle) / 2.0;
                    deltaTo = yDiffProvider.apply(to);
                }
            } else {
                if (deltaFrom > deltaMiddle) {
                    from = (from + middle) / 2.0;
                    deltaFrom = yDiffProvider.apply(from);
                } else {
                    to = (to + middle) / 2.0;
                    deltaTo = yDiffProvider.apply(to);
                }
            }
            middle = (from + to) / 2;
            deltaMiddle = yDiffProvider.apply(middle);
        } while (Math.abs(from - to) / 2 > precision);

        resultMetadata.xError = Math.abs(from - to) / 2;
        resultMetadata.yError = Math.abs(deltaMiddle);
        resultMetadata.iterations = iterations;

        return middle;
    }


    public static void main3(@SuppressWarnings("unused") String[] args) {
        Double value = findNearestResult(x -> (x - 1) * (x - 1), -30, 150, 0.001);
        System.out.println(value);

        Double value6 = findNearestResult(x -> (x - 1) * (x - 1), 1, 150, 0.001);
        System.out.println("Begin: " + value6);

        Double value7 = findNearestResult(x -> (x - 1) * (x - 1), -150, 1, 0.001);
        System.out.println("End: " + value7);

        Double value2 = findNearestResult(x -> 5 - ((x - 1) * (x - 1)), 0.9, 3, 0.001);
        System.out.println(value2);

        Double value5 = findNearestResult(x -> (10 * x) * (10 * x), -1.5, 1, 0.001);
        System.out.println(value5);

//        Double value3 = findNearestResultTo(x -> (x - 1) * (x - 1), 0.9, 3, 0.001);
//        System.out.println(value3);
//
//        Double value4 = findNearestResultTo(x -> 5 - ((x - 1) * (x - 1)), 0.9, 3, 0.001);
//        System.out.println(value4);
    }
}
