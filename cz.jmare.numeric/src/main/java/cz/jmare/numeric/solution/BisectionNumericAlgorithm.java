package cz.jmare.numeric.solution;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.util.LeftRightDiff;
import cz.jmare.numeric.util.NumberPrecisionUtil;

@Deprecated
public class BisectionNumericAlgorithm {
    private Double lastLeftValue;

    private Double lastRightValue;

    private Double prevX;

    private LeftRightDiff diffFunction;

    private double precision;

    private Integer roundingOrder;

    public BisectionNumericAlgorithm(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier,
            DoubleHolder numberHolder) {
        diffFunction = new LeftRightDiff(leftSupplier, rightSupplier, numberHolder);
    }

    public BisectionNumericAlgorithm(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier,
            DoubleHolder numberHolder, double precision) {
        diffFunction = new LeftRightDiff(leftSupplier, rightSupplier, numberHolder);
        this.precision  = precision;
    }

    public void reset(Double prevX, Double prevLeftValue, Double prevRightValue) {
        this.prevX = prevX;
        this.lastLeftValue = prevLeftValue;
        this.lastRightValue = prevRightValue;
    }

    public void reset() {
        this.prevX = null;
        this.lastLeftValue = null;
        this.lastRightValue = null;
    }

    public Double calculateValue(double x, double leftValue, double rightValue) {
        Double value = calculateValueInternal(x, leftValue, rightValue);
        lastLeftValue = leftValue;
        lastRightValue = rightValue;
        prevX = x;
        return value;
    }

    /**
     * Find value which must be between (exclusively) last x and current x
     * @param x current x
     * @param leftValue
     * @param rightValue
     * @return
     */
    private Double calculateValueInternal(double x, double leftValue, double rightValue) {
        if (lastLeftValue == null || lastRightValue == null) {
            return null;
        }

        if (leftValue == rightValue) {
            return x;
        }

        if (Double.isNaN(lastLeftValue) || Double.isNaN(lastRightValue)) {
            ResultMetadata resultMetadata = new ResultMetadata();
            Double findByBisection = BisectionAlgorithm.findResult(diffFunction, prevX, x, precision, resultMetadata);
            if (findByBisection == null) {
                return null;
            }
            return NumberPrecisionUtil.roundOrder(findByBisection, roundingOrder);
        } else
        if (lastLeftValue <= lastRightValue) {
            if (leftValue >= rightValue) {
                // crossed left increasing through right
                ResultMetadata resultMetadata = new ResultMetadata();
                Double findByBisection = BisectionAlgorithm.findResult(diffFunction, prevX, x, precision, resultMetadata);
                if (findByBisection == null) {
                    return null;
                }
                return NumberPrecisionUtil.roundOrder(findByBisection, roundingOrder);
            }
        } else {
            if (leftValue <= rightValue) {
             // crossed left decreasing through right
                ResultMetadata resultMetadata = new ResultMetadata();
                Double findByBisection = BisectionAlgorithm.findResult(diffFunction, prevX, x, precision, resultMetadata);
                if (findByBisection == null) {
                    return null;
                }
                return NumberPrecisionUtil.roundOrder(findByBisection, roundingOrder);
            }
        }

        return null;
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public void setRoundingOrder(int roundingOrder) {
        this.roundingOrder = roundingOrder;
    }
}
