package cz.jmare.numeric.solution;

@Deprecated
public class ResultMetadata {
    /**
     * Always positive value of x error
     */
    public Double xError;

    /**
     * Always positive value of y error
     */
    public Double yError;

    /**
     * Number of iterations used
     */
    public Integer iterations;

    @Override
    public String toString() {
        return "ResultMetadata [xError=" + xError + ", yError=" + yError + ", iterations=" + iterations + "]";
    }
}
