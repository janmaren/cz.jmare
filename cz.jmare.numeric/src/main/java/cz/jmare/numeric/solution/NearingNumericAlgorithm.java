package cz.jmare.numeric.solution;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.util.LeftRightDiff;
import cz.jmare.numeric.util.NumberPrecisionUtil;

@Deprecated
public class NearingNumericAlgorithm {
    private Double lastLeftValue;

    private Double lastRightValue;

    private Double prevX;

    private Supplier<Double> leftSupplier;

    private Supplier<Double> rightSupplier;

    private DoubleHolder numberHolder;

    private double precision;

    private Integer roundingOrder;

    private double nearingPrecision;

    private Boolean lastNearing;

    private LeftRightDiff diffFunction;

    public NearingNumericAlgorithm(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberHolder) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
        diffFunction = new LeftRightDiff(leftSupplier, rightSupplier, numberHolder);
    }

    public NearingNumericAlgorithm(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberHolder, double precision) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
        this.precision = precision;
        diffFunction = new LeftRightDiff(leftSupplier, rightSupplier, numberHolder);
    }

    public void reset(Double prevX, Double prevLeftValue, Double prevRightValue) {
        this.lastLeftValue = prevLeftValue;
        this.lastRightValue = prevRightValue;

        if (prevX != null) {
            numberHolder.accept(prevX + precision);
            double epsLeftValue = leftSupplier.get();
            double epsRightValue = rightSupplier.get();
            lastNearing = resolveNearing(lastLeftValue, lastRightValue, epsLeftValue, epsRightValue);
            this.prevX = prevX;
        } else {
            lastNearing = null;
        }
    }

    public Double calculateValue(double x, double leftValue, double rightValue) {
        double tmpPrevX = prevX;
        this.prevX = x;

        double inputFromPlusEpsilon = x + precision;
        numberHolder.accept(inputFromPlusEpsilon);
        double epsLeftValue = leftSupplier.get();
        double epsRightValue = rightSupplier.get();

        Boolean nearing = resolveNearing(leftValue, rightValue, epsLeftValue, epsRightValue);

        if (nearing == null) {
            return null;
        } else {
            if (lastNearing != null) {
                if (lastNearing && !nearing) {
                    ResultMetadata resultMetadata = new ResultMetadata();
                    Double solveByNearing = BisectionAlgorithm.findNearestResult(diffFunction, tmpPrevX, x, precision, resultMetadata);
                    if (solveByNearing != null) {
                        if (resultMetadata.yError < nearingPrecision) {
                            return NumberPrecisionUtil.roundOrder(solveByNearing, roundingOrder);
                        }
                        return null;
                    }
                }
            }
            lastNearing = nearing;
        }

        return null;
    }

    /**
     * returning true when lines meet right from value, false when meet left from value and null when they are equidistant
     */
    private Boolean resolveNearing(double leftValue, double rightValue, double epsLeftValue,
            double epsRightValue) {
        double delta1 = leftValue - rightValue;
        double delta2 = epsLeftValue - epsRightValue;
        if (delta1 == delta2) {
            return null;
        }
        if (delta1 > 0) {
            return delta1 > delta2;
        } else {
            return delta1 < delta2;
        }
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public void setNearingPrecision(double nearingPrecision) {
        this.nearingPrecision = nearingPrecision;
    }

    public void setRoundingOrder(Integer roundingOrder) {
        this.roundingOrder = roundingOrder;
    }
}
