package cz.jmare.numeric;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.equation.FilterResultPublisher;
import cz.jmare.numeric.equation.ResultPublisher;
import cz.jmare.numeric.equation.SimpleNumericSolution;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;
import cz.jmare.numeric.params.PrecisionProvider;
import cz.jmare.numeric.params.PrecisionValues;
import cz.jmare.numeric.params.SimplePrecision;
import cz.jmare.numeric.range.AutoSolutionRange;
import cz.jmare.numeric.range.FixedSolutionRange;
import cz.jmare.numeric.range.RangeParameters;
import cz.jmare.numeric.range.SolutionRange;
import cz.jmare.numeric.util.LeftRightDiff;

/**
 * Find results for a variable in an equation.
 * Experimental - not for production use
 */
@Deprecated // use e.g. DynamicStepFinderRunnable
public class EquationSolver {
    private DoubleHolder numberHolder;
    private SimpleNumericSolution simpleNumericSolution;
    private SolutionRange solutionRange = new AutoSolutionRange();
    private Consumer<Double> progress;
    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    /**
     * Variable to be calculated - it may be detected
     */
    private String calculateVariable;

    /**
     * All used variables found on definition lines like 'x: 123'
     */
    private Set<VariableIndex> usedVariables;

    private PrecisionProvider precisionProvider = new SimplePrecision();

    public EquationSolver(EquationData equationData) {
        this.leftSupplier = equationData.getLeftSupplier();
        this.rightSupplier = equationData.getRightSupplier();
        this.numberHolder = equationData.getNumberSupplier();
        this.calculateVariable = equationData.getCalculateVariableName();
        this.usedVariables = equationData.getUsedVariables();
        simpleNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        simpleNumericSolution.withPrecisionProvider(precisionProvider);
    }

    /**
     * Find all results until consumer returns false (no more results needed)
     * @param consumer
     */
    public void findAll(Function<Double, Boolean> consumer) {
        ResultPublisher resultPublisher = new FilterResultPublisher(consumer);

        Thread currentThread = Thread.currentThread();
        RangeParameters nextRange = null;
        Double value = null;
        while ((nextRange = solutionRange.getNextRange()) != null) {
            if (currentThread.isInterrupted()) {
                if (progress != null) {
                    progress.accept(null);
                }
                return;
            }
            if (progress != null) {
                progress.accept(nextRange.from);
            }
            PrecisionValues precisionValues = precisionProvider.getPrecisionValues(nextRange.from, nextRange.to);
            double step = precisionValues.step;
            value = nextRange.from;
            boolean continu = resultPublisher.solvingRange(nextRange, step, precisionValues.precision);
            if (!continu) {
                return;
            }
            while ((value = simpleNumericSolution.solve(value, nextRange.to)) != null) {
                if (value >= nextRange.to) {
                    throw new IllegalStateException("Value " + value + " greater than " + nextRange.to);
                }
                continu = resultPublisher.result(value);
                if (!continu) {
                    return;
                }

                if (currentThread.isInterrupted()) {
                    if (progress != null) {
                        progress.accept(null);
                    }
                    return;
                }
                value += step;
                if (value >= nextRange.to) {
                    break;
                }
            }
        }
        resultPublisher.finish();
    }

    public List<Double> findAll() {
        List<Double> results = new ArrayList<>();
        findAll((Double v) -> {
            results.add(v);
            return true;
        });
        return results;
    }

    public EquationSolver withRange(SolutionRange solutionRange) {
        this.solutionRange = solutionRange;
        return this;
    }

    /**
     * Range in which the solution will be searched for variable
     * @param inputXFromIncluding
     * @param inputXToExcluding
     * @return
     */
    public EquationSolver withRange(double inputXFromIncluding, double inputXToExcluding) {
        this.solutionRange = new FixedSolutionRange(inputXFromIncluding, inputXToExcluding);
        return this;
    }

    public void setProgress(Consumer<Double> progress) {
        this.progress = progress;
    }

    public DoubleHolder getNumberSupplier() {
        return numberHolder;
    }

    public Supplier<Double> getLeftSupplier() {
        return leftSupplier;
    }

    public Supplier<Double> getRightSupplier() {
        return rightSupplier;
    }

    public LeftRightDiff getLeftRightDiff() {
        LeftRightDiff yDiffProvider = new LeftRightDiff(leftSupplier, rightSupplier, numberHolder);
        return yDiffProvider;
    }

    public String getCalculateVariable() {
        return calculateVariable;
    }

    /**
     * Get all used variables found on definition lines like 'x: 123'
     * @return All used variables found on definition lines
     */
    public Set<VariableIndex> getUsedVariables() {
        return usedVariables;
    }

    public EquationSolver withPrecision(PrecisionProvider precisionProvider) {
        this.precisionProvider = precisionProvider;
        simpleNumericSolution.withPrecisionProvider(precisionProvider);
        return this;
    }

    public EquationSolver withStepForRangeFunction(BiFunction<Double, Double, Double> stepForRangeFunction) {
        simpleNumericSolution.withStepForRangeFunction(stepForRangeFunction);
        return this;
    }

    public EquationSolver withConstantStep(double constantStep) {
        simpleNumericSolution.withConstantStep(constantStep);
        return this;
    }

    public static EquationSolver of(EqSolverData eqSolverBuilder) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(eqSolverBuilder);
        EquationData equationData = equationDataPreparer.getEquationData();
        EquationSolver equationSolver = new EquationSolver(equationData);
        equationSolver.withRange(eqSolverBuilder.getSolutionRange());
        return equationSolver;
    }

    public static EquationSolver of(String equation) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(equation);
        EquationData equationData = equationDataPreparer.getEquationData();
        return new EquationSolver(equationData);
    }

    public static EquationSolver of(String equation, Config config) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(equation, config);
        EquationData equationData = equationDataPreparer.getEquationData();
        return new EquationSolver(equationData);
    }

    public static EquationSolver of(int index, List<String> definitions) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(index, definitions);
        EquationData equationData = equationDataPreparer.getEquationData();
        return new EquationSolver(equationData);
    }

    public static EquationSolver of(int index, List<String> definitions, Config config) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(index, definitions, config);
        EquationData equationData = equationDataPreparer.getEquationData();
        return new EquationSolver(equationData);
    }

    public static EquationSolver of(int index, List<String> definitions, Map<String, Supplier<?>> predefinedVariableSuppliers, Config config) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(index, definitions, predefinedVariableSuppliers, config);
        EquationData equationData = equationDataPreparer.getEquationData();
        return new EquationSolver(equationData);
    }

    public static EquationSolver of(int index, List<String> statements, String solveVariable) {
        EqSolverData eqSolverData = new EqSolverData().withDefinitions(statements).withEqDefIndex(index).withSolveVariable(solveVariable);
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(eqSolverData);
        return new EquationSolver(equationDataPreparer.getEquationData());
    }

    public static EquationSolver of(int index, List<String> statements, String solveVariable,
            Config config) {
        EqSolverData eqSolverData = new EqSolverData().withDefinitions(statements).withEqDefIndex(index).withSolveVariable(solveVariable).withConfig(config);
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(eqSolverData);
        return new EquationSolver(equationDataPreparer.getEquationData());
    }
}
