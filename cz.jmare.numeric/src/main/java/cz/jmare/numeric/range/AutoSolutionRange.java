package cz.jmare.numeric.range;

/**
 * Return all ranges order by order to comprise all possible values from -9.999999999999998E307 to 9.999999999999998E307.
 * First generated ranges are around 0 and each next range is 10 times larger on positive and negative side
 */
public class AutoSolutionRange implements SolutionRange {
    private RangeParameters lastRangeParameters;

    protected Double fromIncluding;

    protected Double toExcluding;

    public AutoSolutionRange(double fromIncluding, double toExcluding) {
        this.fromIncluding = fromIncluding;
        this.toExcluding = toExcluding;
    }

    public AutoSolutionRange() {
    }

    @Override
    public RangeParameters getNextRange() {
        RangeParameters correctedRange = null;
        RangeParameters rangeParameters = null;
        do {
            rangeParameters = createRange();
            if (rangeParameters == null) {
                return null;
            }
            correctedRange = limitRangeParameters(rangeParameters);
        } while (correctedRange == null);
        return correctedRange;
    }

    private RangeParameters createRange() {
        if (lastRangeParameters == null) {
            lastRangeParameters = new RangeParameters(0, 1);
            return lastRangeParameters;
        }
        if (lastRangeParameters.to > 0) {
            lastRangeParameters = new RangeParameters(-lastRangeParameters.to, -lastRangeParameters.from);
            return lastRangeParameters;
        } else {
            double nextFrom = -lastRangeParameters.from;
            Double nextTo = nextFrom * 10;
            if (nextTo.isInfinite() || nextTo.isNaN()) {
                return null;
            }
            lastRangeParameters = new RangeParameters(nextFrom, nextTo);
            return lastRangeParameters;
        }
    }

    private RangeParameters limitRangeParameters(RangeParameters rangeParameters) {
        RangeParameters limitedRangeParameters = new RangeParameters(rangeParameters.from, rangeParameters.to);
        if (fromIncluding != null) {
            if (limitedRangeParameters.from < fromIncluding) {
                limitedRangeParameters.from = fromIncluding;
            }
        }
        if (toExcluding != null) {
            if (limitedRangeParameters.to > toExcluding) {
                  limitedRangeParameters.to = toExcluding;
            }
        }
        if (limitedRangeParameters.from >= limitedRangeParameters.to) {
            return null;
        }
        return limitedRangeParameters;
    }

    public AutoSolutionRange withFromIncluding(Double fromIncluding) {
        this.fromIncluding = fromIncluding;
        return this;
    }

    public AutoSolutionRange withToExcluding(Double toExcluding) {
        this.toExcluding = toExcluding;
        return this;
    }

    public static void main(String[] args) {
        AutoSolutionRange autoSolutionRange = new AutoSolutionRange();
        autoSolutionRange.fromIncluding = -9d;
        autoSolutionRange.toExcluding = 500d;
        for (int i = 0; i < 1000; i++) {
            RangeParameters nextRange = autoSolutionRange.getNextRange();
            if (nextRange == null) {
                break;
            }
            System.out.println(i + ":" + nextRange);
        }
    }
}
