package cz.jmare.numeric.range;

public class FixedSolutionRange implements SolutionRange {
    private RangeParameters rangeParameters;
    private boolean finished;

    public FixedSolutionRange(double from, double to) {
        super();
        rangeParameters = new RangeParameters(from, to);
    }

    @Override
    public RangeParameters getNextRange() {
        if (finished) {
            return null;
        }
        finished = true;
        return rangeParameters;
    }
}
