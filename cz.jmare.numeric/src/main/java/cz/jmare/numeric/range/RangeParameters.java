package cz.jmare.numeric.range;

public class RangeParameters {
    /**
     * From x excluding
     */
    public double from;

    /**
     * To x including
     */
    public double to;

    public RangeParameters(double from, double to) {
        super();
        this.from = from;
        this.to = to;
    }


    @Override
    public String toString() {
        return "RangeParameters [from=" + from + ", to=" + to + "]";
    }
}
