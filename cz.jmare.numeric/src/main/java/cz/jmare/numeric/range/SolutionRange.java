package cz.jmare.numeric.range;

public interface SolutionRange {
    /**
     * Next range or null when there is no future range
     */
    RangeParameters getNextRange();
}
