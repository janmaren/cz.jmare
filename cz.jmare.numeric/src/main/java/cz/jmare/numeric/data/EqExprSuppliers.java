package cz.jmare.numeric.data;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

public interface EqExprSuppliers {
    Supplier<Double> getRightSupplier();

    Supplier<Double> getLeftSupplier();

    DoubleHolder getNumberSupplier();

    /**
     * Equation definition like '1 / x = 1'
     * @param def
     * @return
     */
    static EqExprSuppliers of(String def) {
        return new EquationDataPreparer(def).getEquationData();
    }
}
