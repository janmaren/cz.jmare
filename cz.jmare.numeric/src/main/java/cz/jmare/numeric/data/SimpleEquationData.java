package cz.jmare.numeric.data;

import java.util.Set;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;

public class SimpleEquationData implements EquationData {
    private DoubleHolder numberHolder;
    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private String calculateVariableName;

    /**
     * All used variables found on definition lines like 'x: 123'
     */
    private Set<VariableIndex> usedVariables;

    public SimpleEquationData(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier,
            DoubleHolder numberHolder, String calculateVariableName, Set<VariableIndex> usedVariables) {
        super();
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
        this.usedVariables = usedVariables;
        this.setCalculateVariableName(calculateVariableName);
    }

    @Override
    public Supplier<Double> getRightSupplier() {
        return rightSupplier;
    }

    @Override
    public Supplier<Double> getLeftSupplier() {
        return leftSupplier;
    }

    @Override
    public DoubleHolder getNumberSupplier() {
        return numberHolder;
    }

    public void setNumberSupplier(DoubleHolder numberHolder) {
        this.numberHolder = numberHolder;
    }

    public void setLeftSupplier(Supplier<Double> leftSupplier) {
        this.leftSupplier = leftSupplier;
    }

    public void setRightSupplier(Supplier<Double> rightSupplier) {
        this.rightSupplier = rightSupplier;
    }

    @Override
    public String getCalculateVariableName() {
        return calculateVariableName;
    }

    public void setCalculateVariableName(String calculateVariableName) {
        this.calculateVariableName = calculateVariableName;
    }

    @Override
    public Set<VariableIndex> getUsedVariables() {
        return usedVariables;
    }

    public void setUsedVariables(Set<VariableIndex> usedVariables) {
        this.usedVariables = usedVariables;
    }
}
