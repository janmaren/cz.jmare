package cz.jmare.numeric.data;

import java.util.Set;

import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;

public interface EquationData extends EqExprSuppliers {
    String getCalculateVariableName();

    Set<VariableIndex> getUsedVariables();
}
