package cz.jmare.numeric.params;

public class PrecisionValues {
    /**
     * Step interval on X axe. Only one solution may exist in the interval.
     * Step by step will be used to find all solution
     */
    public Double step;

    /**
     * PrecisionProvider on X axe. The result error is just this interval in which the exact
     * result can be present
     */
    public double precision;

    /**
     * How much tolerance on Y axe is acceptable to match.
     * Important in cases when parabola doesn't exactly get up to other curve (for example line)
     * because of calculation errors or number storage but certain near point we also want
     * to encouter as a result
     */
    public double yTouchTolerance;

    /**
     * How to round result value. The roundingOrder will be used in {@link NumberPrecisionUtil#roundOrder}
     */
    public int roundingOrder;
}
