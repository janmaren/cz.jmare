package cz.jmare.numeric.params;

public class SimplePrecision implements PrecisionProvider {
    protected int significantFigures;

    protected Double precision;

    protected Double yTouchTolerance;

    protected Integer roundingOrder;

    public SimplePrecision(int significantFigures) {
        super();
        this.significantFigures = significantFigures;
    }

    public SimplePrecision() {
        this(10);
    }

    @Override
    public PrecisionValues getPrecisionValues(double forValue1, double forValue2) {
        double forValue = Math.max(Math.abs(forValue1), Math.abs(forValue2));
        int orderOfMagnitude = getOrderOfMagnitude(forValue);
        double significantPrecision = getSignificantPrecision(forValue, significantFigures, orderOfMagnitude);

        PrecisionValues precisionValues = new PrecisionValues();

        if (roundingOrder != null) {
            precisionValues.roundingOrder = roundingOrder;
        } else {
            precisionValues.roundingOrder = calculateRoundingOrder(orderOfMagnitude);
        }

        if (precision != null) {
            precisionValues.precision = precision;
        } else {
            precisionValues.precision = calculatePrecision(significantPrecision);
        }

        if (yTouchTolerance != null) {
            precisionValues.yTouchTolerance = significantPrecision;
        } else {
               precisionValues.yTouchTolerance = calculateYTouchTolerance(significantPrecision);
           }

        precisionValues.step = calculateStep(forValue, forValue1, forValue2);

        int mag = orderOfMagnitude - 11;
        if (mag < -11) {
            mag = -11;
        }

        return precisionValues;
    }

    private int calculateRoundingOrder(int orderOfMagnitude) {
        return orderOfMagnitude - significantFigures + 3;
    }

    protected double calculateYTouchTolerance(double significantPrecision) {
        double yTouchTolerance = significantPrecision / 100;
           if (yTouchTolerance < 1e-7) {
               yTouchTolerance = 1e-7;
           }
           return yTouchTolerance;
    }

    protected double calculatePrecision(double significantPrecision) {
        return significantPrecision * 0.0001;
    }

    protected double calculateStep(double forValue, double fromValue, double toValue) {
        int orderOfMagnitude = getOrderOfMagnitude(forValue);
        int sigFig = significantFigures - 6;
        if (sigFig < 1) {
            sigFig = 1;
        }
        return getSignificantPrecision(forValue, sigFig, orderOfMagnitude);
    }

    public static int getOrderOfMagnitude(double number) {
        double log10 = Math.log10(number < 0 ? -number : number);
        int order = (int) log10;
        if (order < log10) {
            order++;
        }
        return order;
    }

    public static double getSignificantPrecision(double value, int significantFiguresPrecision, int orderOfMagnitude) {
        double prec = Math.pow(10, orderOfMagnitude);
        double precision = prec * Math.pow(10, -significantFiguresPrecision);
        return precision;
    }

    public void setPrecision(Double precision) {
        this.precision = precision;
    }

    public void setyTouchTolerance(Double nearingPrecision) {
        this.yTouchTolerance = nearingPrecision;
    }

    public void setRoundingSignificantFigures(Integer roundingSignificantFigures) {
        this.roundingOrder = roundingSignificantFigures;
    }
}
