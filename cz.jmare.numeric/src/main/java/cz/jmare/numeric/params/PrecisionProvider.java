package cz.jmare.numeric.params;

public interface PrecisionProvider {
    PrecisionValues getPrecisionValues(double forValue1, double forValue2);
}
