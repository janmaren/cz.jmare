package cz.jmare.numeric.equation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import cz.jmare.numeric.range.RangeParameters;

@Deprecated
public class SmartPublisher implements ResultPublisher {
    List<ResultCandidate> cands = new ArrayList<>();
    private double step;
    private double precision;
    private RangeParameters rangeParameters;
    private Function<Double, Boolean> resultConsumer;

    public SmartPublisher(Function<Double, Boolean> resultConsumer) {
        this.resultConsumer = resultConsumer;
    }

    @Override
    public boolean solvingRange(RangeParameters rangeParameters, double step, double precision) {
        boolean continu = true;
        for (Iterator<ResultCandidate> iterator2 = cands.iterator(); iterator2.hasNext();) {
            ResultCandidate resultCandidate = iterator2.next();
            if (resultCandidate.value >= this.rangeParameters.from - this.precision && resultCandidate.value < this.rangeParameters.to + precision) {
                if (resultCandidate.leftOk == null && resultCandidate.rightOk == null) {
                    if (resultCandidate.value > this.rangeParameters.from + step && resultCandidate.value < this.rangeParameters.to - step) {
                        continu &= resultConsumer.apply(resultCandidate.value);
                        iterator2.remove();
                    }
                }
                if (Boolean.TRUE.equals(resultCandidate.leftOk) && Boolean.TRUE.equals(resultCandidate.rightOk)) {
                    continu &= resultConsumer.apply(resultCandidate.value);
                    iterator2.remove();
                }
                if (!continu) {
                    break;
                }
            }
            if (resultCandidate.leftOk == null) {
                if (this.rangeParameters.to - precision < resultCandidate.value && resultCandidate.value < this.rangeParameters.to + step + precision) {
                    resultCandidate.leftOk = true;

                    if (Boolean.TRUE.equals(resultCandidate.leftOk) && Boolean.TRUE.equals(resultCandidate.rightOk)) {
                        continu &= resultConsumer.apply(resultCandidate.value);
                        iterator2.remove();
                    }
                    if (!continu) {
                        break;
                    }
                }
            }
            if (resultCandidate.rightOk == null) {
                if (resultCandidate.value < this.rangeParameters.from + precision && this.rangeParameters.from - (precision + step) < resultCandidate.value ||
                        this.rangeParameters.from < resultCandidate.value && resultCandidate.value < this.rangeParameters.to - step) {
                    resultCandidate.rightOk = true;

                    if (Boolean.TRUE.equals(resultCandidate.leftOk) && Boolean.TRUE.equals(resultCandidate.rightOk)) {
                        continu &= resultConsumer.apply(resultCandidate.value);
                        iterator2.remove();
                    }
                    if (!continu) {
                        break;
                    }
                }
            }
        }

        this.rangeParameters = rangeParameters;
        this.step = step;
        this.precision = precision;

        return continu;
    }

    @Override
    public boolean result(double value) {
        boolean found = false;
        for (int i = 0; i < cands.size(); i++) {
            ResultCandidate resCand = cands.get(i);
            if (resCand.value > value) {
                ResultCandidate element = new ResultCandidate(value);
                if (Math.abs(resCand.value - value) < (step + precision)) {
                    resCand.leftOk = false;
                    element.rightOk = false;
                } else {
                    resCand.leftOk = true;
                    element.rightOk = true;
                }
                cands.set(i, element);
                if (i > 0) {
                    ResultCandidate prevCand = cands.get(i - 1);
                    if (Math.abs(prevCand.value - value) < (step + precision)) {
                        prevCand.rightOk = false;
                        element.leftOk = false;
                    } else {
                        prevCand.rightOk = true;
                        element.leftOk = true;
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            cands.add(new ResultCandidate(value));
        }


        boolean continu = true;
        for (Iterator<ResultCandidate> iterator2 = cands.iterator(); iterator2.hasNext();) {
            ResultCandidate resCand = iterator2.next();
            if (Boolean.FALSE.equals(resCand.leftOk) && Boolean.FALSE.equals(resCand.rightOk)) {
                iterator2.remove();
                continue;
            }
            if (Boolean.TRUE.equals(resCand.leftOk) && Boolean.TRUE.equals(resCand.rightOk)) {
                continu &= resultConsumer.apply(resCand.value);
                iterator2.remove();
            }
            if (!continu) {
                break;
            }
        }
        return continu;
    }

    @Override
    public void finish() {
        for (Iterator<ResultCandidate> iterator2 = cands.iterator(); iterator2.hasNext();) {
            ResultCandidate resCand = iterator2.next();
            if (!Boolean.FALSE.equals(resCand.leftOk) && !Boolean.FALSE.equals(resCand.rightOk)) {
                boolean continu = resultConsumer.apply(resCand.value);
                iterator2.remove();
                if (!continu) {
                    break;
                }
            } else {
                iterator2.remove();
            }
        }
    }

    public static class ResultCandidate implements Comparator<ResultCandidate> {
        Boolean leftOk;

        Boolean rightOk;

        double value;

        public ResultCandidate(double value) {
            super();
            this.value = value;
        }

        @Override
        public int compare(ResultCandidate o1, ResultCandidate o2) {
            if (o1.value < o2.value) {
                return -1;
            }
            if (o1.value > o2.value) {
                return 1;
            }
            return 0;
        }
    }

    public static void main(String[] args) {
        Function<Double, Boolean> resultConsumer = new Function<Double, Boolean>() {
            @Override
            public Boolean apply(Double value) {
                System.out.println(value);
                return true;
            }};
        SmartPublisher filterResultPublisher = new SmartPublisher(resultConsumer);

        filterResultPublisher.solvingRange(new RangeParameters(0, 10), 1, 0.01);

        filterResultPublisher.result(10);

        filterResultPublisher.solvingRange(new RangeParameters(-10, 0), 1, 0.01);
        filterResultPublisher.solvingRange(new RangeParameters(10, 100), 1, 0.01);
        filterResultPublisher.solvingRange(new RangeParameters(-100, -10), 1, 0.01);

        System.out.println("finishing");
        filterResultPublisher.finish();
    }
}
