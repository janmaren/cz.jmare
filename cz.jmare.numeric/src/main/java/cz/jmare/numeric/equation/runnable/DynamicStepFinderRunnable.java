package cz.jmare.numeric.equation.runnable;

import static cz.jmare.numeric.equation.util.OrderRangesUtil.splitToOrderRanges;

import java.util.List;
import java.util.function.Consumer;

import cz.jmare.numeric.data.EqExprSuppliers;
import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.equation.finder.SteppingSolFinder;
import cz.jmare.numeric.equation.finder.UniqueResultsGuaranteer;
import cz.jmare.numeric.equation.util.OrderRangesUtil;
import cz.jmare.numeric.equation.util.SolveRange;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

/**
 * It separates finding to use concrete order of magnitude and using stepFactor it calculates concrete step for the order
 */
@Deprecated
public class DynamicStepFinderRunnable implements Runnable {
    private SteppingSolFinder finder;
    private SolveRange solveRange = SolveRange.wholeRange();
    private double stepFactor = 0.01;

    public DynamicStepFinderRunnable(String equation, Consumer<IterCalcMetadata> resultConsumer,
            UniqueResultsGuaranteer urg, SolveRange solveRange, double stepFactor) {
        super();
        this.setSolveRange(solveRange);
        this.finder = new SteppingSolFinder(EqExprSuppliers.of(equation), resultConsumer);
        this.finder.setUrg(urg);
        this.stepFactor = stepFactor;
    }

    public DynamicStepFinderRunnable(EquationDataPreparer equation, Consumer<IterCalcMetadata> resultConsumer,
            UniqueResultsGuaranteer urg, SolveRange solveRange, double stepFactor) {
        super();
        this.setSolveRange(solveRange);
        this.finder = new SteppingSolFinder(equation.getEquationData(), resultConsumer);
        this.finder.setUrg(urg);
        this.stepFactor = stepFactor;
    }

    public DynamicStepFinderRunnable(String equation, Consumer<IterCalcMetadata> resultConsumer) {
        super();
        this.finder = new SteppingSolFinder(EqExprSuppliers.of(equation), resultConsumer);
        this.finder.setUrg(new UniqueResultsGuaranteer());
    }

    @Override
    public void run() {
        Thread currentThread = Thread.currentThread();
        List<SolveRange> ranges = splitToOrderRanges(solveRange);
        ranges = OrderRangesUtil.orderRanges(ranges);
        for (SolveRange solveRange : ranges) {
            if (currentThread.isInterrupted()) {
                return;
            }
            double order = solveRange.from == 0 ? 0 : Math.ceil(Math.log10(Math.abs(solveRange.from)));
            double step = stepFactor * Math.pow(10, order);
            finder.find(solveRange.from, solveRange.to, step);
        }
    }

    public void setSolveRange(SolveRange solveRange) {
        this.solveRange = solveRange;
    }

    public void setNearTolerance(double nearTolerance) {
        finder.setNearTolerance(nearTolerance);
    }

    public static void main(String[] args) {
        UniqueResultsGuaranteer resultsCollector = new UniqueResultsGuaranteer();
        SigFigCollector iterCalcCollector = new SigFigCollector();
        DynamicStepFinderRunnable solFinderRunnable = new DynamicStepFinderRunnable("1 / (x + 1000) = 1", iterCalcCollector, resultsCollector, new SolveRange(-1e150, 1e150), 0.01);
        solFinderRunnable.run();
        System.out.println(iterCalcCollector.getValues());
    }
}
