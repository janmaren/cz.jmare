package cz.jmare.numeric.equation.runnable;

import java.util.function.Consumer;

import cz.jmare.numeric.data.EqExprSuppliers;
import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.equation.finder.SteppingSolFinder;
import cz.jmare.numeric.equation.finder.UniqueResultsGuaranteer;
import cz.jmare.numeric.equation.util.SolveRange;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

/**
 * Wrapper for SteppingSolFinder.find to be launched in a thread
 */
public class ConstantStepFinderRunnable implements Runnable {
    private SteppingSolFinder finder;
    private SolveRange solveRange = SolveRange.wholeRange();
    private double step;

    public ConstantStepFinderRunnable(String equation, Consumer<IterCalcMetadata> resultConsumer,
            UniqueResultsGuaranteer urg, SolveRange solveRange, double step) {
        super();
        this.solveRange = solveRange;
        this.finder = new SteppingSolFinder(EqExprSuppliers.of(equation), resultConsumer);
        this.finder.setUrg(urg);
        this.step = step;
    }

    public ConstantStepFinderRunnable(EquationDataPreparer equation, Consumer<IterCalcMetadata> resultConsumer,
            UniqueResultsGuaranteer urg, SolveRange solveRange, double step) {
        super();
        this.solveRange = solveRange;
        this.finder = new SteppingSolFinder(equation.getEquationData(), resultConsumer);
        this.finder.setUrg(urg);
        this.step = step;
    }

    @Override
    public void run() {
        finder.find(solveRange.from, solveRange.to, step);
    }

    public static void main(String[] args) {
        UniqueResultsGuaranteer resultsCollector = new UniqueResultsGuaranteer();
        SigFigCollector iterCalcCollector = new SigFigCollector();
        ConstantStepFinderRunnable solFinderRunnable = new ConstantStepFinderRunnable("1 / (x + 1000) = 1", iterCalcCollector, resultsCollector, new SolveRange(-1e4, 1e4), 1);
        solFinderRunnable.run();
        System.out.println(iterCalcCollector.getValues());
    }
}
