package cz.jmare.numeric.equation.runnable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.util.NumberPrecisionUtil;

public class SigFigCollector implements Consumer<IterCalcMetadata> {
    private List<IterCalcMetadata> results = new ArrayList<>();
    private int significantFiguresPrecision = 6;

    public SigFigCollector(int significantFiguresPrecision) {
        super();
        this.significantFiguresPrecision = significantFiguresPrecision;
    }

    public SigFigCollector() {
        super();
    }

    @Override
    public void accept(IterCalcMetadata iterCalcMetadata) {
        results.add(iterCalcMetadata);
    }

    public List<IterCalcMetadata> getResults() {
        return results;
    }

    public List<Double> getValues() {
        List<Double> values = new ArrayList<Double>();
        for (IterCalcMetadata iterCalcMetadata : results) {
            values.add(NumberPrecisionUtil.roundSignificant(iterCalcMetadata.value, significantFiguresPrecision));
        }
        return values;
    }
}
