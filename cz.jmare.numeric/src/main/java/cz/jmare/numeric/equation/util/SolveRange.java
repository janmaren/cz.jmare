package cz.jmare.numeric.equation.util;

public class SolveRange {
    public double from;

    public double to;

    public SolveRange(double from, double to) {
        super();
        this.from = from;
        this.to = to;
    }

    @Override
    public String toString() {
        return "SolveRange [from=" + from + ", to=" + to + "]";
    }

    public static SolveRange wholeRange() {
        return new SolveRange(-1e106, 1e106);
    }
}
