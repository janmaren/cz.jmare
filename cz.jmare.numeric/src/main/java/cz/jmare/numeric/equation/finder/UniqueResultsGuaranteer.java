package cz.jmare.numeric.equation.finder;

import java.util.ArrayList;
import java.util.List;

public class UniqueResultsGuaranteer {
    private List<DoubleResult> results = new ArrayList<DoubleResult>();

    /**
     * Add result to collection when not exists yet
     * @param doubleResult
     * @return true when really added, false when already existed
     */
    public synchronized boolean add(DoubleResult doubleResult) {
        for (DoubleResult doubleResult2 : results) {
            if (doubleResult.isEqual(doubleResult2)) {
                return false;
            }
        }
        results.add(doubleResult);
        return true;
    }
}
