package cz.jmare.numeric.equation;

public enum ResultType {
    CROSS_VALUE, TURNING_VALUE, NOT_FOUND, DISCONTINUITY
}
