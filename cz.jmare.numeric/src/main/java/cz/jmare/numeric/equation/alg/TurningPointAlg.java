package cz.jmare.numeric.equation.alg;

import static cz.jmare.numeric.equation.ResultType.NOT_FOUND;
import static cz.jmare.numeric.equation.ResultType.TURNING_VALUE;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.equation.IterCalcMetadata;

/**
 * Calculate point with lowest distance in given interval (both values from - to are excluding). Return
 * null when no such point exists in given interval
 */
public class TurningPointAlg {
    public static final double DEFAULT_PRECISION = 1e-11;
    public static final double MAX_ITERATIONS = 500;

    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberSupplier;
    private double precision = DEFAULT_PRECISION;
    private double nearTolerance = 1e-13;

    public TurningPointAlg(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberSupplier) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberSupplier = numberSupplier;
    }

    public IterCalcMetadata calcMetaValue(double from, double to) {
        double middle = (from + to) / 2;

        numberSupplier.accept(middle);
        double leftMiddle = leftSupplier.get();
        double rightMiddle = rightSupplier.get();
        double deltaMiddle = leftMiddle - rightMiddle;
        if (!Double.isFinite(deltaMiddle)) {
            return new IterCalcMetadata();
        }

        numberSupplier.accept(from);
        double leftFrom = leftSupplier.get();
        double rightFrom = rightSupplier.get();
        double deltaFrom = leftFrom - rightFrom;
        if (!Double.isFinite(deltaFrom)) {
            return new IterCalcMetadata();
        }

        numberSupplier.accept(to);
        double leftTo = leftSupplier.get();
        double rightTo = rightSupplier.get();
        double deltaTo = leftTo - rightTo;
        if (!Double.isFinite(deltaTo)) {
            return new IterCalcMetadata();
        }

        boolean opposite = false;
        if ((deltaTo + deltaFrom) / 2 < deltaMiddle) {
            opposite = true;
        }

        int iterations = 0;

        boolean fromChanged = false;
        boolean toChanged = false;

        do {
            iterations++;
            if (iterations > MAX_ITERATIONS) {
                return new IterCalcMetadata();
            }
            if (opposite) {
                if (deltaFrom < deltaMiddle) {
                    from = (from + middle) / 2.0;
                    numberSupplier.accept(from);
                    leftFrom = leftSupplier.get();
                    rightFrom = rightSupplier.get();
                    deltaFrom = leftFrom - rightFrom;
                    fromChanged = true;
                } else {
                    to = (to + middle) / 2.0;
                    numberSupplier.accept(to);
                    leftTo = leftSupplier.get();
                    rightTo = rightSupplier.get();
                    deltaTo = leftTo - rightTo;
                    toChanged = true;
                }
            } else {
                if (deltaFrom > deltaMiddle) {
                    from = (from + middle) / 2.0;
                    numberSupplier.accept(from);
                    leftFrom = leftSupplier.get();
                    rightFrom = rightSupplier.get();
                    deltaFrom = leftFrom - rightFrom;
                    fromChanged = true;
                } else {
                    to = (to + middle) / 2.0;
                    numberSupplier.accept(to);
                    leftTo = leftSupplier.get();
                    rightTo = rightSupplier.get();
                    deltaTo = leftTo - rightTo;
                    toChanged = true;
                }
            }
            middle = (from + to) / 2;
            numberSupplier.accept(middle);
            leftMiddle = leftSupplier.get();
            rightMiddle = rightSupplier.get();
            deltaMiddle = leftMiddle - rightMiddle;

            if (deltaMiddle == deltaFrom || deltaMiddle == deltaTo) {
//                return new IterCalcMetadata();
                if (!fromChanged || !toChanged) {
                    return new IterCalcMetadata(NOT_FOUND, null, iterations);
                }
                double yDistance = Math.abs(deltaMiddle);
                if (yDistance < nearTolerance) {
                    return new IterCalcMetadata(TURNING_VALUE, middle, iterations, yDistance);
                } else {
                    return new IterCalcMetadata(NOT_FOUND, null, iterations);
                }
            }
        } while (Math.abs(from - to) / 2 > precision);

        if (!fromChanged || !toChanged) {
            return new IterCalcMetadata(NOT_FOUND, null, iterations);
        }

        double yDistance = Math.abs(deltaMiddle);
        if (yDistance < nearTolerance) {
            return new IterCalcMetadata(TURNING_VALUE, middle, iterations, yDistance);
        } else {
            return new IterCalcMetadata(NOT_FOUND, null, iterations);
        }
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }

    public void setNearTolerance(double nearTolerance) {
        this.nearTolerance = nearTolerance;
    }
}
