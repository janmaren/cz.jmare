package cz.jmare.numeric.equation;

import java.util.function.Function;

import cz.jmare.numeric.range.RangeParameters;

@Deprecated
public class FlowResultPublisher implements ResultPublisher {
    private Function<Double, Boolean> resultConsumer;

    public FlowResultPublisher(Function<Double, Boolean> resultConsumer) {
        super();
        this.resultConsumer = resultConsumer;
    }

    @Override
    public boolean solvingRange(RangeParameters rangeParameters, double step, double precision) {
        return true;
    }

    @Override
    public boolean result(double value) {
        return resultConsumer.apply(value);
    }

    @Override
    public void finish() {
    }
}
