package cz.jmare.numeric.equation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class IterCalcCollector implements Consumer<IterCalcMetadata> {
    private List<IterCalcMetadata> results = new ArrayList<>();

    @Override
    public void accept(IterCalcMetadata iterCalcMetadata) {
        results.add(iterCalcMetadata);
    }

    public List<IterCalcMetadata> getResults() {
        return results;
    }
}
