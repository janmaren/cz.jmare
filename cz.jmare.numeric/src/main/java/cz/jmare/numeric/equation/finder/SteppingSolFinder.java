package cz.jmare.numeric.equation.finder;

import java.util.function.Consumer;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.data.EqExprSuppliers;
import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.equation.alg.BisectEasyAlg;
import cz.jmare.numeric.equation.alg.TurningPointAlg;

public class SteppingSolFinder {
    private static final int MAX_RECURSIVE_LEVEL = 100;

    public final static double EPSILON = 1e-13;

    public final static double RECURSION_PRECISION = 1e-7;

    private final Supplier<Double> leftSupplier;
    private final Supplier<Double> rightSupplier;
    private final DoubleHolder numberSupplier;
    private final Consumer<IterCalcMetadata> resultConsumer;
    private final BisectEasyAlg bisectCrossPointAlg;
    private final TurningPointAlg turningPointAlg;
    private UniqueResultsGuaranteer urg = new UniqueResultsGuaranteer();
    private Double distanceThreshold;

    public SteppingSolFinder(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberSupplier,
            Consumer<IterCalcMetadata> resultConsumer) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberSupplier = numberSupplier;
        this.resultConsumer = resultConsumer;
        this.bisectCrossPointAlg = new BisectEasyAlg(leftSupplier, rightSupplier, numberSupplier);
        this.turningPointAlg = new TurningPointAlg(leftSupplier, rightSupplier, numberSupplier);
    }

    public SteppingSolFinder(EqExprSuppliers eqDoubleSups, Consumer<IterCalcMetadata> resultConsumer) {
        this(eqDoubleSups.getLeftSupplier(), eqDoubleSups.getRightSupplier(), eqDoubleSups.getNumberSupplier(), resultConsumer);
    }

    /**
     *
     * @param from from which value to start finding
     * @param to to which value (excluding or including depending on excValTo)
     * @param step small step by which to find results
     */
    public void find(double from, double to, double step) {
        step = Math.abs(step);
        double increment = step;
        boolean increasing = true;
        if (from > to) {
            increment = -increment;
            increasing = false;
        }

        double[] xs = new double[4];
        double[] leftVals = new double[4];
        double[] rightVals = new double[4];

        xs[1] = from;
        xs[0] = xs[1] - increment;
        xs[2] = xs[1] + increment;

        numberSupplier.accept(xs[0]);
        leftVals[0] = leftSupplier.get();
        rightVals[0] = rightSupplier.get();

        numberSupplier.accept(xs[1]);
        leftVals[1] = leftSupplier.get();
        rightVals[1] = rightSupplier.get();

        numberSupplier.accept(xs[2]);
        leftVals[2] =  leftSupplier.get();
        rightVals[2] = rightSupplier.get();

        Thread currentThread = Thread.currentThread();
        while ((increasing && xs[1] < to) || (!increasing && xs[1] > to)) {
            if (currentThread.isInterrupted()) {
                return;
            }

            if (increasing) {
                if (xs[2] + step > to) {
                    xs[2] = to;
                }
            } else {
                if (xs[2] - step < to) {
                    xs[2] = to;
                }
            }

            xs[3] = xs[2] + increment;
            numberSupplier.accept(xs[3]);
            leftVals[3] = leftSupplier.get();
            rightVals[3] =  rightSupplier.get();

            boolean turningCandidate = isParabolicCandidate(leftVals, rightVals);
            if (turningCandidate) {
                boolean found = findTurning(xs[0], xs[3], step);
                if (!found) {
                    bisectCrossAlg(xs[1], xs[2], step);
                }
            } else {
                bisectCrossAlg(xs[1], xs[2], step);
            }

            System.arraycopy(xs, 1, xs, 0, 3);
            System.arraycopy(leftVals, 1, leftVals, 0, 3);
            System.arraycopy(rightVals, 1, rightVals, 0, 3);
        }
    }

    private void bisectCrossAlg(double from, double to, double step) {
        numberSupplier.accept(from);
        double leftValFrom = leftSupplier.get();
        double rightValFrom = rightSupplier.get();
        if (!(Double.isFinite(leftValFrom) && Double.isFinite(rightValFrom))) {
            Double result = null;
            if (from < to) {
                result = findDefinedIncreasing(from, to);
            } else {
                result = findDefinedDecreasing(from, to);
            }
            if (result == null) {
                return;
            }
            from = result;
            numberSupplier.accept(from);
            leftValFrom = leftSupplier.get();
            rightValFrom = rightSupplier.get();
        }

        numberSupplier.accept(to);
        double leftValTo = leftSupplier.get();
        double rightValTo = rightSupplier.get();
        if (!(Double.isFinite(leftValTo) && Double.isFinite(rightValTo))) {
            Double result = null;
            if (from < to) {
                result = findDefinedDecreasing(to, from);
            } else {
                result = findDefinedIncreasing(to, from);
            }
            if (result == null) {
                return;
            }
            to = result;
            numberSupplier.accept(to);
            leftValTo = leftSupplier.get();
            rightValTo = rightSupplier.get();
        }

        double middle = (from + to) / 2.0;

        numberSupplier.accept(middle);
        double leftValMiddle = leftSupplier.get();
        double rightValMiddle = rightSupplier.get();

        if (!Double.isFinite(middle)) {
            findCrossRecursive(from, to, step);
        } else if (((leftValFrom <= leftValMiddle && leftValMiddle <= leftValTo) || (leftValFrom >= leftValMiddle && leftValMiddle >= leftValTo)) &&
                ((rightValFrom <= rightValMiddle && rightValMiddle <= rightValTo) || (rightValFrom >= rightValMiddle && rightValMiddle >= rightValTo))  ) {
            findCrossSimple(from, to, step);
        } else {
            findCrossRecursive(from, to, step);
        }
    }

    /**
     * find turning which can be under a threshold. Return true when it doesn't make a sense to look
     * for crossection in this range
     * @param from
     * @param to
     * @param step
     * @return
     */
    private boolean findTurning(double from, double to, double step) {
        IterCalcMetadata calcMetaValue = turningPointAlg.calcMetaValue(from, to);
        if (calcMetaValue.value == null) {
            return false;
        }
        if (distanceThreshold != null) {
            if (calcMetaValue.value > distanceThreshold) {
                return true;
            }
        }
        boolean added = urg.add(new DoubleResult(calcMetaValue.value, step));
        if (added) {
            resultConsumer.accept(calcMetaValue);
        }
        return true;
    }

    private void findCrossRecursive(double from, double to, double step) {
        IterCalcMetadata iterCalcMetadata = findRecursive(from, to, 0);
        if (iterCalcMetadata.value != null) {
            boolean added = urg.add(new DoubleResult(iterCalcMetadata.value, step));
            if (added) {
                resultConsumer.accept(iterCalcMetadata);
            }
        }
    }

    private IterCalcMetadata findRecursive(double from, double to, int level) {
        if (level > MAX_RECURSIVE_LEVEL || from == to) {
            return new IterCalcMetadata();
        }
        double denominator = Math.max(Math.abs(from), Math.abs(to));
        if (denominator == 0) {
            denominator = 1.0;
        }
        if (Math.abs(from - to) / denominator <= RECURSION_PRECISION) {
            return bisectCrossPointAlg.calcMetaValue(from, to);
        }
        double middle = (from + to) / 2.0;
        if (!Double.isFinite(middle)) {
            return new IterCalcMetadata();
        }
        numberSupplier.accept(middle);
        double leftValMiddle = leftSupplier.get();
        double rightValMiddle = rightSupplier.get();

        Double useMiddle = middle;
        if (!(Double.isFinite(leftValMiddle) && Double.isFinite(rightValMiddle))) {
            if (from < to) {
                useMiddle = findDefinedDecreasing(useMiddle, from);
            } else {
                useMiddle = findDefinedIncreasing(useMiddle, from);
            }
        }
        if (useMiddle != null) {
            IterCalcMetadata calcMetaValue = findRecursive(from, useMiddle, level + 1);
            if (calcMetaValue.value != null) {
                return calcMetaValue;
            }
        }

        useMiddle = middle;
        if (!(Double.isFinite(leftValMiddle) && Double.isFinite(rightValMiddle))) {
            if (from < to) {
                useMiddle = findDefinedIncreasing(useMiddle, to);
            } else {
                useMiddle = findDefinedDecreasing(useMiddle, to);
            }
        }

        if (useMiddle != null) {
            return findRecursive(useMiddle, to, level + 1);
        }

        return new IterCalcMetadata();
    }

    private void findCrossSimple(double from, double to, double step) {
        IterCalcMetadata calcMetaValue = bisectCrossPointAlg.calcMetaValue(from, to);
        if (calcMetaValue.value != null) {
            boolean added = urg.add(new DoubleResult(calcMetaValue.value, step));
            if (added) {
                resultConsumer.accept(calcMetaValue);
            }
        }
    }

    private Double findDefinedIncreasing(double from, double limitToExc) {
        double timesEps = 1;
        while (from < limitToExc) {
            numberSupplier.accept(from);
            double leftVal = leftSupplier.get();
            double rightVal = rightSupplier.get();
            if (Double.isFinite(leftVal) && Double.isFinite(rightVal)) {
                return from;
            }
            from += timesEps * EPSILON;
            timesEps *= 2;
        }
        return null;
    }

    private Double findDefinedDecreasing(double from, double limitToExc) {
        double timesEps = 1;
        while (from > limitToExc) {
            numberSupplier.accept(from);
            double leftVal = leftSupplier.get();
            double rightVal = rightSupplier.get();
            if (Double.isFinite(leftVal) && Double.isFinite(rightVal)) {
                return from;
            }
            from -= timesEps * EPSILON;
            timesEps *= 2;
        }
        return null;
    }

    private final static boolean isParabolicCandidate(double[] leftVals, double[] rightVals) {
        double diff0 = leftVals[0] - rightVals[0];
        double diff1 = leftVals[1] - rightVals[1];
        double diff2 = leftVals[2] - rightVals[2];
        double diff3 = leftVals[3] - rightVals[3];
        double absDiff0 = Math.abs(diff0);
        double absDiff1 = Math.abs(diff1);
        double absDiff2 = Math.abs(diff2);
        double absDiff3 = Math.abs(diff3);
        if (Math.abs(absDiff0 - absDiff1) < 1e-13 || Math.abs(absDiff2 - absDiff3) < 1e-13) {
            return false;
        }
        if (absDiff0 < absDiff1 || absDiff2 > absDiff3) {
            return false;
        }
        boolean res = Math.signum(diff0 - diff1) != Math.signum(diff2 - diff3);
        if (!res) {
            return false;
        }
        if (Math.abs(diff0 - diff1) / Math.abs(diff2 - diff3)  > 1000 || Math.abs(diff2 - diff3) / Math.abs(diff0 - diff1) > 1000) {
            return false;
        }
        return true;
    }

    public void setPrecision(double precision) {
        bisectCrossPointAlg.setPrecision(precision);
        turningPointAlg.setPrecision(precision);
    }

    public void setDistanceThreshold(Double underDistance) {
        this.distanceThreshold = underDistance;
    }

    /**
     * Use when various SteppingSolFinder instances should share one result collector
     * @param resultsCollector
     */
    public void setUrg(UniqueResultsGuaranteer resultsCollector) {
        this.urg = resultsCollector;
    }

    public void setNearTolerance(double maxY) {
        this.turningPointAlg.setNearTolerance(maxY);
    }
}
