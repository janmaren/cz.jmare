package cz.jmare.numeric.equation.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderRangesUtil {
    /**
     * Get next order value. For example: 1=&gt;10; 10=&gt;100; -10=&gt;-1; -5=&gt;-1<br>
     * Between -1 and 1 there is not a decimal order but it's always 1, like: -1=&gt;1; -0.5=&gt;1<br>
     * It returns infinity when input value is 1.0E308, so: 1.0E308=&gt;Infinity
     * @param value any decimal value
     * @return next order value, like -100, -10, -1, 1, 10, 100...
     */
    public static double getNextOrderValue(double value) {
        if (value < 0) {
            if (value > -1) {
                return 1;
            }
            double log10 = Math.ceil(Math.log10(Math.abs(value)));
            double posTo = Math.pow(10, log10 - 1);
            if (posTo < 1) {
                if (value < -1) {
                    return -1;
                }
                return 1;
            }
            posTo = -posTo;
            if (posTo == value) {
                throw new RuntimeException("Value is same as input value!");
            }
            return posTo;
        } else {
            if (value < 1) {
                return 1;
            }
            double log10 = Math.floor(Math.log10(Math.abs(value)));
            double posTo = Math.pow(10, log10 + 1);
            if (posTo == value) {
                throw new RuntimeException("Value is same as input value!");
            }
            return posTo;
        }
    }

    /**
     * Split to order ranges where each result SolveRange represents one order
     * @param solveRange range across multiple orders to be splited
     * @return ranges where each range is is a concrete order
     */
    public static List<SolveRange> splitToOrderRanges(SolveRange solveRange) {
        double from = solveRange.from;
        double to = solveRange.to;
        double posFrom = from;
        double posTo = getNextOrderValue(posFrom);
        List<SolveRange> ranges = new ArrayList<>();
        if (Double.isInfinite(posTo)) {
            return ranges;
        }
        while (posFrom < to) {
            ranges.add(new SolveRange(posFrom, posTo < to ? posTo : to));
            posFrom = posTo;
            posTo = getNextOrderValue(posFrom);
            if (Double.isInfinite(posTo)) {
                return ranges;
            }
        }
        return ranges;
    }

    /**
     * Get number of ranges where each range represents the same number of values to be processed with given step based on order
     * @param from
     * @param to
     * @param rangesNumber result number of ranges (it can be number of threads where each thread calculates given range)
     * @return
     */
    public static List<SolveRange> getRanges(double from, double to, int rangesNumber) {
        if (from > to) {
            double tmp = from;
            from = to;
            to = tmp;
        }

        double orders = 0;

        if (from < 0) {
            double fromLog = Math.log10(Math.abs(from));
            if (to < 0) {
                double toLog = Math.log10(Math.abs(to));
                orders = toLog - fromLog;
            } else {
                orders += fromLog;
            }
        }

        if (to > 0) {
            double toLog = Math.log10(to);
            if (from > 0) {
                double fromLog = Math.log10(from);
                orders += toLog - fromLog;
            } else {
                orders += toLog;
            }
        }

        double div = orders / rangesNumber;
        List<SolveRange> ranges = new ArrayList<>();
        double pos = from;
        while (pos < 0) {
            double log10 = Math.log10(Math.abs(pos));
            double upto = -Math.pow(10, log10 - div);
            if (ranges.size() + 1 >= rangesNumber) {
                ranges.add(new SolveRange(pos, to));
                return ranges;
            }
            if (upto > -1) {
                ranges.add(new SolveRange(pos, 0));
                pos = 0;
                break;
            }
            ranges.add(new SolveRange(pos, upto));
            pos = upto;
        }
        while (pos < to) {
            double log10 = pos < 1 ? 0 : Math.log10(pos);
            double upto = pos + Math.pow(10, log10 + div);
            if (upto > to || ranges.size() + 1 >= rangesNumber) {
                ranges.add(new SolveRange(pos, to));
                return ranges;
            }
            ranges.add(new SolveRange(pos, upto));
            pos = upto;
        }

        return ranges;
    }

    /**
     * Sort by order, the order 1 and -1 is lowest which is first but the -1e306 and 1e306 is biggest which is last
     * @param ranges
     * @return
     */
    public static List<SolveRange> orderRanges(List<SolveRange> ranges) {
        Collections.sort(ranges, (o1, o2) -> (int) (Math.log(Math.abs(o1.from)) - Math.log(Math.abs(o2.from))));
        return ranges;
    }

    public static void main(String[] args) {
        List<SolveRange> ranges = getRanges(-10, 1, 2);
        for (SolveRange solveRange : ranges) {
            System.out.println(solveRange + ":" + (Math.log10(Math.abs(solveRange.to/solveRange.from))));
        }

//        ranges = splitToOrderRanges(new SolveRange(-100000.1, 10000.5));
//        for (SolveRange solveRange : ranges) {
//            System.out.println(solveRange);
//        }
    }
}
