package cz.jmare.numeric.equation.finder;

public class DoubleResult {
    /**
     * Result value
     */
    private double value;

    /**
     * The only value in range. When another value in given range exists it supposed to be a duplicit result.
     */
    private double inRange;

    public DoubleResult(double value, double inRange) {
        super();
        this.value = value;
        this.inRange = inRange;
    }

    public boolean isEqual(DoubleResult other) {
        return this.value - inRange <= other.value && other.value <= this.value + inRange || other.value - inRange <= this.value && this.value <= other.value + inRange;
    }
}
