package cz.jmare.numeric.equation;

import cz.jmare.numeric.range.RangeParameters;

@Deprecated
public interface ResultPublisher {
    boolean solvingRange(RangeParameters rangeParameters, double step, double precision);
    boolean result(double value);
    void finish();
}
