package cz.jmare.numeric.equation;

import java.util.List;
import java.util.stream.Collectors;

import cz.jmare.mexpr.Config;
import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.equation.finder.UniqueResultsGuaranteer;
import cz.jmare.numeric.equation.runnable.DynamicStepFinderRunnable;
import cz.jmare.numeric.equation.util.SolveRange;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

public class EqFinders {

    public static List<Double> ofDynamic(String equation) {
        return ofDynamic(0, List.of(equation), null, null);
    }

    public static List<Double> ofDynamic(int index, List<String> statements) {
        return ofDynamic(index, statements, null, null);
    }

    public static List<Double> ofDynamic(int index, List<String> statements, String solveVariable) {
        return ofDynamic(index, statements, null, solveVariable);
    }

    public static List<Double> ofDynamic(int index, List<String> statements, Config config,
            String solveVariable) {
        EqSolverData eqSolverData = new EqSolverData().withDefinitions(statements).withEqDefIndex(index);
        if (config != null) {
            eqSolverData.withConfig(config);
        }
        if (solveVariable != null) {
            eqSolverData.withSolveVariable(solveVariable);
        }
        return ofDynamic(eqSolverData);
    }

    public static List<Double> ofDynamic(EqSolverData eqSolverData) {
        EquationDataPreparer equationDataPreparer = new EquationDataPreparer(eqSolverData);
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        DynamicStepFinderRunnable dynamicStepFinderRunnable = new DynamicStepFinderRunnable(equationDataPreparer, iterCalcCollector, new UniqueResultsGuaranteer(), SolveRange.wholeRange(), 0.01);
        dynamicStepFinderRunnable.setSolveRange(eqSolverData.getSolveRange());
        if (eqSolverData.getNearTolerance() != null) {
            dynamicStepFinderRunnable.setNearTolerance(eqSolverData.getNearTolerance());
        }
        dynamicStepFinderRunnable.run();
        List<Double> values = iterCalcCollector.getResults().stream().filter(r -> r.value != null).map(r -> r.value).collect(Collectors.toList());
        return values;
    }
}
