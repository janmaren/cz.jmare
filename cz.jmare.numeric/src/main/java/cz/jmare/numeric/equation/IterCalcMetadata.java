package cz.jmare.numeric.equation;

public class IterCalcMetadata {
    public Double value;

    public int iterations;

    /**
     * Positive or negative or zero distance of both y values
     * When left expression is lower than right then it's negative
     */
    public Double yDistance;

    public final ResultType resultType;

    public IterCalcMetadata() {
        this.resultType = ResultType.NOT_FOUND;
    }

    public IterCalcMetadata(ResultType resultType) {
        this.resultType = resultType;
    }

    public IterCalcMetadata(ResultType resultType, Double value) {
        this.resultType = resultType;
        this.value = value;
    }

    public IterCalcMetadata(ResultType resultType, Double value, int iterations) {
        super();
        this.resultType = resultType;
        this.value = value;
        this.iterations = iterations;
    }

    public IterCalcMetadata(ResultType resultType, Double value, int iterations, Double yDistance) {
        super();
        this.resultType = resultType;
        this.value = value;
        this.yDistance = yDistance;
        this.iterations = iterations;
    }

    @Override
    public String toString() {
        return "IterCalcMetadata [value=" + value + ", yDistance=" + yDistance + ", iterations=" + iterations + "]";
    }
}
