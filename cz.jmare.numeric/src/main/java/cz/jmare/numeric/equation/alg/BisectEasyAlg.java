package cz.jmare.numeric.equation.alg;

import static cz.jmare.numeric.equation.ResultType.CROSS_VALUE;
import static cz.jmare.numeric.equation.ResultType.DISCONTINUITY;
import static cz.jmare.numeric.equation.ResultType.NOT_FOUND;
import static java.lang.Math.abs;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.data.EqExprSuppliers;
import cz.jmare.numeric.equation.IterCalcMetadata;

/**
 * Calculate crossection point in given interval (both values from - to are including). Return
 * null when no such crossection exists or there is a discontinuity
 */
public class BisectEasyAlg {
    public static final double EPSILON = 1e-10;
    public static final double DEFAULT_PRECISION = 1e-10;

    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberSupplier;
    private double precision = DEFAULT_PRECISION;

    public BisectEasyAlg(EqExprSuppliers equationData) {
        this(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
    }

    public BisectEasyAlg(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberSupplier) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberSupplier = numberSupplier;
    }

    public Double calculateValue(double from, double to) {
        IterCalcMetadata calculateValue = calcMetaValue(from, to);
        return calculateValue.value;
    }

    public Double calculateValue(double from, double to, boolean toValExcluding) {
        IterCalcMetadata calculateValue = calcMetaValue(from, to);
        return calculateValue.value;
    }

    public IterCalcMetadata calcMetaValue(double from, double to) {
        numberSupplier.accept(from);
        double leftValFrom = leftSupplier.get();
        double rightValFrom = rightSupplier.get();

        numberSupplier.accept(to);
        double leftValTo = leftSupplier.get();
        double rightValTo = rightSupplier.get();

        return calcMetaValue(from, to, leftValFrom, rightValFrom, leftValTo, rightValTo);
    }

    public IterCalcMetadata calcMetaValue(double from, double to, double leftValFrom, double rightValFrom, double leftValTo, double rightValTo) {
        double fromPos = from;
        double toPos = to;
        if (!Double.isFinite(leftValFrom) || !Double.isFinite(rightValFrom)) {
            return new IterCalcMetadata();
        }
        if (!Double.isFinite(leftValTo) || !Double.isFinite(rightValTo)) {
            return new IterCalcMetadata();
        }
        if (abs(leftValFrom - rightValFrom) / (leftValFrom == 0 ? 1 : Math.abs(leftValFrom)) < EPSILON) {
            if (abs(leftValTo - rightValTo) / (leftValTo == 0 ? 1 : Math.abs(leftValTo)) < EPSILON) {
                return new IterCalcMetadata();
            }
        }
        if (leftValFrom == rightValFrom) {
            return new IterCalcMetadata(CROSS_VALUE, from, 0, 0.0);
        }
        if (leftValTo == rightValTo) {
            return new IterCalcMetadata(CROSS_VALUE, to, 0, 0.0);
        }
        double middle = 0;
        int iterations = 0;
        while (Math.abs(toPos - fromPos) > precision) {
            iterations++;
            middle = (toPos + fromPos) / 2.0;
            numberSupplier.accept(middle);
            double leftValMid = leftSupplier.get();
            double rightValMid = rightSupplier.get();
            if (!Double.isFinite(leftValMid) || !Double.isFinite(rightValMid)) {
                return new IterCalcMetadata();
            }
            if (leftValMid == rightValMid) {
                return new IterCalcMetadata(CROSS_VALUE, middle, iterations, 0.0);
            }
            if (leftValFrom < rightValFrom) {
                if (leftValMid < rightValMid) {
                    if (leftValTo < rightValTo) {
                        return new IterCalcMetadata(DISCONTINUITY);
                    }
                    fromPos = middle;
                    leftValFrom = leftValMid;
                    rightValFrom = rightValMid;
                } else {
                    if (leftValTo < rightValTo) {
                        return new IterCalcMetadata(DISCONTINUITY);
                    }
                    toPos = middle;
                    leftValTo = leftValMid;
                    rightValTo = rightValMid;
                }
            } else {
                if (leftValMid < rightValMid) {
                    if (leftValTo > rightValTo) {
                        return new IterCalcMetadata(DISCONTINUITY);
                    }
                    toPos = middle;
                    leftValTo = leftValMid;
                    rightValTo = rightValMid;
                } else {
                    if (leftValTo > rightValTo) {
                        return new IterCalcMetadata(DISCONTINUITY);
                    }
                    fromPos = middle;
                    leftValFrom = leftValMid;
                    rightValFrom = rightValMid;
                }
            }
        }

        numberSupplier.accept(middle);
        Double leftValMiddle = leftSupplier.get();
        Double rightValMiddle = rightSupplier.get();
        if (!Double.isFinite(leftValMiddle) || !Double.isFinite(rightValMiddle)) {
            return new IterCalcMetadata(DISCONTINUITY, null, iterations);
        }

        double yDiff = leftValMiddle - rightValMiddle;
        double yDist = Math.abs(yDiff);
        if (yDist > 1) {
            return new IterCalcMetadata(NOT_FOUND, null, iterations); // TODO: find better solution
        }
        double fromDist = leftValFrom - rightValFrom;
        double toDist = leftValTo - rightValTo;
        if (fromDist <= yDiff && yDiff <= toDist) {
            return new IterCalcMetadata(CROSS_VALUE, middle, iterations, yDist);
        }
        if (fromDist >= yDiff && yDiff >= toDist) {
            return new IterCalcMetadata(CROSS_VALUE, middle, iterations, yDist);
        }

        return new IterCalcMetadata(NOT_FOUND, null, iterations);
    }

    public void setPrecision(double precision) {
        this.precision = precision;
    }
}
