package cz.jmare.numeric.equation;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import cz.jmare.mexpr.arit.adapter.FunctionToSupplierAdapter;
import cz.jmare.mexpr.arit.adapter.NumberSupplierAdapter;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.params.PrecisionProvider;
import cz.jmare.numeric.params.PrecisionValues;
import cz.jmare.numeric.params.SimplePrecision;
import cz.jmare.numeric.solution.BisectionNumericAlgorithm;
import cz.jmare.numeric.solution.NearingNumericAlgorithm;
import cz.jmare.numeric.util.SingularityUtil;
import cz.jmare.numeric.util.TwoDoubles;

@Deprecated
public class SimpleNumericSolution {
    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberHolder;
    private BisectionNumericAlgorithm bisectionNumericAlgorithm;
    private NearingNumericAlgorithm nearingNumericAlgorithm;
    private Consumer<Double> progress;
    private PrecisionProvider precisionProvider = new SimplePrecision();
    private BiFunction<Double, Double, Double> stepForRangeFunction;

    public SimpleNumericSolution(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberHolder) {
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
        bisectionNumericAlgorithm = new BisectionNumericAlgorithm(this.leftSupplier, this.rightSupplier, this.numberHolder);
        nearingNumericAlgorithm = new NearingNumericAlgorithm(this.leftSupplier, this.rightSupplier, this.numberHolder);
    }

    public Double solve(double inputFromIncluding, double inputToExcluding) {
        PrecisionValues precisionValues = getPrecisionValues(inputFromIncluding, inputToExcluding);

        double step = stepForRangeFunction != null ? stepForRangeFunction.apply(inputFromIncluding, inputToExcluding) :precisionValues.step;
        nearingNumericAlgorithm.setPrecision(precisionValues.precision);
        nearingNumericAlgorithm.setNearingPrecision(precisionValues.yTouchTolerance);
        nearingNumericAlgorithm.setRoundingOrder(precisionValues.roundingOrder);
        bisectionNumericAlgorithm.setPrecision(precisionValues.precision);
        bisectionNumericAlgorithm.setRoundingOrder(precisionValues.roundingOrder);

        Thread currentThread = Thread.currentThread();

        double prevX = inputFromIncluding;

        numberHolder.accept(prevX - step);
        double firstLeftValue = leftSupplier.get();
        double firstRightValue = rightSupplier.get();

        numberHolder.accept(prevX);
        double prevLeftValue = leftSupplier.get();
        double prevRightValue = rightSupplier.get();
        if (prevLeftValue == prevRightValue) {
            return prevX;
        }

        bisectionNumericAlgorithm.reset(prevX, prevLeftValue, prevRightValue);
        nearingNumericAlgorithm.reset(prevX, prevLeftValue, prevRightValue);

        double x = prevX + step;
        while (prevX < inputToExcluding) {
            if (progress != null) {
                progress.accept(x);
            }

            numberHolder.accept(x);
            double leftValue = leftSupplier.get();
            double rightValue = rightSupplier.get();

            TwoDoubles twoDoublesLeft = null;
            TwoDoubles twoDoublesRight = null;
            if ((SingularityUtil.suspectForSingularity(firstLeftValue, prevLeftValue, leftValue) || SingularityUtil.suspectForSingularity(firstRightValue, prevRightValue, rightValue))
                    && ((twoDoublesLeft = SingularityUtil.getSingularity(leftSupplier, numberHolder, prevX, x,
                            precisionValues.precision)) != null
                            || (twoDoublesRight = SingularityUtil.getSingularity(rightSupplier, numberHolder, prevX,
                                    x, precisionValues.precision)) != null)) {

                BisectionNumericAlgorithm bisectionNumericAlgorithmInner = new BisectionNumericAlgorithm(this.leftSupplier, this.rightSupplier, this.numberHolder);
                bisectionNumericAlgorithmInner.reset(prevX, prevLeftValue, prevRightValue);
                double x1 = twoDoublesLeft != null ? twoDoublesLeft.x1 : twoDoublesRight.x1;

                Double valueInRange = bisectionNumericAlgorithmInner.calculateValue(x1, leftValue, rightValue);

                if (valueInRange == null || valueInRange >= inputToExcluding) {
                    NearingNumericAlgorithm nearingNumericAlgorithmInner = new NearingNumericAlgorithm(this.leftSupplier, this.rightSupplier, this.numberHolder);
                    nearingNumericAlgorithmInner.reset(prevX, prevLeftValue, prevRightValue);
                    valueInRange = nearingNumericAlgorithmInner.calculateValue(x1, leftValue, rightValue);
                }

                if (valueInRange != null) {
                    if (valueInRange < inputToExcluding) {
                        return valueInRange;
                    }
                    return null;
                }

                firstLeftValue = Double.POSITIVE_INFINITY;
                firstRightValue = Double.POSITIVE_INFINITY;

                double x2 = twoDoublesLeft != null ? twoDoublesLeft.x2 : twoDoublesRight.x2;
                prevX = x2;
                numberHolder.accept(prevX);
                prevLeftValue = leftSupplier.get();
                prevRightValue = rightSupplier.get();
                bisectionNumericAlgorithm.reset(x2, prevLeftValue, prevRightValue);
                nearingNumericAlgorithm.reset(x2, prevLeftValue, prevRightValue);

                x = x2 + step;
            } else {
                Double valueByBisection = bisectionNumericAlgorithm.calculateValue(x, leftValue, rightValue);
                if (valueByBisection != null) {
                    if (inputFromIncluding <= valueByBisection) {
                        if (valueByBisection < inputToExcluding) {
                            return valueByBisection;
                        } else {
                            return null;
                        }
                    }
                }

                Double valueByNearing = nearingNumericAlgorithm.calculateValue(x, leftValue, rightValue);
                if (valueByNearing != null) {
                    if (inputFromIncluding <= valueByNearing) {
                        if (valueByNearing < inputToExcluding) {
                            return valueByNearing;
                        } else {
                            return null;
                        }
                    }
                }

                firstLeftValue = prevLeftValue;
                prevLeftValue = leftValue;
                firstRightValue = prevRightValue;
                prevRightValue = rightValue;
                x = (prevX = x) + step;
            }

            if (currentThread.isInterrupted()) {
                if (progress != null) {
                    progress.accept(null);
                }
                return null;
            }
        }
        return null;
    }

    protected PrecisionValues getPrecisionValues(double inputFromIncluding, double inputToExcluding) {
        return precisionProvider.getPrecisionValues(inputFromIncluding, inputToExcluding);
    }

    public void setProgress(Consumer<Double> progress) {
        this.progress = progress;
    }

    public SimpleNumericSolution withPrecisionProvider(PrecisionProvider precisionProvider) {
        this.precisionProvider = precisionProvider;
        return this;
    }

    public SimpleNumericSolution withConstantStep(double constantStep) {
        this.stepForRangeFunction = new BiFunction<Double, Double, Double>() {
            @Override
            public Double apply(Double x1, Double x2) {
                return constantStep;
            }};
        return this;
    }

    public SimpleNumericSolution withStepForRangeFunction(BiFunction<Double, Double, Double> stepForRangeFunction) {
        this.stepForRangeFunction = stepForRangeFunction;
        return this;
    }

    /**
     * Create SimpleNumericSolution with inner NumberHolder where both functions are bound with the NumberHolder
     * Passed objects may be obtained by such creation:
     * <pre>
     * Function<Double, Double> leftFunction = (x) -> x * x;
     * Function<Double, Double> rightFunction = (x) -> 100;
     * // solution for such equation will be number 10
     * </pre>
     * @param leftFunction
     * @param rightFunction
     * @return
     */
    public static SimpleNumericSolution getInstance(Function<Double, Double> leftFunction, Function<Double, Double> rightFunction) {
        FunctionToSupplierAdapter leftFunctionToSupplierAdapter = new FunctionToSupplierAdapter(leftFunction);
        FunctionToSupplierAdapter rightFunctionToSupplierAdapter = new FunctionToSupplierAdapter(rightFunction);
        NumberSupplierAdapter numberSupplierAdapter = new NumberSupplierAdapter(leftFunctionToSupplierAdapter, rightFunctionToSupplierAdapter);
        return new SimpleNumericSolution(leftFunctionToSupplierAdapter, rightFunctionToSupplierAdapter, numberSupplierAdapter);
    }

    /**
     * Create SimpleNumericSolution and bind leftFunction with existing NumberHolder. The rightSupplier must be already bound with NumberHolder.
     * Passed objects may be obtained by such creation:
     * <pre>
     * Function<Double, Double> leftFunction = (x) -> x * x;
     * ExpressionSupplier rightSupplier = new ExpressionSupplier("-((x + 1) ^ 2) + 2");
     * NumberHolder numberHolder = rightSupplier.getNumberSupplier();
     * </pre>
     * @param leftFunction
     * @param rightSupplier
     * @param numberHolder
     * @return
     */
    public static SimpleNumericSolution getInstance(Function<Double, Double> leftFunction, Supplier<Double> rightSupplier, DoubleHolder numberHolder) {
        FunctionToSupplierAdapter leftSupplier = new FunctionToSupplierAdapter(leftFunction);
        numberHolder = new NumberSupplierAdapter(numberHolder, (FunctionToSupplierAdapter) leftSupplier);
        return new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
    }

    /**
     * Create SimpleNumericSolution and bind rightFunction with existing NumberHolder. The leftSupplier must be already bound with NumberHolder.
     * Passed objects may be obtained by such creation:
     * <pre>
     * ExpressionSupplier leftSupplier = new ExpressionSupplier("-((x + 1) ^ 2) + 2");
     * NumberHolder numberHolder = leftSupplier.getNumberSupplier();
     * Function<Double, Double> rightFunction = (x) -> x * x;
     * </pre>
     * @param leftSupplier
     * @param rightFunction
     * @param numberHolder
     * @return
     */
    public static SimpleNumericSolution getInstance(Supplier<Double> leftSupplier, Function<Double, Double> rightFunction, DoubleHolder numberHolder) {
        FunctionToSupplierAdapter rightSupplier = new FunctionToSupplierAdapter(rightFunction);
        numberHolder = new NumberSupplierAdapter(numberHolder, (FunctionToSupplierAdapter) rightSupplier);
        return new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
    }
}
