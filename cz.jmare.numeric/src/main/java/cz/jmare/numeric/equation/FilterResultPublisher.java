package cz.jmare.numeric.equation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import cz.jmare.numeric.range.RangeParameters;

@Deprecated
public class FilterResultPublisher implements ResultPublisher {
    private Function<Double, Boolean> resultConsumer;
    private RangeParameters rangeParameters;
    private double step;
    private double precision;
    private List<ResultCandidate> resultCandidates = new ArrayList<>();

    public FilterResultPublisher(Function<Double, Boolean> resultConsumer) {
        super();
        this.resultConsumer = resultConsumer;
    }

    @Override
    public boolean solvingRange(RangeParameters rangeParameters, double step, double precision) {
        boolean continu = true;
        for (Iterator<ResultCandidate> iterator = resultCandidates.iterator(); iterator.hasNext();) {
            ResultCandidate resultCandidate = iterator.next();
            if (resultCandidate.leftSpace != null && resultCandidate.value - (step + precision) < this.rangeParameters.from ||
                    resultCandidate.rightSpace != null && resultCandidate.value + step + precision > this.rangeParameters.to) {
                // keep
            } else {
                if (!resultCandidate.expunged && continu) {
                    continu &= resultConsumer.apply(resultCandidate.value);
                }
                iterator.remove();
            }
        }
        this.rangeParameters = rangeParameters;
        this.step = step;
        this.precision = precision;
        return continu;
    }

    @Override
    public boolean result(double value) {
        boolean toExpunge = false;
        for (Iterator<ResultCandidate> iterator = resultCandidates.iterator(); iterator.hasNext();) {
            ResultCandidate resultCandidate = iterator.next();
            if (resultCandidate.value > value - (step + precision) && resultCandidate.value < value + step + precision) {
                toExpunge = true;
            }
            if (resultCandidate.expunged) {
                continue;
            }

            if (resultCandidate.leftSpace != null) {
                if (value >= resultCandidate.leftSpace && value <= resultCandidate.value) {
                    resultCandidate.expunged = true;
                    continue;
                }
            }

            if (resultCandidate.rightSpace != null) {
                if (value <= resultCandidate.rightSpace && value >= resultCandidate.value) {
                    if (rangeParameters.from < resultCandidate.value - (step + precision)) {
                        iterator.remove();
                    }
                    resultCandidate.expunged = true;
                    continue;
                }
                if (value > resultCandidate.value) {
                    resultCandidate.rightSpace = null;
                }
            }
            if (resultCandidate.leftSpace == null && resultCandidate.rightSpace == null && !resultCandidate.expunged) {
                Boolean continu = resultConsumer.apply(resultCandidate.value);
                iterator.remove();
                if (!continu) {
                    return false;
                }
            }
        }
        ResultCandidate resultCandidate = new ResultCandidate(value);
        if (rangeParameters.from > value - (step + precision)) {
            resultCandidate.leftSpace = value - (step + precision);
        }
        resultCandidate.rightSpace = value + step + precision;
        resultCandidate.expunged = toExpunge;
        resultCandidates.add(resultCandidate);
        return true;
    }

    @Override
    public void finish() {
        for (Iterator<ResultCandidate> iterator = resultCandidates.iterator(); iterator.hasNext();) {
            ResultCandidate resultCandidate = iterator.next();
            if (!resultCandidate.expunged) {
                Boolean continu = resultConsumer.apply(resultCandidate.value);
                iterator.remove();
                if (!continu) {
                    return;
                }
            }
        }
    }

    static class ResultCandidate {
        double value;
        Double leftSpace;
        Double rightSpace;
        boolean expunged;
        public ResultCandidate(double value) {
            super();
            this.value = value;
        }
        @Override
        public String toString() {
            return "v=" + value + ", l=" + leftSpace + ", r=" + rightSpace
                    + ", e=" + expunged;
        }
    }

    public static void main(String[] args) {
        Function<Double, Boolean> resultConsumer = new Function<Double, Boolean>() {
            @Override
            public Boolean apply(Double value) {
                System.out.println(value);
                return true;
            }};
        FilterResultPublisher filterResultPublisher = new FilterResultPublisher(resultConsumer);

        filterResultPublisher.solvingRange(new RangeParameters(0, 10), 1, 0.01);

        filterResultPublisher.result(10);

        filterResultPublisher.solvingRange(new RangeParameters(-10, 0), 1, 0.01);
        filterResultPublisher.solvingRange(new RangeParameters(10, 100), 1, 0.01);
        filterResultPublisher.solvingRange(new RangeParameters(-100, -10), 1, 0.01);

        System.out.println("finishing");
        filterResultPublisher.finish();
    }
}
