package cz.jmare.numeric.evaluate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.exception.SemanticException;
import cz.jmare.mexpr.exception.SyntaxException;
import cz.jmare.mexpr.exception.semantic.UndefinedVariableException;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.DefinedFunction;
import cz.jmare.mexpr.util.ExprVarUtil;
import cz.jmare.mexpr.util.LeftRight;
import cz.jmare.mexpr.util.NotExpressionException;
import cz.jmare.mexpr.util.StatementAwareEvaluateException;
import cz.jmare.mexpr.util.StatementAwareParseException;
import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;
import cz.jmare.mexpr.util.classutil.SupplierTypeUtil;
import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.data.SimpleEquationData;

@Deprecated
public class EquationDataPreparer {
    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberHolder;
    private String calculateVariableName;

    /**
     * All used variables found on definition lines like 'x: 123'
     */
    private Set<VariableIndex> usedVariables;

    public EquationDataPreparer(int index, List<String> definitions, Config config) {
        EqSolverData eqSolverBuilder = new EqSolverData().withEqDefIndex(index).withDefinitions(definitions).withConfig(config);
        init(eqSolverBuilder);
    }

    public EquationDataPreparer(int index, List<String> definitions, Map<String, Supplier<?>> predefinedVariableSuppliers, Config config) {
        EqSolverData eqSolverBuilder = new EqSolverData().withEqDefIndex(index).withDefinitions(definitions).withConfig(config).withPredefinedVariableSuppliers(predefinedVariableSuppliers);
        init(eqSolverBuilder);
    }

    public EquationDataPreparer(int index, List<String> definitions) {
        EqSolverData eqSolverBuilder = new EqSolverData().withEqDefIndex(index).withDefinitions(definitions);
        init(eqSolverBuilder);
    }

    public EquationDataPreparer(String definition) {
        EqSolverData eqSolverBuilder = new EqSolverData().withDefinition(definition);
        init(eqSolverBuilder);
    }

    public EquationDataPreparer(String definition, Config config) {
        EqSolverData eqSolverBuilder = new EqSolverData().withDefinition(definition).withConfig(config);
        init(eqSolverBuilder);
    }

    public EquationDataPreparer(EqSolverData eqSolverBuilder) {
        init(eqSolverBuilder);
    }

    @SuppressWarnings("unchecked")
    private void init(EqSolverData eqSolverBuilder) {
        int index = eqSolverBuilder.getEqDefIndex();
        List<String> definitionLines = eqSolverBuilder.getDefinitions();
        Map<String, Supplier<?>> predefinedVariableSuppliers = new HashMap<>(eqSolverBuilder.getPredefinedVariableSuppliers());
        Config config = eqSolverBuilder.getConfig();
        List<LeftRight> definitions = LeftRight.convertToLeftRights(definitionLines);
        LeftRight evaluateEquation = definitions.get(index);

        Collection<DefinedFunction> definedFunctions = new ArrayList<DefinedFunction>();
        for (int i = 0; i < definitionLines.size(); i++) {
            String line = definitionLines.get(i);
            LeftRight leftRight = definitions.get(i);
            if (leftRight.isFuncDef()) {
                try {
                    definedFunctions.add(new DefinedFunction(line));
                } catch (SyntaxException e) {
                    throw new StatementAwareParseException(e, i, 0);
                } catch (SemanticException e) {
                    throw new StatementAwareEvaluateException(e, i);
                }
            }
        }

        String solveVariable = eqSolverBuilder.getSolveVariable();
        if (solveVariable != null) {
            Supplier<?> numberSupplier = predefinedVariableSuppliers.get(solveVariable);
            if (numberSupplier != null) {
                if (numberSupplier instanceof DoubleHolder) {
                    numberHolder = (DoubleHolder) numberSupplier;
                } else {
                    throw new IllegalArgumentException(solveVariable + " must be the NumberHolder but it's " + numberSupplier);
                }
            } else {
                numberHolder = new DoubleHolder();
                predefinedVariableSuppliers.put(solveVariable, numberHolder);
            }
            this.calculateVariableName = solveVariable;
        } else {
            if (predefinedVariableSuppliers.size() == 1) {
                Entry<String, Supplier<?>> entry = predefinedVariableSuppliers.entrySet().iterator().next();
                Supplier<?> numberSupplier = entry.getValue();
                if (numberSupplier != null) {
                    if (numberSupplier instanceof DoubleHolder) {
                        numberHolder = (DoubleHolder) numberSupplier;
                        this.calculateVariableName = entry.getKey();
                    } else {
                        throw new IllegalArgumentException(solveVariable + " must be the NumberHolder but it's " + numberSupplier);
                    }
                } else {
                    numberHolder = new DoubleHolder();
                    predefinedVariableSuppliers.put(entry.getKey(), numberSupplier);
                    this.calculateVariableName = entry.getKey();
                }
            } else {
                if (predefinedVariableSuppliers.size() > 1) {
                    throw new IllegalStateException("Multiple variables passed but unable determine which to calculate");
                } else {
                    Map<String, Object> map = new HashMap<>();
                    addDefFuncs(map, definedFunctions);
                    ExpressionParser leftExpressionParser;
                    ExpressionParser rightExpressionParser;
                    try {
                        leftExpressionParser = new ExpressionParser(evaluateEquation.left, map, config);
                        rightExpressionParser = new ExpressionParser(evaluateEquation.right, map, config);
                    } catch (UndefinedVariableException e) {
                        map.put(e.getVariableName(), numberHolder = new DoubleHolder());
                        leftExpressionParser = new ExpressionParser(evaluateEquation.left, map, config);
                        rightExpressionParser = new ExpressionParser(evaluateEquation.right, map, config);
                    }
                }
            }
        }
        if (!evaluateEquation.isEquation()) {
            throw new RuntimeException("Line is not an equation");
        }

        ExpressionParser leftExpressionParser;
        Map<String, Object> baseMap = ExprVarUtil.toBaseMap(predefinedVariableSuppliers);
        addDefFuncs(baseMap, definedFunctions);
        try {
            leftExpressionParser = new ExpressionParser(evaluateEquation.left, baseMap, config);
        } catch (SyntaxException e) {
            throw new StatementAwareParseException(e, index, 0);
        }

        ExpressionParser rightExpressionParser;
        try {
            rightExpressionParser = new ExpressionParser(evaluateEquation.right, baseMap, config);
        } catch (SyntaxException e) {
            throw new StatementAwareParseException(e, index, evaluateEquation.getRightOffset());
        }

        ArrayList<LeftRight> defWithoutMain = new ArrayList<>(definitions);
        defWithoutMain.set(index, null);
        commentsSetNull(defWithoutMain);
        usedVariables = new HashSet<ExprVarUtil.VariableIndex>();

//        Set<String> variables = new HashSet<>(leftExpressionParser.getUnsetVariables());
//        variables.addAll(rightExpressionParser.getUnsetVariables());
//        Map<String, Supplier<?>> foundVarSuppl = new HashMap<>();
//        Set<String> unfoundVariables = new HashSet<>();
//        for (String variableName : variables) {
//            if (variableName.equals(solveVariable)) {
//                continue;
//            }
//            if (predefinedVariableSuppliers.containsKey(variableName)) {
//                continue;
//            }
//            HashSet<VariableIndex> usedVariablesToFindThis = new HashSet<VariableIndex>();
//            Supplier<?> valueSupplier = ExprVarUtil.findValueSupplier(variableName, defWithoutMain, usedVariablesToFindThis, predefinedVariableSuppliers, config, unfoundVariables);
//            if (valueSupplier != null) {
//                foundVarSuppl.put(variableName, valueSupplier);
//            } else {
//                unfoundVariables.add(variableName);
//            }
//            usedVariables.addAll(usedVariablesToFindThis);
//        }

//        Map<String, Supplier<?>> varSuppliers = new HashMap<>(predefinedVariableSuppliers);
//        varSuppliers.putAll(foundVarSuppl);
//        Map<String, Object> map = ExprVarUtil.toBaseMap(varSuppliers);
//        addDefFuncs(map, definedFunctions);
//        try {
//            leftExpressionParser = new ExpressionParser(evaluateEquation.left, map, config);
//        } catch (SyntaxException e) {
//            throw new StatementAwareParseException(e, index, 0);
//        }
//
//        try {
//            rightExpressionParser = new ExpressionParser(evaluateEquation.right, map, config);
//        } catch (SyntaxException e) {
//            throw new StatementAwareParseException(e, index, evaluateEquation.getRightOffset());
//        }
        if (!SupplierTypeUtil.supplierOfType(leftExpressionParser.getResultSupplier(), Double.class)) {
            throw new NotExpressionException("Left part is not a double expression");
        }
        if (!SupplierTypeUtil.supplierOfType(rightExpressionParser.getResultSupplier(), Double.class)) {
            throw new NotExpressionException("Right part is not a double expression");
        }
        this.leftSupplier = (Supplier<Double>) leftExpressionParser.getResultSupplier();
        this.rightSupplier = (Supplier<Double>) rightExpressionParser.getResultSupplier();
    }

    public static void commentsSetNull(List<LeftRight> definitions) {
        for (int i = 0; i < definitions.size(); i++) {
            LeftRight leftRight = definitions.get(i);
            if (leftRight == null) {
                continue;
            }
            if (leftRight.isCommented()) {
                definitions.set(i, null);
            }
        }
    }

    public Supplier<Double> getLeftSupplier() {
        return leftSupplier;
    }

    public Supplier<Double> getRightSupplier() {
        return rightSupplier;
    }

    /**
     * Get all used variables found on definition lines like 'x: 123'
     * @return All used variables found on definition lines
     */
    public Set<VariableIndex> getUsedVariables() {
        return usedVariables;
    }

    public EquationData getEquationData() {
        return new SimpleEquationData(leftSupplier, rightSupplier, numberHolder, getCalculateVariableName(), usedVariables);
    }

    @SuppressWarnings("unused")
    private static boolean containsVariable(String variableName, Set<VariableIndex> usedVariables) {
        for (VariableIndex variableIndex : usedVariables) {
            if (variableIndex.variableName.equals(variableName)) {
                return true;
            }
        }
        return false;
    }

    public String getCalculateVariableName() {
        return calculateVariableName;
    }

    public static void addDefFuncs(Map<String, Object> map, Collection<DefinedFunction> definedFunctions) {
        for (DefinedFunction definedFunction : definedFunctions) {
            map.put(definedFunction.getSignature(), definedFunction.getExpression());
        }
    }
}
