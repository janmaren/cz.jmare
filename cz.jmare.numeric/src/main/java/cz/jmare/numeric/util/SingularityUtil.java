package cz.jmare.numeric.util;

import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;

public class SingularityUtil {
    public static TwoDoubles getSingularity(Supplier<Double> supplier, DoubleHolder numberHolder, double fromX, double toX, double precision) {
        double middle = (fromX + toX) / 2.0;
        double leftX = fromX;
        double rightX = toX;

        numberHolder.accept(middle);
        double middleValue = supplier.get();
        numberHolder.accept(leftX);
        double leftValue = supplier.get();
        if (Double.isNaN(leftValue)) {
            return null;
        }
        numberHolder.accept(rightX);
        double rightValue = supplier.get();
        if (Double.isNaN(rightValue)) {
            return null;
        }

        if (notsuspectForSingularity(leftValue, middleValue, rightValue)) {
            return null;
        }

        while (Math.abs(leftX - rightX) > precision) {
            if (Double.isNaN(leftValue) || Double.isNaN(rightValue)) {
                return null;
            }
            if (Double.isInfinite(middleValue)) {
                while (Math.abs(leftX - rightX) > precision) {
                    leftX += (middle - leftX) / 2.0;
                    rightX -= (rightX - middle) / 2.0;
                }
                return new TwoDoubles(leftX, rightX);
            }
            double leftMiddle = (leftX + middle) / 2.0;
            numberHolder.accept(leftMiddle);
            double leftMiddleValue = supplier.get();

            if (suspectForSingularity(leftValue, leftMiddleValue, middleValue)) {
                rightX = middle;
                rightValue = middleValue;
                middle = (leftX + rightX) / 2.0;
                numberHolder.accept(middle);
                middleValue = supplier.get();
            } else {
                double rightMiddle = (rightX + middle) / 2.0;
                numberHolder.accept(rightMiddle);
                double rightMiddleValue = supplier.get();
                if (suspectForSingularity(middleValue, rightMiddleValue, rightValue)) {
                    leftX = middle;
                    leftValue = middleValue;
                    middle = (leftX + rightX) / 2.0;
                    numberHolder.accept(middle);
                    middleValue = supplier.get();
                } else {
                    return null;
                }
            }
        }
        if (Double.isNaN(leftValue) || Double.isNaN(rightValue)) {
            return null;
        }
        return new TwoDoubles(leftX, rightX);
    }

    public static boolean suspectForSingularity(double y1, double y2, double y3) {
        return y1 > y2 && y2 < y3 || y1 < y2 && y2 > y3;
    }

    public static boolean notsuspectForSingularity(double y1, double y2, double y3) {
        return y1 > y2 && y2 > y3 || y1 < y2 && y2 < y3;
    }
}
