package cz.jmare.numeric.util;

public class TwoDoubles {
    public double x1;

    public double x2;

    public TwoDoubles() {
    }

    public TwoDoubles(double x1, double x2) {
        super();
        this.x1 = x1;
        this.x2 = x2;
    }

    @Override
    public String toString() {
        return "TwoDoubles [x1=" + x1 + ", x2=" + x2 + "]";
    }
}
