package cz.jmare.numeric.util;

import java.util.function.Function;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.DoubleHolder;

/**
 * Return difference of 'yleft' and 'yright' as 'yleft - yright' for given x.
 * Usable for equations
 */
public class LeftRightDiff implements Function<Double, Double>{
    private Supplier<Double> leftSupplier;
    private Supplier<Double> rightSupplier;
    private DoubleHolder numberHolder;
    private Double lastX;
    private Double lastDiff;

    public LeftRightDiff(Supplier<Double> leftSupplier, Supplier<Double> rightSupplier, DoubleHolder numberHolder) {
        super();
        this.leftSupplier = leftSupplier;
        this.rightSupplier = rightSupplier;
        this.numberHolder = numberHolder;
    }

    /**
     * Return difference of 'yleft' and 'yright' as 'yleft - yright' for given x.
     */
    @Override
    public Double apply(Double x) {
        if (lastX != null && x == lastX) {
            return lastDiff;
        }
        numberHolder.accept(lastX = x);
        return lastDiff = leftSupplier.get() - rightSupplier.get();
    }
}
