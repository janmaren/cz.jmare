package cz.jmare.numeric.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class NumberPrecisionUtil {
    public static int getOrderOfMagnitude(double number) {
        double log10 = Math.log10(number < 0 ? -number : number);
        int order = (int) log10;
        if (order < log10) {
            order++;
        }
        return order;
    }

    public static double getSignificantPrecision(double value, int significantFiguresPrecision) {
        int orderOfMagnitude = getOrderOfMagnitude(value);
        double prec = Math.pow(10, orderOfMagnitude);
        double precision = prec * Math.pow(10, -significantFiguresPrecision);
        return precision;
    }

    public static double roundSignificant(double value, int significantFiguresPrecision) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.round(new MathContext(significantFiguresPrecision));
        return bd.doubleValue();
    }

    public static double roundOrder(double value, int order) {
        if (order > 1) {
            BigDecimal bd = new BigDecimal(value);
            bd = bd.round(new MathContext(order));
            return bd.doubleValue();
        } else {
            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(-order, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
    }

    public static void main(String[] args) {
        int orderOfMagnitude = getOrderOfMagnitude(0.001);
        System.out.println(orderOfMagnitude);

        System.out.println(getSignificantPrecision(5000, 5));
    }

    public static double roundDecimal(double value, Integer decimalPlace) {
        if (decimalPlace <= 0) {
            return value;
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
