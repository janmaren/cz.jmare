package cz.jmare.numeric.util;

public class RunnableManager {
    private Thread thread = new Thread();

    public RunnableManager() {

    }

    public void setRunnable(Runnable runnable) {
        stop();

        thread = new Thread(runnable);

        thread.start();
    }

    public void stop() {
        stopSignal();

        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            throw new IllegalStateException("Unable interrupt");
        }
    }

    public void stopSignal() {
        if (thread.isInterrupted()) {
            return;
        }

        thread.interrupt();
    }
}
