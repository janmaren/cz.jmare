package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;
import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;
import cz.jmare.numeric.range.AutoSolutionRange;
import cz.jmare.numeric.range.FixedSolutionRange;

@Ignore
public class FuncTest {
    @Test
    public void funcEquationTest() {
        List<String> statements = new ArrayList<>();
        statements.add("f(x): sqrt(x)");
        statements.add("f(x) = 10");
        EquationSolver equationRangeSolver = EquationSolver.of(1, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(0, 200));
        List<Double> results = equationRangeSolver.findAll();
        assertEquals(1, results.size());
        assertEquals(100, results.get(0), 0.1);
    }

    @Test
    public void funcAutoEquationTest() {
        List<String> statements = new ArrayList<>();
        statements.add("f(x): sqrt(x)");
        statements.add("f(x) = 10");
        EquationSolver equationRangeSolver = EquationSolver.of(1, statements);
        equationRangeSolver.withRange(new AutoSolutionRange(0, 200));
        List<Double> results = equationRangeSolver.findAll();
        assertEquals(1, results.size());
        assertEquals(100, results.get(0), 0.1);
    }

    @Test
    public void multipleVarsTest() {
        List<String> statements = new ArrayList<>();
        statements.add("f(x): sqrt(x)");
        statements.add("a: 10");
        statements.add("c: 20");
        statements.add("b: 30");
        statements.add("f(x) + a + b + c = 10");
        EquationDataPreparer equationEvaluator = new EquationDataPreparer(4, statements, Map.of("x", new DoubleHolder()), new Config());
        Set<VariableIndex> usedVariables = equationEvaluator.getUsedVariables();
        assertEquals(3, usedVariables.size());
        assertTrue(usedVariables.contains(new VariableIndex("a")));
        assertTrue(usedVariables.contains(new VariableIndex("c")));
        assertTrue(usedVariables.contains(new VariableIndex("b")));
        assertEquals("x", equationEvaluator.getCalculateVariableName());
    }
}
