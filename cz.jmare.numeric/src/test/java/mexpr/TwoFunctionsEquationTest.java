package mexpr;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.arit.ExpressionFunction;
import cz.jmare.mexpr.arit.ExpressionSupplier;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.arit.adapter.FunctionToSupplierAdapter;
import cz.jmare.mexpr.arit.adapter.NumberSupplierAdapter;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.equation.SimpleNumericSolution;

@Ignore
public class TwoFunctionsEquationTest {
    @Test
    public void parabolas() {
        // (x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2
        Function<Double, Double> left = new Function<Double, Double>() {
            @Override
            public Double apply(Double x) {
                return Math.pow(x + 1, 2) + 2;
            }
        };

        Function<Double, Double> right = new Function<Double, Double>() {
            @Override
            public Double apply(Double x) {
                return -(Math.pow(x + 1, 2)) + 2;
            }
        };

        SimpleNumericSolution simpleNumericSolution = SimpleNumericSolution.getInstance(left, right);
        Double result = simpleNumericSolution.solve(-2, 2);
        Assert.assertEquals(-1, result, 0.01);
    }

    @Test
    public void parabolas2() {
        // (x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2
        Function<Double, Double> left = new Function<Double, Double>() {
            @Override
            public Double apply(Double x) {
                return Math.pow(x + 1, 2) + 2;
            }
        };

        DoubleHolder numberHolderRight = new DoubleHolder();
        ExpressionParser expressionParser = new ExpressionParser("-((x + 1) ^ 2) + 2", Map.of("x", numberHolderRight));
        @SuppressWarnings("unchecked")
        Supplier<Double> rightSupplier = (Supplier<Double>) expressionParser.getResultSupplier();
        FunctionToSupplierAdapter leftSupplier = new FunctionToSupplierAdapter(left);
        DoubleHolder numberHolder = new NumberSupplierAdapter(leftSupplier, numberHolderRight);

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        Double result = simpleNumericSolution.solve(-2, 2);
        Assert.assertNotNull(result);
        Assert.assertEquals(-1, result, 0.01);
    }

    @Test
    public void parabolasBySuppliers() {
        // (x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("(x + 1) ^ 2 +2", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("-((x + 1) ^ 2) + 2", numberHolder);

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, rightSupplier.getNumberSupplier());
        Double result = simpleNumericSolution.solve(-2, 2);
        Assert.assertNotNull(result);
        Assert.assertEquals(-1, result, 0.01);
    }

    @Test
    public void quadrBySuppliers() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("0");
        ExpressionSupplier rightSupplier = new ExpressionSupplier("x ^ 2 + 7 * x + 12", numberHolder);

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, rightSupplier.getNumberSupplier());
        Double result = simpleNumericSolution.solve(-5, -2);
        Assert.assertNotNull(result);
        AssertUtil.assertOneOf(Set.of(-3.0, -4.0), result, 0.01);
    }

    @Test
    public void parabolasByFunctions() {
        // (x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionFunction leftFunction = new ExpressionFunction("(x + 1) ^ 2 +2", numberHolder);
        ExpressionFunction rightFunction = new ExpressionFunction("-((x + 1) ^ 2) + 2", numberHolder);

        SimpleNumericSolution simpleNumericSolution = SimpleNumericSolution.getInstance(leftFunction, rightFunction);
        Double result = simpleNumericSolution.solve(-2, 2);
        Assert.assertNotNull(result);
        Assert.assertEquals(-1, result, 0.01);
    }

    @Test
    public void parabolasByFunctionAndSupplier() {
        // (x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionFunction leftFunction = new ExpressionFunction("(x + 1) ^ 2 +2", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("-((x + 1) ^ 2) + 2", numberHolder);

        SimpleNumericSolution simpleNumericSolution = SimpleNumericSolution.getInstance(leftFunction, rightSupplier, numberHolder);
        Double result = simpleNumericSolution.solve(-2, 2);
        Assert.assertNotNull(result);
        Assert.assertEquals(-1, result, 0.01);
    }
}
