package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.equation.SimpleNumericSolution;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

@Ignore
public class NumericSolutionTest {

    @Test
    public void simpleNumericSolution() {
        List<String> statements = new ArrayList<>();
        statements.add("sin(x + pi / 2) = 1");
        EquationDataPreparer equationEvaluator = new EquationDataPreparer(0, statements);
        EquationData equationData = equationEvaluator.getEquationData();
        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double result = simpleNumericSolution.solve(-1, 1);
        assertEquals(0, result, 0.001);
    }

    @Test
    public void simpleNumericSolverSolution() {
        EquationSolver numericXSolver = EquationSolver.of("sin(x + pi / 2) = 1").withRange(-1, 1);
        AssertUtil.assertContains(0, numericXSolver.findAll(), 0.01);
    }

    @Test
    public void tripleEquationSolution() {
        EquationSolver solver = EquationSolver.of("x ^ 3 + 3 * x ^ 2 - 4 = 0").withRange(-5, 5);
        double ar[] = {-2, 1};
        List<Double> results = solver.findAll();
        AssertUtil.assertContains(ar, results);
    }

    @Test
    public void logEquationSolution() {
        EquationSolver solver = EquationSolver.of("(3 - x) * ln(x) = (2-x) * ln(4)").withRange(-10, 10);
        double ar[] = {1.5438407, 6.1830721};
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(ar, findAll);
    }

    @Test
    public void log2EquationSolution() {
        EquationSolver solver = EquationSolver.of("log(10 * 3 ^ x - 3, 3) -1 = 2 * x").withRange(-2, 2);
        double ar[] = {1};
        AssertUtil.assertContains(ar, solver.findAll());
    }

    @Test
    public void noSolutionEquationSolution() {
        EquationSolver numericXSolver = EquationSolver.of("1 / x = 0").withRange(-1000, 1000);
        List<Double> findAll = numericXSolver.findAll();
        assertTrue(findAll.size() == 0);
    }

    @Test
    public void log2EquationSolutionLimit() {
        EquationSolver solver = EquationSolver.of("log(10 * 3 ^ x - 3, 3) -1 = 2 * x").withRange(-1, 1.1);
        double ar[] = {-1, 1};
        AssertUtil.assertContains(ar, solver.findAll());
    }

    @Test
    public void simpleNumericSolverSolutionLimit() {
        EquationSolver solver = EquationSolver.of("sin(x + pi / 2) = 0").withRange(1, 2);
        AssertUtil.assertContains(1.57, solver.findAll(), 0.01);
    }

    @Test
    public void log3() {
        EquationSolver solver = EquationSolver.of("x ^ log(x, 3) = 9*x").withRange(0, 9.00001);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(Set.of(0.35, 9.0), findAll, 0.1);
    }

    @Test
    public void log9() {
        EquationSolver solver = EquationSolver.of("log(9, x-2) =2").withRange(0, 9);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(5, findAll, 0.01);
    }

    @Test
    public void logx2() {
        EquationSolver solver = EquationSolver.of("log(x, 2) - 2 * log(x, 1/2)=9").withRange(0, 50);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(8, findAll, 0.01);
    }

    @Test
    public void exp1() {
        EquationSolver solver = EquationSolver.of("2 ^ (3*x-4) = 8 ^ (2*x+1)").withRange(-50, 50);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(-2.33, findAll, 0.01);
    }

    @Test
    public void exp1b() {
        EquationSolver solver = EquationSolver.of("2 ^ (3*x-4) = 8 ^ (2*x+1)").withRange(-50, 50);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(-2.33, findAll, 0.01);
    }

    @Test
    public void kvadr1() {
        EquationSolver solver = EquationSolver.of("6 / (x -1) + 5 / (x + 1) = 6 / (x - 2)").withRange(-50, 50).withConstantStep(0.1);
        List<Double> findAll = solver.findAll();
        AssertUtil.assertContains(Set.of(0.2, 4.0), findAll, 0.01);
    }

    @Test
    public void kvadrAbs() {
        EquationSolver solver = EquationSolver.of("x ^ 2 + abs(x - 1) = 1").withRange(-50, 50);
        double ar[] = {0, 1};
        AssertUtil.assertContains(ar, solver.findAll());
    }

    @Test
    public void irac() {
        EquationSolver solver = EquationSolver.of("2 * sqrt(x - 1) + sqrt(4 * x - 1) = sqrt(3) + 0.08").withRange(0.998, 1.1);
        List<Double> findAll = solver.findAll();
        assertTrue(findAll.get(0) > 0.99 && findAll.get(0) < 1.002);
    }

    @Test
    public void large() {
        EquationSolver solver = EquationSolver.of("1+x=1234567").withRange(1234565, 1234568);
        List<Double> findAll = solver.findAll();
        assertNotNull(findAll);
        assertEquals(1, findAll.size());
        double value = findAll.get(0);
        Assert.assertTrue(value > 1234565d && value < 1234567d);
    }

    @Test
    public void quadrNearing() {
        EquationSolver solver = EquationSolver.of("x ^ 2 - 6 = -6").withRange(-1, 1);
        List<Double> findAll = solver.findAll();
        assertNotNull(findAll);
        assertEquals(1, findAll.size());
        double value = findAll.get(0);
        assertEquals(0, value, 0.001);
    }

}
