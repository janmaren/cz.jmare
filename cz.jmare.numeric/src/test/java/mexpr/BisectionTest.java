package mexpr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.arit.ExpressionSupplier;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.equation.SimpleNumericSolution;
import cz.jmare.numeric.solution.BisectionAlgorithm;
import cz.jmare.numeric.solution.BisectionNumericAlgorithm;
import cz.jmare.numeric.util.LeftRightDiff;
import cz.jmare.numeric.util.SingularityUtil;
import cz.jmare.numeric.util.TwoDoubles;

@Ignore
public class BisectionTest {
    @Test
    public void bisectionTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / (x ^ 2)");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("100");

        BisectionNumericAlgorithm bisectionNumericAlgorithm = new BisectionNumericAlgorithm(expressionSupplier, expressionRightSupplier, numberHolder, 0.05);
        bisectionNumericAlgorithm.setPrecision(0.0001);
        bisectionNumericAlgorithm.setRoundingOrder(-5);
        Double firstResult = null;
        double x = 0;
        while (x < 1) {
            numberHolder.accept(x);
            double leftValue = expressionSupplier.get();
            double rightValue = expressionRightSupplier.get();
            Double calculateValue = bisectionNumericAlgorithm.calculateValue(x, leftValue, rightValue);
            if (calculateValue != null) {
                firstResult = calculateValue;
                break;
            }
            x += 0.05;
        }

        assertEquals(0.1, firstResult, 0.05);
    }

    @Test
    public void pureBisectionTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("x - 1");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("0");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, 0, 1, 0.001);
        assertEquals(1, value, 0.001);

        value = BisectionAlgorithm.findResult(yDiffProvider, 0, 1 - 0.001, 0.001);
        //value = bisectionAlg.findInInterval(0, 1 - 0.001);
        assertEquals(null, value);

        value = BisectionAlgorithm.findResult(yDiffProvider, 1, 2, 0.001);
    //    value = bisectionAlg.findInInterval(1, 2);
        assertEquals(1, value, 0.001);

        value = BisectionAlgorithm.findResult(yDiffProvider, 1 + 0.001, 2, 0.001);
        //value = bisectionAlg.findInInterval(1 + 0.001, 2);
        assertEquals(null, value);
    }

    @Test
    public void pureBisectionSwitchedTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("x - 1");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("0");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, 0, 1, 0.001);
        assertEquals(1, value, 0.001);

        value = BisectionAlgorithm.findResult(yDiffProvider, 1 - 0.001, 0, 0.001);
        assertEquals(null, value);

        value = BisectionAlgorithm.findResult(yDiffProvider, 2, 1, 0.001);
        assertEquals(1, value, 0.001);

        value = BisectionAlgorithm.findResult(yDiffProvider, 2, 1 + 0.001, 0.001);
        assertEquals(null, value);
    }

    @Test
    public void pureHyperTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("-10000");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, -1, -0.00001, 0.001);
        value = BisectionAlgorithm.findResult(yDiffProvider, -1, -0.00001, 0.001);

        assertEquals(-0.0001, value, 0.001);
    }

    @Test
    public void pureHyperDivergTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("-1000");
        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, -0.00000000001, -1, 0.0001);

        assertEquals(-0.001, value, 0.0001);
    }

    @Test
    public void numericSolverHyperDivergTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("1000");

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(expressionSupplier, expressionRightSupplier, numberHolder);
        simpleNumericSolution.withConstantStep(0.1);
        Double value = simpleNumericSolution.solve(-0.01, 1);
        assertNotNull("not found a value", value);
        assertEquals(0.001, value, 0.0001);
    }

    @Test
    public void numericSolverHyperDiverg2Test() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("-10");

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(expressionSupplier, expressionRightSupplier, numberHolder);
        simpleNumericSolution.withConstantStep(0.1);
        Double value = simpleNumericSolution.solve(-1, 1);
        assertEquals(-0.1, value, 0.0001);
    }

    @Test
    public void numericSolverHyperPowerTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("1 / x ^ 2");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("1000000");

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(expressionSupplier, expressionRightSupplier, numberHolder);
        simpleNumericSolution.withConstantStep(0.0001);
        Double value = simpleNumericSolution.solve(-1, 1);
        assertEquals(-0.001, value, 0.0001);

        Double value2 = simpleNumericSolution.solve(0, 1);
        assertEquals(0.001, value2, 0.0001);
    }

    @Test
    public void findLastDefinedPointTest() {
        ExpressionSupplier leftSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = leftSupplier.getNumberSupplier();

        TwoDoubles singularityThresholds = SingularityUtil.getSingularity(leftSupplier, numberHolder, -0.5, 0.5, 0.00000000001);
        assertNotNull(singularityThresholds);
        assertEquals(-1.8626278019961973E-11, singularityThresholds.x1, 0.0000000001);
        assertEquals(1.734723475976807E-16, singularityThresholds.x2, 0.0000000001);
    }

    @Test
    public void findLastDefinedPointParTest() {
        ExpressionSupplier leftSupplier = new ExpressionSupplier("e ^ (-1 / x ^2)");
        DoubleHolder numberHolder = leftSupplier.getNumberSupplier();

        ExpressionSupplier rightSupplier = new ExpressionSupplier("0.02");

        SimpleNumericSolution simpleNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        simpleNumericSolution.withConstantStep(0.001);
        Double value1 = simpleNumericSolution.solve(-2, 2);
        assertEquals(-0.505591, value1, 0.0001);
        Double value2 = simpleNumericSolution.solve(value1 + 0.001, 2);
        assertEquals(0.505591, value2, 0.0001);
    }

    @Test
    public void noEquationRangeTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("x", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("x+1", numberHolder);
        SimpleNumericSolution adaptiveNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        adaptiveNumericSolution.withConstantStep(0.001);
        Double value1 = adaptiveNumericSolution.solve(-2, 2);
        assertNull(value1);
    }

    @Test
    public void behindRangeTooSmallTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("fc", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("1 / (2 * pi * 300000000)", numberHolder);
        SimpleNumericSolution adaptiveNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        adaptiveNumericSolution.withConstantStep(0.001);
        Double value1 = adaptiveNumericSolution.solve(0, 0.001);
        assertNotNull(value1);
        assertEquals(0, value1, 0.0001);
    }

    @Test
    public void singulBugTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("1/(x + 1115)", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("1", numberHolder);
        SimpleNumericSolution adaptiveNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        adaptiveNumericSolution.withConstantStep(0.1);
        Double value1 = adaptiveNumericSolution.solve(-10000, -1000);
        assertNotNull(value1);
        assertEquals(-1114, value1, 0.0001);
    }

    @Test
    public void singulBug2Test() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("1/(x + 1111110)", numberHolder);
        ExpressionSupplier rightSupplier = new ExpressionSupplier("1", numberHolder);
        SimpleNumericSolution adaptiveNumericSolution = new SimpleNumericSolution(leftSupplier, rightSupplier, numberHolder);
        Double value1 = adaptiveNumericSolution.solve(-1111210, 0);
        assertNotNull(value1);
        assertEquals(-1111109.0, value1, 0.0001);
    }

    @Test
    public void singulDevel() {
        ExpressionSupplier leftSupplier = new ExpressionSupplier("1 / x");
        DoubleHolder numberHolder = leftSupplier.getNumberSupplier();
        TwoDoubles singularity = SingularityUtil.getSingularity(leftSupplier, numberHolder, -200.0, 0.1, 0.00000001);
        assertNotNull(singularity);
        assertEquals(0, singularity.x1, 0.0001);
        assertEquals(0, singularity.x2, 0.0001);
    }

    @Test
    public void noSingulDevel() {
        ExpressionSupplier leftSupplier = new ExpressionSupplier("x ^ 2");
        DoubleHolder numberHolder = leftSupplier.getNumberSupplier();
        TwoDoubles singularity = SingularityUtil.getSingularity(leftSupplier, numberHolder, -0.2, 0.2, 0.00000001);
        assertNull(singularity);
    }

    @Test
    public void pureBisectionBugTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("sqrt(0.9 - x)");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("1");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, 2, -2, 0.000000001);
        assertEquals(-0.1, value, 0.0001);
        value = BisectionAlgorithm.findResult(yDiffProvider, -2, 2, 0.000000001);
        assertEquals(-0.1, value, 0.0001);
    }

    @Test
    public void pureBisectionAbsTest() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("x ^ 2 + abs(x - 1)");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("1");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, -1, 0.5, 0.000000001);
        assertEquals(0, value, 0.0001);
        value = BisectionAlgorithm.findResult(yDiffProvider, 0.5, 1.5, 0.000000001);
        assertEquals(1, value, 0.0001);
    }

    @Test
    public void pureBisectionBug3Test() {
        ExpressionSupplier expressionSupplier = new ExpressionSupplier("2 * sqrt(x - 1) + sqrt(4 * x - 1)");
        DoubleHolder numberHolder = expressionSupplier.getNumberSupplier();

        ExpressionSupplier expressionRightSupplier = new ExpressionSupplier("sqrt(3) + 0.08");

        LeftRightDiff yDiffProvider = new LeftRightDiff(expressionSupplier, expressionRightSupplier, numberHolder);
        Double value = BisectionAlgorithm.findResult(yDiffProvider, 0.998, 1.1, 0.0000000001);
        assertEquals(1, value, 0.01); // less precision
    }

    @Test
    public void notPrecisiousBug3Test() {
        EquationSolver equationSolver = EquationSolver.of("1 / x = 0.01");
        LeftRightDiff leftRightDiff = equationSolver.getLeftRightDiff();
        Double value = BisectionAlgorithm.findResult(leftRightDiff, 99.99000000001442, 100.00000000001442, 1.0E-11);
        assertEquals(100, value, 1.0E-4);
    }

    @Test
    public void notPrecisiousBug4Test() {
        EquationSolver equationSolver = EquationSolver.of("1 / x = 0.001");
        LeftRightDiff leftRightDiff = equationSolver.getLeftRightDiff();
        Double value = BisectionAlgorithm.findResult(leftRightDiff, 999, 1001, 1.0E-4);
        assertEquals(1000, value, 1.0E-4);
    }
}
