package mexpr;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "testlineedge", description = "+(/")
public class TestlineEdgeSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public TestlineEdgeSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        Double x = supplier.get();
        if (x > 1) {
            return 2 * x  - 1;
        }
        return Double.NaN;
    }
}
