package mexpr;

import java.util.function.Supplier;

import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "testline", description = "+_/")
public class TestlineSupplier implements Supplier<Double> {
    private Supplier<Double> supplier;


    public TestlineSupplier(Supplier<Double> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Double get() {
        Double x = supplier.get();
        if (x >= 1) {
            return 2 * x  - 1;
        }
        return 1d;
    }
}
