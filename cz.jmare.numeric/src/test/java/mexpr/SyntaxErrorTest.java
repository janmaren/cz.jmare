package mexpr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.ExpressionSolver;
import cz.jmare.mexpr.util.StatementAwareEvaluateException;
import cz.jmare.mexpr.util.StatementAwareParseException;
import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

public class SyntaxErrorTest {
    @Test
    public void simpleParseErrorTest() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y=35 + z");
        subEquations.add("x=y^2");
        subEquations.add("1.4 == z ");
        subEquations.add("1.5*x+x");
        try {
            @SuppressWarnings("unused")
            ExpressionSolver expressionSolver = new ExpressionSolver(3, subEquations);
        } catch (StatementAwareParseException e) {
            Assert.assertEquals("Not fit statement index", 2, e.getDefinitionIndex());
            Assert.assertEquals("Not fit equals position", 5, e.getOffset());
            Assert.assertEquals("Not fit length", 1, e.getLength());
        }
    }

    @Test
    @Ignore
    public void simpleParseError2Test() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y=35 + z");
        subEquations.add("x=y^2");
        subEquations.add("1.4 = z ");
        subEquations.add("1.5*x+*x");
        try {
            @SuppressWarnings("unused")
            ExpressionSolver expressionSolver = new ExpressionSolver(3, subEquations);
        } catch (StatementAwareParseException e) {
            Assert.assertEquals("Not fit statement index", 3, e.getDefinitionIndex());
            Assert.assertEquals("Not fit equals position", 6, e.getOffset());
            Assert.assertEquals("Not fit length", 1, e.getLength());
        }
    }

    @Test
    @Ignore
    public void simpleParseError3Test() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y=35 + z");
        subEquations.add("  x=y^2a ");
        subEquations.add("1.4 = z ");
        subEquations.add("1.5*x+x");
        try {
            @SuppressWarnings("unused")
            ExpressionSolver expressionSolver = new ExpressionSolver(3, subEquations);
        } catch (StatementAwareEvaluateException e) {
            Assert.assertEquals("Not fit statement index", 3, e.getDefinitionIndex());
        }
    }

    @Test
    @Ignore
    public void simpleParseError4Test() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y=35 + z");
        subEquations.add("  x=y^2*a ");
        subEquations.add("1.4 = z ");
        subEquations.add("1.5*x+xx");
        subEquations.add("10=a");
        boolean exceptionCaught = false;
        try {
            ExpressionSolver expressionSolver = new ExpressionSolver(3, subEquations);
            expressionSolver.getResultDoubleSupplier().get();
        } catch (StatementAwareEvaluateException e) {
            exceptionCaught = true;
        }
        Assert.assertTrue("Exception that not exists 'xx' must be thrown", exceptionCaught);
    }

    @Test
    @Ignore
    public void equationParseError5Test() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y: 35");
        subEquations.add("  x: y^2a ");
        subEquations.add("z: 1.4 ");
        subEquations.add("1.5*x+x = z +x +b");

        try {
            EqSolverData eqSolverData = new EqSolverData().withDefinitions(subEquations).withEqDefIndex(3).withSolveVariable("x").withPredefinedVariableSuppliers(Map.of("z", new DoubleHolder()));
            new EquationDataPreparer(eqSolverData);
        } catch (StatementAwareParseException e) {
            Assert.assertEquals("Not fit statement index", 1, e.getDefinitionIndex());
            Assert.assertTrue("Not fit equals position", e.getMessage().contains("Illegal text a"));
        }
    }

    @Test
    public void equationParseError6Test() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y=35");
        subEquations.add("  x=y^2a ");
        subEquations.add("1.4 = z ");
        subEquations.add("1.5*x+x = z# +x +b");

        try {
            EqSolverData eqSolverData = new EqSolverData().withDefinitions(subEquations).withEqDefIndex(3).withSolveVariable("x").withPredefinedVariableSuppliers(Map.of("z", new DoubleHolder()));
            new EquationDataPreparer(eqSolverData);
        } catch (StatementAwareEvaluateException e) {
            Assert.assertEquals("Not fit statement index", 3, e.getDefinitionIndex());
        }
    }
}
