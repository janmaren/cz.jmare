package mexpr;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.function.Function;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.equation.FilterResultPublisher;
import cz.jmare.numeric.equation.SmartPublisher;
import cz.jmare.numeric.range.RangeParameters;

public class FilterResultPublisherTest {
    @Test
    @Ignore
    public void problemPublishTest() {
        Double[] doub = new Double[1];
        Function<Double, Boolean> resultConsumer = new Function<Double, Boolean>() {
            @Override
            public Boolean apply(Double value) {
                doub[0] = value;
                return true;
            }};
        FilterResultPublisher filterResultPublisher = new FilterResultPublisher(resultConsumer);

        filterResultPublisher.solvingRange(new RangeParameters(0, 1), 0.1, 0.01);

        filterResultPublisher.result(0);
        assertNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(-1, 0), 0.1, 0.01);
        assertNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(1, 10), 0.1, 0.01);
        assertNotNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(-10, -1), 0.1, 0.01);

        filterResultPublisher.finish();
    }

    @Test
    public void smartPublishTest() {
        Double[] doub = new Double[1];
        Function<Double, Boolean> resultConsumer = new Function<Double, Boolean>() {
            @Override
            public Boolean apply(Double value) {
                doub[0] = value;
                return true;
            }};
        SmartPublisher filterResultPublisher = new SmartPublisher(resultConsumer);

        filterResultPublisher.solvingRange(new RangeParameters(0, 1), 0.1, 0.01);

        filterResultPublisher.result(0);
        assertNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(-1, 0), 0.1, 0.01);
        assertNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(1, 10), 0.1, 0.01);
        assertNotNull(doub[0]);
        filterResultPublisher.solvingRange(new RangeParameters(-10, -1), 0.1, 0.01);

        filterResultPublisher.finish();
    }
}
