package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.range.FixedSolutionRange;

@Ignore
public class EvaluateEquationTest3 {
    @Test
    public void problemResistTest() {
        List<String> statements = new ArrayList<>();
        statements.add("R = ro * l / s");
        statements.add("ro : 5");
        statements.add("l : 6");
        statements.add("R : 2");
        statements.add("ro*l = R * s");
        statements.add("ro*l/R=s");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "s");
        equationRangeSolver.withRange(new FixedSolutionRange(0, 20));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(15, results.get(0), 0.001);
    }

    @Test
    public void problemResist2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("ro : 5");
        statements.add("l : 6");
        statements.add("R : 2");
        statements.add("ro*l = R * s");
        statements.add("ro*l/R=s");
        EquationSolver equationRangeSolver = EquationSolver.of(4, statements, "s");
        equationRangeSolver.withRange(new FixedSolutionRange(0, 20));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(15, results.get(0), 0.001);
    }

    @Test
    public void problemResist3Test() {
        List<String> statements = new ArrayList<>();
        statements.add("3.0E-7 = (ro * l / R)");
        statements.add("ro : 1.7E-8");
        statements.add("l : 2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "R");
        equationRangeSolver.withRange(new FixedSolutionRange(0, 20));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(0.1135, results.get(0), 0.001);
    }

    @Test
    public void problemResist3AdaptTest() {
        List<String> statements = new ArrayList<>();
        statements.add("3.0E-7 = (ro * l / R)");
        statements.add("ro : 1.7E-8");
        statements.add("l : 2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "R");
        equationRangeSolver.withRange(new FixedSolutionRange(0, 20));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(0.1135, results.get(0), 0.001);
    }

    @Test
    public void multilineEasestTest() {
        EquationSolver equationSolver = EquationSolver.of("x ^ 2 = 100").withRange(-100, 100);
        List<Double> results = equationSolver.findAll();
        AssertUtil.assertContains(Set.of(-10.0, 10.0), results, 0.1);
    }

    @Test
    public void sinBugTest() {
        EquationSolver equationSolver = EquationSolver.of("sin(x) = 10").withRange(1E+08, 1E+09);
        List<Double> results = equationSolver.findAll();
        assertEquals(0, results.size());
    }

    @Test
    public void indirectBugTest() {
        EquationSolver equationSolver = EquationSolver.of("1 / x = 0.01");//.withRange(99, 101);
        List<Double> results = equationSolver.findAll();
        assertEquals(1, results.size());
    }

    @Test
    public void withVariableTest() {
        List<String> statements = new ArrayList<>();
        statements.add("10 = x + 1");
        statements.add("x: 5 + y");

        EquationSolver equationSolver = EquationSolver.of(0, statements, "y").withRange(-5, 5);
        List<Double> results = equationSolver.findAll();
        assertEquals(1, results.size());
        assertEquals(4, results.get(0), 0.001);
    }

    @Test
    public void withDefinedVariableTest() {
        List<String> statements = new ArrayList<>();
        statements.add("10 = x + 1");
        statements.add("x: 5 + y");
        statements.add("y: 125"); // will be ignored

        EquationSolver equationSolver = EquationSolver.of(new EqSolverData().withDefinitions(statements).withRange(-5, 5).withSolveVariable("y"));
        List<Double> results = equationSolver.findAll();
        assertEquals(1, results.size());
        assertEquals(4, results.get(0), 0.001);
    }
}
