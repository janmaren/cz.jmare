package mexpr.bisect.easy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.arit.ExpressionSupplier;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.equation.alg.TurningPointAlg;

@Ignore
public class TurningPointTest {
    @Test
    public void simplestTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("0");
        ExpressionSupplier rightSupplier = new ExpressionSupplier("x^2 + 1", numberHolder);

        TurningPointAlg turningPointAlg = new TurningPointAlg(leftSupplier, rightSupplier, numberHolder);
        turningPointAlg.setNearTolerance(5);
        IterCalcMetadata calcMetaValue = turningPointAlg.calcMetaValue(-2, 1);
        assertEquals(0, calcMetaValue.value, 0.0001);
    }

    @Test
    public void simplestOppTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("0");
        ExpressionSupplier rightSupplier = new ExpressionSupplier("x^2 + 1", numberHolder);

        TurningPointAlg turningPointAlg = new TurningPointAlg(leftSupplier, rightSupplier, numberHolder);
        turningPointAlg.setNearTolerance(5);
        IterCalcMetadata calcMetaValue = turningPointAlg.calcMetaValue(-1, 2);
        assertEquals(0, calcMetaValue.value, 0.0001);
    }

    @Test
    public void noPointTest() {
        DoubleHolder numberHolder = new DoubleHolder();
        ExpressionSupplier leftSupplier = new ExpressionSupplier("0");
        ExpressionSupplier rightSupplier = new ExpressionSupplier("x^3 + 1", numberHolder);

        TurningPointAlg turningPointAlg = new TurningPointAlg(leftSupplier, rightSupplier, numberHolder);
        IterCalcMetadata calcMetaValue = turningPointAlg.calcMetaValue(-2, 1);
        assertNull(calcMetaValue.value);
    }
}
