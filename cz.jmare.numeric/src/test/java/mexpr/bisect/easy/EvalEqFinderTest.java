package mexpr.bisect.easy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.equation.EqFinders;
import cz.jmare.numeric.equation.alg.BisectEasyAlg;
import cz.jmare.numeric.equation.util.SolveRange;
import mexpr.AssertUtil;

@Ignore
public class EvalEqFinderTest {
    @Test
    public void evaluatorEquation2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("y: 23 + 2");
        statements.add("2 * x ^ 2 + 3 * x - 2 = y");
        List<Double> results = EqFinders.ofDynamic(1, statements, "x");
        double[] expected = {-4.5, 3.0};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void noEquationRangeTest() {
        List<String> statements = new ArrayList<>();
        statements.add("x = x + 1");
        List<Double> results = EqFinders.ofDynamic(0, statements, "x");
        System.out.println(results);
        Assert.assertTrue(results.size() == 0);
    }

    @Test
    public void multiDefTest() {
        List<String> statements = new ArrayList<>();
        statements.add("y = x + 1");
        statements.add("y: 10");
        List<Double> results = EqFinders.ofDynamic(0, statements, "x");
        double[] expected = {9};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeErrorTest() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R : 3000000000");
        statements.add("C : 0.1u");
        List<Double> results = EqFinders.ofDynamic(0, statements, new Config().withDefaultMetricPrefixes(), "fc");
        double[] expected = {5.30516E-4};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeError2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R: 3000000000");
        statements.add("C:  0.1u");
        List<Double> results = EqFinders.ofDynamic(0, statements, new Config().withDefaultMetricPrefixes(), "fc");
        double[] expected = {5.30516E-4};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeTooSmallTest() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R: 300000000000000");
        statements.add("C: 0.1u");
        List<Double> results = EqFinders.ofDynamic(0, statements, new Config().withDefaultMetricPrefixes(), "fc");
        double[] expected = {5.0E-9};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void hyperLimitUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = 1");
        List<Double> results = EqFinders.ofDynamic(0, statements);
        AssertUtil.assertSame(List.of(), results);
    }

    @Test
    public void hyperNegativeLimitUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = -1");
        List<Double> results = EqFinders.ofDynamic(0, statements);
        AssertUtil.assertSame(List.of(), results);
    }

    @Test
    public void zeroTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = 0");
        List<Double> results = EqFinders.ofDynamic(0, statements);
        AssertUtil.assertSame(List.of(0.0), results);
    }

    @Test
    public void negativeNearingUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("e ^ x = 0");
        List<Double> results = EqFinders.ofDynamic(0, statements);
        AssertUtil.assertSame(List.of(-750.0), results);
    }

    @Test
    public void cosNear1Test() {
        List<String> statements = new ArrayList<>();
        statements.add("cos(x)=1");
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinitions(statements).withSolveRange(new SolveRange(6, 7)));
        System.out.println(results);
        Assert.assertTrue(results.size() == 1);
        Assert.assertTrue(results.get(0) > 6.28 && results.get(0) < 6.29);
    }

    @Test
    public void hyperbolDefinedTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("1 / (x +1) = 1e+2"));
        Assert.assertTrue(results.size() == 1);
        assertEquals(-0.99, results.get(0), 0.001);
    }


    @Test
    public void testlineTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("testline(x)=3"));
        Assert.assertTrue(results.size() == 1);
        Assert.assertEquals(2, results.get(0), 0.01);
    }

    @Test
    public void testlineEdge1Test() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("testline(x)=3").withSolveRange(new SolveRange(2, 2.5)));
        Assert.assertTrue(results.size() == 1);
        Assert.assertEquals(2, results.get(0), 0.01);
    }


    @Test
    public void nearingTest() {
        List<String> statements = new ArrayList<>();
        statements.add("sin(x)=1");
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("sin(x)=1").withSolveRange(1.4, 1.7));
        Assert.assertTrue(results.size() == 1);
        Assert.assertTrue(results.get(0) > 1.57 && results.get(0) < 1.58);
    }


    @Test
    public void coincidence2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("1 / x = 0.2");
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("1 / x = 0.2").withSolveRange(0, 6));
        Assert.assertEquals(1, results.size());
        Assert.assertEquals(5, results.get(0), 0.01);
    }

    @Test
    public void tangentLineTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("15 - 2 * x ^ 2 = -4 * x + 17").withSolveRange(0, 6));
        AssertUtil.assertSame(List.of(1.0), results);
    }

    @Test
    public void noResultTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("x^2 + 10 * x + 1000 = 0").withSolveRange(-7, 0));
        Assert.assertTrue(results.size() == 0); // 1 when nearing enabled (result -5), 0 when disabled
    }

    @Test
    public void nearResultTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("x^2 + 10 * x + 1000 = 0").withSolveRange(-7, 0).withNearTolerance(1000.0));
        Assert.assertTrue(results.size() == 1); // 1 when nearing enabled (result -5), 0 when disabled
        assertEquals(-5, results.get(0), 0.001);
    }

    @Test
    public void quadrTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("x^2 + 10 * x + 1000 = 975").withSolveRange(-7, 0));
        AssertUtil.assertSame(List.of(-5.0), results);
    }

    @Test
    public void twoParabolasTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("(x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2").withSolveRange(-2, 0));
        Assert.assertEquals(-1, results.get(0), 0.01);
    }

    @Test
    public void sinBugTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("sin(x) = 10").withSolveRange(1.01E+08, 1.04E+08));
        assertEquals(0, results.size());
    }

    @Test
    @Ignore
    public void indirectBugTest() {
        List<Double> results = EqFinders.ofDynamic(new EqSolverData().withDefinition("1 / x = 0.01"));
        assertEquals(1, results.size());
    }

    @Test
    public void aroundDiscontinuityTest() {
        EquationData equationData = new EqSolverData("1 / x = 0.01").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        Double value = bisectEasyAlg.calculateValue(-4e10, 4e10);
        assertNull(value);
    }
}
