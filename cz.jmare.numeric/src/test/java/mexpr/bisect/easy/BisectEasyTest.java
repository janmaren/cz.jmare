package mexpr.bisect.easy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.EqSolverData;
import cz.jmare.numeric.data.EquationData;
import cz.jmare.numeric.equation.IterCalcMetadata;
import cz.jmare.numeric.equation.ResultType;
import cz.jmare.numeric.equation.alg.BisectEasyAlg;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;

@Ignore
public class BisectEasyTest {
    @Test
    public void simplestTest() {
        EquationData equationData = new EqSolverData("x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        Double value = bisectEasyAlg.calculateValue(-2, 3);
        assertEquals(0, value, 1.0E-4);
    }

    @Test
    public void simplestOppTest() {
        EquationData equationData = new EqSolverData("1 - x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        Double value = bisectEasyAlg.calculateValue(-2, 2);
        assertEquals(0.5, value, 1.0E-4);
    }

    @Test
    public void simplestOppRevertTest() {
        EquationData equationData = new EqSolverData("1 - x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(2, -2);
        assertEquals(0.5, value, 1.0E-4);
    }

    @Test
    public void simplestFromMatchTest() {
        EquationData equationData = new EqSolverData("x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(0, 3);
        assertEquals(0, value, 1.0E-4);
    }

    @Test
    public void simplestRightMatchTest() {
        EquationData equationData = new EqSolverData("x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(-3, 1e-13);
        assertEquals(0, value, 1.0E-4);
    }

    @Test
    public void leftUndefTest() {
        EquationData equationData = new EquationDataPreparer("1 / x = 1").getEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(0, 3);
        assertNull(value);
    }

    @Test
    public void bug1Test() {
        EquationData equationData = new EquationDataPreparer("1 / x = 1").getEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(0.9999999999999989, 1.099999999999999);
        assertEquals(1.0, value, 0.00001);
    }

    @Test
    public void undefTest() {
        EquationData equationData = new EquationDataPreparer("1 / x = 1").getEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        Double value = bisectEasyAlg.calculateValue(-3, 3);
        assertNull(value);
    }

    @Test
    public void rightUndefTest() {
        EquationData equationData = new EquationDataPreparer("1 / x = -1").getEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData.getLeftSupplier(), equationData.getRightSupplier(), equationData.getNumberSupplier());
        IterCalcMetadata calcMetaValue = bisectEasyAlg.calcMetaValue(-3, 2);
        assertNull(calcMetaValue.value);
        assertEquals(ResultType.DISCONTINUITY, calcMetaValue.resultType);
    }

    @Test
    public void rightUndefMultiTest() {
        EquationData equationData = new EqSolverData("1 / x = -1").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        for (int i = 0; i < 200; i++) {
            double from = Math.random();
            double to = Math.random();
            double fromRand = -3 + from;
            double toRand = 1 + to;
            IterCalcMetadata calcMetaValue = bisectEasyAlg.calcMetaValue(fromRand, toRand);
            assertNull(calcMetaValue.value);
            assertEquals("i=" + i, ResultType.DISCONTINUITY, calcMetaValue.resultType);
        }
    }


    @Test
    public void leftUndefMultiTest() {
        EquationData equationData = new EqSolverData("1 / x = 1").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        for (int i = 0; i < 200; i++) {
            double from = Math.random();
            double to = Math.random();
            double fromRand = -1 + from;
            double toRand = 2 + to;
            IterCalcMetadata calcMetaValue = bisectEasyAlg.calcMetaValue(fromRand, toRand);
            assertNull("Not working for interval " + fromRand + " - " + toRand + ", i=" + i, calcMetaValue.value);
            assertEquals("i=" + i, ResultType.DISCONTINUITY, calcMetaValue.resultType);
        }
    }

    @Test
    public void defMultiTest() {
        EquationData equationData = new EqSolverData("1 / x = 1").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        for (int i = 0; i < 200; i++) {
            double from = Math.random();
            double to = Math.random();
            double fromRand = 1e-10 + from;
            double toRand = 2 + to;
            IterCalcMetadata calcMetaValue = bisectEasyAlg.calcMetaValue(fromRand, toRand);
            assertEquals("i=" + i, ResultType.CROSS_VALUE, calcMetaValue.resultType);
        }
    }

    @Test
    public void leftUndefBugTest() {
        EquationData equationData = new EqSolverData("1 / x = 1").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        IterCalcMetadata calcMetaValue = bisectEasyAlg.calcMetaValue(-0.4122954274072461, 2.962509637587025);
        assertNull(calcMetaValue.value);
        assertEquals(ResultType.DISCONTINUITY, calcMetaValue.resultType);
    }

    @Test
    public void outLeftTest() {
        EquationData equationData = new EqSolverData("x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        Double value = bisectEasyAlg.calculateValue(-2, -1);
        assertNull(value);
    }

    @Test
    public void outRightTest() {
        EquationData equationData = new EqSolverData("x * 2 = 0").toEquationData();
        BisectEasyAlg bisectEasyAlg = new BisectEasyAlg(equationData);
        Double value = bisectEasyAlg.calculateValue(1, 2);
        assertNull(value);
    }
}
