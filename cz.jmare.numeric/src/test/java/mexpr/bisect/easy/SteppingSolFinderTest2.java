package mexpr.bisect.easy;

import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.equation.EqFinders;
import mexpr.AssertUtil;

@Ignore
public class SteppingSolFinderTest2 {
    @Test
    public void multilineEasestTest() {
        List<Double> results = EqFinders.ofDynamic("x ^ 2 = 100");
        AssertUtil.assertContains(Set.of(-10.0, 10.0), results, 0.1);
    }

    @Test
    public void bugSinTest() {
        List<Double> results = EqFinders.ofDynamic(1, List.of("f(x): x * x", "sin(f(x)) = 1"));
    }
}
