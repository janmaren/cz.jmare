package mexpr.bisect.easy;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.data.EqExprSuppliers;
import cz.jmare.numeric.equation.IterCalcCollector;
import cz.jmare.numeric.equation.finder.SteppingSolFinder;

@Ignore
public class SteppingSolFinderTest {
    @Test
    public void simplestTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x * 2 = 0"), iterCalcCollector);
        steppingSolFinder.find(-5, 5, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(0.0, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    @Ignore
    public void leftUndefTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("1 / x = 1"), iterCalcCollector);
        steppingSolFinder.find(-5, 5, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(1, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    public void hyperLargerTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("1 / (x + 1000) = 1"), iterCalcCollector);
        steppingSolFinder.find(-1005, -900, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(-999, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    public void turningParabTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("(x+2)^2 + 1 = 1"), iterCalcCollector);
        steppingSolFinder.find(-10 + 1 / 3.0, 10, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(-2, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    public void rightEndingTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x * 2 = 0"), iterCalcCollector);
        steppingSolFinder.find(-1, 0, 0.5);
        assertEquals(1, iterCalcCollector.getResults().size());
    }

    @Test
    public void rightParabTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x ^ 2 = 0"), iterCalcCollector);
        steppingSolFinder.find(-5, 0 + 1e-13, 0.1);
        if (iterCalcCollector.getResults().size() == 0) {
            iterCalcCollector = new IterCalcCollector();
            steppingSolFinder.find(0 + 1e-13, 5, 0.1);
            assertEquals(1, iterCalcCollector.getResults().size());
            assertEquals(0.0, iterCalcCollector.getResults().get(0).value, 0.00001);
        } else {
            assertEquals(1, iterCalcCollector.getResults().size());
            assertEquals(0.0, iterCalcCollector.getResults().get(0).value, 0.00001);
        }
    }

    @Test
    public void rightParabOppTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x ^ 2 = 0"), iterCalcCollector);
        steppingSolFinder.find(1 + 1e-13, -1, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(0.0, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    public void overlapTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x * 0 + 1 = 1"), iterCalcCollector);
        steppingSolFinder.find(-5, 5, 0.5);
        assertEquals(0, iterCalcCollector.getResults().size());
    }

    @Test
    public void overlap2Test() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("1 / x = 0"), iterCalcCollector);
        steppingSolFinder.find(1e12, 1e12 + 1, 0.01);
        assertEquals(0, iterCalcCollector.getResults().size());
    }

    @Test
    public void turningParabNearTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("(x+2)^2 - 16 = 0"), iterCalcCollector);
        steppingSolFinder.find(-10, 10, 1 / 3.0);
        assertEquals(2, iterCalcCollector.getResults().size());
        assertEquals(-6, iterCalcCollector.getResults().get(0).value, 0.00001);
        assertEquals(2, iterCalcCollector.getResults().get(1).value, 0.00001);
    }

    @Test
    public void cosTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("cos(x) = 1"), iterCalcCollector);
        steppingSolFinder.find(-2, 2, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(0, iterCalcCollector.getResults().get(0).value, 0.00001);
    }

    @Test
    public void cosBigTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("cos(x) = 1"), iterCalcCollector);
        steppingSolFinder.find(-5, 5, 0.1);
        assertEquals(1, iterCalcCollector.getResults().size());
        assertEquals(0, iterCalcCollector.getResults().get(0).value, 0.1);
    }

    @Test
    public void xxBugTest() {
        IterCalcCollector iterCalcCollector = new IterCalcCollector();
        SteppingSolFinder steppingSolFinder = new SteppingSolFinder(EqExprSuppliers.of("x * x = 10"), iterCalcCollector);
        steppingSolFinder.find(-2, 2, 0.1);
        assertEquals(0, iterCalcCollector.getResults().size());
    }
}
