package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.range.FixedSolutionRange;

@Ignore
public class EvaluateEquationTest2 {

    @Test
    public void testlineTest() {
        List<String> statements = new ArrayList<>();
        statements.add("testline(x)=3");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(1.9, 2.1));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        Assert.assertEquals(2, results.get(0), 0.01);
    }

    @Test
    public void testlineEdge1Test() {
        List<String> statements = new ArrayList<>();
        statements.add("testline(x)=3");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(2, 2.1));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        Assert.assertEquals(2, results.get(0), 0.01);
    }

    @Test
    public void testlineEdge1NotExistsTest() {
        List<String> statements = new ArrayList<>();
        statements.add("testlineedge(x)=1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(0.9, 1.1));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        assertEquals(1.0, results.get(0), 0.001);
    }

    @Test
    public void nearingTest() {
        List<String> statements = new ArrayList<>();
        statements.add("sin(x)=1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(1.4, 1.7));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        Assert.assertTrue(results.get(0) > 1.57 && results.get(0) < 1.58);
    }

    @Test
    public void coincidenceTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x)=1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(0, 200));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return true;
        });
        Assert.assertEquals(0, results.size());
    }

    @Test
    public void coincidenceNegativeTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x)=-1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-200, 0));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return true;
        });
        Assert.assertEquals(0, results.size());
    }

    @Test
    public void coincidence2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("1 / x = 0.2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(0, 6));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return true;
        });
        Assert.assertEquals(1, results.size());
        Assert.assertEquals(5, results.get(0), 0.01);
    }

    @Test
    public void tangentLineTest() {
        List<String> statements = new ArrayList<>();
        statements.add("15 - 2 * x ^ 2 = -4 * x + 17");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(0, 6));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(1, results.get(0), 0.01);
    }

    @Test
    public void noResultTest() {
        List<String> statements = new ArrayList<>();
        statements.add("x^2 + 10 * x + 1000 = 0");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-7, 0));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertTrue(results.size() == 0);
    }

    @Test
    public void twoParabolasTest() {
        List<String> statements = new ArrayList<>();
        statements.add("(x + 1) ^ 2 +2 = -((x + 1) ^ 2) + 2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-2, 0));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return false;
        });
        Assert.assertEquals(-1, results.get(0), 0.01);
    }
}
