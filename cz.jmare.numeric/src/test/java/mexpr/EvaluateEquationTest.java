package mexpr;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.mexpr.Config;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.util.ExprVarUtil.VariableIndex;
import cz.jmare.numeric.EquationSolver;
import cz.jmare.numeric.evaluate.util.EquationDataPreparer;
import cz.jmare.numeric.range.FixedSolutionRange;

@Ignore
public class EvaluateEquationTest {
    @Test
    public void simpleEquationTest() {
        List<String> subEquations = new ArrayList<>();
        subEquations.add("y:1");
        subEquations.add("z:11");
        subEquations.add("2 * x + y = z");
        Supplier<Double> valueSupplier = new DoubleHolder(5);
        EquationDataPreparer equationEvaluator = new EquationDataPreparer(2, subEquations, Map.of("x", valueSupplier), new Config().withDefaultMetricPrefixes());
        Supplier<Double> leftSupplier = equationEvaluator.getLeftSupplier();
        Supplier<Double> rightSupplier = equationEvaluator.getRightSupplier();
        Double leftValue = leftSupplier.get();
        Double rightValue = rightSupplier.get();
        Assert.assertTrue("Both sides must be same", leftValue.equals(rightValue));
    }

    @Test
    public void evaluatorEquation2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("y: 23 + 2");
        statements.add("2 * x ^ 2 + 3 * x - 2 = y");
        EquationSolver equationVariableSolver = EquationSolver.of(1, statements, "x").withRange(-5, 4);
        List<Double> results = equationVariableSolver.findAll();
        double[] expected = {-4.5, 3.0};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void noEquationRangeTest() {
        List<String> statements = new ArrayList<>();
        statements.add("x = x + 1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(1.0E16, 1.0E17));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {results.add(v); return results.size() < 1000;});
        Assert.assertTrue(results.size() == 0);
    }

    @Test
    public void multiDefTest() {
        List<String> statements = new ArrayList<>();
        statements.add("y = x + 1");
        statements.add("y: 10");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "x");
        Set<VariableIndex> usedVariables = equationRangeSolver.getUsedVariables();
        assertEquals(1, usedVariables.size());
        assertEquals(new VariableIndex("y"), usedVariables.iterator().next());
        assertEquals("x", equationRangeSolver.getCalculateVariable());
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {results.add(v); return results.size() < 1;});
        double[] expected = {9};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeErrorTest() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R : 3000000000");
        statements.add("C : 0.1u");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "fc", new Config().withDefaultMetricPrefixes());
        equationRangeSolver.withRange(new FixedSolutionRange(-2d, 0.001));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {results.add(v); return results.size() < 1;});
        double[] expected = {5.30516E-4};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeError2Test() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R: 3000000000");
        statements.add("C:  0.1u");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "fc", new Config().withDefaultMetricPrefixes());
        equationRangeSolver.withRange(new FixedSolutionRange(0, 0.001));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {results.add(v); return results.size() < 1;});
        double[] expected = {5.30516E-4};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void behindRangeTooSmallTest() {
        List<String> statements = new ArrayList<>();
        statements.add("fc = 1 / (2 * pi * R * C)");
        statements.add("R: 300000000000000");
        statements.add("C: 0.1u");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements, "fc", new Config().withDefaultMetricPrefixes());
        equationRangeSolver.withRange(new FixedSolutionRange(0, 0.001));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {results.add(v); return results.size() < 1;});
        double[] expected = {5.0E-9};
        AssertUtil.assertContains(expected, results);
    }

    @Test
    public void hyperLimitUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = 1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-1000, 980));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 0);
    }

    @Test
    public void hyperNegativeLimitUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = -1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-1000, 980));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 0);
    }

    @Test
    public void zeroTest() {
        List<String> statements = new ArrayList<>();
        statements.add("tanh(x) = 0");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-1000, 0.00001));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        assertEquals(0, results.get(0), 0.000000000001);
    }

    @Test
    public void negativeNearingUndefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("e ^ x = 0");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-1000, 980));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        // Assert.assertTrue(results.size() == 0); // now it returns parallel line
    }

    @Test
    public void cosNear1Test() {
        List<String> statements = new ArrayList<>();
        statements.add("cos(x)=1");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(6, 7));
        List<Double> results = new ArrayList<>();
        equationRangeSolver.findAll((Double v) -> {
            results.add(v);
            return results.size() < 1;
        });
        Assert.assertTrue(results.size() == 1);
        Assert.assertTrue(results.get(0) > 6.28 && results.get(0) < 6.29);
    }

    @Test
    public void hyperbolDefinedTest() {
        List<String> statements = new ArrayList<>();
        statements.add("1 / (x +1) = 1e+2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-2, 0));
        List<Double> results = equationRangeSolver.findAll();
        Assert.assertTrue(results.size() == 1);
        assertEquals(-0.99, results.get(0), 0.001);
    }

    @Test
    public void hyperbolDefinedAdaptTest() {
        List<String> statements = new ArrayList<>();
        statements.add("1 / (x +1) = 1e+2");
        EquationSolver equationRangeSolver = EquationSolver.of(0, statements);
        equationRangeSolver.withRange(new FixedSolutionRange(-2, 0));
        List<Double> results = equationRangeSolver.findAll();
        Assert.assertTrue(results.size() == 1);
        assertEquals(-0.99, results.get(0), 0.001);
    }
}
