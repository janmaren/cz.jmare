package mexpr;

import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class AssertUtil {
    public static void assertOneOf(Collection<Double> expectedOneOf, double realValue, double precision) {
        for (double expec : expectedOneOf) {
            if (expec + precision >= realValue && expec - precision <= realValue) {
                return;
            }
        }
        fail("Not found value " + realValue + " among " + expectedOneOf);
    }

    public static void assertContains(double[] expected, List<Double> results) {
        for (double d : expected) {
            boolean found = false;
            for (Double double1 : results) {
                if (d + 0.01 > double1 && double1 > d - 0.01) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new AssertionError("Not found expected number " + d + " among " + results);
            }
        }
    }
    public static void assertContains(Collection<Double> expected, List<Double> results, double precision) {
        for (double d : expected) {
            boolean found = false;
            for (Double double1 : results) {
                if (d + precision > double1 && double1 > d - precision) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new AssertionError("Not found expected number " + d + " among " + results);
            }
        }
    }

    public static void assertContains(double expected, List<Double> results, double precision) {
        if (results == null) {
            throw new AssertionError("Not found any number");
        }
        for (Double result : results) {
            if (expected + precision > result && result > expected - precision) {
                return;
            }
        }
        throw new AssertionError("Not found expected number " + expected + " among " + results);
    }

    public static void assertSame(Collection<Double> expected, List<Double> results) {
        for (double d : expected) {
            boolean found = false;
            for (int i = 0; i < results.size(); i++) {
                Double double1 = results.get(i);
                if (d + 0.01 > double1 && double1 > d - 0.01) {
                    found = true;
                    results.remove(i);
                    break;
                }
            }
            if (!found) {
                throw new AssertionError("Not found expected number " + d + " among " + results);
            }
        }
        if (results.size() > 0) {
            throw new AssertionError("More results than expected: " + results);
        }
    }
}
