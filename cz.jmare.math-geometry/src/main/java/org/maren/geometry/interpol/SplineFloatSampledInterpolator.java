package org.maren.geometry.interpol;

/**
 * Performs spline interpolation given a set of control points.
 *
 */
public class SplineFloatSampledInterpolator {
    private static final int THRESHOLD_TOLERANCE = 1000;
    private float[] mM;
    private float[] yValues;
    private float sampleRate;

    /**
     * Creates a monotone cubic spline from a given set of control points.
     *
     * The spline is guaranteed to pass through each control point exactly. Moreover, assuming the control points are
     * monotonic (Y is non-decreasing or non-increasing) then the interpolated values will also be monotonic.
     *
     * This function uses the Fritsch-Carlson method for computing the spline parameters.
     * http://en.wikipedia.org/wiki/Monotone_cubic_interpolation
     *
     * @param yValues The Y component of the control points
     * @param sampleRate in Hz. (1 / sampleRate) is an x pitch between two values
     * @throws IllegalArgumentException if the yValues are null, have different lengths or have fewer than 2 values.
     */
    public SplineFloatSampledInterpolator(float[] yValues, float sampleRate) {
        this.yValues = yValues;
        this.sampleRate = sampleRate;

        if (yValues == null || yValues.length < 2) {
            throw new IllegalArgumentException("There must be at least two control points");
        }

        final int n = yValues.length;
        float[] d = new float[n - 1]; // could optimize this out
        float[] m = new float[n];

        // Compute slopes of secant lines between successive points.
        for (int i = 0; i < n - 1; i++) {
            float prevValue = yValues[i];
            float currValue = yValues[i + 1];
            d[i] = (currValue - prevValue) * sampleRate;
        }

        // Initialize the tangents as the average of the secants.
        m[0] = d[0];
        for (int i = 1; i < n - 1; i++) {
            m[i] = (d[i - 1] + d[i]) * 0.5f;
        }
        m[n - 1] = d[n - 2];

        // Update the tangents to preserve monotonicity.
        for (int i = 0; i < n - 1; i++) {
            if (d[i] == 0) { // successive Y values are equal
                m[i] = 0;
                m[i + 1] = 0;
            } else {
                float a = m[i] / d[i];
                float b = m[i + 1] / d[i];
                float h = (float) Math.hypot(a, b);
                if (h > 9) {
                    float t = 3f / h;
                    m[i] = t * a * d[i];
                    m[i + 1] = t * b * d[i];
                }
            }
        }

        this.mM = m;
    }

    /**
     * Interpolates the value of Y = f(X) for given X.
     * @param x The X value which must be in the range of input points (can't extrapolate) which is &lt;0 - (N - 1) / sampleRate&gt; where N is number of input values in array
     * @return The interpolated Y = f(X) value.
     */
    public float interpolate(float x) {
        // Handle the boundary cases.
        if (Float.isNaN(x)) {
            return x;
        }
        if (x < 0) {
            if (-x < (1 / sampleRate / THRESHOLD_TOLERANCE)) {
                return yValues[0];
            }
            throw new IllegalArgumentException("Can't interpolate value < 0");
        }
        if (x > (yValues.length - 1) / sampleRate) {
            if (x - ((yValues.length - 1) / sampleRate) < (1 / sampleRate / THRESHOLD_TOLERANCE)) {
                return yValues[yValues.length - 1];
            }
            throw new IllegalArgumentException("Passed x " + x + " but max. interpolable value is " + (yValues.length - 1 / sampleRate));
        }

        int i = (int) (x * sampleRate);
        if (i > yValues.length - 2) {
            // i = yValues.length - 2;
            return yValues[yValues.length - 1];
        }

        // Perform cubic Hermite spline interpolation.
        float h = 1 / sampleRate;
        float t = (x - i * h) / h;
        return (yValues[i] * (1 + 2 * t) + h * mM[i] * t) * (1 - t) * (1 - t)
                + (yValues[i + 1] * (3 - 2 * t) + h * mM[i + 1] * (t - 1)) * t * t;
    }

    public static void main(String[] args) {
        float[] points = {1f, 1.1f, 1.3f, 5f, 4.5f, 4.4f, -1f};
        SplineFloatSampledInterpolator splineFloatSampledInterpolator = new SplineFloatSampledInterpolator(points, 1.0f);
        float x = 0;
        while (x <= 6) {
            float y = splineFloatSampledInterpolator.interpolate(x);
            System.out.println(x + ":" + y);
            x += 0.2f;
        }

        float y = splineFloatSampledInterpolator.interpolate(6.000001f);
        System.out.println(y);
    }
}
