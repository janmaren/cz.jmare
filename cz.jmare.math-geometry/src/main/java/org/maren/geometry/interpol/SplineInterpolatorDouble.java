package org.maren.geometry.interpol;

import java.util.ArrayList;

/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Take from https://gist.github.com/lecho/7627739
 */

import java.util.List;

import org.maren.geometry.util.GPoint;

/**
 * Performs spline interpolation given a set of control points.
 *
 */
public class SplineInterpolatorDouble {
    private double[] mM;
    private List<GPoint> points;

    /**
     * Creates a monotone cubic spline from a given set of control points.
     *
     * The spline is guaranteed to pass through each control point exactly. Moreover, assuming the control points are
     * monotonic (Y is non-decreasing or non-increasing) then the interpolated values will also be monotonic.
     *
     * This function uses the Fritsch-Carlson method for computing the spline parameters.
     * http://en.wikipedia.org/wiki/Monotone_cubic_interpolation
     *
     * @param x
     *            The X component of the control points, strictly increasing.
     * @param dec
     *            The Y component of the control points
     * @return
     *
     * @throws IllegalArgumentException
     *             if the X or Y arrays are null, have different lengths or have fewer than 2 values.
     */
    public SplineInterpolatorDouble(List<GPoint> points) {
        this.points = points;

        if (points.size() < 2) {
            throw new IllegalArgumentException("There must be at least two control "
                    + "points");
        }

        final int n = points.size();
        double[] d = new double[n - 1]; // could optimize this out
        double[] m = new double[n];

        // Compute slopes of secant lines between successive points.
        for (int i = 0; i < n - 1; i++) {
            GPoint prevPoint = points.get(i);
            GPoint currPoint = points.get(i + 1);
            double h = currPoint.x - prevPoint.x;
            if (h <= 0) {
                throw new IllegalArgumentException("The control points must all "
                        + "have strictly increasing X values.");
            }
            d[i] = (currPoint.y - prevPoint.y) / h;
        }

        // Initialize the tangents as the average of the secants.
        m[0] = d[0];
        for (int i = 1; i < n - 1; i++) {
            m[i] = (d[i - 1] + d[i]) * 0.5;
        }
        m[n - 1] = d[n - 2];

        // Update the tangents to preserve monotonicity.
        for (int i = 0; i < n - 1; i++) {
            if (d[i] == 0) { // successive Y values are equal
                m[i] = 0;
                m[i + 1] = 0;
            } else {
                double a = m[i] / d[i];
                double b = m[i + 1] / d[i];
                double h = (double) Math.hypot(a, b);
                if (h > 9) {
                    double t = 3 / h;
                    m[i] = t * a * d[i];
                    m[i + 1] = t * b * d[i];
                }
            }
        }

        this.mM = m;
    }

    public SplineInterpolatorDouble(float[] xValues, float[] yValues) {
        this(toPoints(xValues, yValues));
    }
    
    private static List<GPoint> toPoints(float[] xValues, float[] yValues) {
        if (xValues == null || yValues == null || xValues.length != yValues.length || xValues.length < 2) {
            throw new IllegalArgumentException(
                    "There must be at least two control " + "points and the arrays must be of equal length.");
        }
        ArrayList<GPoint> points = new ArrayList<>(xValues.length);
        for (int i = 0; i < xValues.length; i++) {
            points.add(new GPoint(xValues[i], yValues[i]));
        }
        return points;
    }

    /**
     * Interpolates the value of Y = f(X) for given X. Clamps X to the domain of the spline.
     *
     * @param x
     *            The X value.
     * @return The interpolated Y = f(X) value.
     */
    public double interpolate(double x) {
        // Handle the boundary cases.
        final int n = points.size();
        if (Double.isNaN(x)) {
            return x;
        }
        if (x <= points.get(0).x) {
            return points.get(0).y;
        }
        if (x >= points.get(n - 1).x) {
            return points.get(n - 1).y;
        }

        // Find the index 'i' of the last point with smaller X.
        // We know this will be within the spline due to the boundary tests.
        int i = 0;
        while (x >= points.get(i + 1).x) {
            i += 1;
            if (x == points.get(i).x) {
                return points.get(i).y;
            }
        }

        // Perform cubic Hermite spline interpolation.
        double h = points.get(i + 1).x - points.get(i).x;
        double t = (x - points.get(i).x) / h;
        return (points.get(i).y * (1 + 2 * t) + h * mM[i] * t) * (1 - t) * (1 - t)
                + (points.get(i + 1).y * (3 - 2 * t) + h * mM[i + 1] * (t - 1)) * t * t;
    }

    public static void main(String[] args) {
        List<GPoint> points = new ArrayList<GPoint>();
        points.add(new GPoint(0, 1));
        points.add(new GPoint(1, 1.1));
        points.add(new GPoint(2, 1.3));
        points.add(new GPoint(3, 5));
        points.add(new GPoint(4, 4.5));
        points.add(new GPoint(5, 4.4));
        points.add(new GPoint(6, -1));
        SplineInterpolatorDouble splineInterpolatorDouble = new SplineInterpolatorDouble(points);
        float x = 0;
        while (x <= 6) {
            double y = splineInterpolatorDouble.interpolate(x);
            System.out.println(x + ":" + y);
            x += 0.2f;
        }
    }
}
