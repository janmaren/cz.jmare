package org.maren.geometry.resample;

import org.maren.geometry.interpol.SplineFloatSampledInterpolator;

public class SampledFloatCalc {
    /**
     * Resample from original sampleRate to new sampleRate supposing frequency of curve is same (but in other sampleRate)
     * @param values
     * @param sampleRate
     * @param newSampleRate
     * @return
     */
    public static float[] resample(float[] values, float sampleRate, float newSampleRate) {
        if (values.length == 1) return copyOfArray(values);
        SplineFloatSampledInterpolator splineFloatSampledInterpolator = new SplineFloatSampledInterpolator(values, sampleRate);
        float endX = (values.length - 1) / sampleRate;
        int number = (int) (endX * newSampleRate) + 1;
        float[] newValues = new float[number];
        int i = 0;
        float x = i / newSampleRate;
        while (i < number) {
            float y = splineFloatSampledInterpolator.interpolate(x);
            newValues[i] = y;
            i++;
            x = i / newSampleRate;
        }
        return newValues;
    }

    private static float[] copyOfArray(float[] values) {
        float[] newValues = new float[values.length];
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i];
        }
        return newValues;
    }
}
