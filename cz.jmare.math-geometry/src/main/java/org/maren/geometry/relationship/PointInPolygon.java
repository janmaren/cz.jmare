package org.maren.geometry.relationship;

public class PointInPolygon {
    /**
     * Return true when coordinate is inside polygon<br>
     * Note the vertexes needn't begin and end in the same point, polygon is closed automatically
     * @param x x coordinate
     * @param y y coordinate
     * @param vertX x vertexes
     * @param vertY y vertexes
     * @return
     */
    public static boolean isPointInPolygon(double x, double y, double[] vertX, double[] vertY) {
        if (vertX.length < 3) {
            throw new IllegalArgumentException("Polygon must have at least 3 points but it has " + vertX.length);
        }
        if (vertX.length != vertY.length) {
            throw new IllegalArgumentException("Number of x and y vertexes must be the same");
        }
        boolean c = false;
        for (int i = 0, j = vertX.length - 1; i < vertX.length; j = i++) {
            if (((vertY[i] > y) != (vertY[j] > y))
                    && (x < (vertX[j] - vertX[i]) * (y - vertY[i]) / (vertY[j] - vertY[i]) + vertX[i])) {
                c = !c;
            }
        }
        return c;
    }
    
    /**
     * Return true when coordinate is inside polygon<br>
     * Note the vertexes needn't begin and end in the same point, polygon is closed automatically
     * @param x x coordinate
     * @param y y coordinate
     * @param vertX x vertexes
     * @param vertY y vertexes
     * @return
     */
    public static boolean isPointInPolygon(int x, int y, int[] vertX, int[] vertY) {
        if (vertX.length < 3) {
            throw new IllegalArgumentException("Polygon must have at least 3 points but it has " + vertX.length);
        }
        if (vertX.length != vertY.length) {
            throw new IllegalArgumentException("Number of x and y vertexes must be the same");
        }
        boolean c = false;
        for (int i = 0, j = vertX.length - 1; i < vertX.length; j = i++) {
            if (((vertY[i] > y) != (vertY[j] > y))
                    && ((double) x < (vertX[j] - vertX[i]) * (double)(y - vertY[i]) / (vertY[j] - vertY[i]) + vertX[i])) {
                c = !c;
            }
        }
        return c;
    }

    public static void main(String[] args) {
        double[] xs = { 0, 10, 10, 0, 2};
        double[] ys = { 0, 0, 10, 10, 2};

        boolean pointInPolygon = isPointInPolygon(1, 1.1, xs, ys);
        System.out.println(pointInPolygon); // false
    }
}
