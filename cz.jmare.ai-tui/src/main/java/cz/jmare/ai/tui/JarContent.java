package cz.jmare.ai.tui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class JarContent {
    public static Set<String> getPropertiesFromJARFile(File jar) {
        Set<String> classes = new HashSet<String>();
        try (JarInputStream jarFile = new JarInputStream(new FileInputStream(jar))) {
            JarEntry jarEntry;
            do {
                jarEntry = jarFile.getNextJarEntry();
                if (jarEntry != null && !jarEntry.isDirectory()) {
                    String className = jarEntry.getName();
                    if (className.startsWith("BOOT-INF/classes/")) {
                        className = className.substring("BOOT-INF/classes/".length());
                        if (classes.contains("/")) {
                            continue;
                        }
                        if (className.endsWith(".properties")) {
                            classes.add(className);
                        }
                    }
                }
            } while (jarEntry != null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return classes;
    }
}
