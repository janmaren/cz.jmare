package cz.jmare.ai.tui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import cz.jmare.ai.LearningState;
import cz.jmare.ai.data.LearningResultInfo;
import cz.jmare.ai.imagepoints.CatImagePointsMapping;
import cz.jmare.ai.imagepoints.CategoryColorMapping;
import cz.jmare.ai.imagepoints.CategoryColorMappingFactory;
import cz.jmare.ai.processor.NeuralRasterParallelResult;
import cz.jmare.ai.resolver.AIDataResolver;
import cz.jmare.exception.ExceptionMessage;
import cz.jmare.file.PathUtil;
import cz.jmare.math.raster.entity.AllAreaCategories;
import cz.jmare.math.raster.entity.AreaPixels;
import cz.jmare.math.raster.entity.RGBEntity;
import cz.jmare.progress.NullLongProgressStatus;

public class ResultAIGenMain {
    private static final List<String> SUPPORTED_IMAGES_SUFFIXES = List.of(".png", ".bmp", ".gif", ".tif", ".tiff", ".jpg", ".jpeg");

    private List<String> dirsOrFiles = List.of(".");
    private String learningFile;

    private boolean verbose;

    private boolean info;

    private Integer alphaCategory;

    private RGBEntity alphaColor;

    private Options getOptions() {
        Options options = new Options();
        options.addOption("f", true, "Properties file holding AI weights, color mapping and ai resolver");
        options.addOption("v", false, "Verbose output");
        options.addOption("lambda", false, "List embedded learning files");
        options.addOption("i", false, "Show info about learning");
        options.addOption("a", true, "Alpha category. For such result category the alpha value will be 0 (transparent)");
        options.addOption("c", true, "Color for alpha category. It has sense only when alpha category is populated and besides transparency also the color will be changed.");
        return options;
    }

    private void printHelp(Options options) {
        HelpFormatter hf = new HelpFormatter();
        hf.setWidth(160);
        String header = String.format("AI Result Gen");
        String usage = String.format("<APP> [<OPTION>][ <DIR_OR_IMAGE>[ <DIR_OR_IMAGE>[ ...]]]%n");
        String footer = String.format("Example:%njava -jar ai-result-gen.jar -f learning.properties%n"
                + "java -jar ai-result-gen.jar -f learning.properties /home/user/john/pictures%n"
                + "java -jar ai-result-gen.jar -lambda%n"
                + "java -jar ai-result-gen.jar -lambda -i%n");
        hf.printHelp(usage, header, options, footer);
    }

    public void init(String[] args) {
        Options options = getOptions();
        if (args.length == 0) {
            printHelp(options);
            System.exit(-1);
        }

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            printHelp(options);
            System.exit(-1);
        }

        if (cmd.hasOption("i")) {
            this.info = true;
        }

        if (cmd.hasOption("-lambda")) {
            listProperties();
            System.exit(0);
        }

        if (cmd.hasOption("f")) {
            learningFile = cmd.getOptionValue("f");
            if (new File(learningFile).isDirectory()) {
                System.out.println(learningFile + " is a directory but expected properties file");
            }
        } else {
            System.out.println("missing -f parameter");
            printHelp(options);
            System.exit(-1);
        }

        if (cmd.hasOption("a")) {
            this.alphaCategory = Integer.valueOf(cmd.getOptionValue("a"));
        }

        if (cmd.hasOption("c")) {
            this.alphaColor = new RGBEntity(cmd.getOptionValue("c"));
        }

        if (cmd.hasOption("v")) {
            this.verbose = true;
        }


        List<String> dirsOrFilesLocal = cmd.getArgList();
        if (dirsOrFilesLocal != null && dirsOrFilesLocal.size() > 0) {
            this.dirsOrFiles = dirsOrFilesLocal;
        }
    }

    private void listProperties() {
        if (getClass().getProtectionDomain().getCodeSource().getLocation().getPath().contains("!")) {
            String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            if (absolutePath.startsWith("file:")) {
                absolutePath = absolutePath.substring("file:".length());
            }
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            absolutePath = absolutePath.replaceAll("%20"," "); // Surely need to do this here
            String jarPath = absolutePath.substring(0, absolutePath.indexOf("!"));
            Set<String> propertiesFromJARFile = JarContent.getPropertiesFromJARFile(new File(jarPath));
            for (String string : propertiesFromJARFile) {
                System.out.println(string);
                if (info) {
                    Properties properties = loadProperties(string);
                    LearningResultInfo learningResultInfo = LearningResultInfo.fromProperties(properties);
                    showInfo(learningResultInfo);
                    System.out.println();
                }
            }
        } else {
            String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            String[] list = new File(absolutePath).list((f, n) -> {return n.endsWith(".properties");});
            for (String string : list) {
                System.out.println(string);
                if (info) {
                    Properties properties = loadProperties(string);
                    LearningResultInfo learningResultInfo = LearningResultInfo.fromProperties(properties);
                    showInfo(learningResultInfo);
                    System.out.println();
                }
            }
        }
    }

    public void run() {
        List<File> images = imagesToProcess();
        if (images.size() == 0 && !info) {
            if (dirsOrFiles.size() == 1) {
                System.out.println("No images for " + new File(dirsOrFiles.get(0)).getAbsoluteFile());
            } else {
                System.out.println("No images for " + dirsOrFiles);
            }
        }
        LearningState learningState = null;

        Properties properties = loadProperties(learningFile);

        CategoryColorMapping catImagePoints = CategoryColorMappingFactory.fromProperties(properties);
        if (catImagePoints instanceof CatImagePointsMapping) {
            CatImagePointsMapping catImagePointsMapping = (CatImagePointsMapping) catImagePoints;
            if (alphaCategory != null) {
                catImagePointsMapping.setAlphaCategory(alphaCategory);
            }
            if (alphaColor != null) {
                catImagePointsMapping.setAlphaColor(alphaColor);
            }
        }
        LearningResultInfo learningResultInfo = LearningResultInfo.fromProperties(properties);
        learningState = learningResultInfo.learningState;
        AIDataResolver aiDataResolver = learningResultInfo.aiDataResolver;
        if (info) {
            showInfo(learningResultInfo);
        }

        for (File imageFile : images) {
            try {
                File resultPath = generateFile(imageFile, learningState, aiDataResolver, catImagePoints, learningResultInfo.alphaCategory);
                System.out.println(getHumanReadableFileName(imageFile.getAbsolutePath()) + " => " + getHumanReadableFileName(resultPath.getName()));
            } catch (IOException e) {
                throw new IllegalStateException("Problems to generate result for file " + imageFile, e);
            }
        }
    }

    private Properties loadProperties(String learningFile) {
        InputStream is = null;
        if (new File(learningFile).isFile()) {
            try {
                is = new FileInputStream(learningFile);
            } catch (FileNotFoundException e) {
                throw new IllegalStateException("Problems to open properties file " + learningFile, e);
            }
        } else {
            if (new File(learningFile).isAbsolute()) {
                throw new IllegalStateException("File " + learningFile + " doesn't exist");
            } else {
                URL url = ResultAIGenMain.class.getResource("/" + learningFile);
                if (url != null) {
                    is = ResultAIGenMain.class.getResourceAsStream("/" + learningFile);
                } else {
                    throw new IllegalStateException("File " + learningFile + " doesn't exist on disk nor embedded in jar");
                }
            }
        }
        try {
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        } catch (IOException e) {
            throw new IllegalStateException("Problems to read properties file " + learningFile, e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    private void showInfo(LearningResultInfo learningResultInfo) {
        if (learningResultInfo.name != null) {
            System.out.println("Name: " + learningResultInfo.name);
        }
        if (learningResultInfo.description != null) {
            System.out.println("Description: " + learningResultInfo.description);
        }
        if (learningResultInfo.aiDataResolver != null) {
            System.out.println("Resolver: " + learningResultInfo.aiDataResolver.getClass().getSimpleName());
        }
        List<Integer> dimsList = Arrays.stream(learningResultInfo.learningState.layers).boxed().collect(Collectors.toList());
        String layersText = dimsList.stream().map(l ->String.valueOf(l)).collect(Collectors.joining(", "));
        System.out.println("In., hid., out. layers dimensions: " + layersText);

        if (learningResultInfo.mse != null) {
            System.out.println("MSE: " + learningResultInfo.mse);
        }

        if (learningResultInfo.epochs != null) {
            System.out.println("Epochs: " + learningResultInfo.epochs);
        }

        if (learningResultInfo.learningDurationMillis != null) {
            System.out.println("Training Time Millis: " + learningResultInfo.learningDurationMillis);
        }
    }

    private String getHumanReadableFileName(String str) {
        if (str.startsWith(".\\") || str.startsWith("./")) {
            str = str.substring(2);
        }
        return str;
    }

    private File generateFile(File imageFile, LearningState learningState, AIDataResolver aiDataResolver, CategoryColorMapping categoryColorMapping, Integer alphaCategory) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(imageFile);

        AllAreaCategories areaCategories = NeuralRasterParallelResult.calculateResult(learningState,
                aiDataResolver, new AreaPixels(bufferedImage), new NullLongProgressStatus());
        BufferedImage resultBufferedImage = categoryColorMapping.categoriesToImage(areaCategories, alphaCategory);

        File file = deriveResultPath(imageFile);
        ImageIO.write(resultBufferedImage, "png", file);
        return file;
    }

    private File deriveResultPath(File imageFile) {
        String name = imageFile.getName();
        String[] nameAndSuffix = PathUtil.getNameAndSuffixOrEmpty(name);
        String resultName = nameAndSuffix[0] + "-result";
        File file = new File(imageFile.getParent(), resultName + ".png");
        return file;
    }

    private List<File> imagesToProcess() {
        List<File> imageFiles = new ArrayList<File>();
        for (String dirOrFile : dirsOrFiles) {
            File file = Paths.get(dirOrFile).toAbsolutePath().normalize().toFile();
            if (file.isFile()) {
                imageFiles.add(file);
            } else {
                File[] listFiles = file.listFiles(f -> {
                    String name = f.getName();
                    String[] nameAndSuffix = PathUtil.getNameAndSuffixOrEmpty(name);
                    if (nameAndSuffix[0].endsWith("-selection")) {
                        return false;
                    }
                    if (nameAndSuffix[0].endsWith("-result")) {
                        return false;
                    }
                    if (!SUPPORTED_IMAGES_SUFFIXES.contains(nameAndSuffix[1].toLowerCase())) {
                        return false;
                    }
                    return true;
                });
                List<File> asList = Arrays.asList(listFiles);
                imageFiles.addAll(asList);
            }
        }
        return imageFiles;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public static void main(String[] args) {
        ResultAIGenMain resultGenMain = new ResultAIGenMain();
        resultGenMain.init(args);
        try {
            resultGenMain.run();
        } catch (Exception e) {
            System.out.println(ExceptionMessage.getCombinedMessage(e));
            if (resultGenMain.isVerbose()) {
                e.printStackTrace();
            }
            System.exit(-1);
        } finally {
            NeuralRasterParallelResult.dispose();
        }
    }
}
