package cz.jmare.math.curve.util;

import org.junit.Before;
import org.junit.Test;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.operation.BasicCurveOperations;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.geometry.entity.Point;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class BasicCurveOperationsTest {
    CurveHolder bigPointsCurve;

    CurveHolder bigSampledCurve;

    @Before
    public void prepare() {
        Point testPoint1 = new Point(5, -2);
        Point testPoint2 = new Point(6, 4);
        Point testPoint3 = new Point(8, 7);
        Point testPoint4 = new Point(7.5, 7);

        try (BigRawCurveBuilder bigPointsCurveBuilder = new BigRawCurveBuilder()) {
            bigPointsCurveBuilder.add(testPoint1);
            bigPointsCurveBuilder.add(testPoint2);
            bigPointsCurveBuilder.add(testPoint3);
            bigPointsCurveBuilder.add(testPoint4);
            bigPointsCurve = bigPointsCurveBuilder.build();
        }

        Point tp1 = new Point(3, -2);
        Point tp2 = new Point(4, 4);
        Point tp3 = new Point(5, 7);
        Point tp4 = new Point(6, 7);

        try (BigSampledCurveBuilder bigPointsCurveBuilder = new BigSampledCurveBuilder(1.0)) {
            bigPointsCurveBuilder.add(tp1);
            bigPointsCurveBuilder.add(tp2);
            bigPointsCurveBuilder.add(tp3);
            bigPointsCurveBuilder.add(tp4);
            bigSampledCurve = bigPointsCurveBuilder.build();
        }

    }

	@Test
    public void fillPointsTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigPointsCurve, 1, 10);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        CurveInfo curveInfo = fillPointsLeftRight.getCurveInfo();
        Assert.assertEquals(1, curveInfo.minX, 0.01);
        Assert.assertEquals(4, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(8, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        cachedCurve.close();
    }

    @Test
    public void fillPointsOneLeftAddTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigPointsCurve, 4, 10);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        CurveInfo curveInfo = fillPointsLeftRight.getCurveInfo();
        Assert.assertEquals(4, curveInfo.minX, 0.01);
        Assert.assertEquals(5, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(8, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        cachedCurve.close();
    }

    @Test
    public void fillPointsOneRightAddTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigPointsCurve, 4, 8);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        CurveInfo curveInfo = fillPointsLeftRight.getCurveInfo();
        Assert.assertEquals(4, curveInfo.minX, 0.01);
        Assert.assertEquals(5, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(7.5, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        Assert.assertEquals(8, cachedCurve.get(cachedCurve.size() - 1).x, 0.01);
        cachedCurve.close();
    }

    @Test
    public void fillSampledTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigSampledCurve, 1, 9);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        CurveInfo curveInfo = fillPointsLeftRight.getCurveInfo();
        Assert.assertEquals(1, curveInfo.minX, 0.01);
        Assert.assertEquals(1, cachedCurve.get(0).x, 0.01);
        Assert.assertEquals(2, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(3, cachedCurve.get(2).x, 0.01);
        Assert.assertEquals(4, cachedCurve.get(3).x, 0.01);
        Assert.assertEquals(5, cachedCurve.get(4).x, 0.01);
        Assert.assertEquals(6, cachedCurve.get(5).x, 0.01);
        Assert.assertEquals(7, cachedCurve.get(6).x, 0.01);
        Assert.assertEquals(8, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        Assert.assertEquals(9, cachedCurve.get(cachedCurve.size() - 1).x, 0.01);
        cachedCurve.close();
    }

    @Test
    public void fillSampledAddOneTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigSampledCurve, 2, 8);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        CurveInfo curveInfo = fillPointsLeftRight.getCurveInfo();
        Assert.assertEquals(2, curveInfo.minX, 0.01);
        Assert.assertEquals(2, cachedCurve.get(0).x, 0.01);
        Assert.assertEquals(3, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(4, cachedCurve.get(2).x, 0.01);
        Assert.assertEquals(5, cachedCurve.get(3).x, 0.01);
        Assert.assertEquals(6, cachedCurve.get(4).x, 0.01);
        Assert.assertEquals(7, cachedCurve.get(5).x, 0.01);
        Assert.assertEquals(7, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        Assert.assertEquals(8, cachedCurve.get(cachedCurve.size() - 1).x, 0.01);
        cachedCurve.close();
    }

    @Test
    public void fillSampledNoFillTest() {
        CurveHolder fillPointsLeftRight = BasicCurveOperations.fillPointsLeftRight(bigSampledCurve, 3, 6);
        CachedCurve cachedCurve = new CachedCurve(fillPointsLeftRight, 5);
        Assert.assertEquals(3, cachedCurve.get(0).x, 0.01);
        Assert.assertEquals(4, cachedCurve.get(1).x, 0.01);
        Assert.assertEquals(5, cachedCurve.get(cachedCurve.size() - 2).x, 0.01);
        Assert.assertEquals(6, cachedCurve.get(cachedCurve.size() - 1).x, 0.01);
        cachedCurve.close();
    }
}
