package cz.jmare.math.curve;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.math.curve.operation.BasicCurveOperations;
import cz.jmare.math.curve.operation.SampleCurveOperations;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.curve.sampled.MemorySampledCurveHolder;
import cz.jmare.math.curve.util.DebugCurve;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;

public class SplitAppendCurveTest {
    @Test
    @Ignore
    public void basic() {
        MemoryRawCurveHolder memoryRawCurveHolder = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4,
                4, 3,
                5, 2,
                6, -0.1,
                7, -2,
                8, -3.9,
                9, -1.5,
                10, 0));
        CurveHolder sampledCurveHolder = SampleCurveOperations.sampleFromBeginningMonoSplineInterpolation(memoryRawCurveHolder, 0.5);
        //System.out.println(DebugCurve.stringRepresentation(sampledCurveHolder));
        List<Point> points = CurveUtil.toPoints(sampledCurveHolder);
        Point point = points.get(0);
        assertEquals(0, point.y, 0.01);

        point = points.get(1);
        assertEquals(0.5, point.y, 0.01);
    }

    @Test
    public void appendRaw() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                2, 8,
                2.5, 9,
                4, 10));

        CurveHolder joinedCurve = BasicCurveOperations.appendRawCurve(rawCurveHolder1, rawCurveHolder2, new NullLongProgressStatus());

        List<Point> points = CurveUtil.toPoints(joinedCurve);
        Point point = points.get(0);
        assertEquals(1, point.x, 0.01);
        assertEquals(0, point.y, 0.01);

        point = points.get(1);
        assertEquals(2, point.x, 0.01);
        assertEquals(1, point.y, 0.01);

        point = points.get(2);
        assertEquals(3, point.x, 0.01);
        assertEquals(6, point.y, 0.01);

        point = points.get(3);
        assertEquals(3.5, point.x, 0.01);
        assertEquals(9, point.y, 0.01);

        point = points.get(4);
        assertEquals(5, point.x, 0.01);
        assertEquals(10, point.y, 0.01);
    }

    @Test
    public void appendSample() {
        ArrayList<Double> yps = new ArrayList<Double>();
        yps.add(0.0);
        yps.add(1.0);
        yps.add(4.0);

        MemorySampledCurveHolder sampledCurveHolder1 = new MemorySampledCurveHolder(yps, 1, 1);

        ArrayList<Double> yps2 = new ArrayList<Double>();
        yps2.add(6.0);
        yps2.add(4.0);
        yps2.add(2.0);
        MemorySampledCurveHolder sampledCurveHolder2 = new MemorySampledCurveHolder(yps2, 1, 5);

        CurveHolder joinedCurve = BasicCurveOperations.appendSampledCurve(sampledCurveHolder1, sampledCurveHolder2, new NullLongProgressStatus());
        System.out.println(DebugCurve.stringRepresentation(joinedCurve));

        List<Point> points = CurveUtil.toPoints(joinedCurve);
        Point point = points.get(0);
        assertEquals(1, point.x, 0.01);
        assertEquals(0, point.y, 0.01);

        point = points.get(1);
        assertEquals(2, point.x, 0.01);
        assertEquals(1, point.y, 0.01);

        point = points.get(2);
        assertEquals(3, point.x, 0.01);
        assertEquals(5, point.y, 0.01);

        point = points.get(3);
        assertEquals(4, point.x, 0.01);
        assertEquals(4, point.y, 0.01);

        point = points.get(4);
        assertEquals(5, point.x, 0.01);
        assertEquals(2, point.y, 0.01);
    }

    @Test
    public void splitRaw() {
        MemoryRawCurveHolder memoryRawCurveHolder = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4,
                4, 8,
                5, 9,
                6, 10));
        List<CurveHolder> splitCurve = BasicCurveOperations.splitCurve(memoryRawCurveHolder, 2.5);
//        System.out.println(DebugCurve.stringRepresentation(splitCurve.get(0)));
//        System.out.println(DebugCurve.stringRepresentation(splitCurve.get(1)));
        List<Point> points0 = CurveUtil.toPoints(splitCurve.get(0));
        List<Point> points1 = CurveUtil.toPoints(splitCurve.get(1));
        assertEquals(3, points0.size());
        assertEquals(2, points0.get(1).x, 0.1);
        assertEquals(2.5, points0.get(2).x, 0.1);

        assertEquals(5, points1.size());
        assertEquals(2.5, points1.get(0).x, 0.1);
        assertEquals(3, points1.get(1).x, 0.1);
    }

    @Test
    public void splitSampled() {
        ArrayList<Double> yps = new ArrayList<Double>();
        yps.add(8.0);
        yps.add(9.0);
        yps.add(10.0);
        yps.add(9.5);
        yps.add(8.5);
        yps.add(7.5);

        MemorySampledCurveHolder curveHolder = new MemorySampledCurveHolder(yps, 1);
        List<CurveHolder> splitCurve = BasicCurveOperations.splitCurve(curveHolder, 2.4);
//        System.out.println(DebugCurve.stringRepresentation(splitCurve.get(0)));
//        System.out.println(DebugCurve.stringRepresentation(splitCurve.get(1)));
        List<Point> points0 = CurveUtil.toPoints(splitCurve.get(0));
        List<Point> points1 = CurveUtil.toPoints(splitCurve.get(1));
        assertEquals(3, points0.size());
        assertEquals(9, points0.get(1).y, 0.1);
        assertEquals(10, points0.get(2).y, 0.1);

        assertEquals(4, points1.size());
        assertEquals(10, points1.get(0).y, 0.1);
        assertEquals(9.5, points1.get(1).y, 0.1);
    }
}
