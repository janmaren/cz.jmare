package cz.jmare.math.curve;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import cz.jmare.math.calculate.filter.HighPassRCWaveFilter;
import cz.jmare.math.calculate.filter.LowPassRCWaveFilter;
import cz.jmare.math.curve.operation.FilterCurveOperations;
import cz.jmare.math.curve.sampled.MemorySampledCurveHolder;
import cz.jmare.progress.NullLongProgressStatus;

public class FilterCurveTest {
    @Test
    @Ignore
    public void lowPass() {
        ArrayList<Double> yps = new ArrayList<Double>();
        double x = 0;
        while (x < 2 * Math.PI) {
            yps.add(Math.sin(2 * Math.PI * x * 100));
            x += 0.001;
        }
        MemorySampledCurveHolder curveHolder = new MemorySampledCurveHolder(yps, 0.001);
        LowPassRCWaveFilter lowPassEasyWaveFilter = new LowPassRCWaveFilter(100, 1);
        CurveHolder resultCurve = FilterCurveOperations.filter(curveHolder, lowPassEasyWaveFilter, new NullLongProgressStatus());
        //System.out.println(String.format("result min " + resultCurve.getCurveInfo().minY + ", " + resultCurve.getCurveInfo().maxY));
        assertEquals(3, 20 * Math.log10(1 / resultCurve.getCurveInfo().maxY), 0.5);
    }

    @Test
    @Ignore
    public void highPass() {
        ArrayList<Double> yps = new ArrayList<Double>();
        double x = 0;
        while (x < 2 * Math.PI) {
            yps.add(Math.sin(2 * Math.PI * x * 100));
            x += 0.001;
        }
        MemorySampledCurveHolder curveHolder = new MemorySampledCurveHolder(yps, 0.001);
        HighPassRCWaveFilter highPassEasyWaveFilter = new HighPassRCWaveFilter(100, 1);
        CurveHolder resultCurve = FilterCurveOperations.filter(curveHolder, highPassEasyWaveFilter, new NullLongProgressStatus());
        //System.out.println(String.format("result min " + resultCurve.getCurveInfo().minY + ", " + resultCurve.getCurveInfo().maxY));
        assertEquals(0.58, resultCurve.getCurveInfo().maxY, 0.1);
    }
}
