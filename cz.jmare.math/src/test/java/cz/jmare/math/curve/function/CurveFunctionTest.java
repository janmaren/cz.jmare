package cz.jmare.math.curve.function;

import org.junit.Assert;
import org.junit.Test;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.interpol.LinearInterpolatedCurve;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;

public class CurveFunctionTest {
    @Test
    public void testMonoSpline() {
        CurveHolder createTestCurveHolder = BigSampledCurveBuilder.createBigSampledCurveHolder(2, 1, 2, 4, 3);
        LinearInterpolatedCurve functionPointsAdapter = new LinearInterpolatedCurve(createTestCurveHolder);
        Double apply = functionPointsAdapter.apply(4.999999d);
        Assert.assertEquals(3.0000005, apply, 0.0000001);
        functionPointsAdapter.close();
    }
}
