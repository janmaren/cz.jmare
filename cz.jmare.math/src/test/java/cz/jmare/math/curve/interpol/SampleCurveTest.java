package cz.jmare.math.curve.interpol;

import org.junit.Before;
import org.junit.Test;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.operation.SampleCurveOperations;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.TolerantDoubleCompare;
import cz.jmare.progress.NullLongProgressStatus;

public class SampleCurveTest {
    CurveHolder curveHolder;

    @Before
    public void prepare() {
        try (BigRawCurveBuilder bigPointsCurveBuilder = new BigRawCurveBuilder()) {
            bigPointsCurveBuilder.add(new Point(1, 2));
            bigPointsCurveBuilder.add(new Point(2, 7));
            bigPointsCurveBuilder.add(new Point(3, 4));
            bigPointsCurveBuilder.add(new Point(4, 2));
            bigPointsCurveBuilder.add(new Point(5, 2));
            bigPointsCurveBuilder.add(new Point(6, 0.1));
            curveHolder = bigPointsCurveBuilder.build();
        }
    }

    @Test
    public void testLinear() {
        try (LinearInterpolatedCurve interpolatedCurve = new LinearInterpolatedCurve(curveHolder)) {
            assertEquals(2, interpolatedCurve.getY(1));
            assertEquals(2.5, interpolatedCurve.getY(1.1));
            assertEquals(7, interpolatedCurve.getY(2));
            assertEquals(4, interpolatedCurve.getY(3));
            assertEquals(3, interpolatedCurve.getY(3.5));
            assertEquals(2.2, interpolatedCurve.getY(3.9));
            assertEquals(2, interpolatedCurve.getY(4));
            assertEquals(2, interpolatedCurve.getY(4.5));
            assertEquals(2, interpolatedCurve.getY(5));
            assertEquals(1.81, interpolatedCurve.getY(5.1));
            assertEquals(0.1, interpolatedCurve.getY(6));
        }

        CurveHolder sampleChannelByMonoSplineInterpolation = SampleCurveOperations.sampleToGridByLinearInterpolation(curveHolder, 0.05, new NullLongProgressStatus());
        Point point;
        try (SequentialCurveReader curveReader = sampleChannelByMonoSplineInterpolation.getCurveReader()) {
            point = curveReader.get(2);
            assertEquals(1.1, point.x);
            assertEquals(2.5, point.y);

            point = curveReader.get(50);
            assertEquals(3.5, point.x);
            assertEquals(3, point.y);

            point = curveReader.get(60);
            assertEquals(4, point.x);
            assertEquals(2, point.y);
        }
    }

    @Test
    public void testSampleWithCut() {
        CurveHolder sampleChannelByMonoSplineInterpolation = SampleCurveOperations.sampleToGridByMonoSplineInterpolation(curveHolder, 1.5, new NullLongProgressStatus());
        Point point;
        try (SequentialCurveReader curveReader = sampleChannelByMonoSplineInterpolation.getCurveReader()) {
            point = curveReader.get(0);
            assertEquals(1.5, point.x);
            assertEquals(5, point.y);

            point = curveReader.get(1);
            assertEquals(3, point.x);
            assertEquals(4, point.y);

            point = curveReader.get(2);
            assertEquals(4.5, point.x);
            assertEquals(2, point.y);

            point = curveReader.get(curveReader.size() - 1);
            assertEquals(6, point.x);
            assertEquals(0.1, point.y);
        }
    }

    @Test
    public void testSampleWithCut2() {
        CurveHolder sampleChannelByMonoSplineInterpolation = SampleCurveOperations.sampleToGridByMonoSplineInterpolation(curveHolder, 1.8, new NullLongProgressStatus());
        Point point;
        try (SequentialCurveReader curveReader = sampleChannelByMonoSplineInterpolation.getCurveReader()) {
            point = curveReader.get(0);
            assertEquals(1.8, point.x);

            point = curveReader.get(1);
            assertEquals(3.6, point.x);

            point = curveReader.get(2);
            assertEquals(5.4, point.x);

            assertEquals(3, curveReader.size());
        }
    }


    public static void assertEquals(double expected, double current) {
        TolerantDoubleCompare tolerantDoubleCompare = new TolerantDoubleCompare(0.01);
        if (!tolerantDoubleCompare.equals(expected, current)) {
            throw new IllegalArgumentException("Expected " + expected + " but real is " + current);
        }
    }
}
