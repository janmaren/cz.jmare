package cz.jmare.math.curve;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import cz.jmare.math.curve.operation.BasicCurveOperations;
import cz.jmare.math.curve.operation.Strategy;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;

public class AritCurveTest {
    @Test
    public void appendRaw() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                2, 8,
                2.5, 9,
                4, 10));

        CurveHolder resultCurve = BasicCurveOperations.aritCurve(rawCurveHolder1, rawCurveHolder2, (y1, y2) -> y1 + y2, null, new NullLongProgressStatus());
        //System.out.println(DebugCurve.stringRepresentation(resultCurve));

        List<Point> points = CurveUtil.toPoints(resultCurve);
        assertEquals(5, points.size());
        Point point = points.get(0);
        assertEquals(1, point.x, 0.01);
        assertEquals(0, point.y, 0.01);

        point = points.get(1);
        assertEquals(2, point.x, 0.01);
        assertEquals(9, point.y, 0.01);

        point = points.get(2);
        assertEquals(2.5, point.x, 0.01);
        assertEquals(11.37, point.y, 0.01);

        point = points.get(3);
        assertEquals(3, point.x, 0.01);
        assertEquals(13.48, point.y, 0.01);

        point = points.get(4);
        assertEquals(4, point.x, 0.01);
        assertEquals(10, point.y, 0.01);
    }

    @Test
    public void appendExpungeRaw() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                2, 8,
                2.5, 9,
                4, 10));

        CurveHolder resultCurve = BasicCurveOperations.aritCurve(rawCurveHolder1, rawCurveHolder2, (y1, y2) -> y1 + y2, Strategy.MISSING_POINT_EXPUNGE, new NullLongProgressStatus());

        List<Point> points = CurveUtil.toPoints(resultCurve);
        assertEquals(3, points.size());
        Point point = points.get(0);
        assertEquals(2, point.x, 0.01);
        assertEquals(9, point.y, 0.01);

        point = points.get(1);
        assertEquals(2.5, point.x, 0.01);
        assertEquals(11.37, point.y, 0.01);

        point = points.get(2);
        assertEquals(3, point.x, 0.01);
        assertEquals(13.48, point.y, 0.01);
    }

    @Test
    public void appendExpunge2Raw() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                2, 9,
                3, 6));

        CurveHolder resultCurve = BasicCurveOperations.aritCurve(rawCurveHolder1, rawCurveHolder2, (y1, y2) -> y1 + y2, Strategy.MISSING_POINT_EXPUNGE, new NullLongProgressStatus());

        List<Point> points = CurveUtil.toPoints(resultCurve);
        assertEquals(2, points.size());
        Point point = points.get(0);
        assertEquals(2, point.x, 0.01);
        assertEquals(10, point.y, 0.01);

        point = points.get(1);
        assertEquals(3, point.x, 0.01);
        assertEquals(10, point.y, 0.01);
    }

    @Test
    public void appendExpunge3Raw() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                1, 8,
                2, 9));

        CurveHolder resultCurve = BasicCurveOperations.aritCurve(rawCurveHolder1, rawCurveHolder2, (y1, y2) -> y1 + y2, Strategy.MISSING_POINT_EXPUNGE, new NullLongProgressStatus());

        List<Point> points = CurveUtil.toPoints(resultCurve);
        assertEquals(2, points.size());
        Point point = points.get(0);
        assertEquals(1, point.x, 0.01);
        assertEquals(8, point.y, 0.01);

        point = points.get(1);
        assertEquals(2, point.x, 0.01);
        assertEquals(10, point.y, 0.01);
    }

    @Test
    public void appendRawMisses() {
        MemoryRawCurveHolder rawCurveHolder1 = new MemoryRawCurveHolder(Point.ofList(
                1, 0,
                2, 1,
                3, 4));
        MemoryRawCurveHolder rawCurveHolder2 = new MemoryRawCurveHolder(Point.ofList(
                11, 8,
                12, 9,
                13, 10));

        CurveHolder resultCurve = BasicCurveOperations.aritCurve(rawCurveHolder1, rawCurveHolder2, (y1, y2) -> y1 + y2, null, new NullLongProgressStatus());

        List<Point> points = CurveUtil.toPoints(resultCurve);
        assertEquals(6, points.size());
        Point point = points.get(0);
        assertEquals(1, point.x, 0.01);
        assertEquals(0, point.y, 0.01);

        point = points.get(1);
        assertEquals(2, point.x, 0.01);
        assertEquals(1, point.y, 0.01);

        point = points.get(2);
        assertEquals(3, point.x, 0.01);
        assertEquals(4, point.y, 0.01);

        point = points.get(3);
        assertEquals(11, point.x, 0.01);
        assertEquals(8, point.y, 0.01);

        point = points.get(4);
        assertEquals(12, point.x, 0.01);
        assertEquals(9, point.y, 0.01);

        point = points.get(5);
        assertEquals(13, point.x, 0.01);
        assertEquals(10, point.y, 0.01);
    }

}
