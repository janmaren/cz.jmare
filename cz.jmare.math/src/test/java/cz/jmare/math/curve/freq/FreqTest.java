package cz.jmare.math.curve.freq;

import org.junit.Test;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.curve.util.traverser.FrequencyCalculator;
import cz.jmare.math.curve.util.traverser.TraverseOperations;
import junit.framework.Assert;

public class FreqTest {
    @SuppressWarnings("deprecation")
	@Test
    public void frequencyCalculator() {
        CurveHolder curveHolder = BigSampledCurveBuilder.createBigSampledCurveHolder(1, 0,
                0, 1, 4, 5, 4, 1, -0.5, 0.5, -1, -4, -5, -4, -1,
                0, 1, 4, 5, 4, 1);
        FrequencyCalculator frequencyCalculator = new FrequencyCalculator();
        TraverseOperations.crossHorizontalLineTraverser(curveHolder, frequencyCalculator, 0);
        double frequency = frequencyCalculator.getFrequency();
        Assert.assertEquals(1.0 / 13.0, frequency, 0.1);
    }
}
