package cz.jmare.math.curve;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cz.jmare.math.curve.raw.BigPointsCurveReader;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.curve.raw.BigRawCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.TolerantDoubleCompare;
import cz.jmare.progress.NullLongProgressStatus;

public class BigPointsCurveHolderTest {
    @Test
    public void basic() throws IOException {
        Point testPoint1 = new Point(1, -2);
        Point testPoint2 = new Point(2, 4);
        Point testPoint3 = new Point(3, 7);

        CurveHolder bigSequentialCurve;
        try (BigRawCurveBuilder bigPointsCurveBuilder = new BigRawCurveBuilder()) {
            bigPointsCurveBuilder.add(testPoint1);
            bigPointsCurveBuilder.add(testPoint2);
            bigPointsCurveBuilder.add(testPoint3);
            bigSequentialCurve = bigPointsCurveBuilder.build();
        }

        try (SequentialCurveReader curveReader = bigSequentialCurve.getCurveReader()) {
            Assert.assertEquals(3, curveReader.size());
            assertEquals(testPoint1, curveReader.get(0));
            assertEquals(testPoint2, curveReader.get(1));
            assertEquals(testPoint3, curveReader.get(2));
        }

        RandomAccessCurve randomAccessCurve = CurveUtil.toMemoryOptimalRandomAccessCurve(bigSequentialCurve, new NullLongProgressStatus());
        Assert.assertEquals(3, randomAccessCurve.size());
        assertEquals(testPoint1, randomAccessCurve.get(0));
        assertEquals(testPoint2, randomAccessCurve.get(1));
        assertEquals(testPoint3, randomAccessCurve.get(2));


        File createTempFile = File.createTempFile("test", ".dat");
        createTempFile.deleteOnExit();
        String filepathStr = createTempFile.toString();
        CurveUtil.saveAsCurve(bigSequentialCurve, createTempFile);
        try (BigPointsCurveReader bigPointsCurveReader = new BigPointsCurveReader(filepathStr)) {
            Assert.assertEquals(3, bigPointsCurveReader.size());
            assertEquals(testPoint1, bigPointsCurveReader.get(0));
            assertEquals(testPoint2, bigPointsCurveReader.get(1));
            assertEquals(testPoint3, bigPointsCurveReader.get(2));
        }

        BigRawCurveHolder bigPointsCurve = new BigRawCurveHolder(filepathStr);
        try (SequentialCurveReader curveReader = bigPointsCurve.getCurveReader()) {
            Assert.assertEquals(3, curveReader.size());
            assertEquals(testPoint1, curveReader.get(0));
            assertEquals(testPoint2, curveReader.get(1));
            assertEquals(testPoint3, curveReader.get(2));
        }

        Assert.assertEquals(1.0, bigSequentialCurve.getCurveInfo().minX, 0.1);
        Assert.assertEquals(3.0, bigSequentialCurve.getCurveInfo().maxX, 0.1);
        Assert.assertEquals(-2.0, bigSequentialCurve.getCurveInfo().minY, 0.1);
        Assert.assertEquals(7.0, bigSequentialCurve.getCurveInfo().maxY, 0.1);
    }

    public static void assertEquals(Point point1, Point point2) {
        TolerantDoubleCompare tolerantDoubleCompare = new TolerantDoubleCompare(0.01);
        if (!tolerantDoubleCompare.equals(point1.x, point2.x)) {
            throw new IllegalArgumentException("Expected x " + point1.x + " but real is " + point2.x);
        }
        if (!tolerantDoubleCompare.equals(point1.y, point2.y)) {
            throw new IllegalArgumentException("Expected y " + point1.y + " but real is " + point2.y);
        }
    }
}
