package cz.jmare.math.epsilon;

import org.junit.Ignore;
import org.junit.Test;

public class EpsilonTest {
    @Test
    @Ignore
    public void testSmallEpsilon() {
        double xUnits = 0.001;
        double freq = 44100.0;
        double interval = 1.0 / (freq * xUnits);

        double epsilon = 10e-14;

        for (long i = 1; i < Long.MAX_VALUE; i++) {
            double num1 = (i - 1) * interval;
            double num2 = i * interval;
            double diff = Math.abs((num2 - num1) - interval);
            if (diff > epsilon) {
                epsilon = diff;
                System.out.println(i + ": New epsilon for value " + num2 + " is " + epsilon);
            }
        }
        System.out.println("Result epsilon: " + epsilon);
    }
}
