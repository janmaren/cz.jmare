package cz.jmare.math.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cz.jmare.math.calculate.FilteredGain;
import cz.jmare.math.calculate.filter.PeakBiquadraticWaveFilter;

public class FilterTest {
    @Test
    public void testFilter1() {
        int frequencyCutoff = 1000;
        int frequencySine = 1000;
        double gainDB = 3.0;
        int pointsPerSine = 100;
        int sampleRate = pointsPerSine * frequencySine;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(frequencyCutoff, sampleRate, gainDB);
        double filteredGainForFrequency = FilteredGain.filteredGainForFrequency(peakBiquadraticWaveFilter, sampleRate, frequencySine);
        assertEquals(1.41, filteredGainForFrequency, 0.1);
    }

    @Test
    public void testFilter2() {
        int frequencyCutoff = 1000;
        int frequencySine = 1000;
        double gainDB = -3.0;
        int pointsPerSine = 1000;
        int sampleRate = pointsPerSine * frequencySine;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(frequencyCutoff, sampleRate, gainDB);
        double filteredGainForFrequency = FilteredGain.filteredGainForFrequency(peakBiquadraticWaveFilter, sampleRate, frequencySine);
        assertEquals(0.707, filteredGainForFrequency, 0.1);
    }

    @Test
    public void testFilter3() {
        int frequencyCutoff = 1000;
        int frequencySine = 1000;
        double gainDB = -6.0;
        int pointsPerSine = 1000;
        int sampleRate = pointsPerSine * frequencySine;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(frequencyCutoff, sampleRate, gainDB);
        double filteredGainForFrequency = FilteredGain.filteredGainForFrequency(peakBiquadraticWaveFilter, sampleRate, frequencySine);
        assertEquals(0.5, filteredGainForFrequency, 0.1);
    }

    @Test
    public void testFilter4() {
        int frequencyCutoff = 5000;
        int frequencySine = 70;
        double gainDB = 6;
        double q = 2.1;
        int pointsPerSine = 1000;
        int sampleRate = pointsPerSine * frequencySine;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(frequencyCutoff, sampleRate, q, gainDB);
        double filteredGainForFrequency = FilteredGain.filteredGainForFrequency(peakBiquadraticWaveFilter, sampleRate, frequencySine);
        assertEquals(1, filteredGainForFrequency, 0.1);
    }

    public void testFilterX() {
        int divisions = 50000;
        double f = 1000;
        double x = 0;
        double maxx = 1.0 / f;
        double samplePeriod = maxx / divisions;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(1000, 1 / samplePeriod, -6);
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < 5 * divisions; i++) {
            x = i * samplePeriod;
            double sin = Math.sin(x * 2 * Math.PI * f);
            double nextFilteredY = peakBiquadraticWaveFilter.nextFilteredY(sin);
            //System.out.println(x + ":" + sin);
            if (i == divisions * 0.25) {
                System.out.println(x + ":" + sin + ":" + nextFilteredY);
            }
            if (i == divisions * 0.75) {
                System.out.println(x + ":" + sin + ":" + nextFilteredY);
            }
            if (nextFilteredY > max) {
                max = nextFilteredY;
            }
            if (nextFilteredY < min) {
                min = nextFilteredY;
            }
        }
        System.out.println("Min: " + min + ", max: " + max);
    }
}
