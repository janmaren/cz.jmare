package cz.jmare.math;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import cz.jmare.math.geometry.entity.Point;

public class ChangePointsUtil {
    public static List<Point> movePoints(List<Point> origPoints, double deltaX, double deltaY) {
        List<Point> result = new ArrayList<Point>();
        for (Point point : origPoints) {
            result.add(new Point(point.x + deltaX, point.y + deltaY));
        }
        return result;
    }

    public static List<Point> multiplyPoints(List<Point> origPoints, double multiplyX, double multiplyY) {
        List<Point> result = new ArrayList<Point>();
        for (Point point : origPoints) {
            result.add(new Point(point.x * multiplyX, point.y * multiplyY));
        }
        return result;
    }

    public static List<Point> multiplyPointsY(List<Point> origPoints, double multiplyY) {
        List<Point> result = new ArrayList<Point>();
        for (Point point : origPoints) {
            result.add(new Point(point.x, point.y * multiplyY));
        }
        return result;
    }

    public static List<Point> applyFunctionPoints(List<Point> origPoints, Function<Double, Double> function) {
        List<Point> result = new ArrayList<Point>();
        for (Point point : origPoints) {
            Double yChanged = function.apply(point.y);
            result.add(new Point(point.x, yChanged));
        }
        return result;
    }
}
