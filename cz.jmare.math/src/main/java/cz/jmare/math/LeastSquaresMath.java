package cz.jmare.math;

import java.util.Collection;

import cz.jmare.math.entity.TwoDoubles;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;

public class LeastSquaresMath {

    public static StraightLine solveLeastSquares(Collection<Point> points) {
        TwoDoubles leastSquaresKQ = getLeastSquaresKQ(points);
        return new StraightLine(leastSquaresKQ.x1, leastSquaresKQ.x2);
    }

    public static TwoDoubles getLeastSquaresKQ(Collection<Point> points) {
        double sumXi = 0;
        double sumYi = 0;
        double sumXiSquare = 0;
        double sumXiYi = 0;

        for (Point point : points) {
            sumXi += point.x;
            sumYi += point.y;
            sumXiSquare += point.x * point.x;
            sumXiYi += point.x * point.y;
        }

        // sumXiSquare * k +    sumXi * q          = sumXiYi
        //       sumXi * k +    points.size() * q  = sumYi

        TwoDoubles kxPlusQ = solveEquationsSetTwoUnknowns(sumXiSquare, sumXi, sumXiYi, sumXi, points.size(), sumYi);
        return kxPlusQ;
    }

    public static TwoDoubles solveEquationsSetTwoUnknowns(double a11, double a12, double b1, double a21, double a22, double b2) {
        double x2 = (a11 * b2 - a21 * b1) / (a11 * a22 - a21 * a12);
        double x1 = (b1 - (a12 * x2)) / a11;
        return new TwoDoubles(x1, x2);
    }
}
