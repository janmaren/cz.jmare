package cz.jmare.math.geometry.relation;

import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;

public class IntersectionUtil {

    public static Point straightLinesIntersection(StraightLine line1, StraightLine line2) {
        double j = line2.a;
        double k = line2.b;
        double l = line2.c;

        double denominator = line1.b * j - line1.a * k;
        if (denominator == 0.0f) {
            return null;
        }
        double y = (line1.a * l - line1.c * j) / denominator;
        double x = (line1.c * k - line1.b * l) / denominator;

        return new Point(x, y);
    }

    /**
     * Find intersection or null when intersection is not on line segment or no intersection exists (parallel).
     * Intersection on nodes of straigth line is also matching
     * @param lineSegment
     * @param line
     * @return
     */
    public static Point lineSegmentIntersectLineStrict(LineSegmentDouble lineSegment, StraightLine line) {
        Point intersection = IntersectionUtil.straightLinesIntersection(new StraightLine(lineSegment), line);
        if (intersection == null) {
            // parallel
            return null;
        }

        if (intersection.x > lineSegment.x1 && intersection.x < lineSegment.x2 ||
                intersection.x > lineSegment.x2 && intersection.x < lineSegment.x1 ||
                intersection.y > lineSegment.y1 && intersection.y < lineSegment.y2 ||
                intersection.y > lineSegment.y2 && intersection.y < lineSegment.y1) {
            return intersection;
        }

        return null;
    }

    /**
     * Find intersection or null when intersection is not on line segment or no intersection exists (parallel) or is on one of nodes of line segment
     * @param lineSegment
     * @param line
     * @return
     */
    public static Point lineSegmentIntersectLineBenevolent(LineSegmentDouble lineSegment, StraightLine line) {
        Point intersection = IntersectionUtil.straightLinesIntersection(new StraightLine(lineSegment), line);
        if (intersection == null) {
            // parallel
            return null;
        }

        if (intersection.x >= lineSegment.x1 && intersection.x <= lineSegment.x2 ||
                intersection.x >= lineSegment.x2 && intersection.x <= lineSegment.x1 ||
                intersection.y >= lineSegment.y1 && intersection.y <= lineSegment.y2 ||
                intersection.y >= lineSegment.y2 && intersection.y <= lineSegment.y1) {
            return intersection;
        }

        return null;
    }

    /**
     * Find intersection or null when intersection is not on line segment or no intersection exists (parallel).
     * Intersection on node 1 of straigth line is also matching
     * @param lineSegment
     * @param line
     * @return
     */
    public static Point lineSegmentIntersectLineBenevolent1(LineSegmentDouble lineSegment, StraightLine line) {
        Point intersection = IntersectionUtil.straightLinesIntersection(new StraightLine(lineSegment), line);
        if (intersection == null) {
            // parallel
            return null;
        }

        if (intersection.x >= lineSegment.x1 && intersection.x < lineSegment.x2 ||
                intersection.x > lineSegment.x2 && intersection.x <= lineSegment.x1 ||
                intersection.y >= lineSegment.y1 && intersection.y < lineSegment.y2 ||
                intersection.y > lineSegment.y2 && intersection.y <= lineSegment.y1) {
            return intersection;
        }

        return null;
    }

    /**
     * Find intersection or null when intersection is not on line segment or no intersection exists (parallel).
     * Intersection on node 2 of straigth line is also matching
     * @param lineSegment
     * @param line
     * @return
     */
    public static Point lineSegmentIntersectLineBenevolent2(LineSegmentDouble lineSegment, StraightLine line) {
        Point intersection = IntersectionUtil.straightLinesIntersection(new StraightLine(lineSegment), line);
        if (intersection == null) {
            // parallel
            return null;
        }

        if (intersection.x > lineSegment.x1 && intersection.x <= lineSegment.x2 ||
                intersection.x >= lineSegment.x2 && intersection.x < lineSegment.x1 ||
                intersection.y > lineSegment.y1 && intersection.y <= lineSegment.y2 ||
                intersection.y >= lineSegment.y2 && intersection.y < lineSegment.y1) {
            return intersection;
        }

        return null;
    }

    public static void main(String[] args) {
        Point intersection = lineSegmentIntersectLineStrict(new LineSegmentDouble(-3, 1, -3, -1), StraightLine.createXParallel(0));
        System.out.println(intersection);

        intersection = lineSegmentIntersectLineBenevolent(new LineSegmentDouble(-3, -1, 1, 4), StraightLine.createYParallel(1));
        System.out.println(intersection);

    }
}
