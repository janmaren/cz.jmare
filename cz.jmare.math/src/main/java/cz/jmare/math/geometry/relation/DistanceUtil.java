package cz.jmare.math.geometry.relation;

import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;

public class DistanceUtil {
    /**
     * Return distance from point to line segment or Double.MAX_VALUE when point is not parallel with line segment
     * @param pointX
     * @param pointY
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static double pointToLineSegmentDistance(double pointX, double pointY, double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double dist = ((dx * (pointX - x1)) + (dy * (pointY - y1))) / ((dx * dx) + (dy * dy));
        double crossX = x1 + dx * dist;
        double crossY = y1 + dy * dist;

        if (crossX <= Math.max(x1, x2) && crossX >= Math.min(x1, x2) && crossY <= Math.max(y1, y2) && crossY >= Math.min(y1, y2)) {
            return Math.sqrt((pointX - crossX) * (pointX - crossX) + (pointY - crossY) * (pointY - crossY));
        }

        // cross point not between segment points

        return Math.min(pointToPointDistance(pointX, pointY, x1, y1), pointToPointDistance(pointX, pointY, x2, y2));
    }
    
    /**
     * Return distance to line segment and cross point or null when the crossection is not on segment
     * @param pointX
     * @param pointY
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static double[] pointToLineSegmentDistanceAndCross(double pointX, double pointY, double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double dist = ((dx * (pointX - x1)) + (dy * (pointY - y1))) / ((dx * dx) + (dy * dy));
        double crossX = x1 + dx * dist;
        double crossY = y1 + dy * dist;

        if (crossX <= Math.max(x1, x2) && crossX >= Math.min(x1, x2) && crossY <= Math.max(y1, y2) && crossY >= Math.min(y1, y2)) {
            return new double[] {Math.sqrt((pointX - crossX) * (pointX - crossX) + (pointY - crossY) * (pointY - crossY)), crossX, crossY};
        }
        
        return null;
    }

    public static double pointToLineSegmentDistance(double pointX, double pointY, LineSegmentDouble lineSegment) {
        return pointToLineSegmentDistance(pointX, pointY, lineSegment.x1, lineSegment.y1, lineSegment.x2, lineSegment.y2);
    }

    public static double pointToStraightLineDistance(double pointX, double pointY, double a, double b, double c) {
        return Math.abs(a * pointX + b * pointY + c) / Math.sqrt(a * a + b * b);
    }

    public static double pointToStraightLineDistance(double pointX, double pointY, StraightLine straightLine) {
        return pointToStraightLineDistance(pointX, pointY, straightLine.a, straightLine.b, straightLine.c);
    }

    public static double pointToPointDistance(double point1X, double point1Y, double point2X, double point2Y) {
        return Math.sqrt((point2X - point1X) * (point2X - point1X) + (point2Y - point1Y) * (point2Y - point1Y));
    }

    public static double pointToPointDistance(double pointX, double pointY, Point point) {
        return pointToPointDistance(pointX, pointY, point.x, point.y);
    }
}
