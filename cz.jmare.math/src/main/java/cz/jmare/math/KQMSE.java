package cz.jmare.math;

import cz.jmare.math.entity.TwoDoubles;

public class KQMSE {
    public TwoDoubles leastSquaresKQ;
    public double mse;
    public KQMSE(TwoDoubles leastSquaresKQ, double mse) {
        super();
        this.leastSquaresKQ = leastSquaresKQ;
        this.mse = mse;
    }
    @Override
    public String toString() {
        return "KQMSE [leastSquaresKQ=" + leastSquaresKQ + ", mse=" + mse + "]";
    }
}
