package cz.jmare.math;

public class LogMath {
    public static double logb(double number, double base) {
        return (double) (Math.log(number) / Math.log(base));
    }

    public static double log2(double a) {
        return logb(a, 2);
    }
}
