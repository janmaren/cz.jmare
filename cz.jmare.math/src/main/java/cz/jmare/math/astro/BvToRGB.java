package cz.jmare.math.astro;

import cz.jmare.math.raster.entity.RGBEntUtil;
import cz.jmare.math.raster.entity.RGBEntity;

/**
 * Source: https://stackoverflow.com/questions/21977786/star-beta-v-color-index-to-apparent-rgb-color<br>
 * http://www.isthe.com/chongo/tech/astro/HR-temp-mass-table-byhrclass.html
 * @author marencik
 *
 */
public class BvToRGB {
    public static RGBEntity bvToRGBCalc(double bv) {
        double t, r = 0.0, g = 0.0, b = 0.0;
        if (bv < -0.4)
            bv = -0.4;
        if (bv > 2.0)
            bv = 2.0;
        if ((bv >= -0.40) && (bv < 0.00)) {
            t = (bv + 0.40) / (0.00 + 0.40);
            r = 0.61 + (0.11 * t) + (0.1 * t * t);
        } else if ((bv >= 0.00) && (bv < 0.40)) {
            t = (bv - 0.00) / (0.40 - 0.00);
            r = 0.83 + (0.17 * t);
        } else if ((bv >= 0.40) && (bv < 2.10)) {
            t = (bv - 0.40) / (2.10 - 0.40);
            r = 1.00;
        }
        if ((bv >= -0.40) && (bv < 0.00)) {
            t = (bv + 0.40) / (0.00 + 0.40);
            g = 0.70 + (0.07 * t) + (0.1 * t * t);
        } else if ((bv >= 0.00) && (bv < 0.40)) {
            t = (bv - 0.00) / (0.40 - 0.00);
            g = 0.87 + (0.11 * t);
        } else if ((bv >= 0.40) && (bv < 1.60)) {
            t = (bv - 0.40) / (1.60 - 0.40);
            g = 0.98 - (0.16 * t);
        } else if ((bv >= 1.60) && (bv < 2.00)) {
            t = (bv - 1.60) / (2.00 - 1.60);
            g = 0.82 - (0.5 * t * t);
        }
        if ((bv >= -0.40) && (bv < 0.40)) {
            t = (bv + 0.40) / (0.40 + 0.40);
            b = 1.00;
        } else if ((bv >= 0.40) && (bv < 1.50)) {
            t = (bv - 0.40) / (1.50 - 0.40);
            b = 1.00 - (0.47 * t) + (0.1 * t * t);
        } else if ((bv >= 1.50) && (bv < 1.94)) {
            t = (bv - 1.50) / (1.94 - 1.50);
            b = 0.63 - (0.6 * t * t);
        }
        return new RGBEntity(crossMult(r), crossMult(g), crossMult(b));
    }

    public static int crossMult(double value) {
        if (value > 1.0) {
            return 255;
        }
        if (value < 0) {
            return 0;
        }
        return (int) (value * 255 / 1.0);
    }

    public static void main(String[] args) {
        System.out.println("Minkar:");
        RGBEntity bvToRGBCalc = bvToRGBCalc(1.3);
        System.out.println(RGBEntUtil.toRGBHexString(bvToRGBCalc));
        System.out.println(RGBEntUtil.toRGBNumbersString(bvToRGBCalc));

        System.out.println("Sun:");
        bvToRGBCalc = bvToRGBCalc(0.656);
        System.out.println(RGBEntUtil.toRGBHexString(bvToRGBCalc));
        System.out.println(RGBEntUtil.toRGBNumbersString(bvToRGBCalc));

    }

}
