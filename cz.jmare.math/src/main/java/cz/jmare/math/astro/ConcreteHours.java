package cz.jmare.math.astro;

public class ConcreteHours {
    public int hours;
    public int minutes;
    public double seconds;

    public ConcreteHours(double hoursDecimal) {
        super();
        if (hoursDecimal >= 24.0) {
            hoursDecimal = hoursDecimal % 24;
        } else if (hoursDecimal < 0) {
            hoursDecimal = hoursDecimal % 24 + 24.0;
        }
        this.hours = (int) hoursDecimal;
        this.minutes = (int) ((hoursDecimal - this.hours) * 60.0);
        this.seconds = ((((hoursDecimal) - this.hours) * 60.0) - this.minutes) * 60.0;
    }

    @Override
    public String toString() {
        return (this.hours < 10 ? "0" : "") + this.hours + ":" + (this.minutes < 10 ? "0" : "") + this.minutes + ":" + (this.seconds < 10 ? "0" : "") + this.seconds;
    }

    /**
     * Return degrees by hours
     * @return
     */
    public double toDecimalDegrees() {
        return getHoursDecimal() * 15.0;
    }

    public double getHoursDecimal() {
        return this.hours + this.minutes / 60.0 + this.seconds / 3600.0;
    }

    public static ConcreteHours ofDegrees(double degrees) {
        if (degrees >= 360) {
            degrees = degrees % 360;
        }
        return new ConcreteHours(degrees / 15.0);
    }

    public static ConcreteHours ofSeconds(double seconds) {
        return new ConcreteHours(seconds / 3600.0);
    }
}
