package cz.jmare.math.astro;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class ConcreteMoment {
    public int year;
    public int month;
    public int day;
    public int hours;
    public int minutes;
    public double seconds;

    public ConcreteMoment() {
        this(ZoneOffset.UTC);
    }

    public ConcreteMoment(ZoneId zone) {
        super();
        LocalDateTime nowUTC = LocalDateTime.now(zone);
        this.year = nowUTC.getYear();
        this.month = nowUTC.getMonthValue();
        this.day = nowUTC.getDayOfMonth();
        this.hours = nowUTC.getHour();
        this.minutes = nowUTC.getMinute();
        this.seconds = nowUTC.getSecond() + nowUTC.getNano() / 1_000_000_000.0;
    }

    public ConcreteMoment(int year, int month, int day, int hours, int minutes, double seconds) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public double getHoursDecimal() {
        return hours + minutes / 60.0 + seconds / 3600.0;
    }
}
