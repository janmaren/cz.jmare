package cz.jmare.math.astro;

/**
 * {@link https://thecynster.home.blog/2019/11/04/calculating-sidereal-time/}
 * @author jan
 *
 */
public class AstroCalc {

    /**
     * Modulo - always nonnegative number
     * @param val
     * @param op
     * @return
     */
    public static double mod(double val, double op) {
        int div = (int) (val / op);
        double res = val - div * op;
        return res < 0 ? op + res : res;
    }

    /**
     * Modulo - always nonnegative number (same as {@link #mod(double, double)})
     * @param val
     * @param op
     * @return
     */
    public static double modulo(double val, double op) {
        double res = val % op;
        return res < 0 ? op + res : res;
    }

    public static double reduceAngle(double d) {
        if (d >= 360) {
            d %= 360;
        }

        if (d < 0) {
            d += 360;
        }
        return d;
    }

    public static void main(String[] args) {
    }

}
