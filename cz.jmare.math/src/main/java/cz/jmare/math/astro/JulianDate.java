package cz.jmare.math.astro;

import cz.jmare.data.util.JulianUtil;

public class JulianDate {
    private static double fracDayFromNoon(int hours, int minutes, double seconds) {
        return ((hours - 12) + minutes / 60.0 + seconds / 3600.0) / 24.0;
    }
    
    /**
     * Compare with https://www.onlineconversion.com/julian_date.htm
     * @param concreteMoment
     * @return
     */
    public static double toJulianExactly(ConcreteMoment concreteMoment) {
        return toJulian(concreteMoment) + fracDayFromNoon(concreteMoment.hours, concreteMoment.minutes, concreteMoment.seconds);
    }

    /**
     * Calculate juliand date without decimal part
     * @param concreteMoment
     * @return
     */
    public static double toJulian(ConcreteMoment concreteMoment) {
        return JulianUtil.toJulian(concreteMoment.year, concreteMoment.month, concreteMoment.day);
    }
}
