package cz.jmare.math.astro;

public class SiderealTime {
    /**
     * Calculate GMST in hours with error 0.1 per century
     * https://github.com/jhaupt/Sidereal-Time-Calculator/blob/master/SiderealTimeCalculator.py
     * @param jd
     * @return
     */
    public static double calcGmstHoursSimple(double jd) {
        double gmst = 18.697374558 + 24.06570982441908 * (jd - 2451545);
        return gmst;
    }

    /**
     * Calculate GMST in hours with error 0.1 per century
     * @param jd
     * @return
     */
    public static double calcGmstHoursSimple(ConcreteMoment concreteMoment) {
        double jd = JulianDate.toJulianExactly(concreteMoment);
        return calcGmstHoursSimple(jd);
    }

    /**
     * https://stackoverflow.com/questions/53807401/sas-creating-a-season-variable-from-a-date-variable
     * @param d0
     * @param t
     * @param h
     * @return
     */
    public static double calcGmstHoursBetter(double d0, double t, double h) {
        double gmst = 6.697374558 + 0.06570982441908 * d0 + 1.00273790935 * h + 0.000026 * Math.pow(t, 2);
        return gmst;
    }

    public static double calcGmstHoursBetter(ConcreteMoment concreteMoment) {
        double jd = JulianDate.toJulianExactly(concreteMoment);
        double jd0 = Math.floor(jd) - 0.5;
        double d0 = jd0 - 2451545.0;
        double d = jd - 2451545.0;
        double t = d / 36525.0;
        double gmst = calcGmstHoursBetter(d0, t, concreteMoment.getHoursDecimal());
        return gmst;
    }

    public static double calcUT1Seconds(double jd) {
        double UT1 = (jd - Math.floor(jd) + 0.5) * 86400.0;
        return UT1;
    }

    public static double calculateGmst(ConcreteMoment concreteMoment) {
        double gmst = calculateGmstHoursIAU(concreteMoment);
        gmst %= 24.0;
        gmst = (gmst + 24.0) % 24.0;
        return gmst;
    }

    public static double calculateGmstHoursIAU(ConcreteMoment concreteMoment) {
        double jd = JulianDate.toJulianExactly(concreteMoment);
        double d = jd - 2451545.0;
        double t = d / 36525.0;
        double gmstSeconds = SiderealTime.calculateGmstSecondsIAU(t);
        return gmstSeconds / 3600.0 + concreteMoment.getHoursDecimal();
    }

    public static double calculateGmstSecondsIAU(double t) {
        double gmstSec = 24110.54841 + 8640184.812866 * t
                + 0.093104 * Math.pow(t, 2) - 0.0000062 * Math.pow(t, 3);
        return gmstSec;
    }

    /**
     * Calculates LMST
     * @param gmst Greenwich mean sidereal time
     * @param longitude longitude - on the east it's positive, on the west from Greenwich negative
     * @return local mean sidereal time
     */
    public static double toLmst(double gmst, double longitude) {
        return gmst + longitude / 15.0;
    }

    /**
     * Returns hour angle
     * @param lmst local mean sidereal time
     * @param objectRa right ascension of object. It should be the current RA
     * @return hour angle
     */
    public static double toHa(double lmst, double objectRa) {
        return (lmst - objectRa);
    }

    /**
     * compare with https://astro.subhashbose.com/siderealtime/?longitude=0
     * @param args
     */
    public static void main(String[] args) {
        ConcreteMoment concreteMoment = new ConcreteMoment(2121, 4, 30, 20, 8, 50);
        double calcGmst = calcGmstHoursSimple(concreteMoment);
        ConcreteHours concreteHours = new ConcreteHours(calcGmst);
        System.out.println("gmst simple: " + concreteHours);

        double gmstHoursExactly = calcGmstHoursBetter(concreteMoment);
        System.out.println("gmst better: " + new ConcreteHours(gmstHoursExactly));

        double gmstHoursIAU = SiderealTime.calculateGmstHoursIAU(concreteMoment);
        System.out.println("gmst IAU: " + new ConcreteHours(gmstHoursIAU));

        double natal = SiderealTime.calculateGmstHoursIAU(new ConcreteMoment(1974, 3, 31, 13, 10, 0));
        System.out.println("Natal: " + new ConcreteHours(toLmst(natal, 17 + 8 / 60.0)));
    }
}
