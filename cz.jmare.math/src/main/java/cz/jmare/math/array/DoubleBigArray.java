package cz.jmare.math.array;

public class DoubleBigArray extends AbstractBigArray {

    private static final int ITEM_BYTES_LENGTH = 8;

    public DoubleBigArray() {
        super();
    }

    public DoubleBigArray(int bufferSize) {
        super(bufferSize);
    }

    public void putDouble(long index, double value) {
        prepareBuffer(index);
        buffer.clear();
        buffer.putDouble((int) ((index - bufferPosition ) * ITEM_BYTES_LENGTH), value);
    }

    public double getDouble(long index) {
        prepareBuffer(index);
        buffer.clear();
        return buffer.getDouble((int) ((index - bufferPosition) * ITEM_BYTES_LENGTH));
    }

    @Override
    protected int getItemBytesLength() {
        return ITEM_BYTES_LENGTH;
    }
}
