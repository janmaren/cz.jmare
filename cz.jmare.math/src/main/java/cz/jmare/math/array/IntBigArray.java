package cz.jmare.math.array;

public class IntBigArray extends AbstractBigArray {

    private static final int ITEM_BYTES_LENGTH = 4;

    public IntBigArray() {
        super();
    }

    public IntBigArray(int bufferSize) {
        super(bufferSize);
    }

    public void putInt(long index, int value) {
        prepareBuffer(index);
        buffer.clear();
        buffer.putInt((int) ((index - bufferPosition ) * ITEM_BYTES_LENGTH), value);
    }

    public int getInt(long index) {
        prepareBuffer(index);
        buffer.clear();
        return buffer.getInt((int) ((index - bufferPosition) * ITEM_BYTES_LENGTH));
    }

    @Override
    protected int getItemBytesLength() {
        return ITEM_BYTES_LENGTH;
    }
}
