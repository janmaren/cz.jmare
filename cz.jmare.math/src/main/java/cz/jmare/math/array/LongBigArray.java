package cz.jmare.math.array;

public class LongBigArray extends AbstractBigArray {

    private static final int ITEM_BYTES_LENGTH = 8;

    public LongBigArray() {
        super();
    }

    public LongBigArray(int bufferSize) {
        super(bufferSize);
    }

    public void putLong(long index, long value) {
        prepareBuffer(index);
        buffer.clear();
        buffer.putLong((int) ((index - bufferPosition ) * ITEM_BYTES_LENGTH), value);
    }

    public long getLong(long index) {
        prepareBuffer(index);
        buffer.clear();
        return buffer.getLong((int) ((index - bufferPosition) * ITEM_BYTES_LENGTH));
    }

    @Override
    protected int getItemBytesLength() {
        return ITEM_BYTES_LENGTH;
    }
}
