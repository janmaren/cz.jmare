package cz.jmare.math.array;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import cz.jmare.file.FriendlyAutoCloseable;

public abstract class AbstractBigArray implements FriendlyAutoCloseable {
    private static final int DEFAULT_BUFFER_ITEMS_SIZE = 1024;
    private FileChannel channel;
    private RandomAccessFile file;
    protected ByteBuffer buffer;
    protected long bufferPosition; // current index of buffer against file. Must by multiplied by 8
    protected int bufferSize; // count of double numbers in buffer. Must by multiplied by 8
    private File tempFile;

    public AbstractBigArray() {
        this(DEFAULT_BUFFER_ITEMS_SIZE);
    }

    public AbstractBigArray(int bufferSize) {
        this.bufferSize = bufferSize;
        try {
            tempFile = File.createTempFile("array-", ".dat");
            file = new RandomAccessFile(tempFile, "rw");
            channel = file.getChannel();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to open temporary file");
        }
        buffer = ByteBuffer.allocate(8 * bufferSize);
    }


    protected void prepareBuffer(long index) {
        if (index < bufferPosition || index >= bufferPosition + bufferSize) {
            try {
                writeBufferToFile();
                loadNewPositionDataToBuffer(index);
            } catch (IOException e) {
                throw new IllegalStateException("Unable to prepare buffer for index " + index, e);
            }
        }
    }

    private void loadNewPositionDataToBuffer(long index) throws IOException {
        buffer.flip();
        buffer.clear();
        bufferPosition = (long) (index - (bufferSize / 2.0));
        if (bufferPosition < 0) {
            bufferPosition = 0;
        }
        channel.position(bufferPosition * getItemBytesLength());
        while (buffer.hasRemaining()) {
            int number = channel.read(buffer);
            if (number == -1) {
                break;
            }
        }
    }

    private void writeBufferToFile() throws IOException {
        buffer.flip();
        channel.position(bufferPosition * getItemBytesLength());
        buffer.clear();
        while (buffer.hasRemaining()) {
            channel.write(buffer);
        }
    }


    @Override
    public void close() {
        try {
            channel.close();
            file.close();
            tempFile.delete();
        } catch (IOException e) {
        }
    }

    protected abstract int getItemBytesLength();
}
