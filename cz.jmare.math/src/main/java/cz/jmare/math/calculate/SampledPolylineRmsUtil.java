package cz.jmare.math.calculate;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.fce.SampledPolylineFunction;
import cz.jmare.math.geometry.entity.Point;

public class SampledPolylineRmsUtil {
    public static final double EPSILON  = 0.001;

    public static double calculateRms(CurveHolder curveHolder) {
        Point point = null;//points.get(0);
        double lastX = curveHolder.getCurveInfo().minX;
        double lastY = curveHolder.getCurveInfo().minY;

        double sum = 0;
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            for (long i = 1; i < curveReader.size(); i++) {
                point = curveReader.get(i);
                double deltaX = point.x - lastX;
                double middleY = (point.y + lastY) / 2;
                sum += deltaX * middleY * middleY;
                lastX = point.x;
                lastY = point.y;
            }
        }
        double xRange = curveHolder.getCurveInfo().maxX - curveHolder.getCurveInfo().minX;
        return Math.sqrt(sum / xRange);
    }

    public static double calculateRms(List<Point> points) {
        if (points.size() < 2) {
            return Double.NaN;
        }

        Point point = points.get(0);
        double lastX = point.x;
        double lastY = point.y;

        double sum = 0;
        for (int i = 1; i < points.size(); i++) {
            point = points.get(i);
            double deltaX = point.x - lastX;
            double middleY = (point.y + lastY) / 2;
            sum += deltaX * middleY * middleY;
            lastX = point.x;
            lastY = point.y;
        }
        return Math.sqrt(sum / (points.get(points.size() - 1).x - points.get(0).x));
    }

    public static double calculateRms(List<Point> points, double epsilon) {
        if (points.size() < 2) {
            return Double.NaN;
        }
        SampledPolylineFunction sampledPolylineFunction = new SampledPolylineFunction(points);
        double end = points.get(points.size() - 1).x;
        double sum = 0;
        double x = points.get(0).x;
        int number = 0;
        while (x <= end) {
            Double y = sampledPolylineFunction.apply(x);
            sum += epsilon * y * y;
            number++;
            x += epsilon;
        }

        return Math.sqrt(sum / (number * epsilon));
    }

    public static void main(String[] args) {
        double x = 0;
        ArrayList<Point> points = new ArrayList<Point>();
        while (x < (Math.PI *2)) {
            points.add(new Point(x, Math.sin(x)));
            x += 0.01;
        }
        double rms = calculateRms(points);
        System.out.println("RMS: " + rms);

        rms = calculateRms(points, 0.01);
        System.out.println("RMS with eps: " + rms);
    }
}
