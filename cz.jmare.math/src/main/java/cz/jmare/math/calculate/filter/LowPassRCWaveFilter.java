package cz.jmare.math.calculate.filter;

public class LowPassRCWaveFilter implements WaveFilter {
    private double lastFilteredY = 0;

    private double alpha;

    public LowPassRCWaveFilter(double centerFreq, double sampleRate) {
        double rc = 1.0 / (centerFreq * 2 * Math.PI);
        double dt = 1.0 / sampleRate;
        alpha = dt / (rc + dt);
    }

    @Override
    public double nextFilteredY(double nextInputY) {
        double y = lastFilteredY + (alpha * (nextInputY - lastFilteredY));
        lastFilteredY = y;
        return y;
    }

    @Override
    public void reset() {
        lastFilteredY = 0;
    }
}
