package cz.jmare.math.calculate;

import cz.jmare.math.exception.NotSuccessfulOperationException;

public class BasicFloatCalc {

    public static float quadrantsSum(float[] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("No value in array");
        }
        float sum = 0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i] * values[i];
        }
        return sum;
    }

    public static int maxIndex(float[] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("No value in array");
        }
        float maxValue = 1E-38f;
        int maxIndex = -1;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > maxValue) {
                maxValue = values[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public static int minIndex(float[] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("No value in array");
        }
        float minValue = 1E+38f;
        int minIndex = -1;
        for (int i = 0; i < values.length; i++) {
            if (values[i] < minValue) {
                minValue = values[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static double safeMinus1ToPlus1Factor(float min, float max, int samplePcmBits) throws NotSuccessfulOperationException {
        long halfLongValue = (long) Math.pow(2, samplePcmBits - 1);
        long halfLongValueLess = halfLongValue - 1;
        float maxAllowedValue = (halfLongValueLess / (float) halfLongValue);
        return amplitudeFitFactor(min, max, -1f, maxAllowedValue);
    }

    public static double amplitudeFitFactor(float min, float max, double minAllowedValue, double maxAllowedValue) throws NotSuccessfulOperationException {
        if (min > 0 && max > 0 || min < 0 && max < 0) {
            throw new NotSuccessfulOperationException("Unable to determine amplitude factor");
        }
        if (Math.abs(min / minAllowedValue) > Math.abs(max / maxAllowedValue)) {
            return minAllowedValue / min;
        } else {
            return maxAllowedValue / max;
        }
    }

    public static void changeAmplitude(float[][] values, float amplitudeFactor) {
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                values[i][j] *= amplitudeFactor;
            }
        }
    }
}
