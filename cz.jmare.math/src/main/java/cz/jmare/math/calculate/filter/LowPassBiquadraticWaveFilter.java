package cz.jmare.math.calculate.filter;

public class LowPassBiquadraticWaveFilter implements WaveFilter {
    double a0, a1, a2, b0, b1, b2;
    double x1, x2, y, y1, y2;

    public LowPassBiquadraticWaveFilter(double centerFreq, double sampleRate) {
        this(centerFreq, sampleRate, 1.4142135623730951 /* BW 1 octave */);
    }

    public LowPassBiquadraticWaveFilter(double centerFreq, double sampleRate, double q) {
        double omega = 2 * Math.PI * centerFreq / sampleRate;
        double sn = Math.sin(omega);
        double cs = Math.cos(omega);
        double alpha = sn / (2 * q);

        b0 = (1 - cs) / 2;
        b1 = 1 - cs;
        b2 = (1 - cs) / 2;
        a0 = 1 + alpha;
        a1 = -2 * cs;
        a2 = 1 - alpha;

        b0 /= a0;
        b1 /= a0;
        b2 /= a0;
        a1 /= a0;
        a2 /= a0;
    }

    @Override
    public double nextFilteredY(double nextInputY) {
        y = b0 * nextInputY + b1 * x1 + b2 * x2 - a1 * y1 - a2 * y2;
        x2 = x1;
        x1 = nextInputY;
        y2 = y1;
        y1 = y;
        return y;
    }

    @Override
    public void reset() {
        x2 = 0;
        x1 = 0;
        y2 = 0;
        y1 = 0;
    }
}
