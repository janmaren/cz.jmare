package cz.jmare.math.calculate;

import cz.jmare.math.calculate.filter.WaveFilter;

public class FilteredGain {

    public static double filteredGainForFrequency(WaveFilter waveFilter, double sampleRate, double frequencySine) {
        int pointsPerSine = (int) (sampleRate / frequencySine);
        double samplePeriod = 1 / sampleRate;
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < pointsPerSine * 3; i++) {
            double x = i * samplePeriod;
            double sin = Math.sin(x * 2 * Math.PI * frequencySine);
            double nextFilteredY = waveFilter.nextFilteredY(sin);
            if (i > pointsPerSine) { // ignore first sine wave
                if (nextFilteredY > max) {
                    max = nextFilteredY;
                }
                if (nextFilteredY < min) {
                    min = nextFilteredY;
                }
            }
        }
        if (max == Double.MIN_VALUE) {
            throw new RuntimeException("Max not populated");
        }
        if (min == Double.MAX_VALUE) {
            throw new RuntimeException("Min not populated");
        }
        return (Math.abs(max) + Math.abs(min)) / 2.0;
    }

}
