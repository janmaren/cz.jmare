package cz.jmare.math.calculate.filter;

public class HighPassRCWaveFilter implements WaveFilter {
    private double lastY = 0;
    private double lastFilteredY = 0;

    private double alpha;

    public HighPassRCWaveFilter(double centerFreq, double sampleRate) {
        double rc = 1.0 / (centerFreq * 2 * Math.PI);
        double dt = 1.0 / sampleRate;
        alpha = rc / (rc + dt);
    }

    @Override
    public double nextFilteredY(double x) {
        double y = alpha * (lastFilteredY + x - lastY);
        lastY = x;
        lastFilteredY = y;
        return y;
    }

    @Override
    public void reset() {
        lastY = 0;
        lastFilteredY = 0;
    }
}
