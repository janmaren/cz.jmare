package cz.jmare.math.calculate.filter;

import java.util.List;

public class CompoundWaveFilter implements WaveFilter {
    private List<? extends WaveFilter> waveFilters;

    public CompoundWaveFilter(List<? extends WaveFilter> waveFilters) {
        this.waveFilters = waveFilters;
    }

    @Override
    public double nextFilteredY(double nextInputY) {
        double sum = 0;
        for (WaveFilter waveFilter : waveFilters) {
            sum += waveFilter.nextFilteredY(nextInputY);
        }
        return sum / waveFilters.size();
    }

    @Override
    public void reset() {
        for (WaveFilter waveFilter : waveFilters) {
            waveFilter.reset();
        }
    }
}
