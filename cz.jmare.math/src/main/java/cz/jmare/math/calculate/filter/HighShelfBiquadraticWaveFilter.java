package cz.jmare.math.calculate.filter;

public class HighShelfBiquadraticWaveFilter implements WaveFilter {
    double a0, a1, a2, b0, b1, b2;
    double x1, x2, y, y1, y2;

    public HighShelfBiquadraticWaveFilter(double centerFreq, double sampleRate, double gainDB) {
        double omega = 2 * Math.PI * centerFreq / sampleRate;
        double sn = Math.sin(omega);
        double cs = Math.cos(omega);
        double gainAbs = Math.pow(10, gainDB / 40);
        double beta = Math.sqrt(gainAbs + gainAbs);

        b0 = gainAbs * ((gainAbs + 1) + (gainAbs - 1) * cs + beta * sn);
        b1 = -2 * gainAbs * ((gainAbs - 1) + (gainAbs + 1) * cs);
        b2 = gainAbs * ((gainAbs + 1) + (gainAbs - 1) * cs - beta * sn);
        a0 = (gainAbs + 1) - (gainAbs - 1) * cs + beta * sn;
        a1 = 2 * ((gainAbs - 1) - (gainAbs + 1) * cs);
        a2 = (gainAbs + 1) - (gainAbs - 1) * cs - beta * sn;

        b0 /= a0;
        b1 /= a0;
        b2 /= a0;
        a1 /= a0;
        a2 /= a0;
    }

    @Override
    public double nextFilteredY(double nextInputY) {
        y = b0 * nextInputY + b1 * x1 + b2 * x2 - a1 * y1 - a2 * y2;
        x2 = x1;
        x1 = nextInputY;
        y2 = y1;
        y1 = y;
        return y;
    }

    @Override
    public void reset() {
        x2 = 0;
        x1 = 0;
        y2 = 0;
        y1 = 0;
    }
}
