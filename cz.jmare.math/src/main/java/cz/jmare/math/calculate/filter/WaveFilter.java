package cz.jmare.math.calculate.filter;

public interface WaveFilter {
    double nextFilteredY(double nextInputY);
    void reset();
}
