package cz.jmare.math.projection;

import com.jhlabs.map.MapMath;

import cz.jmare.math.geometry.entity.Point;

/**
 * Taken from com.jhlabs.map.proj.GnomonicAzimuthalProjection
 * the mode=EQUATOR
 */
public class GnomonicEquatorProjection implements Projection {
    @Override
    public Point toScreenCoordinates(double angleHoriz, double angleVert) {
        double sinphi = Math.sin(angleVert);
        double cosphi = Math.cos(angleVert);
        double coslam = Math.cos(angleHoriz);

        double y = cosphi * coslam;
        double x = (y = 1.0 / y) * cosphi * Math.sin(angleHoriz);
        y *= sinphi;
        return new Point(x, y);
    }

    @Override
    public Spheric toRealCoordinates(double x, double y) {
        double lpy = 0;
        double lpx = 0;

        double rh = MapMath.distance(x, y);
        double sinz = Math.sin(lpy = Math.atan(rh));
        double cosz = Math.sqrt(1. - sinz * sinz);
        if (Math.abs(rh) <= EPS10) {
            lpy = 0;
            lpx = 0;
        } else {
            lpy = y * sinz / rh;
            if (Math.abs(lpy) >= 1.)
                lpy = lpy > 0. ? MapMath.HALFPI : - MapMath.HALFPI;
            else
                lpy = Math.asin(lpy);
            y = cosz * rh;
            x *= sinz;
            lpx = Math.atan2(x, y);
        }
        return new Spheric(lpx, lpy);
    }

    public static double distance(double dx, double dy) {
        return Math.sqrt(dx*dx+dy*dy);
    }

    @Override
    public boolean supports24Meridians() {
        return false;
    }

    @Override
    public boolean supportsWholeLongitude() {
        return false;
    }
}
