package cz.jmare.math.projection;

import cz.jmare.math.geometry.entity.Point;

public class SimpleProjection implements Projection {
    @Override
    public Point toScreenCoordinates(double angleHoriz, double angleVert) {
        if (angleHoriz < -Math.PI) {
            angleHoriz += 2 * Math.PI;
        }
        if (angleHoriz > Math.PI) {
            angleHoriz -= 2 * Math.PI;
        }
        return new Point(angleHoriz, angleVert);
    }

    @Override
    public Spheric toRealCoordinates(double x, double y) {
        return new Spheric(x, y);
    }

    @Override
    public boolean supports24Meridians() {
        return true;
    }

    @Override
    public boolean supportsWholeLongitude() {
        return true;
    }
}
