package cz.jmare.math.projection;

import cz.jmare.math.geometry.entity.Point;

public class TanProjection implements Projection {
    @Override
    public Point toScreenCoordinates(double angleHoriz, double angleVert) {
        return new Point(Math.tan(angleHoriz), Math.tan(angleVert));
    }

    @Override
    public Spheric toRealCoordinates(double x, double y) {
        return new Spheric(Math.atan(x), Math.atan(y));
    }

    @Override
    public boolean supports24Meridians() {
        return false;
    }

    @Override
    public boolean supportsWholeLongitude() {
        return true;
    }
}
