package cz.jmare.math.projection;

public class ProjScreenCache {
    /**
     * [from top to bottom][from left to right]
     */
    private double ras[][] = new double[0][];
    /**
     * [from top to bottom][from left to right]
     */
    private double decs[][] = new double[0][];
    private Projection projection;
    private Double zoom;
    private int xCenter;
    private int yCenter;
    private int width;
    private int height;

    public ProjScreenCache(Projection projection) {
        this.setProjection(projection);
    }

    public void initData(int xCenter, int yCenter, int width, int height, double zoom) {
        if (this.zoom == null || this.zoom != zoom ||
                width != this.width || height != this.height ||
                xCenter != this.xCenter || yCenter != this.yCenter) {
            ras = new double[height][];
            decs = new double[height][];
            for (int i = 0; i < height; i++) {
                ras[i] = new double[width];
                decs[i] = new double[width];
            }
            for (int i = 0; i <= yCenter; i++) {
                for (int j = 0; j <= xCenter; j++) {
                    double deltaX = (j - xCenter) / zoom;
                    double deltaY = (yCenter - i) / zoom; // up positive, bottom negative
                    Spheric realCoordinates = projection.toRealCoordinates(deltaX, deltaY);
                    ras[i][j] = realCoordinates.lon;
                    decs[i][j] = realCoordinates.lat;
                }
            }
            // copy to right top quadrant
            for (int i = 0; i <= yCenter; i++) {
                for (int j = xCenter + 1, fromX = xCenter - 1; j < width; j++, fromX--) {
                    ras[i][j] = -ras[i][fromX];
                    decs[i][j] = decs[i][fromX];
                }
            }

            // copy to bottom half
            for (int i = yCenter + 1, fromY = yCenter - 1; i < height; i++, fromY--) {
                for (int j = 0; j < width; j++) {
                    ras[i][j] = ras[fromY][j];
                    decs[i][j] = -decs[fromY][j];
                }
            }
            if (ras[height - 1][width - 1] == 0) {
                throw new IllegalStateException("zero point in right bottom corner");
            }
            this.zoom = zoom;
            this.xCenter = xCenter;
            this.yCenter = yCenter;
            this.width = width;
            this.height = height;
        }
    }

    /**
     * @param screenX
     * @param screenY screen y from top to bottom
     * @return
     */
    public Spheric getPolar(int screenX, int screenY) {
        return new Spheric(ras[screenY][screenX], decs[screenY][screenX]);
    }

    public void setProjection(Projection projection) {
        this.projection = projection;
        this.zoom = null;
    }
}
