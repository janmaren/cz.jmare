package cz.jmare.math.projection;

import cz.jmare.math.geometry.entity.Point;

public interface Projection {
    final static double EPS10 = 1e-10;
    /**
     * Convert real angle coordinates to screen cartesian. It returns a real number and no zoom (radius) is encountered.
     * Point.x is enlarging from left to right and point.y is enarging from bottom to up.<br>
     * Point with angles 0, 0 must return coordinates 0, 0
     * @param angleHoriz
     * @param angleVert
     * @return coordinates for center of pixel
     */
    Point toScreenCoordinates(double angleHoriz, double angleVert);

    /**
     * Convert screen coordinates to real
     * @param x screen coordinate in the middle of pixel, but without zoom (radius) encountered
     * @param y screen coordinate in the middle of pixel, but without zoom (radius) encountered
     * @return
     */
    Spheric toRealCoordinates(double x, double y);

    boolean supports24Meridians();

    boolean supportsWholeLongitude();
}
