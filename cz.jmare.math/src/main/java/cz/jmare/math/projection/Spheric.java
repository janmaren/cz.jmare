package cz.jmare.math.projection;

import cz.jmare.data.util.GonUtil;

/**
 * Polar coordinates in radians where ra represents a right ascension (or longitude) and dec is declination (or latitude)
 */
public class Spheric {
    /**
     * Right ascension (or longitude) with meanful values 0 to 2*pi radians unless a next round
     */
    public double lon;

    /**
     * Declination (or latitude) with meanful values -pi/2 to pi/2 radians
     */
    public double lat;

    public Spheric(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getRaHours() {
        double hoursDecimal = 12 * lon / Math.PI;
        if (hoursDecimal >= 24.0) {
            hoursDecimal = hoursDecimal % 24;
        } else if (hoursDecimal < 0) {
            hoursDecimal = hoursDecimal % 24 + 24.0;
        }
        return hoursDecimal;
    }

    public double getDecDeg() {
        double degrees = 180 * lat / Math.PI;
        if (degrees >= 360.0) {
            degrees %= 360.0;
        } else if (degrees < 0) {
            degrees = degrees % 360.0 + 360.0;
        }
        return degrees;
    }

    @Override
    public String toString() {
        return "Spheric [lon=" + Math.toDegrees(lon) + "deg, lat=" + Math.toDegrees(lat) + "deg]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lon);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Spheric other = (Spheric) obj;
        if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
            return false;
        if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon))
            return false;
        return true;
    }

    public static Spheric normalizedSpheric(double lon, double lat) {
        return new Spheric(GonUtil.normalizeRadZeroTwoPi(lon), GonUtil.normalizeRadMinusPiPi(lat));
    }
}
