package cz.jmare.math.projection;

import com.jhlabs.map.MapMath;

import cz.jmare.math.geometry.entity.Point;

/**
 * Taken from com.jhlabs.map.proj.StereographicAzimuthalProjection
 * the mode=EQUATOR, spherical=true
 */
public class StereoEquatorSpherProjection implements Projection {
    double akm1 = 2. * 0.994;

    @Override
    public Point toScreenCoordinates(double angleHoriz, double angleVert) {
        double coslam = Math.cos(angleHoriz);
        double sinlam = Math.sin(angleHoriz);
        double sinphi = Math.sin(angleVert);
        double cosphi = Math.cos(angleVert);

        double x = 0;
        double y = 1. + cosphi * coslam;
        if (y <= EPS10) {
            throw new IllegalStateException("Invalid y");
        }
        x = (y = akm1 / y) * cosphi * sinlam;
        y *= sinphi;

        return new Point(x, y);
    }

    @Override
    public Spheric toRealCoordinates(double x, double y) {
        double  c, rh, sinc, cosc;

        sinc = Math.sin(c = 2. * Math.atan((rh = MapMath.distance(x, y)) / akm1));
        cosc = Math.cos(c);
        double lpx = 0;
        double lpy;

        if (Math.abs(rh) <= EPS10)
            lpy = 0.;
        else
            lpy = Math.asin(y * sinc / rh);
        if (cosc != 0. || x != 0.)
            lpx = Math.atan2(x * sinc, cosc * rh);

        if (Math.abs(rh) <= EPS10)
            lpy = 0.;
        else
            lpy = Math.asin(y * sinc / rh);
        if (cosc != 0. || x != 0.)
            lpx = Math.atan2(x * sinc, cosc * rh);

        return new Spheric(lpx, lpy);
    }

    @Override
    public boolean supports24Meridians() {
        return true;
    }

    @Override
    public boolean supportsWholeLongitude() {
        return false;
    }
}
