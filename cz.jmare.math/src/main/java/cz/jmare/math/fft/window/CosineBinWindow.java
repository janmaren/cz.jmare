package cz.jmare.math.fft.window;

public class CosineBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return (float) Math.cos(Math.PI * index / (length - 1) - Math.PI / 2);
    }

    @Override
    public String toString() {
        return "Cosine Window";
    }
}
