package cz.jmare.math.fft.window;

public class BlackmanHarrisWindow implements BinWindow {
    private float a0 = 0.35875f;
    private float a1 = 0.48829f;
    private float a2 = 0.14128f;
    private float a3 = 0.01168f;

    public BlackmanHarrisWindow() {
    }

    @Override
    public float getValue(int length, int index) {
        return a0 - a1 * (float) Math.cos(TWO_PI * index / (length - 1f))
                + a2 * (float) Math.cos(4f * Math.PI * index / (length - 1f))
                - a3 * (float) Math.cos(6f * Math.PI * index / (length - 1f));
    }

    @Override
    public String toString() {
        return "Blackman-Harris Window";
    }
}
