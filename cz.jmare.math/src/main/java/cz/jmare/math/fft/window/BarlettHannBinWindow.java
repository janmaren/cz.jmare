package cz.jmare.math.fft.window;

public class BarlettHannBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return 0.62f - 0.48f * (float) Math.abs(index / (length - 1f) - 0.5f) - 0.38f * (float) Math.cos(TWO_PI * index / (length - 1f));
    }

    @Override
    public String toString() {
        return "BarlettHann Window";
    }
}
