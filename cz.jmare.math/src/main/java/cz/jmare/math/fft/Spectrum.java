package cz.jmare.math.fft;

import cz.jmare.math.entity.TwoFloats;
import cz.jmare.math.fft.window.BinWindow;

public class Spectrum {
    /**
     * Points - range half
     */
    private float[] spectrumValues;

    /**
     * Full bins number which is 2 x spectrumValues.size()
     */
    private int binsNumber;

    private float binFrequency;

    private BinWindow binWindow;

    private Float sampleFrequency;

    private Float rms;

    private Float binsSum;

    public Spectrum(float[] spectrumPoints, float binFrequency) {
        this.spectrumValues = spectrumPoints;
        this.binFrequency = binFrequency;
        this.binsNumber = spectrumPoints.length * 2;
    }

    public float[] getSpectrumValues() {
        return spectrumValues;
    }

    /**
     * Full bins number which is 2 x spectrumValues.size()
     * @return
     */
    public int getBinsNumber() {
        return spectrumValues.length * 2;
    }

    public float getBinFrequency() {
        return binFrequency;
    }

    public BinWindow getBinWindow() {
        return binWindow;
    }

    public void setBinWindow(BinWindow binWindow) {
        this.binWindow = binWindow;
    }

    public float getSampleFrequency() {
        return sampleFrequency;
    }

    public void setSampleFrequency(float sampleFrequency) {
        this.sampleFrequency = sampleFrequency;
    }

    public float getBandwidth() {
        return spectrumValues.length * binFrequency;
    }

    public float getValueByFrequency(float frequency) {
        int binIndex = (int) ((frequency * binsNumber / sampleFrequency));
        return spectrumValues[binIndex];
    }

    public int getBin(float frequency) {
        int binIndex = (int) ((frequency * binsNumber / sampleFrequency));
        if (binIndex >= spectrumValues.length) {
            binIndex--;
        }
        return binIndex;
    }

    public TwoFloats getFrequencyRange(int binIndex) {
        return new TwoFloats(binIndex * binFrequency, (binIndex + 1) * binFrequency);
    }

    public void setRms(float rms) {
        this.rms = rms;
    }

    public Float getRms() {
        return rms;
    }

    public void setBinsSum(float binsSum) {
        this.binsSum = binsSum;
    }

    public Float getBinsSum() {
        return binsSum;
    }

    public int calculateBinsLeakage(float consta) {
        float[] testSineValues = new float[binsNumber];
        for (int i = 0; i < binsNumber; i++) {
            testSineValues[i] = (float) Math.sin((2.0 * Math.PI * i * 750.0) / sampleFrequency);
        }
        float[] calculateFftValues = SpectrumResolver.calculateFftValues(testSineValues, binsNumber, binWindow);
        float sum = 0.0f;
        float max = Float.MIN_VALUE;
        int maxBin = -1;
        for (int i = 0; i < calculateFftValues.length; i++) {
            sum += calculateFftValues[i];
            if (calculateFftValues[i] > max) {
                max = calculateFftValues[i];
                maxBin = i;
            }
        }
        float s = calculateFftValues[maxBin];
        int neigh = 0;
        while (s / sum < consta && neigh < calculateFftValues.length) {
            neigh++;
            if (maxBin - neigh >= 0) {
                s += calculateFftValues[maxBin - neigh];
            }
            if (maxBin + neigh < calculateFftValues.length) {
                s+= calculateFftValues[maxBin + neigh];
            }
        }
        return neigh;
    }

    @Override
    public String toString() {
        return "Spectrum [sampleFrequency=" + sampleFrequency + ", spectrumPoints=" + spectrumValues.length + ", binsNumber="
                + binsNumber + ", binFrequency=" + binFrequency + ", bandwidth=" + getBandwidth() + ", binWindow=" + binWindow + ", rms=" + rms + "]";
    }
}
