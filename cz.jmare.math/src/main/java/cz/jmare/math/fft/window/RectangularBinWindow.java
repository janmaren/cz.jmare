package cz.jmare.math.fft.window;

public class RectangularBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return 1.0f;
    }

    @Override
    public String toString() {
        return "Rectangular Window";
    }
}
