package cz.jmare.math.fft.window;

public class LanczosBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        double x = 2.0 * index / (length - 1) - 1;
        return (float) Math.sin(Math.PI * x) / (float) (Math.PI * x);
    }

    @Override
    public String toString() {
        return "Lanczos Window";
    }
}
