package cz.jmare.math.fft.window;

public class BlackmanBinWindow implements BinWindow {
    protected float alpha;

    public BlackmanBinWindow(float alpha) {
        this.alpha = alpha;
    }

    /** Constructs a Blackman window with a default alpha value of 0.16 */
    public BlackmanBinWindow() {
        this(0.16f);
    }

    @Override
    public float getValue(int length, int index) {
        float a0 = (1f - this.alpha) / 2.0f;
        float a1 = 0.5f;
        float a2 = this.alpha / 2.0f;

        return a0 - a1 * (float) Math.cos(TWO_PI * index / (length - 1f))
                + a2 * (float) Math.cos(4f * Math.PI * index / (length - 1f));
    }

    @Override
    public String toString() {
        return "Blackman Window";
    }
}
