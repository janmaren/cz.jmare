package cz.jmare.math.fft.window;

public class FlattopBinWindow implements BinWindow {
    public FlattopBinWindow() {
    }

    @Override
    public float getValue(int length, int index) {
        return (float) (0.21557895 - 0.41663158 * Math.cos(2 * Math.PI * index / (length - 1))
                + 0.277263158 * Math.cos(4 * Math.PI * index / (length - 1))
                - 0.083578947 * Math.cos(6 * Math.PI * index / (length - 1))
                + 0.006947368 * Math.cos(8 * Math.PI * index / (length - 1)));
    }

    @Override
    public String toString() {
        return "Flattop Window";
    }
}
