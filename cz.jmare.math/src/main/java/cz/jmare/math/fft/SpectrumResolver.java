package cz.jmare.math.fft;

import cz.jmare.math.fft.window.BinWindow;
import cz.jmare.math.fft.window.BlackmanBinWindow;

public class SpectrumResolver {
    protected BinWindow binWindow = new BlackmanBinWindow();

    private Integer maxPoints;

    private float[] values;

    private float sampleRate;

    public SpectrumResolver(float[] values, float sampleRate, Integer maxPoints) {
        this.values = values;
        this.sampleRate = sampleRate;
        this.maxPoints = maxPoints;
    }

    public SpectrumResolver(float[] values, float sampleRate) {
        this.values = values;
        this.sampleRate = sampleRate;
    }

    public Spectrum getSpectrum() {
        long size = values.length;

        if (maxPoints != null && maxPoints < size) {
            size = maxPoints;
        }

        int power = (int) (Math.log(size) / Math.log(2));
        int binsNumber = (int) Math.pow(2, power);
        float binFrequency = sampleRate / binsNumber;

        float[] resultSpectrum = calculateFftValues(values, binsNumber, binWindow);

        float rms = calculateRms(resultSpectrum);
        float binsSum = calculateSum(resultSpectrum);

        Spectrum spectrum = new Spectrum(resultSpectrum, binFrequency);
        spectrum.setBinWindow(binWindow);
        spectrum.setSampleFrequency(sampleRate);
        spectrum.setRms(rms);
        spectrum.setBinsSum(binsSum);

        return spectrum;
    }

    /**
     * Calculate FFT spectrum for curve sampled values. Number of result FFT values
     * is division 2 of input points
     * @param sampleValues which have to have the length twice larger than binsNumber
     * @param binsNumber result binsNumber
     * @param binWindow
     * @return
     */
    public static float[] calculateFftValues(float[] sampleValues, int binsNumber, BinWindow binWindow) {
        float[] resultSpectrum = new float[binsNumber / 2];

        Complex[] complexInputs = new Complex[binsNumber];

        int pointIndex = 0;
        for (int i = 0; i < binsNumber; i++) {
            float value = sampleValues[pointIndex++];
            float windowValue = binWindow.getValue(binsNumber, i);
            complexInputs[i] = new Complex(windowValue * value, 0);
        }

        Complex[] result = FFT.fft(complexInputs);

        for (int i = 0; i < result.length / 2; i++) {
            Complex complex1 = result[i];
            float value = complex1.abs();
            Complex complex2 = result[result.length - i - 1];
            float value2 = complex2.abs();
            resultSpectrum[i] = (value + value2) / 2;
        }
        return resultSpectrum;
    }

    public void setBinWindow(BinWindow binWindow) {
        this.binWindow = binWindow;
    }
    
    public static float calculateRms(float[] values) {
        if (values.length == 0) {
            throw new IllegalArgumentException("No input value in array provided");
        }
        float sum = 0;
        for (int i = 0; i < values.length; i++) {
            float value = values[i];
            sum += value * value;
        }
        return (float) Math.sqrt(sum / values.length);
    }

    public static float calculateSum(float[] values) {
        float sum = 0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum;
    }
}
