package cz.jmare.math.fft.window;

public class HammingBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return 0.54f - 0.46f * (float) Math.cos(TWO_PI * index / (length - 1));
    }

    @Override
    public String toString() {
        return "Hamming Window";
    }
}
