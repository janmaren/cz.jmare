package cz.jmare.math.fft.window;

public class HannBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return 0.5f * (1.0f - (float) Math.cos(TWO_PI * index / (length - 1.0f)));
    }

    @Override
    public String toString() {
        return "Hann Window";
    }
}
