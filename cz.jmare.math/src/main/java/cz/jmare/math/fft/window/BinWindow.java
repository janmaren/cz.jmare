package cz.jmare.math.fft.window;

public interface BinWindow {
    float TWO_PI = (float) (2 * Math.PI);

    float getValue(int length, int index);
}
