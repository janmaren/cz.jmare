package cz.jmare.math.fft.window;

public class TriangularBinWindow implements BinWindow {
    @Override
    public float getValue(int length, int index) {
        return 2.0f / length * (length / 2.0f - (float) Math.abs(index - (length - 1f) / 2.0f));
    }

    @Override
    public String toString() {
        return "Triangular Window";
    }
}
