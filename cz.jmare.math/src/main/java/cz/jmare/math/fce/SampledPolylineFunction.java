package cz.jmare.math.fce;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;
import cz.jmare.math.number.EpsCompare;

public class SampledPolylineFunction implements Function<Double, Double>, SamplingProvider {
    int index = -1;
    private List<Point> points;
    int j = 0;

    /**
     * Points where x value is acending (a sampled function)
     * @param points
     */
    public SampledPolylineFunction(List<Point> points) {
        this.points = points;
        Iterator<Point> iterator = points.iterator();
        if (iterator.hasNext()) {
            Point next = iterator.next();
            double lastX = next.x;
            while (iterator.hasNext()) {
                Point next2 = iterator.next();
                if (lastX < next2.x) {
                    lastX = next2.x;
                } else {
                    throw new IllegalArgumentException("X values are not ascending");
                }
            }
        }
    }

    @Override
    public Double apply(Double value) {
        if (points.size() == 0) {
            return Double.NaN;
        }
        while(j < points.size()) {
            double lastX = points.get(j).x;
            if (lastX >= value || EpsCompare.doubleGreaterOrEqualsThanDouble(lastX, value)) {
                if (j == 0) {
                    if (lastX > value && !EpsCompare.doubleLessOrEqualsThanDouble(lastX, value)) {
                        return Double.NaN;
                    }
                    j++;
                    continue;
                }
                Point point1 = points.get(j - 1);
                Point point2 = points.get(j);
                StraightLine straightLine = new StraightLine(point1.x, point1.y, point2.x, point2.y);
                return straightLine.yOnLine(value);
            }
            j++;
        }
        return Double.NaN;
    }

    public double getCurrentX() {
        if (index == -1) {
            throw new IllegalStateException("nextX() hasn't been called");
        }
        return points.get(index).x;
    }

    public double getCurrentY() {
        if (index == -1) {
            throw new IllegalStateException("nextX() hasn't been called");
        }
        return points.get(index).y;
    }

    @Override
    public Double nextX() {
        index++;
        if (index >= points.size()) {
            return null;
        }
        return points.get(index).x;
    }

    @Override
    public void reset() {
        index = -1;
    }

    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(1, 2), new Point(3, 4), new Point(5, 3));
        SampledPolylineFunction functionPointsAdapter = new SampledPolylineFunction(points);
        Double apply = functionPointsAdapter.apply(4.999999d);
        System.out.println(apply);
    }
}
