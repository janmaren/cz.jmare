package cz.jmare.math.fce;

import java.util.function.Function;

import cz.jmare.math.geometry.entity.LineSegmentDouble;
import cz.jmare.math.geometry.entity.Point;

public class LineSegmentFunction implements Function<Double, Double> {
    private double x1;

    private double y1;

    private double x2;

    private double y2;

    private double a;

    private double b;

    private double c;

    public LineSegmentFunction(double x1, double y1, double x2, double y2) {
        super();
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        init();
    }

    public LineSegmentFunction(LineSegmentDouble lineSegment) {
        super();
        this.x1 = lineSegment.x1;
        this.y1 = lineSegment.y1;
        this.x2 = lineSegment.x2;
        this.y2 = lineSegment.y2;
        init();
    }

    public LineSegmentFunction(Point point1, Point point2) {
        super();
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
        init();
    }

    private void init() {
        if (x1 > x2) {
            double tmpX = x1;
            double tmpY = y1;
            x1 = x2;
            y1 = y2;
            x2 = tmpX;
            y2 = tmpY;
        }
        a = y1 - y2;
        b = x2 - x1;
        c = (x1 - x2) * y1 + (y2 - y1) * x1;
    }

    @Override
    public Double apply(Double x) {
        if (x < x1) {
            return Double.NaN;
        }
        if (x > x2) {
            return Double.NaN;
        }

        return -(a * x + c) / b;
    }
}
