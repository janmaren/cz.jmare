package cz.jmare.math.fce;

import java.util.function.Function;

import cz.jmare.math.geometry.entity.StraightLine;

public class LineFunction implements Function<Double, Double>{
    private double a;

    private double b;

    private double c;

    public LineFunction(StraightLine line) {
        this.a = line.a;
        this.b = line.b;
        this.c = line.c;
    }

    public LineFunction(double a, double b, double c) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public Double apply(Double x) {
        return -(a * x + c) / b;
    }
}
