package cz.jmare.math.fce;

public class FastFuncs {
    public static double exp(double val) {
        final long tmp = (long) (1512775 * val + 1072632447);
        return Double.longBitsToDouble(tmp << 32);
    }

    public static double log(double x) {
        return 6 * (x - 1) / (x + 1 + 4 * (Math.sqrt(x)));
    }

    public static double pow(double a, double b) {
        long tmp = (long) (9076650 * (a - 1) / (a + 1 + 4 * (Math.sqrt(a))) * b + 1072632447);
        return Double.longBitsToDouble(tmp << 32);
    }


    public static void main(String[] args) {
        double exp = 0;
        long start1 = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            exp = exp(i);
        }
        long end1 = System.nanoTime();
        System.out.println(exp + ":" + (end1 - start1));

        long start2 = System.nanoTime();
        double exp2 = 0;
        for (int i = 0; i < 100; i++) {
            exp2 = Math.exp(i);
        }
        long end2 = System.nanoTime();
        System.out.println(exp2 + ":" + (end2 - start2));
    }
}
