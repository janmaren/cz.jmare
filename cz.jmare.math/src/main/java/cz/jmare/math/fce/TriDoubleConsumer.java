package cz.jmare.math.fce;

public interface TriDoubleConsumer {
    void accept(double d1, double d2, double d3);
}
