package cz.jmare.math.fce;

public class SimpleSamplingProvider implements SamplingProvider {

    private double min;
    private double period;
    private long currentIndex;
    private long toIndex;
    private double max;

    public SimpleSamplingProvider(double min, double max, double period) {
        this.min = min;
        this.max = max;
        this.period = period;
        toIndex = (long) ((max - min) / period) + 1;
    }

    @Override
    public Double nextX() {
        if (currentIndex < toIndex) {
            double x = min + period * currentIndex;
            currentIndex++;
            return x;
        }
        return null;
    }

    @Override
    public void reset() {
        this.currentIndex = 0;
    }

    double getMin() {
        return min;
    }

    void setMin(double min) {
        this.min = min;
    }

    double getPeriod() {
        return period;
    }

    void setPeriod(double period) {
        this.period = period;
    }

    double getMax() {
        return max;
    }

    void setMax(double max) {
        this.max = max;
    }
}
