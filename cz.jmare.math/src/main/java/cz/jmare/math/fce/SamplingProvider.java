package cz.jmare.math.fce;

public interface SamplingProvider {
    Double nextX();

    void reset();
}
