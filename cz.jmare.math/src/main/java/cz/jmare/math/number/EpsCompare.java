package cz.jmare.math.number;

/**
 * Compare double values with precision 1e-11
 * @author jan
 */
public class EpsCompare {
    public static float epsilonFloat = 1e-6f;

    public static double epsilonDouble = 1e-11;

    private static TolerantDoubleCompare tolerantCompare;
    static {
        tolerantCompare = new TolerantDoubleCompare(epsilonDouble);
    }

    public static boolean equals(float f1, float f2) {
        return f1 == f2 || f1 - epsilonFloat < f2 && f2 < f1 + epsilonFloat;
    }

    public static boolean equalsZero(float f1) {
        return f1 == 0 || f1 - epsilonFloat < 0.0f && 0.0f < f1 + epsilonFloat;
    }

    public static boolean equals(double d1, double d2) {
        return d1 == d2 || d1 - epsilonDouble < d2 && d2 < d1 + epsilonDouble;
    }

    public static boolean equalsZero(double d1) {
        return tolerantCompare.equalsZero(d1);
    }

    public static boolean doubleLessOrEqualsThanDouble(double d1, double d2) {
        return tolerantCompare.doubleLessOrEqualsThanDouble(d1, d2);
    }

    public static boolean doubleGreaterOrEqualsThanDouble(double d1, double d2) {
        return tolerantCompare.doubleGreaterOrEqualsThanDouble(d1, d2);
    }

    public static boolean doubleLessThanDouble(double d1, double d2) {
        return tolerantCompare.doubleLessThanDouble(d1, d2);
    }

    public static boolean doubleGreaterThanDouble(double d1, double d2) {
        return tolerantCompare.doubleGreaterThanDouble(d1, d2);
    }
}
