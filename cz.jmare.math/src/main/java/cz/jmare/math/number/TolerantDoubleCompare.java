package cz.jmare.math.number;

public class TolerantDoubleCompare {
    public double epsilonDouble;

    public TolerantDoubleCompare() {
        super();
    }

    public TolerantDoubleCompare(double epsilonDouble) {
        super();
        this.epsilonDouble = epsilonDouble;
    }

    public boolean doubleLessOrEqualsThanDouble(double d1, double d2) {
        return d1 - epsilonDouble <= d2;
    }

    public boolean doubleGreaterOrEqualsThanDouble(double d1, double d2) {
        return d1 + epsilonDouble >= d2;
    }

    public boolean doubleLessThanDouble(double d1, double d2) {
        return !doubleGreaterOrEqualsThanDouble(d1, d2);
    }

    public boolean doubleGreaterThanDouble(double d1, double d2) {
        return !doubleLessOrEqualsThanDouble(d1, d2);
    }

    public boolean equals(double d1, double d2) {
        return d1 == d2 || d1 - epsilonDouble < d2 && d2 < d1 + epsilonDouble;
    }

    public boolean equalsZero(double d1) {
        return d1 == 0d || d1 - epsilonDouble < 0.0d && 0.0d < d1 + epsilonDouble;
    }

    public static TolerantDoubleCompare createTolerantDoubleCompare(double maxComparedNumber) {
        double number = Double.longBitsToDouble(Double.doubleToLongBits(maxComparedNumber) + 1);
        double epsilon = number - maxComparedNumber;
        return new TolerantDoubleCompare(epsilon);
    }
}
