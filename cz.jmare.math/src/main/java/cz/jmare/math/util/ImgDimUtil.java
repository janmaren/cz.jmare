package cz.jmare.math.util;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

public class ImgDimUtil {

    public static double bestDividerForResize(int imageWidth, int imageHeight, int requiredWidth) {
        int from = (int) (requiredWidth * 0.9);
        int upTo = (int) (requiredWidth * 1.1);
        int bestIndex = from;
        double bestDiff = Double.MAX_VALUE;
        for (int i = from; i < upTo; i++) {
            double div = (double) imageWidth / i;

            double divw = imageWidth / div;
            double divh = imageHeight / div;

            int divwr = (int) divw;
            int divwh = (int) divh;

            double diff = Math.sqrt((divw - divwr) * (divw - divwr) + (divh - divwh) * (divh - divwh));
            if (diff < bestDiff) {
                bestIndex = i;
                bestDiff = diff;
            }
        }
        return (double) imageWidth / bestIndex;
    }

    public static Dimension getImageDimension(File imgFile) {
        int pos = imgFile.getName().lastIndexOf(".");
        if (pos == -1) {
            throw new IllegalArgumentException("No extension for file: " + imgFile.getAbsolutePath());
        }
        String suffix = imgFile.getName().substring(pos + 1);
        Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
        while (iter.hasNext()) {
            ImageReader reader = iter.next();
            try {
                ImageInputStream stream = new FileImageInputStream(imgFile);
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                return new Dimension(width, height);
            } catch (IOException e) {
                throw new IllegalArgumentException("Unable to read " + imgFile, e);
            } finally {
                reader.dispose();
            }
        }
        throw new IllegalArgumentException("Not a known image file: " + imgFile.getAbsolutePath());
    }

    public static void main(String[] args) throws IOException {
        getImageDimension(new File("c:\\Users\\marencik\\Pictures\\pleiades20161129large.jpg"));
        int imageWidth = 4196;
        int imageHeight = 2712;
        double div = bestDividerForResize(imageWidth, imageHeight, 300);
        System.out.println(imageWidth / div + "," + imageHeight / div);
    }
}
