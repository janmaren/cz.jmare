package cz.jmare.math.util;

public class EncodeRevertsUtil {
    // ok
    public static void shortToBytaRevert(short data, byte[] bytes, int offset) {
        bytes[offset + 1] = (byte) (data >> 8);
        bytes[offset] = (byte) data;
    }

    // ok
    public static void longToBytaRevert(long longBits, byte[] bytes, int offset, int numberOfLongBytesUse) {
        for (int i = 0, shift = 0; i < numberOfLongBytesUse; i++, shift += 8) {
            bytes[offset + i] = (byte) (longBits >> shift);
        }
    }

    // asi ok
    public static void intToBytaRevert(int intBits, byte[] bytes, int offset, int numberOfLongBytesUse) {
        for (int i = 0, shift = 0; i < numberOfLongBytesUse; i++, shift += 8) {
            bytes[offset + i] = (byte) (intBits >> shift);
        }
    }

    // ok
    public static void intToBytaRevert(int value, byte[] bytes, int offset) {
        bytes[offset + 3] = (byte) (value >> 24);
        bytes[offset + 2] = (byte) (value >> 16);
        bytes[offset + 1] = (byte) (value >> 8);
        bytes[offset] = (byte) (value);
    }

    // ok
    public static void floatToBytaRevert(float value, byte[] bytes, int offset) {
        int intBits =  Float.floatToIntBits(value);

        bytes[offset + 3] = (byte) (intBits >> 24);
        bytes[offset + 2] = (byte) (intBits >> 16);
        bytes[offset + 1] = (byte) (intBits >> 8);
        bytes[offset] = (byte) (intBits);
    }

    // asi ok
    public static byte[] uintToBytaRevert(long value, byte[] bytes, int offset) {
        bytes[offset] = (byte) value;
        bytes[offset + 1] = (byte) (value >>> 8);
        bytes[offset + 2] = (byte) (value >>> 16);
        bytes[offset + 3] = (byte) (value >>> 24);
        return bytes;
    }

    // asi ok
    public static void ushortToBytaRevert(int value, byte[] bytes, int offset) {
        bytes[offset] = (byte) value;
        bytes[1 + offset] = (byte) (value >>> 8);
    }

    // ok
    public static void doubleToBytaRevert(double value, byte[] bytes, int offset) {
        long longBits =  Double.doubleToLongBits(value);
        bytes[offset + 7] = (byte) (longBits >> 56);
        bytes[offset + 6] = (byte) (longBits >> 48);
        bytes[offset + 5] = (byte) (longBits >> 40);
        bytes[offset + 4] = (byte) (longBits >> 32);
        bytes[offset + 3] = (byte) (longBits >> 24);
        bytes[offset + 2] = (byte) (longBits >> 16);
        bytes[offset + 1] = (byte) (longBits >> 8);
        bytes[offset] = (byte) (longBits);
    }
}
