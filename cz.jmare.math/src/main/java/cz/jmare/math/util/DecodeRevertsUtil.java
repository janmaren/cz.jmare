package cz.jmare.math.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class DecodeRevertsUtil {

    public static long bytesToAbsoluteLongRevert(byte[] byteArray, int index, int count) {
        long d = 0;
        int shift = 0;
        for (int i = 0; i < count; i++, shift += 8) {
            d |= ((long) (byteArray[index + i] & 0xff) << shift);
        }
        return d;
    }

    public static float bytesToFloatRevert(byte[] byteArray, int index) {
        float f = ByteBuffer.wrap(byteArray, index, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        return f;
    }

    public static short bytesToShortRevert(byte[] byteArray, int index) {
        short sh = ByteBuffer.wrap(byteArray, index, 2).order(ByteOrder.LITTLE_ENDIAN).getShort();
        return sh;
    }

    public static int convertUByte(byte number) {
        return (int) number & 0xff;
    }

    public static int byteToUByte(byte[] byteArray, int index) {
        return (int) byteArray[index] & 0xff;
    }

    public static int bytesToIntRevert(byte[] byteArray, int index) {
        int val = ByteBuffer.wrap(byteArray, index, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
        return val;
    }

    public static int bytesToUnsignedShortRevert(byte[] byteArray, int index) {
        int output = 0;
        output |= convertUByte(byteArray[index + 1]) << 8;
        output |= convertUByte(byteArray[index]);
        return output;
    }

    public static int bytesToInt24Revert(byte[] byteArray, int index) {
        int output = 0;
        output = (int) (((int) byteArray[index + 2]) << 16);
        output |= convertUByte(byteArray[index + 1]) << 8;
        output |= convertUByte(byteArray[index]);
        return output;
    }

    public static double bytesToDoubleRevert(byte[] byteArray, int index) {
        double sh = ByteBuffer.wrap(byteArray, index, 8).order(ByteOrder.LITTLE_ENDIAN).getDouble();
        return sh;
    }

    public static int bytesToInt32Revert(byte[] byteArray, int index) {
        int output = 0;
        output = (int) (((int) byteArray[index + 3]) << 24);
        output |= convertUByte(byteArray[index + 2]) << 16;
        output |= convertUByte(byteArray[index + 1]) << 8;
        output |= convertUByte(byteArray[index]);
        return output;
    }

    public static long bytesToUnsignedInt32Revert(byte[] byteArray, int index) {
        long output = 0;
        output = convertUByte(byteArray[index + 3]) << 24;
        output |= convertUByte(byteArray[index + 2]) << 16;
        output |= convertUByte(byteArray[index + 1]) << 8;
        output |= convertUByte(byteArray[index]);
        return output;
    }
}
