package cz.jmare.math.util;

public class PiUtil {
    public static double calculatePi(int iterations) {
        double accum = 1.0;
        double c1 = 2.0;
        double c2 = 1.0;
        for (int i = 0; i < iterations; i++) {
            accum *= ((c1 * c1) / (c2 * (c2 + 2.0)));
            c1 += 2.0;
            c2 += 2.0;
        }
        return accum * 2.0;
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        double d = calculatePi(100000000);
        long end = System.currentTimeMillis();
        System.out.println("Time for pi " + d + ": " + (end - start));
        System.out.println("Orig pi: " + Math.PI);
    }

}
