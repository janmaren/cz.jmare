package cz.jmare.math.util;

public class EncodeUtil {
    // asi ok
    public static void shortToByta(short data, byte[] bytes, int offset) {
        bytes[offset] = (byte)((data >> 8) & 0xff);
        bytes[offset + 1] = (byte)((data >> 0) & 0xff);
    }

    // asi ok
    public static void longToByta(long longBits, byte[] bytes, int offset, int numberOfLongBytesUse) {
        for (int i = 0, shift = (numberOfLongBytesUse - 1) * 8; i < numberOfLongBytesUse; i++, shift -= 8) {
            bytes[offset + i] = (byte) (longBits >> shift);
        }
    }

    // asi ok
    public static void intToByta(int intBits, byte[] bytes, int offset, int numberOfLongBytesUse) {
        for (int i = 0, shift = (numberOfLongBytesUse - 1) * 8; i < numberOfLongBytesUse; i++, shift -= 8) {
            bytes[offset + i] = (byte) (intBits >> shift);
        }
    }

    // asi ok
    public static void floatToByta(float value, byte[] bytes, int offset) {
        int intBits =  Float.floatToIntBits(value);

        bytes[offset] = (byte) (intBits >> 24);
        bytes[offset + 1] = (byte) (intBits >> 16);
        bytes[offset + 2] = (byte) (intBits >> 8);
        bytes[offset + 3] = (byte) (intBits);
    }

    // asi ok
    public static void intToByta(int value, byte[] bytes, int offset) {
        bytes[offset] = (byte) (value >> 24);
        bytes[offset + 1] = (byte) (value >> 16);
        bytes[offset + 2] = (byte) (value >> 8);
        bytes[offset + 3] = (byte) (value);
    }

    // asi ok
    public static byte[] uintToByta(long value, byte[] bytes, int offset) {
        bytes[offset + 3] = (byte) value;
        bytes[offset + 2] = (byte) (value >>> 8);
        bytes[offset + 1] = (byte) (value >>> 16);
        bytes[offset] = (byte) (value >>> 24);
        return bytes;
    }

    // asi ok
    public static void ushortToByta(int value, byte[] bytes, int offset) {
        bytes[offset] = (byte) (value >>> 8);
        bytes[offset + 1] = (byte) value;
    }

    // asi ok
    public static void doubleToByta(double value, byte[] bytes, int offset) {
        long longBits =  Double.doubleToLongBits(value);
        bytes[offset] = (byte) (longBits >> 56);
        bytes[offset + 1] = (byte) (longBits >> 48);
        bytes[offset + 2] = (byte) (longBits >> 40);
        bytes[offset + 3] = (byte) (longBits >> 32);
        bytes[offset + 4] = (byte) (longBits >> 24);
        bytes[offset + 5] = (byte) (longBits >> 16);
        bytes[offset + 6] = (byte) (longBits >> 8);
        bytes[offset + 7] = (byte) (longBits);
    }
}
