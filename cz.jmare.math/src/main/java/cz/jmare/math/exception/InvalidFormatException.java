package cz.jmare.math.exception;

public class InvalidFormatException extends RuntimeException {
    private static final long serialVersionUID = 1533430711779695358L;

    public InvalidFormatException() {
        super();
    }

    public InvalidFormatException(String message) {
        super(message);
    }
}
