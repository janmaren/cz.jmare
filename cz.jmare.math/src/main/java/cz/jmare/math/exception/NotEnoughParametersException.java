package cz.jmare.math.exception;

public class NotEnoughParametersException extends RuntimeException {

    private static final long serialVersionUID = 5480830014230505114L;

    public NotEnoughParametersException() {
        super();
    }

    public NotEnoughParametersException(String message) {
        super(message);
    }
}
