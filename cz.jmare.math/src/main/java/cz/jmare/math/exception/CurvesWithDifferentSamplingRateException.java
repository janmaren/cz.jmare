package cz.jmare.math.exception;

/**
 * Used when impossible to do operation with multiple curves because they have different sampling rate (period or frequency)
 * @author jan
 *
 */
public class CurvesWithDifferentSamplingRateException extends RuntimeException {
    private static final long serialVersionUID = 134790748639182007L;

    public CurvesWithDifferentSamplingRateException() {
        super();
    }

    public CurvesWithDifferentSamplingRateException(String message) {
        super(message);
    }
}
