package cz.jmare.math.exception;

public class UnableToReadException extends RuntimeException {
    private static final long serialVersionUID = 1246758163348593346L;

    public UnableToReadException() {
        super();
    }

    public UnableToReadException(String message) {
        super(message);
    }

    public UnableToReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
