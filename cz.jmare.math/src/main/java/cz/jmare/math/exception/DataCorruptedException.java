package cz.jmare.math.exception;

public class DataCorruptedException extends RuntimeException {
    private static final long serialVersionUID = -7590034959024451265L;

    public DataCorruptedException() {
        super();
    }

    public DataCorruptedException(String message) {
        super(message);
    }
}
