package cz.jmare.math.exception;

public class NotAscendingXException extends RuntimeException {
    private static final long serialVersionUID = 207126272202237235L;

    public NotAscendingXException() {
        super();
    }

    public NotAscendingXException(String message) {
        super(message);
    }
}
