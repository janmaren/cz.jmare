package cz.jmare.math.exception;

public class MissingSamplingFrequencyException extends RuntimeException {
    private static final long serialVersionUID = -25343734920433571L;

    public MissingSamplingFrequencyException() {
        super();
    }

    public MissingSamplingFrequencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingSamplingFrequencyException(String message) {
        super(message);
    }

    public MissingSamplingFrequencyException(Throwable cause) {
        super(cause);
    }
}
