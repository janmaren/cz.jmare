package cz.jmare.math.exception;

public class UnableToWriteException extends RuntimeException {
    private static final long serialVersionUID = -8686436991508771281L;

    public UnableToWriteException() {
        super();
    }

    public UnableToWriteException(String message) {
        super(message);
    }

    public UnableToWriteException(String message, Throwable cause) {
        super(message, cause);
    }
}
