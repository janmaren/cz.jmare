package cz.jmare.math.exception;

/**
 * Used when impossible to do operation because there is a curve which is not normalized.
 * It usually means there is a Y value less than -1 or value greater than +1 but the threshold
 * infact may be different and they should be present in message.
 */
public class NotNormalizedException extends RuntimeException {
    private static final long serialVersionUID = 2017494207564567515L;

    public NotNormalizedException() {
        super();
    }

    public NotNormalizedException(String message) {
        super(message);
    }
}
