package cz.jmare.math.exception;

/**
 * Used when impossible to do operation with multiple curves because they have different number of points or they have different beginning (offset) or end
 * @author jan
 *
 */
public class CurvesPointsNotFitException extends RuntimeException {
    private static final long serialVersionUID = 7660316078198052200L;

    public CurvesPointsNotFitException() {
        super();
    }

    public CurvesPointsNotFitException(String message) {
        super(message);
    }
}
