package cz.jmare.math.exception;

public class NotSuccessfulOperationException extends RuntimeException {

    private static final long serialVersionUID = -1161052170224078411L;


    public NotSuccessfulOperationException(String message) {
        super(message);
    }

}
