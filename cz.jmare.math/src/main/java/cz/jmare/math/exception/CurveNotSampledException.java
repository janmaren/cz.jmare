package cz.jmare.math.exception;

/**
 * Used when impossible to do operation because at least one of curves is not sampled
 */
public class CurveNotSampledException extends RuntimeException {

    private static final long serialVersionUID = 7003400146791279527L;

    public CurveNotSampledException() {
        super();
    }

    public CurveNotSampledException(String message) {
        super(message);
    }
}
