package cz.jmare.math.exception;

/**
 * Used when impossible to do operation because there wouldn't be a curve with at least 2 points
 */
public class NotAtLeastTwoPointsException extends RuntimeException {
    private static final long serialVersionUID = -3200200198194243561L;

    public NotAtLeastTwoPointsException() {
        super();
    }

    public NotAtLeastTwoPointsException(String message) {
        super(message);
    }
}
