package cz.jmare.math.exception;

/**
 * Used when impossible to do operation with curve because the minX value isn't zero
 */
public class CurveNotStartsWithZeroException extends RuntimeException {

    private static final long serialVersionUID = 5407708650095734965L;

    public CurveNotStartsWithZeroException() {
        super();
    }

    public CurveNotStartsWithZeroException(String message) {
        super(message);
    }
}
