package cz.jmare.math.curve.operation;

import java.util.function.Supplier;

import cz.jmare.math.curve.CurveUtil;
import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.curve.util.CachedCurve;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class SmoothCurveOperations {
    public static CurveHolder smoothByMovingAverage(CurveHolder curveHolder, Supplier<double[]> weightsSupplier, ProgressStatus<Long> progressStatus) {
        return smoothByMovingAverage(curveHolder, weightsSupplier.get(), progressStatus);
    }

    public static CurveHolder smoothByMovingAverage(CurveHolder curveHolder, double[] weights, ProgressStatus<Long> progressStatus) {
        if (weights.length % 2 == 0) {
            throw new IllegalArgumentException("There must be an odd number of weights for centered moving average function");
        }
        int correctByNeighboursNumber = weights.length / 2;
        if (curveHolder.getCurveInfo().size < correctByNeighboursNumber) {
            throw new IllegalArgumentException("Too little points in curve to be smoothed, needed at least " + correctByNeighboursNumber + " points in curve");
        }

        try (CachedCurve cachedCurve = new CachedCurve(curveHolder, weights.length); SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(curveHolder)) {
            progressStatus.setMaxValue(cachedCurve.size());

            for (long i = 0; i < correctByNeighboursNumber; i++) {
                Point point = smoothPoint(cachedCurve, weights, (int) i, correctByNeighboursNumber, i);
                curveBuilder.add(point);
            }

            progressStatus.setProgressValue((long) (correctByNeighboursNumber - 1));

            for (long i = correctByNeighboursNumber; i < cachedCurve.size() - correctByNeighboursNumber; i++) {
                Point point = smoothPoint(cachedCurve, weights, correctByNeighboursNumber, correctByNeighboursNumber, i);
                curveBuilder.add(point);
                progressStatus.setProgressValue(i);
            }

            for (long i = cachedCurve.size() - correctByNeighboursNumber; i < cachedCurve.size(); i++) {
                Point point = smoothPoint(cachedCurve, weights, correctByNeighboursNumber, (int) (cachedCurve.size() - i - 1), i);
                curveBuilder.add(point);
            }

            progressStatus.setProgressValue(cachedCurve.size());

            return curveBuilder.build();
        }
    }

    private static Point smoothPoint(CachedCurve cachedCurve, double[] weights, int neighboursPrev, int neighboursNext, long i) {
        Point computePoint = cachedCurve.get(i);
        double ySum = 0.0;
        int weightIndex = weights.length / 2 - neighboursPrev;
        int sumWeights = 0;
        for (long j = i - neighboursPrev; j <= i + neighboursNext; j++) {
            Point point = cachedCurve.get(j);
            sumWeights += weights[weightIndex];
            ySum += point.y * weights[weightIndex++];
        }
        double average = ySum / sumWeights;
        Point point = new Point(computePoint.x, average);
        return point;
    }
}
