package cz.jmare.math.curve;

import cz.jmare.file.FriendlyAutoCloseable;
import cz.jmare.math.geometry.entity.Point;

/**
 * Return a point from collection<br>
 * The {@link #get(int)} must be called in ascending order in range 0 - size (excl.) and no index can be used repeatedly<br>
 * Using usually for very big data which don't fit to RAM but rather use disk as temporary storage
 */

public interface SequentialCurveReader extends FriendlyAutoCloseable {
    Point get(long index);

    long size();
}
