package cz.jmare.math.curve.util.traverser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.interpol.MonoSplineInterpolatedCurve;
import cz.jmare.math.curve.operation.BasicCurveOperations;
import cz.jmare.math.curve.util.CurveValidationUtil;
import cz.jmare.math.entity.TwoDoubles;
import cz.jmare.math.exception.CurveNotSampledException;
import cz.jmare.math.exception.CurvesPointsNotFitException;
import cz.jmare.math.exception.CurvesWithDifferentSamplingRateException;
import cz.jmare.math.fce.TriDoubleConsumer;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;
import cz.jmare.math.geometry.entity.TwoPoints;
import cz.jmare.math.number.EpsCompare;
import cz.jmare.progress.ProgressStatus;

public class TraverseOperations {

    public static void traverseTwoCurveHolders(CurveHolder solidInterpolatedCurve, CurveHolder iterateCurve, TriDoubleConsumer consumer) {
        TwoDoubles minMaxOpposite = BasicCurveOperations.getMinMaxXIntersect(Arrays.asList(solidInterpolatedCurve, iterateCurve));

        try (MonoSplineInterpolatedCurve monoSplineInterpolatedCurve = new MonoSplineInterpolatedCurve(solidInterpolatedCurve);
            SequentialCurveReader curveReader = iterateCurve.getCurveReader()) {
            CurveInfo curveInfo = iterateCurve.getCurveInfo();
            long size = curveInfo.size;
            for (long i = 0; i < size; i++) {
                Point point = curveReader.get(i);
                double x = point.x;
                if (x < minMaxOpposite.x1) {
                    continue;
                }
                if (x > minMaxOpposite.x2) {
                    break;
                }
                double y1 = point.y;
                double y2 = monoSplineInterpolatedCurve.getY(x);
                consumer.accept(x, y2, y1);
            }
        }
    }

    public static void crossHorizontalLineTraverser(CurveHolder curveHolder, BiFunction<Double, Boolean, Boolean> consumer, double yHorizontalCrossLine) {
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            Point point = curveReader.get(0);
            boolean lastIsUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
            boolean lastIsDown = !lastIsUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);
            if (!lastIsDown && !lastIsUp) {
                lastIsDown = true;
                lastIsUp = true;
            }
            Point lastPointUpOrDown = null;
            if (lastIsDown || lastIsUp) {
                lastPointUpOrDown = point;
            }
            boolean cont = true;
            for (int i = 1; i < curveReader.size(); i++) {
                point = curveReader.get(i);
                boolean isUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
                boolean isDown = !isUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);
                if (lastIsDown && isUp) {
                    cont = consumer.apply(TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine), true); // true is ascending
                } else if (lastIsUp && isDown) {
                    cont = consumer.apply(TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine), false); // false is descending
                }
                if (!cont) {
                    break;
                }
                if (isUp || isDown) {
                    lastPointUpOrDown = point;
                    lastIsDown = isDown;
                    lastIsUp = isUp;
                }
            }
        }
    }

    public static void crossHorizontalLineTraverserSmart(CurveHolder curveHolder,
            Function<Double, Boolean> consumer, double minPeriod, double yHorizontalCrossLine,
            boolean ascending) {
        Double crosspointX = null;
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            Point point = curveReader.get(0);
            boolean lastIsUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
            boolean lastIsDown = !lastIsUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);
            if (!lastIsDown && !lastIsUp) {
                lastIsDown = true;
                lastIsUp = true;
            }
            Point lastPointUpOrDown = null;
            if (lastIsDown || lastIsUp) {
                lastPointUpOrDown = point;
            }
            boolean cont = true;
            for (int i = 1; i < curveReader.size(); i++) {
                point = curveReader.get(i);

                if (crosspointX != null && point.x - crosspointX > minPeriod) {
                    double nowCrosspointX = TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine);
                    cont = consumer.apply(nowCrosspointX);
                    if (!cont) {
                        break;
                    }
                    crosspointX = null;
                }

                boolean isUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
                boolean isDown = !isUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);
                if (lastIsDown && isUp) {
                    if (ascending) {
                        crosspointX = TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine);
                    } else {
                        crosspointX = null;
                    }
                } else if (lastIsUp && isDown) {
                    if (!ascending) {
                        crosspointX = TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine);
                    } else {
                        crosspointX = null;
                    }
                }

                if (isUp || isDown) {
                    lastPointUpOrDown = point;
                    lastIsDown = isDown;
                    lastIsUp = isUp;
                }
            }
            if (crosspointX != null) {
                consumer.apply(crosspointX);
            }
        }
    }

    public static void crossHorizontalLineTraverserSmart(CurveHolder curveHolder,
            BiFunction<Double, Boolean, Boolean> consumer, double minPeriod, double yHorizontalCrossLine, ProgressStatus<Long> progressStatus) {
        Double crosspointX = null;
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            Point point = curveReader.get(0);
            boolean lastIsUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
            boolean lastIsDown = !lastIsUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);
            if (!lastIsDown && !lastIsUp) {
                lastIsDown = true;
                lastIsUp = true;
            }
            Point lastPointUpOrDown = null;
            if (lastIsDown || lastIsUp) {
                lastPointUpOrDown = point;
            }
            progressStatus.setMaxValue(curveReader.size());
            boolean cont = true;
            for (int i = 1; i < curveReader.size(); i++) {
                progressStatus.setProgressValue((long) i);
                point = curveReader.get(i);

                if (crosspointX != null && point.x - crosspointX > minPeriod) {
                    double nowCrosspointX = TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine);
                    if (Double.isNaN(nowCrosspointX) || !Double.isFinite(nowCrosspointX)) {
                        nowCrosspointX = point.x;
                    }
                    cont = consumer.apply(nowCrosspointX, lastIsUp);
                    if (!cont) {
                        return;
                    }
                    crosspointX = null;
                }

                boolean isUp = EpsCompare.doubleGreaterThanDouble(point.y, yHorizontalCrossLine);
                boolean isDown = !isUp && EpsCompare.doubleLessThanDouble(point.y, yHorizontalCrossLine);

                if (lastIsDown && isUp || lastIsUp && isDown) {
                    crosspointX = TraverseOperations.calculateCrosspointX(new TwoPoints(lastPointUpOrDown, point), yHorizontalCrossLine);
                }

                if (isUp || isDown) {
                    lastPointUpOrDown = point;
                    lastIsDown = isDown;
                    lastIsUp = isUp;
                }
            }
            if (crosspointX != null) {
                consumer.apply(crosspointX, lastIsUp);
            }
        }
    }

    public static double calculateCrosspointX(TwoPoints twoPoints, double yHorizontalCrossLine) {
        StraightLine straightLine = new StraightLine(twoPoints);
        return straightLine.xOnLine(yHorizontalCrossLine);
    }

    public static void crossXAxeTraverserSmart(CurveHolder curveHolder, Function<Double, Boolean> consumer, boolean ascending, double minPeriod) {
        crossHorizontalLineTraverserSmart(curveHolder, consumer, minPeriod, 0, ascending);
    }

    public static void crossXAxeTraverser(CurveHolder curveHolder, BiFunction<Double, Boolean, Boolean> consumer) {
        crossHorizontalLineTraverser(curveHolder, consumer, 0);
    }

    public static void forEachCurve(List<CurveHolder> curveHolders, Function<CurveHolder, CurveHolder> function) {
        for (int i = 0; i < curveHolders.size(); i++) {
            CurveHolder curveHolder = curveHolders.get(i);
            CurveHolder resultCurveHolder = function.apply(curveHolder);
            curveHolders.set(i, resultCurveHolder);
        }
    }

    /**
     * Accept all samples of curve
     * @param curveHolder curve to process
     * @param biConsumer x and y coordinates of all samples to accept
     */
    public static void forEachSample(CurveHolder curveHolder, BiConsumer<Double, Double> biConsumer) {
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            for (long i = 0; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                biConsumer.accept(point.x, point.y);
            }
        }
    }

    /**
     * Calculate a sample in all curves together. BiConsumers accepts x coordinate and list of y coordinates for each sample.
     * All curves must be sampled and have the same x coordinates otherwise exception is throwed
     * @param curveHolders curves to traverse
     * @param biConsumer consumer accepting each sample of all curves
     * @throws CurveNotSampledException
     * @throws CurvesPointsNotFitException
     * @throws CurvesWithDifferentSamplingRateException
     */
    public static void forEachSample(List<CurveHolder> curveHolders, BiConsumer<Double, List<Double>> biConsumer)
            throws CurveNotSampledException, CurvesPointsNotFitException, CurvesWithDifferentSamplingRateException {
        CurveValidationUtil.validationOverlapping(curveHolders);
        List<SequentialCurveReader> readers = new ArrayList<>();
        for (int i = 0; i < curveHolders.size(); i++) {
            readers.add(curveHolders.get(i).getCurveReader());
        }
        try {
            long size = readers.get(0).size();
            for (long i = 0; i < size; i++) {
                double x = 0;
                ArrayList<Double> ypsilons = new ArrayList<>();
                for (int j = 0; j < readers.size(); j++) {
                    SequentialCurveReader sequentialCurveReader = readers.get(j);
                    Point point = sequentialCurveReader.get(j);
                    if (j == 0) {
                        x = point.x;
                    }
                    ypsilons.add(point.y);
                }
                biConsumer.accept(x, ypsilons);
            }
        } finally {
            for (int i = 0; i < readers.size(); i++) {
                readers.get(i).close();
            }
        }
    }
}
