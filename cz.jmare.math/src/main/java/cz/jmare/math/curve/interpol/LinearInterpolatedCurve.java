package cz.jmare.math.curve.interpol;

import java.util.function.Function;

import cz.jmare.file.FriendlyAutoCloseable;
import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.sampled.BigSampledCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;
import cz.jmare.math.number.EpsCompare;

public class LinearInterpolatedCurve implements FriendlyAutoCloseable, InterpolatedCurve, Function<Double, Double> {
    private SequentialCurveReader curveReader;

    // Index to be read
    int index = 0;

    private Point prevPoint;

    private Point currPoint;

    private double maxX;

    private double minX;

    public LinearInterpolatedCurve(CurveHolder curveHolder) {
        super();
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        minX = curveInfo.minX;
        maxX = curveInfo.maxX;
        this.curveReader = curveHolder.getCurveReader();
        if (curveReader.size() < 2) {
            curveReader.close();
            throw new IllegalArgumentException("Number of points less than 2");
        }
    }

    @Override
    public double getY(double x) {
        if (index == 0) {
            prevPoint = curveReader.get(index++);
            currPoint = curveReader.get(index++);
            double lastX = prevPoint.x;
            if (EpsCompare.equals(lastX, x)) {
                return prevPoint.y;
            } else if (lastX > x) {
                throw new IllegalArgumentException("Required value for x=" + x + " but LinearInterpolatedCurve contains first value x[0]=" + lastX);
            }
        }

        while (index < size()) {
            if (x > currPoint.x) {
                prevPoint = currPoint;
                currPoint = curveReader.get(index++);
            } else {
                StraightLine straightLine = new StraightLine(prevPoint.x, prevPoint.y, currPoint.x, currPoint.y);
                return straightLine.yOnLine(x);
            }
        }
        if (x < currPoint.x) {
            StraightLine straightLine = new StraightLine(prevPoint.x, prevPoint.y, currPoint.x, currPoint.y);
            return straightLine.yOnLine(x);
        } else
        if (EpsCompare.equals(currPoint.x, x)) {
            return currPoint.y;
        }

        throw new IllegalArgumentException("Required value for x=" + x + " but LinearInterpolatedCurve contains last value x[" + (index - 1) +"]=" + currPoint.x);
    }

    public long size() {
        return curveReader.size();
    }


    @Override
    public void close() {
        curveReader.close();
    }

    @Override
    public double getMinX() {
        return minX;
    }

    @Override
    public double getMaxX() {
        return maxX;
    }

    @Override
    public Double apply(Double x) {
        return getY(x);
    }

    public static void main(String[] args) {
        BigSampledCurveHolder bigSampledCurveHolder = new BigSampledCurveHolder("/home/jan/git/moval/laborator/etc/data/points-3-sampled.wmt");
        try (SequentialCurveReader curveReader = bigSampledCurveHolder.getCurveReader()) {
            Point point0 = curveReader.get(0);
            Point point1 = curveReader.get(1);
            Point point2 = curveReader.get(2);
            System.out.println(point0);
            System.out.println(point1);
            System.out.println(point2);
        }

        try (
            LinearInterpolatedCurve linearInterpolatedCurve = new LinearInterpolatedCurve(bigSampledCurveHolder)) {
            Double y = linearInterpolatedCurve.getY(1.9);
            System.out.println(y);
        }
    }
}

