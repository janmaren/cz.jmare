package cz.jmare.math.curve.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.CurveUtil;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.importexport.TxtCurveYLoader;
import cz.jmare.math.curve.interpol.MonoSplineInterpolatedCurve;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.curve.util.CachedCurve;
import cz.jmare.math.curve.util.CurveValidationUtil;
import cz.jmare.math.curve.util.traverser.FirstTraverseConsumer;
import cz.jmare.math.curve.util.traverser.TraverseOperations;
import cz.jmare.math.entity.TwoDoubles;
import cz.jmare.math.exception.CurveNotSampledException;
import cz.jmare.math.exception.CurvesWithDifferentSamplingRateException;
import cz.jmare.math.exception.NotAtLeastTwoPointsException;
import cz.jmare.math.exception.NotSuccessfulOperationException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.geometry.entity.StraightLine;
import cz.jmare.math.geometry.relation.IntersectionUtil;
import cz.jmare.math.number.EpsCompare;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class BasicCurveOperations {
    public static TwoDoubles getMinMaxXSurround(List<CurveHolder> curveHolders) {
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;
        for (CurveHolder curveHolder : curveHolders) {
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            double maxValue = curveInfo.maxX;
            double minValue = curveInfo.minX;
            if (minValue < min) {
                min = minValue;
            }
            if (maxValue > max) {
                max = maxValue;
            }
        }
        return new TwoDoubles(min, max);
    }

    public static TwoDoubles getMinMaxYSurround(List<CurveHolder> curveHolders) {
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;
        for (CurveHolder curveHolder : curveHolders) {
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            double maxValue = curveInfo.maxY;
            double minValue = curveInfo.minY;
            if (minValue < min) {
                min = minValue;
            }
            if (maxValue > max) {
                max = maxValue;
            }
        }
        return new TwoDoubles(min, max);
    }

    public static double amplitude(TwoDoubles twoDoubles) {
        return (twoDoubles.x2 - twoDoubles.x1) / 2;
    }

    public static double amplitude(CurveHolder curveHolder) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        return (curveInfo.maxY - curveInfo.minY) / 2;
    }

    /**
     * Get factor number to be used to get requiredMaxAmplitude
     * @param curveHolder
     * @param requiredMaxAmplitude
     * @return
     * @throws NotSuccessfulOperationException
     */
    public static double amplitudeFitFactor(List<CurveHolder> curveHolder, double requiredMaxAmplitude) throws NotSuccessfulOperationException {
        Double max = curveHolder.stream().mapToDouble(ch -> ch.getCurveInfo().maxY).max().getAsDouble();
        Double min = curveHolder.stream().mapToDouble(ch -> ch.getCurveInfo().minY).min().getAsDouble();
        if (min > 0 && max > 0 || min < 0 && max < 0) {
            throw new NotSuccessfulOperationException("Unable to determine amplitude factor");
        }
        double factor = requiredMaxAmplitude / Math.max(Math.abs(max), Math.abs(min));
        return factor;
    }

    /**
     * Get factor number to be used to get requiredMaxAmplitude
     * @param curveHolder
     * @param requiredMaxAmplitude
     * @return
     * @throws NotSuccessfulOperationException
     */
    public static double amplitudeFitFactor(CurveHolder curveHolder, double requiredMaxAmplitude) throws NotSuccessfulOperationException {
        Double max = curveHolder.getCurveInfo().maxY;
        Double min = curveHolder.getCurveInfo().minY;
        if (min > 0 && max > 0 || min < 0 && max < 0) {
            throw new NotSuccessfulOperationException("Unable to determine amplitude factor");
        }
        double factor = requiredMaxAmplitude / Math.max(Math.abs(max), Math.abs(min));
        return factor;
    }

    /**
     * Get factor number to be used to get requiredMaxAmplitude
     * @param curveHolder
     * @param requiredMaxAmplitude
     * @return
     * @throws NotSuccessfulOperationException
     */
    public static double amplitudeFitFactor(List<CurveHolder> curveHolder, double minAllowedValue, double maxAllowedValue) throws NotSuccessfulOperationException {
        Double max = curveHolder.stream().mapToDouble(ch -> ch.getCurveInfo().maxY).max().getAsDouble();
        Double min = curveHolder.stream().mapToDouble(ch -> ch.getCurveInfo().minY).min().getAsDouble();
        if (min > 0 && max > 0 || min < 0 && max < 0) {
            throw new NotSuccessfulOperationException("Unable to determine amplitude factor");
        }
        if (Math.abs(min / minAllowedValue) > Math.abs(max / maxAllowedValue)) {
            return minAllowedValue / min;
        } else {
            return maxAllowedValue / max;
        }
    }

    /**
     * Get factor number to be used to get requiredMaxAmplitude
     * @param twoDoubles
     * @param requiredMaxAmplitude
     * @return
     * @throws NotSuccessfulOperationException
     */
    public static double amplitudeFitFactor(TwoDoubles twoDoubles, double requiredMaxAmplitude) throws NotSuccessfulOperationException {
        if (twoDoubles.x1 > 0 && twoDoubles.x2 > 0 || twoDoubles.x1 < 0 && twoDoubles.x2 < 0) {
            throw new NotSuccessfulOperationException("Unable to determine amplitude factor");
        }
        double factor = requiredMaxAmplitude / Math.max(Math.abs(twoDoubles.x1), Math.abs(twoDoubles.x2));
        return factor;
    }

    public static double halfOfPeaks(TwoDoubles twoDoubles) {
        return (twoDoubles.x2 + twoDoubles.x1) / 2;
    }

    public static double halfOfPeaks(CurveHolder curveHolder) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        return (curveInfo.maxY + curveInfo.minY) / 2;
    }

    public static TwoDoubles getMinMaxXIntersect(List<CurveHolder> curveHolders) {
        double min = -Double.MAX_VALUE;
        double max = Double.MAX_VALUE;
        for (CurveHolder curveHolder : curveHolders) {
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            double maxValue = curveInfo.maxX;
            double minValue = curveInfo.minX;
            if (minValue > min) {
                min = minValue;
            }
            if (maxValue < max) {
                max = maxValue;
            }
        }
        return new TwoDoubles(min, max);
    }


    public static CurveHolder recalculatePointsX(CurveHolder curveHolder, Function<Double, Double> xFunction, ProgressStatus<Long> progressStatus) {
        return recalculatePointsXY(curveHolder, xFunction, y -> y, progressStatus);
    }

    public static CurveHolder recalculatePointsY(CurveHolder curveHolder, Function<Double, Double> yFunction, ProgressStatus<Long> progressStatus) {
        return recalculatePointsXY(curveHolder, x -> x, yFunction, progressStatus);
    }

    public static CurveHolder recalculatePointsXY(CurveHolder curveHolder, Function<Double, Double> xFunction, Function<Double, Double> yFunction, ProgressStatus<Long> progressStatus) {
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader();
                SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(curveHolder)) {
            progressStatus.setMaxValue(curveReader.size());
            for (long i = 0; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                curveBuilder.add(new Point(xFunction.apply(point.x), yFunction.apply(point.y)));
                progressStatus.setProgressValue(i);
            }
            return curveBuilder.build();
        }
    }

    public static CurveHolder cutPointsLeftRightExact(CurveHolder origCurve, Double cutLessThanX, Double cutGreaterThanX, ProgressStatus<Long> progressStatus) {
        Function<Double, Boolean> cutMinFunction = (x) -> cutLessThanX != null && x < cutLessThanX;
        Function<Double, Boolean> cutMaxFunction = (x) -> cutGreaterThanX != null && x > cutGreaterThanX;
        return cutPointsLeftRight(origCurve, cutMinFunction, cutMaxFunction, progressStatus);
    }

    public static CurveHolder cutPointsLeftRight(CurveHolder origCurve, Function<Double, Boolean> cutMinFunction, Function<Double, Boolean> cutMaxFunction, ProgressStatus<Long> progressStatus) {
        CurveInfo curveInfo = origCurve.getCurveInfo();
        progressStatus.setMaxValue(curveInfo.size);
        try (SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(origCurve); SequentialCurveReader curveReader = origCurve.getCurveReader()) {
            for (long i = 0; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                if (cutMinFunction.apply(point.x)) {
                    continue;
                }
                if (cutMaxFunction.apply(point.x)) {
                    break;
                }
                curveBuilder.add(point);
                progressStatus.setProgressValue(i);
            }
            CurveHolder build = curveBuilder.build();
            if (build.getCurveInfo().size < 2) {
                throw new NotSuccessfulOperationException("Not at least 2 points would rest after operation");
            }
            return build;
        }
    }

    public static CurveHolder fillPointsLeftRight(CurveHolder origCurve, double populateFromX, double populateToX) {
        CurveInfo curveInfo = origCurve.getCurveInfo();
        if (EpsCompare.doubleLessThanDouble(curveInfo.minX, populateFromX)) {
            throw new IllegalArgumentException("Unable to fill - the lowest point x is " + curveInfo.minX + " which is less than " + populateFromX);
        }
        if (EpsCompare.doubleLessThanDouble(populateToX, curveInfo.maxX)) {
            throw new IllegalArgumentException("Unable to fill - required to fill up to " + populateToX + " but it is less than max x point " + curveInfo.maxX);
        }
        if (origCurve instanceof SampledCurveHolder) {
            SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) origCurve;
            return fillSampledPointsLeftRight(origCurve, populateFromX, populateToX, sampledCurveHolder.getCurveInfo().samplingPeriod);
        } else {
            return fillRawPointsLeftRight(origCurve, populateFromX, populateToX);
        }
    }

    private static CurveHolder fillRawPointsLeftRight(CurveHolder origCurve, double populateFromX,
            double populateToX) {
        try (CachedCurve cachedCurve = new CachedCurve(origCurve, 5)) {
            double minX = cachedCurve.get(0).x;
            double diff01 = cachedCurve.get(1).x - minX;
            boolean populatePrefix = EpsCompare.doubleLessThanDouble(populateFromX, minX);
            double maxX = cachedCurve.get(cachedCurve.size() - 1).x;
            double diffN = cachedCurve.get(cachedCurve.size() - 2).x - maxX;
            boolean populateSuffix = EpsCompare.doubleLessThanDouble(cachedCurve.size() - 1, populateToX);
            try (SequentialCurveBuilder curveBuilder = new BigRawCurveBuilder(); SequentialCurveReader curveReader = origCurve.getCurveReader()) {
                if (populatePrefix) {
                    if (minX - diff01 > populateFromX) {
                        curveBuilder.add(new Point(populateFromX, 0));
                        curveBuilder.add(new Point(minX - diff01, 0));
                    } else {
                        curveBuilder.add(new Point(populateFromX, 0));
                    }
                }
                for (long i = 0; i < curveReader.size(); i++) {
                    Point point = curveReader.get(i);
                    curveBuilder.add(point);
                }
                if (populateSuffix) {
                    if (maxX + diffN < populateToX) {
                        curveBuilder.add(new Point(maxX + diffN, 0));
                        curveBuilder.add(new Point(populateToX, 0));
                    } else {
                        curveBuilder.add(new Point(populateToX, 0));
                    }
                }
                return curveBuilder.build();
            }
        }
    }

    private static CurveHolder fillSampledPointsLeftRight(CurveHolder origCurve, double populateFromX,
            double populateToX, Double samplingPeriod) {
        CurveInfo curveInfo = origCurve.getCurveInfo();
        double maxX = curveInfo.maxX;
        double prefixRange = curveInfo.minX - populateFromX;
        long addPrefixCount = Math.round(prefixRange / samplingPeriod);
        double newMinX = curveInfo.minX - addPrefixCount * samplingPeriod;
        try (SequentialCurveBuilder curveBuilder = new BigSampledCurveBuilder(samplingPeriod); SequentialCurveReader curveReader = origCurve.getCurveReader()) {
            for (long i = 0; i < addPrefixCount; i++) {
                curveBuilder.add(new Point(newMinX + i * samplingPeriod, 0));
            }
            for (long i = 0; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                curveBuilder.add(point);
            }
            double suffixRange = populateToX - curveInfo.maxX;
            long addSuffixCount = Math.round(suffixRange / samplingPeriod);
            for (long i = 0; i < addSuffixCount; i++) {
                curveBuilder.add(new Point(maxX + (i + 1) * samplingPeriod, 0));
            }
            return curveBuilder.build();
        }
    }

    /**
     * Return negative delta value when grid is left or positive value when grid is right or zero when no shift present
     * @param curveHolder
     * @return
     */
    public static double shiftToSamplingGridNearest(SampledCurveHolder curveHolder) {
        double minX = curveHolder.getCurveInfo().minX;
        double samplingPeriod = curveHolder.getCurveInfo().samplingPeriod;
        return shiftToSamplingGridNearest(minX, samplingPeriod);
    }

    /**
     * Return negative delta value when grid is left or positive value when grid is right or zero when no shift present
     * @param curveHolder
     * @return
     */
    public static double shiftToSamplingGridNearest(double x, double samplingPeriod) {
        long i = (long) (x / samplingPeriod);
        if (x > 0) {
            double diffLeft = x - (i * samplingPeriod);
            double diffRight = ((i + 1) * samplingPeriod) - x;
            if (Math.abs(diffLeft) < Math.abs(diffRight)) {
                if (diffLeft == 0) {
                    return 0;
                }
                return -diffLeft;
            } else {
                return diffRight;
            }
        } else {
            double diffRight = x - (i * samplingPeriod);
            double diffLeft = ((i - 1) * samplingPeriod) - x;
            if (Math.abs(diffLeft) < Math.abs(diffRight)) {
                if (diffLeft == 0) {
                    return 0;
                }
                return diffLeft;
            } else {
                return -diffRight;
            }
        }
    }

    public static List<CurveHolder> splitCurve(CurveHolder curveHolder, double xSplit) {
        return splitCurve(curveHolder, xSplit, new NullLongProgressStatus());
    }

    public static List<CurveHolder> splitCurve(CurveHolder origCurve, double xSplit, ProgressStatus<Long> progressStatus) {
        CurveInfo curveInfo = origCurve.getCurveInfo();
        progressStatus.setMaxValue(curveInfo.size);
        try (SequentialCurveBuilder curveBuilder1 = CurveUtil.createNewBigCurveBuilder(origCurve); SequentialCurveBuilder curveBuilder2 = CurveUtil.createNewBigCurveBuilder(origCurve); SequentialCurveReader curveReader = origCurve.getCurveReader()) {
            long i = 0;
            Point lastPoint = null;
            for (; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                if (point.x < xSplit) {
                    curveBuilder1.add(point);
                } else {
                    if (origCurve instanceof SampledCurveHolder) {
                        if (lastPoint != null) {
                            double diffPrev = xSplit - lastPoint.x;
                            double diffNext = point.x - xSplit;
                            if (diffPrev < diffNext) {
                                curveBuilder2.add(lastPoint);
                                curveBuilder2.add(point);
                            } else {
                                curveBuilder1.add(point);
                                curveBuilder2.add(point);
                            }
                        }
                    } else {
                        Point intersection = null;
                        if (EpsCompare.equals(point.x, xSplit)) {
                            intersection = point;
                        } else {
                            intersection = IntersectionUtil.straightLinesIntersection(new StraightLine(lastPoint.x, lastPoint.y, point.x, point.y), StraightLine.createYParallel(xSplit));
                        }
                        curveBuilder1.add(intersection);
                        curveBuilder2.add(intersection);
                        curveBuilder2.add(point);
                    }
                    i++;
                    break;
                }
                lastPoint = point;
                progressStatus.setProgressValue(i);
            }
            for (; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                curveBuilder2.add(point);
            }
            CurveHolder build1 = curveBuilder1.build();
            if (build1.getCurveInfo().size < 2) {
                throw new NotAtLeastTwoPointsException("Not at least 2 points would rest after operation in curve 1");
            }
            CurveHolder build2 = curveBuilder2.build();
            if (build2.getCurveInfo().size < 2) {
                throw new NotAtLeastTwoPointsException("Not at least 2 points would rest after operation in curve 2");
            }

            return CurveUtil.of(build1, build2);
        }
    }

    public static CurveHolder appendRawCurve(CurveHolder baseCurveHolder, CurveHolder appendCurveHolder) {
        return appendRawCurve(baseCurveHolder, appendCurveHolder, new NullLongProgressStatus());
    }

    public static CurveHolder appendRawCurve(CurveHolder baseCurveHolder, CurveHolder appendCurveHolder, ProgressStatus<Long> progressStatus) {
        double offset = baseCurveHolder.getCurveInfo().maxX - appendCurveHolder.getCurveInfo().minX;

        progressStatus.setMaxValue(baseCurveHolder.getCurveInfo().size + appendCurveHolder.getCurveInfo().size);
        try (SequentialCurveBuilder curveBuilder = new BigRawCurveBuilder(); SequentialCurveReader baseCurveReader = baseCurveHolder.getCurveReader(); SequentialCurveReader appendCurveReader = appendCurveHolder.getCurveReader()) {
            for (long i = 0; i < baseCurveReader.size() - 1; i++) {
                Point point = baseCurveReader.get(i);
                curveBuilder.add(point);
                progressStatus.setProgressValue(i);
            }

            Point point1 = baseCurveReader.get(baseCurveReader.size() - 1);
            Point point2 = appendCurveReader.get(0);

            Point joinPoint = new Point(point1.x, (point1.y + point2.y) / 2.0);
            curveBuilder.add(joinPoint);

            long pos = baseCurveReader.size();

            for (long i = 1; i < appendCurveReader.size(); i++) {
                Point point = appendCurveReader.get(i);
                Point pointCorrected = new Point(point.x + offset, point.y);
                curveBuilder.add(pointCorrected);
                progressStatus.setProgressValue(pos++);
            }

            return curveBuilder.build();
        }
    }

    public static CurveHolder appendSampledCurve(CurveHolder baseCurveHolder, CurveHolder appendCurveHolder) throws CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        return appendSampledCurve(baseCurveHolder, appendCurveHolder, new NullLongProgressStatus());
    }

    public static CurveHolder appendSampledCurve(CurveHolder baseCurveHolder, CurveHolder appendCurveHolder, ProgressStatus<Long> progressStatus) throws CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        CurveValidationUtil.validationSampledSameRate(CurveUtil.of(baseCurveHolder, appendCurveHolder));

        double offset = baseCurveHolder.getCurveInfo().maxX - appendCurveHolder.getCurveInfo().minX;

        progressStatus.setMaxValue(baseCurveHolder.getCurveInfo().size + appendCurveHolder.getCurveInfo().size);
        try (SequentialCurveBuilder curveBuilder = new BigSampledCurveBuilder(); SequentialCurveReader baseCurveReader = baseCurveHolder.getCurveReader(); SequentialCurveReader appendCurveReader = appendCurveHolder.getCurveReader()) {
            for (long i = 0; i < baseCurveReader.size() - 1; i++) {
                Point point = baseCurveReader.get(i);
                curveBuilder.add(point);
                progressStatus.setProgressValue(i);
            }

            Point point1 = baseCurveReader.get(baseCurveReader.size() - 1);
            Point point2 = appendCurveReader.get(0);

            Point joinPoint = new Point(point1.x, (point1.y + point2.y) / 2.0);
            curveBuilder.add(joinPoint);

            long pos = baseCurveReader.size();

            for (long i = 1; i < appendCurveReader.size(); i++) {
                Point point = appendCurveReader.get(i);
                Point pointCorrected = new Point(point.x + offset, point.y);
                curveBuilder.add(pointCorrected);
                progressStatus.setProgressValue(pos++);
            }

            return curveBuilder.build();
        }
    }

    public static CurveHolder aritCurve(CurveHolder curveHolder1, CurveHolder curveHolder2, BiFunction<Double, Double, Double> aritFunction, Strategy strategy, ProgressStatus<Long> progressStatus) throws CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        progressStatus.setMaxValue(curveHolder1.getCurveInfo().size + curveHolder2.getCurveInfo().size);
        try (SequentialCurveReader curveReader1 = curveHolder1.getCurveReader();
                SequentialCurveReader curveReader2 = curveHolder2.getCurveReader();
                MonoSplineInterpolatedCurve interpolatedCurve1 = new MonoSplineInterpolatedCurve(curveHolder1);
                MonoSplineInterpolatedCurve interpolatedCurve2 = new MonoSplineInterpolatedCurve(curveHolder2);
                BigRawCurveBuilder bigRawCurveBuilder = new BigRawCurveBuilder()) {
            long i1 = 0;
            long i2 = 0;
            Point point1 = curveReader1.get(i1++);
            Point point2 = curveReader2.get(i2++);
            while (point1.x != Double.MAX_VALUE || point2.x != Double.MAX_VALUE) {
                while (point1.x < point2.x) {
                    progressStatus.setProgressValue(i1 + i2);
                    double y2 = 0;
                    if (curveHolder2.getCurveInfo().minX <= point1.x && point1.x <= curveHolder2.getCurveInfo().maxX) {
                        y2 = interpolatedCurve2.getY(point1.x);
                    } else {
                        if (strategy == Strategy.MISSING_POINT_EXPUNGE) {
                            if (i1 < curveHolder1.getCurveInfo().size) {
                                point1 = curveReader1.get(i1++);
                            } else {
                                point1 = new Point(Double.MAX_VALUE, 0);
                            }
                            continue;
                        }
                    }
                    bigRawCurveBuilder.add(new Point(point1.x, aritFunction.apply(point1.y, y2)));
                    if (i1 < curveHolder1.getCurveInfo().size) {
                        point1 = curveReader1.get(i1++);
                    } else {
                        point1 = new Point(Double.MAX_VALUE, 0);
                    }
                }
                while (point1.x > point2.x) {
                    progressStatus.setProgressValue(i1 + i2);
                    double y1 = 0;
                    if (curveHolder1.getCurveInfo().minX <= point2.x && point2.x <= curveHolder1.getCurveInfo().maxX) {
                        y1 = interpolatedCurve1.getY(point2.x);
                    } else {
                        if (strategy == Strategy.MISSING_POINT_EXPUNGE) {
                            if (i2 < curveHolder2.getCurveInfo().size) {
                                point2 = curveReader2.get(i2++);
                            } else {
                                point2 = new Point(Double.MAX_VALUE, 0);
                            }
                            continue;
                        }
                    }
                    bigRawCurveBuilder.add(new Point(point2.x, aritFunction.apply(point2.y, y1)));
                    if (i2 < curveHolder2.getCurveInfo().size) {
                        point2 = curveReader2.get(i2++);
                    } else {
                        point2 = new Point(Double.MAX_VALUE, 0);
                    }
                }
                while (point1.x == point2.x && (point1.x != Double.MAX_VALUE)) {
                    progressStatus.setProgressValue(i1 + i2);
                    bigRawCurveBuilder.add(new Point(point1.x, aritFunction.apply(point1.y, point2.y)));
                    if (i1 < curveHolder1.getCurveInfo().size) {
                        point1 = curveReader1.get(i1++);
                    } else {
                        point1 = new Point(Double.MAX_VALUE, 0);
                    }
                    if (i2 < curveHolder2.getCurveInfo().size) {
                        point2 = curveReader2.get(i2++);
                    } else {
                        point2 = new Point(Double.MAX_VALUE, 0);
                    }
                }
            }
            return bigRawCurveBuilder.build();
        }
    }

    public static Double calculateLeastDiffAmpFactor(CurveHolder fixedCurveHolder, CurveHolder processCurveHolder, double precision, ProgressStatus<Long> progressStatus) {
            double max1 = fixedCurveHolder.getCurveInfo().maxY;
            double max2 = processCurveHolder.getCurveInfo().maxY;
            if (max1 < 0 || max2 < 0) {
                return null;
            }
            double facFrom = 1;
            double facTo = 1;

            int compareRms = compareRms(fixedCurveHolder, processCurveHolder, 1);
            if (compareRms < 0) {
                facFrom *= 0.6666;
                while (compareRms(fixedCurveHolder, processCurveHolder, facFrom) < 0) {
                    facFrom *= 0.6666;
                }
                facTo = 1.1;
            } else {
                facTo *= 1.6666;
                while (compareRms(fixedCurveHolder, processCurveHolder, facTo) > 0) {
                    facTo *= 1.6666;
                }
                facFrom = 0.9;
            }

            double log10 = Math.log10(precision);
            int cycles = 1;
            if (log10 < 0) {
                cycles = (int) Math.round(Math.abs(log10));
            }
            int divs = 11;
            progressStatus.setMaxValue((long) cycles * divs);
            Double factor = factor(fixedCurveHolder, processCurveHolder, facFrom, facTo, divs, progressStatus, 0);
            int fromProgress = divs;
            int num = 1;
            while (num < cycles) {
                double step = (facTo - facFrom) / divs;
                facFrom = factor - step;
                facTo = factor + step;
                factor = factor(fixedCurveHolder, processCurveHolder, facFrom, facTo, divs, progressStatus, fromProgress);
                num++;
                fromProgress += divs;
            }
            return factor;
        }

    private static Double factor(CurveHolder fixedCurveHolder, CurveHolder processCurveHolder, double facFrom,
            double facTo, int divs, ProgressStatus<Long> progressStatus, int fromProgress) {
        double step = (facTo - facFrom) / divs;
        double[] factor = new double[1];
        int bestIndex = -1;
        double lowestSum = Double.MAX_VALUE;
        for (int i = 0; i < divs + 1; i++) {
            factor[0] = facFrom + i * step;
            double[] vals = new double[1];
            TraverseOperations.traverseTwoCurveHolders(fixedCurveHolder, processCurveHolder, (x, y1, y2) -> {
                double diff = y1 - (y2 * factor[0]);
                vals[0] += diff * diff;
            });
            if (vals[0] < lowestSum) {
                lowestSum = vals[0];
                bestIndex = i;
            }
            progressStatus.setProgressValue((long) fromProgress + i);
        }

        double result = facFrom + bestIndex * step;
        return result;
    }

    private static int compareRms(CurveHolder fixedCurveHolder, CurveHolder processCurveHolder, double factor) {
        double[] vals = new double[2];
        TraverseOperations.traverseTwoCurveHolders(fixedCurveHolder, processCurveHolder, (x, y1, y2) -> {
            vals[0] += y1 * y1;
            double diff = (y2 * factor);
            vals[1] += diff * diff;
        });
        if (vals[0] == vals[1]) {
            return 0;
        }
        if (vals[0] < vals[1]) {
            return -1;
        }
        return 1;
    }

    public static Double avgXOffset(CurveHolder curveHolder, double frequency) {
        double period = 1 / frequency;
        double maxClusterLength = period / 4;
        double minHalfPeriodXDist = period / 4; // how long the distance between asc and desc crospoint must be
        class Data {
            Double firstXInCluster;
            Double lastX;
            List<Double> clusterMidXs = new ArrayList<Double>();
            boolean error;
            Boolean firstCrossAsc;
        }
        Data data = new Data();
        TraverseOperations.crossXAxeTraverser(curveHolder, (x, asc) -> {
            if (data.firstXInCluster == null) {
                data.firstXInCluster = x;
                data.lastX = x;
                return true;
            }
            if (x < data.firstXInCluster + maxClusterLength) {
                data.lastX = x;
                return true;
            }
            if (x < data.lastX + minHalfPeriodXDist) {
                data.error = true;
                return false;
            }
            double mid = data.firstXInCluster + (data.lastX - data.firstXInCluster) / 2.0;
            data.clusterMidXs.add(mid);
            data.lastX = x;
            data.firstXInCluster = x;
            if (data.firstCrossAsc == null) {
                data.firstCrossAsc = !asc;
            }
            return true;
        });

        if (data.error) {
            return null;
        }

        // last point in data ignored
        int count = 0;
        double sum = 0;
        double halfPeriod = period / 2;
        double phaseShift = data.firstCrossAsc ? - halfPeriod : 0;
        for (int i = 1; i < data.clusterMidXs.size(); i++) { // ignore first
            Double mid = data.clusterMidXs.get(i) + phaseShift;
            sum += mid - count * halfPeriod;
            count++;
        }

        double avgShift = sum / count;

        return avgShift;
    }

    public static double calculateRms(CurveHolder curveHolder) {
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            double sum = 0;
            for (long i = 0; i < curveReader.size(); i++) {
                double y = curveReader.get(i).y;
                sum += y * y;
            }
            return Math.sqrt(sum / curveReader.size());
        }
    }

    public static void main(String[] args) {
        TxtCurveYLoader universalTextCurveYImport = new TxtCurveYLoader("/home/jan/Documents/noise-500-0_3-sample-shift-left-cut.txt");
        List<CurveHolder> curveHolders = universalTextCurveYImport.load(new NullLongProgressStatus());
        Double xOffset = avgXOffset(curveHolders.get(0), 500);
        System.out.println(xOffset);
    }

    public static Double findFirstXCrosspointSmart(CurveHolder curveHolder, double minPeriod, boolean ascending) {
        FirstTraverseConsumer consumer = new FirstTraverseConsumer();
        TraverseOperations.crossXAxeTraverserSmart(curveHolder, consumer, ascending, minPeriod);
        return consumer.getResultX();
    }

    /**
     * Move first ascending or descending crossection to given target X
     * @param curveHolder
     * @param ascending
     * @param s
     * @return
     */
    public static double firstCrossectionToX(CurveHolder curveHolder, Boolean ascending, ProgressStatus<Long> s) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        Double avgSamplingPeriod = null;
        if (curveHolder instanceof SampledCurveHolder) {
            SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
            avgSamplingPeriod = sampledCurveHolder.getCurveInfo().samplingPeriod;
        } else {
            avgSamplingPeriod = (curveInfo.maxX - curveInfo.minX) / (curveInfo.size - 1);
        }
        Double x = findFirstXCrosspointSmart(curveHolder, 4 * avgSamplingPeriod, ascending);
        if (x == null || Double.isNaN(x)) {
            throw new NotSuccessfulOperationException("Unable to find point crossing X axe");
        }
        return x;
    }

    public static CurveHolder movePoints(CurveHolder curveHolder, double deltaX, double deltaY, double deltaAmpY) {
        return BasicCurveOperations.movePoints(curveHolder, deltaX, deltaY, deltaAmpY, new NullLongProgressStatus());
    }

    public static CurveHolder movePoints(CurveHolder curveHolder, double deltaX, double deltaY, double deltaAmpY, ProgressStatus<Long> progressStatus) {
        progressStatus.setMaxValue(curveHolder.getCurveInfo().size);
        try (SequentialCurveReader reader = curveHolder.getCurveReader(); SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(curveHolder)) {
            for (long i = 0; i < reader.size(); i++) {
                Point point = reader.get(i);
                curveBuilder.add(new Point(point.x + deltaX, point.y * deltaAmpY + deltaY));
                progressStatus.setProgressValue(i);
            }

            return curveBuilder.build();
        }
    }

    public static CurveHolder multiplyPoints(CurveHolder curveHolder, double multiplyX, double multiplyY) {
        return BasicCurveOperations.multiplyPoints(curveHolder, multiplyX, multiplyY, new NullLongProgressStatus());
    }

    public static CurveHolder multiplyPoints(CurveHolder curveHolder, double multiplyX, double multiplyY, ProgressStatus<Long> progressStatus) {
        progressStatus.setMaxValue(curveHolder.getCurveInfo().size);
        try (SequentialCurveReader reader = curveHolder.getCurveReader(); SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(curveHolder)) {
            for (long i = 0; i < reader.size(); i++) {
                Point point = reader.get(i);
                curveBuilder.add(new Point(point.x * multiplyX, point.y * multiplyY));
                progressStatus.setProgressValue(i);
            }

            return curveBuilder.build();
        }
    }
}
