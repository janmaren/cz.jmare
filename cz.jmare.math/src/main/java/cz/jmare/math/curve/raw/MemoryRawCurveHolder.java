package cz.jmare.math.curve.raw;

import java.util.ArrayList;
import java.util.List;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.entity.TwoDoubles;
import cz.jmare.math.geometry.entity.Point;

public class MemoryRawCurveHolder implements CurveHolder, SequentialCurveReader {
    private List<Point> points;

    private CurveInfo curveInfo;

    public MemoryRawCurveHolder(List<Point> points) {
        this.points = (ArrayList<Point>) points;
    }

    public MemoryRawCurveHolder(RandomAccessCurve curve) {
        int size = curve.size();
        ArrayList<Point> points = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            points.add(curve.get(i));
        }
        this.points = points;
    }

    @Override
    public Point get(long index) {
        if (index > Integer.MAX_VALUE) {
            throw new IndexOutOfBoundsException("MemoryRawCurveHolder supports only numbers up to 2147483647");
        }
        return points.get((int) index);
    }

    @Override
    public long size() {
        return points.size();
    }

    @Override
    public SequentialCurveReader getCurveReader() {
        return this;
    }

    @Override
    public void close() {
    }

    @Override
    public CurveInfo getCurveInfo() {
        if (curveInfo == null) {
            populateCurveInfo();
        }
        return curveInfo;
    }

    private void populateCurveInfo() {
        CurveInfo curveInfo = new CurveInfo();
        curveInfo.size = points.size();
        curveInfo.minX = points.get(0).x;
        curveInfo.maxX = points.get(points.size() - 1).x;
        TwoDoubles minMaxValue = getMinMaxValue(points);
        curveInfo.minY = minMaxValue.x1;
        curveInfo.maxY = minMaxValue.x2;
        this.curveInfo = curveInfo;
    }


    public static TwoDoubles getMinMaxValue(List<Point> polyLine) {
        double max = -Double.MAX_VALUE;
        double min = Double.MAX_VALUE;
        for (Point point : polyLine) {
            if (point.y > max) {
                max = point.y;
            }
            if (point.y < min) {
                min = point.y;
            }
        }
        return new TwoDoubles(min, max);
    }
}
