package cz.jmare.math.curve.importexport;

public class SampledReadParameters {
    /**
     * Minimal x offset
     */
    public Double offset;

    /**
     * In Hz
     */
    public Double samplingRate;

    public SampledReadParameters withOffset(double offset) {
        this.offset = offset;
        return this;
    }

    public SampledReadParameters withSamplingRate(double samplingRate) {
        this.samplingRate = samplingRate;
        return this;
    }
}
