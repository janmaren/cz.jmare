package cz.jmare.math.curve;

public enum PolylineType {
    RAW, SAMPLED
}
