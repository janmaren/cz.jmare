package cz.jmare.math.curve;

public interface SampledCurveHolder extends CurveHolder {
    @Override
    SampledCurveInfo getCurveInfo();
}
