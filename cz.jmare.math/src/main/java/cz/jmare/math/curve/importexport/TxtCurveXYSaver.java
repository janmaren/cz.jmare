package cz.jmare.math.curve.importexport;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.exception.CurveNotSampledException;
import cz.jmare.math.exception.CurvesPointsNotFitException;
import cz.jmare.math.exception.CurvesWithDifferentSamplingRateException;
import cz.jmare.math.exception.UnableToWriteException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class TxtCurveXYSaver {
    public static void exportFile(String filepath, List<CurveHolder> curveHolders) throws CurvesPointsNotFitException, CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        exportFile(filepath, curveHolders, new NullLongProgressStatus());
    }

    public static void exportFile(String filepath, List<CurveHolder> curveHolders, ProgressStatus<Long> progressStatus) throws CurvesPointsNotFitException, CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        try (OutputStream os = new FileOutputStream(filepath); Writer writer = new OutputStreamWriter(os, StandardCharsets.ISO_8859_1.toString())) {
            exportFile(writer, curveHolders, progressStatus);
        } catch (IOException e) {
            throw new UnableToWriteException("Unable to export to " + filepath, e);
        }
    }

    public static void exportFile(StringWriter writer, List<CurveHolder> curveHolders) throws CurvesPointsNotFitException, CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        exportFile(writer, curveHolders, new NullLongProgressStatus());
    }


    public static void exportFile(Writer writer, List<CurveHolder> curveHolders, ProgressStatus<Long> progressStatus) throws CurvesPointsNotFitException, CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        check(curveHolders);

        CurveInfo curveInfo = curveHolders.get(0).getCurveInfo();

        List<SequentialCurveReader> readers = new ArrayList<>();
        for (int i = 0; i < curveHolders.size(); i++) {
            readers.add(curveHolders.get(i).getCurveReader());
        }

        try {
            PrintWriter pw = new PrintWriter(writer);
            progressStatus.setMaxValue(curveInfo.size);
            pw.println("Polyline Type: X, Y");
            pw.println();
            for (long i = 0; i < curveInfo.size; i++) {
                progressStatus.setProgressValue(i);
                Point zeroPoint = null;
                for (int j = 0; j < curveHolders.size(); j++) {
                    SequentialCurveReader sequentialCurveReader = readers.get(j);
                    Point point = sequentialCurveReader.get(i);
                    if (j == 0) {
                        zeroPoint = point;
                        if (point.x >= 0) {
                            pw.print(" ");
                        }
                        pw.print(format13(point.x));
                        pw.print(",\t");
                        if (point.y >= 0) {
                            pw.print(" ");
                        }
                        pw.print(format13(point.y));
                    } else {
                        if (!EpsCompare.equals(zeroPoint.x, point.x)) {
                            throw new CurvesPointsNotFitException("There is point with x=" + zeroPoint.x + " which is different than " + point.x);
                        }
                        pw.print(",\t");
                        if (point.y >= 0) {
                            pw.print(" ");
                        }
                        pw.print(format13(point.y));
                    }
                }
                pw.println();
            }
        } finally {
            for (int i = 0; i < readers.size(); i++) {
                readers.get(i).close();
            }
        }
    }

    private static void check(List<CurveHolder> curveHolders) {
        Double samplingPeriod = null;
        Double minX = null;
        Double maxX = null;
        for (CurveHolder curveHolder : curveHolders) {
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            if (curveHolders.size() > 1) {
                if (!(curveHolder instanceof SampledCurveHolder)) {
                    throw new CurveNotSampledException("There is an curve whitch is not sampled");
                }
                SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
                if (samplingPeriod != null) {
                    if (!EpsCompare.equals(samplingPeriod, sampledCurveHolder.getCurveInfo().samplingPeriod)) {
                        throw new CurvesWithDifferentSamplingRateException("Curves havn't the same sampling rate");
                    }
                }
            }
            if (curveHolder instanceof SampledCurveHolder) {
                SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
                samplingPeriod = sampledCurveHolder.getCurveInfo().samplingPeriod;
            }

            if (minX == null) {
                minX = curveInfo.minX;
            } else {
                if (!EpsCompare.equals(minX, curveInfo.minX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same beginning x coordinate");
                }
            }
            if (maxX == null) {
                maxX = curveInfo.maxX;
            } else {
                if (!EpsCompare.equals(maxX, curveInfo.maxX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same ending X coordinate");
                }
            }
        }
    }

    private static String format13(double d) {
        return String.format(Locale.ENGLISH, "%.13f", d);
    }
}
