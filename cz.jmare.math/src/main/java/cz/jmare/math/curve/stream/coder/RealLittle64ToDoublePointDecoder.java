package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.util.DecodeRevertsUtil;

/**
 * Convert 8 bytes into point with extracted Y axe
 * Bytes must make the double type
 * Result min max values are -1 including, 1 including
 * Little endian order
 */
public class RealLittle64ToDoublePointDecoder implements DoublePointDecoder {
    public RealLittle64ToDoublePointDecoder() {
        super();
    }

    @Override
    public Point getPoint(double x, byte[] bytes, int index) {
        double f = DecodeRevertsUtil.bytesToDoubleRevert(bytes, index);
        Point point = new Point(x, (double) f);
        return point;
    }

    @Override
    public int getSampleLengthBytes() {
        return 8;
    }
}
