package cz.jmare.math.curve.raw;

import java.util.List;

import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.geometry.entity.Point;

public class RawRandomAccessReader implements RandomAccessCurve {
    private List<Point> points;

    public RawRandomAccessReader(List<Point> points) {
        this.points = points;
    }

    @Override
    public Point get(int index) {
        return points.get(index);
    }

    @Override
    public int size() {
        return points.size();
    }
}
