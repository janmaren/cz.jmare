package cz.jmare.math.curve.weights;

import java.util.function.Supplier;

/**
 * Weights by text, e.g. "121", "11211", "111", "12321", "13531", "1234321", "1248421", "12345654321"
 */
public class TextEnterWeightsSupplier implements Supplier<double[]> {
    private double[] weights;

    public TextEnterWeightsSupplier(String smoothType) {
        super();
        weights = new double[smoothType.length()];
        for (int i = 0; i < smoothType.length(); i++) {
            weights[i] = smoothType.charAt(i) - 48;
        }
    }

    @Override
    public double[] get() {
        return weights;
    }
}
