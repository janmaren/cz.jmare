package cz.jmare.math.curve;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.jmare.math.curve.randomaccess.PointsFloatFloatSimplifiedCurve;
import cz.jmare.math.curve.randomaccess.PointsFloatShortSimplifiedCurve;
import cz.jmare.math.curve.randomaccess.Sampled16SimplifiedCurve;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.curve.raw.BigRawCurveHolder;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.curve.raw.RawRandomAccessReader;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.curve.sampled.BigSampledCurveHolder;
import cz.jmare.math.curve.sampled.MemorySampledCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class CurveUtil {
    public static SequentialCurveBuilder createNewBigCurveBuilder(CurveHolder curveHolder) {
        if (curveHolder instanceof BigRawCurveHolder || curveHolder instanceof MemoryRawCurveHolder) {
            return new BigRawCurveBuilder();
        }
        if (curveHolder instanceof BigSampledCurveHolder || curveHolder instanceof MemorySampledCurveHolder) {
            return new BigSampledCurveBuilder();
        }
        throw new IllegalArgumentException("Unable to create new instance similar to " + curveHolder.getClass() + " - not known type");
    }

    public static RandomAccessCurve toMemoryOptimalRandomAccessCurve(CurveHolder curveHolder, ProgressStatus<Long> progressStatus) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        if (curveHolder instanceof SampledCurveHolder) {
            SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
            return Sampled16SimplifiedCurve.getSampled16SimplifiedCurve(sampledCurveHolder, progressStatus);
        } else {
            if (curveInfo.size > 1000000) {
                return PointsFloatShortSimplifiedCurve.getPointsFloatShortSimplifiedCurve(curveHolder, progressStatus);
            }
            return PointsFloatFloatSimplifiedCurve.getPointsFloatSimplifiedCurve(curveHolder, progressStatus);
        }
    }


    public static CurveHolder toRAMCurveHolder(CurveHolder curveHolder) {
        CurveHolder resultCurveHolder;
        if (curveHolder instanceof SampledCurveHolder) {
            SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
            resultCurveHolder = new MemorySampledCurveHolder(toSampledDoubles(sampledCurveHolder), sampledCurveHolder.getCurveInfo().samplingPeriod, curveHolder.getCurveInfo().minX);
        } else {
            resultCurveHolder = new MemoryRawCurveHolder(toPoints(curveHolder));
        }
        return resultCurveHolder;
    }

    public static RandomAccessCurve toRandomAccessCurve(CurveHolder curveHolder) {
        if (curveHolder instanceof RandomAccessCurve) {
            return (RandomAccessCurve) curveHolder;
        }
        return new RawRandomAccessReader(toPoints(curveHolder));
    }

    public static float[] toSampledFloats(SampledCurveHolder curveHolder) {
        try (SequentialCurveReader reader = curveHolder.getCurveReader()) {
            if (reader.size() > Integer.MAX_VALUE) {
                throw new IllegalStateException("Too much points to be converted to float[]");
            }
            int size = (int) reader.size();
            float[] values = new float[size];
            for (int i = 0; i < size; i++) {
                Point point = reader.get(i);
                values[i] = (float) point.y;
            }
            return values;
        }
    }

    public static double[] toSampledDoubles(SampledCurveHolder curveHolder) {
        try (SequentialCurveReader reader = curveHolder.getCurveReader()) {
            if (reader.size() > Integer.MAX_VALUE) {
                throw new IllegalStateException("Too much points to be converted to float[]");
            }
            int size = (int) reader.size();
            double[] values = new double[size];
            for (int i = 0; i < size; i++) {
                Point point = reader.get(i);
                values[i] = point.y;
            }
            return values;
        }
    }


    public static CurveHolder saveAsCurve(CurveHolder curveHolder, File file) {
        try (BigRawCurveBuilder bigPointsCurveBuilder = new BigRawCurveBuilder(file); SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            long size = curveReader.size();
            for (int i = 0; i < size; i++) {
                bigPointsCurveBuilder.add(curveReader.get(i));
            }
            CurveHolder savedCurveHolder = bigPointsCurveBuilder.build();
            return savedCurveHolder;
        }
    }

    public static SampledCurveHolder saveAsCurve(SampledCurveHolder curveHolder, File file) {
        try (BigSampledCurveBuilder bigPointsCurveBuilder = new BigSampledCurveBuilder(file, curveHolder.getCurveInfo().samplingPeriod, curveHolder.getCurveInfo().minX); SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            long size = curveReader.size();
            for (int i = 0; i < size; i++) {
                bigPointsCurveBuilder.add(curveReader.get(i));
            }
            SampledCurveHolder savedCurveHolder = bigPointsCurveBuilder.build();
            return savedCurveHolder;
        }
    }

    public static List<Point> toPoints(CurveHolder curveHolder) {
        ArrayList<Point> result = new ArrayList<Point>();
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            long size = curveReader.size();
            for (long i = 0; i < size; i++) {
                Point point = curveReader.get(i);
                result.add(point);
            }
        }
        return result;
    }

    public static List<CurveHolder> of(CurveHolder... curveHolder) {
        List<CurveHolder> curveHolders = new ArrayList<CurveHolder>();
        for (CurveHolder curveHolder2 : curveHolder) {
            curveHolders.add(curveHolder2);
        }
        return curveHolders;
    }

    public static List<SampledCurveHolder> of(SampledCurveHolder... curveHolder) {
        List<SampledCurveHolder> curveHolders = new ArrayList<SampledCurveHolder>();
        for (SampledCurveHolder curveHolder2 : curveHolder) {
            curveHolders.add(curveHolder2);
        }
        return curveHolders;
    }
}
