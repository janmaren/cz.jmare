package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Encode a Point.y casted into float into 4 bytes
 * Values of Y axe are from -1 including to 1 including
 * Little endian order
 */
public class RealLittle32FromDoublePointEncoder implements DoublePointEncoder {
    public RealLittle32FromDoublePointEncoder() {
        super();
    }

    @Override
    public void encode(Point point, byte[] bytes, int offset) {
        double y = point.y;
        toByta((float) y, bytes, offset);
    }

    @Override
    public int getSampleLengthBytes() {
        return 4;
    }

    public static void toByta(float value, byte[] bytes, int offset) {
        int intBits =  Float.floatToIntBits(value);

        bytes[offset] = (byte) (intBits >> 24);
        bytes[offset + 1] = (byte) (intBits >> 16);
        bytes[offset + 2] = (byte) (intBits >> 8);
        bytes[offset + 3] = (byte) (intBits);
    }
}
