package cz.jmare.math.curve.randomaccess;

import java.nio.ByteBuffer;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

/**
 * Points collection where point is representated as to float numbers. One point
 * takes 8 byte
 */
public class PointsFloatFloatSimplifiedCurve implements RandomAccessCurve {
    private ByteBuffer points;

    private int size;

    public PointsFloatFloatSimplifiedCurve(ByteBuffer points, int size) {
        super();
        this.points = points;
        this.size = size;
    }

    @Override
    public Point get(int index) {
        points.position(index * 8);
        float x = points.getFloat();
        float y = points.getFloat();
        return new Point(x, y);
    }

    @Override
    public int size() {
        return size;
    }

    public static PointsFloatFloatSimplifiedCurve getPointsFloatSimplifiedCurve(CurveHolder curveHolder) {
        return getPointsFloatSimplifiedCurve(curveHolder, new NullLongProgressStatus());
    }

    public static PointsFloatFloatSimplifiedCurve getPointsFloatSimplifiedCurve(CurveHolder curveHolder,
            ProgressStatus<Long> progressStatus) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();

        long size = curveInfo.size;
        if (size > Integer.MAX_VALUE) {
            throw new IllegalStateException(
                    "getSampled16SimplifiedCurve may be used only for curve with max points " + Integer.MAX_VALUE);
        }
        int sizeInt = (int) size;

        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            ByteBuffer points = ByteBuffer.allocate(sizeInt * 4 * 2);
            progressStatus.setMaxValue(size);
            for (int i = 0; i < size; i++) {
                Point point = curveReader.get(i);
                double x = point.x;
                double y = point.y;
                points.putFloat((float) x);
                points.putFloat((float) y);
                progressStatus.setProgressValue((long) i);
            }

            return new PointsFloatFloatSimplifiedCurve(points, sizeInt);
        }
    }
}
