package cz.jmare.math.curve.importexport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.PolylineType;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.exception.NotEnoughParametersException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class TxtCurveYLoader {
    private final static Pattern STARTS_NUMBER_PATTERN = Pattern.compile("^\\s*(?<number>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)");
    private final static Pattern OFFSET_PATTERN = Pattern.compile("^\\s*X offset:\\s*(?<offset>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)");

    private String filepath;
    private Integer yColumnsNumber;
    private String separator;
    private List<Double> metricSuffixY;
    private Double offset;
    private Double samplingRate;
    private Double samplingPeriod;

    public TxtCurveYLoader(String filepath) {
        this(new PolylineFileTypeAnalyzator(filepath));
    }

    public TxtCurveYLoader(PolylineFileTypeAnalyzator analyzator) throws InvalidFormatException {
        if (analyzator.getPolylineType() != null && analyzator.getPolylineType() != PolylineType.SAMPLED) {
            throw new InvalidFormatException("File " + filepath + " doesn't contain any data to be loaded or not supported format");
        }
        this.filepath = analyzator.getFilepath();
        this.separator = analyzator.getSeparator();
        if (analyzator.isSeparatorDetected()) {
            yColumnsNumber = analyzator.getColumnsNumber();
        }
        this.metricSuffixY = analyzator.getColumnsMetrics();
        this.samplingRate = analyzator.getSamplingRate();
    }

    public List<CurveHolder> load(ProgressStatus<Long> progressStatus) throws InvalidFormatException, NotEnoughParametersException {
        if (samplingRate == null && samplingPeriod == null) {
            throw new NotEnoughParametersException("No sampling rate or sampling period defined");
        }
        List<Double> metricFactorsY = new ArrayList<>();
        for (int i = 0; i < yColumnsNumber; i++) {
            metricFactorsY.add(1.0);
        }
        if (metricSuffixY != null) {
            metricFactorsY = metricSuffixY;
        }

        List<BigSampledCurveBuilder> builders = new ArrayList<>();
        for (int i = 0; i < yColumnsNumber; i++) {
            builders.add(new BigSampledCurveBuilder());
        }

        double lastMetricFactorY = 1;
        List<Double> metricFactorYs = new ArrayList<Double>();
        for (int i = 0; i < yColumnsNumber; i++) {
            if (i < metricFactorsY.size()) {
                lastMetricFactorY = metricFactorsY.get(i);
            }
            metricFactorYs.add(lastMetricFactorY);
        }

        File file = new File(filepath);
        long length = file.length();
        progressStatus.setMaxValue(length);
        try (FriendlyBufferReader friendlyBufferReader = new FriendlyBufferReader(filepath)) {
            String line;
            while ((line = friendlyBufferReader.readLine()) != null) {
                if (offset == null) {
                    Matcher matcherOffset = OFFSET_PATTERN.matcher(line);
                    if (matcherOffset.find()) {
                        offset = Double.parseDouble(matcherOffset.group("offset"));
                    }
                }
                if (lineStartsWithNumber(line)) {
                    break;
                }
            }
            if (line == null) {
                throw new InvalidFormatException("No data present");
            }

            if (yColumnsNumber == null) {
                yColumnsNumber = ParseLineUtil.parseDoubles(line, separator).length;
            }

            long numRead = 0;
            long i = 0;
            double x;
            if (offset == null) {
                offset = 0.0;
            }
            do {
                line = line.trim();
                if ("".equals(line)) {
                    continue;
                }
                double[] parseDoubles = ParseLineUtil.parseDoubles(line, separator);
                if (parseDoubles.length != yColumnsNumber) {
                    throw new InvalidFormatException("Invalid number of columns on line " + line);
                }
                for (int j = 0; j < builders.size(); j++) {
                    BigSampledCurveBuilder bigSampledCurveBuilder = builders.get(j);
                    if (samplingRate != null) {
                        x = i / samplingRate + offset;
                    } else {
                        x = i * samplingPeriod + offset;
                    }
                    Point point = new Point(x, parseDoubles[j] * metricFactorYs.get(j));
                    bigSampledCurveBuilder.add(point);
                }

                numRead += line.length() + 1;
                progressStatus.setProgressValue(numRead);
                i++;
            } while ((line = friendlyBufferReader.readLine()) != null);

            List<CurveHolder> holders = new ArrayList<>();
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                holders.add(bigSampledCurveBuilder.build());
            }
            return holders;
        } finally {
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                bigSampledCurveBuilder.close();
            }
        }
    }

    public boolean allValuesPresent() {
        if (metricSuffixY == null || metricSuffixY.size() == 0) {
            return false;
        }

        return true;
    }

    public void setMetricSuffixY(List<Double> metricSuffixY) {
        this.metricSuffixY = metricSuffixY;
    }

    public void setMetricSuffixY(double metricSuffixY) {
        this.metricSuffixY = new ArrayList<Double>();
        this.metricSuffixY.add(metricSuffixY);
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getSeparator() {
        return separator;
    }

    private static boolean lineStartsWithNumber(String line) {
        return STARTS_NUMBER_PATTERN.matcher(line).find();
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public void setSamplingRate(Double samplingRate) {
        this.samplingRate = samplingRate;
    }

    public void setSamplingPeriod(Double samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }
}
