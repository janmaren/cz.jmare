package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.util.DecodeRevertsUtil;

/**
 * Convert 4 bytes into point with extracted Y axe
 * Result min max values are -1 including, 1 including
 * Little endian order
 */
public class IntegerLittle32ToDoublePointDecoder implements DoublePointDecoder {
    public static int HALF = (int) Math.pow(2, 32 - 1);
    public IntegerLittle32ToDoublePointDecoder() {
        super();
    }

    @Override
    public Point getPoint(double x, byte[] bytes, int index) {
        int val = DecodeRevertsUtil.bytesToIntRevert(bytes, index);
        return new Point(x, (double) val / HALF);
    }

    @Override
    public int getSampleLengthBytes() {
        return 4;
    }
}
