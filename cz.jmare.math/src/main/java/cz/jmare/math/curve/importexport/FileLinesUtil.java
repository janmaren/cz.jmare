package cz.jmare.math.curve.importexport;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileLinesUtil {
    public static List<String> readNLines(String filepath, String charset, int firstNLines) {
        List<String> result = new ArrayList<String>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), charset))) {
            String line;
            int readLines = 0;
            while ((line = br.readLine()) != null) {
                result.add(line);
                if (++readLines >= firstNLines) {
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read " + filepath, e);
        }

        return result;
    }

    public static void skipNLines(BufferedReader br, int skipNLines) {
        try {
            @SuppressWarnings("unused")
            String line;
            int readLines = 0;
            while ((line = br.readLine()) != null) {
                if (++readLines >= skipNLines) {
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read inputstream", e);
        }
    }
}
