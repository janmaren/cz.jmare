package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Decode a number of bytes which usually come with stream into Point
 * The min and max values of Y are usually from -1 to 1 (both including)
 */
public interface DoublePointDecoder {
    Point getPoint(double x, byte[] bytes, int index);

    int getSampleLengthBytes();
}
