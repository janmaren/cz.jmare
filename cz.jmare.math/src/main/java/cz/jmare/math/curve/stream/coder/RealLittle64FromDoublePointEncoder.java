package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Encode a Point.y casted into double into 8 bytes
 * Values of Y axe are from -1 including to 1 including
 * Little endian order
 */
public class RealLittle64FromDoublePointEncoder implements DoublePointEncoder {
    public RealLittle64FromDoublePointEncoder() {
        super();
    }

    @Override
    public void encode(Point point, byte[] bytes, int offset) {
        double y = point.y;
        toByta(y, bytes, offset);
    }

    @Override
    public int getSampleLengthBytes() {
        return 8;
    }

    public static void toByta(double value, byte[] bytes, int offset) {
        long longBits =  Double.doubleToLongBits(value);
        bytes[offset] = (byte) (longBits >> 56);
        bytes[offset + 1] = (byte) (longBits >> 48);
        bytes[offset + 2] = (byte) (longBits >> 40);
        bytes[offset + 3] = (byte) (longBits >> 32);
        bytes[offset + 4] = (byte) (longBits >> 24);
        bytes[offset + 5] = (byte) (longBits >> 16);
        bytes[offset + 6] = (byte) (longBits >> 8);
        bytes[offset + 7] = (byte) (longBits);
    }
}
