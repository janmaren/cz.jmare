package cz.jmare.math.curve.interpol;

public interface InterpolatedCurve {
    double getY(double x);

    double getMinX();

    double getMaxX();
}
