package cz.jmare.math.curve.importexport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;
import cz.jmare.progress.ProgressStatus;
import cz.jmare.progress.ReadableProgressStatus;

public class PicoTextCurveImportExport {
    private final static Pattern A_CHANNEL_PATTERN = Pattern.compile("^\\s*Time\\s+(?<channelNames>Channel.*)");

    public static List<String> detectChannelsPresent(String firstLine) {
        Matcher matcher = A_CHANNEL_PATTERN.matcher(firstLine);
        if (matcher.find()) {
            String channelNames = matcher.group("channelNames");
            String[] split = channelNames.split("(?i)Channel");
            List<String> names = Arrays.asList(split).stream().map(s -> s.trim()).collect(Collectors.toList());
            names.remove(0);
            return names;
        }
        return null;
    }

    public static double[] detectUnitsScalesPresent(String secondLine) {
        String[] split = secondLine.split("\\(");
        double[] result = new double[split.length - 1];
        for (int i = 1; i < split.length; i++) {
            String value = split[i];
            int indexOf = value.indexOf(")");
            if (indexOf == -1) {
                return null;
            }
            String str = value.substring(0, indexOf);
            double multiply = 1;
            if ("ms".equalsIgnoreCase(str)) {
                multiply = 1;
            } else if ("s".equalsIgnoreCase(str)) {
                multiply = 1000;
            } else if ("ns".equalsIgnoreCase(str)) {
                multiply = 0.001;
            } else if ("V".equalsIgnoreCase(str)) {
                multiply = 1;
            } else if ("mV".equalsIgnoreCase(str)) {
                multiply = 0.001;
            } else if ("uV".equalsIgnoreCase(str)) {
                multiply = 0.000001;
            }
            result[i - 1] = multiply;
        }
        return result;
    }

    @Deprecated
    public static List<CurveHolder> load(String filepath, ProgressStatus<Long> progressStatus) throws InvalidFormatException {
        List<String> linesList = FileLinesUtil.readNLines(filepath, "ISO-8859-1", 15);
        String[] lines = linesList.toArray(new String[linesList.size()]);

        List<String> names = detectChannelsPresent(lines[0]);
        if (names == null) {
            throw new InvalidFormatException("Not a pico file because unable to detect columns");
        }
        double[] unitsScalesPresent = detectUnitsScalesPresent(lines[1]);
        if (unitsScalesPresent == null || unitsScalesPresent.length != names.size() + 1) {
            return null;
        }

        List<BigRawCurveBuilder> builders = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            builders.add(new BigRawCurveBuilder());
        }

        double lastX = -Double.MAX_VALUE;
        progressStatus.setMaxValue(new File(filepath).length());
        try (FriendlyBufferReader friendlyBufferReader = new FriendlyBufferReader(filepath)) {
            FileLinesUtil.skipNLines(friendlyBufferReader, 3);
            String line;
            long numRead = 0;
            while ((line = friendlyBufferReader.readLine()) != null) {
                line = line.trim();
                if ("".equals(line)) {
                    continue;
                }
                String[] split = line.split("\\s+");
                if (split.length != unitsScalesPresent.length) {
                    return null;
                }

                double x = Double.valueOf(split[0]) * unitsScalesPresent[0];

                // ascending check
                if (x < lastX) {
                    throw new InvalidFormatException("X value is not ascending, there is " + lastX + " > " + x);
                }
                lastX = x;

                for (int j = 0; j < names.size(); j++) {
                    BigRawCurveBuilder bigPointsCurveBuilder = builders.get(j);
                    bigPointsCurveBuilder.add(new Point(x, Double.valueOf(split[j + 1]) * unitsScalesPresent[j + 1]));
                }

                numRead += line.length() + 1;
                progressStatus.setProgressValue(numRead);
            }
            List<CurveHolder> holders = new ArrayList<>();
            for (BigRawCurveBuilder bigPointsCurveBuilder : builders) {
                holders.add(bigPointsCurveBuilder.build());
            }

            return holders;
        } finally {
            for (BigRawCurveBuilder bigPointsCurveBuilder : builders) {
                bigPointsCurveBuilder.close();
            }
        }
    }

    public static String format9(double d) {
        return String.format(Locale.ENGLISH, "%.9f", d);
    }

    public static class ChannelsPresent {
        public List<String> columnNames = new ArrayList<>();
    }

    private static void check(List<CurveHolder> curveHolders) {
        Double samplingPeriod = null;
        Double minX = null;
        Double maxX = null;
        for (CurveHolder curveHolder : curveHolders) {
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            if (curveHolders.size() > 1) {
                if (!(curveHolder instanceof SampledCurveHolder)) {
                    throw new IllegalArgumentException("There is an curve whitch is not sampled");
                }
                SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
                if (samplingPeriod != null) {
                    if (!EpsCompare.equals(samplingPeriod, sampledCurveHolder.getCurveInfo().samplingPeriod)) {
                        throw new IllegalArgumentException("Curves havn't the same sampling rate");
                    }
                }
            }
            samplingPeriod = ((SampledCurveHolder)curveHolder).getCurveInfo().samplingPeriod;

            if (minX == null) {
                minX = curveInfo.minX;
            } else {
                if (!EpsCompare.equals(minX, curveInfo.minX)) {
                    throw new IllegalArgumentException("Curves havn't the same beginning x coordinate");
                }
            }
            if (maxX == null) {
                maxX = curveInfo.maxX;
            } else {
                if (!EpsCompare.equals(maxX, curveInfo.maxX)) {
                    throw new IllegalArgumentException("Curves havn't the same ending X coordinate");
                }
            }
        }
    }


    public static void writeToFile(List<CurveHolder> curveHolders, List<String> names, String filename, ReadableProgressStatus<Long> progressStatus) throws IOException {
        check(curveHolders);

        List<SequentialCurveReader> readers = new ArrayList<>();
        for (int i = 0; i < curveHolders.size(); i++) {
            readers.add(curveHolders.get(i).getCurveReader());
        }

        try (OutputStream os = new FileOutputStream(filename); Writer writer = new OutputStreamWriter(os, StandardCharsets.ISO_8859_1.toString()); PrintWriter pw = new PrintWriter(writer)) {
            StringBuilder namesBuilder = new StringBuilder();
            StringBuilder voltagesBuilder = new StringBuilder();

            for (String name : names) {
                if (namesBuilder.length() > 0) {
                    namesBuilder.append("   ");
                }
                namesBuilder.append("Channel ").append(name);

                if (voltagesBuilder.length() > 0) {
                    voltagesBuilder.append(" ");
                }
                voltagesBuilder.append("(V)");
            }
            pw.println("Time    " + namesBuilder);
            pw.println("(ms)    " + voltagesBuilder);
            pw.println();
            long count = readers.get(0).size();
            progressStatus.setMaxValue(count);
            for (long i = 0; i < count; i++) {
                progressStatus.setProgressValue(i);
                StringBuilder line = new StringBuilder();
                for (int j = 0; j < readers.size(); j++) {
                    SequentialCurveReader reader = readers.get(j);
                    Point point = reader.get(i);
                    if (j == 0) {
                        line.append(format9(point.x));
                    }
                    line.append("\t").append(format9(point.y));
                }
                pw.println(line);
            }
        } finally {
            for (int i = 0; i < readers.size(); i++) {
                readers.get(i).close();
            }
        }
    }
}
