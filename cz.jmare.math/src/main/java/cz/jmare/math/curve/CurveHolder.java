package cz.jmare.math.curve;

public interface CurveHolder {
    SequentialCurveReader getCurveReader();

    CurveInfo getCurveInfo();
}
