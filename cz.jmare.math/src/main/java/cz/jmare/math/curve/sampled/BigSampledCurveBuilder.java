package cz.jmare.math.curve.sampled;

import static cz.jmare.math.curve.util.CurveFileUtil.getDataOutputStream;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.exception.NotSuccessfulOperationException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;

public class BigSampledCurveBuilder implements SequentialCurveBuilder {
    private String filepath;

    private DataOutputStream dos;

    private long pointsSize;

    private double minY = Double.MAX_VALUE;

    private double maxY = -Double.MAX_VALUE;

    private Double minX;

    private double maxX;

    private Double samplingPeriod;

    public BigSampledCurveBuilder() {
        this(null);
    }

    public BigSampledCurveBuilder(Double samplingPeriod) {
        this(createTmpFile(), samplingPeriod, null);
    }

    public BigSampledCurveBuilder(File file, Double samplingPeriod, Double minX) {
        super();
        this.samplingPeriod = samplingPeriod;
        this.minX = minX;
        filepath = file.toString();
        try {
            dos = getDataOutputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to create " + filepath, e);
        }
        try {
            dos.writeLong(MAIN_FORMAT_MARKER);     // marker
            dos.writeLong(SUB_FORMAT_MARKER_SAMPLED); // type
            dos.writeLong(-1L);             // length - replace later
            dos.writeLong(0);             // reserved
            dos.writeDouble(Double.NaN);    // minX
            dos.writeDouble(Double.NaN);    // maxX
            dos.writeDouble(Double.NaN);    // minY
            dos.writeDouble(Double.NaN);    // maxY
            dos.writeDouble(Double.NaN);// sampling period
            dos.writeLong(0);
            dos.writeLong(0);
            dos.writeLong(0);
            dos.writeLong(0);
            dos.writeLong(0);
            dos.writeLong(0);
            dos.writeLong(0);
        } catch (IOException e) {
        }
    }

    @Override
    public void add(Point point) {
        if (pointsSize == 0) {
            if (minX != null) {
                if (!EpsCompare.equals(minX, point.x)) {
                    throw new IllegalArgumentException("MinX should be " + minX + " but is " + point.x);
                }
            }
            minX = point.x;
        }

        try {
            dos.writeDouble(point.y);
            pointsSize++;
            if (point.y < minY) {
                minY = point.y;
            }
            if (point.y > maxY) {
                maxY = point.y;
            }
            maxX = point.x;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to write to " + filepath, e);
        }
    }

    @Override
    public SampledCurveHolder build() {
        try {
            dos.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close " + filepath);
        }
        if (pointsSize < 2) {
            throw new NotSuccessfulOperationException("Result curve doesn't contain at least 2 points");
        }
        double calculatedSamplingPeriod = (maxX - minX) / (pointsSize - 1);
        if (samplingPeriod == null) {
            samplingPeriod = calculatedSamplingPeriod;
        } else {
            if (!EpsCompare.equals(samplingPeriod, calculatedSamplingPeriod)) {
                throw new IllegalStateException("Entered data don't have required sampling period " + samplingPeriod + " but " + calculatedSamplingPeriod);
            }
        }
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(filepath, "rw")) {
            randomAccessFile.skipBytes(16);
            randomAccessFile.writeLong(pointsSize);
            randomAccessFile.writeLong(0); // reserved
            randomAccessFile.writeDouble(minX);
            randomAccessFile.writeDouble(maxX);
            randomAccessFile.writeDouble(minY);
            randomAccessFile.writeDouble(maxY);
            randomAccessFile.writeDouble(samplingPeriod);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to write to RandomAccessFile " + filepath, e);
        }
        return new BigSampledCurveHolder(filepath);
    }

    @Override
    public void close() {
        try {
            dos.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close " + filepath);
        }
    }

    public static CurveHolder createBigSampledCurveHolder(double samplingPeriod, double minX, double... y) {
        int index = 0;
        try (BigSampledCurveBuilder builder = new BigSampledCurveBuilder(samplingPeriod)) {
            while (index < y.length) {
                Point point = new Point(index * samplingPeriod + minX, y[index]);
                builder.add(point);
                index++;
            }
            return builder.build();
        }
    }

    private static File createTmpFile() {
        try {
            File createTempFile = File.createTempFile("bigsampled", ".dat");
            createTempFile.deleteOnExit();
            return createTempFile;
        } catch (IOException e1) {
            throw new IllegalStateException("Unable to create temporary file", e1);
        }
    }
}
