package cz.jmare.math.curve.randomaccess;

import java.nio.ByteBuffer;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class PointsFloatShortSimplifiedCurve implements RandomAccessCurve {
    private ByteBuffer points;

    private int size;

    private double yFactor;

    private double minY;

    public PointsFloatShortSimplifiedCurve(ByteBuffer points, int size, double yFactor, double minY) {
        super();
        this.points = points;
        this.size = size;
        this.yFactor = yFactor;
        this.minY = minY;
    }

    @Override
    public Point get(int index) {
        points.position(index * 6);
        float x = points.getFloat();
        short yRel = points.getShort();
        return new Point(x, yFactor * (yRel + 32768) + minY);
    }

    @Override
    public int size() {
        return size;
    }

    public static PointsFloatShortSimplifiedCurve getPointsFloatShortSimplifiedCurve(CurveHolder curveHolder) {
        return getPointsFloatShortSimplifiedCurve(curveHolder, new NullLongProgressStatus());
    }

    public static PointsFloatShortSimplifiedCurve getPointsFloatShortSimplifiedCurve(CurveHolder curveHolder,
            ProgressStatus<Long> progressStatus) {
        CurveInfo curveInfo = curveHolder.getCurveInfo();

        double minY = curveInfo.minY;
        double maxY = curveInfo.maxY;

        long size = curveInfo.size;
        if (size > Integer.MAX_VALUE) {
            throw new IllegalStateException(
                    "getSampled16SimplifiedCurve may be used only for curve with max points " + Integer.MAX_VALUE);
        }
        int sizeInt = (int) size;

        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {

            double simplifiedPointsYFactor = (maxY - minY) / 65535;

            ByteBuffer points = ByteBuffer.allocate(sizeInt * 6);
            progressStatus.setMaxValue(size);
            for (int i = 0; i < size; i++) {
                Point point = curveReader.get(i);
                double x =  point.x;
                double y = point.y;
                points.putFloat((float) x);
                points.putShort((short) (((y - minY) / simplifiedPointsYFactor) - 32768));
                progressStatus.setProgressValue((long) i);
            }

            return new PointsFloatShortSimplifiedCurve(points, sizeInt, simplifiedPointsYFactor, minY);
        }
    }
}
