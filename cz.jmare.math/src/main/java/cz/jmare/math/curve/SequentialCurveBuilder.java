package cz.jmare.math.curve;

import cz.jmare.math.geometry.entity.Point;

/**
 * Building big curve which needn't fit to RAM. The x value in {@link SequentialCurveBuilder#add(Point)} musn't be descending
 */
public interface SequentialCurveBuilder extends AutoCloseable {
    public static long MAIN_FORMAT_MARKER = 0x06030708L;

    public static long SUB_FORMAT_MARKER_POINTS = 1;

    public static long SUB_FORMAT_MARKER_SAMPLED = 2;

    /**
     * Add point. The x value musn't be descending. Method musn't be called after building.
     * @param point
     */
    void add(Point point);

    /**
     * Get result CurveHolder.
     */
    CurveHolder build();

    @Override
    void close();
}
