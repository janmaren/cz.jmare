package cz.jmare.math.curve.importexport;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SampledCurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.util.CurveValidationUtil;
import cz.jmare.math.exception.UnableToWriteException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class TxtCurveYSaver {
    public static void exportFile(String filepath, List<CurveHolder> curveHolders) {
        exportFile(filepath, curveHolders, new NullLongProgressStatus());
    }

    public static void exportFile(String filepath, List<CurveHolder> curveHolders, ProgressStatus<Long> progressStatus) {
        try (OutputStream os = new FileOutputStream(filepath);
                Writer writer = new OutputStreamWriter(os, StandardCharsets.ISO_8859_1.toString())) {
            exportFile(writer, curveHolders, progressStatus);
        } catch (IOException e) {
            throw new UnableToWriteException("Unable to export to " + filepath, e);
        }
    }

    public static void exportFile(Writer writer, List<CurveHolder> curveHolders) {
        exportFile(writer, curveHolders, new NullLongProgressStatus());
    }

    public static void exportFile(Writer writer, List<CurveHolder> curveHolders,
            ProgressStatus<Long> progressStatus) {
        CurveValidationUtil.validationOverlapping(curveHolders);

        SampledCurveHolder curveHolder = (SampledCurveHolder) curveHolders.get(0);
        SampledCurveInfo curveInfo = curveHolder.getCurveInfo();
        double samplingInterval = curveInfo.samplingPeriod;
        BigDecimal decimalRate = new BigDecimal(1);
        BigDecimal decimalDivider = new BigDecimal(samplingInterval);
        BigDecimal resultDecimalRate = decimalRate.divide(decimalDivider, 6, RoundingMode.HALF_UP);

        double minX = curveInfo.minX;

        List<SequentialCurveReader> readers = new ArrayList<>();
        for (int i = 0; i < curveHolders.size(); i++) {
            readers.add(curveHolders.get(i).getCurveReader());
        }

        try {
            PrintWriter pw = new PrintWriter(writer);
            pw.println("Polyline Type: Y");
            pw.println("Sampling rate: " + String.format(Locale.ENGLISH, "%.13f", resultDecimalRate) + " Hz");
            if (minX != 0) {
                pw.println("X offset: " + format13(minX));
            }
            pw.println();
            progressStatus.setMaxValue(curveInfo.size);
            for (long i = 0; i < curveInfo.size; i++) {
                progressStatus.setProgressValue(i);
                for (int j = 0; j < curveHolders.size(); j++) {
                    SequentialCurveReader sequentialCurveReader = readers.get(j);
                    Point point = sequentialCurveReader.get(i);
                    if (j > 0) {
                        pw.print(",\t");
                    }
                    if (point.y >= 0) {
                        pw.print(" ");
                    }
                    pw.print(format13(point.y));
                }
                pw.println();
            }
        } finally {
            for (int i = 0; i < readers.size(); i++) {
                readers.get(i).close();
            }
        }
    }

    private static String format13(double d) {
        return String.format(Locale.ENGLISH, "%.13f", d);
    }
}
