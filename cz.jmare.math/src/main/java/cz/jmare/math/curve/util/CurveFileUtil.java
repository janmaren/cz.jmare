package cz.jmare.math.curve.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import cz.jmare.function.ConsumerCheckException;
import cz.jmare.math.curve.SequentialCurveBuilder;

public class CurveFileUtil {
    public static DataInputStream getDataInputStream(String filepath) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(filepath);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        DataInputStream dis = new DataInputStream(bufferedInputStream);
        return dis;
    }

    public static DataOutputStream getDataOutputStream(String filepath) throws FileNotFoundException {
        FileOutputStream fileOutputStream = new FileOutputStream(filepath);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        DataOutputStream dos = new DataOutputStream(bufferedOutputStream);
        return dos;
    }

    public static void consumeFile(String filepath, long formatType, ConsumerCheckException<DataInputStream> consumer) {
        try (DataInputStream dis = getDataInputStream(filepath)) {
            long marker = dis.readLong();
            if (marker != SequentialCurveBuilder.MAIN_FORMAT_MARKER) {
                dis.close();
                throw new IllegalStateException("Not valid file");
            }
            long type = dis.readLong();
            if (type != formatType) {
                dis.close();
                throw new IllegalStateException("Not valid format " + formatType);
            }
            consumer.accept(dis);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to read " + filepath, e);
        }
    }
}
