package cz.jmare.math.curve.raw;

import static cz.jmare.math.curve.util.CurveFileUtil.getDataInputStream;

import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;

public class BigPointsCurveReader implements SequentialCurveReader {
    private DataInputStream dis;

    private int size;

    /**
     * Index of point to be read
     */
    private int pointer;

    public BigPointsCurveReader(String filepath) {
        try {
            dis = getDataInputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to open " + filepath, e);
        }
        try {
            BigRawCurveHolder.checkFormat(dis);

            long size = dis.readLong();
            this.size = (int) size;
            dis.readLong();

            @SuppressWarnings("unused")
            double minX = dis.readDouble();
            @SuppressWarnings("unused")
            double maxX = dis.readDouble();
            @SuppressWarnings("unused")
            double minY = dis.readDouble();
            @SuppressWarnings("unused")
            double maxY = dis.readDouble();

            for (int i = 0; i < 8; i++) dis.readLong();

            pointer = 0;
        } catch (IOException e) {
            try {
                dis.close();
            } catch (IOException e1) {
            }
            throw new IllegalStateException("Unable to read " + filepath, e);
        }
    }

    @Override
    public Point get(long index) {
        if (index < pointer) {
            throw new IllegalArgumentException("Index must be ascending, pointer is " + pointer + ", argument is " + index);
        }
        if (index > pointer) {
            try {
                long jump = index - pointer;
                dis.skip(jump * 16);
                pointer += jump;
            } catch (IOException e) {
                throw new IllegalStateException("Unable skip to " + index);
            }
        }
        try {
            double x = dis.readDouble();
            double y = dis.readDouble();
            pointer++;
            return new Point(x, y);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read from file", e);
        }
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public void close() {
        try {
            dis.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close file");
        }
    }
}
