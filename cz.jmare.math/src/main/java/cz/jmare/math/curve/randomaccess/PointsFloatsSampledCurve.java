package cz.jmare.math.curve.randomaccess;

import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.geometry.entity.Point;

public class PointsFloatsSampledCurve implements RandomAccessCurve {
    private float[] values;

    private float sampleRate;

    private float xOffset = 0;

    public PointsFloatsSampledCurve(float[] points, float sampleRate) {
        super();
        this.values = points;
        this.sampleRate = sampleRate;
    }

    public PointsFloatsSampledCurve(float[] points, float sampleRate, float xOffset) {
        super();
        this.values = points;
        this.sampleRate = sampleRate;
        this.xOffset = xOffset;
    }

    @Override
    public Point get(int index) {
        float x = (float) (index / sampleRate + xOffset);
        float y = values[index];
        return new Point(x, y);
    }

    @Override
    public int size() {
        return values.length;
    }
}
