package cz.jmare.math.curve.util.traverser;

import java.util.function.Function;

public class FirstTraverseConsumer implements Function<Double, Boolean> {
    private Double resultX;

    @Override
    public Boolean apply(Double x) {
        resultX = x;
        return false;
    }

    public Double getResultX() {
        return resultX;
    }
}
