package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.util.DecodeRevertsUtil;

/**
 * Convert 4 bytes into point with extracted Y axe
 * Bytes must make the float type
 * Result min max values are -1 including, 1 including
 * Little endian order
 */
public class RealLittle32ToDoublePointDecoder implements DoublePointDecoder {
    public RealLittle32ToDoublePointDecoder() {
        super();
    }

    @Override
    public Point getPoint(double x, byte[] bytes, int index) {
        float f = DecodeRevertsUtil.bytesToFloatRevert(bytes, index);
        Point point = new Point(x, (double) f);
        return point;
    }

    @Override
    public int getSampleLengthBytes() {
        return 4;
    }
}
