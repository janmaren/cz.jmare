package cz.jmare.math.curve.importexport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.exception.MissingSamplingFrequencyException;
import cz.jmare.math.exception.UnableToReadException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class TxtYPicoscopeCurveLoader {
    private final static Pattern HZ_PATTERN = Pattern.compile("(?<number>[+-]?([0-9]*[.])?[0-9]+)\\s*(?<units>[kmg]?hz)", Pattern.CASE_INSENSITIVE);
    private final static Pattern TI_PATTERN = Pattern.compile("(?<number>[+-]?([0-9]*[.])?[0-9]+)\\s*(?<units>[mun]?S)", Pattern.CASE_INSENSITIVE);
    private final static Pattern OF_PATTERN = Pattern.compile("(?<number>[+-]?([0-9]*[.])?[0-9]+)\\s*(?<units>[mun]?S)", Pattern.CASE_INSENSITIVE);
    private final static Pattern V_PATTERN = Pattern.compile("[\\(\\[](?<units>[mun]?V)[\\)\\]]", Pattern.CASE_INSENSITIVE);
    private final static Pattern NUMBER_PATTERN = Pattern.compile("^\\s*(?<number>[+-]?([0-9]*[.])?[0-9]+)");
    private final static String[] SEPARATORS = {",", ";", "\\t+"};
    private final static String[] DETECTION_SEPARATORS = {",", ";", "\t"};

    @Deprecated
    public static List<CurveHolder> load(String filepath, ProgressStatus<Long> progressStatus, /* nullable */ Double samplingFrequency) throws MissingSamplingFrequencyException, UnableToReadException {
        List<String> linesList;
        try {
            linesList = FileLinesUtil.readNLines(filepath, "ISO-8859-1", 15);
        } catch (Exception e) {
            throw new UnableToReadException("Unable to read " + filepath, e);
        }
        String[] lines = linesList.toArray(new String[linesList.size()]);
        int dataRowIndex = findDataLine(lines);
        if (dataRowIndex == -1) {
            return null;
        }

        Double interval = null;

        if (samplingFrequency == null) {
            interval = findInterval(lines, dataRowIndex);
            Double frequency = null;
            if (interval == null) {
                frequency = findFrequency(lines, dataRowIndex, "FREQ");
                if (frequency == null) {
                    frequency = findFrequency(lines, dataRowIndex, null);
                    if (frequency == null) {
                        throw new MissingSamplingFrequencyException();
                    }
                }
                interval = 1.0 / frequency;
            }
        } else {
            interval = 1.0 / samplingFrequency;
        }

        String separator = detectSeparator(lines[dataRowIndex]);
        int columns;
        if (separator == null) {
            columns = 1;
        } else {
            columns = getColumns(lines[dataRowIndex], separator);
        }

        Double[] voltages = new Double[columns];
        if (dataRowIndex > 0) {
            voltages = detectVoltages(lines[dataRowIndex - 1], separator, columns);
            boolean noVoltagesLine = allItemsNull(voltages);
            double defaultVoltageFactor = 1;
            if (noVoltagesLine) {
                populateValue(voltages, defaultVoltageFactor );
            }
        } else {
            populateValue(voltages, 1);
        }

        Double offset = detectOffset(lines);

        List<BigSampledCurveBuilder> builders = new ArrayList<>();
        for (int i = 0; i < columns; i++) {
            if (voltages[i] == null) {
                continue;
            }
            builders.add(new BigSampledCurveBuilder(interval));
        }
        if (offset == null) {
            offset = 0.0;
        }

        if (builders.size() == 0) {
            return null;
        }


        progressStatus.setMaxValue(new File(filepath).length());

        try (FriendlyBufferReader friendlyBufferReader = new FriendlyBufferReader(filepath)) {
            FileLinesUtil.skipNLines(friendlyBufferReader, dataRowIndex);
            String line;
            long pointIndex = 0;
            long numRead = 0;
            while ((line = friendlyBufferReader.readLine()) != null) {
                Matcher matcher = NUMBER_PATTERN.matcher(line);
                if (!matcher.find()) {
                    break;
                } else {
                    List<Double> splitColumns = splitColumns(line, separator);
                    if (splitColumns.size() != columns) {
                        throw new IllegalArgumentException("Line hasn't count of columns " + columns + ":" + line);
                    }
                    int columnIndex = 0;
                    for (int j = 0; j < columns; j++) {
                        if (voltages[j] == null) {
                            continue;
                        }
                        BigSampledCurveBuilder bigSampledCurveBuilder = builders.get(columnIndex);
                        bigSampledCurveBuilder.add(new Point(pointIndex * interval + offset, splitColumns.get(j) * voltages[j]));
                        columnIndex++;
                    }
                    pointIndex++;
                    numRead += line.length() + 1;
                }
                progressStatus.setProgressValue(numRead);
            }

            List<CurveHolder> holders = new ArrayList<>();
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                holders.add(bigSampledCurveBuilder.build());
            }

            return holders;
        } finally {
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                bigSampledCurveBuilder.close();
            }
        }
    }

    private static boolean allItemsNull(Double[] items) {
        for (Double double1 : items) {
            if (double1 != null) {
                return false;
            }
        }

        return true;
    }

    private static void populateValue(Double[] items, double value) {
        for (int i = 0; i < items.length; i++) {
            items[i] = value;
        }
    }

    private static Double detectOffset(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String str = lines[i];
            if (!str.toUpperCase().contains("OFFSET")) {
                continue;
            }
            Matcher matcher = OF_PATTERN.matcher(str);
            if (matcher.find()) {
                String value = matcher.group("number");
                double valDouble = Double.parseDouble(value);
                String units = matcher.group("units").toUpperCase();
                if ("S".equals(units)) {
                    return valDouble * 1000;
                } else {
                    char c = units.charAt(0);
                    if (c == 'M') {
                        return valDouble;
                    } else if (c == 'U') {
                        return valDouble * 0.001;
                    } else if (c == 'N') {
                        return valDouble * 0.000001;
                    } else {
                        throw new IllegalArgumentException("Unknown units " + units);
                    }
                }
            }
        }
        return null;
    }

    private static String detectSeparator(String numbersLine) {
        for (int i = 0; i < DETECTION_SEPARATORS.length; i++) {
            String ds = DETECTION_SEPARATORS[i];
            if (numbersLine.contains(ds)) {
                return SEPARATORS[i];
            }
        }
        return null;
    }

    private static Double[] detectVoltages(String voltagesLine, String separator, int columns) {
        String[] split = separator == null ? new String[] {voltagesLine} : voltagesLine.split(separator);
        Double[] factors = new Double[columns];
        for (int i = 0; i < split.length; i++) {
            String str = split[i];
            Matcher matcher = V_PATTERN.matcher(str);
            if (matcher.find()) {
                String units = matcher.group("units");
                if ("V".equals(units)) {
                    factors[i] = 1.0;
                } else {
                    String upperCase = units.toUpperCase();
                    char c = upperCase.charAt(0);
                    if (c == 'M') {
                        factors[i] = 0.001;
                    } else if (c == 'U') {
                        factors[i] = 0.000001;
                    } else if (c == 'N') {
                        factors[i] = 0.000000001;
                    } else {
                        throw new IllegalArgumentException("Unknown units " + units);
                    }
                }
            } else {
                factors[i] = null;
            }
        }
        return factors;
    }

    private static int getColumns(String numbersLine, String separator) {
        String[] split = numbersLine.split(separator);
        return split.length;
    }

//    private static double detectVoltageFactor(String columnsHeaderLine) {
//    }

    private static Double findInterval(String[] lines, int uptoLineExcluding) {
        for (int i = 0; i < uptoLineExcluding; i++) {
            String line = lines[i];
            Matcher matcher = TI_PATTERN.matcher(line);
            if (matcher.find()) {
                String upperCase = line.toUpperCase();
                if (upperCase.contains("INTERVAL") || upperCase.contains("SAMPL")) {
                    Double value = Double.parseDouble(matcher.group("number"));
                    String units = matcher.group("units");
                    double factor = 1;
                    if (!units.equalsIgnoreCase("s")) {
                        String uUnits = units.toUpperCase();
                        char charAt = uUnits.charAt(0);
                        if (charAt == 'M') {
                            factor = 1;
                        } else if (charAt == 'U') {
                            factor = 0.001;
                        } else if (charAt == 'N') {
                            factor = 0.000001;
                        } else if (charAt == 'P') {
                            factor = 0.000000001;
                        }
                    }
                    return value * factor;
                }
            }
        }
        return null;
    }

    private static Double findFrequency(String[] lines, int uptoLineExcluding, String frequencyLineMarker) {
        for (int i = 0; i < uptoLineExcluding; i++) {
            String line = lines[i];
            Matcher matcher = HZ_PATTERN.matcher(line);
            if (matcher.find()) {
                String upperCase = line.toUpperCase();
                if (frequencyLineMarker == null || upperCase.contains(frequencyLineMarker)) {
                    Double value = Double.parseDouble(matcher.group("number"));
                    String units = matcher.group("units");
                    double factor = 1;
                    if (!units.equalsIgnoreCase("s")) {
                        String uUnits = units.toUpperCase();
                        char charAt = uUnits.charAt(0);
                        if (charAt == 'K') {
                            factor = 1000;
                        } else if (charAt == 'M') {
                            factor = 1000000;
                        } else if (charAt == 'G') {
                            factor = 1000000000;
                        }
                    }
                    return value * factor;
                }
            }
        }
        return null;
    }

    private static int findDataLine(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            Matcher matcher = NUMBER_PATTERN.matcher(line);
            if (matcher.find()) {
                return i;
            }
        }
        return -1;
    }

    private static List<Double> splitColumns(String line, String separator) {
        String[] split = separator == null ? new String[] {line} : line.split(separator);
        List<Double> result = new ArrayList<>();
        for (String string : split) {
            string = string.trim();
            if ("".equals(string)) {
                continue;
            }
            try {
                double value = Double.parseDouble(string);
                result.add(value);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Unable to parse line " + line);
            }
        }
        return result;
    }

    private static void check(CurveHolder curveHolder) {
        if (!(curveHolder instanceof SampledCurveHolder)) {
            throw new IllegalArgumentException("Curve is not sampled");
        }
    }

    public static void writeToFileY(SampledCurveHolder curveHolder, String channelName, String filename, double xUnits) throws IOException {
        check(curveHolder);
        double averageSamplingPeriodMillis = curveHolder.getCurveInfo().samplingPeriod;
        double averageSamplingPeriod = averageSamplingPeriodMillis * xUnits;

        try (SequentialCurveReader curveReader = curveHolder.getCurveReader(); OutputStream os = new FileOutputStream(filename); Writer writer = new OutputStreamWriter(os, StandardCharsets.ISO_8859_1.toString()); PrintWriter pw = new PrintWriter(writer)) {
            pw.println("Channel: " + channelName);
            pw.println("Sampling frequency: " + String.format(Locale.ENGLISH, "%.9f", (1 / averageSamplingPeriod)) + " Hz");
            pw.println("Sampling interval: " + String.format(Locale.ENGLISH, "%.9f", averageSamplingPeriodMillis) + " mS");
            pw.println("Units: V");
            if (curveReader.size() > 0) {
                pw.println(String.format(Locale.ENGLISH, "Offset: %.9f ms", curveHolder.getCurveInfo().minX));
            }
            pw.println();
            pw.println("[V]");

            for (long i = 0; i < curveReader.size(); i++) {
                Point point = curveReader.get(i);
                pw.println(String.format(Locale.ENGLISH, String.format("%.9f", point.y)));
            }
        }
    }
}
