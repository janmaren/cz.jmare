package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.util.DecodeRevertsUtil;

/**
 * Convert 3 bytes into point with extracted Y axe
 * Result min max values are -1 including, 1 including
 * Little endian order
 */
public class IntegerLittle24ToDoublePointDecoder implements DoublePointDecoder {
    public static int HALF = (int) Math.pow(2, 24 - 1);
    public IntegerLittle24ToDoublePointDecoder() {
        super();
    }

    @Override
    public Point getPoint(double x, byte[] bytes, int index) {
        int val = DecodeRevertsUtil.bytesToInt24Revert(bytes, index);
        return new Point(x, (double) val / HALF);
    }

    @Override
    public int getSampleLengthBytes() {
        return 3;
    }
}
