package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Encode a Point.y into 2 bytes
 * Values of Y axe are from -1 including to 1 excluding
 * Little endian order
 */
public class IntegerLittle16ToDoublePointEncoder implements DoublePointEncoder {
    public IntegerLittle16ToDoublePointEncoder() {
        super();
    }

    @Override
    public void encode(Point point, byte[] bytes, int offset) {
        double y = point.y;
        if (y >= 1 || y < -1) {
            throw new IllegalArgumentException("Not normalized to interval [-1, 1)");
        }
        short sh = (short) (y * 32768);
        toByta(sh, bytes, offset);
    }

    @Override
    public int getSampleLengthBytes() {
        return 2;
    }

    public static void toByta(short data, byte[] bytes, int offset) {
        bytes[offset] = (byte)((data >> 8) & 0xff);
        bytes[offset + 1] = (byte)((data >> 0) & 0xff);
    }
}
