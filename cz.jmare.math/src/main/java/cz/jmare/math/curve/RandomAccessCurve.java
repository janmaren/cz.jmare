package cz.jmare.math.curve;

import cz.jmare.math.geometry.entity.Point;

/**
 * Return a point from collection<br>
 * The {@link #get(int)} may be called with any parameter in range 0 - size (excl.).<br>
 * Using usually for simplified data because it must fit to RAM
 */
public interface RandomAccessCurve {
    Point get(int index);

    int size();
}
