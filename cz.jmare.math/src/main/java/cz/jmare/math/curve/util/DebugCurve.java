package cz.jmare.math.curve.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveUtil;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.importexport.TxtCurveXYSaver;
import cz.jmare.math.curve.importexport.TxtCurveYSaver;
import cz.jmare.progress.NullLongProgressStatus;

public class DebugCurve {
    public static String stringRepresentation(CurveHolder curveHolder) {
        try (StringWriter writer = new StringWriter()) {
            if (curveHolder instanceof SampledCurveHolder) {
                TxtCurveYSaver.exportFile(writer, CurveUtil.of(curveHolder));
            } else {
                TxtCurveXYSaver.exportFile(writer, CurveUtil.of(curveHolder));
            }
            return writer.toString();
        } catch (IOException e) {
            return null;
        }
    }

    public static String stringRepresentation(List<CurveHolder> curveHolders) {
        try (StringWriter writer = new StringWriter()) {
            if (curveHolders.get(0) instanceof SampledCurveHolder) {
                try {
                    TxtCurveYSaver.exportFile(writer, curveHolders, new NullLongProgressStatus());
                } catch (Exception e) {
                    writer.append("----------Separatelly---------");
                    writer.append(System.lineSeparator());
                    for (int i = 0; i < curveHolders.size(); i++) {
                        CurveHolder curveHolder = curveHolders.get(i);
                        if (i > 0) {
                            writer.append("------------------------");
                            writer.append(System.lineSeparator());
                        }
                        TxtCurveYSaver.exportFile(writer, CurveUtil.of(curveHolder), new NullLongProgressStatus());
                    }
                }
            } else {
                try {
                    TxtCurveYSaver.exportFile(writer, curveHolders);
                } catch (Exception e) {
                    writer.append("----------Separatelly---------");
                    writer.append(System.lineSeparator());
                    for (int i = 0; i < curveHolders.size(); i++) {
                        CurveHolder curveHolder = curveHolders.get(i);
                        if (i > 0) {
                            writer.append("------------------------");
                            writer.append(System.lineSeparator());
                        }
                        TxtCurveXYSaver.exportFile(writer, CurveUtil.of(curveHolder), new NullLongProgressStatus());
                    }
                }

            }
            return writer.toString();
        } catch (IOException e) {
            return null;
        }
    }
}
