package cz.jmare.math.curve.operation;

import cz.jmare.math.calculate.filter.WaveFilter;
import cz.jmare.math.curve.CurveUtil;
import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class FilterCurveOperations {
    public static CurveHolder filter(CurveHolder curveHolder, WaveFilter waveFilter, ProgressStatus<Long> progressStatus) {
        progressStatus.setMaxValue(curveHolder.getCurveInfo().size);
        try (SequentialCurveReader reader = curveHolder.getCurveReader(); SequentialCurveBuilder curveBuilder = CurveUtil.createNewBigCurveBuilder(curveHolder)) {
            for (long i = 0; i < reader.size(); i++) {
                Point point = reader.get(i);
                double nextFilteredY = waveFilter.nextFilteredY(point.y);
                Point filteredPoint = new Point(point.x, nextFilteredY);
                curveBuilder.add(filteredPoint);
                progressStatus.setProgressValue(i);
            }

            return curveBuilder.build();
        }
    }
}
