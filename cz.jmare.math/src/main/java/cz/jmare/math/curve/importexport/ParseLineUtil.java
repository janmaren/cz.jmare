package cz.jmare.math.curve.importexport;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.math.exception.InvalidFormatException;

public class ParseLineUtil {
    private final static Pattern NUMBER_PATTERN = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");

    public static double[] parseDoubles(String line, String separator) {
        if (separator == null) {
            List<Double> doublesList = new ArrayList<Double>();
            Matcher matcher = NUMBER_PATTERN.matcher(line);
            while (matcher.find()) {
                doublesList.add(Double.parseDouble(matcher.group()));
            }
            double[] doubles = doublesList.stream().mapToDouble(Double::doubleValue).toArray();
            return doubles;
        }
        line = line.trim();
        String[] split = line.split(separator);
        double[] doubles = new double[split.length];
        for (int i = 0; i < split.length; i++) {
            String d1 = split[i];
            try {
                doubles[i] = Double.parseDouble(d1);
            } catch (NumberFormatException e) {
                throw new InvalidFormatException("Unable to parse line " + line);
            }
        }
        return doubles;
    }
}
