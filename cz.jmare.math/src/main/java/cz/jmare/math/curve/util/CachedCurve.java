package cz.jmare.math.curve.util;

import java.util.ArrayList;

import cz.jmare.file.FriendlyAutoCloseable;
import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.geometry.entity.Point;

public class CachedCurve implements FriendlyAutoCloseable {
    /**
     * How much passed points to remember
     */
    private int historySize;

    /**
     * Cache
     */
    private ArrayList<Point> cache = new ArrayList<>();

    /**
     * real index which corresponds to [0] index in {@link #cache}
     */
    private int offset = 0;

    private SequentialCurveReader curveReader;

    public CachedCurve(CurveHolder targetCurve, int historySize) {
        super();
        this.historySize = historySize;
        this.curveReader = targetCurve.getCurveReader();
    }

    public Point get(long index) {
        if (index >= offset && index < cache.size() + offset) {
            return cache.get((int) (index - offset));
        }
        if (index < offset) {
            throw new IllegalArgumentException("Required index " + index + " but lowest possible index in history is " + offset);
        }
        int diff = (int) (index - (offset + cache.size()));
        if (diff > 2 * historySize) {
            offset = (int) (index - historySize);
            cache = new ArrayList<>();
        }

        while (offset + cache.size() < index) {
            cache.add(curveReader.get(offset + cache.size()));
        }

        Point point = curveReader.get(index);
        cache.add(point);

        if (cache.size() > 2 * historySize) {
            ArrayList<Point> newCache = new ArrayList<>();
            int fromIndex = cache.size() - historySize - 1;
            for (int i = fromIndex; i < cache.size(); i++) {
                newCache.add(cache.get(i));
            }
            offset += fromIndex;
            cache = newCache;
        }

        return point;
    }

    public long size() {
        return curveReader.size();
    }

    @Override
    public void close() {
        curveReader.close();
    }

    public static void main(String[] args) {
        ArrayList<Point> points = new ArrayList<>();
        points.add(new Point(0, 1));
        points.add(new Point(1, 1));
        points.add(new Point(2, 1));
        points.add(new Point(3, 1));
        points.add(new Point(4, 1));
        points.add(new Point(5, 1));
        points.add(new Point(6, 1));
        points.add(new Point(7, 1));
        points.add(new Point(8, 1));
        points.add(new Point(9, 1));
        points.add(new Point(10, 1));
        MemoryRawCurveHolder pointsCurve = new MemoryRawCurveHolder(points);

        CachedCurve cacheCurve = new CachedCurve(pointsCurve, 2);
//        System.out.println(cacheCurve.get(2));
        System.out.println(cacheCurve.get(3));
        System.out.println(cacheCurve.get(4));
        System.out.println(cacheCurve.get(5));
        System.out.println(cacheCurve.get(5));
        System.out.println(cacheCurve.get(3));
        System.out.println(cacheCurve.get(10));
        System.out.println(cacheCurve.get(8));
        System.out.println(cacheCurve.get(8));
        cacheCurve.close();
    }
}
