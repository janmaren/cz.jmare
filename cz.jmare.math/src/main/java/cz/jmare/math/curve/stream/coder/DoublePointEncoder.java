package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Encode a Point.y into a number of bytes to be usually used in a stream
 * The min and max values of Y are usually from -1 to 1 (both including)
 */
public interface DoublePointEncoder {
    void encode(Point point, byte[] bytes, int offset);

    int getSampleLengthBytes();
}
