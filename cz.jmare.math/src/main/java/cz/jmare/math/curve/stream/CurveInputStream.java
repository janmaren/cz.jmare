package cz.jmare.math.curve.stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.stream.coder.DoublePointEncoder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class CurveInputStream extends InputStream {
    private List<SequentialCurveReader> sequentialCurveReaders = new ArrayList<SequentialCurveReader>();
    private int curvePointer;
    private long pointPointer;
    private DoublePointEncoder doublePointEncoder;
    byte[] buffer;
    int bufferPointer;
    boolean bufferPopulated;
    private long size = -1;
    private ProgressStatus<Long> progressStatus;

    public CurveInputStream(CurveHolder curveHolder, DoublePointEncoder doublePointEncoder) {
        this(toList(curveHolder), doublePointEncoder);
    }

    public CurveInputStream(CurveHolder curveHolder, DoublePointEncoder doublePointEncoder, ProgressStatus<Long> progressStatus) {
        this(toList(curveHolder), doublePointEncoder, progressStatus);
    }

    public CurveInputStream(List<CurveHolder> curveHolders, DoublePointEncoder doublePointEncoder) {
        this(curveHolders, doublePointEncoder, new NullLongProgressStatus());
    }

    public CurveInputStream(List<CurveHolder> curveHolders, DoublePointEncoder doublePointEncoder, ProgressStatus<Long> progressStatus) {
        super();
        this.doublePointEncoder = doublePointEncoder;
        this.progressStatus = progressStatus;
        buffer = new byte[doublePointEncoder.getSampleLengthBytes()];
        for (CurveHolder curveHolder : curveHolders) {
            SequentialCurveReader curveReader = curveHolder.getCurveReader();
            sequentialCurveReaders.add(curveReader);
            CurveInfo curveInfo = curveHolder.getCurveInfo();
            if (size == -1) {
                size = curveInfo.size;
            } else {
                if (curveInfo.size != size) {
                    throw new IllegalArgumentException("Curves don't have the same size of points");
                }
            }
        }
        progressStatus.setMaxValue(size);
    }


    @Override
    public int read() {
        int ret = -1;
        if (!bufferPopulated) {
            if (pointPointer >= size) {
                return -1;
            }
            SequentialCurveReader sequentialCurveReader = sequentialCurveReaders.get(curvePointer);
            Point point = sequentialCurveReader.get(pointPointer);
            doublePointEncoder.encode(point, buffer, 0);
            curvePointer++;
            if (curvePointer >= sequentialCurveReaders.size()) {
                curvePointer = 0;
                pointPointer++;
                progressStatus.setProgressValue(pointPointer);
            }
            bufferPopulated = true;
        }
        ret = buffer[bufferPointer] & 0xff;
        bufferPointer++;
        if (bufferPointer >= doublePointEncoder.getSampleLengthBytes()) {
            bufferPopulated = false;
            bufferPointer = 0;
        }
        return ret;
    }


    @Override
    public int read(byte[] b, int off, int len) {
        if (bufferPopulated) {
            int readSize = 0;
            while (bufferPopulated && readSize < len) {
                int read = read();
                b[off + readSize] = (byte) (read & 0xff);
                readSize++;
            }
            return readSize;
        }
        if (pointPointer >= size) {
            return -1;
        }
        SequentialCurveReader sequentialCurveReader = sequentialCurveReaders.get(curvePointer);
        Point point = sequentialCurveReader.get(pointPointer);
        doublePointEncoder.encode(point, b, off);
        curvePointer++;
        if (curvePointer >= sequentialCurveReaders.size()) {
            curvePointer = 0;
            pointPointer++;
            progressStatus.setProgressValue(pointPointer);
        }
        return doublePointEncoder.getSampleLengthBytes();
    }


    @Override
    public void close() {
        for (SequentialCurveReader sequentialCurveReader : sequentialCurveReaders) {
            sequentialCurveReader.close();
        }
    }

    private static List<CurveHolder> toList(CurveHolder curveHolder) {
        ArrayList<CurveHolder> curveHolders = new ArrayList<CurveHolder>();
        curveHolders.add(curveHolder);
        return curveHolders;
    }

    @Override
    public int available() throws IOException {
        if (bufferPopulated) {
            return doublePointEncoder.getSampleLengthBytes() - bufferPointer;
        }
        if (pointPointer >= size) {
            return 0;
        }
        return doublePointEncoder.getSampleLengthBytes();
    }
}
