package cz.jmare.math.curve.randomaccess;

import java.nio.ByteBuffer;

import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SampledCurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class Sampled16SimplifiedCurve implements RandomAccessCurve {
    private ByteBuffer points;

    private double yFactor;

    private double minY;

    private double samplingPeriod;

    private double minX;

    private int size;

    public Sampled16SimplifiedCurve(ByteBuffer points, int size, double samplingPeriod, double minX, double yFactor, double minY) {
        super();
        this.points = points;
        this.samplingPeriod = samplingPeriod;
        this.minX = minX;
        this.yFactor = yFactor;
        this.minY = minY;
        this.size = size;
    }

    @Override
    public Point get(int index) {
        points.position(index * 2);
        short shortY = points.getShort();
        return new Point(index * samplingPeriod + minX, yFactor * (shortY + 32768) + minY);
    }

    @Override
    public int size() {
        return size;
    }

    public static Sampled16SimplifiedCurve getSampled16SimplifiedCurve(SampledCurveHolder curveHolder) {
        return getSampled16SimplifiedCurve(curveHolder, new NullLongProgressStatus());
    }

    public static Sampled16SimplifiedCurve getSampled16SimplifiedCurve(SampledCurveHolder curveHolder,
            ProgressStatus<Long> progressStatus) {
        SampledCurveInfo curveInfo = curveHolder.getCurveInfo();
        double minX = curveInfo.minX;
        double minY = curveInfo.minY;
        double maxY = curveInfo.maxY;

        long size = curveInfo.size;
        if (size > Integer.MAX_VALUE) {
            throw new IllegalStateException(
                    "getSampled16SimplifiedCurve may be used only for curve with max points " + Integer.MAX_VALUE);
        }
        int sizeInt = (int) size;


        try (SequentialCurveReader curveReader = curveHolder.getCurveReader();) {
            double factorY = (maxY - minY) / 65535;
            ByteBuffer points = ByteBuffer.allocate(sizeInt * 2);
            progressStatus.setMaxValue((long) size);
            for (int i = 0; i < sizeInt; i++) {
                progressStatus.setProgressValue((long) i);
                double y = curveReader.get(i).y;
                short yShort = (short) (((y - minY) / factorY) - 32768);
                points.putShort(yShort);
            }

            return new Sampled16SimplifiedCurve(points, sizeInt, curveInfo.samplingPeriod, minX, factorY, minY);
        }
    }
}
