package cz.jmare.math.curve;

public class CurveInfo {
    public long size;

    public double minX;

    public double maxX;

    public double minY;

    public double maxY;

    @Override
    public String toString() {
        return "CurveInfo [size=" + size + ", minX=" + minX + ", maxX=" + maxX + ", minY=" + minY + ", maxY=" + maxY
                + "]";
    }
}
