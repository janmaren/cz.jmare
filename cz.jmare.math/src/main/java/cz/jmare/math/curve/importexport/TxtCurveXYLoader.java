package cz.jmare.math.curve.importexport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.PolylineType;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.exception.NotAscendingXException;
import cz.jmare.math.exception.NotEnoughParametersException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.ProgressStatus;

public class TxtCurveXYLoader {
    private final static Pattern STARTS_NUMBER_PATTERN = Pattern.compile("^\\s*(?<number>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)");

    private String filepath;
    private Integer yColumnsNumber;
    private String separator;
    private Double metricSuffixX;

    private List<Double> metricSuffixY;
    private boolean separatorDetected;

    public TxtCurveXYLoader(String filepath) {
        this(new PolylineFileTypeAnalyzator(filepath));
    }

    public TxtCurveXYLoader(PolylineFileTypeAnalyzator analyzator) throws InvalidFormatException {
        if (analyzator.getPolylineType() != null && analyzator.getPolylineType() != PolylineType.RAW) {
            throw new InvalidFormatException("File " + filepath + " doesn't contain any data to be loaded or not supported format");
        }
        this.filepath = analyzator.getFilepath();
        this.separator = analyzator.getSeparator();
        this.separatorDetected = analyzator.isSeparatorDetected();
        if (separatorDetected) {
            yColumnsNumber = analyzator.getColumnsNumber() - 1;
        }
        if (analyzator.getColumnsMetrics() != null && analyzator.getColumnsMetrics().size() > 0) {
            this.metricSuffixX = analyzator.getColumnsMetrics().get(0);
            List<Double> metrics = new ArrayList<Double>(analyzator.getColumnsMetrics());
            if (metrics.size() > 1) {
                metrics.remove(0);
            }
            this.metricSuffixY = metrics;
        }
    }

    public List<CurveHolder> load(ProgressStatus<Long> progressStatus) throws InvalidFormatException, NotAscendingXException, NotEnoughParametersException {
        double metricFactorX = 1;
        if (metricSuffixX != null) {
            metricFactorX = metricSuffixX;
        }
        List<Double> metricFactorsY = new ArrayList<>();
        for (int i = 0; i < yColumnsNumber; i++) {
            metricFactorsY.add(1.0);
        }
        if (metricSuffixY != null) {
            metricFactorsY = metricSuffixY;
        }

        List<BigRawCurveBuilder> builders = new ArrayList<>();

        double lastX = -Double.MAX_VALUE;
        progressStatus.setMaxValue(new File(filepath).length());
        try (FriendlyBufferReader friendlyBufferReader = new FriendlyBufferReader(filepath)) {
            String line;
            while ((line = friendlyBufferReader.readLine()) != null) {
                if (lineStartsWithNumber(line)) {
                    break;
                }
            }
            if (line == null) {
                throw new InvalidFormatException("No data present");
            }

            if (yColumnsNumber == null) {
                yColumnsNumber = ParseLineUtil.parseDoubles(line, separator).length - 1;
            }

            for (int i = 0; i < yColumnsNumber; i++) {
                builders.add(new BigRawCurveBuilder());
            }

            double lastMetricFactorY = 1;
            List<Double> metricFactorYs = new ArrayList<Double>();
            for (int i = 0; i < yColumnsNumber; i++) {
                if (i < metricFactorsY.size()) {
                    lastMetricFactorY = metricFactorsY.get(i);
                }
                metricFactorYs.add(lastMetricFactorY);
            }

            long numRead = 0;
            do {
                line = line.trim();
                if ("".equals(line)) {
                    continue;
                }
                double[] doubles = ParseLineUtil.parseDoubles(line, separator);
                if (doubles.length != yColumnsNumber + 1) {
                    throw new InvalidFormatException("Invalid number of columns on line " + line);
                }

                double x = Double.valueOf(doubles[0]) * metricFactorX;

                // ascending check
                if (x < lastX) {
                    throw new NotAscendingXException("Not a raw file because X value is not ascending, there is " + lastX + " > " + x);
                }
                lastX = x;

                for (int j = 0; j < yColumnsNumber; j++) {
                    BigRawCurveBuilder bigPointsCurveBuilder = builders.get(j);
                    bigPointsCurveBuilder.add(new Point(x, doubles[j + 1] * metricFactorYs.get(j)));
                }

                numRead += line.length() + 1;
                progressStatus.setProgressValue(numRead);
            } while ((line = friendlyBufferReader.readLine()) != null);
            List<CurveHolder> holders = new ArrayList<>();
            for (BigRawCurveBuilder bigPointsCurveBuilder : builders) {
                holders.add(bigPointsCurveBuilder.build());
            }

            return holders;
        } finally {
            for (BigRawCurveBuilder bigPointsCurveBuilder : builders) {
                bigPointsCurveBuilder.close();
            }
        }
    }

    public boolean allValuesPresent() {
        if (metricSuffixX == null) {
            return false;
        }

        if (metricSuffixY == null || metricSuffixY.size() == 0) {
            return false;
        }

        return true;
    }

    public void setMetricSuffixY(List<Double> metricSuffixY) {
        this.metricSuffixY = metricSuffixY;
    }

    public void setMetricSuffixY(double metricSuffixY) {
        this.metricSuffixY = new ArrayList<Double>();
        this.metricSuffixY.add(metricSuffixY);
    }

    public void setMetricSuffixX(double metricSuffixX) {
        this.metricSuffixX = metricSuffixX;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    private static boolean lineStartsWithNumber(String line) {
        return STARTS_NUMBER_PATTERN.matcher(line).find();
    }
}
