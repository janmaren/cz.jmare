package cz.jmare.math.curve.sampled;

import java.util.List;

import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SampledCurveInfo;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.exception.NotAtLeastTwoPointsException;
import cz.jmare.math.geometry.entity.Point;

/**
 * Sampled curve stored in RAM
 */
public class MemorySampledCurveHolder implements SampledCurveHolder, RandomAccessCurve {
    private double[] yValues;

    private double xOffset = 0;

    private double samplingPeriod;

    private SampledCurveInfo curveInfo;

    public MemorySampledCurveHolder(List<Double> yValues, double samplingPeriod, double xOffset) {
        if (yValues.size() < 2) {
            throw new NotAtLeastTwoPointsException();
        }
        this.yValues = yValues.stream().mapToDouble(Double::doubleValue).toArray();
        this.samplingPeriod = samplingPeriod;
        this.xOffset = xOffset;
    }

    public MemorySampledCurveHolder(List<Double> yValues, double samplingPeriod) {
        if (yValues.size() < 2) {
            throw new NotAtLeastTwoPointsException();
        }
        this.yValues = yValues.stream().mapToDouble(Double::doubleValue).toArray();
        this.samplingPeriod = samplingPeriod;
    }

    public MemorySampledCurveHolder(float[] yValues, double samplingPeriod, double xOffset) {
        if (yValues.length < 2) {
            throw new NotAtLeastTwoPointsException();
        }
        this.yValues = new double[yValues.length];
        for (int i = 0; i < yValues.length; i++) {
            this.yValues[i] = ((double) yValues[i]);
        }
        this.samplingPeriod = samplingPeriod;
        this.xOffset = xOffset;
    }

    public MemorySampledCurveHolder(double[] samples, double samplingPeriod, double xOffset) {
        if (samples.length < 2) {
            throw new NotAtLeastTwoPointsException();
        }
        this.yValues = samples;
        this.samplingPeriod = samplingPeriod;
        this.xOffset = xOffset;
    }

    @Override
    public Point get(int index) {
        return new Point(xOffset + index * samplingPeriod, yValues[index]);
    }

    @Override
    public int size() {
        return yValues.length;
    }

    double getSamplingPeriod() {
        return samplingPeriod;
    }

    @Override
    public SequentialCurveReader getCurveReader() {
        return new SampledCurveReader();
    }

    public class SampledCurveReader implements SequentialCurveReader {
        @Override
        public Point get(long index) {
            if (index > Integer.MAX_VALUE) {
                throw new IndexOutOfBoundsException("SampledCurveReader supports only numbers up to 2147483647");
            }
            return MemorySampledCurveHolder.this.get((int) index);
        }

        @Override
        public long size() {
            return MemorySampledCurveHolder.this.size();
        }

        @Override
        public void close() {
        }
    }

    @Override
    public SampledCurveInfo getCurveInfo() {
        if (curveInfo == null) {
            populateCurveInfo();
        }
        return curveInfo;
    }

    private void populateCurveInfo() {
        SampledCurveInfo curveInfo = new SampledCurveInfo();
        curveInfo.size = yValues.length;
        curveInfo.minX = xOffset;
        curveInfo.maxX = xOffset + (yValues.length - 1) * samplingPeriod;
        double maxY = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        for (int i = 0; i < yValues.length; i++) {
            if (this.yValues[i] < minY) {
                minY = this.yValues[i];
            }
            if (this.yValues[i] > maxY) {
                maxY = this.yValues[i];
            }
        }
        curveInfo.minY = minY;
        curveInfo.maxY = maxY;
        curveInfo.samplingPeriod = samplingPeriod;
        this.curveInfo = curveInfo;
    }
}
