package cz.jmare.math.curve.sampled;

import java.util.ArrayList;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;

/**
 * Create SampledCurveBuilder stored in RAM.
 * For small curves.
 */
public class MemorySampledCurveBuilder implements SequentialCurveBuilder {
    private ArrayList<Double> yValues = new ArrayList<Double>();

    private Double minX;

    private Double maxX;

    private Double samplingPeriod;

    public MemorySampledCurveBuilder() {
        super();
    }

    public MemorySampledCurveBuilder(Double samplingPeriod) {
        this(samplingPeriod, null);
    }

    /**
     * Create curve with sampling period and beginning X
     * @param samplingPeriod
     * @param minX starting X coordinate or null to detect by X of first point
     */
    public MemorySampledCurveBuilder(Double samplingPeriod, Double minX) {
        super();
        this.samplingPeriod = samplingPeriod;
        this.minX = minX;
    }

    @Override
    public void add(Point point) {
        if (yValues.size() == 0) {
            if (minX != null) {
                if (!EpsCompare.equals(minX, point.x)) {
                    throw new IllegalArgumentException("MinX should be " + minX + " but is " + point.x);
                }
            }
            minX = point.x;
            yValues = new ArrayList<>();
        }

        maxX = point.x;
        yValues.add(point.y);
    }

    @Override
    public CurveHolder build() {
        double diff = maxX - minX;
        double calculatedSamplingPeriod = (maxX - minX) / (yValues.size() - 1);
        if (samplingPeriod == null) {
            samplingPeriod = calculatedSamplingPeriod;
        } else {
            if (!EpsCompare.equals(samplingPeriod, calculatedSamplingPeriod)) {
                throw new IllegalStateException("Entered data don't have required sampling period " + samplingPeriod + " but " + calculatedSamplingPeriod);
            }
        }

        maxX = minX + diff;
        if (samplingPeriod == null) {
            samplingPeriod = (maxX - minX) / (yValues.size() - 1);
        }
        return new MemorySampledCurveHolder(yValues, samplingPeriod, minX);
    }

    @Override
    public void close() {
    }

    public static CurveHolder createSampledCurveHolder(double samplingPeriod, double minX, double... y) {
        int index = 0;
        try (MemorySampledCurveBuilder builder = new MemorySampledCurveBuilder(samplingPeriod, minX)) {
            while (index < y.length) {
                Point point = new Point(index * samplingPeriod + minX, y[index]);
                builder.add(point);
                index++;
            }
            return builder.build();
        }
    }
}
