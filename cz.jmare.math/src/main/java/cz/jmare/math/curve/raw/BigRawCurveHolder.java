package cz.jmare.math.curve.raw;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.util.CurveFileUtil;

public class BigRawCurveHolder implements CurveHolder {
    private String filepath;

    private CurveInfo curveInfo;

    public BigRawCurveHolder(String filepath) {
        super();
        this.filepath = filepath;
        if (!new File(filepath).isFile()) {
            throw new IllegalArgumentException(filepath + " doesn't exist");
        }
    }

    @Override
    public SequentialCurveReader getCurveReader() {
        return new BigPointsCurveReader(filepath);
    }

    static void checkFormat(DataInputStream dis) throws IOException {
        long marker = dis.readLong();
        if (marker != SequentialCurveBuilder.MAIN_FORMAT_MARKER) {
            dis.close();
            throw new IllegalStateException("Not valid file");
        }
        long type = dis.readLong();
        if (type != SequentialCurveBuilder.SUB_FORMAT_MARKER_POINTS) {
            dis.close();
            throw new IllegalStateException("Not valid sampled file");
        }
    }

    @Override
    public CurveInfo getCurveInfo() {
        if (curveInfo == null) {
            populateCurveInfo();
        }
        return curveInfo;
    }

    private void populateCurveInfo() {
        CurveInfo curveInfo = new CurveInfo();
        CurveFileUtil.consumeFile(filepath, SequentialCurveBuilder.SUB_FORMAT_MARKER_POINTS, dis -> {
            curveInfo.size = dis.readLong();
            dis.readLong();
            curveInfo.minX = dis.readDouble();
            curveInfo.maxX = dis.readDouble();
            curveInfo.minY = dis.readDouble();
            curveInfo.maxY = dis.readDouble();
        });
        this.curveInfo = curveInfo;
    }
}
