package cz.jmare.math.curve.importexport;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.curve.stream.coder.DoublePointDecoder;
import cz.jmare.math.curve.stream.coder.IntegerLittle16ToDoublePointDecoder;
import cz.jmare.math.curve.stream.coder.IntegerLittle24ToDoublePointDecoder;
import cz.jmare.math.curve.stream.coder.IntegerLittle32ToDoublePointDecoder;
import cz.jmare.math.curve.stream.coder.RealLittle32ToDoublePointDecoder;
import cz.jmare.math.curve.stream.coder.RealLittle64ToDoublePointDecoder;
import cz.jmare.math.exception.DataCorruptedException;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.exception.UnableToReadException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

// http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
public class WavReadUtil {
    /**
     * Load curve from wav file. Number of returned curves corresponds with channels. All curves start
     * with x=0.
     * @param filenamePath full path of file to load
     * @return
     * @throws InvalidFormatException when format uses not supported number of bits for sample
     * @throws DataCorruptedException when some data in header are wrong
     * @throws UnableToReadException unable erro during reading
     */
    public static List<CurveHolder> loadWave(String filenamePath) throws InvalidFormatException, DataCorruptedException, UnableToReadException {
        return loadWave(filenamePath, new NullLongProgressStatus());
    }

    /**
     * Load curve from wav file. Number of returned curves corresponds with channels. All curves start
     * with x=0.
     * @param filenamePath full path of file to load
     * @param readCurveParameters how to load curve
     * @param progressStatus progress to update during reading
     * @return
     * @throws InvalidFormatException when format uses not supported number of bits for sample
     * @throws DataCorruptedException when some data in header are wrong
     * @throws UnableToReadException unable erro during reading
     */
    public static List<CurveHolder> loadWave(String filenamePath, ProgressStatus<Long> progressStatus) throws InvalidFormatException, DataCorruptedException, UnableToReadException {
        List<BigSampledCurveBuilder> builders = new ArrayList<>();

        long totalFramesRead = 0;
        File fileIn = new File(filenamePath);

        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(fileIn);

            AudioFormat format = audioInputStream.getFormat();
            if (format.isBigEndian()) {
                throw new InvalidFormatException("Waveform is in big endian which is not supported to load");
            }
            int sampleSizeInBits = format.getSampleSizeInBits();
            int sampleSizeinBytes = (sampleSizeInBits + 7) / 8;

            DoublePointDecoder doublePointDecoder = null;
            if (format.getEncoding() == Encoding.PCM_SIGNED) {
                if (sampleSizeinBytes == 2) {
                    doublePointDecoder = new IntegerLittle16ToDoublePointDecoder();
                } else if (sampleSizeinBytes == 3) {
                    doublePointDecoder = new IntegerLittle24ToDoublePointDecoder();
                } else if (sampleSizeinBytes == 4) {
                    doublePointDecoder = new IntegerLittle32ToDoublePointDecoder();
                } else {
                    throw new InvalidFormatException("PCM signed format with " + sampleSizeInBits + "bits depth not supported");
                }
            } else if (format.getEncoding() == Encoding.PCM_FLOAT) {
                if (sampleSizeinBytes == 4) {
                    doublePointDecoder = new RealLittle32ToDoublePointDecoder();
                } else if (sampleSizeinBytes == 8) {
                    doublePointDecoder = new RealLittle64ToDoublePointDecoder();
                } else {
                    throw new InvalidFormatException("PCM float format with " + sampleSizeInBits + "bits depth not supported");
                }
            } else {
                throw new InvalidFormatException("Encoding " + format.getEncoding() + " not supported");
            }

            long sampleRate = (int) format.getSampleRate();
            double samplingPeriod = 1.0 / sampleRate;

            int bytesPerFrame = format.getFrameSize();
            int channels = format.getChannels();

            if (bytesPerFrame == AudioSystem.NOT_SPECIFIED) {
                bytesPerFrame = sampleSizeinBytes * channels;
            }

            if (channels * sampleSizeinBytes != bytesPerFrame) {
                throw new DataCorruptedException("Wav file looks corrupted");
            }

            int numBytes = 1024 * bytesPerFrame;
            byte[] audioBytes = new byte[numBytes];

            for (int i = 0; i < channels; i++) {
                builders.add(new BigSampledCurveBuilder(samplingPeriod));
            }

            progressStatus.setMaxValue(fileIn.length());

            double xOffset = 0; // has no sense because unable to change it
            int numBytesRead = 0;
            int numFramesRead = 0;
            while ((numBytesRead = audioInputStream.read(audioBytes)) != -1) {
                numFramesRead = numBytesRead / bytesPerFrame;
                for (int i = 0; i < numFramesRead; i++) {
                    for (int j = 0; j < channels; j++) {
                        int offset = i * bytesPerFrame + j * sampleSizeinBytes;

                        Point point = doublePointDecoder.getPoint(xOffset + samplingPeriod * (totalFramesRead + i), audioBytes, offset);
                        BigSampledCurveBuilder bigSampledCurveBuilder = builders.get(j);
                        bigSampledCurveBuilder.add(point);

                    }
                }
                totalFramesRead += numFramesRead;
                progressStatus.setProgressValue(totalFramesRead * bytesPerFrame);
            }
            List<CurveHolder> holders = new ArrayList<>();
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                holders.add(bigSampledCurveBuilder.build());
            }

            return holders;
        } catch (IOException | UnsupportedAudioFileException e) {
            throw new UnableToReadException("Problems to read file " + filenamePath, e);
        } finally {
            for (BigSampledCurveBuilder bigSampledCurveBuilder : builders) {
                bigSampledCurveBuilder.close();
            }
        }
    }
}
