package cz.jmare.math.curve.operation;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.interpol.InterpolatedCurve;
import cz.jmare.math.curve.interpol.LinearInterpolatedCurve;
import cz.jmare.math.curve.interpol.MonoSplineInterpolatedCurve;
import cz.jmare.math.curve.sampled.BigSampledCurveBuilder;
import cz.jmare.math.exception.NotSuccessfulOperationException;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class SampleCurveOperations {
    public static CurveHolder sampleCurveIntoGrid(double samplingPeriod, InterpolatedCurve linearInterpolatedCurve, ProgressStatus<Long> progressStatus) {
        double minX = linearInterpolatedCurve.getMinX();
        double maxX = linearInterpolatedCurve.getMaxX();

        long i = (long) (minX / samplingPeriod);
        if (EpsCompare.doubleLessThanDouble(i * samplingPeriod, minX)) {
            i++;
        }
        long toExcluding = (long) (maxX / samplingPeriod);
        if (EpsCompare.doubleLessOrEqualsThanDouble(toExcluding * samplingPeriod, maxX)) {
            toExcluding++;
        }
        if (toExcluding - i <= 1) {
            throw new NotSuccessfulOperationException("There is no point after sampling");
        }
        progressStatus.setMaxValue(toExcluding);
        try (BigSampledCurveBuilder curveBuilder = new BigSampledCurveBuilder(samplingPeriod)) {
            while (i < toExcluding) {
                double x = i * samplingPeriod;
                Double value = linearInterpolatedCurve.getY(x);
                curveBuilder.add(new Point(x, value));
                i++;
                progressStatus.setProgressValue(i);
            }

            return curveBuilder.build();
        }
    }

    public static CurveHolder sampleCurveFromBeginning(double samplingPeriod, InterpolatedCurve linearInterpolatedCurve, ProgressStatus<Long> progressStatus) {
        double minX = linearInterpolatedCurve.getMinX();
        double maxX = linearInterpolatedCurve.getMaxX();

        long count = (long) ((maxX - minX) / samplingPeriod) - 1;
        progressStatus.setMaxValue(count);
        try (BigSampledCurveBuilder curveBuilder = new BigSampledCurveBuilder(samplingPeriod)) {
            double x = minX;
            long i = 0;
            while (x <= maxX) {
                Double value = linearInterpolatedCurve.getY(x);
                curveBuilder.add(new Point(x, value));
                progressStatus.setProgressValue(i);
                i++;
                x = minX + i * samplingPeriod;
            }

            return curveBuilder.build();
        }
    }

    public static CurveHolder sampleToGridByLinearInterpolation(CurveHolder curveHolder, double samplingPeriod, ProgressStatus<Long> progressStatus) {
        try (LinearInterpolatedCurve interpolatedCurve = new LinearInterpolatedCurve(curveHolder)) {
            return sampleCurveIntoGrid(samplingPeriod, interpolatedCurve, progressStatus);
        }
    }

    public static CurveHolder sampleToGridByMonoSplineInterpolation(CurveHolder curveHolder, double samplingPeriod, ProgressStatus<Long> progressStatus) {
        try (MonoSplineInterpolatedCurve interpolatedCurve = new MonoSplineInterpolatedCurve(curveHolder)) {
            return sampleCurveIntoGrid(samplingPeriod, interpolatedCurve, progressStatus);
        }
    }

    public static CurveHolder sampleFromBeginningByLinearInterpolation(CurveHolder curveHolder, double samplingPeriod, ProgressStatus<Long> progressStatus) {
        try (LinearInterpolatedCurve interpolatedCurve = new LinearInterpolatedCurve(curveHolder)) {
            return sampleCurveFromBeginning(samplingPeriod, interpolatedCurve, progressStatus);
        }
    }

    public static CurveHolder sampleFromBeginningMonoSplineInterpolation(CurveHolder curveHolder, double samplingPeriod) {
        return sampleFromBeginningByLinearInterpolation(curveHolder, samplingPeriod, new NullLongProgressStatus());
    }

    public static CurveHolder sampleFromBeginningMonoSplineInterpolation(CurveHolder curveHolder, double samplingPeriod, ProgressStatus<Long> progressStatus) {
        try (MonoSplineInterpolatedCurve interpolatedCurve = new MonoSplineInterpolatedCurve(curveHolder)) {
            return sampleCurveFromBeginning(samplingPeriod, interpolatedCurve, progressStatus);
        }
    }

}
