package cz.jmare.math.curve.util.traverser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

public class FrequencyCalculator implements BiFunction<Double, Boolean, Boolean> {
    /**
     * Factor how much may half wave differ from opposite half wave
     */
    private final static double NEIGHBOUR = 0.5;
    private final static double NEIGHBOUR_LEFT = 1 - NEIGHBOUR;
    private final static double NEIGHBOUR_RIGHT = 1 + NEIGHBOUR;

    private List<Record> records = new ArrayList<>();

    private Double maxFrequency;

    @Override
    public Boolean apply(Double x, Boolean ascending) {
        Record record = new Record();
        record.x = x;
        record.ascending = ascending;

        if (records.size() == 0) {
            records.add(record);
        } else {
            Record previousRecord = records.get(records.size() - 1);
            if (previousRecord.ascending == ascending) {
                throw new IllegalArgumentException("Not switching up down now is " + ascending + " at point x " + " but the previous was also " + previousRecord.ascending + " at point " + previousRecord.x);
            }
            double period = x - previousRecord.x;
            double realFreq = 1 / (period * 2);
            if (maxFrequency != null) {
                if (realFreq < maxFrequency) {
                    records.add(record);
                } else {
                    records.remove(records.size() - 1);
                }
            } else {
                records.add(record);
            }
        }
        return true;
    }

    public double getFrequency() {
        if (records.size() == 0) {
            return Double.NaN;
        }
        if (maxFrequency != null) {
            System.err.println("maxFrequency should be null");
        }
        return calculateFrequency();
    }

    private double calculateFrequency() {
        List<Double> periods = new ArrayList<Double>();
        for (int i = 0; i < records.size() - 2; i++) {
            Record record0 = records.get(i);
            Record record1 = records.get(i + 1);

            boolean direction = record0.ascending;
            if (record1.ascending == direction) {
                throw new RuntimeException("Not oscillating ascending descendig");
            }
            double period1 = record1.x - record0.x;
            double halfWave = getHalfWave(i + 1, period1, record1.ascending);
            if (halfWave > 0) {
                periods.add(period1 + halfWave);
            }
        }
        if (periods.size() == 0) {
            return Double.NaN;
        }
        Collections.sort(periods, Collections.reverseOrder());
        double sumPeriods = periods.get(0);
        int number = 1;
        double lastPeriod = periods.get(0);
        int i = 1;
        while (i < periods.size() && periods.get(i) * 1.1 > lastPeriod && periods.get(i) * 0.9 < lastPeriod) {
            sumPeriods += periods.get(i);
            number++;
            lastPeriod = periods.get(i);
            i++;
        }

        return 1 / (sumPeriods / (double) number);
    }

    private double getHalfWave(int index, double period, boolean asc) {
        for (int i = index; i < records.size() - 1; i++) {
            Record record = records.get(i);
            Record record1 = records.get(i + 1);
            double diff = record1.x - record.x;
            if (diff > period * NEIGHBOUR_LEFT) {
                if (diff < period * NEIGHBOUR_RIGHT) {
                    if (record.ascending && !asc) {
                        return record.x - records.get(index).x;
                    } else {
                        return record1.x - records.get(index).x;
                    }
                } else {
                    return 0;
                }
            }
        }
        return 0;
    }

    public void setMaxFrequency(Double maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    static class Record {
        double x;
        boolean ascending;
    }
}
