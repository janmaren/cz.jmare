package cz.jmare.math.curve.sampled;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;

import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SampledCurveInfo;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.util.CurveFileUtil;

public class BigSampledCurveHolder implements SampledCurveHolder {

    private String filepath;
    private SampledCurveInfo curveInfo;

    public BigSampledCurveHolder(String filepath) {
        this.filepath = filepath;
        if (!new File(filepath).isFile()) {
            throw new IllegalArgumentException(filepath + " doesn't exist");
        }
    }

    @Override
    public SequentialCurveReader getCurveReader() {
        return new BigSampledCurveReader(filepath);
    }

    static void checkFormat(DataInputStream dis) throws IOException {
        long marker = dis.readLong();
        if (marker != SequentialCurveBuilder.MAIN_FORMAT_MARKER) {
            dis.close();
            throw new IllegalStateException("Not valid file");
        }
        long type = dis.readLong();
        if (type != SequentialCurveBuilder.SUB_FORMAT_MARKER_SAMPLED) {
            dis.close();
            throw new IllegalStateException("Not valid sampled file");
        }
    }

    @Override
    public SampledCurveInfo getCurveInfo() {
        if (curveInfo == null) {
            populateCurveInfo();
        }
        return curveInfo;
    }

    private void populateCurveInfo() {
        SampledCurveInfo curveInfo = new SampledCurveInfo();
        CurveFileUtil.consumeFile(filepath, SequentialCurveBuilder.SUB_FORMAT_MARKER_SAMPLED, dis -> {
            curveInfo.size = dis.readLong();
            dis.readLong();
            curveInfo.minX = dis.readDouble();
            curveInfo.maxX = dis.readDouble();
            curveInfo.minY = dis.readDouble();
            curveInfo.maxY = dis.readDouble();
            curveInfo.samplingPeriod = dis.readDouble();
        });
        this.curveInfo = curveInfo;
    }
}
