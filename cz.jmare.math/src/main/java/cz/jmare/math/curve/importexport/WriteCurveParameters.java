package cz.jmare.math.curve.importexport;

public class WriteCurveParameters {
    /**
     * e.g. millis are 0.001
     */
    public double xScaleFactor = 1;

    /**
     * When false the first X coordinate must start with 0.
     * Otherwise it accept also shifted curve but it will
     * be moved to 0 coordinate in result file
     */
    public boolean xNeedntStartWithZero = false;

    /**
     * Vertical resolution in bits, like 16, 24, 32, 64
     */
    public Integer sampleSizeInBits;

    public WriteCurveParameters withXScaleFactor(double xScaleFactor) {
        this.xScaleFactor = xScaleFactor;
        return this;
    }

    public WriteCurveParameters withXNeedntStartWithZero(boolean xNeedntStartWithZero) {
        this.xNeedntStartWithZero = xNeedntStartWithZero;
        return this;
    }

    public WriteCurveParameters withSampleSizeInBits(Integer sampleSizeInBits) {
        this.sampleSizeInBits = sampleSizeInBits;
        return this;
    }
}
