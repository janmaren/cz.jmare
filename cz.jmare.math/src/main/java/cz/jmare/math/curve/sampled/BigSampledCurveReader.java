package cz.jmare.math.curve.sampled;

import static cz.jmare.math.curve.util.CurveFileUtil.getDataInputStream;

import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;

public class BigSampledCurveReader implements SequentialCurveReader {
    private DataInputStream dis;

    private int size;

    /**
     * Index of point to be read
     */
    private int pointer;

    private double samplingPeriod;

    private double minX;

    public BigSampledCurveReader(String filepath) {
        try {
            dis = getDataInputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to open " + filepath, e);
        }
        try {
            BigSampledCurveHolder.checkFormat(dis);
            long size = dis.readLong();
            this.size = (int) size;

            dis.readLong(); // reserved

            minX = dis.readDouble();
            dis.readDouble(); // maxX not needed
            dis.readDouble(); // minY not needed
            dis.readDouble(); // maxY not needed
            samplingPeriod = dis.readDouble(); // sampling period
            dis.readLong();
            dis.readLong();
            dis.readLong();
            dis.readLong();
            dis.readLong();
            dis.readLong();
            dis.readLong();

            pointer = 0;
        } catch (IOException e) {
            try {
                dis.close();
            } catch (IOException e1) {
            }
            throw new IllegalStateException("Unable to read " + filepath, e);
        }
    }

    @Override
    public Point get(long index) {
        if (index < pointer) {
            throw new IllegalArgumentException("Index must be ascending, current index is already " + pointer + " but required index is " + index);
        }
        if (index > pointer) {
            try {
                long jump = index - pointer;
                dis.skip(jump * 8);
                pointer += jump;
            } catch (IOException e) {
                throw new IllegalStateException("Unable skip to " + index);
            }
        }
        try {
            double x = minX + index * samplingPeriod;
            double y = dis.readDouble();
            pointer++;
            return new Point(x, y);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read from file", e);
        }
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public void close() {
        try {
            dis.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close file");
        }
    }
}
