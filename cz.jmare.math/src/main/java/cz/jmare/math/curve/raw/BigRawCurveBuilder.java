package cz.jmare.math.curve.raw;

import static cz.jmare.math.curve.util.CurveFileUtil.getDataOutputStream;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveBuilder;
import cz.jmare.math.exception.NotSuccessfulOperationException;
import cz.jmare.math.geometry.entity.Point;

public class BigRawCurveBuilder implements SequentialCurveBuilder {
    private String filepath;

    private DataOutputStream dos;

    private long pointsSize;

    private double minY = Double.MAX_VALUE;

    private double maxY = -Double.MAX_VALUE;

    private double minX = Double.MAX_VALUE;

    private double maxX = -Double.MAX_VALUE;

    public BigRawCurveBuilder() {
        this(createTmpFile());
    }

    public BigRawCurveBuilder(File file) {
        filepath = file.toString();
        try {
            dos = getDataOutputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to create " + filepath, e);
        }
        try {
            dos.writeLong(MAIN_FORMAT_MARKER);// marker
            dos.writeLong(SUB_FORMAT_MARKER_POINTS); // type
            dos.writeLong(-1L);// length - replace later
            dos.writeLong(0);// reserved
            dos.writeDouble(Double.NaN);// minX
            dos.writeDouble(Double.NaN);// maxX
            dos.writeDouble(Double.NaN);// minY
            dos.writeDouble(Double.NaN);// maxY

            for (int i = 0; i < 8; i++) dos.writeLong(0);
        } catch (IOException e) {
        }
    }

    @Override
    public void add(Point point) {
        try {
            dos.writeDouble(point.x);
            dos.writeDouble(point.y);

            if (pointsSize == 0) {
                minX = point.x;
            }
            maxX = point.x;

            if (point.y < minY) {
                minY = point.y;
            }
            if (point.y > maxY) {
                maxY = point.y;
            }

            pointsSize++;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to write to " + filepath, e);
        }
    }

    @Override
    public CurveHolder build() {
        try {
            dos.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close " + filepath);
        }
        if (pointsSize < 2) {
            throw new NotSuccessfulOperationException("Result curve doesn't contain at least 2 points");
        }
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(filepath, "rw")) {
            randomAccessFile.skipBytes(16);
            randomAccessFile.writeLong(pointsSize);
            randomAccessFile.skipBytes(8);
            randomAccessFile.writeDouble(minX);
            randomAccessFile.writeDouble(maxX);
            randomAccessFile.writeDouble(minY);
            randomAccessFile.writeDouble(maxY);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to write to RandomAccessFile " + filepath, e);
        }
        return new BigRawCurveHolder(filepath);
    }

    @Override
    public void close() {
        try {
            dos.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close " + filepath);
        }
    }

    public static CurveHolder createBigCurveHolder(double... xy) {
        if (xy.length % 2 != 0) {
            throw new IllegalArgumentException("Must be even number representing x, y, x, y, ...");
        }
        int index = 0;
        try (BigRawCurveBuilder builder = new BigRawCurveBuilder()) {
            while (index < xy.length) {
                Point point = new Point(xy[index++], xy[index++]);
                builder.add(point);
            }
            return builder.build();
        }
    }

    private static File createTmpFile() {
        try {
            File createTempFile = File.createTempFile("bigpoints", ".dat");
            createTempFile.deleteOnExit();
            return createTempFile;
        } catch (IOException e1) {
            throw new IllegalStateException("Unable to create temporary file", e1);
        }
    }
}
