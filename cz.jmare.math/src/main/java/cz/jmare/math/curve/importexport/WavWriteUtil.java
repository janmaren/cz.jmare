package cz.jmare.math.curve.importexport;

import java.io.BufferedInputStream;
import java.io.File;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.stream.CurveInputStream;
import cz.jmare.math.curve.stream.coder.RealLittle64FromDoublePointEncoder;
import cz.jmare.math.curve.stream.coder.DoublePointEncoder;
import cz.jmare.math.curve.stream.coder.RealLittle32FromDoublePointEncoder;
import cz.jmare.math.curve.stream.coder.IntegerLittle16ToDoublePointEncoder;
import cz.jmare.math.curve.stream.coder.IntegerLittleAnyFromDoublePointEncoder;
import cz.jmare.math.curve.util.CurveValidationUtil;
import cz.jmare.math.exception.CurveNotSampledException;
import cz.jmare.math.exception.CurvesPointsNotFitException;
import cz.jmare.math.exception.CurvesWithDifferentSamplingRateException;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.exception.NotNormalizedException;
import cz.jmare.progress.NullLongProgressStatus;
import cz.jmare.progress.ProgressStatus;

public class WavWriteUtil {
    /**
     * Save curve(s) to wav. Note the curves must have the same sampling rate, beginning a end (x) and
     * the Y coordinate must be normalized to interval [-1, 1)
     * Also note the sampling rate is already present in curves. Make sure some commons frequencies are
     * used, like 44100, 48000, 88200, 96000
     * @param filenamePath name of the wav file - full path
     * @param curveHolders one or more curves to be saved
     * @param writeCurveParameters how to write
     * @throws CurveNotSampledException Not sampled curve present
     * @throws CurvesPointsNotFitException Curves don't have the same beginning or end
     * @throws CurvesWithDifferentSamplingRateException Curves don't have the same sampling rate
     * @throws InvalidFormatException When used not supported sampleSizeInBits
     * @throws NotNormalizedException When there is a Y value less than -1 or equals or greater than +1
     */
    public static void saveWave(String filenamePath, List<CurveHolder> curveHolders, WriteCurveParameters writeCurveParameters)
            throws CurveNotSampledException, CurvesPointsNotFitException, CurvesWithDifferentSamplingRateException,
            InvalidFormatException, NotNormalizedException {
        saveWave(filenamePath, curveHolders, writeCurveParameters, new NullLongProgressStatus());
    }

    /**
     * Save curve(s) to wav. Note the curves must start with x=0 and have the same sampling rate, beginning a end (x) and
     * the Y coordinate must be normalized to interval [-1, 1).
     * Also note the sampling rate is already present in curves. Make sure some commons frequencies are
     * used, like 44100, 48000, 88200, 96000
     * @param filenamePath name of the wav file - full path
     * @param curveHolders one or more curves to be saved
     * @param writeCurveParameters how to write
     * @param progressStatus
     * @throws CurveNotSampledException Not sampled curve present
     * @throws CurvesPointsNotFitException Curves don't have the same beginning or end
     * @throws CurvesWithDifferentSamplingRateException Curves don't have the same sampling rate
     * @throws InvalidFormatException When used not supported sampleSizeInBits
     * @throws NotNormalizedException When there is a Y value less than -1 or equals or greater than +1
     */
    public static void saveWave(String filenamePath, List<CurveHolder> curveHolders, WriteCurveParameters writeCurveParameters,
            ProgressStatus<Long> progressStatus)
            throws CurveNotSampledException, CurvesPointsNotFitException, CurvesWithDifferentSamplingRateException,
            InvalidFormatException, NotNormalizedException {

        Encoding encoding = null;
        DoublePointEncoder doublePointEncoder;
        boolean mustBeNormalized = false;
        if (writeCurveParameters.sampleSizeInBits == 16) {
            doublePointEncoder = new IntegerLittle16ToDoublePointEncoder();
            encoding = Encoding.PCM_SIGNED;
            mustBeNormalized = true;
        } else if (writeCurveParameters.sampleSizeInBits == 24) {
            doublePointEncoder = new IntegerLittleAnyFromDoublePointEncoder(24);
            encoding = Encoding.PCM_SIGNED;
            mustBeNormalized = true;
        } else if (writeCurveParameters.sampleSizeInBits == 32) {
            doublePointEncoder = new RealLittle32FromDoublePointEncoder();
            encoding = Encoding.PCM_FLOAT;
        } else if (writeCurveParameters.sampleSizeInBits == 64) {
            doublePointEncoder = new RealLittle64FromDoublePointEncoder();
            encoding = Encoding.PCM_FLOAT;
        } else {
            throw new InvalidFormatException("Not supported format with " + writeCurveParameters.sampleSizeInBits + "bits");
        }

        double samplingRateAndCheck = CurveValidationUtil.getSamplingRateAndCheck(curveHolders, writeCurveParameters.xScaleFactor, writeCurveParameters.xNeedntStartWithZero, mustBeNormalized, writeCurveParameters.sampleSizeInBits);

        CurveInputStream curveInputStream = new CurveInputStream(curveHolders, doublePointEncoder, progressStatus);
        BufferedInputStream is = new BufferedInputStream(curveInputStream);

        AudioFormat frmt = new AudioFormat(encoding, (float) samplingRateAndCheck, writeCurveParameters.sampleSizeInBits, curveHolders.size(), writeCurveParameters.sampleSizeInBits / 8 * curveHolders.size(), (float) samplingRateAndCheck, true);
        AudioInputStream ais = new AudioInputStream(is, frmt, curveHolders.get(0).getCurveInfo().size);

        try {
            AudioSystem.write(ais, AudioFileFormat.Type.WAVE, new File(filenamePath));
        } catch (Exception e) {
            e.printStackTrace();
        }

        curveInputStream.close();
    }
}
