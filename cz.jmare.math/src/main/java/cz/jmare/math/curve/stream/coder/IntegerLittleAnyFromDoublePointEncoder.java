package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;

/**
 * Encode a Point.y into a number of bytes
 * Values of Y axe are from -1 including to 1 including
 * Little endian order
 */
public class IntegerLittleAnyFromDoublePointEncoder implements DoublePointEncoder {
    private int sampleLengthBytes;
    private long halfLongValue;

    public IntegerLittleAnyFromDoublePointEncoder(int sampleSizeInBits) {
        super();
        this.sampleLengthBytes = sampleSizeInBits / 8;
        this.halfLongValue = (long) Math.pow(2, sampleSizeInBits - 1);
    }

    @Override
    public void encode(Point point, byte[] bytes, int offset) {
        double y = point.y;
        if (y >= 1 || y < -1) {
            throw new IllegalArgumentException("Not normalized to interval <-1, 1)");
        }
        long l = (long) (y * halfLongValue);
        toByta(l, bytes, offset);
    }


    public void toByta(long longBits, byte[] bytes, int offset) {
        for (int i = 0, shift = (sampleLengthBytes - 1) * 8; i < sampleLengthBytes; i++, shift -= 8) {
            bytes[offset + i] = (byte) (longBits >> shift);
        }
    }

    @Override
    public int getSampleLengthBytes() {
        return sampleLengthBytes;
    }
}
