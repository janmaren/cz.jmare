package cz.jmare.math.curve.util;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.type.VectorD2Impl;

public class CurveVectorConvert {

    public static VectorD2 curveHolderToVectorD2(CurveHolder curveHolder) {
        long size = curveHolder.getCurveInfo().size;
        if (size > Integer.MAX_VALUE) {
            throw new IllegalStateException("File is too big");
        }
        int sizeInt = (int) size;
        VectorD1[] vec = new VectorD1[sizeInt];
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            for (int i = 0; i < sizeInt; i++) {
                Point point = curveReader.get(i);
                double[] xy = new double[2];
                xy[0] = point.x;
                xy[1] = point.y;
                VectorD1Impl vectorD1Impl = new VectorD1Impl(xy);
                vec[i] = vectorD1Impl;
            }
        }
        return new VectorD2Impl(vec);
    }

}
