package cz.jmare.math.curve.interpol;

import java.util.function.Function;

import cz.jmare.file.FriendlyAutoCloseable;
import cz.jmare.math.array.DoubleBigArray;
import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.CurveInfo;
import cz.jmare.math.curve.util.CachedCurve;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.number.EpsCompare;

public class MonoSplineInterpolatedCurve implements FriendlyAutoCloseable, InterpolatedCurve, Function<Double, Double> {
    private CachedCurve curveReader;

    // Index to be read
    int index = 0;

    private Point prevPoint;

    private Point currPoint;

    private DoubleBigArray mArray;

    private double minX;

    private double maxX;

    private Double lastX;

    private CurveHolder curveHolder;

    public MonoSplineInterpolatedCurve(CurveHolder curveHolder) {
        super();
        this.curveHolder = curveHolder;
        CurveInfo curveInfo = curveHolder.getCurveInfo();
        minX = curveInfo.minX;
        maxX = curveInfo.maxX;
        try (CachedCurve curveReader = new CachedCurve(curveHolder, 5); DoubleBigArray dArray = new DoubleBigArray()) {
            if (curveReader.size() < 2) {
                curveReader.close();
                throw new IllegalArgumentException("Number of points less than 2");
            }
            mArray = new DoubleBigArray();

            final long n = curveReader.size();

            for (long i = 0; i < n - 1; i++) {
                Point prevPoint = curveReader.get(i);
                Point currPoint = curveReader.get(i + 1);
                double h = currPoint.x - prevPoint.x;
                if (h <= 0) {
                    throw new IllegalArgumentException("The control points must all "
                            + "have strictly increasing X values.");
                }
                dArray.putDouble(i, (currPoint.y - prevPoint.y) / h);
            }

            mArray.putDouble(0, dArray.getDouble(0));
            for (int i = 1; i < n - 1; i++) {
                mArray.putDouble(i, (dArray.getDouble(i - 1) + dArray.getDouble(i)) * 0.5);
            }
            mArray.putDouble(n - 1, dArray.getDouble(n - 2));

            for (int i = 0; i < n - 1; i++) {
                if (dArray.getDouble(i) == 0) {
                    mArray.putDouble(i, 0);
                    mArray.putDouble(i + 1, 0);
                } else {
                    double a = mArray.getDouble(i) / dArray.getDouble(i);
                    double b = mArray.getDouble(i + 1) / dArray.getDouble(i);
                    double h = (double) Math.hypot(a, b);
                    if (h > 9) {
                        double t = 3 / h;
                        mArray.putDouble(i, t * a * dArray.getDouble(i));
                        mArray.putDouble(i + 1, t * b * dArray.getDouble(i));
                    }
                }
            }
        }

        this.curveReader = new CachedCurve(curveHolder, 5);
    }

    @Override
    public double getY(double x) {
        if (lastX != null && x < lastX) {
            throw new IllegalArgumentException("x values must be ascending, last value was " + lastX + " and now " + x);
        }
        lastX = x;

        if (index == 0) {
            prevPoint = curveReader.get(index++);
            currPoint = curveReader.get(index++);
            double lastX = prevPoint.x;
            if (EpsCompare.equals(lastX, x)) {
                return prevPoint.y;
            } else if (lastX > x) {
                throw new IllegalArgumentException("Required value for x=" + x + " but minimum x value is " + lastX);
            }
        }

        while (index < size()) {
            if (x > currPoint.x) {
                prevPoint = currPoint;
                currPoint = curveReader.get(index++);
            } else {
                double h = currPoint.x - prevPoint.x;
                double t = (x - prevPoint.x) / h;
                return (prevPoint.y * (1 + 2 * t) + h * mArray.getDouble(index - 2) * t) * (1 - t) * (1 - t)
                        + (currPoint.y * (3 - 2 * t) + h * mArray.getDouble(index - 1) * (t - 1)) * t * t;
            }
        }

        if (x < currPoint.x) {
            double h = currPoint.x - prevPoint.x;
            double t = (x - prevPoint.x) / h;
            return (prevPoint.y * (1 + 2 * t) + h * mArray.getDouble(index - 2) * t) * (1 - t) * (1 - t)
                    + (currPoint.y * (3 - 2 * t) + h * mArray.getDouble(index - 1) * (t - 1)) * t * t;
        } else
        if (EpsCompare.equals(currPoint.x, x)) {
            return currPoint.y;
        }

        throw new IllegalArgumentException("Required value for x=" + x + " but maximum x value x[" + (index - 1) +"] is " + currPoint.x);
    }

    public long size() {
        return curveReader.size();
    }


    @Override
    public void close() {
        mArray.close();
        curveReader.close();
    }

    @Override
    public double getMinX() {
        return minX;
    }

    @Override
    public double getMaxX() {
        return maxX;
    }

    @Override
    public Double apply(Double x) {
        return getY(x);
    }

    /**
     * Used when need to evaluate previous x value
     */
    public void reset() {
        curveReader.close();
        this.curveReader = new CachedCurve(curveHolder, 5);
        this.index = 0;
        this.lastX = null;
    }
}
