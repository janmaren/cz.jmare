package cz.jmare.math.curve;

/**
 * Point which is designed to consume low memory.<br>
 * But better way is using ByteBuffer
 */
public class SimplifiedPoint {
    public float x;

    public short y;

    public SimplifiedPoint(float x, short y) {
        this.x = x;
        this.y = y;
    }
}
