package cz.jmare.math.curve.util;

import java.util.List;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SampledCurveHolder;
import cz.jmare.math.curve.SampledCurveInfo;
import cz.jmare.math.exception.CurveNotSampledException;
import cz.jmare.math.exception.CurveNotStartsWithZeroException;
import cz.jmare.math.exception.CurvesPointsNotFitException;
import cz.jmare.math.exception.CurvesWithDifferentSamplingRateException;
import cz.jmare.math.exception.InvalidFormatException;
import cz.jmare.math.exception.NotNormalizedException;
import cz.jmare.math.number.EpsCompare;

public class CurveValidationUtil {
    /**
     * Get sampling rate in Hz which must be same for all curves. It also checks whether curves fit
     * @param curveHolders
     * @param xScaleFactor
     * @param xNeedntStartWithZero
     * @param sampleSizeInBits
     * @return
     * @throws CurveNotSampledException
     * @throws CurvesPointsNotFitException
     * @throws CurvesWithDifferentSamplingRateException
     * @throws InvalidFormatException
     * @throws NotNormalizedException
     */
    public static double getSamplingRateAndCheck(List<CurveHolder> curveHolders, double xScaleFactor, boolean xNeedntStartWithZero, boolean checkNormalization, Integer sampleSizeInBits)
            throws CurveNotSampledException, CurvesPointsNotFitException, CurvesWithDifferentSamplingRateException,
            InvalidFormatException, NotNormalizedException {
        Double samplingPeriod = null;
        Double minX = null;
        Double maxX = null;
        for (CurveHolder curveHolder : curveHolders) {
            if (!(curveHolder instanceof SampledCurveHolder)) {
                throw new CurveNotSampledException("There is an curve whitch is not sampled");
            }
            SampledCurveHolder sampledCurveHolder = (SampledCurveHolder) curveHolder;
            SampledCurveInfo curveInfo = sampledCurveHolder.getCurveInfo();

            if (samplingPeriod != null) {
                if (!EpsCompare.equals(samplingPeriod, curveInfo.samplingPeriod)) {
                    throw new CurvesWithDifferentSamplingRateException("Curves havn't the same sampling rate");
                }
            }
            samplingPeriod = curveInfo.samplingPeriod;
            if (checkNormalization && curveInfo.minY < -1) {
                throw new NotNormalizedException("There is a curve with Y value under -1");
            }
            if ((sampleSizeInBits != null) && (sampleSizeInBits == 16 || sampleSizeInBits == 24)) {
                double maxAllowedValue = 1;
                long halfLongValue = (long) Math.pow(2, sampleSizeInBits - 1);
                long halfLongValueLess = halfLongValue - 1;
                maxAllowedValue = (halfLongValueLess / (double) halfLongValue);
                if (checkNormalization && curveInfo.maxY > maxAllowedValue) {
                    throw new NotNormalizedException("There is a curve with Y value larger than " + maxAllowedValue);
                }
            }
            if (minX == null) {
                minX = curveInfo.minX;
            } else {
                if (!EpsCompare.equals(minX, curveInfo.minX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same beginning x coordinate");
                }
            }
            if (!xNeedntStartWithZero && !EpsCompare.equals(minX, 0)) {
                throw new CurveNotStartsWithZeroException("There is a curve shifted right, on x=" + minX);
            }
            if (maxX == null) {
                maxX = curveInfo.maxX;
            } else {
                if (!EpsCompare.equals(maxX, curveInfo.maxX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same ending X coordinate");
                }
            }
        }
        double value = Math.round(1.0 / (samplingPeriod * xScaleFactor));
        return value;
    }

    public static void validationOverlapping(List<CurveHolder> curveHolders)
            throws CurveNotSampledException, CurvesPointsNotFitException, CurvesWithDifferentSamplingRateException {
        Double samplingPeriod = null;
        Double minX = null;
        Double maxX = null;
        for (CurveHolder curveHolder : curveHolders) {
            if (!(curveHolder instanceof SampledCurveHolder)) {
                throw new CurveNotSampledException("There is an curve whitch is not sampled");
            }
            SampledCurveInfo curveInfo = ((SampledCurveHolder) curveHolder).getCurveInfo();
            if (samplingPeriod != null) {
                if (!EpsCompare.equals(samplingPeriod, curveInfo.samplingPeriod)) {
                    throw new CurvesWithDifferentSamplingRateException("Curves havn't the same sampling rate");
                }
            }
            samplingPeriod = curveInfo.samplingPeriod;
            if (minX == null) {
                minX = curveInfo.minX;
            } else {
                if (!EpsCompare.equals(minX, curveInfo.minX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same beginning x coordinate");
                }
            }
            if (maxX == null) {
                maxX = curveInfo.maxX;
            } else {
                if (!EpsCompare.equals(maxX, curveInfo.maxX)) {
                    throw new CurvesPointsNotFitException("Curves havn't the same ending X coordinate");
                }
            }
        }
    }

    public static void validationSampledSameRate(List<CurveHolder> curveHolders)
            throws CurveNotSampledException, CurvesWithDifferentSamplingRateException {
        Double samplingPeriod = null;
        for (CurveHolder curveHolder : curveHolders) {
            if (!(curveHolder instanceof SampledCurveHolder)) {
                throw new CurveNotSampledException("There is an curve whitch is not sampled");
            }
            SampledCurveInfo curveInfo = ((SampledCurveHolder) curveHolder).getCurveInfo();
            if (samplingPeriod != null) {
                if (!EpsCompare.equals(samplingPeriod, curveInfo.samplingPeriod)) {
                    throw new CurvesWithDifferentSamplingRateException("Curves havn't the same sampling rate");
                }
            } else {
                samplingPeriod = curveInfo.samplingPeriod;
            }
        }
    }

    public static boolean allSampled(List<CurveHolder> curveHolders) {
        for (CurveHolder curveHolder : curveHolders) {
            if (!(curveHolder instanceof SampledCurveHolder)) {
                return false;
            }
        }
        return true;
    }
}
