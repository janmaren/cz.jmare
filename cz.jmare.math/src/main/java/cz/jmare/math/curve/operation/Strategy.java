package cz.jmare.math.curve.operation;

public enum Strategy {
    MISSING_POINT_AS_ZERO, MISSING_POINT_EXPUNGE
}
