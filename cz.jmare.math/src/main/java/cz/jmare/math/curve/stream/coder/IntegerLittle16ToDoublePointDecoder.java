package cz.jmare.math.curve.stream.coder;

import cz.jmare.math.geometry.entity.Point;
import cz.jmare.math.util.DecodeRevertsUtil;

/**
 * Convert 2 bytes into point with extracted Y axe
 * Result min max values are -1 including, 1 including
 * Little endian order
 */
public class IntegerLittle16ToDoublePointDecoder implements DoublePointDecoder {
    public IntegerLittle16ToDoublePointDecoder() {
        super();
    }

    @Override
    public Point getPoint(double x, byte[] bytes, int index) {
        short sh = DecodeRevertsUtil.bytesToShortRevert(bytes, index);
        return new Point(x, (double) sh / 32768.0);
    }

    @Override
    public int getSampleLengthBytes() {
        return 2;
    }
}
