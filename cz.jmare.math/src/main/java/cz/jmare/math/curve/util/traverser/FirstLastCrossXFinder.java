package cz.jmare.math.curve.util.traverser;

import java.util.function.BiFunction;

public class FirstLastCrossXFinder implements BiFunction<Double, Boolean, Boolean> {
    private Double firstX;
    private Double lastX;
    private Boolean firstIsAscending;

    @Override
    public Boolean apply(Double x, Boolean ascending) {
        if (firstX == null) {
            firstX = x;
            firstIsAscending = ascending;
        } else {
            if (ascending == firstIsAscending) {
                lastX = x;
            }
        }
        return true;
    }

    public Double getFirstX() {
        return firstX;
    }

    public Double getLastX() {
        return lastX;
    }
}
