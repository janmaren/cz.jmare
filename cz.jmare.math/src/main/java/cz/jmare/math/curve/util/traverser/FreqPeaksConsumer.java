package cz.jmare.math.curve.util.traverser;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import cz.jmare.math.curve.RandomAccessCurve;
import cz.jmare.math.geometry.entity.Point;

public class FreqPeaksConsumer implements BiFunction<Double, Boolean, Boolean> {
    private Boolean lastAsc;

    private Double lastX;

    private List<Point> points = new ArrayList<>();
    private RandomAccessCurve curve;
    private int smoothNumber = 5;
    private double freqLimit = 50000;

    private boolean onlyUpper;

    public FreqPeaksConsumer(RandomAccessCurve curve) {
        this.curve = curve;
    }

    @Override
    public Boolean apply(Double x, Boolean asc) {
        if (lastX == null) {
            lastX = x;
            lastAsc = asc;
            return true;
        }
        if (asc.equals(lastAsc)) {
            throw new RuntimeException("Not switching asc desc");
        }
        int size = curve.size();
        for (int i = 0; i < size; i++) {
            Point point = curve.get(i);
            if (point.x > lastX) {
                Double max = -Double.MAX_VALUE;
                for (int j = i + 1; j < size; j++) {
                    Point point2 = curve.get(j);
                    if (point2.x >= x) {
                        break;
                    }
                    double avg = getAvg(j);
                    if (avg > max) {
                        max = avg;
                    }
                }

                double period = (x - lastX) * 2;
                double freq = 1.0 / period;
                if (freq <= freqLimit && max != -Double.MAX_VALUE) {
                    if (!onlyUpper || !asc) {
                        Point freqPoint = new Point(freq, max);
                        points.add(freqPoint);
                    }
                }
                lastAsc = asc;
                lastX = x;
                return true;
            } else {
                continue;
            }
        }
        throw new IllegalStateException("Not found point with x=" + x + ", lastX=" + lastX);
    }

    private double getAvg(int index) {
        int neigh = smoothNumber / 2;
        int from = index - neigh;
        if (from < 0) {
            from = 0;
        }
        int to = index + neigh + 1;
        if (to > curve.size()) {
            to = curve.size();
        }
        double sum = 0;
        for (int i = from; i < to; i++) {
            sum += curve.get(i).y;
        }
        return Math.abs(sum) / (to - from);
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setSmoothNumber(int smoothNumber) {
        if (smoothNumber % 2 != 1) {
            throw new IllegalArgumentException("smooth number must be an odd number");
        }
        this.smoothNumber = smoothNumber;
    }

    public void setFreqLimit(double freqLimit) {
        this.freqLimit = freqLimit;
    }

    public void setOnlyUpper(boolean onlyUpper) {
        this.onlyUpper = onlyUpper;
    }
}
