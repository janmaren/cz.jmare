package cz.jmare.math.curve.importexport;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import cz.jmare.file.FriendlyAutoCloseable;

public class FriendlyBufferReader extends BufferedReader implements FriendlyAutoCloseable {

    public FriendlyBufferReader(Reader in) {
        super(in);
    }

    public FriendlyBufferReader(String filepath) {
        super(createReader(filepath));
    }

    @Override
    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to close file", e);
        }
    }

    public static Reader createReader(String filePath) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(filePath);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.ISO_8859_1);
            return inputStreamReader;
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to open file " + filePath, e);
        }
    }

    @Override
    public String readLine() {
        try {
            return super.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read line", e);
        }
    }
}
