package cz.jmare.math.curve.importexport;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.jmare.math.curve.PolylineType;
import cz.jmare.math.exception.InvalidFormatException;

public class PolylineFileTypeAnalyzator {
    private final static Pattern FORMAT_Y_PATTERN = Pattern.compile("^\\s*Polyline\\s+Type\\s*:\\s*Y");
    private final static Pattern FORMAT_XY_PATTERN = Pattern.compile("^\\s*Polyline\\s+Type\\s*:\\s*X\\s*,\\s*Y");
    private final static Pattern STARTS_NUMBER_PATTERN = Pattern.compile("^\\s*(?<number>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)");
    private final static String[] SEPARATORS = {",", ";", "|"};
    private final static Pattern SAMPLE_RATE_PATTERN = Pattern.compile("^\\s*Sample rate:\\s*(?<samplingRate>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)\\s*Hz", Pattern.CASE_INSENSITIVE);
    private final static Pattern SAMPLING_RATE_PATTERN = Pattern.compile("^\\s*Sampling rate:\\s*(?<samplingRate>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)\\s*Hz", Pattern.CASE_INSENSITIVE);
    private final static Pattern OFFSET_PATTERN = Pattern.compile("^\\s*X offset:\\s*(?<offset>[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?)");

    private PolylineType polylineType;
    private String filepath;
    private String[] firstLines;
    private int firstDataLine;
    private String separator;
    private boolean separatorDetected;
    private Integer columnsNumber;
    private List<Double> columnsMetrics;
    private Double samplingRate;
    private Double xOffset;

    public PolylineFileTypeAnalyzator(String filepath) {
        super();
        this.filepath = filepath;

        List<String> linesList = FileLinesUtil.readNLines(filepath, "ISO-8859-1", 30);
        firstLines = linesList.toArray(new String[linesList.size()]);
        firstDataLine = findDataLine();
        if (firstDataLine == -1) {
            throw new InvalidFormatException("File " + filepath + " doesn't contain any data to be loaded or not supported format");
        }
        detectSeparator(firstLines[firstDataLine]);
        detectPolylineType();
        detectFreqOffset();
        if (samplingRate != null && polylineType == null) {
            setPolylineType(PolylineType.SAMPLED);
        }
        if (polylineType == PolylineType.RAW || polylineType == PolylineType.SAMPLED) {
            columnsMetrics = List.of(1.0);
        } else {
            List<Double> picoMetrics = picoMetrics(firstLines, firstDataLine);
            if (picoMetrics != null) {
                columnsMetrics = picoMetrics;
                setPolylineType(PolylineType.RAW);
            }
        }
    }

    private int findDataLine() {
        for (int i = 0; i < firstLines.length; i++) {
            String line = firstLines[i];
            Matcher matcher = STARTS_NUMBER_PATTERN.matcher(line);
            if (matcher.find()) {
                return i;
            }
        }
        return -1;
    }

    private void detectSeparator(String numbersLine) {
        for (int i = 0; i < SEPARATORS.length; i++) {
            String ds = SEPARATORS[i];
            if (numbersLine.contains(ds)) {
                for (int j = 0; j < SEPARATORS.length; j++) {
                    if (j == i) {
                        continue;
                    }
                    if (numbersLine.contains(SEPARATORS[j])) {
                        return;
                    }
                }
                setSeparator(SEPARATORS[i]);
                return;
            }
        }
        setSeparator(null);
    }

    private void setSeparator(String separator) {
        int columnsNumberCheckDoubles = ParseLineUtil.parseDoubles(firstLines[firstDataLine], separator).length;
        if (columnsNumberCheckDoubles != -1) {
            columnsNumber = columnsNumberCheckDoubles;
            this.separator = separator;
            this.separatorDetected = true;
        }
    }

    public void detectPolylineType() {
        for (int i = 0; i < firstLines.length; i++) {
            String line = firstLines[i];
            if (lineHasNumber(line)) {
                break;
            }
            if (FORMAT_Y_PATTERN.matcher(line).find()) {
                this.setPolylineType(PolylineType.SAMPLED);
                return;
            }
            if (FORMAT_XY_PATTERN.matcher(line).find()) {
                this.setPolylineType(PolylineType.RAW);
                return;
            }
        }
    }

    public void detectFreqOffset() {
        for (int i = 0; i < firstLines.length; i++) {
            String line = firstLines[i];
            if (lineHasNumber(line)) {
                break;
            }
            Matcher matcher = SAMPLING_RATE_PATTERN.matcher(line);
            if (matcher.find()) {
                samplingRate = Double.parseDouble(matcher.group("samplingRate"));
            } else {
                matcher = SAMPLE_RATE_PATTERN.matcher(line);
                if (matcher.find()) {
                    samplingRate = Double.parseDouble(matcher.group("samplingRate"));
                }
            }
            Matcher matcherOffset = OFFSET_PATTERN.matcher(line);
            if (matcherOffset.find()) {
                xOffset = Double.parseDouble(matcherOffset.group("offset"));
            }
        }
    }

    public static List<Double> picoMetrics(String[] firstLines, int firstDataLine) {
        if (firstDataLine == 0) {
            return null;
        }
        String secondLine = firstLines[firstDataLine - 1];
        if ("".equals(secondLine.trim()) && firstDataLine >= 2) {
            secondLine = firstLines[firstDataLine - 2];
        }
        List<Double> newMetricPrefix = new ArrayList<Double>();
        String[] split = secondLine.split("\\(");
        double[] result = new double[split.length - 1];
        for (int i = 1; i < split.length; i++) {
            String value = split[i];
            int indexOf = value.indexOf(")");
            if (indexOf == -1) {
                return null;
            }
            String str = value.substring(0, indexOf);
            double multiply = 1;
            if ("ms".equalsIgnoreCase(str)) {
                multiply = 0.001;
            } else if ("s".equalsIgnoreCase(str)) {
                multiply = 1;
            } else if ("ns".equalsIgnoreCase(str)) {
                multiply = 0.000000001;
            } else if ("V".equalsIgnoreCase(str)) {
                multiply = 1;
            } else if ("mV".equalsIgnoreCase(str)) {
                multiply = 0.001;
            } else if ("uV".equalsIgnoreCase(str)) {
                multiply = 0.000001;
            }
            result[i - 1] = multiply;

            newMetricPrefix.add(multiply);
        }
        if (newMetricPrefix.size() == 0) {
            return null;
        }

        return newMetricPrefix;
    }

    private static boolean lineHasNumber(String line) {
        return STARTS_NUMBER_PATTERN.matcher(line).find();
    }

    public PolylineType getPolylineType() {
        return polylineType;
    }

    public String getFilepath() {
        return filepath;
    }

    public String[] getFirstLines() {
        return firstLines;
    }

    public String getSeparator() {
        return separator;
    }

    public boolean isSeparatorDetected() {
        return separatorDetected;
    }

    public Integer getColumnsNumber() {
        return columnsNumber;
    }

    public List<Double> getColumnsMetrics() {
        return columnsMetrics;
    }

    public Double getSamplingRate() {
        return samplingRate;
    }

    public Double getXOffset() {
        return xOffset;
    }

    public void setPolylineType(PolylineType polylineType) {
        this.polylineType = polylineType;
    }
}
