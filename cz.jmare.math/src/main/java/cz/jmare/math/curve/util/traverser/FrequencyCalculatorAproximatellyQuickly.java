package cz.jmare.math.curve.util.traverser;

import java.util.function.BiFunction;

public class FrequencyCalculatorAproximatellyQuickly implements BiFunction<Double, Boolean, Boolean> {
    private double maxFrequence = 100000000;

    private Record firstRecord;

    private Double resultFrequence;

    @Override
    public Boolean apply(Double x, Boolean ascending) {
        Record record = new Record();
        record.x = x;
        record.up = ascending;

        if (firstRecord == null) {
            firstRecord = record;
            return true;
        } else {
            if (firstRecord.up == ascending) {
                throw new IllegalArgumentException("Not switching up down now is " + ascending + " at point x " + " but the previous was also " + firstRecord.up + " at point " + firstRecord.x);
            }
            double period = 2 * (x - firstRecord.x);
            double realFreq = 1 / period;
            if (realFreq < maxFrequence) {
                this.resultFrequence = realFreq;
                return false;
            } else {
                this.firstRecord = record;
                return true;
            }
        }
    }

    public Double getFrequency() {
        if (resultFrequence == null) {
            return Double.NaN;
        }
        return resultFrequence;
    }

    public void setMaxFrequence(double maxFrequence) {
        this.maxFrequence = maxFrequence;
    }

    static class Record {
        double x;
        boolean up;
    }
}
