package cz.jmare.math.curve.util.traverser;

import java.util.function.BiFunction;

public class FirstAscOrDescConsumer implements BiFunction<Double, Boolean, Boolean> {
    private boolean ascending;

    private Double resultX;

    @Override
    public Boolean apply(Double x, Boolean asc) {
        if (asc) {
            if (ascending) {
                resultX = x;
                return false;
            }
        } else {
            if (!ascending) {
                resultX = x;
                return false;
            }
        }
        return true;
    }

    public Double getResultX() {
        return resultX;
    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }
}
