package cz.jmare.math;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import cz.jmare.math.entity.TwoDoubles;
import cz.jmare.math.geometry.entity.Point;

public class LeastSquaresMSE {
    public static double mseForKQPoints(Collection<TwoDoubles> expectedAndReals) {
        double sum = 0;
        for (TwoDoubles twoDoubles : expectedAndReals) {
            sum += (twoDoubles.x1 - twoDoubles.x2) * (twoDoubles.x1 - twoDoubles.x2);
        }
        return sum / expectedAndReals.size();
    }

    public static Collection<TwoDoubles> expectedAndReals(Collection<Point> points, TwoDoubles leastSquaresKQ) {
        return points.stream().map(p -> new TwoDoubles(p.y, p.x * leastSquaresKQ.x1 + leastSquaresKQ.x2)).collect(Collectors.toList());
    }

    public static KQMSE getLeastSquaresWithMse(Collection<Point> points) {
        TwoDoubles leastSquaresKQ = LeastSquaresMath.getLeastSquaresKQ(points);
        Collection<TwoDoubles> expectedAndReals = expectedAndReals(points, leastSquaresKQ);
        double mse = mseForKQPoints(expectedAndReals);
        return new KQMSE(leastSquaresKQ, mse);
    }

    public static void main(String[] args) {
        double c0 = 5;
        double c1 = 7;
        List<Double> vals = List.of(3.0, 5.0, 6.0, 7.0, 20.0, 30.0);
        Random random = new Random();
        Collection<Point> points = vals.stream().map(v -> new Point(v, c0 + c1 * v + random.nextDouble() * 0.5 - 0.25)).collect(Collectors.toList());
        System.out.println(points);
        KQMSE leastSquares = LeastSquaresMSE.getLeastSquaresWithMse(points);
        System.out.println("k, q:" + leastSquares);
    }
}
