package cz.jmare.mexpr.func;

import java.util.function.Supplier;

import cz.jmare.math.calculate.FilteredGain;
import cz.jmare.math.calculate.filter.PeakBiquadraticWaveFilter;
import cz.jmare.mexpr.type.ConstantHolder;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "biquadPeekGain", description = "Biquadratic peek gain for a frequency after the peek filter used for central cutoff frequency. Syntax: biquadPeekGain(freqTested, fCutoff, gainCutoff [, qCutoff])\nfreqTested: filtered value for this frequency"
        + "\nfCutoff: frequency with max/min gain"
        + "\ngainCutoff: gain of frequency (also negative to cut)"
        + "\nqCutoff: quality factor, default is 1.414213562 which is BW 1 octave")
public class BiquadPeekGainSupplier implements Supplier<Double> {
    private final static int pointsPerSine = 100;
    private Supplier<Double> freqTestedSupplier;
    private Supplier<Double> fCutoffSupplier;
    private Supplier<Double> gainSupplier;
    private Supplier<Double> qSupplier = new ConstantHolder(1.4142135623730951); // 1 octave

    public BiquadPeekGainSupplier(Supplier<Double> freqTestedSupplier, Supplier<Double> fCutoffSupplier, Supplier<Double> gainSupplier) {
        this.freqTestedSupplier = freqTestedSupplier;
        this.fCutoffSupplier = fCutoffSupplier;
        this.gainSupplier = gainSupplier;
    }

    public BiquadPeekGainSupplier(Supplier<Double> freqTestedSupplier, Supplier<Double> fCutoffSupplier, Supplier<Double> gainSupplier, Supplier<Double> qSupplier) {
        this.freqTestedSupplier = freqTestedSupplier;
        this.fCutoffSupplier = fCutoffSupplier;
        this.gainSupplier = gainSupplier;
        this.qSupplier = qSupplier;
    }

    @Override
    public Double get() {
        double frequencyCutoff = fCutoffSupplier.get();
        double frequencySine = freqTestedSupplier.get();
        if (frequencySine < 25) {
            return 0.0;
        }
        double sinePoints = pointsPerSine;
        if (frequencySine < 100) {
            sinePoints = 1000;
        }
        if (frequencySine < 20) {
            sinePoints = 10000;
        }
        double gainDB = gainSupplier.get();
        double sampleRate = sinePoints * frequencySine;
        PeakBiquadraticWaveFilter peakBiquadraticWaveFilter = new PeakBiquadraticWaveFilter(frequencyCutoff, sampleRate, qSupplier.get(), gainDB);
        double result = FilteredGain.filteredGainForFrequency(peakBiquadraticWaveFilter, sampleRate, frequencySine);
        return result;
    }
}
