package cz.jmare.gisplatformorg.maren.gis.util;

import static java.lang.Math.toRadians;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.GonUtil;

public class GonUtilTest {
	static Spheric toSpheric(double lonDeg, double latDeg) {
		return new Spheric(toRadians(lonDeg), toRadians(latDeg));
	}
	
	static double getLatDeg(Spheric spheric) {
		return Math.toDegrees(spheric.lat);
	}
	
	@Test
	void normalizeGeoSpheric() {
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50))), 0.1);
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50 + 360))), 0.1);
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50 + 720))), 0.1);
		
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50))), 0.1);
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50 - 360))), 0.1);
		assertEquals(50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 50 - 720))), 0.1);
		
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50))), 0.1);
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50 + 360))), 0.1);
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50 + 720))), 0.1);
		
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50))), 0.1);
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50 - 360))), 0.1);
		assertEquals(-50, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -50 - 720))), 0.1);
		
		
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110))), 0.1);
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110 + 360))), 0.1);
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110 + 720))), 0.1);
		
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110))), 0.1);
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110 - 360))), 0.1);
		assertEquals(70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, 110 - 720))), 0.1);
		
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110))), 0.1);
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110 + 360))), 0.1);
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110 + 720))), 0.1);
		
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110))), 0.1);
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110 - 360))), 0.1);
		assertEquals(-70, getLatDeg(GonUtil.normalizeGeoSpheric(toSpheric(15, -110 - 720))), 0.1);
		
		Spheric sph = GonUtil.normalizeGeoSpheric(toSpheric(-24.611667612851257, 301.0041256497737));
		assertEquals(-58.98, getLatDeg(sph), 0.1);
		assertEquals(-24.61, Math.toDegrees(sph.lon), 0.1);
	}
}
