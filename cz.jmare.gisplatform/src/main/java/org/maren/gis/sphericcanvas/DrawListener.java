package org.maren.gis.sphericcanvas;

import org.eclipse.swt.graphics.GC;

public interface DrawListener {
    void draw(GC gc, CoordState coordState);
}
