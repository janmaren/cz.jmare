package org.maren.gis.sphericcanvas;

public interface CanvasKeyListener {
    void onCanvasKey(int keyCode, int stateMask);
}
