package org.maren.gis.sphericcanvas;

import org.maren.gis.type.Spheric;

public interface CursorChangedListener {
    /**
     * Listener doing an action when cusrsor position changes
     * @param newCursorPosition new position of cursor or null when out of range
     * @param stateMask
     */
    void onCursorChanged(Spheric newCursorPosition, int stateMask);
}
