package org.maren.gis.sphericcanvas;

import org.eclipse.swt.graphics.GC;

/**
 * Constructing a shape, accepting mouse clicks and able to draw current shape depending on current cursor position
 */
public interface ToolListener {
    void mouseUpEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button);
    
    void mouseDownEvent(CoordState coordState, int screenX, int screenY, int stateMask, int button);
        
    void drawEvent(GC gc, CoordState coordState, int screenX, int screenY);
}
