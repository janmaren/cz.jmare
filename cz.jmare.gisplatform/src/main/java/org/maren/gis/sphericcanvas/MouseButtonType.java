package org.maren.gis.sphericcanvas;

public enum MouseButtonType {
    LEFT, MIDDLE, RIGHT
}
