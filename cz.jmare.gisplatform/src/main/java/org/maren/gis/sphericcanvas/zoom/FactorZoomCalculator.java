package org.maren.gis.sphericcanvas.zoom;

/**
 * Zoom by a factor value. Zoomin is multiplication, zoomout is division.
 * It means that zoom is a number how much times the pixel is larger than real unit
 */
public class FactorZoomCalculator implements ZoomCalculator {
    private double factor = 2;
    
    private Double zoomoutLimit;
    
    private Double zoominLimit;
    
    public FactorZoomCalculator() {
    }

    public FactorZoomCalculator(double factor) {
        if (factor <= 1) {
            throw new IllegalArgumentException("Factor isn't greater than 1");
        }
        this.factor = factor;
    }

    /**
     * 
     * @param factor greater than 1
     * @param zoomoutLimit a low number
     * @param zoominLimit a high number
     */
    public FactorZoomCalculator(double factor, Double zoomoutLimit, Double zoominLimit) {
        if (factor <= 1) {
            throw new IllegalArgumentException("Factor isn't greater than 1");
        }
        this.factor = factor;
        this.zoomoutLimit = zoomoutLimit;
        this.zoominLimit = zoominLimit;
    }

    @Override
    public double nextZoomIn(double currentZoom) {
        double validZoom = getValidZoom(currentZoom);
        return getValidZoom(validZoom * factor);
    }

    @Override
    public double nextZoomOut(double currentZoom) {
        double validZoom = getValidZoom(currentZoom);
        return getValidZoom(validZoom / factor);
    }

    @Override
    public double getValidZoom(double currentZoom) {
        long mult = Math.round(log(currentZoom, factor));
        double zoom = Math.pow(factor, mult);
        if (zoomoutLimit != null && zoom < zoomoutLimit) {
            return Math.pow(factor, Math.ceil(log(zoomoutLimit, factor)));
        } else if (zoominLimit != null && zoom > zoominLimit) {
            return Math.pow(factor, Math.floor(log(zoominLimit, factor)));
        }
        return zoom;
    }
    
    private static double log(double value, double base) {
        return Math.log(value) / Math.log(base);
    }
}
