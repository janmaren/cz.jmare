package org.maren.gis.sphericcanvas;

public interface ZoomChangedListener {
    /**
     * Listener doing an action when zoom changes
     * @param zoom new zoom
     * @param stateMask
     */
    void onZoomChanged(double zoom, int stateMask);
}
