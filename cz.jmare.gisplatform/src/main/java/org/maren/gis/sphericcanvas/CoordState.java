package org.maren.gis.sphericcanvas;

import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.type.Spheric;

public interface CoordState {
    Spheric toSpheric(int screenX, int screenY);

    ScreenPoint toScreenCoordinates(Spheric spheric);

    public int getScreenHeight();

    public int getScreenWidth();
    
    public double getZoom();
    
    int getCenterScreenX();
    
    int getCenterScreenY();
}
