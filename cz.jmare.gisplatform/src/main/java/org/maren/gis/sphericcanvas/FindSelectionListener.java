package org.maren.gis.sphericcanvas;

import org.eclipse.swt.SWT;

public interface FindSelectionListener {
    /**
     * Passing the clicking coordinates to find selection among spatial data
     * @param screenX click X coordinate
     * @param screenY click Y coordinate
     * @param coordState
     * @param stateMask stateMask, use {@link SWT#SHIFT}, {@link SWT#CONTROL}, etc. when needed to do some action when such key pressed
     * @return true when an object has been found
     */
	boolean onFindingClick(int screenX, int screenY, CoordState coordState, int stateMask);
}
