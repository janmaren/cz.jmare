package org.maren.gis.sphericcanvas;

import org.eclipse.swt.widgets.Shell;

class ResizeRunnable implements Runnable {
    SphericCanvas spView;
    private Shell shell;
    private boolean updating;

    public ResizeRunnable(SphericCanvas spView, Shell shell) {
        this.spView = spView;
        this.shell = shell;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                break;
            }
            if (updating) {
                updating = false;
            } else {
                shell.getDisplay().asyncExec(() -> {
                    spView.clearResizeRunnable();
                    spView.refresh();
                });
                break;
            }
        }
        spView.clearResizeRunnable();
    }

    public void update() {
        updating = true;
    }
}
