package org.maren.gis.sphericcanvas;

import java.util.Set;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.maren.gis.projection.SphProjection;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.zoom.FactorZoomCalculator;
import org.maren.gis.sphericcanvas.zoom.ZoomCalculator;
import org.maren.gis.type.Spheric;

public class SphericCanvas {
    private Canvas canvas;

    private int screenHeight;

    private int screenWidth;

    /**
     * From left
     */
    private int centerScreenX;

    /**
     * From top
     */
    private int centerScreenY;

    /**
     * Spheric coordinates of point which is at center of screen at the far hemisphere
     */
    private Spheric spheric0;

    private Image image;
    
    /**
     * The bigger number the bigger zoom on screen
     */
    private double zoom = 1;

    private SphProjection sphProjection;

    private DrawListener drawListener;
    
    private CursorChangedListener cursorChangedListener;
    
    private ZoomChangedListener zoomChangedListener;
    
    private FindSelectionListener findSelectionListener;

    private ToolListener toolListener;
    
    private CanvasKeyListener canvasKeyListener;
    
    private boolean dragging;
    private boolean draggingButtonDown;
    private Integer draggingButtonDownScreenX;
    private Integer draggingButtonDownScreenY;
    private double draggingSincePixels = 5;
    private Spheric startDragSpheric;
    private Spheric startDragSpheric0;
    private int startDragScreenY;
    private double raInc = 0;
    private double decInc = 0;
    
    /**
     * Current cursor X position on screen
     */
    private int screenX = -1;
    
    /**
     * Current cursor Y position on screen
     */
    private int screenY = -1;
    
    private MouseButtonType dragButtonType = MouseButtonType.LEFT;
    
    private ZoomCalculator zoomCalculator = new FactorZoomCalculator(2, 1e-9, 1e9);

    private ResizeRunnable resizeRunnable;

    private Color backgroundColor;
    
    private Set<Integer> toolButtons = Set.of(1);

    public SphericCanvas(Composite parent, SphProjection sphProjection, DrawListener drawListener) {
        this.drawListener = drawListener;
        spheric0 = new Spheric(0, 0);
        setSphProjection(sphProjection);
        doInit(parent);

        parent.getShell().addControlListener(new ControlListener() {
            @Override
            public void controlMoved(ControlEvent e) {
            }

            @Override
            public void controlResized(ControlEvent e) {
                if (resizeRunnable == null) {
                    resizeRunnable = new ResizeRunnable(SphericCanvas.this, parent.getShell());
                    new Thread(resizeRunnable).start();
                } else {
                    resizeRunnable.update();
                }
            }
        });

        backgroundColor = parent.getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE);
    }

    protected void reinitProjection() {
        sphProjection.init(spheric0, centerScreenX, centerScreenY, zoom);
    }

    private void doInit(Composite parent) {
        canvas = new Canvas(parent, SWT.NO_BACKGROUND | SWT.BORDER);
        canvas.addPaintListener(new PaintListener() {
            @Override
            public void paintControl(PaintEvent e) {
                if (resizeRunnable != null) {
                    e.gc.setBackground(backgroundColor);
                    e.gc.fillRectangle(new Rectangle(0, 0, screenWidth + 6, screenHeight + 6));
                    return;
                }
                initScreenDimensions();

                reinitProjection();
                
                CoordState coordState = buildCoordState();
                if (image == null) {
                    image = new Image(canvas.getDisplay(), canvas.getSize().x, canvas.getSize().y);
                    GC gc = new GC(image);
                    gc.setInterpolation(SWT.NONE);
                    gc.setBackground(backgroundColor);
                    gc.fillRectangle(new Rectangle(0, 0, screenWidth + 6, screenHeight + 6));
                    
                    drawListener.draw(gc, coordState);
                    
                    gc.dispose();
                }

                if (toolListener != null) {
                    Image imageWithTool = new Image(canvas.getDisplay(), canvas.getSize().x, canvas.getSize().y);
                    GC gcTool = new GC(imageWithTool);
                    gcTool.drawImage(image, 0, 0);
                    toolListener.drawEvent(gcTool, coordState, screenX, screenY);
                    e.gc.drawImage(imageWithTool, 0, 0);
                    gcTool.dispose();
                    imageWithTool.dispose();
                } else {
                    e.gc.drawImage(image, 0, 0);
                }
                
            }
        });
        
        canvas.addKeyListener(KeyListener.keyPressedAdapter(ev -> {
            if (canvasKeyListener != null) {
                canvasKeyListener.onCanvasKey(ev.keyCode, ev.stateMask);
            }
        }));
        
        canvas.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseScrolled(MouseEvent event) {
                int wheelCount = event.count;
                if (wheelCount > 0) {
                    zoom = (((event.stateMask & SWT.CTRL) > 1) ? zoomCalculator.nextZoomIn(zoomCalculator.nextZoomIn(zoom)): zoomCalculator.nextZoomIn(zoom));
                    initScreenDimensions();
                    reinitProjection();
                    refresh();
                }

                if (wheelCount < 0) {
                    zoom = (((event.stateMask & SWT.CTRL) > 1) ? zoomCalculator.nextZoomOut(zoomCalculator.nextZoomOut(zoom)): zoomCalculator.nextZoomOut(zoom));
                    initScreenDimensions();
                    reinitProjection();
                    refresh();
                }
                
                if (zoomChangedListener != null) {
                    zoomChangedListener.onZoomChanged(zoom, event.stateMask);
                }
            }
        });
        canvas.addMouseMoveListener(new MouseMoveListener() {
            @Override
            public void mouseMove(MouseEvent mouseEvent) {
                screenX = mouseEvent.x;
                screenY = mouseEvent.y;
                boolean dr = draggingButtonDown;
                if (draggingButtonDownScreenX != null) {
                	int diffX = draggingButtonDownScreenX - screenX;
                	int diffY = draggingButtonDownScreenY - screenY;
                	double dist = Math.sqrt(diffX * diffX + diffY * diffY);
                	if (toolListener != null && dist < draggingSincePixels) {
                		dr = false;
                	} else {
                		draggingButtonDownScreenX = null;
                		draggingButtonDownScreenY = null;
                	}
                }
                if (dr && (mouseEvent.stateMask & SWT.SHIFT) != SWT.SHIFT) { // shift may cause setting spheric0 so prevent not to change it by dragging
                    dragging = true;
                    parent.getShell().setCursor(new Cursor(canvas.getDisplay(), SWT.CURSOR_SIZEALL));
                    sphProjection.init(startDragSpheric0, centerScreenX, centerScreenY, zoom);
                    Spheric endDrag = sphProjection.toSphericCoordinates(mouseEvent.x, mouseEvent.y);
                    if (endDrag != null) {
                        raInc = startDragSpheric.lon - endDrag.lon;
                        decInc = startDragSpheric.lat - endDrag.lat;
                        if (mouseEvent.y < startDragScreenY) {
                            decInc = -Math.abs(decInc);
                        } else {
                            decInc = Math.abs(decInc);
                        }
                        spheric0 = new Spheric(startDragSpheric0.lon + raInc, startDragSpheric0.lat + decInc);
                     
                        sphProjection.init(spheric0, centerScreenX, centerScreenY, zoom);
                        refresh();
                        return;
                    }
                }
                Spheric spheric = sphProjection.toSphericCoordinates(mouseEvent.x, mouseEvent.y);
                if (cursorChangedListener != null && spheric0 != null) {
                    cursorChangedListener.onCursorChanged(spheric, mouseEvent.stateMask);
                }
                quickRefresh();
            }
        });
        canvas.addMouseListener(new MouseListener() {
            @Override
            public void mouseUp(MouseEvent mouseEvent) {
                doMouseUp(mouseEvent, parent.getShell(), mouseEvent.x, mouseEvent.y);
            }

            @Override
            public void mouseDown(MouseEvent mouseEvent) {
                canvas.setFocus();
                doMouseDown(mouseEvent, parent.getShell(), mouseEvent.x, mouseEvent.y);
            }

            @Override
            public void mouseDoubleClick(MouseEvent mouseEvent) {
                Spheric spheric0Loc = sphProjection.toSphericCoordinates(mouseEvent.x, mouseEvent.y);
                if (spheric0Loc != null) {
                    spheric0 = spheric0Loc;
                }
                if (dragging) {
                    dragging = false;
                    parent.getShell().setCursor(new Cursor(canvas.getDisplay(), SWT.CURSOR_ARROW));
                }
                refresh();
            }
        });
        
        canvas.addControlListener(new ControlListener() {
            @Override
            public void controlResized(ControlEvent e) {
                initScreenDimensions();
                refresh();
            }
            
            @Override
            public void controlMoved(ControlEvent e) {
            }
        });
    }

    private void doMouseDown(MouseEvent mouseEvent, Shell shell, int screenX, int screenY) {
        boolean draggingButton = isDraggingButton(mouseEvent);
        if (draggingButton) {
            draggingButtonDown = true;
            draggingButtonDownScreenX = mouseEvent.x;
            draggingButtonDownScreenY = mouseEvent.y;
        }
        if (draggingButton) {
            startDragSpheric0 = this.spheric0;
            startDragSpheric = sphProjection.toSphericCoordinates(mouseEvent.x, mouseEvent.y);
            if (startDragSpheric != null) {
                startDragScreenY = mouseEvent.y;
            } else {
                draggingButtonDown = false;
            }
        }
        if (isToolButton(mouseEvent) && toolListener != null) {
        	if (sphProjection.toSphericCoordinates(screenX, screenY) != null) {
	            toolListener.mouseDownEvent(buildCoordState(), screenX, screenY, mouseEvent.stateMask, mouseEvent.button);
	            quickRefresh();
        	}
        }
        if (mouseEvent.button == 1) {
            if (findSelectionListener != null && findSelectionListener.onFindingClick(mouseEvent.x, mouseEvent.y, buildCoordState(), mouseEvent.stateMask)) {
                refresh();
            }
        }
    }

    private void doMouseUp(MouseEvent mouseEvent, Shell shell, int screenX, int screenY) {
        boolean draggingButton = isDraggingButton(mouseEvent);
        if (draggingButton) {
            draggingButtonDown = false;
        }
        if (dragging && draggingButton) {
            dragging = false;
            shell.setCursor(new Cursor(canvas.getDisplay(), SWT.CURSOR_ARROW));
            Spheric endDrag = sphProjection.toSphericCoordinates(mouseEvent.x, mouseEvent.y);
            if (endDrag != null) {
                this.spheric0 = new Spheric(startDragSpheric0.lon + raInc, startDragSpheric0.lat + decInc);
                sphProjection.init(this.spheric0, centerScreenX, centerScreenY, zoom);

                raInc = 0;
                decInc = 0;
            }
            refresh();
        } else {
            if (isToolButton(mouseEvent) && toolListener != null) {
            	if (sphProjection.toSphericCoordinates(screenX, screenY) != null) {
	                toolListener.mouseUpEvent(buildCoordState(), screenX, screenY, mouseEvent.stateMask, mouseEvent.button);
	                quickRefresh();
            	}
            }
        }
    }

    private boolean isToolButton(MouseEvent mouseEvent) {
    	return toolButtons.contains(mouseEvent.button);
    }
    
    private boolean isDraggingButton(MouseEvent mouseEvent) {
        if (mouseEvent.button == 1 && dragButtonType == MouseButtonType.LEFT) {
            return true;
        }
        if (mouseEvent.button == 2 && dragButtonType == MouseButtonType.MIDDLE) {
            return true;
        }
        if (mouseEvent.button == 3 && dragButtonType == MouseButtonType.RIGHT) {
            return true;
        }
        return false;
    }
    
    public void setZoom(double zoom) {
        this.zoom = zoomCalculator.getValidZoom(zoom);
        reinitProjection();
    }

    public double getZoom() {
        return zoom;
    }

    public void setZoomCalculator(ZoomCalculator zoomCalculator, double initialZoom) {
        this.zoomCalculator = zoomCalculator;
        this.zoom = zoomCalculator.getValidZoom(initialZoom);
        reinitProjection();
    }

    public void setLayoutData(Object layoutData) {
        canvas.setLayoutData(layoutData);
    }

    public void initScreenDimensions() {
        Rectangle clientArea = canvas.getClientArea();
        screenHeight = clientArea.height;
        screenWidth = clientArea.width;
        centerScreenX = screenWidth / 2;
        centerScreenY = screenHeight / 2;
    }

    private CoordState buildCoordState() {
        CoordState coordState = new CoordState() {
            @Override
            public Spheric toSpheric(int screenX, int screenY) {
                return sphProjection.toSphericCoordinates(screenX, screenY);
            }

            /**
             * Returns VisibleScreenPoint which is never null and contains screen point and
             * info whether such point is visibleHemis
             * @param spheric
             * @return
             */
            @Override
            public ScreenPoint toScreenCoordinates(Spheric spheric) {
                return sphProjection.toScreenCoordinates(spheric);
            }

            @Override
            public int getScreenHeight() {
                return screenHeight;
            }

            @Override
            public int getScreenWidth() {
                return screenWidth;
            }

            @Override
            public double getZoom() {
                return zoom;
            }

            @Override
            public int getCenterScreenX() {
                return screenWidth / 2;
            }

            @Override
            public int getCenterScreenY() {
                return screenHeight / 2;
            }
        };
        return coordState;
    }

    /**
     * Draw immediatelly something on canvas. Note, this will be cleared by next refresh action which could be during some milliseconds.
     * Useful for drawing some click response, for example something found
     * @param consumer
     */
    public void forceDraw(Consumer<GC> consumer) {
        GC gc = new GC(canvas);
        consumer.accept(gc);
        gc.dispose();
    }

    public void quickRefresh() {
        canvas.redraw();
    }
    
    public void refresh() {
        if (image != null) {
            image.dispose();
        }
        image = null;
        canvas.redraw();
    }
    
    public SphericCanvas setCursorChangedListener(CursorChangedListener cursorChangedListener) {
        this.cursorChangedListener = cursorChangedListener;
        return this;
    }

    public void setZoomChangedListener(ZoomChangedListener zoomChangedListener) {
        this.zoomChangedListener = zoomChangedListener;
    }

    public SphericCanvas setSphProjection(SphProjection sphProjection) {
        this.sphProjection = sphProjection;
        reinitProjection();
        return this;
    }


    public SphericCanvas setFindSelectionListener(FindSelectionListener pointClickedListener) {
        this.findSelectionListener = pointClickedListener;
        return this;
    }
    
    public SphericCanvas setSpheric0(Spheric spheric0) {
        this.spheric0 = spheric0;
        return this;
    }
    

    public Spheric getSpheric0() {
        return spheric0;
    }
    
    public void setToolListener(ToolListener toolListener) {
        this.toolListener = toolListener;
    }

    public SphProjection getSphProjection() {
        return sphProjection;
    }

    public void setDragButtonType(MouseButtonType dragButtonType) {
        this.dragButtonType = dragButtonType;
    }

    
    public void setCanvasKeyListener(CanvasKeyListener canvasCancelListener) {
        this.canvasKeyListener = canvasCancelListener;
    }

    void clearResizeRunnable() {
        this.resizeRunnable = null;
    }

    public void print(GC gcPrinter, int marginLeftPagePixels, int marginTopPagePixels, int pageWidth) {
        double scaleFactor = (pageWidth - marginLeftPagePixels * 2.0) / canvas.getSize().x;
        CoordState coordState = buildCoordState();

        Image image = new Image(canvas.getDisplay(), canvas.getSize().x, canvas.getSize().y);
        GC gc = new GC(image);
        gc.setInterpolation(SWT.NONE);
        gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_WHITE));
        gc.fillRectangle(new Rectangle(0, 0, screenWidth, screenHeight));

        drawListener.draw(gc, coordState);

        gc.dispose();

        gcPrinter.drawImage(image, 0, 0, image.getImageData().width, image.getImageData().height, marginLeftPagePixels, marginTopPagePixels,
                (int) (scaleFactor * image.getImageData().width), (int) (scaleFactor * image.getImageData().height));
    }

    public Image getImage() {
        return image;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Set buttons on which the tool listener should react. 1 = usually left button, 2 = middle, 3 = usually right
     * @param toolButtons
     */
	public void setToolButtons(Set<Integer> toolButtons) {
		this.toolButtons = toolButtons;
	}

	public void setDraggingButtonSinceMovePixels(int draggingButtonSinceMovePixels) {
		this.draggingSincePixels = draggingButtonSinceMovePixels;
	}
}
