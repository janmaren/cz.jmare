package org.maren.gis.projection;

import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;

/**
 * See <a href="https://github.com/OSUCartography/JMapProjLib/tree/master/src/com/jhlabs/map/proj">JMapProjLib</a> how to create a projection
 * @author jmarencik
 */
public interface SphProjection {
    /**
     * Returns VisibleScreenPoint which is never null and contains screen point and
     * info whether such point is visibleHemis
     * @param spheric
     * @return
     */
    ScreenPoint toScreenCoordinates(Spheric spheric);
    
    /**
     * Return spheric coordinates of point of screen or null when no such point exists
     * @param screenX
     * @param screenY
     * @return
     */
    Spheric toSphericCoordinates(int screenX, int screenY);
    
    /**
     * Set info about screen. Call this anytime any of parameters changes
     * @param spheric0
     * @param centerScreenX
     * @param centerScreenY
     * @param zoom
     */
    void init(Spheric spheric0, int centerScreenX, int centerScreenY, double zoom);
    
    class ScreenPoint {
        public PointInt point;
        
        /**
         * Point is on visible hemisphere regardless position on screen (it can be outside of screen but still true)
         */
        public boolean visibleHemis;
    }
}
