package org.maren.gis.projection;

import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointFloat;
import org.maren.gis.util.PointInt;

/**
 * X increases from left to right, Y from bottom to top. Zoom and shift supported.
 */
public class DirectSphProjection implements SphProjection {
    private Spheric spheric0;
    private int centerScreenX;
    private int centerScreenY;
    private double zoom;
    @Override
    public ScreenPoint toScreenCoordinates(Spheric spheric) {
        ScreenPoint screenPoint = new ScreenPoint();
        double x = zoom * (spheric.lon - spheric0.lon);
        double y = zoom * (spheric.lat - spheric0.lat);
        PointFloat pointReal = new PointFloat((float) (x + centerScreenX), (float) (centerScreenY - y));
        screenPoint.point = new PointInt((int) Math.round(pointReal.x), (int) Math.round(pointReal.y));
        screenPoint.visibleHemis = true;
        return screenPoint;
    }

    @Override
    public Spheric toSphericCoordinates(int screenX, int screenY) {
        double x = screenX - centerScreenX;
        double y = centerScreenY - screenY;
        double lam = x / zoom + spheric0.lon;
        double phi = y / zoom + spheric0.lat;
        return new Spheric(lam, phi);
    }

    @Override
    public void init(Spheric spheric0, int centerScreenX, int centerScreenY, double zoom) {
        this.spheric0 = spheric0;
        this.centerScreenX = centerScreenX;
        this.centerScreenY = centerScreenY;
        this.zoom = zoom;
    }
}
