package org.maren.gis.projection;

import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointFloat;
import org.maren.gis.util.PointInt;

/**
 * Projection of farther hemisphere. The meridians are from 0 to 360 degrees.
 */
public class OrthoUniSphProjection implements SphProjection {
    private Spheric spheric0;
    private int centerScreenX;
    private int centerScreenY;
    private double zoom;

    private double cosSpheric0Dec;
    private double sinSpheric0Dec;
    /**
     * https://www.projectpluto.com/project.htm
     * https://en.wikipedia.org/wiki/Orthographic_map_projection
     * spheric.ra is lambda
     * spheric.dec is phi (ala latitude)
     * @return screen coordinates which can be directly plotted by gc draw or null when not visibleHemis point
     */
    @Override
    public ScreenPoint toScreenCoordinates(Spheric spheric) {
        ScreenPoint visibleScreenPoint = new ScreenPoint();
        double deltaRa = spheric0.lon - spheric.lon;
        double sinDec = sin(spheric.lat);
        double cosDec = cos(spheric.lat);
        double cosDeltaRa = cos(deltaRa);
        double x = zoom * cosDec * sin(deltaRa);
        double y = zoom * (cosSpheric0Dec * sinDec - sinSpheric0Dec * cosDec * cosDeltaRa);
        PointFloat pointReal = new PointFloat((float) (centerScreenX + x), (float) (centerScreenY - y));
        visibleScreenPoint.point = new PointInt((int) Math.round(pointReal.x), (int) Math.round(pointReal.y));
        double z1 = sinDec * sinSpheric0Dec + cosDec * cosSpheric0Dec * cosDeltaRa;
        visibleScreenPoint.visibleHemis = z1 >= 0;
        return visibleScreenPoint;
    }

    /**
     * spheric.ra is lambda
     * spheric.dec is phi (ala latitude)
     * https://en.wikipedia.org/wiki/Orthographic_map_projection
     */
    @Override
    public Spheric toSphericCoordinates(int screenX, int screenY) {
        double x = centerScreenX - screenX; // swapping sign
        double y = centerScreenY - screenY;
        if (x == 0 && y == 0) {
            return spheric0;
        }
        double ro = sqrt(x * x + y * y);
        if (ro > zoom) {
            return null;
        }
        double c = asin(ro / zoom);
        double dec = asin(cos(c) * sinSpheric0Dec + (y * sin(c) * cosSpheric0Dec / ro) );
        double ra = spheric0.lon + Math.atan(x * sin(c) / (ro * cos(c) * cosSpheric0Dec - y * sin(c) * sinSpheric0Dec));
        Spheric spheric = new Spheric(ra, dec);
        
        // detection whether we are not on the other side
        ScreenPoint screenPoint = toScreenCoordinates(spheric);
        PointInt screenCoordinates = screenPoint.point;
        if (Math.abs(screenX - screenCoordinates.x) > 2 || Math.abs(screenY - screenCoordinates.y) > 2) {
            ra += PI;
            spheric = new Spheric(ra, dec);
        }
        
        return spheric;
    }

    public boolean isVisible(Spheric spheric) {
        double deltaRa = spheric0.lon - spheric.lon;;
        double sinDec = sin(spheric.lat);
        double cosDec = cos(spheric.lat);
        double cosDeltaRa = cos(deltaRa);
        double z1 = sinDec * sinSpheric0Dec + cosDec * cosSpheric0Dec * cosDeltaRa;
        return z1 >= 0;
    }

    @Override
    public void init(Spheric spheric0, int centerScreenX, int centerScreenY, double zoom) {
        this.spheric0 = spheric0;
        this.centerScreenX = centerScreenX;
        this.centerScreenY = centerScreenY;
        this.zoom = zoom;
        
        cosSpheric0Dec = cos(spheric0.lat);
        sinSpheric0Dec = sin(spheric0.lat);
    }
}
