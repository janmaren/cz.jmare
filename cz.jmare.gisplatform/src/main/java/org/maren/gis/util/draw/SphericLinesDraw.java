package org.maren.gis.util.draw;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.maren.gis.projection.SphProjection.ScreenPoint;
import org.maren.gis.sphericcanvas.CoordState;
import org.maren.gis.type.Spheric;
import org.maren.gis.util.PointInt;

public class SphericLinesDraw {
    private static final int LINES_STEP_DEGREES = 15;


	/**
     * Draw meridian and parallels lines using grey color on universe spheroid (longitude 0 - 24h).<br>
     * It's responsibility of calling code to set also the right font
     * @param gc
     * @param coordState
     * @param showLinesNumbers
     */
    public static void drawGreyDarkUniverseLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        int origAlpha = gc.getAlpha();
        gc.setAlpha(140);
        
        Color origColor = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
        
        drawUniverseLines(gc, coordState, showLinesNumbers);
        
        gc.setForeground(origColor);
        gc.setAlpha(origAlpha);
    }
    
    /**
     * Draw meridian and parallels lines using grey color on earth spheroid (longitude -180 to 180 degrees).<br>
     * It's responsibility of calling code to set also the right font
     * @param gc
     * @param coordState
     * @param showLinesNumbers
     */
    public static void drawGreyDarkEarthLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
    	drawGreyDarkEarthLines(gc, coordState, showLinesNumbers, Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
    }
    
    /**
     * Draw meridian and parallels lines using grey color on earth spheroid (longitude -180 to 180 degrees).<br>
     * It's responsibility of calling code to set also the right font
     * @param gc
     * @param coordState
     * @param showLinesNumbers
     */
    public static void drawGreyDarkEarthLines(GC gc, CoordState coordState, boolean showLinesNumbers, Color color) {
        int origAlpha = gc.getAlpha();
        gc.setAlpha(140);
        
        Color origColor = gc.getForeground();
        gc.setForeground(color);
        
        drawEarthLines(gc, coordState, showLinesNumbers);
        
        gc.setForeground(origColor);
        gc.setAlpha(origAlpha);
    }
    
    /**
     * Draw meridian and parallels lines on universe spheroid (longitude 0 - 24h).<br>
     * It's responsibility of calling code to set also the right font and color
     * @param gc
     * @param coordState
     * @param showLinesNumbers
     */
    public static void drawUniverseLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        for (int hour = 0; hour < 24; hour++) {
            double hourRad = hour * 15 * Math.PI / 180.0;
            SphericLinesDraw.drawMeridian(gc, hourRad, coordState, String.valueOf(hour));
        }
        for (int par = -90; par <= 90; par += 15) {
            double degRad = Math.toRadians(par);
            SphericLinesDraw.drawParallel(gc, degRad, coordState, String.valueOf(par));
        }
    }

    /**
     * Draw meridian and parallels lines on earth spheroid (longitude -180 to 180 degrees).<br>
     * It's responsibility of calling code to set also the right font and color
     * @param gc
     * @param coordState
     * @param showLinesNumbers
     */
    public static void drawEarthLines(GC gc, CoordState coordState, boolean showLinesNumbers) {
        for (int lon = -180; lon < 180; lon += LINES_STEP_DEGREES) {
            double lonRad = Math.toRadians(lon);
            SphericLinesDraw.drawMeridian(gc, lonRad, coordState, String.valueOf(lon));
        }
        for (int par = -90; par <= 90; par += LINES_STEP_DEGREES) {
            double degRad = Math.toRadians(par);
            SphericLinesDraw.drawParallel(gc, degRad, coordState, String.valueOf(par));
        }
    }
    
    public static void drawMeridian(GC gc, double ra, CoordState coordState, String showLabel) {
        Integer lastX = null;
        Integer lastY = null;
        Spheric lastSph = null;
        boolean drawnTime = false;
        int invisibleCount = 0;
        for (int i = -90; i <= 90; i += 5) {
            double dec = i * Math.PI / 180.0;
            Spheric sph = new Spheric(ra, dec);
            ScreenPoint visibleScreenPoint = coordState.toScreenCoordinates(sph);
            boolean visible = visibleScreenPoint.visibleHemis;
            if (!visible) {
                invisibleCount++;
            } else {
                invisibleCount = 0;
            }
            PointInt screenCoordinates = visibleScreenPoint.point;
            if (lastX != null && lastY != null && invisibleCount < 2) {
                gc.drawLine(lastX, lastY, screenCoordinates.x, screenCoordinates.y);
                if (!drawnTime && i > -40 && coordState.getZoom() >= 80 && showLabel != null && screenCoordinates.y < coordState.getScreenHeight() - 10) {
                    gc.drawString(showLabel, screenCoordinates.x + 1, screenCoordinates.y, true);
                    drawnTime = true;
                }
            } else {
                if (lastX != null && lastY != null && visibleScreenPoint.visibleHemis) {
                    drawCroppedLine(gc, lastSph, sph, coordState);
                }
            }
            lastX = screenCoordinates.x;
            lastY = screenCoordinates.y;
            lastSph = sph;
        }
    }

    public static void drawParallel(GC gc, double degRad, CoordState coordState, String showLabel) {
        boolean drawnTime = false;
        Integer lastX = null;
        Integer lastY = null;
        int from = -180;
        int invisibleCount = 0;
        for (int i = from; i <= 180; i += 5) {
            double ra = i * Math.PI / 180.0;
            Spheric sph = Spheric.normalizedSpheric(ra, degRad);
            ScreenPoint visibleScreenPoint = coordState.toScreenCoordinates(sph);
            boolean visible = visibleScreenPoint.visibleHemis;
            if (!visible) {
                invisibleCount++;
            } else {
                invisibleCount = 0;
            }
            PointInt screenCoordinates = visibleScreenPoint.point;
            if (lastX != null && lastY != null && invisibleCount < 2) {
                gc.drawLine(lastX, lastY, screenCoordinates.x, screenCoordinates.y);
                if (!drawnTime && coordState.getZoom() >= 80 && showLabel != null) {
                    gc.drawString(showLabel, screenCoordinates.x, screenCoordinates.y - 2, true);
                    drawnTime = true;
                }
            }
            lastX = screenCoordinates.x;
            lastY = screenCoordinates.y;
        }
    }

    
    public static void drawCroppedLine(GC gc, Spheric sph1, Spheric sph2, CoordState coordState) {
        ScreenPoint vsp1 = coordState.toScreenCoordinates(sph1);
        ScreenPoint vsp2 = coordState.toScreenCoordinates(sph2);
        if (!vsp1.visibleHemis && !vsp2.visibleHemis) {
            return;
        }
        gc.drawLine(vsp1.point.x, vsp1.point.y, vsp2.point.x, vsp2.point.y);
    }
}
