package org.maren.gis.util.draw;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.maren.gis.sphericcanvas.CoordState;

public class BorderDraw {
    /**
     * Draw circle around spheroid using dark grey color
     * @param gc
     * @param coordState
     */
    public static void drawGreyDarkSphericBorder(GC gc, CoordState coordState) {
        Color origColor = gc.getForeground();
        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY));
        drawSphericBorder(gc, coordState);
        gc.setForeground(origColor);
    }
    
    /**
     * Draw circle around spheroid
     * @param gc
     * @param coordState
     */
    public static void drawSphericBorder(GC gc, CoordState coordState) {
        int centerScreenX = coordState.getCenterScreenX();
        int centerScreenY = coordState.getCenterScreenY();
        gc.drawOval(centerScreenX - (int)(coordState.getZoom()), centerScreenY - (int) (coordState.getZoom()), (int) coordState.getZoom() * 2, (int) coordState.getZoom() * 2);
    }
}
