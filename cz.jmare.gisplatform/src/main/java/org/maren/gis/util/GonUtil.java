package org.maren.gis.util;

import static java.lang.Math.PI;

import org.maren.gis.type.Spheric;

public class GonUtil {
    public static final double TWO_PI = 2 * PI;
    
    public static final double PI_HALF = PI / 2;
    
    public final static double MINUS_PI_HALF = -PI_HALF;
    
    /**
     * Normalize to interval 0 to 2 * PI
     * @param angleRad
     * @return
     */
    public static double normalizeRadZeroTwoPi(double angleRad) {
        angleRad %= TWO_PI;
        angleRad = (angleRad + TWO_PI) % TWO_PI;
        return angleRad;
    }

    /**
     * Normalize to interval -PI to PI
     * @param angleRad
     * @return
     */
    public static double normalizeRadMinusPiPi(double angleRad) {
        angleRad %= TWO_PI;
        angleRad = (angleRad + TWO_PI) % TWO_PI;
        if (angleRad > PI) {
            angleRad -= TWO_PI;
        }
        return angleRad;
    }
    
    public static double angularDistanceRad(double lambda1Rad, double beta1Rad, double lambda2Rad, double beta2Rad) {
        double sum = Math.sin(beta1Rad) * Math.sin(beta2Rad) + Math.cos(beta1Rad) * Math.cos(beta2Rad) * Math.cos(lambda1Rad - lambda2Rad);
        if (sum > 1.0) {
            if (sum > 1 + 1e-5) {
                return Double.NaN;
            } else {
                return 0;
            }
        }
        return Math.acos(sum);
    }
	
	public static Spheric normalizeGeoSpheric(Spheric spheric) {
		Spheric point = new Spheric(spheric.lon, spheric.lat);
		int quadrant = (int) (Math.floor(Math.abs(spheric.lat) / PI_HALF) % 4);
		var pole = (spheric.lat > 0) ? PI_HALF : -PI_HALF;
		var offset = spheric.lat % PI_HALF;

		switch (quadrant) {
		case 0:
			point.lat = offset;
			break;
		case 1:
			point.lat = pole - offset;
			point.lon += Math.PI;
			break;
		case 2:
			point.lat = -offset;
			point.lon += Math.PI;
			break;
		case 3:
			point.lat = -pole + offset;
			break;
		}

		if (point.lon > Math.PI || point.lon < -Math.PI) {
			point.lon -= Math.floor((point.lon + Math.PI) / TWO_PI) * TWO_PI;
		}

		return point;
	}
    
    public static void main(String[] args) {
		Spheric normalizedLatLonGeoLoc = normalizeGeoSpheric(new Spheric(-6.027192999051206, -5.416721558438529));
		normalizedLatLonGeoLoc = normalizeGeoSpheric(new Spheric(-0.5505308289704693, -7.610323594755265));
		System.out.println(Math.toDegrees(normalizedLatLonGeoLoc.lat) + ", " + Math.toDegrees(normalizedLatLonGeoLoc.lon));
	}
}
