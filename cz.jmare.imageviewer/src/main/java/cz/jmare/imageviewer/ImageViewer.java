package cz.jmare.imageviewer;

import cz.jmare.pureraster.DrawObjectsListener;
import cz.jmare.pureraster.ImageCoordinate;
import cz.jmare.pureraster.PureKeyPressedListener;
import cz.jmare.pureraster.RasterToolListener;
import cz.jmare.pureraster.ZoomPan;
import cz.jmare.pureraster.ZoomPanChangedListener;
import cz.jmare.swt.action.ActionOperation;
import cz.jmare.swt.action.SwitchOperation;
import cz.jmare.swt.image.ImageUtil;
import cz.jmare.swt.util.Create;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import cz.jmare.rastercomposite.RasterComposite;

import java.util.StringJoiner;

import static cz.jmare.rastercomposite.RasterComposite.FEATURE_DRAW_CURSOR;
import static cz.jmare.rastercomposite.RasterComposite.FEATURE_SHOW_COLOR;
import static cz.jmare.rastercomposite.RasterComposite.FEATURE_SHOW_ZOOM;

public class ImageViewer extends ApplicationWindow implements ZoomPanChangedListener, PureKeyPressedListener {

    private final ImageViewerEvent eventHandler;
    private ToolBarManager toolbarManager = new ToolBarManager();
    private MenuManager menuFile;
    private MenuManager menuManager = new MenuManager();
    private RasterComposite rasterComposite;
    private ActionOperation fitOperation;
    private SwitchOperation lockOperation;

    private ActionOperation refreshOperation;
    private Label imageInfoLabel;

    private boolean supportRefresh;

    private boolean supportLock;

    private int initWidth = 900;

    private int initHeight = 750;

    public ImageViewer(Shell shell, ImageViewerEvent eventHandler) {
        super(shell);
        this.eventHandler = eventHandler;
        menuManager.add(menuFile = new MenuManager("File"));
        addMenuBar();
        addToolBar(SWT.FLAT | SWT.WRAP);
        addStatusLine();
    }

    protected Control createContents(Composite parent) {
        initActions();

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());

        rasterComposite = new RasterComposite(composite, SWT.NONE,
                FEATURE_SHOW_ZOOM | FEATURE_DRAW_CURSOR | FEATURE_SHOW_COLOR);
        rasterComposite.setZoomPanChangedListener(this);
        rasterComposite.setPureKeyPressedListener(this);
        rasterComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        imageInfoLabel = Create.label(composite, "");
        GridData imInfoData = new GridData();
        imInfoData.grabExcessHorizontalSpace = true;
        imInfoData.horizontalAlignment = SWT.FILL;
        imageInfoLabel.setLayoutData(imInfoData);
        
        populateMenuFile();
        menuManager.update(true);
        menuFile.update(true);
        toolbarManager.update(true);
        
        return composite;
    }

    private void populateMenuFile() {
        toolbarManager.add(fitOperation);
        if (supportLock) {
            toolbarManager.add(lockOperation);
        }
        if (supportRefresh) {
            toolbarManager.add(refreshOperation);
        }
        populateCustomMenuFile(toolbarManager);
    }

    /**
     * Sublass with method which adds own toolbar operations
     */
    protected void populateCustomMenuFile(ToolBarManager toolbarManager) {
    }

    private void initActions() {
        ImageUtil.put("folder", "/image/folder.png");
        ImageUtil.put("fit", "/image/fit.png");
        ImageUtil.put("lock", "/image/lock.png");
        ImageUtil.put("refresh", "/image/refresh.png");

        fitOperation = new ActionOperation("Fit", "fit", SWT.ALT | 'O', () -> {
            rasterComposite.fit();
            rasterComposite.refresh();
        });

        lockOperation = new SwitchOperation("Lock Zoom", "lock", SWT.ALT | 'L', (b) -> {

        });

        refreshOperation = new ActionOperation("Reload", "refresh", SWT.CTRL | 'R', () -> {
            eventHandler.onReload(this);
        });
    }

    public void setImageAndCenterFitRedraw(Image image, Integer bitsPerPixel) {
        rasterComposite.setImageAndCenterFitRedraw(image);
        populateInfo(image, bitsPerPixel);
    }

    public void setImageAndRedraw(Image image, Integer bitsPerPixel) {
        rasterComposite.setImageAndRedraw(image);
        populateInfo(image, bitsPerPixel);
    }
    
    public void setImage(Image image, Integer bitsPerPixel) {
        rasterComposite.setImage(image);
        populateInfo(image, bitsPerPixel);
    }

    private void populateInfo(Image image, Integer bitsPerPixel) {
        StringJoiner sj = new StringJoiner(", ");
        if (image != null) {
            sj.add("Dim: " + image.getBounds().width + " x " + image.getBounds().height);
        }
        if (bitsPerPixel != null) {
            sj.add("Bits per pixel: " + bitsPerPixel);
        }
        imageInfoLabel.setText(sj.toString());
    }

    @Override
    protected ToolBarManager createToolBarManager(int style) {
        return toolbarManager;
    }
    
    @Override
    protected MenuManager createMenuManager() {
        return menuManager;
    }
    
    protected void initializeBounds() {
        if (initWidth != -1 && initHeight != -1) {
            getShell().setSize(initWidth, initHeight);
        }
        getShell().setMinimumSize(360, 300);
    }
    
    @Override
    public boolean close() {
        eventHandler.onImageViewerClose(this);
        return super.close();
    }

    @Override
    protected void handleShellCloseEvent() {
        super.handleShellCloseEvent();
    }


    public void centerToCoordinate(double x, double y) {
        rasterComposite.centerToCoordinateAndRedraw(x, y);
    }

    @Override
    protected int getShellStyle() {
        return SWT.SHELL_TRIM | SWT.MODELESS;
    }

    public void setLabel(String path) {
        getShell().setText(path);
    }

    public void setShellLocation(int x, int y) {
        getShell().setLocation(x, y);
    }

    public void setShellSize(int width, int height) {
        getShell().setSize(width, height);
    }

    public Point getShellLocation() {
        return getShell().getLocation();
    }

    public void setFocus() {
        getShell().setFocus();
    }

    public void refresh() {
        rasterComposite.refresh();
    }

    public void closeWindowImage() {
        super.close();
    }

    @Override
    public void onZoomPanChanged(ZoomPan zoomPan) {
        if (lockOperation.isChecked()) {
            eventHandler.onPanChanged(this, zoomPan.getPan());
        } else {
            eventHandler.onZoomPanChanged(this, zoomPan);
        }
    }

    public void setZoomPanAndRedraw(ZoomPan zoomPan) {
        if (lockOperation.isChecked()) {
            rasterComposite.setPanAndRedraw(zoomPan.getPan());
        } else {
            rasterComposite.setZoomPanAndRedraw(zoomPan);
        }
    }

    public boolean isUpright() {
        return rasterComposite.isUpright();
    }

    public void setPanAndRedraw(ImageCoordinate pan) {
        rasterComposite.setPanAndRedraw(pan);
    }

    public void setRasterToolListener(RasterToolListener rasterToolListener) {
        rasterComposite.setRasterToolListener(rasterToolListener);
    }

    public void setDragButton(int dragButton) {
        rasterComposite.setDragButton(dragButton);
    }

    public void setDrawObjectsListener(DrawObjectsListener drawObjectsListener) {
        rasterComposite.setDrawObjectsListener(drawObjectsListener);
    }

    public void setSupportRefresh(boolean supportRefresh) {
        this.supportRefresh = supportRefresh;
    }

    public void setSupportLock(boolean supportLock) {
        this.supportLock = supportLock;
    }

    /**
     * Initial dimensions, must be called before open
     */
    public void setInitialDimensions(ViewerSize viewerSize) {
        if (viewerSize == ViewerSize.SMALL) {
            initWidth = 300;
            initHeight = 250;
        } else if (viewerSize == ViewerSize.MEDIUM_SMALL) {
            initWidth = 600;
            initHeight = 500;
        } else if (viewerSize == ViewerSize.MEDIUM) {
            initWidth = 900;
            initHeight = 750;
        } else if (viewerSize == ViewerSize.LARGE) {
            initWidth = 1200;
            initHeight = 1000;
        }
    }

    @Override
    public void onRasterKeyPressed(int keyCode, int stateMask) {
        eventHandler.onRasterKeyPressed(this, keyCode, stateMask);
    }

    public void setCursor(ImageData sourceData) {
        rasterComposite.setCursor(sourceData);
    }

    public void setErrorMessage(String str) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), str != null ? str : "Operation failed");
    }

    public void setErrorMessage(Exception e) {
        getStatusLineManager().setErrorMessage(ImageUtil.getSystemImage16(SWT.ICON_WARNING), e.getMessage() != null ? e.getMessage() : "Operation failed");
    }

    public void setInfoMessage(String str) {
        if (str == null) {
            return;
        }
        getStatusLineManager().setMessage(ImageUtil.getSystemImage16(SWT.ICON_INFORMATION), str);
    }
    
    public ZoomPan getZoomPan() {
        return rasterComposite.getZoomPan();
    }
}
