package cz.jmare.imageviewer;

import cz.jmare.pureraster.ImageCoordinate;
import cz.jmare.pureraster.ZoomPan;

public interface ImageViewerEvent {
    void onImageViewerClose(ImageViewer imageViewer);

    void onZoomPanChanged(ImageViewer imageViewer, ZoomPan zoomPan);

    void onPanChanged(ImageViewer imageViewer, ImageCoordinate pan);

    void onReload(ImageViewer imageViewer);

    void onRasterKeyPressed(ImageViewer imageViewer, int keyCode, int stateMask);
}
