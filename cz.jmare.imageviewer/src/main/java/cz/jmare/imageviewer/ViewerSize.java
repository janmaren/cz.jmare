package cz.jmare.imageviewer;

public enum ViewerSize {
    SMALL, MEDIUM_SMALL, MEDIUM, LARGE
}
