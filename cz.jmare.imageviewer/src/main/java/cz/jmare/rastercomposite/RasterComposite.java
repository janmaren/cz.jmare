package cz.jmare.rastercomposite;

import cz.jmare.pureraster.DrawObjectsListener;
import cz.jmare.pureraster.ImageCoordinate;
import cz.jmare.pureraster.PureKeyPressedListener;
import cz.jmare.pureraster.PureRasterManager;
import cz.jmare.pureraster.RasterCursorMoveListener;
import cz.jmare.pureraster.RasterToolListener;
import cz.jmare.pureraster.ZoomPan;
import cz.jmare.pureraster.ZoomPanChangedListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import cz.jmare.swt.util.Create;

public class RasterComposite extends Composite {
    private PureRasterManager pureRasterManager;
    private Label xLabel;
    private Label yLabel;
    private Label zoomLabel;
    private Label fovLabel;
    public final static int FEATURE_NO = 0;
    public final static int FEATURE_SHOW_ZOOM = 1;
    public final static int FEATURE_DRAW_CURSOR = 2;
    public final static int FEATURE_SHOW_FOV = 4;
    public final static int FEATURE_SHOW_COLOR = 8;
    private boolean showZoom;
    private boolean showFov;
    private boolean showColor;
    private boolean drawCursor;

    private RasterCursorMoveListener rasterCursorMoveListener;
    private Label colorLabel;

    public RasterComposite(Composite parent, int style) {
        super(parent, style);
        this.drawCursor = true;
        setLayout(new GridLayout());
        createPanelContent();
    }

    public RasterComposite(Composite parent, int style, int features) {
        super(parent, style);
        this.showZoom = (features & FEATURE_SHOW_ZOOM) == FEATURE_SHOW_ZOOM;
        this.drawCursor = (features & FEATURE_DRAW_CURSOR) == FEATURE_DRAW_CURSOR;
        this.showFov = (features & FEATURE_SHOW_FOV) == FEATURE_SHOW_FOV;
        this.showColor = (features & FEATURE_SHOW_COLOR) == FEATURE_SHOW_COLOR;
        setLayout(new GridLayout());
        createPanelContent();
    }
    
    private void createPanelContent() {
        pureRasterManager = new PureRasterManager(this, SWT.BORDER);
        GridData gridDataMain = new GridData(SWT.FILL, SWT.FILL, true, true);
        pureRasterManager.setLayoutData(gridDataMain);
        
        pureRasterManager.setRasterCursorMoveListener((positionData, mouseEvent) -> {
            if (positionData.imageX != null && positionData.imageY != null) {
                xLabel.setText(String.valueOf((int) (double) positionData.imageX));
                yLabel.setText(String.valueOf((int) (double) positionData.imageY));
            } else {
                xLabel.setText("");
                yLabel.setText("");
            }
            if (zoomLabel != null) {
                zoomLabel.setText(String.valueOf(positionData.zoom));
            }
            if (rasterCursorMoveListener != null) {
                rasterCursorMoveListener.changedTo(positionData, mouseEvent);
            }
            if (showColor) {
                if (positionData.imageX != null && positionData.imageY != null) {
                    RGB color = pureRasterManager.getColor(new ImageCoordinate(positionData.imageX, positionData.imageY));
                    if (color == null) {
                        colorLabel.setText("");
                    } else {
                        colorLabel.setText("R: " + color.red + ", G: " + color.green + ", B: " + color.blue);
                    }
                } else {
                    colorLabel.setText("");
                }
            }
        });

        if (drawCursor) {
            Image crossImage = new Image(getShell().getDisplay(), PureRasterManager.class.getResourceAsStream("/image/cursor-cross.png"));
            ImageData sourceData = crossImage.getImageData();
            sourceData.transparentPixel = 0;
            pureRasterManager.setCursor(sourceData);
        }

        Composite raDecComposite = Create.noMarginGridComposite(this, 1);
        GridData raDecData = new GridData();
        raDecData.verticalAlignment = SWT.TOP;
        raDecComposite.setLayoutData(raDecData);
        Composite xyComposite = Create.noMarginGridComposite(raDecComposite, 6);
        Create.label(xyComposite, "X:");
        xLabel = Create.label(xyComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 60;
            label.setLayoutData(layoutData);
        });
        Create.label(xyComposite, "Y:");
        yLabel = Create.label(xyComposite, "", label -> {
            GridData layoutData = new GridData();
            layoutData.widthHint = 60;
            label.setLayoutData(layoutData);
        });
        if (showZoom) {
            Create.label(xyComposite, "Zoom:");
            zoomLabel = Create.label(xyComposite, "", label -> {
                GridData layoutData = new GridData();
                layoutData.widthHint = 120;
                label.setLayoutData(layoutData);
            });
        }
        if (showFov) {
            Create.label(raDecComposite, "Fov:");
            fovLabel = Create.label(raDecComposite, "", label -> {
                GridData layoutData = new GridData();
                layoutData.widthHint = 210;
                label.setLayoutData(layoutData);
            });
        }
        if (showColor) {
            colorLabel = Create.label(raDecComposite, "", label -> {
                GridData layoutData = new GridData();
                layoutData.widthHint = 140;
                label.setLayoutData(layoutData);
            });
        }
    }

    public void setImageAndCenterFitRedraw(Image image) {
        pureRasterManager.setImageAndCenterFitRefresh(image);
    }
    
    public void setImageAndRedraw(Image image) {
        pureRasterManager.setImageAndRefresh(image);
    }
    
    public void setDrawObjectsListener(DrawObjectsListener drawObjectsListener) {
        pureRasterManager.setDrawObjectsListener(drawObjectsListener);
    }

    public void setZoomPanChangedListener(ZoomPanChangedListener zoomPanChangedListener) {
        pureRasterManager.setZoomPanChangedListener(zoomPanChangedListener);
    }
    
    public void refresh() {
        pureRasterManager.refresh();
    }

    public void setFovText(String text) {
        if (fovLabel != null) {
            if (text == null) {
                fovLabel.setText("");
            } else {
                fovLabel.setText(text);
            }
        }
    }

    public Rectangle getCanvasDim() {
        return pureRasterManager.getCanvasDim();
    }

    public double getZoom() {
        return pureRasterManager.getZoom();
    }

    public void setRasterCursorMoveListener(RasterCursorMoveListener rasterCursorMoveListener) {
        this.rasterCursorMoveListener = rasterCursorMoveListener;
    }

    public void centerToCoordinateAndRedraw(double x, double y) {
        pureRasterManager.centerToPosition(x, y);
        pureRasterManager.refresh();
    }

    public void center() {
        pureRasterManager.center();
    }

    public void fit() {
        pureRasterManager.fit();
    }

    public void setZoomPanAndRedraw(ZoomPan zoomPan) {
        pureRasterManager.setZoomPanAndRefresh(zoomPan);
    }

    public void setPanAndRedraw(ImageCoordinate coordinate) {
        pureRasterManager.setPanAndRefresh(coordinate);
    }

    public boolean isUpright() {
        Image image = pureRasterManager.getImage();
        if (image == null) {
            return false;
        }
        return image.getImageData().height > image.getImageData().width;
    }

    public void setRasterToolListener(RasterToolListener rasterToolListener) {
        pureRasterManager.setRasterToolListener(rasterToolListener);
    }

    public void setDragButton(int dragButton) {
        pureRasterManager.setDragButton(dragButton);
    }

    public void setDragStateMask(int dragStateMask) {
        pureRasterManager.setDragStateMask(dragStateMask);
    }

    public void setPureKeyPressedListener(PureKeyPressedListener pureKeyPressedListener) {
        pureRasterManager.setPureKeyPressedListener(pureKeyPressedListener);
    }

    public void setCursor(ImageData sourceData) {
        pureRasterManager.setCursor(sourceData);
    }

    public void setImage(Image image) {
        pureRasterManager.setImage(image);
    }
    
    public ZoomPan getZoomPan() {
        return pureRasterManager.getZoomPan();
    }
}
