package curve;

import org.junit.Before;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.raw.BigRawCurveBuilder;
import cz.jmare.math.geometry.entity.Point;

public class SampleCurveTest {
    CurveHolder curveHolder;

    @Before
    public void prepare() {
        try (BigRawCurveBuilder bigPointsCurveBuilder = new BigRawCurveBuilder()) {
            bigPointsCurveBuilder.add(new Point(1, 2));
            bigPointsCurveBuilder.add(new Point(2, 7));
            bigPointsCurveBuilder.add(new Point(3, 4));
            bigPointsCurveBuilder.add(new Point(4, 2));
            bigPointsCurveBuilder.add(new Point(5, 2));
            bigPointsCurveBuilder.add(new Point(6, 0.1));
            curveHolder = bigPointsCurveBuilder.build();
        }
    }
}
