package curve;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.function.Supplier;

import org.junit.Test;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.curve.func.CurveHolderSupplier;
import cz.jmare.math.curve.func.RawCurveSupplier;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.mexpr.ExpressionParser;
import cz.jmare.mexpr.type.VectorD1Holder;
import cz.jmare.mexpr.type.VectorD1Impl;

public class CurveExprTest {
    @Test
    public void rawFuncInterpolPointsTest() {
        ExpressionParser expressionParser = new ExpressionParser("interpol(rawCurve(\"1, 2; 2, 7; 3, 4; 4, 2; 5, 2; 6, 0.1\"), 1.1)");
        Object evaluate = expressionParser.evaluate();
        assertEquals(2.53, (Double) evaluate, 0.01);
    }

    @Test
    public void rawFuncFuncPointsTest() {
        Supplier<CurveHolder> supp = new Supplier<CurveHolder>() {
            @Override
            public CurveHolder get() {
                return RawCurveSupplier.parse("1, 2; 2, 7; 3, 4; 4, 2; 5, 2; 6, 0.1");
            }};
        ExpressionParser expressionParser = new ExpressionParser("f(1.1)", Map.of("c", supp, "f(x)", "interpol(c, x)"));
        Object evaluate = expressionParser.evaluate();
        assertEquals(2.53, (Double) evaluate, 0.01);
    }

    @Test
    public void foreachToCurveTest() {
        Supplier<CurveHolder> supp = new CurveHolderSupplier(RawCurveSupplier.parse("1, 2; 2, 7; 3, 4; 4, 2; 5, 2; 6, 0.1"));
        VectorD1Impl vectorD1Impl = new VectorD1Impl(new double[] {1, 1.1, 2, 3, 3.5, 3.9, 4, 4.5, 5, 5.1, 6});
        VectorD1Holder vectorD1Holder = new VectorD1Holder(vectorD1Impl);

        ExpressionParser expressionParser = new ExpressionParser("foreach(interpol(origPoints, x), x, samplePoints)", Map.of("origPoints", supp, "samplePoints", vectorD1Holder));
        CurveHolder curveHolder = (CurveHolder) expressionParser.evaluate();
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            Point point = curveReader.get(0);
            assertEquals(1.0, point.x, 0.01);
            assertEquals(2.0, point.y, 0.01);

            point = curveReader.get(1);
            assertEquals(1.1, point.x, 0.01);
            assertEquals(2.53, point.y, 0.01);

            point = curveReader.get(2);
            assertEquals(2.0, point.x, 0.01);
            assertEquals(7.0, point.y, 0.01);

            point = curveReader.get(3);
            assertEquals(3.0, point.x, 0.01);
            assertEquals(4.0, point.y, 0.01);

            point = curveReader.get(4);
            assertEquals(3.5, point.x, 0.01);
            assertEquals(2.6875, point.y, 0.01);

            point = curveReader.get(5);
            assertEquals(3.9, point.x, 0.01);
            assertEquals(2.03, point.y, 0.01);

            point = curveReader.get(6);
            assertEquals(4.0, point.x, 0.01);
            assertEquals(2.0, point.y, 0.01);

            point = curveReader.get(7);
            assertEquals(4.5, point.x, 0.01);
            assertEquals(2.0, point.y, 0.01);

            point = curveReader.get(8);
            assertEquals(5.0, point.x, 0.01);
            assertEquals(2.0, point.y, 0.01);

            point = curveReader.get(9);
            assertEquals(5.1, point.x, 0.01);
            assertEquals(1.96, point.y, 0.01);

            point = curveReader.get(10);
            assertEquals(6.0, point.x, 0.01);
            assertEquals(0.1, point.y, 0.01);
        }
    }

    @Test
    public void sampleCurveTest() {
        Supplier<CurveHolder> supp = new CurveHolderSupplier(
                RawCurveSupplier.parse("1, 2; 2, 7; 3, 4; 4, 2; 5, 2; 6, 0.1"));

        ExpressionParser expressionParser = new ExpressionParser(
                "foreach(interpol(origPoints, x), x, sequence(1, 6, 0.5))", Map.of("origPoints", supp));
        CurveHolder curveHolder = (CurveHolder) expressionParser.evaluate();
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            Point point = curveReader.get(0);
            assertEquals(1.0, point.x, 0.01);
            assertEquals(2.0, point.y, 0.01);

            point = curveReader.get(1);
            assertEquals(1.5, point.x, 0.01);

            point = curveReader.get(2);
            assertEquals(2.0, point.x, 0.01);
            assertEquals(7.0, point.y, 0.01);

            point = curveReader.get(3);
            assertEquals(2.5, point.x, 0.01);

            point = curveReader.get(4);

            long size = curveReader.size();
            point = curveReader.get(size - 1);
            assertEquals(6, point.x, 0.01);
        }
    }
}
