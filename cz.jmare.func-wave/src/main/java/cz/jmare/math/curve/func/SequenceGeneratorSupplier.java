package cz.jmare.math.curve.func;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.type.VectorD1Impl;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sequence")
public class SequenceGeneratorSupplier implements Supplier<VectorD1> {
    private Supplier<Double> fromSupplier;
    private Supplier<Double> toSupplier;
    private Supplier<Double> periodSupplier;

    public SequenceGeneratorSupplier(Supplier<Double> fromSupplier, Supplier<Double> toSupplier,
            Supplier<Double> periodSupplier) {
        super();
        this.fromSupplier = fromSupplier;
        this.toSupplier = toSupplier;
        this.periodSupplier = periodSupplier;
    }

    @Override
    public VectorD1 get() {
        double from = fromSupplier.get();
        double period = periodSupplier.get();
        double number = 1 + (toSupplier.get() - from) / period;
        double ceil = Math.ceil(number);
        int count;
        if (ceil - number * period < 1e+13) {
            count = (int) ceil;
        } else {
            count = (int) number;
        }
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            res.add(from + i * period);
        }
        return new VectorD1Impl(res.stream().mapToDouble(Double::doubleValue).toArray());
    }

}
