package cz.jmare.math.curve.func;

import java.util.List;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.importexport.TxtCurveXYLoader;
import cz.jmare.math.curve.importexport.TxtCurveYLoader;
import cz.jmare.math.curve.util.CurveVectorConvert;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.type.VectorD3;
import cz.jmare.mexpr.type.VectorD3Impl;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.progress.NullLongProgressStatus;

@FunctionalSupplier(name = "loadTxtD3Curve", description = "Load multiple channels with curves from txt.")
public class LoadTxtD3Supplier implements Supplier<VectorD3> {
    private VectorD3 vectorD3;

    private Supplier<String> file;

    public LoadTxtD3Supplier(@Desc("file") Supplier<String> file) {
        this.file = file;
    }

    @Override
    public VectorD3 get() {
        if (vectorD3 == null) {
            TxtCurveXYLoader txtCurveXYLoader = new TxtCurveXYLoader(file.get());
            List<CurveHolder> curveHolders;
            try {
                curveHolders = txtCurveXYLoader.load(new NullLongProgressStatus());
            } catch (Exception e) {
                TxtCurveYLoader txtCurveYLoader = new TxtCurveYLoader(file.get());
                curveHolders = txtCurveYLoader.load(new NullLongProgressStatus());
            }
            VectorD2[] vecs = new VectorD2[curveHolders.size()];
            for (int i = 0; i < curveHolders.size(); i++) {
                CurveHolder curveHolder = curveHolders.get(i);
                VectorD2 vectorD2 = CurveVectorConvert.curveHolderToVectorD2(curveHolder);
                vecs[i] = vectorD2;
            }
            vectorD3 = new VectorD3Impl(vecs);
        }
        return vectorD3;
    }
}
