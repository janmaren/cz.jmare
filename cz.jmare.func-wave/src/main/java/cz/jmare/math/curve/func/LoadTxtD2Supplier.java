package cz.jmare.math.curve.func;

import java.util.List;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.importexport.TxtCurveXYLoader;
import cz.jmare.math.curve.importexport.TxtCurveYLoader;
import cz.jmare.math.curve.util.CurveVectorConvert;
import cz.jmare.mexpr.type.VectorD2;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.progress.NullLongProgressStatus;

@FunctionalSupplier(name = "loadTxtD2Curve", description = "Load 1 channel with curve from txt.")
public class LoadTxtD2Supplier implements Supplier<VectorD2> {
    private VectorD2 vectorD2;

    private Supplier<String> file;

    public LoadTxtD2Supplier(@Desc("file") Supplier<String> file) {
        this.file = file;
    }

    @Override
    public VectorD2 get() {
        if (vectorD2 == null) {
            TxtCurveXYLoader txtCurveXYLoader = new TxtCurveXYLoader(file.get());
            List<CurveHolder> curveHolders;
            try {
                curveHolders = txtCurveXYLoader.load(new NullLongProgressStatus());
            } catch (Exception e) {
                TxtCurveYLoader txtCurveYLoader = new TxtCurveYLoader(file.get());
                curveHolders = txtCurveYLoader.load(new NullLongProgressStatus());
            }
            if (curveHolders.size() != 1) {
                throw new IllegalStateException(
                        "File " + file.get() + " has " + curveHolders.size() + " curves but VectorD2 can store only 1");
            }
            CurveHolder curveHolder = curveHolders.get(0);
            vectorD2 = CurveVectorConvert.curveHolderToVectorD2(curveHolder);
        }
        return vectorD2;
    }
}
