package cz.jmare.math.curve.func;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.mexpr.type.DoubleHolder;
import cz.jmare.mexpr.type.VectorD1;
import cz.jmare.mexpr.util.Desc;
import cz.jmare.mexpr.util.FunctionalSupplier;
import cz.jmare.mexpr.util.InnerVariable;
import cz.jmare.mexpr.util.UseInnerVariable;

@FunctionalSupplier(name = "foreach", description = "Do an operation with each item of VectorD1. Example: foreach(x * 10, x, d1(1, 2, 3, 4, 5))")
public class ForeachToCurveSupplier implements Supplier<CurveHolder> {
    private Supplier<Double> supplier;
    private DoubleHolder itemHolder;
    private Supplier<VectorD1> vectorD1Supplier;


    public ForeachToCurveSupplier(@Desc("expression") @UseInnerVariable Supplier<Double> supplier, @Desc("var") @InnerVariable DoubleHolder itemHolder, @Desc("values") Supplier<VectorD1> vectorD1Supplier) {
        this.supplier = supplier;
        this.itemHolder = itemHolder;
        this.vectorD1Supplier = vectorD1Supplier;
    }

    @Override
    public CurveHolder get() {
        List<Point> points = new ArrayList<Point>();
        VectorD1 vectorD1 = this.vectorD1Supplier.get();
        int size = vectorD1.size();
        for (int i = 0; i < size; i++) {
            double xVal = vectorD1.get(i);
            itemHolder.accept(xVal);
            Double yVal = supplier.get();
            Point point = new Point(xVal, yVal);
            points.add(point);
        }

        MemoryRawCurveHolder memoryRawCurveHolder = new MemoryRawCurveHolder(points);
        return memoryRawCurveHolder;
    }
}
