package cz.jmare.math.curve.func;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.maren.geometry.interpol.SplineInterpolatorDouble;
import org.maren.geometry.util.GPoint;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.SequentialCurveReader;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "interpol", description = "Parse string with format 'y1, y2, ..., yn' and builds sampled CurveHolder")
public class MonoSplineSupplier implements Supplier<Double> {
    private Supplier<CurveHolder> curveHolderSupplier;

    private Supplier<Double> xSupplier;

    private SplineInterpolatorDouble splineInterpolatorDouble;

    public MonoSplineSupplier(Supplier<CurveHolder> curveHolderSupplier, Supplier<Double> xSupplier) {
        this.curveHolderSupplier = curveHolderSupplier;
        this.xSupplier = xSupplier;
    }

    @Override
    public Double get() {
        if (splineInterpolatorDouble == null) {
            splineInterpolatorDouble = new SplineInterpolatorDouble(toPoints(curveHolderSupplier.get()));
        }
        return splineInterpolatorDouble.interpolate(xSupplier.get());
    }
    
    public static List<GPoint> toPoints(CurveHolder curveHolder) {
        ArrayList<GPoint> result = new ArrayList<GPoint>();
        try (SequentialCurveReader curveReader = curveHolder.getCurveReader()) {
            long size = curveReader.size();
            for (long i = 0; i < size; i++) {
                Point point = curveReader.get(i);
                result.add(new GPoint(point.x, point.y));
            }
        }
        return result;
    }
}
