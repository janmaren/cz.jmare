package cz.jmare.math.curve.func;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.sampled.MemorySampledCurveHolder;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "sampledCurve", description = "Parse string with format 'y1, y2, ..., yn' and builds sampled CurveHolder")
public class SampledCurveSupplier implements Supplier<CurveHolder> {
    private Supplier<String> dataSupplier;
    private Supplier<Double> periodSupplier;
    private Supplier<Double> offsetSupplier;

    private CurveHolder curveHolder;

    public SampledCurveSupplier(Supplier<String> dataSupplier) {
        this.dataSupplier = dataSupplier;
    }

    @Override
    public CurveHolder get() {
        if (curveHolder != null) {
            return curveHolder;
        }
        String string = dataSupplier.get();
        Double period = periodSupplier.get();
        double offset = offsetSupplier != null ? offsetSupplier.get() : 0.0;
        curveHolder = parse(string, period, offset);
        return curveHolder;
    }

    public static CurveHolder parse(String string, double samplingPeriod, double offset) {
        List<Double> points = new ArrayList<>();
        String[] split = string.split(",");
        for (String pointStr : split) {
            points.add(Double.valueOf(pointStr.trim()));
        }
        CurveHolder curveHolder = new MemorySampledCurveHolder(points, samplingPeriod, offset);
        return curveHolder;
    }
}
