package cz.jmare.math.curve.func;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;
import cz.jmare.math.curve.raw.MemoryRawCurveHolder;
import cz.jmare.math.geometry.entity.Point;
import cz.jmare.mexpr.util.FunctionalSupplier;

@FunctionalSupplier(name = "rawCurve", description = "Parse string with format 'x1, y2; x2, y2, ...; xn, yn' and builds CurveHolder")
public class RawCurveSupplier implements Supplier<CurveHolder> {
    private Supplier<String> dataSupplier;

    private CurveHolder curveHolder;

    public RawCurveSupplier(Supplier<String> dataSupplier) {
        this.dataSupplier = dataSupplier;
    }

    @Override
    public CurveHolder get() {
        if (curveHolder != null) {
            return curveHolder;
        }
        String string = dataSupplier.get();
        curveHolder = parse(string);
        return curveHolder;
    }

    public static CurveHolder parse(String string) {
        List<Point> points = new ArrayList<>();
        String[] split = string.split(";");
        for (String pointStr : split) {
            String[] splitPoi = pointStr.split(",");
            if (splitPoi.length != 2) {
                throw new IllegalArgumentException("Invalid point " + splitPoi);
            }
            Point point = new Point(Double.valueOf(splitPoi[0].trim()), Double.valueOf(splitPoi[1].trim()));
            points.add(point);
        }
        CurveHolder curveHolder = new MemoryRawCurveHolder(points);
        return curveHolder;
    }
}
