package cz.jmare.math.curve.func;

import java.util.function.Consumer;
import java.util.function.Supplier;

import cz.jmare.math.curve.CurveHolder;

public class CurveHolderSupplier implements Supplier<CurveHolder>, Consumer<CurveHolder> {
    private CurveHolder value;

    public CurveHolderSupplier(CurveHolder value) {
        this.value = value;
    }

    public CurveHolderSupplier() {
    }

    @Override
    public CurveHolder get() {
        return value;
    }

    @Override
    public void accept(CurveHolder value) {
        this.value = value;
    }
}
