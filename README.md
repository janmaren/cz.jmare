# cz.jmare

Java project which contains several libraries focused on math, signal processing, showing graphs and numeric math

One of such libraries is [mexpr](https://bitbucket.org/stmare/cz.jmare/wiki/mexpr/index.md)

To use the libraries configure your Maven project in this way:

```
<dependency>
    <groupId>cz.jmare</groupId>
    <artifactId>cz.jmare.LIBRARY</artifactId>
    <version>1.LATEST</version>
</dependency>

<repositories>
    <repository>
        <id>jmare_repo</id>
        <url>https://bitbucket.org/stmare/jmare-maven-repo/raw/master/repository/</url>
    </repository>
</repositories>
```
