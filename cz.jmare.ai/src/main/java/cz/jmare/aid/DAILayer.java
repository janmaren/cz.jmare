package cz.jmare.aid;

import java.lang.reflect.InvocationTargetException;

import cz.jmare.ai.actfunc.ActFunc;

public class DAILayer {
    public double[][] weights;
    public double[] biases;
    public double[] outputs;
    public ActFunc actFunc;

    public DAILayer(int neuronsCount, int eachSynapsesCount, ActFunc actFunc) {
        weights = new double[neuronsCount][];
        biases = new double[neuronsCount];
        outputs = new double[neuronsCount];
        for (int i = 0; i < neuronsCount; i++) {
            weights[i] = new double[eachSynapsesCount];
        }
        this.actFunc = actFunc;
    }

    /**
     *
     * @param actFunc
     * @param ds[neuron][synapse] last item in each neuron isn't synapse but bias
     */
    public DAILayer(double[][] ds, Class<?> actFunc) {
        int neuronsCount = ds.length;
        weights = new double[neuronsCount][];
        biases = new double[neuronsCount];
        outputs = new double[neuronsCount];
        for (int i = 0; i < ds.length; i++) {
            double[] wghs = ds[i];
            double[] synaps = new double[wghs.length - 1];
            for (int j = 0; j < synaps.length; j++) {
                synaps[j] = wghs[j];
            }
            biases[i] = wghs[wghs.length - 1];
            weights[i] = synaps;
        }

        try {
            this.actFunc = (ActFunc) actFunc.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            throw new IllegalStateException("Unable to create instance of " + actFunc);
        }
    }
}
