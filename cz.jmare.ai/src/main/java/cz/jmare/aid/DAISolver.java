package cz.jmare.aid;

import cz.jmare.ai.LearningState;
import cz.jmare.ai.actfunc.ActFunc;

/**
 * Simple class to evaluate pretrained neural network using inputs
 */
public class DAISolver {
    private DAILayer[] hiddenLayers;

    public DAISolver(DAILayer[] hiddenLayers) {
        this.hiddenLayers = hiddenLayers;
    }

    public DAISolver(LearningState learningState) {
        int[] layers = learningState.layers;
        hiddenLayers = new DAILayer[layers.length - 1];
        for (int i = 0; i < layers.length - 1; i++) {
            hiddenLayers[i] = new DAILayer(learningState.weights[i], learningState.actFuncs[i]);
        }
    }

    public double[] solve(double... inputs) {
        double[] useInputs = inputs;
        for (int i = 0; i < hiddenLayers.length; i++) {
            DAILayer faiLayer = hiddenLayers[i];
            double[] biases = faiLayer.biases;
            double[][] weights = faiLayer.weights;
            double[] outputs = faiLayer.outputs;
            for (int j = 0; j < biases.length; j++) {
                double[] ws = weights[j];
                double sum = 0;
                for (int k = 0; k < ws.length; k++) {
                    sum += ws[k] * useInputs[k];
                }
                sum += biases[j];
                outputs[j] = faiLayer.actFunc.apply(sum);
            }
            useInputs = outputs;
        }
        return useInputs;
    }
    
    /**
     * Create layers which aren't trained. The training must follow and after that can be used in constructor DAISolver(DAILayer[]) 
     * @param hiddenActFunc activation function for hidden layers
     * @param outputActFunc
     * @param layers
     * @return
     */
    public static DAILayer[] createLayers(ActFunc hiddenActFunc, ActFunc outputActFunc, int... layers) {
        DAILayer[] hiddenLayers = new DAILayer[layers.length - 1];
        int eachSynapsesCount = layers[0];
        for (int i = 1; i < layers.length; i++) {
            hiddenLayers[i - 1] = new DAILayer(layers[i], eachSynapsesCount, i < layers.length - 1 ? hiddenActFunc : outputActFunc);
            eachSynapsesCount = layers[i];
        }
        return hiddenLayers;
    }
}
