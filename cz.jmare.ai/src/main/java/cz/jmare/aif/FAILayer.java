package cz.jmare.aif;

import java.lang.reflect.InvocationTargetException;

import cz.jmare.ai.factfunc.FActFunc;

public class FAILayer {
    public float[][] weights;
    public float[] biases;
    public float[] outputs;
    public FActFunc actFunc;

    public FAILayer(int neuronsCount, int eachSynapsesCount, FActFunc actFunc) {
        weights = new float[neuronsCount][];
        biases = new float[neuronsCount];
        outputs = new float[neuronsCount];
        for (int i = 0; i < neuronsCount; i++) {
            weights[i] = new float[eachSynapsesCount];
        }
        this.actFunc = actFunc;
    }

    /**
     *
     * @param actFunc
     * @param ds[neuron][synapse] last item in each neuron isn't synapse but bias
     */
    public FAILayer(double[][] ds, Class<?> actFunc) {
        int neuronsCount = ds.length;
        weights = new float[neuronsCount][];
        biases = new float[neuronsCount];
        outputs = new float[neuronsCount];
        for (int i = 0; i < ds.length; i++) {
            double[] wghs = ds[i];
            float[] synaps = new float[wghs.length - 1];
            for (int j = 0; j < synaps.length; j++) {
                synaps[j] = (float) wghs[j];
            }
            biases[i] = (float) wghs[wghs.length - 1];
            weights[i] = synaps;
        }

        try {
            String packageName = actFunc.getPackageName();
            String simpleName = actFunc.getSimpleName();
            if (packageName.equals("cz.jmare.ai.actfunc")) {
                packageName = "cz.jmare.ai.factfunc";
            } else {
                throw new IllegalStateException("Unable to convert activation function to float - unknown package " + packageName);
            }
            simpleName = "F" + simpleName;
            actFunc = Class.forName(packageName + "." + simpleName);
            this.actFunc = (FActFunc) actFunc.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException | ClassNotFoundException e) {
            throw new IllegalStateException("Unable to create instance of " + actFunc);
        }
    }
}
