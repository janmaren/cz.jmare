package cz.jmare.aif;

import cz.jmare.ai.LearningState;
import cz.jmare.ai.factfunc.FActFunc;

public class FAISolver {
    private FAILayer[] hiddenLayers;

    public FAISolver(FActFunc hiddenActFunc, FActFunc outputActFunc, int... layers) {
        hiddenLayers = new FAILayer[layers.length - 1];
        int eachSynapsesCount = layers[0];
        for (int i = 1; i < layers.length; i++) {
            hiddenLayers[i - 1] = new FAILayer(layers[i], eachSynapsesCount, i < layers.length - 1 ? hiddenActFunc : outputActFunc);
            eachSynapsesCount = layers[i];
        }
    }

    public FAISolver(LearningState learningState) {
        int[] layers = learningState.layers;
        hiddenLayers = new FAILayer[layers.length - 1];
        for (int i = 0; i < layers.length - 1; i++) {
            hiddenLayers[i] = new FAILayer(learningState.weights[i], learningState.actFuncs[i]);
        }
    }

    public float[] solve(float[] inputs) {
        float[] useInputs = inputs;
        for (int i = 0; i < hiddenLayers.length; i++) {
            FAILayer faiLayer = hiddenLayers[i];
            float[] biases = faiLayer.biases;
            float[][] weights = faiLayer.weights;
            float[] outputs = faiLayer.outputs;
            for (int j = 0; j < biases.length; j++) {
                float[] ws = weights[j];
                float sum = 0;
                for (int k = 0; k < ws.length; k++) {
                    sum += ws[k] * useInputs[k];
                }
                sum += biases[j];
                outputs[j] = (float) faiLayer.actFunc.apply(sum);
            }
            useInputs = outputs;
        }
        return useInputs;
    }
}
