package cz.jmare.ai.factfunc;

public interface FActFunc {
    float apply(float n);

    /**
     * Derivation for backward propagation
     * @param n
     * @return
     */
    float applyDerivation(float n);
}
