package cz.jmare.ai.factfunc;

public class FSoftplus implements FActFunc  {
    @Override
    public float apply(float x) {
        return (float) Math.log(1f + Math.exp(x));
    }

    @Override
    public float applyDerivation(float x) {
        return (float) (1.0f / (1f + Math.exp(-x)));
    }
}
