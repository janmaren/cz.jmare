package cz.jmare.ai.factfunc;

/**
 * Logistic (sigmoid)
 * (0, 1)
 */
public class FLogsig implements FActFunc {

    @Override
    public float apply(float n) {
        return (float) (1.0d / (1.0d + Math.exp(-n)));
    }

    @Override
    public float applyDerivation(float n) {
        float a = apply(n);
        return a * (1 - a);
    }
}
