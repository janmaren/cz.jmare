package cz.jmare.ai.factfunc;

public class Fastexp {
    static {
        buildexptable(-10, 10, 0.01);
    }
    /* fast floating point exp function
     * must initialize table with buildexptable before using

    Based on
     A Fast, Compact Approximation of the Exponential Function
     Nicol N. Schraudolph 1999
    Adapted to single precision to improve speed and added adjustment table to improve accuracy.
    Alrecenk 2014
     * i = ay + b
     * a = 2^(mantissa bits) / ln(2)   ~ 12102203
     * b = (exponent bias) * 2^ ( mantissa bits) ~ 1065353216
     */
    public static float fastexp(float x) {
        final int temp = (int) (12102203 * x + 1065353216);
        return Float.intBitsToFloat(temp) * expadjust[(temp >> 15) & 0xff];
    }

    static float expadjust[];

    //build correction table to improve result in region of interest
    //if region of interest is large enough then improves result everywhere
    public static void buildexptable(double min, double max, double step) {
        expadjust = new float[256];
        int amount[] = new int[256];
        // calculate what adjustments should have been for values in region
        for (double x = min; x < max; x += step) {
            double exp = Math.exp(x);
            int temp = (int) (12102203 * x + 1065353216);
            int index = (temp >> 15) & 0xff;
            double fexp = Float.intBitsToFloat(temp);
            expadjust[index] += exp / fexp;
            amount[index]++;
        }
        // average them out to get adjustment table
        for (int k = 0; k < amount.length; k++) {
            expadjust[k] /= amount[k];
        }
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        double exp = Math.exp(0.5);
        long end = System.nanoTime();
        System.out.println(exp);

        long start3 = System.nanoTime();
        float exp3 = fastexp(0.5f);
        long end3 = System.nanoTime();
        System.out.println(exp3);

        System.out.println("First " + (end - start));
        System.out.println("Second " + (end3 - start3));
    }
}
