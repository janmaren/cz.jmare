package cz.jmare.ai;

import static org.junit.jupiter.api.Assertions.assertEquals;

import cz.jmare.ai.util.BPSolverUtil;
import cz.jmare.aid.DAISolver;

public class BPSolverTest {
    static double[][] inputs = new double[][] { new double[] { 0, 1 }, new double[] { 0, 0 }, new double[] { 1, 0 },
            new double[] { 1, 1 } };
    static double[][] targets = new double[][] { new double[] { 1 }, new double[] { 0 }, new double[] { 1 },
            new double[] { 0 } };

    public void testXorBothSolvers() {
        BPSolver bpSolver = new BPSolver(2, 3, 1);
        BPSolverUtil.applySmartParameters(bpSolver);
        bpSolver.train(inputs, targets);
        
        double[] solveByBpSolver = bpSolver.solve(0, 1);
        
        LearningState learningState = bpSolver.getLearningState();
        
        DAISolver daiSolver = new DAISolver(learningState);
        double[] solveByDaiSolver = daiSolver.solve(0, 1);
        assertEquals(solveByBpSolver[0], solveByDaiSolver[0]);
     }
}
